
#ifndef _CPDF_TEMPLATE_GENERATOR_
#define _CPDF_TEMPLATE_GENERATOR_

#pragma comment(lib,"libhpdf")


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <setjmp.h>
#include <time.h>
#include <libharu-2.1.0-vc8\include\hpdf.h>


class CPDF_Template_Generator
{

	jmp_buf env;

public:

	typedef struct HPDF_Margins
	{
		HPDF_REAL top;
		HPDF_REAL bottom;
		HPDF_REAL left;
		HPDF_REAL right;
	}HPDF_Margins;


	HPDF_Doc  pdf;    
	HPDF_Page page;
	HPDF_Font font;
	int  LineSpace;
	int   FontSize;   

	/* Common Header Parameters */
	float CHeaderUperCornerX;
	float CHeaderUperCornerY;
	float CHWidth;
	float CHHeight;


	/* For Two Column base writing */
	float clmX[2];	
	float clmWidth;

	HPDF_Margins HPDF_Margin;

	int PageHeight,PageWidth;

	CString m_szLastError;

	CString GetErrorString()
	{
		return m_szLastError;
	}

	/* Common utility functions used for writing PDFs */
	void setDefaultParameters()
	{
		m_szLastError = "";
		
		/* page size for A4 default 
		* 600 dpi (print) = 4960 X 7016 pixels
		*/	
		/* create default-font */
		font = HPDF_GetFont (pdf, "Helvetica", NULL);
		FontSize = 6;
		LineSpace = -1;
		HPDF_Margin.top    = 20;	
		HPDF_Margin.bottom = 20;
		HPDF_Margin.left   = 35; 
		HPDF_Margin.right  = 55; 

		/* Parameters for Commmon Headers..*/
		CHeaderUperCornerX = 30;
		CHeaderUperCornerY = PageHeight - (10*FontSize) - 200;
		CHWidth = 535;
		CHHeight = 10 * FontSize;

	}

	
	void HPDF_AddSizedPage( HPDF_PageSizes size = HPDF_PAGE_SIZE_A4 , HPDF_PageDirection  direction = HPDF_PAGE_PORTRAIT )
	{
		m_szLastError = "";
		page = HPDF_AddPage(pdf) ;
		HPDF_Page_SetSize(page,size,direction);		
		
	}

	void SetMargin( HPDF_REAL top=30,HPDF_REAL bottom=30,HPDF_REAL left=15,HPDF_REAL right=15)
	{
		m_szLastError = "";
		HPDF_Margin.top    = top;	
		HPDF_Margin.bottom = bottom;
		HPDF_Margin.left   = left; 
		HPDF_Margin.right  = right; 	
	}

	/* Default Constructor */
	CPDF_Template_Generator()
	{	
		m_szLastError = "";
		pdf = HPDF_New (&(CPDF_Template_Generator::error_handler), NULL);
		if (!pdf) {
			//MessageBox(0,_T("error: cannot create PdfDoc object"),0,0);
			return;
		}

		if (setjmp(env)) {
			HPDF_Free (pdf);
			//MessageBox(0,_T("Jumped"),0,0);
			return;
		}

		/* set compression mode */
		HPDF_SetCompressionMode (pdf, HPDF_COMP_ALL);

	
		/* add a new page object. */
		HPDF_AddSizedPage();				
	
		PageHeight = HPDF_Page_GetHeight (page );				
		PageWidth = HPDF_Page_GetWidth (page );

		/* Specify Default Parameters */
		setDefaultParameters();

		/* Initializes the text mode(GMODE) */
		HPDF_Page_BeginText(page);
		HPDF_Page_SetFontAndSize (page, font, FontSize);
		HPDF_Page_MoveTextPos(page,HPDF_Margin.left ,PageHeight-HPDF_Margin.top); 

		/* Set Defaults for ColumnBase writing */
		HPDF_Point s_currentPos = HPDF_GetNewLinePoint();
		clmX[0]  = s_currentPos.x;
		clmX[1]  = s_currentPos.x + 200; 
		clmWidth = 200.00;

		
		 
		//HPDF_Page_EndText(page); 
	}

	// Returns NewLine Point and if needed,
	// Adds page to the Document
	HPDF_Point HPDF_GetNewLinePoint()
	{
		m_szLastError = "";

		HPDF_Point s_currentPos = HPDF_Page_GetCurrentTextPos(page);
		s_currentPos.x = HPDF_Margin.left;
		s_currentPos.y +=LineSpace - FontSize;
		if( s_currentPos.y < HPDF_Margin.bottom)
		{
			HPDF_AddSizedPage(); 
			HPDF_Page_BeginText(page);
			HPDF_Page_SetFontAndSize(page, font, FontSize);
			s_currentPos.x = HPDF_Margin.left;
			s_currentPos.y = PageHeight-HPDF_Margin.top;
		}
		return s_currentPos;
	}
	HPDF_Point HPDF_GetNewLinePoint(HPDF_Point s_currentPos)
	{		 
		m_szLastError = "";

		s_currentPos.x = HPDF_Margin.left;
		s_currentPos.y +=LineSpace - FontSize;
		if( s_currentPos.y < HPDF_Margin.bottom)
		{
			HPDF_AddSizedPage(); 
			HPDF_Page_BeginText(page);
			HPDF_Page_SetFontAndSize(page, font, FontSize);
			s_currentPos.x = HPDF_Margin.left;
			s_currentPos.y = PageHeight-HPDF_Margin.top;
		}
		return s_currentPos;
	}
	int HPDF_MeasureText(HPDF_REAL textWidth)
	{
		m_szLastError = "";

		HPDF_Point s_currentPos = HPDF_Page_GetCurrentTextPos(page);  
		if( (s_currentPos.x +textWidth) > (PageWidth-HPDF_Margin.right) )
			return 1;// Text is out of width

		return 0;// OK
	}
	void PDF_Write(const char *str)
	{
		/* For more information :--
		 * http://libharu.org/wiki/Documentation/API/Page#HPDF_Page_TextWidth.28.29
		 */

		//HPDF_Page_BeginText (page);
		HPDF_Page_SetFontAndSize(page, font, FontSize);
		HPDF_REAL textWidth =  HPDF_Page_TextWidth (page, str);		

		HPDF_Point s_currentPos = HPDF_Page_GetCurrentTextPos(page);

		
		/* IF New line needed to write the Sentence */
		if( HPDF_MeasureText(textWidth))					
			s_currentPos = HPDF_GetNewLinePoint();	

		
		HPDF_Page_TextOut (page,s_currentPos.x,s_currentPos.y,str);  
		//HPDF_Page_EndText(page);
	}

	void PDF_WriteLine(const char *str)
	{
		/* For more information :--
		 * http://libharu.org/wiki/Documentation/API/Page#HPDF_Page_TextWidth.28.29
		 */
	
		//HPDF_Page_BeginText(page); 
		HPDF_Page_SetFontAndSize(page, font, FontSize);	
		HPDF_Point s_currentPos = HPDF_GetNewLinePoint();
			
		HPDF_Page_TextOut (page,s_currentPos.x,s_currentPos.y,str);  
		//HPDF_Page_EndText(page);
	}
	
	// x, y = 0 means Current Position
	void PDF_PutPngImage(const char *pngImageFile,HPDF_REAL x = 0,HPDF_REAL y=0, bool Center = true)	
	{		
		m_szLastError = "";

		HPDF_Point pos = HPDF_Page_GetCurrentTextPos(page);
		
		HPDF_Page_EndText(page);
		HPDF_Image img =  HPDF_LoadPngImageFromFile(pdf,pngImageFile);
		double iw = 120;//HPDF_Image_GetWidth (img);
		double ih = 50;//HPDF_Image_GetHeight (img);

		if(y==0) 
			y  = pos.y - ih;

		if(Center)		
			x = (PageWidth/2.0) - (iw/2);
		else if(x == 0)
			x = HPDF_Margin.left;

		/* Draw image to the canvas. (normal-mode with actual size.)*/

		DWORD S = HPDF_Page_DrawImage(page,img,x,y,iw,ih);
		
		if(S != HPDF_OK)
		{
			CString status;
			status.Format(_T("Error Status: %d @ (PutPngImage)"),S);
			//MessageBox(0,status,0,0);
			status.ReleaseBuffer();
		}		

		pos.y = y;
		S = HPDF_Page_BeginText(page);
		if(S != HPDF_OK)
		{
			CString status;
			status.Format(_T("Error Status: %d @ (PutPngImage)"),S);
			//MessageBox(0,status,0,0);
			status.ReleaseBuffer();
		}		
		pos = HPDF_GetNewLinePoint(pos);		
		S = HPDF_Page_MoveTextPos(page,pos.x ,pos.y);

		if(S != HPDF_OK)
		{
			CString status;
			status.Format(_T("Error Status: %d @ (PutPngImage)"),S);
			//MessageBox(0,status,0,0);
			status.ReleaseBuffer();
		}		
	}

	static void error_handler (HPDF_STATUS error_no,HPDF_STATUS detail_no, void  *user_data)
	{
		printf ("ERROR: error_no=%04X, detail_no=%u\n", (HPDF_UINT)error_no,(HPDF_UINT)detail_no);	
	}

	bool SaveToFile(const char *fname)
	{
		m_szLastError = "";

		DWORD S = HPDF_SaveToFile (pdf, fname);

		if(S != HPDF_OK)
		{
			LPVOID lpMsgBuf;
			FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
						  NULL, GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&lpMsgBuf, 0, NULL);
			m_szLastError = (LPTSTR)lpMsgBuf;
			m_szLastError.TrimRight();
			LocalFree(lpMsgBuf);
			return false;
		}	

		return true;
	}

	~CPDF_Template_Generator()
	{		
		HPDF_Free (pdf);
	}


	/* These Methods will be used in Real Template Generation */
	void DrawRectangle(HPDF_REAL x,HPDF_REAL y,HPDF_REAL width,HPDF_REAL height )
	{
		HPDF_Page_EndText(page);
		HPDF_Page_Rectangle(page,x,y,width,height);
		HPDF_Page_Stroke(page);
		HPDF_Page_BeginText(page);
	}	

	void OpenCommonHeader(int LeftUperX = 30,  int numberOfLines = 10, int width = 535)
	{
		CHeaderUperCornerX = (float)LeftUperX;
		CHeaderUperCornerY = PageHeight - (numberOfLines*FontSize) - 85; 
		CHWidth = width;  
		CHHeight = numberOfLines* ((-LineSpace)+ FontSize);
	}

	void CloseCommonHeader()
	{
		DrawRectangle(CHeaderUperCornerX , CHeaderUperCornerY,CHWidth,CHHeight );			
	}

	void OpenCommonFooter(int LeftUperX = 30,  int numberOfLines = 10, int width = 535)
	{
		CHeaderUperCornerX = (float)LeftUperX;
		CHeaderUperCornerY = PageHeight - (numberOfLines*FontSize) - 503; 
		CHWidth = width;  
		CHHeight = numberOfLines* ((-LineSpace)+ FontSize);
	}

	void CloseCommonFooter()
	{
		DrawRectangle(CHeaderUperCornerX , CHeaderUperCornerY,CHWidth,CHHeight );			
	}

	void OpenColumnWriting(int FirstColumnWidth = 150 /* @ 26 Charecter of FontSize = 15 */)
	{
		HPDF_Point s_currentPos = HPDF_GetNewLinePoint();
		clmX[0]  = s_currentPos.x;
		clmX[1]  = s_currentPos.x + FirstColumnWidth; 
		clmWidth = (float)FirstColumnWidth;
		
	}

	
		
	void PDF_Write(const char *str,int ColumnNumber )
	{
		/* For more information :--
		 * http://libharu.org/wiki/Documentation/API/Page#HPDF_Page_TextWidth.28.29
		 */

		//HPDF_Page_BeginText (page);
		HPDF_Page_SetFontAndSize(page, font, FontSize);
		HPDF_Point s_currentPos = HPDF_Page_GetCurrentTextPos(page);

		s_currentPos.x = clmX[ColumnNumber]; 		
				
		HPDF_Page_TextOut (page,s_currentPos.x,s_currentPos.y,str);  
		//HPDF_Page_EndText(page);
	}

	void PDF_WriteLine(const char *str,int ColumnNumber)
	{
		/* For more information :--
		 * http://libharu.org/wiki/Documentation/API/Page#HPDF_Page_TextWidth.28.29
		 */
	
		//HPDF_Page_BeginText(page); 
		HPDF_Page_SetFontAndSize(page, font, FontSize);	

		HPDF_Point s_currentPos = HPDF_GetNewLinePoint();
		s_currentPos.x = clmX[ColumnNumber]; 		

		HPDF_Page_TextOut (page,s_currentPos.x,s_currentPos.y,str);  
		//HPDF_Page_EndText(page);
	}



};


#endif


/*
		 if((xpos+textWidth) >= PageWidth)
			xpos = 0.0;
		 HPDF_Page_MoveTextPos (page, xpos, ypos);
	
		 
		
		 HPDF_Page_ShowText (page, font_list[i]);
		 HPDF_Page_MoveTextPos (page, 0, -18);
		 HPDF_UINT  textWidth = 26;
		 
		 HPDF_Page_TextOut (page, xpos, ypos, str);
		HPDF_Page_EndText (page);
			
		xpos = xpos + textWidth;
		*/