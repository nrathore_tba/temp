
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <shared/MDDDataHandler.hpp>


#ifdef _DEBUG
#pragma comment(lib,"\\Thebeast\\application\\BFImageSharedD.lib")
#pragma comment(lib,"\\SharedApps\\BFInterestRateSharedD.lib")
#pragma comment(lib,"\\TheBeast\\Shared\\BFMarketD.lib")
#pragma comment(lib,"\\TheBeast\\Shared\\BFSvrFieldsD.lib")
#pragma comment(lib,"\\TheBeast\\Shared\\BFUtilD.lib")
#pragma comment(lib,"\\TheBeast\\Shared\\persistd.lib")
#else
#pragma comment(lib,"\\Thebeast\\application\\BFImageShared.lib")
#pragma comment(lib,"\\SharedApps\\BFInterestRateShared.lib")
#pragma comment(lib,"\\TheBeast\\Shared\\BFMarket.lib")
#pragma comment(lib,"\\TheBeast\\Shared\\BFSvrFields.lib")
#pragma comment(lib,"\\TheBeast\\Shared\\BFUtil.lib")
#pragma comment(lib,"\\TheBeast\\Shared\\persistr.lib")
#endif


BF_NAMESPACE_BEGIN

using namespace std;

const string __strHeader = "TRADE_DATE,TRADE_TIME,CALL_TIME,CONTRACT_SYMBOL,TICKER_SYMBOL,FOI_SYMBOL_IND,TICKER_SYMBOL_DESCRIPTION,MARKET_SECTOR,ASSET_CLASS,CONTRACT_YEAR,CONTRACT_MONTH,STRIKE_PRICE,PUT_CALL,EXCH_CODE,TRADE_PRICE,TRADE_QUANTITY,SOURCE,SPREAD_TYPE,SPREAD_DESCRIPTION,CONTRACT_SYMBOL_2,TICKER_SYMBOL_2,TICKER_SYMBOL_DESCRIPTION_2,MARKET_SECTOR_2,ASSET_CLASS_2,FOI_SYMBOL_IND_2,CONTRACT_YEAR_2,CONTRACT_MONTH_2,STRIKE_PRICE_2,PUT_CALL_2,EXCH_CODE_2,TRADE_PRICE_2,TRADE_QUANTITY_2,CONTRACT_SYMBOL_3,TICKER_SYMBOL_3,TICKER_SYMBOL_DESCRIPTION_3,MARKET_SECTOR_3,ASSET_CLASS_3,FOI_SYMBOL_IND_3,CONTRACT_YEAR_3,CONTRACT_MONTH_3,STRIKE_PRICE_3,PUT_CALL_3,EXCH_CODE_3,CONTRACT_SYMBOL_4,TICKER_SYMBOL_4,TICKER_SYMBOL_DESCRIPTION_4,MARKET_SECTOR_4,ASSET_CLASS_4,FOI_SYMBOL_IND_4,CONTRACT_YEAR_4,CONTRACT_MONTH_4,STRIKE_PRICE_4,PUT_CALL_4,EXCH_CODE_4,\n";

const CString __csOPColumnName[] =
{
	"TRADE_DATE",
	"TRADE_TIME",
	"CALL_TIME",
	"CONTRACT_SYMBOL",
	"TICKER_SYMBOL",
	"FOI_SYMBOL_IND",
	"TICKER_SYMBOL_DESCRIPTION",
	"MARKET_SECTOR",
	"ASSET_CLASS",
	"CONTRACT_YEAR",
	"CONTRACT_MONTH",
	"STRIKE_PRICE",
	"PUT_CALL",
	"EXCH_CODE",
	"TRADE_PRICE",
	"TRADE_QUANTITY",
	"SOURCE",
	"SPREAD_TYPE",
	"SPREAD_DESCRIPTION",
	"CONTRACT_SYMBOL_2",
	"TICKER_SYMBOL_2",
	"TICKER_SYMBOL_DESCRIPTION_2",
	"MARKET_SECTOR_2",
	"ASSET_CLASS_2",
	"FOI_SYMBOL_IND_2",
	"CONTRACT_YEAR_2",
	"CONTRACT_MONTH_2",
	"STRIKE_PRICE_2",
	"PUT_CALL_2",
	"EXCH_CODE_2",
	"TRADE_PRICE_2",
	"TRADE_QUANTITY_2",
	"CONTRACT_SYMBOL_3",
	"TICKER_SYMBOL_3",
	"TICKER_SYMBOL_DESCRIPTION_3",
	"MARKET_SECTOR_3",
	"ASSET_CLASS_3",
	"FOI_SYMBOL_IND_3",
	"CONTRACT_YEAR_3",
	"CONTRACT_MONTH_3",
	"STRIKE_PRICE_3",
	"PUT_CALL_3",
	"EXCH_CODE_3",
	"CONTRACT_SYMBOL_4",
	"TICKER_SYMBOL_4",
	"TICKER_SYMBOL_DESCRIPTION_4",
	"MARKET_SECTOR_4",
	"ASSET_CLASS_4",
	"FOI_SYMBOL_IND_4",
	"CONTRACT_YEAR_4",
	"CONTRACT_MONTH_4",
	"STRIKE_PRICE_4",
	"PUT_CALL_4",
	"EXCH_CODE_4"

};


// 54 columns..
enum ColumnName
{
	enm_TRADE_DATE,
	enm_TRADE_TIME,
	enm_CALL_TIME,
	enm_CONTRACT_SYMBOL,
	enm_TICKER_SYMBOL,
	enm_FOI_SYMBOL_IND,
	enm_TICKER_SYMBOL_DESCRIPTION,
	enm_MARKET_SECTOR,
	enm_ASSET_CLASS,
	enm_CONTRACT_YEAR,
	enm_CONTRACT_MONTH,
	enm_STRIKE_PRICE,
	enm_PUT_CALL,
	enm_EXCH_CODE,
	enm_TRADE_PRICE,
	enm_TRADE_QUANTITY,
	enm_SOURCE,
	enm_SPREAD_TYPE,
	enm_SPREAD_DESCRIPTION,
	enm_CONTRACT_SYMBOL_2,
	enm_TICKER_SYMBOL_2,
	enm_TICKER_SYMBOL_DESCRIPTION_2,
	enm_MARKET_SECTOR_2,
	enm_ASSET_CLASS_2,
	enm_FOI_SYMBOL_IND_2,
	enm_CONTRACT_YEAR_2,
	enm_CONTRACT_MONTH_2,
	enm_STRIKE_PRICE_2,
	enm_PUT_CALL_2,
	enm_EXCH_CODE_2,
	enm_TRADE_PRICE_2,
	enm_TRADE_QUANTITY_2,
	enm_CONTRACT_SYMBOL_3,
	enm_TICKER_SYMBOL_3,
	enm_TICKER_SYMBOL_DESCRIPTION_3,
	enm_MARKET_SECTOR_3,
	enm_ASSET_CLASS_3,
	enm_FOI_SYMBOL_IND_3,
	enm_CONTRACT_YEAR_3,
	enm_CONTRACT_MONTH_3,
	enm_STRIKE_PRICE_3,
	enm_PUT_CALL_3,
	enm_EXCH_CODE_3,
	enm_CONTRACT_SYMBOL_4,
	enm_TICKER_SYMBOL_4,
	enm_TICKER_SYMBOL_DESCRIPTION_4,
	enm_MARKET_SECTOR_4,
	enm_ASSET_CLASS_4,
	enm_FOI_SYMBOL_IND_4,
	enm_CONTRACT_YEAR_4,
	enm_CONTRACT_MONTH_4,
	enm_STRIKE_PRICE_4,
	enm_PUT_CALL_4,
	enm_EXCH_CODE_4,

};

struct BBODetail
{
	vector<string> strRecord;

	void GetRowMessage(string &msg )
	{
		msg = strRecord[enm_TRADE_DATE];
		for(unsigned int iI = 1; iI < strRecord.size(); iI++ )
		{
			msg += "," + strRecord[iI];
		}
		msg += "\n";		
	}
};	

typedef vector<BBODetail> vec_BBODetail;

struct BBOSubscriptionDtl
{
	BBOSubscriptionDtl()
	{
		fpOut = NULL;
		iRowCount = 0;
		iOrderId  = 0;
		iBlockType = 0;

		stCUST_NAME = "";
		stCONTACT_EMAIL = "";
		stDEST_ACCOUNT = "";
		stFTP_PASSWORD = "";
		stVENUE_CODE = "";
		stALL_PRODUCTS = "";
		stPROD_CODE = "";
		stBLOCK_CODE = "";
		stEXCH_CODE = "";
		stOutputFileName = "";
	}

	FILE *fpOut;
	int iRowCount;
	int iOrderId;
	int iBlockType; // BLOCK , BLOCKP

	CString stCUST_NAME;
	CString stCONTACT_EMAIL;
	CString stDEST_ACCOUNT;
	CString stFTP_PASSWORD;
	CString stVENUE_CODE;
	CString stALL_PRODUCTS;
	CString stPROD_CODE;
	CString stBLOCK_CODE;
	CString stEXCH_CODE;
	CString stOutputFileName;	
};

struct FileDtl
{
	string strFileName;
	string strFilePath;
	FILE *fpOut;
	int iRowCount;

	FileDtl( )
	{
		strFileName = "";
		strFilePath = "";
		fpOut = NULL;
		iRowCount = 0;
	}
};

struct SubFileDtl
{
	string strDestAccount;
	string strFileName;
	string strFolderPath;

	string strOutPutFileName;

	FILE *fpOut;
	int iRowCount;
	int iFileCount;
	int iUploadReturn;

	SubFileDtl( )
	{
		strFileName = "";
		strFolderPath = "";
		fpOut = NULL;
		iRowCount = 0;
		iFileCount = 0;
		iUploadReturn = 999;
	}
};

typedef map<string, SubFileDtl> SubFileDtlMap;

typedef map<string, FileDtl> FileDtlMap;
typedef vector<BBOSubscriptionDtl> vec_BBOSubscriptionDtl;

class CME_BBO_TNS_DECODE : public BaseImage
{
public:

	enum FieldIDs
	{
		e_Message   = 11,

		e_DestAccount = 56,
		e_S3Bucket = 57,

		e_DefaultBasePath	  =	100,
		e_DirectoryInputPath  = 101,
		e_DirectoryOutputPath = 102,

		e_DefaultDate   = 205,
		e_FileExtList   = 210,
		e_Block_Or_Blocp = 207,

		e_UploadAllFiles		= 216,
		e_ViewSubscription	 = 220,
		e_ProcessSubscription		= 211,

		e_PgUpBase		= 90,
		e_PgDnBase		= 91,

		e_SubscrID		=  1000,
		e_CUST_NAME		=  1050,
		e_CONTACT_EMAIL =  1100,
		e_DEST_AC_FTP_PASSWORD = 1150,
		e_VENUE_CODE	= 1200,
		e_ALL_PRODUCTS	= 1250,
		e_PROD_CODE		= 1300,
		e_BLOCK_CODE		= 1350,
		e_EXCH_CODE		= 1400,
		e_OutputFileName= 1450,		
		e_Upload		= 1500,

	};

	CME_BBO_TNS_DECODE();
	~CME_BBO_TNS_DECODE();	

	bool InitData();		
	void Recalculate();	

	void GenerateFileName();

	static const int m_niInputColumnCount = 72;
	static const int m_niOutputColumnCount = 70;

	FileDtlMap m_clProdFileDtlMap;
	FileDtlMap m_clExchFileDtlMap;

	vec_BBOSubscriptionDtl	m_cl_vecBBOSubscriptionDtl;
	SubFileDtlMap m_clSubFileDtlMap;

	void fnUncompressGZ( );
	void ReadBlockCSVFile( );
	void ParseLine( string &ssLine );

	bool fnCheckInSubscription( BBODetail   &blockRowData  );
	void fnAddRowInSubscription( BBODetail &clBBODetail, FileDtl &blockSubDtl );

	void LoadSubscription();
	void fnPageUpDown( );
	void DisplaySubscription();

	void UploadSubscription();

	bool CreateSubscriptionFile(SubFileDtl &clSubFileDtl, std::string &strInputFile, FILE *fileIn );

private:
	
	BaseField * m_strDefaultBasePath ;
	BaseField * m_strDirectoryInputPath ; 
	BaseField * m_strDirectoryOutputPath ;
	BaseField * m_strMessage ;

	BaseField * m_btnViewSubscription ;
	BaseField * m_btnProcessSubscription ;
	BaseField * m_btnUploadAllFiles;

	BaseField * m_btnPageUp ;
	BaseField * m_btnPageDown ;
	
	ListField * m_fld_FileExtensionList; //gz or zip
	ListField * m_fld_BlockOrBlocpList; //BLOCK OR BLOP

	ListField * m_S3Bucket ;
	ListField * m_DestAccount ;

	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpDBInt;

	int m_numofRows;
	int m_curTopRow;

public:

	bool m_bIsThreadCompleted;
	bool m_bIsThreadRunning;

	CString m_csMsg;
	int m_iTotalCount;

	void Parse();

	CCriticalSection	m_MsgCriticalSec;

	void fnWriteMessage(CString csString )
	{
		CSingleLock clMsgSingleLock(&m_MsgCriticalSec);

		clMsgSingleLock.Lock();

		m_csMsg = csString.GetString();

		clMsgSingleLock.Unlock();
	}

	CString fnReadMessage( )
	{
		CSingleLock clMsgSingleLock(&m_MsgCriticalSec);

		clMsgSingleLock.Lock();

		CString csStr = m_csMsg.GetString();

		clMsgSingleLock.Unlock();

		return csStr;
	}

};

BF_NAMESPACE_END


