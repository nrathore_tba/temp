#include <fstream>
#include <sstream>
#include <stdafx.h> 
#include <shldisp.h>
#include <tlhelp32.h>
#include "zip.h"
#include "EodDataProcessingFinapp.hpp"


#define SPLIT 49999

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(EodDataProcessingFinapp)

EodDataProcessingFinapp::EodDataProcessingFinapp()
{	
		/*HZIP hz = CreateZip("c:\\simple1.zip",0);
	    ZipAdd(hz,"ziptest\\test1\\21363_SwarmRecapReport.csv","c:\\ziptest\\test1\\21363_SwarmRecapReport.csv");	
        ZipAdd(hz,"ziptest\\test2\\21363_SwarmRecapReport.csv","c:\\ziptest\\test2\\21363_SwarmRecapReport.csv");	
		CloseZip(hz);*/
}

EodDataProcessingFinapp::~EodDataProcessingFinapp()
{
}

bool EodDataProcessingFinapp::InitData()
{
    GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));

/*	COleDateTime dtToday = COleDateTime::GetCurrentTime();
	BFDate bfToday;
	bfToday.SetYearMonthDate(dtToday.GetYear(),dtToday.GetMonth() - 1, dtToday.GetDay() ); */
	
	fld_Type = (ListField*) GetField(e_Type);
	fld_ParseYear = (ListField*) GetField(e_ParseYear);
	fld_InputFilePath = GetField( e_InputFilePath );
	fld_HidOutputDir = GetField(e_HiddenOutputFolder);	
	fld_Status = GetField(e_Status);

	m_bIsThreadRunning = false;
	m_lTotalRead = 0;
	
	return true;
}

void EodDataProcessingFinapp::fnUncompress( )
{
	if( !IsFieldNew(e_Uncompress) )
		return;

	CString csInputFile = fld_InputFilePath->GetValueString();
	if( csInputFile.Find(".gz") == -1 )
	{
		fld_Status->SetValueString("Input file path is not exist ..!!");
		return;
	}

	CString csUncompFileName = csInputFile;
	csUncompFileName.Replace(".gz","");
	CString csCommand;
	//csCommand.Format("gzip.exe -d -c \\powervault3\CME_s3\daily\endofday\20150805\EOD_20150805_E.gz > \\powervault3\CME_s3\daily\endofday\20150805\EOD_20150805_E.txt");
	csCommand.Format("gzip.exe -d -c %s > %s", csInputFile, csUncompFileName);
		
	int	iValue = system(csCommand);

	if( iValue == 0 )
	{
		//UpdateUncompressflag(1, index);
		fld_Status->SetValueString("Uncomprssed file successfully.");
	}
	else
	{
		CString csTemp;
		csTemp.Format("error no = %d - path %s, uncompress unsuccessful",iValue, csInputFile);
		fld_Status->SetValueString(csTemp);
	}
}

UINT EODProcessor(void *pVoid)
{
	EodDataProcessingFinapp *pParser = (EodDataProcessingFinapp*)pVoid;
	
	pParser->m_lTotalRead = 0;
	pParser->m_bIsThreadRunning = true;
	pParser->autoParseFile( );
	pParser->m_bIsThreadRunning = false;

	return 0;
}


void EodDataProcessingFinapp::Recalculate()
{
	if( m_bIsThreadRunning )
	{
		CString csTemp;
		csTemp.Format("Reading rows %d",m_lTotalRead);
		GetField(e_Message)->SetTitle(csTemp);
	}
	else
	{
		GetField(e_Message)->SetTitle("");

		fnUncompress( );

		if( IsFieldNew(e_ParseFile) )
		{
			CString csInputFile = fld_InputFilePath->GetValueString();
			string sOutputPath = GetField( e_OutputFolderPath )->GetValueString();

			csInputFile.Replace(".gz","");
			string sInputFile = csInputFile.GetString();

			int iFile = PathFileExists(sInputFile.c_str());

			if( iFile != 1 )//file path is not valid
			{
				fld_Status->SetValueString("Input file path is not exist ..!!");
				return;
			}

			iFile = PathFileExists(sOutputPath.c_str());
			if( iFile != 1 )
			{
				CreateDirectory(sOutputPath.c_str(), NULL);
			}

			iFile = PathFileExists(sOutputPath.c_str());
			if( iFile != 1 )
			{
				GetField( e_Status )->SetValueString("Output folder is not exist ..!!");
				return;
			}

			sOutputPath = sOutputPath + "\\" + fld_Type->GetShortString();

			iFile = PathFileExists(sOutputPath.c_str());
			if( iFile != 1 )
			{
				CreateDirectory(sOutputPath.c_str(), NULL);
			}

			iFile = PathFileExists(sOutputPath.c_str());
			if( iFile != 1 )
			{
				GetField( e_Status )->SetValueString("Output folder is not exist ..!!");
				return;
			}			

			fld_HidOutputDir->SetValueString( sOutputPath.c_str() );

			COleDateTime dtCurrent = COleDateTime::GetCurrentTime();
			CString strCurrentTime;
			strCurrentTime.Format("%04d/%02d%02d %02d:%02d:%02d", dtCurrent.GetYear(), dtCurrent.GetMonth(), dtCurrent.GetDay(), 
				dtCurrent.GetHour(), dtCurrent.GetMinute(), dtCurrent.GetSecond());

			GetField(e_StartTime)->SetValueString(strCurrentTime);
			GetField(e_EndTime)->SetValueString("");

			GetField( e_Status )->SetValueString("File Processing Started ..!!");
			AfxBeginThread(EODProcessor, this);
			//autoParseFile( );
		}
	}
}

void  EodDataProcessingFinapp::autoParseFile()
{
	CString csInputFile = fld_InputFilePath->GetValueString();		
	csInputFile.Replace(".gz","");
	string fileName = csInputFile.GetString();

	m_clExchangePathList.clear();
	m_clExchangeYearPathList.clear();
	m_clExchangeYearMonthPathList.clear();
	m_clExchangeYearMonthDatePathList.clear();

	string sExchange, sProduct, sProductType, sTradeDate, key, newFilePath;

	int iExchangeIndex(0), iProductIndex(0), iProductTypeIndex(0), iTradeDateIndex(0);

	CString line;
	CStdioFile readInputFile;

	map<string, FILE * > mapProductFiles;
	map<string, FILE * >::iterator itr_mapProductFiles;
	int iOpenFileCount = 0;

	vector<string> recordList;
	FILE *fp = NULL;
	
	bool bstarte = false;
	CString csYearPars;
	csYearPars.Format("%d", fld_ParseYear->GetValueInt());

	int iCount(0);

	if( readInputFile.Open(fileName.c_str(), CFile::modeRead) )
	{
		int header = 0;
		while (readInputFile.ReadString(line))
		{
			header++;		
			splitString(line, ",", recordList);
			if( header == 1 )
			{
				iTradeDateIndex = indexofColumn(recordList, "TRADE_DATE");
				iExchangeIndex	= indexofColumn(recordList, "EXCH_MIC_CODE");
				iProductIndex	= indexofColumn(recordList, "TICKER_SYMBOL");
				iProductTypeIndex = indexofColumn(recordList, "FO_IND");
				continue;
			}
			
			sTradeDate = recordList.at(iTradeDateIndex);
			iCount++;

			if( iCount >= 1000 )
			{
				m_lTotalRead += iCount;
				iCount = 0;
				this->RequestExternalUpdate();
			}

			if( fld_ParseYear->GetValueInt() ^ -1 )
			{
				if( sTradeDate.find(csYearPars) == -1 && !bstarte )
				{
					continue;
				}

				if( sTradeDate.find(csYearPars) == -1 && bstarte )
				{
					break;
				}

				bstarte = true;
			}

			sProduct	 = recordList.at(iProductIndex);
			sProductType = recordList.at(iProductTypeIndex);

			if( sProductType == "F" )
				sProductType = "FUT";

			if( sProductType == "O" )
				sProductType = "OPT";

			sExchange = recordList.at(iExchangeIndex);
			key = sExchange + "_" + sProduct + "_" + sProductType + "_" + sTradeDate;

			CString csOutputPath = fnCreatePath( sTradeDate.c_str(), sExchange.c_str() );

			string strNewMsg;
			GetRowMessage(recordList, strNewMsg);
					
			itr_mapProductFiles = mapProductFiles.find(key);
			if (itr_mapProductFiles != mapProductFiles.end())
			{				
				fp = itr_mapProductFiles->second;

				if( fp == NULL )
				{
					if( iOpenFileCount >= 500 )
					{
						itr_mapProductFiles = mapProductFiles.begin();
						while( itr_mapProductFiles != mapProductFiles.end() )
						{
							if( itr_mapProductFiles->second != NULL )
							{
								fclose(itr_mapProductFiles->second);
								itr_mapProductFiles->second = NULL;
							}
							itr_mapProductFiles++;
						}

						iOpenFileCount = 0;
					}

					newFilePath = createProductFile(csOutputPath.GetString(), key);
					
					//reopen file
					fp = fopen(newFilePath.c_str(), "a+");
					itr_mapProductFiles->second = fp;
					iOpenFileCount++;
				}

				fputs(strNewMsg.c_str(), fp);
			}
			else
			{
				if( iOpenFileCount >= 500 )
				{
					itr_mapProductFiles = mapProductFiles.begin();
					while( itr_mapProductFiles != mapProductFiles.end() )
					{
						if( itr_mapProductFiles->second != NULL )
						{
							fclose(itr_mapProductFiles->second);
							itr_mapProductFiles->second = NULL;
						}
						itr_mapProductFiles++;
					}

					iOpenFileCount = 0;
				}

				newFilePath = createProductFile(csOutputPath.GetString(), key);
				
				FILE * writeFile = fopen(newFilePath.c_str(), "w");
				mapProductFiles.insert(pair<string, FILE *>(key, writeFile));
				iOpenFileCount++;

				fputs(__strHeader.c_str(), writeFile); // Write Header...
				fputs(strNewMsg.c_str(), writeFile);				
			}
		}

		//Close all open product files
		int iTotalFileCount = mapProductFiles.size();
		itr_mapProductFiles = mapProductFiles.begin();

		while (itr_mapProductFiles != mapProductFiles.end())
		{
			fp = itr_mapProductFiles->second;

			if( fp != NULL )
				fclose(fp);

			itr_mapProductFiles++;
		}

		readInputFile.Close();

		m_lTotalRead += iCount;
		iCount = 0;

		CString csTemp;
		csTemp.Format("%d product file created, Total Rows %d ",iTotalFileCount, m_lTotalRead);
		fld_Status->SetValueString(csTemp);
	}
	else
	{
		fld_Status->SetValueString("Error while Opening File");
	}	
	
	CString strCurrentTime;

	COleDateTime dtCurrent = COleDateTime::GetCurrentTime();
	strCurrentTime.Format("%04d/%02d%02d %02d:%02d:%02d", dtCurrent.GetYear(), dtCurrent.GetMonth(), dtCurrent.GetDay(), 
	dtCurrent.GetHour(), dtCurrent.GetMinute(), dtCurrent.GetSecond());

	GetField(e_EndTime)->SetValueString(strCurrentTime);
	this->RequestExternalUpdate();
}

void EodDataProcessingFinapp::GetRowMessage(vector<string> recordList , string &msg )
{
	msg = recordList[enm_TRADE_DATE];
	for( int iI = 1; iI < recordList.size(); iI++ )
	{
		if( iI == enm_LAST_TRADE_DATE || iI == enm_PCLS_IND || recordList[iI] == "\n" )
			continue;
		msg += "," + recordList[iI];
	}

	msg += "," + recordList[enm_LAST_TRADE_DATE] + "\n";		
}

CString EodDataProcessingFinapp::fnCreatePath( CString csDate, CString csExchange )
{
	CString csYear = csDate.Mid(0,4);
	CString csMonth = csDate.Mid(4,2);
	CString csDay = csDate.Mid(6,2);

	CString csExchangeDate = csExchange+"_"+csDate;

	if( m_clExchangeYearMonthDatePathList.find(csExchangeDate) != m_clExchangeYearMonthDatePathList.end() )
	{
		CString csOutputPath = fld_HidOutputDir->GetValueString();
		csOutputPath = csOutputPath + "\\" + csExchange + "\\" + csYear + "\\" + csMonth + "\\" + csDate;
		return csOutputPath;
	}		

	CString csYearMonth = csDate.Mid(0,6);
	CString csExchangeYearMonth = csExchange+"_"+csYearMonth;
	if( m_clExchangeYearMonthPathList.find(csExchangeYearMonth) != m_clExchangeYearMonthPathList.end() )
	{
		CString csOutputPath = fld_HidOutputDir->GetValueString();
		csOutputPath = csOutputPath +  "\\" + csExchange + "\\" + csYear + "\\" + csMonth + "\\" + csDate;
		CreateDirectory(csOutputPath, NULL);

		m_clExchangeYearMonthDatePathList.insert(std::pair<CString,bool>(csExchangeDate, true));
		return csOutputPath;
	}

	CString csExchangeYear = csExchange+"_"+csYear;
	if( m_clExchangeYearPathList.find(csExchangeYear) != m_clExchangeYearPathList.end() )
	{
		CString csOutputPath = fld_HidOutputDir->GetValueString();
		csOutputPath = csOutputPath + "\\" + csExchange + "\\" + csYear + "\\" + csMonth;
		CreateDirectory(csOutputPath, NULL);

		m_clExchangeYearMonthPathList.insert(std::pair<CString,bool>(csExchangeYearMonth, true));

		csOutputPath = fld_HidOutputDir->GetValueString();
		csOutputPath = csOutputPath + "\\" + csExchange + "\\" + csYear + "\\" + csMonth + "\\" + csDate;
		CreateDirectory(csOutputPath, NULL);

		m_clExchangeYearMonthDatePathList.insert(std::pair<CString,bool>(csExchangeDate, true));
		return csOutputPath;
	}

	if( m_clExchangePathList.find(csExchange) != m_clExchangePathList.end() )
	{
		CString csOutputPath = fld_HidOutputDir->GetValueString();
		csOutputPath = csOutputPath + "\\" + csExchange + "\\" + csYear;
		CreateDirectory(csOutputPath, NULL);

		m_clExchangeYearPathList.insert(std::pair<CString,bool>(csExchangeYear, true));

		csOutputPath = fld_HidOutputDir->GetValueString();
		csOutputPath = csOutputPath + "\\" + csExchange + "\\" + csYear + "\\" + csMonth;
		CreateDirectory(csOutputPath, NULL);

		m_clExchangeYearMonthPathList.insert(std::pair<CString,bool>(csExchangeYearMonth, true));

		csOutputPath = fld_HidOutputDir->GetValueString();
		csOutputPath = csOutputPath + "\\" + csExchange + "\\" + csYear + "\\" + csMonth + "\\" + csDate;
		CreateDirectory(csOutputPath, NULL);

		m_clExchangeYearMonthDatePathList.insert(std::pair<CString,bool>(csExchangeDate, true));
		return csOutputPath;
	}

	CString csOutputPath = fld_HidOutputDir->GetValueString();
	csOutputPath = csOutputPath + "\\" + csExchange;
	CreateDirectory(csOutputPath, NULL);

	m_clExchangePathList.insert(std::pair<CString,bool>(csExchange, true));

	csOutputPath = fld_HidOutputDir->GetValueString();
	csOutputPath = csOutputPath + "\\" + csExchange + "\\" + csYear;
	CreateDirectory(csOutputPath, NULL);

	m_clExchangeYearPathList.insert(std::pair<CString,bool>(csExchangeYear, true));

	csOutputPath = fld_HidOutputDir->GetValueString();
	csOutputPath = csOutputPath + "\\" + csExchange + "\\" + csYear + "\\" + csMonth;
	CreateDirectory(csOutputPath, NULL);

	m_clExchangeYearMonthPathList.insert(std::pair<CString,bool>(csExchangeYearMonth, true));

	csOutputPath = fld_HidOutputDir->GetValueString();
	csOutputPath = csOutputPath + "\\" + csExchange + "\\" + csYear + "\\" + csMonth + "\\" + csDate;
	CreateDirectory(csOutputPath, NULL);

	m_clExchangeYearMonthDatePathList.insert(std::pair<CString,bool>(csExchangeDate, true));

	return csOutputPath;
}

void EodDataProcessingFinapp::splitString(CString cStr, string delimiter, vector<string> &result)
{
	result.clear();

	size_t pos = 0;
	string fieldValue, str;

	str = cStr;
	while( (pos = str.find(delimiter)) != string::npos )
	{
		fieldValue = str.substr(0, pos);
		if( fieldValue == "null" )
			fieldValue = "";

		result.push_back(fieldValue);
		str.erase(0, pos + delimiter.length());
	}
	result.push_back(str);

	while( result.size() > m_niInputColumnCount )
	{
		result[enm_PRODUCT_DESCRIPTION] = result[enm_PRODUCT_DESCRIPTION] + "," + result[enm_FO_IND];
		result.erase(result.begin() + enm_FO_IND);
	}
}

string EodDataProcessingFinapp::createProductFile(string outputPath, string strkey )
{
	//Creating Product File
	string stnewFilePath = outputPath + "\\" + strkey + ".csv";
	return stnewFilePath;
}

size_t  EodDataProcessingFinapp::indexofColumn(vector<string> vec, string searchKey)
{
	vector<string>::iterator itr = std::find(vec.begin(), vec.end(), searchKey);
	if (itr != vec.end())
	{
		size_t index = distance(vec.begin(), itr);
		return index;
	}
}

//
//CString EodDataProcessingFinapp::unzip(CString csFilepath,CString csFileName)
//{		
//	try
//	{			
//		CString csSource = csFilepath + csFileName;
//			
//		BSTR source = csSource.AllocSysString();
//
//		//"EOD_20150716_E.zip"
//		csFilepath = csFilepath + csFileName.SpanExcluding(".") ;
//		BSTR dest	= csFilepath.AllocSysString();
//
//		if(CreateDirectory (csFilepath, NULL))			
//		{
//			printf("\nSuccessfully created.");
//		}
//		
//
//		HRESULT        hResult;
//		IShellDispatch *pISD;
//		Folder         *pToFolder = NULL;
//		VARIANT        vDir, vFile, vOpt;
//
//		CoInitialize(NULL);
//
//		hResult = CoCreateInstance(CLSID_Shell, NULL, CLSCTX_INPROC_SERVER, IID_IShellDispatch, (void **)&pISD);
//
//		if( SUCCEEDED(hResult) )
//		{
//			VariantInit(&vDir);
//			vDir.vt = VT_BSTR;
//			vDir.bstrVal = dest;
//			hResult = pISD->NameSpace(vDir, &pToFolder);
//
//			if( hResult != S_FALSE && pToFolder != NULL )//if  (SUCCEEDED(hResult))
//			{
//				Folder *pFromFolder = NULL;
//
//				VariantInit(&vFile);
//				vFile.vt = VT_BSTR;
//				vFile.bstrVal = source;//L"C:\\test.txt";				
//				HRESULT hFileFound = pISD->NameSpace(vFile, &pFromFolder);
//				
//				if(hFileFound == S_FALSE || pFromFolder == NULL )
//				{
//					pToFolder->Release();
//					CString str = "File : " + csSource + " Not Found , unZip unsuccessfull";
//					throw str;
//				}
//
//				FolderItems *fi = NULL;
//				pFromFolder->Items(&fi);
//				VariantInit(&vOpt);
//				vOpt.vt = VT_I4;
//				vOpt.lVal = FOF_NO_UI;//4; // Do not display a progress dialog box
//
//				// Creating a new Variant with pointer to FolderItems to be copied
//				VARIANT newV;
//				VariantInit(&newV);
//				newV.vt = VT_DISPATCH;
//				newV.pdispVal = fi;
//				hResult = pToFolder->CopyHere(newV, vOpt);
//								
//				pFromFolder->Release();
//				pToFolder->Release();
//			}
//			else
//			{
//				CString str = "File : " + csFilepath + " Not Found , unZip unsuccessfull";
//				throw str;
//			}
//
//			pISD->Release();
//		}
//		CoUninitialize();
//
//		return "";
//	}
//	catch(CString strError)
//	{	
//		SetErrorMessage(ErrorSeverity::e_Error,strError);	
//		return strError;
//	}
//	return "";
//}
//
//void EodDataProcessingFinapp::GenerateFileName(CString &strInputFile, CString &strOutputFile,CString &strUnzipFile)
//{
//   CString strDefaultPath = GetField(e_DefaultPath)->GetValueString();
//   BFDate bfDefaultDate;
//
//   bfDefaultDate = GetField(e_DefaultDate)->GetValueDate(); 
//   int iYr = 0;
//   int iMonth = 0;
//   int iDay = 0;
//
//   iYr = bfDefaultDate.GetYear();
//   iMonth = bfDefaultDate.GetMonth() + 1 ;
//   iDay = bfDefaultDate.GetDate();
//
//   CString strSettelment = SettelmentList->GetShortString();  
//   CString strFileTypeList = FileTypeList->GetShortString();
//   CString strFileExtensionList = FileExtensionList->GetShortString();
//
//   strSettelment = strSettelment.Trim();
//   strFileTypeList = strFileTypeList.Trim(); 
//   strFileExtensionList = strFileExtensionList.Trim(); 
//
//   CString strTemp = "";
//   strTemp = strDefaultPath.Right(1);
//   strTemp = strTemp.Trim(); 
//      
//	if( strTemp.CompareNoCase("\\") != 0)
//	{
//		strDefaultPath = strDefaultPath + "\\";
//		strDefaultPath = strDefaultPath.Trim(); 
//	}
//
//	CString strMainFolderName = "";
//	strMainFolderName.Format("%s_%d%02d%02d_%s",strFileTypeList,iYr,iMonth,iDay,strSettelment.Left(1)); 
//	m_strMainFolder = strMainFolderName;
//
//   strInputFile.Format("%s%s_%d%02d%02d_%s.%s",strDefaultPath,strFileTypeList,iYr,iMonth,iDay,strSettelment.Left(1),strFileExtensionList.MakeLower());    
//   strInputFile = strInputFile.Trim(); 
//  
//   if( strFileExtensionList.CompareNoCase("zip") == 0 )
//   {
//	   strOutputFile.Format("%s%s_%d%02d%02d_%s\\",strDefaultPath,strFileTypeList,iYr,iMonth,iDay,strSettelment.Left(1)); 
//	   strUnzipFile.Format("%s_%d%02d%02d_%s.%s",strFileTypeList,iYr,iMonth,iDay,strSettelment.Left(1),strFileExtensionList.MakeLower());  
//
//	   strOutputFile = strOutputFile.Trim(); 
//	   strUnzipFile = strUnzipFile.Trim(); 
//   }
//   else
//   {
//	   strOutputFile.Format("%s",strDefaultPath); 
//	   strOutputFile = strOutputFile.Trim(); 
//	   strUnzipFile.Format("");  
//   }
//
//   //EOD_20150716_E.zip");	  
//
//}
//
//
//void EodDataProcessingFinapp::ParseFile(CString strInputFile)
//{	
//	    BFDate bfDefaultDate;
//		bfDefaultDate = GetField(e_DefaultDate)->GetValueDate(); 
//		int iYr = 0;
//		int iMonth = 0;
//		int iDay = 0;
//
//		iYr = bfDefaultDate.GetYear();
//		iMonth = bfDefaultDate.GetMonth() + 1 ;
//		iDay = bfDefaultDate.GetDate();
//
//		CString strDate="";
//		strDate.Format("%d%02d%02d",iYr,iMonth,iDay);
//
//		CString strFileExtensionList = FileExtensionList->GetShortString();
//
//		if( strFileExtensionList.CompareNoCase("zip") == 0 )
//		{
//			strInputFile = strInputFile.SpanExcluding("."); 
//			CString strCSVfileName = GetFileName(strInputFile);
//			strCSVfileName = strCSVfileName.Trim(); 
//
//			ReadFile( strCSVfileName,strDate );				
//		}
//		else
//		{
//			strInputFile = GetField(e_DirectoryInputPath)->GetValueString();  
//			strInputFile = strInputFile.Trim(); 
//			ReadFile( strInputFile,strDate );	
//		}		
//
//}
//
//CString EodDataProcessingFinapp::GetFileName(CString strInputFile)
//{  	 
//		CString strFileName="";
//	    try
//		{
//			CString strFPath = "";
//			strFPath = strInputFile;
//			strFPath = strFPath.Trim();
//			CString sFullMask = strFPath + CString("\\*.csv");		
//			sFullMask = sFullMask.Trim(); 
//
//			CFileFind finder;
//			bool bFound;
//			bFound=finder.FindFile(sFullMask);
//
//			while ((bFound))
//			{
//				bFound = finder.FindNextFile();
//				if (!finder.IsDots())
//				{
//			
//					CString sName = finder.GetFilePath();
//					if (finder.IsDirectory()) 
//					{
//						sName += "\\";
//					}
//					strFileName =  sName;
//					// Call callback procedure		
//				}
//			}
//
//		}
//		catch(...)
//		{
//			PrintRawMessage("Exception File Read.");
//		}
//
//		return strFileName;
//
//}
//
//
//void EodDataProcessingFinapp::ReadFile(CString strFilePath,CString strDate)
//{
//	  GetExchanegProduct();
//
//	  bool bALLFlage = false;
//	  for(int i=0; i<m_ExchangeProductFilterList.size();i++)
//	  {
//		  if( m_ExchangeProductFilterList.at(i).strExchange.CompareNoCase("ALL") == 0 && m_ExchangeProductFilterList.at(i).strProduct.CompareNoCase("ALL") == 0 )
//		  {
//			  bALLFlage = true;
//		  }
//	  }
//	  
//	  CString strheader = "";
//	    try
//		{	
//
//			////read file 				
//			CString sFileName = strFilePath;	
//			FILE *file(NULL);			
//
//			if(!fopen_s(&file,sFileName,"r"))
//			{
//				char line[3072] = {0};
//				CString sLine, sWord;				
//				int iRow = 0;
//
//				while(fgets(line,3072,file))
//				{
//					sLine = line;					
//
//					if( iRow == 0)
//					{
//						ExtractExchangeProductCol(sLine);						
//						strheader = sLine;
//					}
//					else
//					{		
//							if(bALLFlage)
//							{
//									ExtractTokens(sLine,1);					
//							}
//							else
//							{
//								    ExtractTokens(sLine);	
//
//							}
//
//					}
//					iRow++;
//				}
//			}
//			else
//			{
//				//m_sError = "Error in opening CSV file";
//				return ;
//			}
//		
//			if(file != NULL) 
//				fclose(file);
//
//			int isize = m_AllDetailVec.size(); 
//
//			int iInFile = PathFileExists(sFileName);
//			if( (iInFile == 1) )
//			{
//				remove(sFileName);
//			}
//
//			m_ExchangeList.clear();			
//				 
//
//			if( bALLFlage )
//			{
//				for(int i=0;i<m_AllDetailVec.size(); i++ )
//				{
//					m_ExchangeList.push_back(m_AllDetailVec.at(i).strExchange); 				
//				}
//				
//				m_ExchangeList.erase( std::unique(m_ExchangeList.begin(), m_ExchangeList.end()),  m_ExchangeList.end());
//				std::sort(m_ExchangeList.begin(), m_ExchangeList.end());
//			}
//			else
//			{
//				for(int i=0; i <m_ExchangeProductFilterList.size(); i++ )
//				{
//					m_ExchangeList.push_back(m_ExchangeProductFilterList.at(i).strExchange.Trim()); 
//				}
//			}
//						
//
//			CString strStatus = SettelmentList->GetShortString();  					
//			
//			CString stTemp="";
//			stTemp.Format("%s\\%s",m_strDirOutputPath,strStatus); 
//			if( CreateDirectory (stTemp, NULL) )
//			{
//				printf("\nSuccessfully created.");
//			}
//
//			for(int i=0; i<m_ExchangeList.size(); i++ )
//			{				
//					CString stTemp="";
//					stTemp.Format("%s\\%s\\%s",m_strDirOutputPath,strStatus,m_ExchangeList.at(i)); 
//					if( CreateDirectory (stTemp, NULL) )
//					{
//						printf("\nSuccessfully created.");
//					}
//					else
//					{ 
//						printf("\n not created.");
//					}			
//			}
//
//			 //create zip
//			CString strzPath ="";
//			strzPath.Format("%s%s_Generated.zip",m_strDirOutputPath,m_strMainFolder);
//			HZIP hz = CreateZip(strzPath,0);	         
//		
//
//			for(int i=0; i<m_AllDetailVec.size(); i++ )
//			{
//					//strheader
//					CString stTemp="";
//					stTemp.Format("%s\\%s\\%s\\%s",m_strDirOutputPath,strStatus,m_AllDetailVec.at(i).strExchange,m_AllDetailVec.at(i).strProduct ); 
//					if( CreateDirectory (stTemp, NULL) )
//					{
//						printf("\nSuccessfully created.");
//					}
//					else
//					{
//
//						printf("\n not created.");
//					}	
//
//					//create  date folder
//					stTemp.Format("%s\\%s\\%s\\%s\\%s",m_strDirOutputPath,strStatus,m_AllDetailVec.at(i).strExchange,m_AllDetailVec.at(i).strProduct,strDate ); 
//					if( CreateDirectory (stTemp, NULL) )
//					{
//						printf("\nSuccessfully created.");
//					}
//
//
//					///create multiple file 
//					if( m_AllDetailVec.at(i).iLineCount > SPLIT )
//					{
//
//						int iFileCount =0;
//						float fOffset = 0,fTemp =0;
//						iFileCount = m_AllDetailVec.at(i).iLineCount / SPLIT ;
//						fOffset = (float)m_AllDetailVec.at(i).iLineCount / SPLIT ;
//						fTemp = (fOffset -  iFileCount);
//
//						if(fTemp > 0)
//						{
//							iFileCount = iFileCount + 1;
//						}
//
//						CStringArray CsvFiles;
//
//						for(int l = 1; l<= iFileCount; l++)
//						{
//							CString strTemp = "";
//							strTemp.Format("%s%s\\%s\\%s\\%s\\%s_%s_%2d.csv",m_strDirOutputPath,strStatus,m_AllDetailVec.at(i).strExchange,m_AllDetailVec.at(i).strProduct,strDate,m_AllDetailVec.at(i).strProduct,m_AllDetailVec.at(i).strExchange,l ); 
//							CsvFiles.Add(strTemp);
//						}
//					
//						CString strFileData = "",strFileRemainData = "";						
//						int k = SPLIT;
//						int iCount = 0;
//						for(int t=0; t<iFileCount;t++ )
//						{			
//							    if( m_AllDetailVec.at(i).iLineCount > SPLIT)
//								{
//									strFileData = m_AllDetailVec.at(i).strData.SpanExcluding("\001");									
//									strFileRemainData = m_AllDetailVec.at(i).strData.Mid(strFileData.GetLength() + 1, m_AllDetailVec.at(i).strData.GetLength() ); 
//									m_AllDetailVec.at(i).strData = strFileRemainData;
//									m_AllDetailVec.at(i).iLineCount = m_AllDetailVec.at(i).iLineCount - SPLIT;
//								}
//								else
//								{
//									strFileData = m_AllDetailVec.at(i).strData;									
//								}
//								 
//								ofstream Dailyfile( CsvFiles.GetAt(iCount) );
//								Dailyfile<< strheader;
//								Dailyfile<< strFileData << endl;
//								Dailyfile.close();									
//								strFileData = "";
//
//								CString strZipDestPath = "";
//								strZipDestPath.Format("%s\\%s\\%s\\%s\\%s_%s_%2d.csv",strStatus,m_AllDetailVec.at(i).strExchange,m_AllDetailVec.at(i).strProduct,strDate,m_AllDetailVec.at(i).strExchange,m_AllDetailVec.at(i).strProduct,iCount+1); 
//								ZipAdd(hz,strZipDestPath,CsvFiles.GetAt(iCount));	
//								iCount++;
//						}
//
//					}
//					else
//					{		
//						CString CsvFilename;
//						CsvFilename.Format("%s\\%s\\%s\\%s\\%s\\%s_%s.csv",m_strDirOutputPath,strStatus,m_AllDetailVec.at(i).strExchange,m_AllDetailVec.at(i).strProduct,strDate,m_AllDetailVec.at(i).strProduct,m_AllDetailVec.at(i).strExchange ); 
//						
//						ofstream Dailyfile(CsvFilename);
//						Dailyfile<< strheader;
//						Dailyfile<< m_AllDetailVec.at(i).strData << endl;
//						Dailyfile.close();
//
//						CString strZipDestPath = "";
//						strZipDestPath.Format("%s\\%s\\%s\\%s\\%s_%s.csv",strStatus,m_AllDetailVec.at(i).strExchange,m_AllDetailVec.at(i).strProduct,strDate,m_AllDetailVec.at(i).strProduct,m_AllDetailVec.at(i).strExchange ); 
//						ZipAdd(hz,strZipDestPath,CsvFilename);	
//					}
//			}
//
//
//			CloseZip(hz);
//			m_ExchangeList.clear(); 						
//
//		}
//		catch(...)
//		{
//			PrintRawMessage("Exception File Read.");
//		}
//
//
//}
//
//void EodDataProcessingFinapp::ExtractExchangeProductCol (CString s)
//{	
//
//   CString Delimiters = "";
//   Delimiters = ",";
//
//   m_ExchangeColID = 0;
//   m_ProductColID = 0;
//
//   int iCounter = 0;
//
//   int     Hit;
//   int     EarliestHit;
//   int     DelimiterIndex;
//   CString sExtract;
//   BOOL    MoreTokens = TRUE;
//   BOOL    GotToken = FALSE;
//   TCHAR   CurrDelimiter;   
//
//   while (MoreTokens)
//   {
//      GotToken = FALSE;
//      EarliestHit = s.GetLength();
//
//      // Trawl the string looking for the leftmost (earliest) hit in
//      // the list of valid separators.
//      for (DelimiterIndex = 0; 
//           DelimiterIndex < Delimiters.GetLength();
//           DelimiterIndex++)
//      {
//         CurrDelimiter = Delimiters [DelimiterIndex];
//         Hit = s.Find (CurrDelimiter);
//         if (Hit != -1)
//         {
//            if (Hit < EarliestHit)
//            {
//               EarliestHit = Hit;
//            }
//            GotToken = TRUE;
//         }
//      }
//
//      if (GotToken)
//      {
//         sExtract = s.Left(EarliestHit);
//         s = s.Right(s.GetLength() - EarliestHit - 1);
//		 
//		 if( sExtract.CompareNoCase("Exchange Code") == 0 )  
//		 {
//			m_ExchangeColID = iCounter;
//		 }
//
//		 if( sExtract.CompareNoCase("Product Code") == 0 ) 
//		 {
//			m_ProductColID = iCounter;
//		 }		 
//		 iCounter++;
//        //  OutputArray.Add(sExtract);
//      }
//      else
//      {
//         // Trailing token.
//         MoreTokens = FALSE;
//         //OutputArray.Add(s);
//      }
//
//
//	  if( m_ExchangeColID != 0 && m_ProductColID !=0)
//	  {
//		  break;
//	  }
//   }
//}
//
//void EodDataProcessingFinapp::ExtractTokens (CString s,int iALL)
//{	
//
//   CString Delimiters="";
//   Delimiters = ",";
//
//   stFileData objstFileData;
//   CString strLine = "";
//   strLine = s;
//
//   int iCounter = 0;
//   
//   int     Hit;
//   int     EarliestHit;
//   int     DelimiterIndex;
//   CString sExtract;
//   BOOL    MoreTokens = TRUE;
//   BOOL    GotToken = FALSE;
//   TCHAR   CurrDelimiter;      
//
//   while (MoreTokens)
//   {
//      GotToken = FALSE;
//      EarliestHit = s.GetLength();
//
//      // Trawl the string looking for the leftmost (earliest) hit in
//      // the list of valid separators.
//      for (DelimiterIndex = 0; 
//           DelimiterIndex < Delimiters.GetLength();
//           DelimiterIndex++)
//      {
//         CurrDelimiter = Delimiters [DelimiterIndex];
//         Hit = s.Find (CurrDelimiter);
//         if (Hit != -1)
//         {
//            if (Hit < EarliestHit)
//            {
//               EarliestHit = Hit;
//            }
//            GotToken = TRUE;
//         }
//      }
//
//      if (GotToken)
//      {
//         sExtract = s.Left(EarliestHit);
//         s = s.Right(s.GetLength() - EarliestHit - 1);
//		 
//		 if( m_ExchangeColID == iCounter )  // insert in exchange vector
//		 {
//			 objstFileData.strExchange = sExtract; 
//			// m_ExchangeList.push_back(sExtract);   			 
//		 }
//
//		 if( m_ProductColID == iCounter )  // insert in Product vector
//		 {
//			 objstFileData.strProduct = sExtract;			
//		 }		
//
//		 iCounter++;
//       //  OutputArray.Add(sExtract);
//      }
//      else
//      {
//         // Trailing token.
//         MoreTokens = FALSE;
//         //OutputArray.Add(s);
//      }
//
//	  if( objstFileData.strExchange.CompareNoCase("") !=0 && objstFileData.strProduct.CompareNoCase("") !=0 )
//	  {
//		  break;
//	  }
//   }
//   
//    CString strExchange = "";
//	CString strProduct = "";
//	
//	objstFileData.strExchange.Trim();
//	objstFileData.strProduct.Trim();
//	objstFileData.strData.Trim();
//
//	bool bFound = false;
//    if( iALL != 1 )
//	{
//		    for(int i=0; i<m_ExchangeProductFilterList.size();i++)
//			{
//				strExchange = m_ExchangeProductFilterList.at(i).strExchange;
//				strProduct = m_ExchangeProductFilterList.at(i).strProduct;	
//
//				if( strExchange.CompareNoCase(objstFileData.strExchange) == 0 || strProduct.CompareNoCase(objstFileData.strProduct) ==0  )
//				{
//					bFound = true;
//				}
//			}
//    }
//
//	if( iALL != 1 && !bFound )
//	{
//		return;
//	}
//	
//
//
//	if( iALL == 1 )
//	{		
//			if(m_AllDetailVec.size() == 0 )
//			{
//				objstFileData.strData = strLine;  
//				m_AllDetailVec.push_back(objstFileData);
//			}
//			else
//			{			
//
//				for(int i = 0; i<m_AllDetailVec.size();i++ )
//				{
//					if( m_AllDetailVec.at(i).strExchange.CompareNoCase(objstFileData.strExchange) == 0 && m_AllDetailVec.at(i).strProduct.CompareNoCase(objstFileData.strProduct) == 0   )
//					{
//						m_AllDetailVec.at(i).strData = m_AllDetailVec.at(i).strData + strLine;
//						m_AllDetailVec.at(i).iLineCount = m_AllDetailVec.at(i).iLineCount + 1;						
//
//						///add \001 after getting each file line count 50000 and more
//						if( m_AllDetailVec.at(i).iLineCount >= SPLIT )
//						{							
//							for(int k = SPLIT; k<= m_AllDetailVec.at(i).iLineCount; k = k + SPLIT)
//							{
//								if( m_AllDetailVec.at(i).iLineCount == k )
//								{
//									m_AllDetailVec.at(i).strData = m_AllDetailVec.at(i).strData + "\001";									
//								}								
//							}
//						}
//						///////////////
//
//					}			
//				}
//
//
//				int iFlage = 0;
//				for(int i = 0; i<m_AllDetailVec.size();i++ )
//				{
//					if( m_AllDetailVec.at(i).strExchange.CompareNoCase(objstFileData.strExchange) == 0 && m_AllDetailVec.at(i).strProduct.CompareNoCase(objstFileData.strProduct) == 0   )
//					{
//						iFlage =1;				
//					}
//				}
//
//				if( iFlage == 0 )
//				{
//					objstFileData.strData = strLine;
//					m_AllDetailVec.push_back(objstFileData); 
//				}	
//
//			}
//	}
//	
//
//	for(int i=0; i<m_ExchangeProductFilterList.size();i++)
//	{
//			strExchange = m_ExchangeProductFilterList.at(i).strExchange;
//			strProduct = m_ExchangeProductFilterList.at(i).strProduct;	
//					
//	
//						if( strExchange.CompareNoCase("ALL") != 0 && strProduct.CompareNoCase("ALL") == 0)
//						{
//
//								if(m_AllDetailVec.size() == 0 )
//								{
//									objstFileData.strData = strLine;  				
//
//									if(objstFileData.strExchange.CompareNoCase(strExchange) == 0)
//									{
//										m_AllDetailVec.push_back(objstFileData);
//									}
//								}
//								else
//								{	
//									for(int i = 0; i<m_AllDetailVec.size();i++ )
//									{
//										if( m_AllDetailVec.at(i).strExchange.CompareNoCase(objstFileData.strExchange) == 0 && m_AllDetailVec.at(i).strProduct.CompareNoCase(objstFileData.strProduct) == 0   )
//										{
//											m_AllDetailVec.at(i).strData = m_AllDetailVec.at(i).strData + strLine;
//											m_AllDetailVec.at(i).iLineCount = m_AllDetailVec.at(i).iLineCount + 1;
//
//											///add \001 after getting each file line count 50000 and more
//											if( m_AllDetailVec.at(i).iLineCount >= SPLIT )
//											{
//												for(int k = SPLIT; k<= m_AllDetailVec.at(i).iLineCount; k = k + SPLIT)
//												{
//													if( m_AllDetailVec.at(i).iLineCount == k)
//													{
//														m_AllDetailVec.at(i).strData = m_AllDetailVec.at(i).strData + "\001";
//													}
//												}
//											}
//											///////////////
//
//										}			
//									}
//
//
//									int iFlage = 0;
//									for(int i = 0; i<m_AllDetailVec.size();i++ )
//									{
//										if( m_AllDetailVec.at(i).strExchange.CompareNoCase(objstFileData.strExchange) == 0 && m_AllDetailVec.at(i).strProduct.CompareNoCase(objstFileData.strProduct) == 0   )
//										{
//											iFlage =1;				
//										}
//									}
//
//									if( iFlage == 0 )
//									{
//											objstFileData.strData = strLine;
//											if(objstFileData.strExchange.CompareNoCase(strExchange) == 0)
//											{
//												m_AllDetailVec.push_back(objstFileData);
//											}				
//									}	
//
//								}
//
//						}
//						else if( strExchange.CompareNoCase("ALL") == 0 && strProduct.CompareNoCase("ALL") != 0)
//						{
//
//								if(m_AllDetailVec.size() == 0 )
//								{
//									objstFileData.strData = strLine;  				
//
//									if(objstFileData.strProduct.CompareNoCase(strProduct) == 0)
//									{
//										m_AllDetailVec.push_back(objstFileData);
//									}
//								}
//								else
//								{	
//									for(int i = 0; i<m_AllDetailVec.size();i++ )
//									{
//										if( m_AllDetailVec.at(i).strExchange.CompareNoCase(objstFileData.strExchange) == 0 && m_AllDetailVec.at(i).strProduct.CompareNoCase(objstFileData.strProduct) == 0   )
//										{
//											m_AllDetailVec.at(i).strData = m_AllDetailVec.at(i).strData + strLine;
//											m_AllDetailVec.at(i).iLineCount = m_AllDetailVec.at(i).iLineCount + 1;
//
//											///add \001 after getting each file line count 50000 and more
//											if( m_AllDetailVec.at(i).iLineCount >= SPLIT )
//											{
//												for(int k = SPLIT; k<= m_AllDetailVec.at(i).iLineCount; k = k + SPLIT)
//												{
//													if( m_AllDetailVec.at(i).iLineCount == k)
//													{
//														m_AllDetailVec.at(i).strData = m_AllDetailVec.at(i).strData + "\001";
//													}
//												}
//											}
//											///////////////
//
//										}			
//									}
//
//
//									int iFlage = 0;
//									for(int i = 0; i<m_AllDetailVec.size();i++ )
//									{
//										if( m_AllDetailVec.at(i).strExchange.CompareNoCase(objstFileData.strExchange) == 0 && m_AllDetailVec.at(i).strProduct.CompareNoCase(objstFileData.strProduct) == 0   )
//										{
//											iFlage =1;				
//										}
//									}
//
//									if( iFlage == 0 )
//									{
//											objstFileData.strData = strLine;
//											if(objstFileData.strProduct.CompareNoCase(strProduct) == 0)
//											{
//												m_AllDetailVec.push_back(objstFileData);
//											}				
//									}	
//
//								}
//
//						}
//						else if( strExchange.CompareNoCase("ALL") != 0 && strProduct.CompareNoCase("ALL") != 0)
//						{
//
//								if(m_AllDetailVec.size() == 0 )
//								{
//									objstFileData.strData = strLine;  				
//
//									if(objstFileData.strExchange.CompareNoCase(strExchange) == 0 && objstFileData.strProduct.CompareNoCase(strProduct) == 0)
//									{
//										m_AllDetailVec.push_back(objstFileData);
//									}
//								}
//								else
//								{	
//									for(int i = 0; i<m_AllDetailVec.size();i++ )
//									{
//										if( m_AllDetailVec.at(i).strExchange.CompareNoCase(objstFileData.strExchange) == 0 && m_AllDetailVec.at(i).strProduct.CompareNoCase(objstFileData.strProduct) == 0   )
//										{
//											m_AllDetailVec.at(i).strData = m_AllDetailVec.at(i).strData + strLine;
//											m_AllDetailVec.at(i).iLineCount = m_AllDetailVec.at(i).iLineCount + 1;
//						
//											///add \001 after getting each file line count 50000 and more
//											if( m_AllDetailVec.at(i).iLineCount >= SPLIT )
//											{
//												for(int k = SPLIT; k<= m_AllDetailVec.at(i).iLineCount; k = k + SPLIT)
//												{
//													if( m_AllDetailVec.at(i).iLineCount == k)
//													{
//														m_AllDetailVec.at(i).strData = m_AllDetailVec.at(i).strData + "\001";
//													}
//												}
//											}
//											///////////////
//
//										}			
//									}
//
//
//									int iFlage = 0;
//									for(int i = 0; i<m_AllDetailVec.size();i++ )
//									{
//										if( m_AllDetailVec.at(i).strExchange.CompareNoCase(objstFileData.strExchange) == 0 && m_AllDetailVec.at(i).strProduct.CompareNoCase(objstFileData.strProduct) == 0   )
//										{
//											iFlage =1;				
//										}
//									}
//
//									if( iFlage == 0 )
//									{
//											objstFileData.strData = strLine;
//											if(objstFileData.strExchange.CompareNoCase(strExchange) == 0 &&  objstFileData.strProduct.CompareNoCase(strProduct) == 0 )
//											{
//												m_AllDetailVec.push_back(objstFileData);
//											}				
//									}	
//
//								}
//
//						}
//						////////////
//	}	
//
//}
//
//
