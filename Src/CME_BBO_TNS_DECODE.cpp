// Copyright (C) 2002 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#include "stdafx.h"
#include "CME_BBO_TNS_DECODE.hpp"
#include "BBOConstants.h"
#include <fields/ListField.hpp>
#include <fstream>
#include <time.h>
#include "unzip.h"
#include "zip.h"
#include <algorithm>
#include <util\ColorManager.hpp>



BF_NAMESPACE_USE

IMPLEMENT_IMAGE(CME_TNS_DECODE)	



const int		__niMsgCouunt = 500;
CString __csMsgArry[__niMsgCouunt] = { "" };




static UINT TNSProcesser(LPVOID lparam)
{
	CME_TNS_DECODE *pParser = (CME_TNS_DECODE*)lparam;

	pParser->fnAddMessage("TNSProcesser called ");

	pParser->m_iTotalCount = 0;
	pParser->m_bIsThreadRunning = true;
	pParser->m_bIsTrheadCompleted = false;

	pParser->ProcessTNS();
	pParser->RequestExternalUpdate();

	pParser->createZip();
	pParser->RequestExternalUpdate();
	
	pParser->m_bIsThreadRunning = false;
	pParser->RequestExternalUpdate();

	return 0;

	return true;
}


bool CME_TNS_DECODE::InitData()
{
	GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));

	m_curTopRow= 0; m_curTopRowDis = 0;
	for(m_numofRows = 0; GetField(e_LogMsg + m_numofRows); m_numofRows++);
	for(m_numofRowsDis = 0; GetField(e_SubscrID + m_numofRowsDis); m_numofRowsDis++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","UAT-RDS");
	csServerName = strServerName.get_value().c_str();


	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");

	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();

	if( !IsRestoreUpdate( ) )
	{
		BF_Reg_Key_Ptr pKey1(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\CME"));

		int iFTPLocation;
		RegDWORD strServerName(pKey1, "S3FTPLocation",0);
		iFTPLocation = strServerName.get_value();
		GetField(e_S3FTPBucket)->SetValueInt( iFTPLocation );

		int iInputLocation;
		RegDWORD strS3InPutLocation(pKey1, "S3InputLocation",2);
		iInputLocation = strS3InPutLocation.get_value();
		GetField(e_S3CMEBucket)->SetValueInt( iInputLocation );

		COleDateTime dtToday = COleDateTime::GetCurrentTime();
		BFDate bfToday;
		bfToday.SetYearMonthDate(dtToday.GetYear(),dtToday.GetMonth() - 1, dtToday.GetDay() );
		GetField(e_DateFilter)->SetValueDate(bfToday); 
	}

	fld_OutputPath = GetField(e_OutputPath);

	//m_clScheduleInfo.bValid = false;
	
	m_bIsThreadRunning = false;
	m_bIsTrheadCompleted = true;
	m_iTotalCount = 0;
	m_iOpenFileCount = 0;

	m_enmOperation = OP_None;
	return true;
} 

void CME_TNS_DECODE::fnAddMessage( CString csaNewMsg )
{	
	for( int iI = __niMsgCouunt-1; iI > 0; iI-- )
		__csMsgArry[iI] = __csMsgArry[iI -1];
	
	CString csMsg;
	csMsg.Format("(%s) %s",currentDateTime().c_str(),csaNewMsg);
	__csMsgArry[0] = csMsg;	
}

void CME_TNS_DECODE::fnDisplayMsg()
{
	int iRow = 0;
	for( int iIndex = m_curTopRow; iIndex < __niMsgCouunt; iIndex++, iRow++ )
	{
		if( iRow >= m_numofRows )
			break;

		BaseField * fldComment		= GetField(e_LogMsg + iRow);

		if(__csMsgArry[iIndex].Find("Success")!=-1 || __csMsgArry[iIndex].Find("success")!=-1)
			fldComment->SetForeColor(ColorManager::eFrameGreen);
		else if(__csMsgArry[iIndex].Find("Error")!=-1 || __csMsgArry[iIndex].Find("error")!=-1)
			fldComment->SetForeColor(ColorManager::eFrameRed);
		else
			fldComment->SetForeColor(ColorManager::eFrameWhite) ;

		fldComment->SetValueString( __csMsgArry[iIndex] );
		
		fldComment->SetNotManual();
	}

	while( iRow < m_numofRows )
	{
		BaseField * fldComment		= GetField(e_LogMsg + iRow);
		fldComment->SetBlankState();
		iRow++;
	}
}

void CME_TNS_DECODE::UploadSingleSub()
{
	CString __csDestinationPath = ((ListField*)GetField(e_S3FTPBucket))->GetShortString();

	CString __csAccount = ((ListField*)GetField(e_DestAccount))->GetShortString(); // This is Test Account

	CString csProfile = ((ListField*)GetField(e_S3FTPBucket))->GetLongString();

	int iRow = 0;
	for(unsigned int iIndex = m_curTopRowDis; iIndex < m_vecvecDisplaySub.size(); iIndex++, iRow++)
	{
		if( iRow >= m_numofRowsDis )
			break;

		TNSDisplaySubDtl &clBlockSub= m_vecvecDisplaySub[iIndex];

		if( IsFieldNew( e_Upload + iRow ) )
		{
			CString csSourcePath = clBlockSub.stOutputFileName;
			CString csDestAccount,csDestinationFolder;

			mapTNSSubscriptionDtl::iterator it = m_TNSSubMap.find(clBlockSub.stDEST_ACCOUNT);

			if(it!=m_TNSSubMap.end())
			{

				std::map<CString,stSubOutPutFile>::iterator itFile =  it->second.m_mapOutputFiles.begin();
				std::map<CString,stSubOutPutFile>::iterator itFileEnd =  it->second.m_mapOutputFiles.end();
				for( ;itFile!=itFileEnd; itFile++)
				{
					for(unsigned int i =0;i < itFile->second._mVecFileListForSplit.size();i++)
					{

						CString csOutputGZipFile;
						csOutputGZipFile.Format("%s.gz", itFile->second._mVecFileListForSplit[i].GetBuffer());

							if(exist(std::string(csOutputGZipFile.GetBuffer())))
							{

								CString csDestAccount,csDestinationFolder;

								if( GetField(e_DestAccount)->GetValueInt() == 1 )
									csDestAccount = it->second.stDEST_ACCOUNT;
								else
									csDestAccount = __csAccount;

								csDestinationFolder.Format("%s%s/TICK/",__csDestinationPath,  csDestAccount );

								CString csAWSCommand;

								csAWSCommand.Format(_T("aws s3 cp \"%s\" --profile \"%s\" \"%s\""),csOutputGZipFile,csProfile, csDestinationFolder);					
								it->second.iUploadReturn = system(csAWSCommand);	


								CString csFileName;// = strSourceZipFile;
								int iIndex = csOutputGZipFile.ReverseFind('\\');
								csFileName = csOutputGZipFile.Mid(iIndex+1);

								if( it->second.iUploadReturn == 0 )
								{
									CString stroutput;
									stroutput.Format("Success:: cmd %s", csAWSCommand );
									fnAddMessage( stroutput );
									unsigned long lsize = GetFileSize(csOutputGZipFile.GetString());
									fnInsertDeliveryReport("TICK", csDestAccount, csFileName, lsize, csAWSCommand);
									//fnInsertDeliveryReport("TICK", csDestAccount, csFileName, lsize);
								}
								else
								{
									CString stroutput;
									stroutput.Format("Error:: in upload cmd %s", csAWSCommand );
									fnAddMessage( stroutput );
									fnInsertErroReport("TICK",csDestAccount,csFileName,"",csAWSCommand);
								}

								//it->second.bUploaded = true;
							}
					}
				}
			}
		}		
	}
}
void CME_TNS_DECODE::Recalculate()
{
	BFDate bfdt = GetField(e_DateFilter)->GetValueDate();
	m_csProcessDate.Format("%d%02d%02d", bfdt.GetYear(), bfdt.GetMonth()+1, bfdt.GetDate());
			

	if(IsFieldNew(e_Button_loadSubscription))
	{
		
		LoadSubscription();
		fnAddMessage("Subscription loaded successfully.");
	}
	else if(IsFieldNew(e_Button_DownloadFROMS3))
	{
		fnAddMessage("starting Download from s3");
		fnCopyS3FiletoLocal();
		fnAddMessage("Download from s3 complete.");
	}
	else if(IsFieldNew(e_Button_Uncompress))
	{
		fnAddMessage("Started Decompressing files.");
		decompressAllFiles();
		fnAddMessage("Decompressing of files complete.");
	}
	else if(IsFieldNew(e_Button_ProcessSub))
	{
		AfxBeginThread(TNSProcesser, this);
	}
	else if(IsFieldNew(e_Button_UploadtoS3))
	{
		fnAddMessage("Upload to S3 started.");
		UploadSubscription();
		fnAddMessage("Upload to S3 Done.");
	}

	UploadSingleSub();

	int iTotalRecord = m_vecvecDisplaySub.size( );
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpBase,e_PgDnBase,iTotalRecord,m_numofRowsDis,m_curTopRowDis);	

	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_pgUP,e_pgDN,200,m_numofRows,m_curTopRow);	

	fnDisplayMsg();	
	DisplaySubscription( );
}
void CME_TNS_DECODE::setOutPutFile(CString & csAccount,CString & csFile)
{
	for(unsigned int i=0;i<m_vecvecDisplaySub.size();i++)
	{
		if(csAccount == m_vecvecDisplaySub[i].stDEST_ACCOUNT)
		{
			m_vecvecDisplaySub[i].stOutputFileName = csFile;
		}
	}
}

void CME_TNS_DECODE::parseFile(TNSSubscriptionDtl & objSubDtl ,CString & csFile,CString & csExchange)
{
	try
	{
		CString stddir;
		stddir.Format("%s\\%s",GetField(e_OutputPath)->GetValueString(),objSubDtl.stDEST_ACCOUNT);

		if(!dirExists(std::string(stddir.GetBuffer())))
			CreateDirectory(stddir.GetBuffer(), NULL);

		CString csTXTFile = csFile;
		csTXTFile.Replace(".ZIP",".txt");
		CString csLog;
		csLog.Format("Processing file: %s",csTXTFile);
		fnAddMessage(csLog);

		std::ifstream fpInputFile;
		fpInputFile.open(csTXTFile.GetBuffer());
		
		std::string strLine;	
		while(!fpInputFile.eof())
		{
			getline(fpInputFile,strLine);
			if (strLine.length() == BBO_STANDARD_LENGH || strLine.length() == BBO_OLD_LENGTH) 
			{
				std::string strSessionInd = strLine.substr(22, 1);
				std::string strAskBidType = strLine.substr(52, 1);
				std::string strIndQuote = strLine.substr(53, 1);
				std::string strMktQuote = strLine.substr(54, 1);
				std::string strCloseOpenType = strLine.substr(55, 1);

				if((strAskBidType.compare("A")!= 0) && (strAskBidType.compare("B")!= 0) && (strIndQuote.compare("I")!= 0) && (strMktQuote.compare("M")!= 0) && (strCloseOpenType.compare("B")!= 0) )
				{
					std::string strout="";

					ConvertRecord(strout,std::string(csExchange.GetBuffer()),strLine);

					std::map<CString,stSubOutPutFile>::iterator itFile = objSubDtl.m_mapOutputFiles.find(csExchange);
					if(itFile!=objSubDtl.m_mapOutputFiles.end())
					{
						/*if((itFile->second.iLineCount % 50000) == 0)
						{
						itFile->second.iFileCount = itFile->second.iFileCount + 1;

						CString tmpOutputFile;
						long lUniq = generateUniqueNo(objSubDtl.stDEST_ACCOUNT,csExchange,m_csProcessDate);
						CString tmpExch= csExchange;
						tmpExch.Replace("X","");
						tmpOutputFile.Format("%s\\TICK_%s_%s_%ld_%02d.csv",stddir,tmpExch,m_csProcessDate,lUniq,itFile->second.iFileCount);
						itFile->second.csFileName = tmpOutputFile;

						fclose(itFile->second.fp);
						itFile->second.fp = NULL;

						itFile->second.bIsFileOpen = false;

						if( fopen_s(&itFile->second.fp, itFile->second.csFileName,"w") )
						return;

						itFile->second.bIsFileOpen = true;
						itFile->second._mVecFileListForSplit.push_back(tmpOutputFile);

						fputs("T.Date,T.Time,Sequence,Session Ind,Symbol,C/P/F,Contract Delivery,Volume,Strike Price,T.Price,A/B,IND,MKQ,C/O,VOE,PC,CAN,INS,F/L,CAB,BKI,Entry Date,exch_code\n",itFile->second.fp);

						fputs(strout.c_str(),itFile->second.fp);
						fputs("\n",itFile->second.fp);
						itFile->second.iLineCount++;
						}
						else
						{*/
							fputs(strout.c_str(),itFile->second.fp);
							fputs("\n",itFile->second.fp);
							itFile->second.iLineCount++;
						//}
					}
					else
					{
						//create new file
						
						stSubOutPutFile _objstSubOutPutFile;
						_objstSubOutPutFile.csExch = csExchange;
						_objstSubOutPutFile.iFileCount = 0;
						_objstSubOutPutFile.iLineCount = 1;
						_objstSubOutPutFile.fp=NULL;

						CString tmpOutputFile;
						long lUniq = generateUniqueNo(objSubDtl.stDEST_ACCOUNT,csExchange,m_csProcessDate);
						CString tmpExch= csExchange;
						tmpExch.Replace("X","");
						tmpOutputFile.Format("%s\\TICK_%s_%s_%ld_%02d.csv",stddir,tmpExch,m_csProcessDate,lUniq,_objstSubOutPutFile.iFileCount);
						
						_objstSubOutPutFile.csFileName = tmpOutputFile;
						_objstSubOutPutFile._mVecFileListForSplit.push_back(tmpOutputFile);
						
						
						if( fopen_s(&_objstSubOutPutFile.fp, _objstSubOutPutFile.csFileName,"w") )
						{
							return;
						}
					
						_objstSubOutPutFile.bIsFileOpen =true;
						//_objstSubOutPutFile._mVecFileListForSplit.push_back(_objstSubOutPutFile.csFileName);	
						//fputs("T.Date,T.Time,Sequence,Session Ind,Symbol,C/P/F,Contract Delivery,Volume,Strike Price,T.Price,A/B,IND,MKQ,C/O,VOE,PC,CAN,INS,F/L,CAB,BKI,Entry Date,exch_code\n",_objstSubOutPutFile.fp);
						fputs(strout.c_str(),_objstSubOutPutFile.fp);
						fputs("\n",_objstSubOutPutFile.fp);
						objSubDtl.m_mapOutputFiles.insert(std::pair<CString,stSubOutPutFile>(csExchange,_objstSubOutPutFile));
						
					}
										/*fputs(strout.c_str(),objSubDtl.fp);
								fputs("\n",objSubDtl.fp);
								objSubDtl._mlCount++;
								
								if((objSubDtl._mlCount % 50000) == 0)
								{
									fclose(objSubDtl.fp);
				
									objSubDtl._miFileCount = objSubDtl._miFileCount + 1;
									objSubDtl.stOutputFileName.Format("%s\\%s_%d.csv",stddir,m_csProcessDate,objSubDtl._miFileCount);
					
									if( fopen_s(&objSubDtl.fp, objSubDtl.stOutputFileName,"w") )
										return;

									objSubDtl._vecFileList.push_back(objSubDtl.stOutputFileName);
									fputs("T.Date,T.Time,Sequence,Session Ind,Symbol,C/P/F,Contract Delivery,Volume,Strike Price,T.Price,A/B,IND,MKQ,C/O,VOE,PC,CAN,INS,F/L,CAB,BKI,Entry Date,exch_code\n",objSubDtl.fp);
									fputs(strout.c_str(),objSubDtl.fp);
									fputs("\n",objSubDtl.fp);
									objSubDtl._mlCount++;
								}*/
				}
			}
		}
		
		fpInputFile.close();
	}
	catch (CMemoryException* e)
	{
		e->Delete();
	}
	catch (CFileException* e)
	{
		e->Delete();
	}
	catch (CException* e)
	{
		e->Delete();
	}
	
}



void CME_TNS_DECODE::ProcessTNS()
{
	
	mapTNSSubscriptionDtl::iterator it = m_TNSSubMap.begin();
	for(;it!=m_TNSSubMap.end();it++)
	{
		CString csLog;
		csLog.Format("Processing Subscription for %s ",it->second.stDEST_ACCOUNT);
		fnAddMessage(csLog);

		CString csFile;
		for(unsigned int i=0;i<it->second._mVecExchProdCodes.size();i++)
		{
			
			if(it->second._mVecExchProdCodes[i].csALLPRODUCT == "1")
			{
				for(unsigned int j =0; j<m_vecDownloadedZipfiles.size();j++)
				{

					CString csLog;
					csLog.Format("m_vecDownloadedZipfiles[j]: %s -- Exch: %s",m_vecDownloadedZipfiles[j],it->second._mVecExchProdCodes[i].csExch);
					fnAddMessage(csLog);

					if(m_vecDownloadedZipfiles[j].Find(it->second._mVecExchProdCodes[i].csExch.GetBuffer())!=-1) 
					{
						CString stddir,strDisplayOutputFile;
						stddir.Format("%s\\%s",GetField(e_OutputPath)->GetValueString(),it->second.stDEST_ACCOUNT);

						strDisplayOutputFile.Format("%s\\%s.zip",stddir,m_csProcessDate);

						setOutPutFile(it->second.stDEST_ACCOUNT,strDisplayOutputFile);
					
						parseFile(it->second,m_vecDownloadedZipfiles[j],it->second._mVecExchProdCodes[i].csExch);
					}
				}
			}
			else 
			{
				if(it->second._mVecExchProdCodes[i].csVENUE == "X")
				{
					csFile.Format("%s_%s_%s_%s.ZIP",it->second._mVecExchProdCodes[i].csExch,it->second._mVecExchProdCodes[i].csTNS,it->second._mVecExchProdCodes[i].csFUTOPT,m_csProcessDate.Right(m_csProcessDate.GetLength()-2).GetBuffer());		
					for(unsigned int j =0; j<m_vecDownloadedZipfiles.size();j++)
					{
						if((m_vecDownloadedZipfiles[j].Find(csFile.GetBuffer())!=-1))
						{
							CString stddir,strDisplayOutputFile;

							stddir.Format("%s\\%s",GetField(e_OutputPath)->GetValueString(),it->second.stDEST_ACCOUNT);

							strDisplayOutputFile.Format("%s\\%s.zip",stddir,m_csProcessDate);

							setProdExchOutPutFile(it->second.stDEST_ACCOUNT,it->second._mVecExchProdCodes[i].csExch,it->second._mVecExchProdCodes[i].csTNS,strDisplayOutputFile);

							parseFile(it->second,m_vecDownloadedZipfiles[j],it->second._mVecExchProdCodes[i].csExch);
						}
					}
				}
				else if(it->second._mVecExchProdCodes[i].csVENUE == "F")
				{
					csFile.Format("%s_%s_%s_%s.ZIP",it->second._mVecExchProdCodes[i].csExch,it->second._mVecExchProdCodes[i].csTNS,it->second._mVecExchProdCodes[i].csFUTOPT,m_csProcessDate.Right(m_csProcessDate.GetLength()-2).GetBuffer());		
					for(unsigned int j =0; j<m_vecDownloadedZipfiles.size();j++)
					{
						if((m_vecDownloadedZipfiles[j].Find(csFile.GetBuffer())!=-1))
						{
							if(m_vecDownloadedZipfiles[j].Find("openoutcry")!=-1)
							{

							CString stddir,strDisplayOutputFile;

							stddir.Format("%s\\%s",GetField(e_OutputPath)->GetValueString(),it->second.stDEST_ACCOUNT);

							strDisplayOutputFile.Format("%s\\%s.zip",stddir,m_csProcessDate);

							setProdExchOutPutFile(it->second.stDEST_ACCOUNT,it->second._mVecExchProdCodes[i].csExch,it->second._mVecExchProdCodes[i].csTNS,strDisplayOutputFile);

							parseFile(it->second,m_vecDownloadedZipfiles[j],it->second._mVecExchProdCodes[i].csExch);
							}
						}
					}
			}
			else if(it->second._mVecExchProdCodes[i].csVENUE == "E")
			{
				csFile.Format("%s_%s_%s_%s.ZIP",it->second._mVecExchProdCodes[i].csExch,it->second._mVecExchProdCodes[i].csTNS,it->second._mVecExchProdCodes[i].csFUTOPT,m_csProcessDate.Right(m_csProcessDate.GetLength()-2).GetBuffer());		
				for(unsigned int j =0; j<m_vecDownloadedZipfiles.size();j++)
				{
					if((m_vecDownloadedZipfiles[j].Find(csFile.GetBuffer())!=-1))
					{
						if(m_vecDownloadedZipfiles[j].Find("globex")!=-1)
						{

							CString stddir,strDisplayOutputFile;

							stddir.Format("%s\\%s",GetField(e_OutputPath)->GetValueString(),it->second.stDEST_ACCOUNT);

							strDisplayOutputFile.Format("%s\\%s.zip",stddir,m_csProcessDate);

							setProdExchOutPutFile(it->second.stDEST_ACCOUNT,it->second._mVecExchProdCodes[i].csExch,it->second._mVecExchProdCodes[i].csTNS,strDisplayOutputFile);

							parseFile(it->second,m_vecDownloadedZipfiles[j],it->second._mVecExchProdCodes[i].csExch);
						}
					}
				}
			}
		}
	}


	mapTNSSubscriptionDtl::iterator it1 = m_TNSSubMap.begin();
	for(;it1!=m_TNSSubMap.end();it1++)
	{
	
		std::map<CString,stSubOutPutFile>::iterator itFile =  it1->second.m_mapOutputFiles.begin();
		std::map<CString,stSubOutPutFile>::iterator itFileEnd =  it1->second.m_mapOutputFiles.end();
		for( ;itFile!=itFileEnd; itFile++)
		{
			if(itFile->second.fp!=NULL)
			{
				fclose(itFile->second.fp);
				itFile->second.fp=NULL;
			}
		}
	}
}
}
void CME_TNS_DECODE::setProdExchOutPutFile(CString & csDestAccount,CString & csExch,CString & csTICKCODE,CString & csOutputfile)
{
	for(unsigned int i=0;i<m_vecvecDisplaySub.size();i++)
	{
		if((csDestAccount == m_vecvecDisplaySub[i].stDEST_ACCOUNT) && (csExch == m_vecvecDisplaySub[i].stEXCH_CODE) && (csTICKCODE == m_vecvecDisplaySub[i].stTNS_CODE))
		{
			m_vecvecDisplaySub[i].stOutputFileName = csOutputfile;
		}
	}
}


void CME_TNS_DECODE::LoadSubscription()
{
	m_TNSSubMap.clear(); 
	m_vecvecDisplaySub.clear();
	
	int icount=1;
	CString sql;	
	sql.Format("SELECT CUST_NAME, CONTACT_EMAIL, DEST_ACCOUNT, FTP_PASSWORD, VENUE_CODE, ALL_PRODUCTS, PROD_CODE, BBO_CODE,TICK_CODE, EXCH_CODE ,FOI_CODE FROM dbo.subscription_2015 \
				where FILE_TYPE = 'TICK' \
				and Active = 1\
				and ALL_PRODUCTS = 0\
				and BBO_CODE <> 'null'\
				and DEST_ACCOUNT <> 'ftp_beast'\
				order by DEST_ACCOUNT");

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		ASSERT(0);
		return;
	}
	
	
	while(VARIANT_FALSE == set->adoEOF)
	{
		TNSSubscriptionDtl clBlockSub;
		CString FOI,csDestAccount;

		SETSTR(	csDestAccount,	set->Fields->Item[_variant_t("DEST_ACCOUNT")]->Value);

		mapTNSSubscriptionDtl::iterator it =  m_TNSSubMap.find(csDestAccount);

		if(it!=m_TNSSubMap.end())
		{
			//Update
			stEXchProd objstEXchProd;

			SETSTR(	objstEXchProd.csVENUE,		set->Fields->Item[_variant_t("VENUE_CODE")]->Value);
			SETSTR(	objstEXchProd.csALLPRODUCT,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);
			SETSTR(	objstEXchProd.csTNS,		set->Fields->Item[_variant_t("BBO_CODE")]->Value);	
			SETSTR(	objstEXchProd.csExch,		set->Fields->Item[_variant_t("EXCH_CODE")]->Value);
			SETSTR(	objstEXchProd.csFUTOPT,		set->Fields->Item[_variant_t("FOI_CODE")]->Value);

				if(objstEXchProd.csVENUE == "E")
					objstEXchProd.csFileName.Format("globex/%s/%s_%s_%s_%s.ZIP",m_csProcessDate,objstEXchProd.csExch,objstEXchProd.csTNS,objstEXchProd.csFUTOPT,m_csProcessDate.Right(m_csProcessDate.GetLength()-2));
				else if(objstEXchProd.csVENUE == "F")
					objstEXchProd.csFileName.Format("openoutcry/%s/%s_%s_%s_%s.ZIP",m_csProcessDate,objstEXchProd.csExch,objstEXchProd.csTNS,objstEXchProd.csFUTOPT,m_csProcessDate.Right(m_csProcessDate.GetLength()-2));
				else if(objstEXchProd.csVENUE == "X")
					objstEXchProd.csFileName.Format("%s/%s_%s_%s_%s.ZIP",m_csProcessDate,objstEXchProd.csExch,objstEXchProd.csTNS,objstEXchProd.csFUTOPT,m_csProcessDate.Right(m_csProcessDate.GetLength()-2));
			
						

			TNSDisplaySubDtl objTNSDisplaySubDtl;
			
			objTNSDisplaySubDtl.iOrderId = 1000*icount;
			objTNSDisplaySubDtl.stCUST_NAME = it->second.stCUST_NAME;
			objTNSDisplaySubDtl.stCONTACT_EMAIL = it->second.stCONTACT_EMAIL;
			objTNSDisplaySubDtl.stDEST_ACCOUNT = it->second.stDEST_ACCOUNT;
			objTNSDisplaySubDtl.stFTP_PASSWORD = it->second.stFTP_PASSWORD;

			objTNSDisplaySubDtl.stALL_PRODUCTS = objstEXchProd.csALLPRODUCT;
			objTNSDisplaySubDtl.stTNS_CODE = objstEXchProd.csTNS;
			objTNSDisplaySubDtl.stEXCH_CODE = objstEXchProd.csExch;
			objTNSDisplaySubDtl.stVENUE_CODE = objstEXchProd.csVENUE;
			objTNSDisplaySubDtl.stFOI = objstEXchProd.csFUTOPT;
		
			
			   if(!isSubExists(it->second,objstEXchProd))
				{
					m_vecvecDisplaySub.push_back(objTNSDisplaySubDtl);
					it->second._mVecExchProdCodes.push_back(objstEXchProd);
					icount++;
				}
		}
		else
		{
			//create
			TNSSubscriptionDtl clBlockSub;


			SETSTR(	clBlockSub.stCUST_NAME,		set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
			SETSTR(	clBlockSub.stCONTACT_EMAIL,	set->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);	
			SETSTR(	clBlockSub.stDEST_ACCOUNT,	set->Fields->Item[_variant_t("DEST_ACCOUNT")]->Value);	
			SETSTR(	clBlockSub.stFTP_PASSWORD,	set->Fields->Item[_variant_t("FTP_PASSWORD")]->Value);	
			
			stEXchProd objstEXchProd;

			SETSTR(	objstEXchProd.csVENUE,		set->Fields->Item[_variant_t("VENUE_CODE")]->Value);
			SETSTR(	objstEXchProd.csALLPRODUCT,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);
			SETSTR(	objstEXchProd.csTNS,		set->Fields->Item[_variant_t("BBO_CODE")]->Value);	
			SETSTR(	objstEXchProd.csExch,		set->Fields->Item[_variant_t("EXCH_CODE")]->Value);
			SETSTR(	objstEXchProd.csFUTOPT,		set->Fields->Item[_variant_t("FOI_CODE")]->Value);
			
				if(objstEXchProd.csVENUE == "E" )
					objstEXchProd.csFileName.Format("globex/%s/%s_%s_%s_%s.ZIP",m_csProcessDate,objstEXchProd.csExch,objstEXchProd.csTNS,objstEXchProd.csFUTOPT,m_csProcessDate.Right(m_csProcessDate.GetLength()-2));
				else if(objstEXchProd.csVENUE == "F")
					objstEXchProd.csFileName.Format("openoutcry/%s/%s_%s_%s_%s.ZIP",m_csProcessDate,objstEXchProd.csExch,objstEXchProd.csTNS,objstEXchProd.csFUTOPT,m_csProcessDate.Right(m_csProcessDate.GetLength()-2));
				else if(objstEXchProd.csVENUE == "X" )
					objstEXchProd.csFileName.Format("%s/%s_%s_%s_%s.ZIP",m_csProcessDate,objstEXchProd.csExch,objstEXchProd.csTNS,objstEXchProd.csFUTOPT,m_csProcessDate.Right(m_csProcessDate.GetLength()-2));

		
			

			TNSDisplaySubDtl objTNSDisplaySubDtl;

			objTNSDisplaySubDtl.iOrderId = 1000*icount;
			objTNSDisplaySubDtl.stCUST_NAME = clBlockSub.stCUST_NAME;
			objTNSDisplaySubDtl.stCONTACT_EMAIL = clBlockSub.stCONTACT_EMAIL;
			objTNSDisplaySubDtl.stDEST_ACCOUNT = clBlockSub.stDEST_ACCOUNT;
			objTNSDisplaySubDtl.stFTP_PASSWORD = clBlockSub.stFTP_PASSWORD;
			
			objTNSDisplaySubDtl.stALL_PRODUCTS = objstEXchProd.csALLPRODUCT;
			objTNSDisplaySubDtl.stTNS_CODE = objstEXchProd.csTNS;
			objTNSDisplaySubDtl.stEXCH_CODE = objstEXchProd.csExch;
			objTNSDisplaySubDtl.stVENUE_CODE = objstEXchProd.csVENUE;
			objTNSDisplaySubDtl.stFOI = objstEXchProd.csFUTOPT;
			
			if(!isSubExists(clBlockSub,objstEXchProd))
			{
				m_vecvecDisplaySub.push_back(objTNSDisplaySubDtl);
				clBlockSub._mVecExchProdCodes.push_back(objstEXchProd);
				icount++;
			}

			m_TNSSubMap.insert(std::pair<CString,TNSSubscriptionDtl>(clBlockSub.stDEST_ACCOUNT,clBlockSub));

		}
		set->MoveNext();
	
	}

	set->Close();	


		
	sql.Format("SELECT CUST_NAME, CONTACT_EMAIL, DEST_ACCOUNT, FTP_PASSWORD, VENUE_CODE, ALL_PRODUCTS, PROD_CODE, BBO_CODE, EXCH_CODE , 'null' as FOI \
				FROM dbo.subscription_2015 \
				where FILE_TYPE = 'TICK' \
				and Active = 1 \
				and ALL_PRODUCTS =1");


	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		ASSERT(0);
		return;
	}
	while(VARIANT_FALSE == set->adoEOF)
	{
		TNSSubscriptionDtl clBlockSub;
		CString FOI,csDestAccount;

		SETSTR(	csDestAccount,	set->Fields->Item[_variant_t("DEST_ACCOUNT")]->Value);

		mapTNSSubscriptionDtl::iterator it =  m_TNSSubMap.find(csDestAccount);
		if(it!=m_TNSSubMap.end())
		{
			stEXchProd objstEXchProd;

			SETSTR(	objstEXchProd.csVENUE,		set->Fields->Item[_variant_t("VENUE_CODE")]->Value);
			SETSTR(	objstEXchProd.csALLPRODUCT,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);
			SETSTR(	objstEXchProd.csTNS,		set->Fields->Item[_variant_t("BBO_CODE")]->Value);	
			SETSTR(	objstEXchProd.csExch,		set->Fields->Item[_variant_t("EXCH_CODE")]->Value);
			SETSTR(	objstEXchProd.csFUTOPT,		set->Fields->Item[_variant_t("FOI_CODE")]->Value);

			
			objstEXchProd.csFileName.Format("%s_*.ZIP",objstEXchProd.csExch);
		
			


			TNSDisplaySubDtl objTNSDisplaySubDtl;
			objTNSDisplaySubDtl.iOrderId = 1000*icount;
			objTNSDisplaySubDtl.stCUST_NAME = it->second.stCUST_NAME;
			objTNSDisplaySubDtl.stCONTACT_EMAIL = it->second.stCONTACT_EMAIL;
			objTNSDisplaySubDtl.stDEST_ACCOUNT = it->second.stDEST_ACCOUNT;
			objTNSDisplaySubDtl.stFTP_PASSWORD = it->second.stFTP_PASSWORD;

			objTNSDisplaySubDtl.stALL_PRODUCTS = objstEXchProd.csALLPRODUCT;
			objTNSDisplaySubDtl.stTNS_CODE = objstEXchProd.csTNS;
			objTNSDisplaySubDtl.stEXCH_CODE = objstEXchProd.csExch;
			objTNSDisplaySubDtl.stVENUE_CODE = objstEXchProd.csVENUE;
			objTNSDisplaySubDtl.stFOI = objstEXchProd.csFUTOPT;
		
			if(!isSubExists(it->second,objstEXchProd))
			{
				m_vecvecDisplaySub.push_back(objTNSDisplaySubDtl);
				it->second._mVecExchProdCodes.push_back(objstEXchProd);
				icount++;
			}
		}
		else
		{
			//create
			TNSSubscriptionDtl clBlockSub;

			SETSTR(	clBlockSub.stCUST_NAME,		set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
			SETSTR(	clBlockSub.stCONTACT_EMAIL,	set->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);	
			SETSTR(	clBlockSub.stDEST_ACCOUNT,	set->Fields->Item[_variant_t("DEST_ACCOUNT")]->Value);	
			SETSTR(	clBlockSub.stFTP_PASSWORD,	set->Fields->Item[_variant_t("FTP_PASSWORD")]->Value);	
			
			stEXchProd objstEXchProd;

			SETSTR(	objstEXchProd.csVENUE,		set->Fields->Item[_variant_t("VENUE_CODE")]->Value);
			SETSTR(	objstEXchProd.csALLPRODUCT,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);
			SETSTR(	objstEXchProd.csTNS,		set->Fields->Item[_variant_t("BBO_CODE")]->Value);	
			SETSTR(	objstEXchProd.csExch,		set->Fields->Item[_variant_t("EXCH_CODE")]->Value);
			SETSTR(	objstEXchProd.csFUTOPT,		set->Fields->Item[_variant_t("FOI_CODE")]->Value);
				
			objstEXchProd.csFileName.Format("%s_*.ZIP",objstEXchProd.csExch);
				

			TNSDisplaySubDtl objTNSDisplaySubDtl;

			objTNSDisplaySubDtl.iOrderId = 1000*icount;
			objTNSDisplaySubDtl.stCUST_NAME = clBlockSub.stCUST_NAME;
			objTNSDisplaySubDtl.stCONTACT_EMAIL = clBlockSub.stCONTACT_EMAIL;
			objTNSDisplaySubDtl.stDEST_ACCOUNT = clBlockSub.stDEST_ACCOUNT;
			objTNSDisplaySubDtl.stFTP_PASSWORD = clBlockSub.stFTP_PASSWORD;

			objTNSDisplaySubDtl.stALL_PRODUCTS = objstEXchProd.csALLPRODUCT;
			objTNSDisplaySubDtl.stTNS_CODE = objstEXchProd.csTNS;
			objTNSDisplaySubDtl.stEXCH_CODE = objstEXchProd.csExch;
			objTNSDisplaySubDtl.stVENUE_CODE = objstEXchProd.csVENUE;
			objTNSDisplaySubDtl.stFOI = objstEXchProd.csFUTOPT;
			
			if(!isSubExists(clBlockSub,objstEXchProd))
			{
				m_vecvecDisplaySub.push_back(objTNSDisplaySubDtl);
				clBlockSub._mVecExchProdCodes.push_back(objstEXchProd);
				icount++;
			}

		    m_TNSSubMap.insert(std::pair<CString,TNSSubscriptionDtl>(clBlockSub.stDEST_ACCOUNT,clBlockSub));
		}
		set->MoveNext();
		
	}
	set->Close();	
}

bool CME_TNS_DECODE::exist(const std::string& name)
{
	
	std::ifstream file(name);
	if(!file)            // If the file was not found, then file is 0, i.e. !file=1 or true.
		return false;    // The file was not found.
	else                 // If the file was found, then file is non-0.
		return true;     // The file was found.
}



void CME_TNS_DECODE::UploadSubscription()
{
	CString csTemp;

	CString __csDestinationPath = ((ListField*)GetField(e_S3FTPBucket))->GetShortString();

	CString __csAccount = ((ListField*)GetField(e_DestAccount))->GetShortString(); // This is Test Account

	CString csProfile = ((ListField*)GetField(e_S3FTPBucket))->GetLongString();

	mapTNSSubscriptionDtl::iterator it = m_TNSSubMap.begin();
	for(;it!=m_TNSSubMap.end();it++)
	{
		std::map<CString,stSubOutPutFile>::iterator itFile =  it->second.m_mapOutputFiles.begin();
		std::map<CString,stSubOutPutFile>::iterator itFileEnd =  it->second.m_mapOutputFiles.end();
		for( ;itFile!=itFileEnd; itFile++)
		{
			for(unsigned int i =0;i < itFile->second._mVecFileListForSplit.size();i++)
			{

				CString csOutputGZipFile;
				csOutputGZipFile.Format("%s.gz", itFile->second._mVecFileListForSplit[i].GetBuffer());
				
					if(exist(std::string(csOutputGZipFile.GetBuffer())))
					{

						CString csDestAccount,csDestinationFolder;

						if( GetField(e_DestAccount)->GetValueInt() == 1 )
							csDestAccount = it->second.stDEST_ACCOUNT;
						else
							csDestAccount = __csAccount;

						csDestinationFolder.Format("%s%s/TICK/",__csDestinationPath,  csDestAccount );

						CString csAWSCommand;

						csAWSCommand.Format(_T("aws s3 cp \"%s\" --profile \"%s\" \"%s\""),csOutputGZipFile,csProfile, csDestinationFolder);					
						it->second.iUploadReturn = system(csAWSCommand);	

						CString csFileName;
						int iIndex = csOutputGZipFile.ReverseFind('\\');
						csFileName = csOutputGZipFile.Mid(iIndex+1);

						if( it->second.iUploadReturn == 0 )
						{
							CString stroutput;
							stroutput.Format("Success:: cmd %s", csAWSCommand );
							fnAddMessage( stroutput );
											
							unsigned long lsize = GetFileSize(csOutputGZipFile.GetString());

							//fnInsertDeliveryReport("TICK", csDestAccount, csFileName, lsize);
							fnInsertDeliveryReport("TICK", csDestAccount, csFileName, lsize, csAWSCommand);
						}
						else
						{
							CString stroutput;
							stroutput.Format("Error:: in upload cmd %s", csAWSCommand );
							fnAddMessage( stroutput );
							fnInsertErroReport("TICK",csDestAccount,csFileName,"",csAWSCommand);
						}

					}
			}
		}
	}
}

void CME_TNS_DECODE::fnCopyS3FiletoLocal( )
{

	CString strBasePath = fld_OutputPath->GetValueString();

	CString csInputBucket = ((ListField*)GetField(e_S3CMEBucket))->GetShortString();
	CString csProfile = ((ListField*)GetField(e_S3CMEBucket))->GetLongString();

	CString strFullInputPath,csAWSCommand;
	CString csDestinationFolder;
	bool bIsGLobex=false;
	bool bIsOpenOut=false;

	int iDownloadCount=0;

	m_vecDownloadedZipfiles.clear();
	// GLOBEX

	strFullInputPath.Format("%s/daily/bbo/globex/%s/",csInputBucket,m_csProcessDate);
	csDestinationFolder.Format("%s\\globex\\%s\\",strBasePath, m_csProcessDate);


	if(!dirExists(std::string(csDestinationFolder.GetBuffer())))
		CreateDirectory(csDestinationFolder.GetBuffer(), NULL);

	csAWSCommand.Format(_T("aws s3 sync --profile %s %s %s"),csProfile,strFullInputPath, csDestinationFolder);	
	int iGlobexValue = system(csAWSCommand);	
	if(iGlobexValue == 0) 
	{
		bIsGLobex = true;
		fnAddMessage("Success:: cmd " + csAWSCommand);
	}
	else
	{
		bIsGLobex = false;
		/*m_clScheduleInfo.clScheduleDtTime = COleDateTime::GetCurrentTime();;
		m_clScheduleInfo.clScheduleDtTime += COleDateTimeSpan(0,0,10,0);
		m_clScheduleInfo.iRetryCount++;*/
		m_enmOperation = OP_None;
		CString csTemp;
		csTemp.Format("Error:: cmd %s", csAWSCommand );
		fnAddMessage( csTemp );
	}

	// OPENOUTCRY
	strFullInputPath.Format("%s/daily/bbo/openoutcry/%s/",csInputBucket,m_csProcessDate);
	csDestinationFolder.Format("%s\\openoutcry\\%s\\",strBasePath, m_csProcessDate);


	if(!dirExists(std::string(csDestinationFolder.GetBuffer())))
		CreateDirectory(csDestinationFolder.GetBuffer(), NULL);

	csAWSCommand.Format(_T("aws s3 sync --profile %s %s %s"),csProfile,strFullInputPath, csDestinationFolder);	
	iGlobexValue = system(csAWSCommand);	
	if(iGlobexValue == 0) 
	{
		bIsOpenOut = true;
		fnAddMessage("Success:: cmd " + csAWSCommand);
	}
	else
	{
		bIsOpenOut = false;
		/*m_clScheduleInfo.clScheduleDtTime = COleDateTime::GetCurrentTime();;
		m_clScheduleInfo.clScheduleDtTime += COleDateTimeSpan(0,0,10,0);
		m_clScheduleInfo.iRetryCount++;*/
		m_enmOperation = OP_None;
		CString csTemp;
		csTemp.Format("Error:: cmd %s", csAWSCommand );
		fnAddMessage( csTemp );
	}

	if(!bIsOpenOut && !bIsGLobex)
	{
		m_enmOperation = OP_None;
		return;
	}

	mapTNSSubscriptionDtl::iterator it =  m_TNSSubMap.begin();
	for(;it!=m_TNSSubMap.end();it++)
	{
		for(unsigned int i=0;i<it->second._mVecExchProdCodes.size();i++)
		{
			if(it->second._mVecExchProdCodes[i].csALLPRODUCT == "1")
			{
				if(bIsGLobex)
				{
					csDestinationFolder.Format("%s\\globex\\%s\\",strBasePath, m_csProcessDate);
					get_all_files_names_within_folder(std::string(csDestinationFolder.GetBuffer()),std::string(it->second._mVecExchProdCodes[i].csExch));
				}

				if(bIsOpenOut)
				{
					csDestinationFolder.Format("%s\\openoutcry\\%s\\",strBasePath, m_csProcessDate);
					get_all_files_names_within_folder(std::string(csDestinationFolder.GetBuffer()),std::string(it->second._mVecExchProdCodes[i].csExch));
				}
			}
			else
			{
				if(it->second._mVecExchProdCodes[i].csVENUE == "E")
				{
					if(bIsGLobex)
					{
						csDestinationFolder.Format("%s\\globex\\%s\\",strBasePath, m_csProcessDate);
						CString csFileName ;
						csFileName.Format("%s_%s_%s_%s",it->second._mVecExchProdCodes[i].csExch,it->second._mVecExchProdCodes[i].csTNS,it->second._mVecExchProdCodes[i].csFUTOPT,m_csProcessDate.Right(m_csProcessDate.GetLength()-2));
						get_all_files_names_within_folder(std::string(csDestinationFolder.GetBuffer()),std::string(csFileName));
					}
				}
				else if(it->second._mVecExchProdCodes[i].csVENUE == "F")
				{
					if(bIsOpenOut)
					{
						csDestinationFolder.Format("%s\\openoutcry\\%s\\",strBasePath, m_csProcessDate);
						CString csFileName ;
						csFileName.Format("%s_%s_%s_%s",it->second._mVecExchProdCodes[i].csExch,it->second._mVecExchProdCodes[i].csTNS,it->second._mVecExchProdCodes[i].csFUTOPT,m_csProcessDate.Right(m_csProcessDate.GetLength()-2));
						get_all_files_names_within_folder(std::string(csDestinationFolder.GetBuffer()),std::string(csFileName));
					}
				}
				else if(it->second._mVecExchProdCodes[i].csVENUE == "X")
				{
					if(bIsOpenOut)
					{
						csDestinationFolder.Format("%s\\openoutcry\\%s\\",strBasePath, m_csProcessDate);
						CString csFileName ;
						csFileName.Format("%s_%s_%s_%s",it->second._mVecExchProdCodes[i].csExch,it->second._mVecExchProdCodes[i].csTNS,it->second._mVecExchProdCodes[i].csFUTOPT,m_csProcessDate.Right(m_csProcessDate.GetLength()-2));
						get_all_files_names_within_folder(std::string(csDestinationFolder.GetBuffer()),std::string(csFileName));
					}
					if(bIsGLobex)
					{
						csDestinationFolder.Format("%s\\globex\\%s\\",strBasePath, m_csProcessDate);
						CString csFileName ;
						csFileName.Format("%s_%s_%s_%s",it->second._mVecExchProdCodes[i].csExch,it->second._mVecExchProdCodes[i].csTNS,it->second._mVecExchProdCodes[i].csFUTOPT,m_csProcessDate.Right(m_csProcessDate.GetLength()-2));
						get_all_files_names_within_folder(std::string(csDestinationFolder.GetBuffer()),std::string(csFileName));
					}
				}

			}
		}
	}

	CString csDestFolder;

	if(bIsOpenOut)
	{
		csDestFolder.Format("%s\\openoutcry\\%s\\",strBasePath, m_csProcessDate);
		RemoveUnncessaryFiles(std::string(csDestFolder.GetBuffer()));
	}
	if(bIsGLobex)
	{
		csDestFolder.Format("%s\\globex\\%s\\",strBasePath, m_csProcessDate);
		RemoveUnncessaryFiles(std::string(csDestFolder.GetBuffer()));
	}

	if(bIsOpenOut || bIsGLobex)   
		m_enmOperation = OP_Uncompress;
}

void CME_TNS_DECODE::RemoveUnncessaryFiles(std::string & folder)
{

	char search_path[200];
    sprintf(search_path, "%s/*.*", folder.c_str());
    WIN32_FIND_DATA fd; 
    HANDLE hFind = ::FindFirstFile(search_path, &fd); 
    if(hFind != INVALID_HANDLE_VALUE) { 
        do { 
            // read all (real) files in current folder
            // , delete '!' read other 2 default folder . and ..
            if(! (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ) 
			{
					CString csFile(fd.cFileName);
					if((csFile.Find(".ZIP")!=-1))
					{
						std::string strFile = folder + fd.cFileName;

						if (std::find(m_vecDownloadedZipfiles.begin(), m_vecDownloadedZipfiles.end(), CString(strFile.c_str()))!=m_vecDownloadedZipfiles.end() )
						{
						   continue; 
						}
						else
						{
							std::remove(strFile.c_str());
							//m_vecDownloadedZipfiles.push_back(CString(strFile.c_str()));
							//fnAddMessage("push_back " + CString(strFile.c_str()));
						}
					}
				
            }
        }while(::FindNextFile(hFind, &fd)); 
        ::FindClose(hFind); 
    } 
}

void CME_TNS_DECODE::decompressAllFiles()
{
	for (unsigned int i = 0 ;i<m_vecDownloadedZipfiles.size();i++)
		createUnzip(std::string(m_vecDownloadedZipfiles[i].GetBuffer()));
	m_enmOperation = OP_Process;
 }

void CME_TNS_DECODE::createUnzip(std::string & sInputFile)
{
	size_t index = sInputFile.find_last_of("\\");
	std::string unzipath = sInputFile.substr(0, index+1);
	std::string strOutputString;

	HZIP hz = OpenZip(sInputFile.c_str(),0);
	ZIPENTRY ze; GetZipItem(hz,-1,&ze); int numitems=ze.index;
	// -1 gives overall information about the zipfile
	for (int zi=0; zi<numitems; zi++)
	{ 
		ZIPENTRY ze; GetZipItem(hz,zi,&ze); // fetch individual details

		strOutputString =  unzipath + ze.name;
		UnzipItem(hz, zi, strOutputString.c_str());         // e.g. the item's name.
	}
	CloseZip(hz);

	CString csOutput;
	csOutput += "Success:: UnZip created: ";
	csOutput += strOutputString.c_str();
	fnAddMessage(csOutput);
	RequestExternalUpdate();
	
}


bool CME_TNS_DECODE::ConvertRecord(std::string & strout,std::string & strExch,std::string & strLine)
{
	strout.append(strLine.substr(0, 8));
	strout.append(DELIM);

	strout.append(formatTime(strLine.substr(8, 6)));
	strout.append(DELIM);

	std::string strNum = strLine.substr(14, 8);
	trim2(strNum);

	strout.append(strNum);
	strout.append(DELIM);

	strout.append(strLine.substr(22, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(23, 3));
	strout.append(DELIM);

	strout.append(strLine.substr(26, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(27, 4));
	strout.append(DELIM);

	std::string strNum1 = strLine.substr(31, 5);
	trim2(strNum1);

	strout.append(strNum1);
	strout.append(DELIM);

	std::string strPrice1 = strLine.substr(36, 7);
	trim2(strPrice1);

	std::string strPrice2 = strLine.substr(43, 1);
	trim2(strPrice2);

	strout.append(formatPrice(strPrice1, strPrice2));
	strout.append(DELIM);

	strPrice1 = strLine.substr(44, 7);
	trim2(strPrice1);

	strPrice2 = strLine.substr(51, 1);
	trim2(strPrice2);

	strout.append(formatPrice(strPrice1, strPrice2));
	strout.append(DELIM);

	strout.append(strLine.substr(52, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(53, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(54, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(55, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(56, 2));
	strout.append(DELIM);

	strout.append(strLine.substr(58, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(59, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(60, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(61, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(62, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(63, 1));
	strout.append(DELIM);

	strout.append(formatEntryDate(strLine));
	strout.append(DELIM);

	CString csExch(strExch.c_str());
	csExch.Replace("X","");
	strout.append(csExch.GetBuffer());
	strout.append(DELIM);

	return true;
}

std::string CME_TNS_DECODE::formatTime(std::string  trdtime)
{
	switch (trdtime.length()) {
	case (1):
		return ONEDIGETTIME + trdtime;
	case (2):
		return TWODIGETTIME + trdtime;
	case (3):
		return THREEDIGETTIME + trdtime.substr(0, 1) + COL
			+ trdtime.substr(1);
	case (4):
		return FOURDIGETTIME + trdtime.substr(0, 1) + COL
			+ trdtime.substr(2);
	case (5):
		return FIVEDIGETTIME + trdtime.substr(0, 1) + COL
			+ trdtime.substr(1, 2) + COL + trdtime.substr(3);
	default:
		return trdtime.substr(0, 2) + COL + trdtime.substr(2, 2)
			+ COL + trdtime.substr(4, 2);

	}
}

std::string CME_TNS_DECODE::formatEntryDate(std::string line) 
{
	if (line.length() == 70) 
	{
		std::string strtmp = line.substr(64, 6);
		trim2(strtmp);
		return TWENTY + strtmp;
	} 
	else 
	{
		return EMPTY_ENTRY_DATE;
	}
}

void CME_TNS_DECODE::trim2(std::string & str)
{
	std::string::size_type pos = str.find_last_not_of(' ');
	if(pos != std::string::npos) {
		str.erase(pos + 1);
		pos = str.find_first_not_of(' ');
		if(pos != std::string::npos) str.erase(0, pos);
	}
	else str.erase(str.begin(), str.end());
}

std::string CME_TNS_DECODE::formatPrice(std::string inPrice, std::string decimalLoc) 
{
	if (!_strcmpi(inPrice.c_str(),"")) 
	{
		return PRICEZERO;
	} 
	else
	{
		if (!_strcmpi(decimalLoc.c_str(),""))
		{
			return inPrice;
		} 
		else 
		{
			double idx = atof(decimalLoc.c_str());
			double price = atof(inPrice.c_str());

			std::ostringstream os;
			os << double(price / pow(10, idx));
			std::string tmpPrice = os.str();

			int dicl = tmpPrice.find(DOT);
			switch (tmpPrice.substr(dicl + 1).length()) {
			case (1):
				return tmpPrice + SIXZERO;
			case (2):
				return tmpPrice + FIVEZERO;
			case (3):
				return tmpPrice + FOURZERO;
			case (4):
				return tmpPrice + THREEZERO;
			case (5):
				return tmpPrice + TWOZERO;
			case (6):
				return tmpPrice + ONEZERO;
			default:
				return tmpPrice;
			}

		}
	}
}


bool CME_TNS_DECODE::dirExists(const std::string& dirName_in)
{
	DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
	if (ftyp == INVALID_FILE_ATTRIBUTES)
		return false;  //something is wrong with your path!

	if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
		return true;   // this is a directory!

	return false;    // this is not a directory!
}
 
void CME_TNS_DECODE::createZip()
{

	mapTNSSubscriptionDtl::iterator it1 = m_TNSSubMap.begin();
	int iZipCount =0;
	for(;it1!=m_TNSSubMap.end();it1++)
	{
		std::map<CString,stSubOutPutFile>::iterator itFile =  it1->second.m_mapOutputFiles.begin();
		std::map<CString,stSubOutPutFile>::iterator itFileEnd =  it1->second.m_mapOutputFiles.end();
		for( ;itFile!=itFileEnd; itFile++)
		{
			for(unsigned int i =0;i < itFile->second._mVecFileListForSplit.size();i++)
			{

				CString csOutputGZipFile;
				csOutputGZipFile.Format("%s.gz", itFile->second._mVecFileListForSplit[i].GetBuffer());
				CString csCommand;
				csCommand.Format("gzip.exe -1 -c \"%s\" > \"%s\"", itFile->second._mVecFileListForSplit[i].GetBuffer(), csOutputGZipFile);

				int	iValue = system(csCommand);

				if( iValue == 0 )
				{
					CString csLog;
					csLog.Format("Success:: Gzip Created %s successfully.",csOutputGZipFile);
					fnAddMessage(csLog);
					RequestExternalUpdate();
				}
				else 
				{
					CString csLog;
					csLog.Format("Error:: cmd %s ",csCommand);
					fnAddMessage(csLog);
					RequestExternalUpdate();
				}
			}
		}
	}
	fnAddMessage("Process Subscription complete.");
	RequestExternalUpdate();
}


void CME_TNS_DECODE::fnInsertDeliveryReport(CString csPrdouct, CString csDestAccount, CString csFileName, unsigned long &lFileSize, CString &csAwscmd )
{
	CString csCheckSum = "";
	CString csExchange = "";

	CString csDeliveryTime;
	COleDateTime oleDateTime = COleDateTime::GetCurrentTime();
	csDeliveryTime.Format("%02d-%02d-%02d %02d:%02d", oleDateTime.GetYear(), oleDateTime.GetMonth(), oleDateTime.GetDay(), oleDateTime.GetHour(), oleDateTime.GetMinute());

	CString csSql;
	csSql.Format("EXEC Proc_Beast_Submit_CME_Daily_Delivery_Report '%s', '%s', '%s', %u, '%s','%s', '%s', '%s'", csPrdouct, csDestAccount, csFileName, lFileSize, csCheckSum, csDeliveryTime, csExchange, csAwscmd);

	_RecordsetPtr set;
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)csSql, true)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSql);
		PrintRawMessage(csSql);
		return;
	}
}


void CME_TNS_DECODE::get_all_files_names_within_folder(std::string & folder, std::string & csExch)
{
	char search_path[200];
    sprintf(search_path, "%s/*.*", folder.c_str());
    WIN32_FIND_DATA fd; 
    HANDLE hFind = ::FindFirstFile(search_path, &fd); 
    if(hFind != INVALID_HANDLE_VALUE) { 
        do { 
            // read all (real) files in current folder
            // , delete '!' read other 2 default folder . and ..
            if(! (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ) 
			{
					CString csFile(fd.cFileName);
					if((csFile.Find(".ZIP")!=-1) && csFile.Find(csExch.c_str())!=-1)
					{
						std::string strFile = folder + fd.cFileName;
						if (std::find(m_vecDownloadedZipfiles.begin(), m_vecDownloadedZipfiles.end(), CString(strFile.c_str()))!=m_vecDownloadedZipfiles.end() )
						{				
							continue; // we already downloaded this file skipping it.
						   fnAddMessage("SKIP FILE: " + csFile);

						}
						else
						{
							m_vecDownloadedZipfiles.push_back(CString(strFile.c_str()));
							fnAddMessage("Downloaded: " + CString(strFile.c_str()));
						}
					}
				
            }
        }while(::FindNextFile(hFind, &fd)); 
        ::FindClose(hFind); 
    } 
 
}



void CME_TNS_DECODE::DisplaySubscription()
{	
	int iRow = 0;
	for(unsigned int iIndex = m_curTopRowDis; iIndex < m_vecvecDisplaySub.size(); iIndex++, iRow++)
	{
		if( iRow >= m_numofRowsDis )
			break;
		
		TNSDisplaySubDtl &clBlockSub= m_vecvecDisplaySub[iIndex];

		GetField(e_SubscrID		+ iRow)->SetValueInt(clBlockSub.iOrderId);  
		GetField(e_CUST_NAME	+ iRow)->SetValueString(clBlockSub.stCUST_NAME);  

		GetField(e_CONTACT_EMAIL + iRow)->SetValueString(clBlockSub.stDEST_ACCOUNT);  
		GetField(e_DEST_AC_FTP_PASSWORD + iRow)->SetValueString(clBlockSub.stDEST_ACCOUNT);  
		GetField(e_VENUE_CODE	+ iRow)->SetValueString(clBlockSub.stVENUE_CODE);
		GetField(e_ALL_PRODUCTS + iRow)->SetValueString(clBlockSub.stALL_PRODUCTS);
		GetField(e_TNS_CODE	+ iRow)->SetValueString(clBlockSub.stTNS_CODE);
		GetField(e_EXCH_CODE	+ iRow)->SetValueString(clBlockSub.stEXCH_CODE);
		GetField(e_FOI_CODE	+ iRow)->SetValueString(clBlockSub.stFOI);
		GetField(e_OutputFileName + iRow)->SetValueString(clBlockSub.stOutputFileName);	
		if(clBlockSub.stOutputFileName.GetLength() > 0)
			GetField(e_Upload + iRow)->SetEnabled(true);
		else
			GetField(e_Upload + iRow)->SetEnabled(false);
		
	}

	while( iRow < m_numofRowsDis )
	{
		GetField(e_SubscrID		+ iRow)->SetBlankState();  
		GetField(e_CUST_NAME	+ iRow)->SetBlankState();  

		GetField(e_CONTACT_EMAIL + iRow)->SetBlankState();  
		GetField(e_DEST_AC_FTP_PASSWORD + iRow)->SetBlankState();  
		GetField(e_VENUE_CODE	+ iRow)->SetBlankState();  
		GetField(e_ALL_PRODUCTS + iRow)->SetBlankState();    
		GetField(e_TNS_CODE	+ iRow)->SetBlankState();  
		GetField(e_EXCH_CODE	+ iRow)->SetBlankState(); 
		GetField(e_FOI_CODE	+ iRow)->SetBlankState();
		GetField(e_OutputFileName + iRow)->SetBlankState();  
		GetField(e_OutputFileName + iRow)->SetBackColor(ColorManager::eDefault);
		GetField(e_Upload + iRow)->SetEnabled( false );
		iRow++;
	}
}

void CME_TNS_DECODE::fnInsertErroReport(CString csPrdouct, CString csDestAccount, CString csFileName, CString csExchnage, CString &csErrorCmd )
{	
	CString csSql;
	csSql.Format("EXEC Proc_Beast_Submit_CME_Daily_Error_Report '%s', '%s', '%s', '%s', '%s'", csPrdouct, csDestAccount, csFileName, csExchnage, csErrorCmd);

	_RecordsetPtr set;
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)csSql, true)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSql);
		PrintRawMessage(csSql);
		return;
	}
}

//void CME_TNS_DECODE::createZip()
//{
//	mapTNSSubscriptionDtl::iterator it1 = m_TNSSubMap.begin();
//	int iZipCount =0;
//	for(;it1!=m_TNSSubMap.end();it1++)
//	{
//		CString strZipFile;
//		strZipFile.Format("%s\\%s\\%s.ZIP",GetField(e_OutputPath)->GetValueString(),it1->second.stDEST_ACCOUNT,m_csProcessDate);
//		iZipCount =0;
//		HZIP hz = CreateZip(strZipFile.GetBuffer(),0);
//		ZRESULT res;
//
//		std::map<CString,stSubOutPutFile>::iterator itFile =  it1->second.m_mapOutputFiles.begin();
//		std::map<CString,stSubOutPutFile>::iterator itFileEnd =  it1->second.m_mapOutputFiles.end();
//		for( ;itFile!=itFileEnd; itFile++)
//		{
//			for(unsigned int i =0;i < itFile->second._mVecFileListForSplit.size();i++)
//			{
//				int iIndex = itFile->second._mVecFileListForSplit[i].ReverseFind('\\');
//				CString fileName = itFile->second._mVecFileListForSplit[i].Mid(iIndex);
//				res = 	ZipAdd(hz,fileName.GetBuffer(), itFile->second._mVecFileListForSplit[i].GetBuffer());	
//				iZipCount++;  
//				//added
//			}
//		}
//		CloseZip(hz);
//		CString csLog;
//		if(iZipCount> 0)
//		{
//			csLog.Format("Success:: Zip Created %s successfully.",strZipFile);
//			fnAddMessage(csLog);
//			RequestExternalUpdate();
//		}
//		else
//		{
//			std::remove(strZipFile.GetBuffer());
//		}
//	}
//}

/*std::string CME_TNS_DECODE::formatTime(std::string  trdtime)
{
	switch (trdtime.length()) {
	case (1):
		return ONEDIGETTIME + trdtime;
	case (2):
		return TWODIGETTIME + trdtime;
	case (3):
		return THREEDIGETTIME + trdtime.substr(0, 1) + COL
			+ trdtime.substr(1);
	case (4):
		return FOURDIGETTIME + trdtime.substr(0, 2) + COL
			+ trdtime.substr(2);
	case (5):
		return FIVEDIGETTIME + trdtime.substr(0, 1) + COL
			+ trdtime.substr(1, 3) + COL + trdtime.substr(3);
	default:
		return trdtime.substr(0, 2) + COL + trdtime.substr(2, 4)
			+ COL + trdtime.substr(4, 6);

	}
}
*/
/*
void CME_TNS_DECODE::fnUpdateScheduleStatus( int iStatus )
{
	CString csdttime;
	csdttime.Format("%d-%02d-%02d %02d:%02d:00",m_clScheduleInfo.clScheduleDtTime.GetYear(), m_clScheduleInfo.clScheduleDtTime.GetMonth(), m_clScheduleInfo.clScheduleDtTime.GetDay(), m_clScheduleInfo.clScheduleDtTime.GetHour(), m_clScheduleInfo.clScheduleDtTime.GetMinute());

	COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
	CString csTimeStatus;
	csTimeStatus.Format("%d-%02d-%02d %02d:%02d:00",dtCurrentTime.GetYear(), dtCurrentTime.GetMonth(), dtCurrentTime.GetDay(), dtCurrentTime.GetHour(), dtCurrentTime.GetMinute());

	CString sql;
	if( iStatus == 1 )
		sql.Format("UPDATE dbo.CME_Subscription_Schedule SET Status = %d, ProcessStart = '%s' WHERE FileType ='TICK' and SubType = '%s' and ScheduleDateTime = '%s'", iStatus, csTimeStatus, m_clScheduleInfo.csSubType, csdttime);
	else
		sql.Format("UPDATE dbo.CME_Subscription_Schedule SET Status = %d,   ProcessEnd = '%s' WHERE FileType ='TICK' and SubType = '%s' and ScheduleDateTime = '%s'", iStatus, csTimeStatus, m_clScheduleInfo.csSubType, csdttime);

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		GetField(e_NextDownloadMsg)->SetTitle("Error while Loading Schedule");
		return;
	}
}
*/
