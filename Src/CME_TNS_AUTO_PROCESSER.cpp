// Copyright (C) 2002 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#include "stdafx.h"
#include "CME_TNS_AUTO_PROCESSER.hpp"
#include "BBOConstants.h"
#include <fields/ListField.hpp>
#include "TNS.h"
#include <time.h>
#include "unzip.h"
#include "zip.h"
#include <algorithm>
#include <util\ColorManager.hpp>
#include <MailAutomation/MailAutomation.h>



BF_NAMESPACE_USE

IMPLEMENT_IMAGE(CME_TNS_AUTO_PROCESSER)	


const int		__niMsgCouunt = 500;
CString __csMsgArry[__niMsgCouunt] = { "" };


static UINT TNSProcesser(LPVOID lparam)
{

	CME_TNS_AUTO_PROCESSER *pParser = (CME_TNS_AUTO_PROCESSER*)lparam;

	pParser->fnAddMessage("TNSProcesser called ");

	pParser->m_bIsThreadRunning = true;
	pParser->m_bIsTrheadCompleted = false;

	pParser->fnUpdateScheduleStatus( 1 );

	pParser->ProcessTNS();
	pParser->RequestExternalUpdate();

	pParser->createZip();
	pParser->RequestExternalUpdate();
	
	pParser->UploadSubscription();
	pParser->RequestExternalUpdate();

	pParser->fnUpdateScheduleStatus( 2 );
	
	pParser->m_enmOperation = OP_ProcessCompleted;
	pParser->m_clScheduleInfo.bValid = false;

	pParser->m_bIsThreadRunning = false;
	pParser->RequestExternalUpdate();

	return 0;
}


bool CME_TNS_AUTO_PROCESSER::InitData()
{
	GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));

	m_curTopRow= 0;
	for(m_numofRows = 0; GetField(e_LogMsg + m_numofRows); m_numofRows++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","UAT-RDS");
	csServerName = strServerName.get_value().c_str();


	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");

	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();

	if( !IsRestoreUpdate( ) )
	{
		BF_Reg_Key_Ptr pKey1(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\CME"));

		int iFTPLocation;
		RegDWORD strServerName(pKey1, "S3FTPLocation",0);
		iFTPLocation = strServerName.get_value();
		GetField(e_S3FTPBucket)->SetValueInt( iFTPLocation );

		int iInputLocation;
		RegDWORD strS3InPutLocation(pKey1, "S3InputLocation",2);
		iInputLocation = strS3InPutLocation.get_value();
		GetField(e_S3CMEBucket)->SetValueInt( iInputLocation );

		COleDateTime dtToday = COleDateTime::GetCurrentTime();
		BFDate bfToday;
		bfToday.SetYearMonthDate(dtToday.GetYear(),dtToday.GetMonth() - 1, dtToday.GetDay() );
		GetField(e_DateFilter)->SetValueDate(bfToday); 
	}

	fld_OutputPath = GetField(e_OutputPath);

	m_clScheduleInfo.bValid = false;
	m_clScheduleInfo.iRetInterval = GetField(e_RtryInterval)->GetValueInt() > 0 ? GetField(e_RtryInterval)->GetValueInt(): 10;

	m_bIsThreadRunning = false;
	m_bIsTrheadCompleted = true;
	
	m_enmOperation = OP_None;
	return true;
} 

void CME_TNS_AUTO_PROCESSER::fnAddMessage( CString csaNewMsg )
{	
	for( int iI = __niMsgCouunt-1; iI > 0; iI-- )
		__csMsgArry[iI] = __csMsgArry[iI -1];
	
	CString csMsg;
	
	csMsg.Format("(%s) %s",currentDateTime().c_str(),csaNewMsg);
	
	__csMsgArry[0] = csMsg;	
}

void CME_TNS_AUTO_PROCESSER::fnDisplayMsg()
{
	int iRow = 0;
	for( int iIndex = m_curTopRow; iIndex < __niMsgCouunt; iIndex++, iRow++ )
	{
		if( iRow >= m_numofRows )
			break;

		BaseField * fldComment		= GetField(e_LogMsg + iRow);

		if(__csMsgArry[iIndex].Find("Success")!=-1 || __csMsgArry[iIndex].Find("success")!=-1)
			fldComment->SetForeColor(ColorManager::eFrameGreen);
		else if(__csMsgArry[iIndex].Find("Error")!=-1 || __csMsgArry[iIndex].Find("error")!=-1)
			fldComment->SetForeColor(ColorManager::eFrameRed);
		else
			fldComment->SetForeColor(ColorManager::eFrameWhite) ;



		fldComment->SetValueString( __csMsgArry[iIndex] );
		fldComment->SetNotManual();
	}

	while( iRow < m_numofRows )
	{
		BaseField * fldComment		= GetField(e_LogMsg + iRow);
		fldComment->SetBlankState();
		iRow++;
	}
}

void CME_TNS_AUTO_PROCESSER::Recalculate()
{

	if( IsFieldNew(e_RtryInterval) )
		m_clScheduleInfo.iRetInterval = GetField(e_RtryInterval)->GetValueInt() > 0 ? GetField(e_RtryInterval)->GetValueInt(): 10;

	if( m_bIsThreadRunning )
	{
		GetField(e_MsgTitle)->SetTitle(m_csMsg);		
	}
	else
	{
		if( !m_bIsThreadRunning && !m_bIsTrheadCompleted )
		{
			// After completion of thread
			GetField(e_MsgTitle)->SetTitle("Message");
			m_bIsTrheadCompleted = true;
		}

		if( !m_clScheduleInfo.bValid || m_enmOperation == OP_ProcessCompleted || IsFieldNew(e_ReloadSchedule) )
		{		
			fnGetNextSchedule( );
		}

		if(!m_clScheduleInfo.bValid )	
		{
			StartRecalcTimer(60, false);
			return;
		}

		if( m_enmOperation == OP_Process || IsFieldNew(e_Process) )
		{
			m_enmOperation = OP_ReadThread;

			if( m_enmOperation == OP_ReadThread )
			{
				AfxBeginThread(TNSProcesser, this);
			}
		}

		if( m_enmOperation == OP_Uncompress )
		{
			fnAddMessage("Started Decompressing files.");
			decompressAllFiles();
			fnAddMessage("Decompressing of files complete.");
			
		}

		if( m_enmOperation == OP_Download )
		{
			fnAddMessage("starting Download from s3");
			fnCopyS3FiletoLocal( );
			fnAddMessage("Download from s3 complete.");
		}

		if( m_enmOperation == OP_None )
			fnRecalc1( );

		if( m_enmOperation != OP_None && m_enmOperation != OP_ReadThread )
			StartRecalcTimer(0, false);
	}

	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_pgUP,e_pgDN,200,m_numofRows,m_curTopRow);	
	fnDisplayMsg();	
}

void CME_TNS_AUTO_PROCESSER::fnRecalc1( )
{
	COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
	long lRecalcTime = 0;

	if( GetField( e_ManualRun )->GetValueInt() == 1 )
	{
		GetField(e_NextDownloadMsg)->SetTitle("Processing Set To Manaul");
	}
	else if( dtCurrentTime >= m_clScheduleInfo.clScheduleDtTime )
	{
		//Need to Process subscription...

		LoadSubscription();
		fnAddMessage("Subscription loaded successfully.");

		GetField(e_NextDownloadMsg)->SetTitle("Processing File");
		m_enmOperation = OP_Download;		
		fnAddMessage("Downloading File");
	}
	else if( dtCurrentTime < m_clScheduleInfo.clScheduleDtTime )
	{
		COleDateTimeSpan dtSpan = m_clScheduleInfo.clScheduleDtTime - dtCurrentTime;
		lRecalcTime =(long) dtSpan.GetTotalSeconds();

		CString csTimer,csTmp;

		int iDays =	dtSpan.GetDays();
		int iLeftTotalHr = dtSpan.GetHours();
		int iLeftTotalMinute = dtSpan.GetMinutes();
		int iLeftTotalSecond = dtSpan.GetSeconds();

		if( iDays > 0 )
		{
			csTmp.Format("%d Day and ",iDays);					
			csTimer += csTmp;

			csTmp.Format("%02d:",iLeftTotalHr);					
			csTimer += csTmp;
		}
		else
		{
			if( iLeftTotalHr > 0 )
			{					
				csTmp.Format("%02d:",iLeftTotalHr);					
				csTimer += csTmp;
			}
		}

		csTmp.Format("%02d:",iLeftTotalMinute);					
		csTimer += csTmp;

		csTmp.Format("%02d",iLeftTotalSecond);
		csTimer += csTmp;				

		int iDate	= m_clScheduleInfo.clScheduleDtTime.GetDay();
		int iMonth	= m_clScheduleInfo.clScheduleDtTime.GetMonth();
		int iYear	= m_clScheduleInfo.clScheduleDtTime.GetYear();
		int iHour	= m_clScheduleInfo.clScheduleDtTime.GetHour();
		int iMinute = m_clScheduleInfo.clScheduleDtTime.GetMinute();
		int iSecond = m_clScheduleInfo.clScheduleDtTime.GetSecond();

		CString csStr;
		csStr.Format("Next Download: %d-%d-%d %2d:%2d:%2d [Timer: %s]",iDate,iMonth,iYear,iHour,iMinute,iSecond, csTimer);

		GetField(e_NextDownloadMsg)->SetTitle(csStr);
		GetField(e_NextDownloadMsg)->SetTitleForeColor(ColorManager::eSignalPositiveLight);

		if( lRecalcTime > 4 || lRecalcTime < 0 )
			lRecalcTime = 4;
	}

	if( lRecalcTime )
		StartRecalcTimer(lRecalcTime, false);
}


void CME_TNS_AUTO_PROCESSER::parseFile(TNSSubscriptionDtl & objSubDtl ,CString & csFile,CString & csExchange)
{
	try
	{
		CString stddir;
		stddir.Format("%s\\%s",GetField(e_OutputPath)->GetValueString(),objSubDtl.stDEST_ACCOUNT);

		if(!dirExists(std::string(stddir.GetBuffer())))
			CreateDirectory(stddir.GetBuffer(), NULL);

		CString csTXTFile = csFile;
		csTXTFile.Replace(".ZIP",".txt");
		CString csLog;
		csLog.Format("Processing file: %s",csTXTFile);
		fnAddMessage(csLog);

		std::ifstream fpInputFile;
		fpInputFile.open(csTXTFile.GetBuffer());
		
		std::string strLine;	
		while(!fpInputFile.eof())
		{
			getline(fpInputFile,strLine);
			if (strLine.length() == BBO_STANDARD_LENGH || strLine.length() == BBO_OLD_LENGTH) 
			{
				std::string strSessionInd = strLine.substr(22, 1);
				std::string strAskBidType = strLine.substr(52, 1);
				std::string strIndQuote = strLine.substr(53, 1);
				std::string strMktQuote = strLine.substr(54, 1);
				std::string strCloseOpenType = strLine.substr(55, 1);

				if((strAskBidType.compare("A")!= 0) && (strAskBidType.compare("B")!= 0) && (strIndQuote.compare("I")!= 0) && (strMktQuote.compare("M")!= 0) && (strCloseOpenType.compare("B")!= 0) )
				{
					std::string strout="";

					ConvertRecord(strout,std::string(csExchange.GetBuffer()),strLine);

					std::map<CString,stSubOutPutFile>::iterator itFile = objSubDtl.m_mapOutputFiles.find(csExchange);
					if(itFile!=objSubDtl.m_mapOutputFiles.end())
					{
						/*if((itFile->second.iLineCount % 50000) == 0)
						{
							itFile->second.iFileCount = itFile->second.iFileCount + 1;

							CString tmpOutputFile;
							long lUniq = generateUniqueNo(objSubDtl.stDEST_ACCOUNT,csExchange,m_clScheduleInfo.csDate);
							CString tmpExch= csExchange;
							tmpExch.Replace("X","");
							tmpOutputFile.Format("%s\\TICK_%s_%s_%ld_%02d.csv",stddir,tmpExch,m_clScheduleInfo.csDate,lUniq,itFile->second.iFileCount);
							itFile->second.csFileName = tmpOutputFile;

							fclose(itFile->second.fp);
							itFile->second.fp = NULL;

							itFile->second.bIsFileOpen = false;

							if( fopen_s(&itFile->second.fp, itFile->second.csFileName,"w") )
								return;

							itFile->second.bIsFileOpen = true;
							itFile->second._mVecFileListForSplit.push_back(tmpOutputFile);

							fputs("T.Date,T.Time,Sequence,Session Ind,Symbol,C/P/F,Contract Delivery,Volume,Strike Price,T.Price,A/B,IND,MKQ,C/O,VOE,PC,CAN,INS,F/L,CAB,BKI,Entry Date,exch_code\n",itFile->second.fp);

							fputs(strout.c_str(),itFile->second.fp);
							fputs("\n",itFile->second.fp);
							itFile->second.iLineCount++;
						}
						else
						{*/
							fputs(strout.c_str(),itFile->second.fp);
							fputs("\n",itFile->second.fp);
							itFile->second.iLineCount++;
						//}
					}
					else
					{
						//create new file
						
						stSubOutPutFile _objstSubOutPutFile;
						_objstSubOutPutFile.csExch = csExchange;
						_objstSubOutPutFile.iFileCount = 0;
						_objstSubOutPutFile.iLineCount = 1;
						_objstSubOutPutFile.fp=NULL;

						CString tmpOutputFile;
						long lUniq = generateUniqueNo(objSubDtl.stDEST_ACCOUNT,csExchange,m_clScheduleInfo.csDate);
						CString tmpExch= csExchange;
						tmpExch.Replace("X","");
						tmpOutputFile.Format("%s\\TICK_%s_%s_%ld_%02d.csv",stddir,tmpExch,m_clScheduleInfo.csDate,lUniq,_objstSubOutPutFile.iFileCount);
						
						_objstSubOutPutFile.csFileName = tmpOutputFile;
						_objstSubOutPutFile._mVecFileListForSplit.push_back(tmpOutputFile);
						
						
						if( fopen_s(&_objstSubOutPutFile.fp, _objstSubOutPutFile.csFileName,"w") )
						{
							return;
						}
					
						_objstSubOutPutFile.bIsFileOpen =true;
						//_objstSubOutPutFile._mVecFileListForSplit.push_back(_objstSubOutPutFile.csFileName);
						//fputs("T.Date,T.Time,Sequence,Session Ind,Symbol,C/P/F,Contract Delivery,Volume,Strike Price,T.Price,A/B,IND,MKQ,C/O,VOE,PC,CAN,INS,F/L,CAB,BKI,Entry Date,exch_code\n",_objstSubOutPutFile.fp);
						fputs(strout.c_str(),_objstSubOutPutFile.fp);
						fputs("\n",_objstSubOutPutFile.fp);
						objSubDtl.m_mapOutputFiles.insert(std::pair<CString,stSubOutPutFile>(csExchange,_objstSubOutPutFile));
						
					}
										/*fputs(strout.c_str(),objSubDtl.fp);
								fputs("\n",objSubDtl.fp);
								objSubDtl._mlCount++;
								
								if((objSubDtl._mlCount % 50000) == 0)
								{
									fclose(objSubDtl.fp);
				
									objSubDtl._miFileCount = objSubDtl._miFileCount + 1;
									objSubDtl.stOutputFileName.Format("%s\\%s_%d.csv",stddir,m_csProcessDate,objSubDtl._miFileCount);
					
									if( fopen_s(&objSubDtl.fp, objSubDtl.stOutputFileName,"w") )
										return;

									objSubDtl._vecFileList.push_back(objSubDtl.stOutputFileName);
									fputs("T.Date,T.Time,Sequence,Session Ind,Symbol,C/P/F,Contract Delivery,Volume,Strike Price,T.Price,A/B,IND,MKQ,C/O,VOE,PC,CAN,INS,F/L,CAB,BKI,Entry Date,exch_code\n",objSubDtl.fp);
									fputs(strout.c_str(),objSubDtl.fp);
									fputs("\n",objSubDtl.fp);
									objSubDtl._mlCount++;
								}*/
				}
			}
		}
		
		fpInputFile.close();
	}
	catch (CMemoryException* e)
	{
		e->Delete();
	}
	catch (CFileException* e)
	{
		e->Delete();
	}
	catch (CException* e)
	{
		e->Delete();
	}
	
}

void CME_TNS_AUTO_PROCESSER::ProcessTNS()
{
	mapTNSSubscriptionDtl::iterator it = m_TNSSubMap.begin();
	for(;it!=m_TNSSubMap.end();it++)
	{
		CString csLog;
		csLog.Format("Processing Subscription for %s ",it->second.stDEST_ACCOUNT);
		fnAddMessage(csLog);

			CString csFile;
			for(unsigned int i=0;i<it->second._mVecExchProdCodes.size();i++)
			{
				//Take one Subscription from file
				if(it->second._mVecExchProdCodes[i].csALLPRODUCT == "1")
				{
					for(unsigned int j =0; j<m_vecDownloadedZipfiles.size();j++)
					{
						if(m_vecDownloadedZipfiles[j].Find(it->second._mVecExchProdCodes[i].csExch.GetBuffer())!=-1)
						{
							parseFile(it->second,m_vecDownloadedZipfiles[j],it->second._mVecExchProdCodes[i].csExch);
						}
					}

				}
				else
				{
				
					if(it->second._mVecExchProdCodes[i].csVENUE == "X")
					{
						csFile.Format("%s_%s_%s_%s.ZIP",it->second._mVecExchProdCodes[i].csExch,it->second._mVecExchProdCodes[i].csTNS,it->second._mVecExchProdCodes[i].csFUTOPT,m_clScheduleInfo.csDate.Right(m_clScheduleInfo.csDate.GetLength()-2).GetBuffer());		
						for(unsigned int j =0; j<m_vecDownloadedZipfiles.size();j++)
						{
							if((m_vecDownloadedZipfiles[j].Find(csFile.GetBuffer())!=-1))
							{
								parseFile(it->second,m_vecDownloadedZipfiles[j],it->second._mVecExchProdCodes[i].csExch);
							}
						}
					}
					else if(it->second._mVecExchProdCodes[i].csVENUE == "F")
					{
						csFile.Format("%s_%s_%s_%s.ZIP",it->second._mVecExchProdCodes[i].csExch,it->second._mVecExchProdCodes[i].csTNS,it->second._mVecExchProdCodes[i].csFUTOPT,m_clScheduleInfo.csDate.Right(m_clScheduleInfo.csDate.GetLength()-2).GetBuffer());		
						for(unsigned int j =0; j<m_vecDownloadedZipfiles.size();j++)
						{
							if((m_vecDownloadedZipfiles[j].Find(csFile.GetBuffer())!=-1))
							{
								if(m_vecDownloadedZipfiles[j].Find("openoutcry")!=-1)
								{
									parseFile(it->second,m_vecDownloadedZipfiles[j],it->second._mVecExchProdCodes[i].csExch);
								}
							}
						}
					}
					else if(it->second._mVecExchProdCodes[i].csVENUE == "E")
					{
						csFile.Format("%s_%s_%s_%s.ZIP",it->second._mVecExchProdCodes[i].csExch,it->second._mVecExchProdCodes[i].csTNS,it->second._mVecExchProdCodes[i].csFUTOPT,m_clScheduleInfo.csDate.Right(m_clScheduleInfo.csDate.GetLength()-2).GetBuffer());		
						for(unsigned int j =0; j<m_vecDownloadedZipfiles.size();j++)
						{
							if((m_vecDownloadedZipfiles[j].Find(csFile.GetBuffer())!=-1))
							{
								if(m_vecDownloadedZipfiles[j].Find("globex")!=-1)
								{
									parseFile(it->second,m_vecDownloadedZipfiles[j],it->second._mVecExchProdCodes[i].csExch);
								}
							}
						}
					}
				}
			}
	}

	mapTNSSubscriptionDtl::iterator it1 = m_TNSSubMap.begin();
	for(;it1!=m_TNSSubMap.end();it1++)
	{

		std::map<CString,stSubOutPutFile>::iterator itFile =  it1->second.m_mapOutputFiles.begin();
		std::map<CString,stSubOutPutFile>::iterator itFileEnd =  it1->second.m_mapOutputFiles.end();
		for( ;itFile!=itFileEnd; itFile++)
		{
			if(itFile->second.fp!=NULL)
			{
				fclose(itFile->second.fp);
				itFile->second.fp=NULL;
			}
		}
	}
}

void CME_TNS_AUTO_PROCESSER::fnGetNextSchedule( )
{
	m_clScheduleInfo.bValid = false;
	m_enmOperation = OP_None;

	BFDate bfdt = GetField(e_DateFilter)->GetValueDate();

	CString csStrDtFilter;
	csStrDtFilter.Format("%d-%02d-%02d 00:00:00",bfdt.GetYear(), bfdt.GetMonth()+1, bfdt.GetDate());

	CString sql;
	sql.Format("SELECT top 1* from dbo.CME_Subscription_Schedule WHERE FileType ='TICK' and IsNull(Status,0) In(0,1) and ScheduleDateTime > '%s' ORDER BY ScheduleDateTime", csStrDtFilter);

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		GetField(e_NextDownloadMsg)->SetTitle("Error while Loading Schedule");
		return;
	}

	if( VARIANT_FALSE == set->adoEOF )
	{
		SETSTR(	m_clScheduleInfo.csSubType,		set->Fields->Item[_variant_t("SubType")]->Value);			
		SETLONG( m_clScheduleInfo.iStatus,		set->Fields->Item[_variant_t("Status")]->Value);	
		SETOLEDATE(	m_clScheduleInfo.clScheduleDtTime,	set->Fields->Item[_variant_t("ScheduleDateTime")]->Value);	

		m_clScheduleInfo.clMainScheduleDtTime = m_clScheduleInfo.clScheduleDtTime;

		m_clScheduleInfo.iRetryCount = 0;
		m_clScheduleInfo.csDate.Format("%d%02d%02d", m_clScheduleInfo.clScheduleDtTime.GetYear(), m_clScheduleInfo.clScheduleDtTime.GetMonth(), m_clScheduleInfo.clScheduleDtTime.GetDay() );

		m_clScheduleInfo.bValid = true;
	}

	set->Close();

	if( m_clScheduleInfo.bValid  )
	{
		GetField( e_Year )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetYear() );
		GetField( e_Month )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetMonth() );
		GetField( e_Day )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetDay() );
		GetField( e_Hour )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetHour() );
		GetField( e_Minute )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetMinute() );
	}
	else
	{
		GetField(e_NextDownloadMsg)->SetTitle("No Schedule Loaded");
	}
}

void CME_TNS_AUTO_PROCESSER::LoadSubscription()
{
	m_TNSSubMap.clear(); 
	
	CString sql;	
	sql.Format("SELECT CUST_NAME, CONTACT_EMAIL, DEST_ACCOUNT, FTP_PASSWORD, VENUE_CODE, ALL_PRODUCTS, PROD_CODE, BBO_CODE,TICK_CODE, EXCH_CODE ,FOI_CODE FROM dbo.subscription_2015 \
			   where FILE_TYPE = 'TICK' \
			   and Active = 1\
			   and ALL_PRODUCTS = 0\
			   and BBO_CODE <> 'null'\
			   and DEST_ACCOUNT <> 'ftp_beast'\
			   order by DEST_ACCOUNT");

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		ASSERT(0);
		return;
	}
	
	
	while(VARIANT_FALSE == set->adoEOF)
	{
		TNSSubscriptionDtl clBlockSub;
		CString FOI,csDestAccount;

		SETSTR(	csDestAccount,	set->Fields->Item[_variant_t("DEST_ACCOUNT")]->Value);

		mapTNSSubscriptionDtl::iterator it =  m_TNSSubMap.find(csDestAccount);

		if(it!=m_TNSSubMap.end())
		{
			//Update
			stEXchProd objstEXchProd;

			SETSTR(	objstEXchProd.csVENUE,		set->Fields->Item[_variant_t("VENUE_CODE")]->Value);
			SETSTR(	objstEXchProd.csALLPRODUCT,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);
			SETSTR(	objstEXchProd.csTNS,		set->Fields->Item[_variant_t("BBO_CODE")]->Value);	
			SETSTR(	objstEXchProd.csExch,		set->Fields->Item[_variant_t("EXCH_CODE")]->Value);
			SETSTR(	objstEXchProd.csFUTOPT,		set->Fields->Item[_variant_t("FOI_CODE")]->Value);

				if(objstEXchProd.csVENUE == "E" )
					objstEXchProd.csFileName.Format("globex/%s/%s_%s_%s_%s.ZIP",m_clScheduleInfo.csDate,objstEXchProd.csExch,objstEXchProd.csTNS,objstEXchProd.csFUTOPT,m_clScheduleInfo.csDate.Right(m_clScheduleInfo.csDate.GetLength()-2));
				else if(objstEXchProd.csVENUE == "F")
					objstEXchProd.csFileName.Format("openoutcry/%s/%s_%s_%s_%s.ZIP",m_clScheduleInfo.csDate,objstEXchProd.csExch,objstEXchProd.csTNS,objstEXchProd.csFUTOPT,m_clScheduleInfo.csDate.Right(m_clScheduleInfo.csDate.GetLength()-2));
				else if(objstEXchProd.csVENUE == "X")
					objstEXchProd.csFileName.Format("%s/%s_%s_%s_%s.ZIP",m_clScheduleInfo.csDate,objstEXchProd.csExch,objstEXchProd.csTNS,objstEXchProd.csFUTOPT,m_clScheduleInfo.csDate.Right(m_clScheduleInfo.csDate.GetLength()-2));

				if(!isSubExists(it->second,objstEXchProd))
					it->second._mVecExchProdCodes.push_back(objstEXchProd);
		}
		else
		{
			//create
			TNSSubscriptionDtl clBlockSub;

			SETSTR(	clBlockSub.stCUST_NAME,		set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
			SETSTR(	clBlockSub.stCONTACT_EMAIL,	set->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);	
			SETSTR(	clBlockSub.stDEST_ACCOUNT,	set->Fields->Item[_variant_t("DEST_ACCOUNT")]->Value);	
			SETSTR(	clBlockSub.stFTP_PASSWORD,	set->Fields->Item[_variant_t("FTP_PASSWORD")]->Value);	
			
			stEXchProd objstEXchProd;

			SETSTR(	objstEXchProd.csVENUE,		set->Fields->Item[_variant_t("VENUE_CODE")]->Value);
			SETSTR(	objstEXchProd.csALLPRODUCT,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);
			SETSTR(	objstEXchProd.csTNS,		set->Fields->Item[_variant_t("BBO_CODE")]->Value);	
			SETSTR(	objstEXchProd.csExch,		set->Fields->Item[_variant_t("EXCH_CODE")]->Value);
			SETSTR(	objstEXchProd.csFUTOPT,		set->Fields->Item[_variant_t("FOI_CODE")]->Value);
			

				if(objstEXchProd.csVENUE == "E")
					objstEXchProd.csFileName.Format("globex/%s/%s_%s_%s_%s.ZIP",m_clScheduleInfo.csDate,objstEXchProd.csExch,objstEXchProd.csTNS,objstEXchProd.csFUTOPT,m_clScheduleInfo.csDate.Right(m_clScheduleInfo.csDate.GetLength()-2));
				else if(objstEXchProd.csVENUE == "F")
					objstEXchProd.csFileName.Format("openoutcry/%s/%s_%s_%s_%s.ZIP",m_clScheduleInfo.csDate,objstEXchProd.csExch,objstEXchProd.csTNS,objstEXchProd.csFUTOPT,m_clScheduleInfo.csDate.Right(m_clScheduleInfo.csDate.GetLength()-2));
				else if(objstEXchProd.csVENUE == "X")
					objstEXchProd.csFileName.Format("%s/%s_%s_%s_%s.ZIP",m_clScheduleInfo.csDate,objstEXchProd.csExch,objstEXchProd.csTNS,objstEXchProd.csFUTOPT,m_clScheduleInfo.csDate.Right(m_clScheduleInfo.csDate.GetLength()-2));

			if(!isSubExists(clBlockSub,objstEXchProd))
				clBlockSub._mVecExchProdCodes.push_back(objstEXchProd);

			m_TNSSubMap.insert(std::pair<CString,TNSSubscriptionDtl>(clBlockSub.stDEST_ACCOUNT,clBlockSub));
		}
		set->MoveNext();
	}

	set->Close();	


		
	sql.Format("SELECT CUST_NAME, CONTACT_EMAIL, DEST_ACCOUNT, FTP_PASSWORD, VENUE_CODE, ALL_PRODUCTS, PROD_CODE, BBO_CODE, EXCH_CODE , 'null' as FOI \
				FROM dbo.subscription_2015 \
				where FILE_TYPE = 'TICK' \
				and Active = 1 \
				and ALL_PRODUCTS =1");


	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		ASSERT(0);
		return;
	}
	while(VARIANT_FALSE == set->adoEOF)
	{
		TNSSubscriptionDtl clBlockSub;
		CString FOI,csDestAccount;

		SETSTR(	csDestAccount,	set->Fields->Item[_variant_t("DEST_ACCOUNT")]->Value);

		mapTNSSubscriptionDtl::iterator it =  m_TNSSubMap.find(csDestAccount);
		if(it!=m_TNSSubMap.end())
		{
			stEXchProd objstEXchProd;

			SETSTR(	objstEXchProd.csVENUE,		set->Fields->Item[_variant_t("VENUE_CODE")]->Value);
			SETSTR(	objstEXchProd.csALLPRODUCT,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);
			SETSTR(	objstEXchProd.csTNS,		set->Fields->Item[_variant_t("BBO_CODE")]->Value);	
			SETSTR(	objstEXchProd.csExch,		set->Fields->Item[_variant_t("EXCH_CODE")]->Value);
			SETSTR(	objstEXchProd.csFUTOPT,		set->Fields->Item[_variant_t("FOI_CODE")]->Value);

			objstEXchProd.csFileName.Format("%s_*.ZIP",objstEXchProd.csExch);

			if(!isSubExists(it->second,objstEXchProd))			
				it->second._mVecExchProdCodes.push_back(objstEXchProd);
		}
		else
		{
			//create
			TNSSubscriptionDtl clBlockSub;

			SETSTR(	clBlockSub.stCUST_NAME,		set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
			SETSTR(	clBlockSub.stCONTACT_EMAIL,	set->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);	
			SETSTR(	clBlockSub.stDEST_ACCOUNT,	set->Fields->Item[_variant_t("DEST_ACCOUNT")]->Value);	
			SETSTR(	clBlockSub.stFTP_PASSWORD,	set->Fields->Item[_variant_t("FTP_PASSWORD")]->Value);	
			
			stEXchProd objstEXchProd;

			SETSTR(	objstEXchProd.csVENUE,		set->Fields->Item[_variant_t("VENUE_CODE")]->Value);
			SETSTR(	objstEXchProd.csALLPRODUCT,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);
			SETSTR(	objstEXchProd.csTNS,		set->Fields->Item[_variant_t("BBO_CODE")]->Value);	
			SETSTR(	objstEXchProd.csExch,		set->Fields->Item[_variant_t("EXCH_CODE")]->Value);
			SETSTR(	objstEXchProd.csFUTOPT,		set->Fields->Item[_variant_t("FOI_CODE")]->Value);
			
			objstEXchProd.csFileName.Format("%s_*.ZIP",objstEXchProd.csExch);
			
			if(!isSubExists(clBlockSub,objstEXchProd))
				clBlockSub._mVecExchProdCodes.push_back(objstEXchProd);

			m_TNSSubMap.insert(std::pair<CString,TNSSubscriptionDtl>(clBlockSub.stDEST_ACCOUNT,clBlockSub));
		}
		set->MoveNext();
	}
	set->Close();	
}

void CME_TNS_AUTO_PROCESSER::UploadSubscription()
{
	fnAddMessage("Upload to S3 started.");
	CString csTemp;

	CString __csDestinationPath = ((ListField*)GetField(e_S3FTPBucket))->GetShortString();

	CString __csAccount = ((ListField*)GetField(e_DestAccount))->GetShortString(); // This is Test Account

	CString csProfile = ((ListField*)GetField(e_S3FTPBucket))->GetLongString();

	mapTNSSubscriptionDtl::iterator it = m_TNSSubMap.begin();
	for(;it!=m_TNSSubMap.end();it++)
	{
		std::map<CString,stSubOutPutFile>::iterator itFile =  it->second.m_mapOutputFiles.begin();
		std::map<CString,stSubOutPutFile>::iterator itFileEnd =  it->second.m_mapOutputFiles.end();
		for( ;itFile!=itFileEnd; itFile++)
		{
			for(unsigned int i =0;i < itFile->second._mVecFileListForSplit.size();i++)
			{

				CString csOutputGZipFile;
				csOutputGZipFile.Format("%s.gz", itFile->second._mVecFileListForSplit[i].GetBuffer());

				if(exist(std::string(csOutputGZipFile.GetBuffer())))
				{

					CString csDestAccount,csDestinationFolder;

					if( GetField(e_DestAccount)->GetValueInt() == 1 )
						csDestAccount = it->second.stDEST_ACCOUNT;
					else
						csDestAccount = __csAccount;

					csDestinationFolder.Format("%s%s/TICK/",__csDestinationPath,  csDestAccount );

					CString csAWSCommand;

					csAWSCommand.Format(_T("aws s3 cp \"%s\" --profile \"%s\" \"%s\""),csOutputGZipFile,csProfile, csDestinationFolder);					
					it->second.iUploadReturn = system(csAWSCommand);	

					CString csFileName;// = strSourceZipFile;
					int iIndex = csOutputGZipFile.ReverseFind('\\');
					csFileName = csOutputGZipFile.Mid(iIndex+1);

					if( it->second.iUploadReturn == 0 )
					{
						CString stroutput;
						stroutput.Format("Success:: cmd %s", csAWSCommand );
						fnAddMessage( stroutput );						
						//csFileName.Replace(itr->second.strOutputFolderPath,"");
						//csFileName.Replace("\\","");
						unsigned long lsize = GetFileSize(csOutputGZipFile.GetString());

						fnInsertDeliveryReport("TICK", csDestAccount, csFileName, lsize, csAWSCommand);
					}
					else
					{
						CString stroutput;
						stroutput.Format("Error:: in upload cmd %s", csAWSCommand );
						fnAddMessage( stroutput );

						fnInsertErroReport("TICK",csDestAccount,csFileName,"",csAWSCommand);
					}

					//it->second.bUploaded = true;
				}
			}
		}
	}

	fnSendMail();
	fnAddMessage("Upload to S3 Done.");	
}

void CME_TNS_AUTO_PROCESSER::fnCopyS3FiletoLocal( )
{

	CString strBasePath = fld_OutputPath->GetValueString();
	
	CString csInputBucket = ((ListField*)GetField(e_S3CMEBucket))->GetShortString();
	CString csProfile = ((ListField*)GetField(e_S3CMEBucket))->GetLongString();

	CString strFullInputPath,csAWSCommand;
	CString csDestinationFolder;
	bool bIsGLobex=false;
	bool bIsOpenOut=false;
	int iDownloadCount=0;
	
	m_vecDownloadedZipfiles.clear();
	// GLOBEX

	strFullInputPath.Format("%s/daily/bbo/globex/%s/",csInputBucket,m_clScheduleInfo.csDate);
	csDestinationFolder.Format("%s\\globex\\%s\\",strBasePath, m_clScheduleInfo.csDate);

	if(!dirExists(std::string(csDestinationFolder.GetBuffer())))
		CreateDirectory(csDestinationFolder.GetBuffer(), NULL);

	csAWSCommand.Format(_T("aws s3 sync --profile %s %s %s"),csProfile,strFullInputPath, csDestinationFolder);	
	int iGlobexValue = system(csAWSCommand);	
	if(iGlobexValue == 0) 
	{

		if(fnGetDownloadedFilesCount(std::string(csDestinationFolder.GetBuffer())))
		{
			bIsGLobex =true;
			fnAddMessage("Success:: cmd " + csAWSCommand);
		}
		else
		{
			bIsGLobex = false;
			m_clScheduleInfo.clScheduleDtTime = COleDateTime::GetCurrentTime();;
			m_clScheduleInfo.clScheduleDtTime += COleDateTimeSpan(0,0,10,0);
			m_clScheduleInfo.iRetryCount++;
			m_enmOperation = OP_None;
			fnAddMessage("Error:: cmd " + csAWSCommand);
		}
	}
	else
	{
		bIsGLobex =false;
		m_clScheduleInfo.clScheduleDtTime = COleDateTime::GetCurrentTime();;
		m_clScheduleInfo.clScheduleDtTime += COleDateTimeSpan(0,0,10,0);
		m_clScheduleInfo.iRetryCount++;
		m_enmOperation = OP_None;
		CString csTemp;
		csTemp.Format("Error:: cmd %s", csAWSCommand );
		fnAddMessage( csTemp );
	}

	// OPENOUTCRY
	int iOpenoutCry;
	strFullInputPath.Format("%s/daily/bbo/openoutcry/%s/",csInputBucket,m_clScheduleInfo.csDate);
	csDestinationFolder.Format("%s\\openoutcry\\%s\\",strBasePath, m_clScheduleInfo.csDate);

	if(!dirExists(std::string(csDestinationFolder.GetBuffer())))
		CreateDirectory(csDestinationFolder.GetBuffer(), NULL);

	csAWSCommand.Format(_T("aws s3 sync --profile %s %s %s"),csProfile,strFullInputPath, csDestinationFolder);	
	
	iOpenoutCry = system(csAWSCommand);	

	if(iOpenoutCry == 0) 
	{
		if(fnGetDownloadedFilesCount(std::string(csDestinationFolder.GetBuffer())))
		{
			bIsOpenOut =true;
			fnAddMessage("Success:: cmd " + csAWSCommand);
		}
		else
		{
			bIsOpenOut = false;
			m_clScheduleInfo.clScheduleDtTime = COleDateTime::GetCurrentTime();;
			m_clScheduleInfo.clScheduleDtTime += COleDateTimeSpan(0,0,10,0);
			m_clScheduleInfo.iRetryCount++;
			m_enmOperation = OP_None;
			fnAddMessage("Error:: cmd " + csAWSCommand);
		}
	}
	else
	{
		bIsOpenOut = false;
		m_clScheduleInfo.clScheduleDtTime = COleDateTime::GetCurrentTime();;
		m_clScheduleInfo.clScheduleDtTime += COleDateTimeSpan(0,0,10,0);
		m_clScheduleInfo.iRetryCount++;
		m_enmOperation = OP_None;
		CString csTemp;
		csTemp.Format("Error:: cmd %s", csAWSCommand );
		fnAddMessage( csTemp );
	}


	if(!bIsOpenOut || !bIsGLobex)
	{
		m_enmOperation = OP_None;
		return;
	}


	mapTNSSubscriptionDtl::iterator it =  m_TNSSubMap.begin();
	for(;it!=m_TNSSubMap.end();it++)
	{
		for(unsigned int i=0;i<it->second._mVecExchProdCodes.size();i++)
			{
				if(it->second._mVecExchProdCodes[i].csALLPRODUCT == "1")
				{
					if(bIsGLobex)
					{
						csDestinationFolder.Format("%s\\globex\\%s\\",strBasePath, m_clScheduleInfo.csDate);
						get_all_files_names_within_folder(std::string(csDestinationFolder.GetBuffer()),std::string(it->second._mVecExchProdCodes[i].csExch));
					}
					
					if(bIsOpenOut)
					{
						csDestinationFolder.Format("%s\\openoutcry\\%s\\",strBasePath, m_clScheduleInfo.csDate);
						get_all_files_names_within_folder(std::string(csDestinationFolder.GetBuffer()),std::string(it->second._mVecExchProdCodes[i].csExch));
					}
				}
				else
				{
					if(it->second._mVecExchProdCodes[i].csVENUE == "E")
					{
						if(bIsGLobex)
						{
							csDestinationFolder.Format("%s\\globex\\%s\\",strBasePath, m_clScheduleInfo.csDate);
							CString csFileName ;
							csFileName.Format("%s_%s_%s_%s",it->second._mVecExchProdCodes[i].csExch,it->second._mVecExchProdCodes[i].csTNS,it->second._mVecExchProdCodes[i].csFUTOPT,m_clScheduleInfo.csDate.Right(m_clScheduleInfo.csDate.GetLength()-2));
							get_all_files_names_within_folder(std::string(csDestinationFolder.GetBuffer()),std::string(csFileName));
						}
					}
					else if(it->second._mVecExchProdCodes[i].csVENUE == "F")
					{
						if(bIsOpenOut)
						{
							csDestinationFolder.Format("%s\\openoutcry\\%s\\",strBasePath, m_clScheduleInfo.csDate);
							CString csFileName ;
							csFileName.Format("%s_%s_%s_%s",it->second._mVecExchProdCodes[i].csExch,it->second._mVecExchProdCodes[i].csTNS,it->second._mVecExchProdCodes[i].csFUTOPT,m_clScheduleInfo.csDate.Right(m_clScheduleInfo.csDate.GetLength()-2));
							get_all_files_names_within_folder(std::string(csDestinationFolder.GetBuffer()),std::string(csFileName));
						}
					}
					else if(it->second._mVecExchProdCodes[i].csVENUE == "X")
					{
						if(bIsOpenOut)
						{
							csDestinationFolder.Format("%s\\openoutcry\\%s\\",strBasePath, m_clScheduleInfo.csDate);
							CString csFileName ;
							csFileName.Format("%s_%s_%s_%s",it->second._mVecExchProdCodes[i].csExch,it->second._mVecExchProdCodes[i].csTNS,it->second._mVecExchProdCodes[i].csFUTOPT,m_clScheduleInfo.csDate.Right(m_clScheduleInfo.csDate.GetLength()-2));
							get_all_files_names_within_folder(std::string(csDestinationFolder.GetBuffer()),std::string(csFileName));
						}
						if(bIsGLobex)
						{
							csDestinationFolder.Format("%s\\globex\\%s\\",strBasePath, m_clScheduleInfo.csDate);
							CString csFileName ;
							csFileName.Format("%s_%s_%s_%s",it->second._mVecExchProdCodes[i].csExch,it->second._mVecExchProdCodes[i].csTNS,it->second._mVecExchProdCodes[i].csFUTOPT,m_clScheduleInfo.csDate.Right(m_clScheduleInfo.csDate.GetLength()-2));
							get_all_files_names_within_folder(std::string(csDestinationFolder.GetBuffer()),std::string(csFileName));
						}
					}

				}
			}
		}

	   CString csDestFolder;
	   if(bIsOpenOut)
	   {
			csDestFolder.Format("%s\\openoutcry\\%s\\",strBasePath, m_clScheduleInfo.csDate);
			RemoveUnncessaryFiles(std::string(csDestFolder.GetBuffer()));
	   }
	   if(bIsGLobex)
	   {
		   csDestFolder.Format("%s\\globex\\%s\\",strBasePath, m_clScheduleInfo.csDate);
		   RemoveUnncessaryFiles(std::string(csDestFolder.GetBuffer()));
	   }

	if(bIsOpenOut && bIsGLobex)   
	m_enmOperation = OP_Uncompress;
}


bool CME_TNS_AUTO_PROCESSER::fnGetDownloadedFilesCount(std::string & folder)
{
	int iFileCount =0;
	char search_path[200];
    sprintf(search_path, "%s/*.*", folder.c_str());
    WIN32_FIND_DATA fd; 
    HANDLE hFind = ::FindFirstFile(search_path, &fd); 
    if(hFind != INVALID_HANDLE_VALUE) 
	{ 
        do { 
            // read all (real) files in current folder
            // , delete '!' read other 2 default folder . and ..
            if(! (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ) 
			{
					CString csFile(fd.cFileName);
					if((csFile.Find(".ZIP")!=-1))
					{
						iFileCount++;
					}
				
            }
        }while(::FindNextFile(hFind, &fd)); 
        ::FindClose(hFind); 
    }

	if(iFileCount > 0)
		return true;
	else
		return false;
}


void CME_TNS_AUTO_PROCESSER::RemoveUnncessaryFiles(std::string & folder)
{

	char search_path[200];
    sprintf(search_path, "%s/*.*", folder.c_str());
    WIN32_FIND_DATA fd; 
    HANDLE hFind = ::FindFirstFile(search_path, &fd); 
    if(hFind != INVALID_HANDLE_VALUE) { 
        do { 
            // read all (real) files in current folder
            // , delete '!' read other 2 default folder . and ..
            if(! (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ) 
			{
					CString csFile(fd.cFileName);
					if((csFile.Find(".ZIP")!=-1))
					{
						std::string strFile = folder + fd.cFileName;

						if (std::find(m_vecDownloadedZipfiles.begin(), m_vecDownloadedZipfiles.end(), CString(strFile.c_str()))!=m_vecDownloadedZipfiles.end() )
						{
						   continue; 
						}
						else
						{
							std::remove(strFile.c_str());
						}
					}
				
            }
        }while(::FindNextFile(hFind, &fd)); 
        ::FindClose(hFind); 
    } 
}

void CME_TNS_AUTO_PROCESSER::decompressAllFiles()
{
	for (unsigned int i = 0 ;i<m_vecDownloadedZipfiles.size();i++)
		createUnzip(std::string(m_vecDownloadedZipfiles[i].GetBuffer()));
	m_enmOperation = OP_Process;
 }

void CME_TNS_AUTO_PROCESSER::createUnzip(std::string & sInputFile)
{

	size_t index = sInputFile.find_last_of("\\");
	std::string unzipath = sInputFile.substr(0, index+1);
	std::string strOutputString;

	HZIP hz = OpenZip(sInputFile.c_str(),0);
	ZIPENTRY ze; GetZipItem(hz,-1,&ze); int numitems=ze.index;
	// -1 gives overall information about the zipfile
	for (int zi=0; zi<numitems; zi++)
	{ ZIPENTRY ze; GetZipItem(hz,zi,&ze); // fetch individual details

		strOutputString =  unzipath + ze.name;
		UnzipItem(hz, zi, strOutputString.c_str());         // e.g. the item's name.

	}
	CloseZip(hz);

	CString csOutput;
	csOutput += "Success:: UnZip created: ";
	csOutput += strOutputString.c_str();
	fnAddMessage(csOutput);
	RequestExternalUpdate();
}

void CME_TNS_AUTO_PROCESSER::fnUpdateScheduleStatus( int iStatus )
{
	CString csdttime;
	csdttime.Format("%d-%02d-%02d %02d:%02d:00",m_clScheduleInfo.clMainScheduleDtTime.GetYear(), m_clScheduleInfo.clMainScheduleDtTime.GetMonth(), m_clScheduleInfo.clMainScheduleDtTime.GetDay(), m_clScheduleInfo.clMainScheduleDtTime.GetHour(), m_clScheduleInfo.clMainScheduleDtTime.GetMinute());

	COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
	CString csTimeStatus;
	csTimeStatus.Format("%d-%02d-%02d %02d:%02d:00",dtCurrentTime.GetYear(), dtCurrentTime.GetMonth(), dtCurrentTime.GetDay(), dtCurrentTime.GetHour(), dtCurrentTime.GetMinute());

	CString sql;
	if( iStatus == 1 )
		sql.Format("UPDATE dbo.CME_Subscription_Schedule SET Status = %d, ProcessStart = '%s' WHERE FileType ='TICK' and SubType = '%s' and ScheduleDateTime = '%s'", iStatus, csTimeStatus, m_clScheduleInfo.csSubType, csdttime);
	else
		sql.Format("UPDATE dbo.CME_Subscription_Schedule SET Status = %d,   ProcessEnd = '%s' WHERE FileType ='TICK' and SubType = '%s' and ScheduleDateTime = '%s'", iStatus, csTimeStatus, m_clScheduleInfo.csSubType, csdttime);

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		GetField(e_NextDownloadMsg)->SetTitle("Error while Loading Schedule");
		return;
	}
}

bool CME_TNS_AUTO_PROCESSER::ConvertRecord(std::string & strout,std::string & strExch,std::string & strLine)
{
	strout.append(strLine.substr(0, 8));
	strout.append(DELIM);

	strout.append(formatTime(strLine.substr(8, 6)));
	strout.append(DELIM);

	std::string strNum = strLine.substr(14, 8);
	trim2(strNum);

	strout.append(strNum);
	strout.append(DELIM);

	strout.append(strLine.substr(22, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(23, 3));
	strout.append(DELIM);

	strout.append(strLine.substr(26, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(27, 4));
	strout.append(DELIM);

	std::string strNum1 = strLine.substr(31, 5);
	trim2(strNum1);

	strout.append(strNum1);
	strout.append(DELIM);

	std::string strPrice1 = strLine.substr(36, 7);
	trim2(strPrice1);

	std::string strPrice2 = strLine.substr(43, 1);
	trim2(strPrice2);

	strout.append(formatPrice(strPrice1, strPrice2));
	strout.append(DELIM);

	strPrice1 = strLine.substr(44, 7);
	trim2(strPrice1);

	strPrice2 = strLine.substr(51, 1);
	trim2(strPrice2);

	strout.append(formatPrice(strPrice1, strPrice2));
	strout.append(DELIM);

	strout.append(strLine.substr(52, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(53, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(54, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(55, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(56, 2));
	strout.append(DELIM);

	strout.append(strLine.substr(58, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(59, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(60, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(61, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(62, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(63, 1));
	strout.append(DELIM);

	strout.append(formatEntryDate(strLine));
	strout.append(DELIM);

	CString csExch(strExch.c_str());
	csExch.Replace("X","");
	strout.append(csExch.GetBuffer());
	strout.append(DELIM);

	return true;
}

std::string CME_TNS_AUTO_PROCESSER::formatTime(std::string  trdtime)
{
	switch (trdtime.length()) {
	case (1):
		return ONEDIGETTIME + trdtime;
	case (2):
		return TWODIGETTIME + trdtime;
	case (3):
		return THREEDIGETTIME + trdtime.substr(0, 1) + COL
			+ trdtime.substr(1);
	case (4):
		return FOURDIGETTIME + trdtime.substr(0, 1) + COL
			+ trdtime.substr(2);
	case (5):
		return FIVEDIGETTIME + trdtime.substr(0, 1) + COL
			+ trdtime.substr(1, 2) + COL + trdtime.substr(3);
	default:
		return trdtime.substr(0, 2) + COL + trdtime.substr(2, 2)
			+ COL + trdtime.substr(4, 2);

	}
}

std::string CME_TNS_AUTO_PROCESSER::formatEntryDate(std::string line) 
{
	if (line.length() == 70) 
	{
		std::string strtmp = line.substr(64, 6);
		trim2(strtmp);
		return TWENTY + strtmp;
	} 
	else 
	{
		return EMPTY_ENTRY_DATE;
	}
}

void CME_TNS_AUTO_PROCESSER::trim2(std::string & str)
{
	std::string::size_type pos = str.find_last_not_of(' ');
	if(pos != std::string::npos) {
		str.erase(pos + 1);
		pos = str.find_first_not_of(' ');
		if(pos != std::string::npos) str.erase(0, pos);
	}
	else str.erase(str.begin(), str.end());
}

std::string CME_TNS_AUTO_PROCESSER::formatPrice(std::string inPrice, std::string decimalLoc) 
{
	if (!_strcmpi(inPrice.c_str(),"")) 
	{
		return PRICEZERO;
	} 
	else
	{
		if (!_strcmpi(decimalLoc.c_str(),""))
		{
			return inPrice;
		} 
		else 
		{
			double idx = atof(decimalLoc.c_str());
			double price = atof(inPrice.c_str());

			std::ostringstream os;
			os << double(price / pow(10, idx));
			std::string tmpPrice = os.str();

			int dicl = tmpPrice.find(DOT);
			switch (tmpPrice.substr(dicl + 1).length()) {
			case (1):
				return tmpPrice + SIXZERO;
			case (2):
				return tmpPrice + FIVEZERO;
			case (3):
				return tmpPrice + FOURZERO;
			case (4):
				return tmpPrice + THREEZERO;
			case (5):
				return tmpPrice + TWOZERO;
			case (6):
				return tmpPrice + ONEZERO;
			default:
				return tmpPrice;
			}

		}
	}
}

bool CME_TNS_AUTO_PROCESSER::dirExists(const std::string& dirName_in)
{
	DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
	if (ftyp == INVALID_FILE_ATTRIBUTES)
		return false;  //something is wrong with your path!

	if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
		return true;   // this is a directory!

	return false;    // this is not a directory!
}


void CME_TNS_AUTO_PROCESSER::createZip()
{

	mapTNSSubscriptionDtl::iterator it1 = m_TNSSubMap.begin();
	int iZipCount =0;
	for(;it1!=m_TNSSubMap.end();it1++)
	{
		std::map<CString,stSubOutPutFile>::iterator itFile =  it1->second.m_mapOutputFiles.begin();
		std::map<CString,stSubOutPutFile>::iterator itFileEnd =  it1->second.m_mapOutputFiles.end();
		for( ;itFile!=itFileEnd; itFile++)
		{
			for(unsigned int i =0;i < itFile->second._mVecFileListForSplit.size();i++)
			{

				CString csOutputGZipFile;
				csOutputGZipFile.Format("%s.gz", itFile->second._mVecFileListForSplit[i].GetBuffer());
				CString csCommand;
				csCommand.Format("gzip.exe -1 -c \"%s\" > \"%s\"", itFile->second._mVecFileListForSplit[i].GetBuffer(), csOutputGZipFile);

				int	iValue = system(csCommand);

				if( iValue == 0 )
				{
					CString csLog;
					csLog.Format("Success:: Gzip Created %s successfully.",csOutputGZipFile);
					fnAddMessage(csLog);
					RequestExternalUpdate();
				}
				else 
				{
					CString csLog;
					csLog.Format("Error:: cmd %s ",csCommand);
					fnAddMessage(csLog);
					RequestExternalUpdate();
				}
			}
		}
	}
	fnAddMessage("Process Subscription complete.");
	RequestExternalUpdate();
}

void CME_TNS_AUTO_PROCESSER::get_all_files_names_within_folder(std::string & folder, std::string & csExch)
{
    char search_path[200];
    sprintf(search_path, "%s/*.*", folder.c_str());
    
	WIN32_FIND_DATA fd; 
    HANDLE hFind = ::FindFirstFile(search_path, &fd); 

    if(hFind != INVALID_HANDLE_VALUE) 
	{ 
        do 
		{ 
			// read all (real) files in current folder
			// , delete '!' read other 2 default folder . and ..
			if(! (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ) 
			{
				CString csFile(fd.cFileName);
				if((csFile.Find(".ZIP")!=-1) && csFile.Find(csExch.c_str())!=-1)
				{
					std::string strFile = folder + fd.cFileName;
					if (std::find(m_vecDownloadedZipfiles.begin(), m_vecDownloadedZipfiles.end(), CString(strFile.c_str()))!=m_vecDownloadedZipfiles.end() )
					{
						continue; // we already downloaded this file skipping it.
						fnAddMessage("SKIP FILE: " + csFile);
					}
					else
					{
						m_vecDownloadedZipfiles.push_back(CString(strFile.c_str()));
						fnAddMessage("Downloaded: " + CString(strFile.c_str()));
					}
				}
			}
		}
		while(::FindNextFile(hFind, &fd)); 

        ::FindClose(hFind); 
    }  
}

bool CME_TNS_AUTO_PROCESSER::exist(const std::string& name)
{

	std::ifstream file(name);
	if(!file)            // If the file was not found, then file is 0, i.e. !file=1 or true.
		return false;    // The file was not found.
	else                 // If the file was found, then file is non-0.
		return true;     // The file was found.
}

void CME_TNS_AUTO_PROCESSER::fnInsertDeliveryReport(CString csPrdouct, CString csDestAccount, CString csFileName, unsigned long &lFileSize, CString &csAwscmd )
{
	CString csCheckSum = "";
	CString csExchange = "";

	CString csDeliveryTime;
	COleDateTime oleDateTime = COleDateTime::GetCurrentTime();
	csDeliveryTime.Format("%02d-%02d-%02d %02d:%02d", oleDateTime.GetYear(), oleDateTime.GetMonth(), oleDateTime.GetDay(), oleDateTime.GetHour(), oleDateTime.GetMinute());

	CString csSql;
	csSql.Format("EXEC Proc_Beast_Submit_CME_Daily_Delivery_Report '%s', '%s', '%s', %u, '%s','%s', '%s', '%s'", csPrdouct, csDestAccount, csFileName, lFileSize, csCheckSum, csDeliveryTime, csExchange, csAwscmd);

	_RecordsetPtr set;
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)csSql, true)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSql);
		PrintRawMessage(csSql);
		return;
	}
}


void CME_TNS_AUTO_PROCESSER::fnInsertErroReport(CString csPrdouct, CString csDestAccount, CString csFileName, CString csExchnage, CString &csErrorCmd )
{	
	CString csSql;
	csSql.Format("EXEC Proc_Beast_Submit_CME_Daily_Error_Report '%s', '%s', '%s', '%s', '%s'", csPrdouct, csDestAccount, csFileName, csExchnage, csErrorCmd);
	
	_RecordsetPtr set;
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)csSql, true)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSql);
		PrintRawMessage(csSql);
		return;
	}
}


void CME_TNS_AUTO_PROCESSER::fnSendMail()
{

	CString csSmtp; 
	CSmtp mail;
	CString strBody="",strSubject="",strTemp="";

	mail.SetSMTPServer("email-smtp.us-east-1.amazonaws.com", 587, true);
	mail.SetSecurityType((SMTP_SECURITY_TYPE)USE_TLS);

	mail.SetLogin("AKIAIERFOJMRYCRRAHYQ");
	mail.SetPassword("AvauV+bh9qVBlRQuCI5u9GMG2O1N2hajyhzE7MhyYANC");

	
	CString strSendTo		= "vcmops@thebeastapps.com";
	//CString strCC			= "mpatel@thebeastapps.com,dmodi@thebeastapps.com,mvpatel@thebeastapps.com,cbhavsar@thebeastapps.com";

	const CString	__strSender	= "cmenotifications@thebeastapps.com";
	
	
	
	
	mail.SetSenderMail(__strSender);		
	mail.AddRecipient(strSendTo);
	mail.AddCCRecipient("mpatel@thebeastapps.com");
	mail.AddCCRecipient("dmodi@thebeastapps.com");
	mail.AddCCRecipient("mvpatel@thebeastapps.com");
	mail.AddCCRecipient("cbhavsar@thebeastapps.com");
	
	strSubject = "TICK Notification";
	
	strBody.Format("\nTICK Files For Date: %s\n\n",m_clScheduleInfo.csDate);

	strTemp.Format("Status: Processed successfully\n\n");
	strBody += strTemp;

	strTemp.Format("Thanks\nBeast Apps Team\n");
	strBody += strTemp;
		

	mail.AddBody(strBody);
	mail.SetSubject(strSubject);

	try
	{
		mail.Send();
	}
	catch(ECSmtp e)
	{
		CString csMsg;
		csMsg.Format("Exception in Send Mail desc: %s",e.GetErrorText());
		fnAddMessage(csMsg);
	}
	catch(...)
	{
		CString csMsg;
		csMsg.Format("Exception in Send Mail Error no: %d",GetLastError());
		fnAddMessage(csMsg);
	}

}


//void CME_TNS_AUTO_PROCESSER::createZip()
//{
//	mapTNSSubscriptionDtl::iterator it1 = m_TNSSubMap.begin();
//	int iZipCount =0;
//	for(;it1!=m_TNSSubMap.end();it1++)
//	{
//		CString strZipFile;
//		strZipFile.Format("%s\\%s\\%s.ZIP",GetField(e_OutputPath)->GetValueString(),it1->second.stDEST_ACCOUNT,m_clScheduleInfo.csDate);
//		iZipCount =0;
//		HZIP hz = CreateZip(strZipFile.GetBuffer(),0);
//		ZRESULT res;
//
//		std::map<CString,stSubOutPutFile>::iterator itFile =  it1->second.m_mapOutputFiles.begin();
//		std::map<CString,stSubOutPutFile>::iterator itFileEnd =  it1->second.m_mapOutputFiles.end();
//		for( ;itFile!=itFileEnd; itFile++)
//		{
//			for(unsigned int i =0;i < itFile->second._mVecFileListForSplit.size();i++)
//			{
//				int iIndex = itFile->second._mVecFileListForSplit[i].ReverseFind('\\');
//				CString fileName = itFile->second._mVecFileListForSplit[i].Mid(iIndex);
//				res = ZipAdd(hz,fileName.GetBuffer(), itFile->second._mVecFileListForSplit[i].GetBuffer());	
//				iZipCount++;
//			}
//		}
//		CloseZip(hz);
//		CString csLog;
//		if(iZipCount> 0)
//		{
//			csLog.Format("Success:: Zip Created %s successfully.",strZipFile);
//			fnAddMessage(csLog);
//			RequestExternalUpdate();
//		}
//		else
//		{
//			std::remove(strZipFile.GetBuffer());
//		}
//		
//	}
//}
/*
std::string CME_TNS_AUTO_PROCESSER::formatTime(std::string  trdtime)
{
	switch (trdtime.length()) {
	case (1):
		return ONEDIGETTIME + trdtime;
	case (2):
		return TWODIGETTIME + trdtime;
	case (3):
		return THREEDIGETTIME + trdtime.substr(0, 1) + COL
			+ trdtime.substr(1);
	case (4):
		return FOURDIGETTIME + trdtime.substr(0, 2) + COL
			+ trdtime.substr(2);
	case (5):
		return FIVEDIGETTIME + trdtime.substr(0, 1) + COL
			+ trdtime.substr(1, 3) + COL + trdtime.substr(3);
	default:
		return trdtime.substr(0, 2) + COL + trdtime.substr(2, 4)
			+ COL + trdtime.substr(4, 6);

	}
}
*/