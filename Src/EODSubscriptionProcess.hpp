
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <shared/MDDDataHandler.hpp>

BF_NAMESPACE_BEGIN
using namespace std;

const string __strHeader = "Trade Date,Exchange Code,Asset Class,Product Code,Clearing Code,Product Description,Product Type ,Underlying Product Code,Put/Call ,Strike Price,Contract Year,Contract Month,Contract Day,Settlement,Settlement Cabinet Indicator,Open Interest,Total Volume,Globex Volume,Floor Volume,PNT Volume,Block Volume ,EFP Volume,EOO Volume,EFR Volume,EFS Volume,EFB Volume,EFM Volume,SUB Volume,OPNT Volume,TAS Volume,TAS Block Volume ,TAM Singapore Volume ,TAM Singapore Block Volume ,TAM London Volume ,TAM London Block Volume ,Globex Open Price,Globex Open Price Bid/Ask Indicator,Globex Open Price Cabinet Indicator,Globex High Price,Globex High Price Bid/Ask Indicator,Globex High Price Cabinet Indicator,Globex Low Price,Globex Low Price Bid/Ask Indicator,Globex Low Price Cabinet Indicator,Globex Close Price,Globex Close Price Bid/Ask Indicator,Globex Close Price Cabinet Indicator,Floor Open Price,Floor Open Price Bid/Ask Indicator,Floor Open Price Cabinet Indicator,Floor Open Second Price,Floor Open Second Price Bid/Ask Indicator,Floor High Price,Floor High Price Bid/Ask Indicator,Floor High Price Cabinet Indicator,Floor Low Price,Floor Low Price Bid/Ask Indicator,Floor Low Price Cabinet Indicator,Floor Close Price,Floor Close Price High Bid/Ask Indicator,Floor Close Price Cabinet Indicator,Floor Close Second Price,Floor Close Second Price Bid/Ask Indicator,Floor Post-Close Price,Floor Post-Close Price Bid/Ask Indicator,Floor Post-Close Second Price,Floor Post-Close Second Price Bid/Ask Indicator,Delta,Implied Volatility,Last Trade Date,\n";

const CString __csOPColumnName[ 70 ] =
{
"Trade Date",
"Exchange Code",
"Asset Class",
"Product Code",
"Clearing Code",
"Product Description",
"Product Type ",
"Underlying Product Code",
"Put/Call ",
"Strike Price",
"Contract Year",
"Contract Month",
"Contract Day",
"Settlement",
"Settlement Cabinet Indicator",
"Open Interest",
"Total Volume",
"Globex Volume",
"Floor Volume",
"PNT Volume",
"Block Volume ",
"EFP Volume",
"EOO Volume",
"EFR Volume",
"EFS Volume",
"EFB Volume",
"EFM Volume",
"SUB Volume",
"OPNT Volume",
"TAS Volume",
"TAS Block Volume ",
"TAM Singapore Volume ",
"TAM Singapore Block Volume ",
"TAM London Volume ",
"TAM London Block Volume ",
"Globex Open Price",
"Globex Open Price Bid/Ask Indicator",
"Globex Open Price Cabinet Indicator",
"Globex High Price",
"Globex High Price Bid/Ask Indicator",
"Globex High Price Cabinet Indicator",
"Globex Low Price",
"Globex Low Price Bid/Ask Indicator",
"Globex Low Price Cabinet Indicator",
"Globex Close Price",
"Globex Close Price Bid/Ask Indicator",
"Globex Close Price Cabinet Indicator",
"Floor Open Price",
"Floor Open Price Bid/Ask Indicator",
"Floor Open Price Cabinet Indicator",
"Floor Open Second Price",
"Floor Open Second Price Bid/Ask Indicator",
"Floor High Price",
"Floor High Price Bid/Ask Indicator",
"Floor High Price Cabinet Indicator",
"Floor Low Price",
"Floor Low Price Bid/Ask Indicator",
"Floor Low Price Cabinet Indicator",
"Floor Close Price",
"Floor Close Price High Bid/Ask Indicator",
"Floor Close Price Cabinet Indicator",
"Floor Close Second Price",
"Floor Close Second Price Bid/Ask Indicator",
"Floor Post-Close Price",
"Floor Post-Close Price Bid/Ask Indicator",
"Floor Post-Close Second Price",
"Floor Post-Close Second Price Bid/Ask Indicator",
"Delta",
"Implied Volatility",
"Last Trade Date"
};


// 72 columns..
enum ColumnName
{
	enm_TRADE_DATE,
	enm_LAST_TRADE_DATE, // this column not present in output..
	enm_EXCH_MIC_CODE,
	enm_QUADRANT_ASSET,
	enm_TICKER_SYMBOL,
	enm_COMMODITY_CODE,
	enm_PRODUCT_DESCRIPTION,
	enm_FO_IND,
	enm_UNDERLYING_CONTRACT,
	enm_PC_IND,
	enm_STRIKE_PRICE,
	enm_CONTRACT_YR,
	enm_CONTRACT_MO,
	enm_CONTRACT_DAY,
	enm_SETL_PRICE,
	enm_SETL_CAB_IND,
	enm_OPEN_INTEREST,
	enm_TOTAL_VOL,
	enm_GLOBEX_VOL,
	enm_EXPIT_VOL,
	enm_PIT_VOL,
	enm_BLOCK_VOL,
	enm_EFP_VOL,
	enm_EOO_VOL,
	enm_EFR_VOL,
	enm_EFS_VOL,
	enm_EFB_VOL,
	enm_EFM_VOL,
	enm_SUB_VOL,
	enm_PNT_VOL,
	enm_TAS_VOL,
	enm_BLOCK_TAS_VOL,
	enm_TAMS_VOL,
	enm_TAMS_BLOCK_VOL,
	enm_TAML_VOL,
	enm_TAML_BLOCK_VOL,
	enm_ETH_OPEN1_PRICE,
	enm_ETH_OPEN1_AB_IND,
	enm_ETH_OPEN1_CAB_IND,
	enm_ETH_HI_PRICE,
	enm_ETH_HI_PRICE_AB_IND,
	enm_ETH_HI_PRICE_CAB_IND,
	enm_ETH_LO_PRICE,
	enm_ETH_LO_PRICE_AB_IND,
	enm_ETH_LO_PRICE_CAB_IND,
	enm_ETH_CLOSE1_PRICE,
	enm_ETH_CLOSE1_AB_IND,
	enm_ETH_CLOSE1_CAB_IND,
	enm_OPEN1_PRICE,
	enm_OPEN1_AB_IND,
	enm_OPEN1_CAB_IND,
	enm_OPEN2_PRICE,
	enm_OPEN2_AB_IND,
	enm_HI_PRICE,
	enm_HI_PRICE_AB_IND,
	enm_HI_PRICE_CAB_IND,
	enm_LO_PRICE,
	enm_LO_PRICE_AB_IND,
	enm_LO_PRICE_CAB_IND,
	enm_CLOSE1_PRICE,
	enm_CLOSE1_AB_IND,
	enm_CLOSE1_CAB_IND,
	enm_CLOSE2_PRICE,
	enm_CLOSE2_AB_IND,
	enm_PCLS_HI_PRICE,
	enm_PCLS_HI_AB_IND,
	enm_PCLS_IND,
	enm_PCLS_LO_PRICE,
	enm_PCLS_LO_AB_IND,
	enm_DELTA,
	enm_VOLATILITY,
	enm_END	
};

struct EodDetail
{
	std::vector<std::string> strArray;

	void GetRowMessage(std::string &msg )
	{
		msg = strArray[enm_TRADE_DATE];
		for( int iI = 1; iI < strArray.size(); iI++ )
		{
			if( iI == enm_LAST_TRADE_DATE || iI == enm_PCLS_IND || strArray[iI] == "\n" )
				continue;
			msg += "," + strArray[iI];
		}

		msg += "," + strArray[enm_LAST_TRADE_DATE] + "\n";		
	}
};	

typedef std::vector<EodDetail> EODDetailV;

struct EODSubscriptionDtl
{
	EODSubscriptionDtl()
	{
		fpOut = NULL;
		iRowCount = 0;
		iOrderId  = 0;
		iEODType = 0;

		stCUST_NAME = "";
		stCONTACT_EMAIL = "";
		stDEST_ACCOUNT = "";
		stFTP_PASSWORD = "";
		stVENUE_CODE = "";
		stALL_PRODUCTS = "";
		stPROD_CODE = "";
		stEOD_CODE = "";
		stEXCH_CODE = "";
		stOutputFileName = "";
	}

	FILE *fpOut;
	int iRowCount;
	int iOrderId;
	int iEODType; // EOD, EDOE
	
	CString stCUST_NAME;
	CString stCONTACT_EMAIL;
	CString stDEST_ACCOUNT;
	CString stFTP_PASSWORD;
	CString stVENUE_CODE;
	CString stALL_PRODUCTS;
	CString stPROD_CODE;
	CString stEOD_CODE;
	CString stEXCH_CODE;
	CString stOutputFileName;	
};

struct FileDtl
{
	std::string strFileName;
	std::string strFilePath;
	FILE *fpOut;
	int iRowCount;

	FileDtl( )
	{
		strFileName = "";
		strFilePath = "";
		fpOut = NULL;
		iRowCount = 0;
	}
};

struct SubFileDtl
{
	std::string strDestAccount;
	std::string strFileName;
	std::string strFolderPath;

	std::string strOutPutFileName;

	FILE *fpOut;
	int iRowCount;
	int iFileCount;
	int iUploadReturn;

	SubFileDtl( )
	{
		strFileName = "";
		strFolderPath = "";
		fpOut = NULL;
		iRowCount = 0;
		iFileCount = 0;
		iUploadReturn = 999;
	}
};

typedef std::map<std::string, SubFileDtl> SubFileDtlMap;

typedef std::map<std::string, FileDtl> FileDtlMap;
typedef std::vector<EODSubscriptionDtl> EODSubscriptionDtlV;

class EodDataProcessingFinapp : public BaseImage
{
public:
	
	enum FieldIDs
	{
		e_Message   = 11,

		e_DestAccount = 56,
		e_S3Bucket = 57,
		e_S3InputBucket = 58,

		e_GetEODSub	 = 220,
	   
		e_DefaultBasePath	  =	100,
		e_DirectoryInputPath  = 101,
		e_DirectoryOutputPath = 102,

		e_DefaultDate   = 205,
		e_EODTypeList	= 206,
		e_FileExtList   = 210,

		e_EODOREODE		= 207,

		e_Decompress	= 214,

		e_UploadALL		= 216,

		e_DownloadFile	= 221,
		
		e_Submit		= 211,

		e_PgUpBase		= 90,
		e_PgDnBase		= 91,

		e_SubscrID		=  1000,
		e_CUST_NAME		=  1050,
		e_CONTACT_EMAIL =  1100,
		e_DEST_ACCOUNT_FTP_PASSWORD = 1150,
		e_VENUE_CODE	= 1200,
		e_ALL_PRODUCTS	= 1250,
		e_PROD_CODE		= 1300,
		e_EOD_CODE		= 1350,
		e_EXCH_CODE		= 1400,
		e_OutputFileName= 1450,		
		e_Upload		= 1500,
		
	};
	
	EodDataProcessingFinapp();
	~EodDataProcessingFinapp();	

	bool InitData();		
	void Recalculate();	

	void GenerateFileName();
		
	static const int m_niInputColumnCount = 72;
	static const int m_niOutputColumnCount = 70;

	FileDtlMap m_clProdFileDtlMap;
	FileDtlMap m_clExchFileDtlMap;

	EODSubscriptionDtlV	m_clEODSubV;
	SubFileDtlMap m_clSubFileDtlMap;

	void EodDataProcessingFinapp::fnUncompressGZ( );
	void EodDataProcessingFinapp::ReadEODFileCSV( );
	void EodDataProcessingFinapp::ParseLine( std::string &ssLine );
	
	bool EodDataProcessingFinapp::fnCheckInSubscription( EodDetail &eodRowData );
	void EodDataProcessingFinapp::fnAddRowInSubscription( EodDetail &clEodDetail, FileDtl &eodSubDtl );

	void EodDataProcessingFinapp::LoadSubscription();
	void EodDataProcessingFinapp::fnPageUpDown( );
	void EodDataProcessingFinapp::DisplaySubscription();

	void EodDataProcessingFinapp::UploadSubscription();

	bool EodDataProcessingFinapp::CreateSubscriptionFile(SubFileDtl &clSubFileDtl, std::string &strInputFile, FILE *fileIn );

	void EodDataProcessingFinapp::fnCopyS3FiletoLocal( );

	void EodDataProcessingFinapp::fnCloseAllOpenFiles( );

private:
	ListField *m_fldpEODTypeList; //E/P/F list
	ListField *m_fldpFileExtensionList; //gz or zip

	ListField *m_fldpEODEODEList;

	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;
	
	int m_numofRows;
	int m_curTopRow;

public:
	bool m_bIsTrheadCompleted;
	bool m_bIsThreadRunning;
	CString m_csMsg;
	int m_iTotalCount;
	int m_iOpenFileCount;

	void EodDataProcessingFinapp::Parse( );

	CCriticalSection	m_MsgCriticalSec;

	void fnWriteMessage(CString csString )
	{
		CSingleLock clMsgSingleLock(&m_MsgCriticalSec);

		clMsgSingleLock.Lock();

		m_csMsg = csString.GetString();

		clMsgSingleLock.Unlock();
	}

	CString fnReadMessage( )
	{
		CSingleLock clMsgSingleLock(&m_MsgCriticalSec);

		clMsgSingleLock.Lock();

		CString csStr = m_csMsg.GetString();

		clMsgSingleLock.Unlock();

		return csStr;
	}
	
};

BF_NAMESPACE_END


