// Copyright (C) 2002 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#pragma once


#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include "BBOConstants.h"

#ifdef _DEBUG
	#pragma comment(lib,"\\Thebeast\\application\\BFImageSharedD.lib")
	#pragma comment(lib,"\\SharedApps\\BFInterestRateSharedD.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFMarket.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFSvrFieldsD.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFUtilD.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\persistd.lib")
#else
	#pragma comment(lib,"\\Thebeast\\application\\BFImageShared.lib")
	#pragma comment(lib,"\\SharedApps\\BFInterestRateShared.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFMarket.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFSvrFields.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFUtil.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\persiste.lib")
#endif



// It is recommended that you put your application code in the 
// standard BEAST namespace, that will save you from having to
// use the namespace qualifiers. Macros are used to be able to
// disable namespaces. Use the macros on either side of class
// declarations as shown in this example.
BF_NAMESPACE_BEGIN

// An application image must derive from BaseImage. BaseImage
// has a number of virtual functions that we can override, but
// this example only works on the two pure virtual functions.



struct BBODetail
{
	std::vector<std::string> strArray;

	void GetRowMessage(std::string &msg )
	{
		msg = strArray[0];
		
		/*for( int iI = 1; iI < strArray.size(); iI++ )
		{
			if( iI == enm_LAST_TRADE_DATE || iI == enm_PCLS_IND || strArray[iI] == "\n" )
				continue;
			msg += "," + strArray[iI];
		}
		msg += "," + strArray[enm_LAST_TRADE_DATE] + "\n";		
		*/
	}
};	

typedef std::vector<BBODetail> BBODetailV;

struct BBOSubscriptionDtl
{
	BBOSubscriptionDtl()
	{
		strProductV.clear();
		strExchangeV.clear();
		
		fpOut = NULL;
		iFileCount = 0;
		iRowCount = 0;
		iOrderId  = 0;
		iBLOCKType = 0;
		iStatus = 0;
		iDelStatus = 0;

		stCUST_NAME = "";
		stCONTACT_EMAIL = "";
		stDEST_ACCOUNT = "";
		stFTP_PASSWORD = "";
		stVENUE_CODE = "";
		stALL_PRODUCTS = "";
		stPROD_CODE = "";
		stBBO_CODE = "";
		stEXCH_CODE = "";
		stOutputFileName = "";
	}

	int iOrderId;
	int iBLOCKType; // EOD, EDOE
	int iStatus;
	int iDelStatus;	

	std::vector<std::string> strExchangeV;
	std::vector<std::string> strProductV;

	CString stCUST_NAME;
	CString stCONTACT_EMAIL;
	CString stDEST_ACCOUNT;
	CString stFTP_PASSWORD;
	CString stVENUE_CODE;
	CString stALL_PRODUCTS;
	CString stPROD_CODE;
	CString stBBO_CODE;
	CString stEXCH_CODE;
	CString stOutputFileName;

	FILE *fpOut;
	int iFileCount;
	int iRowCount;
};

typedef std::vector<BBOSubscriptionDtl> BBOSubscriptionDtlV;

class CME_BBO_TNS_DECODE: public BaseImage
{
public:
	virtual bool InitData();
	virtual void Recalculate();


	void fnPageUpDown( );
	void LoadSubscription();
	void DisplaySubscription();
	void GenerateFileName( );
	void fnUncompressGZ();

	BBOSubscriptionDtlV	m_clBBOSubV;

private:
	ListField *m_fldpEODTypeList; //remove
	ListField *m_fldpFileExtensionList; //gz or zip

	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;

	int m_numofRows;
	int m_curTopRow;
	
};

BF_NAMESPACE_END
