#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <shared/MDDDataHandler.hpp>
#include <fields/GridField.hpp>
#include <vector>

BF_NAMESPACE_BEGIN

using namespace std;


class OrderDetails
{

public:

	CString sOrderId;
	CString sProductName;
	CString sCustomerId;
	int		iOrderType;
	CString sPrice;
	CString sFileFormat;
	int		iDeliveryPref;
	CString sFTPUserName;
	CString sFTPPassword;
	CString sPaymentOption;
	int		iPaymentStatus;
	int		iOrderStatus;
	CString sExchangeCode;
	CString sProductCode;
	CString sAllProduct;
	CString sAssetClass;
	CString sFosType;
	CString sProductFullName;
	BFDate  StartDate;
	BFDate  EndDate;
	BFDate  DeliveryDate;
	CString sPExchangeCode;
	CString sSubPordCode;

	OrderDetails()
	{
		sOrderId = sProductName = sCustomerId = sPrice = sFileFormat = sFTPUserName = sPaymentOption = sFTPPassword = "";
		sExchangeCode = sProductCode = sAllProduct = sAssetClass = sFosType = "";
		iOrderType = 1;
		iDeliveryPref = iPaymentStatus = iOrderStatus = 0;
	}

};


class CMEOrderViewer : public BaseImage
{
public:

	enum FieldIDs
	{
		e_btnGetDetail		= 1,
		e_btnUp				= 2,
		e_btnDown			= 3,
		e_lstFileType		= 30,
		e_Label_Navigate	= 31,
		e_Status			= 33,
		e_lstOrderType		= 35,
		e_OrdrStatus		= 36,

		e_Order_Id			= 1001,
		e_ProductName		= 1801,
		e_Customer_Id		= 1051,
		e_Order_Type		= 1101,
		e_Price				= 1151,
		e_FileFormat		= 1201,
		e_DeliveryPref		= 1351,
		e_FTPUserName		= 1401,
		e_PaymentOption		= 1451,
		e_PaymentStatus		= 1501,
		e_OrderStatus		= 1551,
		e_ExchangeCode		= 1651,
		e_ProductCode		= 1701,
		e_AllProduct		= 1751,
		e_AssetClass		= 1251,
		e_FosType			= 1301,		
		e_StartDate			= 1851,
		e_EndDate			= 1901,
		e_DeliveryDate		= 1951
	};

	CMEOrderViewer();
	~CMEOrderViewer();	

	bool InitData();		
	void Recalculate();	

	void GetOrderData();
	void SetOrderData();

	vector<OrderDetails> obj_orderdetail ;

private:

	int		m_iNumofRows;
	int		m_iCurTopRow;

	DBConnection m_clDBConn;
	DBInterfaceNew	* m_clpDBInt;
	
	BaseField * btnGetDetail ;
	BaseField * GetOrderStatus;

	ListField *	m_lstFileType;
	ListField *	m_lstOrderType;		
	void CMEOrderViewer::fnPageUpDown( );
};

BF_NAMESPACE_END