// Copyright (C) 2000 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#pragma once


#include <portalhandler/BFInterestRateBaseApp.hpp>
#include <shared/DBConnection.hpp>

#define _ColumnCount 17

BF_NAMESPACE_BEGIN

struct SubscriptionSt
{
	int iFileType;
	int iOrderType;
	int iSubPeriod;
	CString csProduct;
	CString csAsstClass;
	CString csExchange;
	CString csFOIS;
	CString csVenue;
	CString csFileFormat;
	CString csContr;
	
	BFDate  bfFromDt;
	BFDate  bfToDt;
};

class ProductMasterFinApp : public BFInterestRateBaseApp 
{

public:
	
	enum FileType
	{
		enm_MD,
		enm_EOD,
		enm_BLOCK,
		enm_BBO,
		enm_TS,
		enm_Ref
	};

	enum Field
	{		
		e_FileType	= 10,
		e_OrderType	= 200,
		e_FOIType	= 230,
		e_VenueType = 205,
		e_ExchType	= 210,
		e_AssetType	= 214,
		e_SubscriptionPeriod = 207,
		e_FileFormatType = 208,
		e_Contract	= 225,
				
		e_ProdFilter	= 110,
		
		e_AllProdNameBase = 1000,		
		e_SelProdNameBase = 2000,
		
		e_DateRangeStart = 3000,
		e_DateRangeEnd   = 4000,

		e_Message = 48,
		e_Create = 49,
		
		e_PgUpBase	= 90,
		e_PgDnBase	= 91,

		e_PgUpSelBase	= 92,
		e_PgDnSelBase	= 93,

		e_PgUpSubBase	= 94,
		e_PgDnSubBase	= 95,

		e_ClearSelection = 60,
		e_TotalProduct = 61,

		//Subscription..
		e_SubFileType = 5300,
		e_SubOrderType = 5350,
		e_SubProdName = 5000,
		e_SubAssetClass = 5050,
		e_SubExchange = 5100,
		e_SubTypes = 5150,
		e_SubDates = 5200,
		e_SubContracts = 5250,
	};

	ProductMasterFinApp();
	virtual ~ProductMasterFinApp();	

	bool RealInitData();
	void RealRecalculate();
	
	int m_numofRows;
	int m_curTopRow;

	int m_numofSelRows;
	int m_curTopSelRow;

	int m_numofSubRows;
	int m_curTopSubRow;

	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;

	bool bLoadedFromDB;

	std::map<CString,CString> m_clAssetMap;

	std::vector<CString> m_csProductsV;
	std::vector<CString> m_csSelectedProductsV;
	std::vector<BFDate> m_BFFromDtV;
	std::vector<BFDate> m_BFToDtV;

	std::vector<SubscriptionSt> m_clSubscriptionStV;

	BFDate m_BFDateAllPordStart;
	BFDate m_BFDateAllPordEnd;

private:

	bool ProductMasterFinApp::fnCheckExchangeAssets(bool &bExchangeChange );
	void ProductMasterFinApp::ProductFromDB( );
	void ProductMasterFinApp::FilterEnabledDisabled( );
	void ProductMasterFinApp::fnCreateSubscription( );

	void ProductMasterFinApp::fnPageUpDown( );
	void ProductMasterFinApp::fnDisplayDetails( );

	void ProductMasterFinApp::fnDisplaySubscription( );

	void ProductMasterFinApp::fnValidateSelectedProduct( );
};

BF_NAMESPACE_END
