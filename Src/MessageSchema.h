#pragma once

#include <string>
#include <list>
#include <vector>
#include <map>
#include <set>
using namespace std;

typedef vector<string> FileList;
typedef FileList::iterator FileListItr;

class Contract
{
public:

	Contract();
	~Contract();

	long SecurityID;
	int ApplID;
	string SecurityExchange;
	string SecurityGroup;
	string Asset;
	string CFICode;
	string Symbol;

	string getContract();
	string getType();
};

class FixHeader
{
public:
	FixHeader();
	~FixHeader();

	string MsgTyp;

	__int64 MsgSeqNum;
	string SendingTime;
	string TransactTime;
	string MatchEventIndicator;
	string TradeDate;

	string calculateTags(string &strBody);
};

class NoUnderlyings
{
public:
	NoUnderlyings()
	{
		UnderlyingSymbol = "";
		UnderlyingSecurityID = 0;
		UnderlyingSecurityIDSource = "";
	}

	~NoUnderlyings() {};

	string UnderlyingSymbol;
	int UnderlyingSecurityID;
	string UnderlyingSecurityIDSource;
};

class NoLegs
{
public:
	NoLegs()
	{
		LegSecurityID = 0;
		LegSecurityIDSource = "";
		LegSide = 0;
		LegRatioQty = 0;
		LegPrice = 0;
		LegOptionDelta = 0;
	}

	~NoLegs() {};

	long LegSecurityID;
	string LegSecurityIDSource;
	int LegSide;
	int LegRatioQty;
	double LegPrice;
	double LegOptionDelta;
};

class NoMdFeedTypes
{
public:
	NoMdFeedTypes()
	{
		MDFeedType = "";
		MarketDepth = 0;
	}

	~NoMdFeedTypes() {};

	string MDFeedType;
	int MarketDepth;
};

class NoEvents
{
public:
	NoEvents()
	{
		EventType = 0;
		EventTime = "";
	}

	~NoEvents() {};

	int EventType;
	string EventTime;
};

class NoInstAttrib
{
public:
	NoInstAttrib()
	{
		InstAttribType = 0;
		InstAttribValue = "0";
	}

	~NoInstAttrib() {};

	int InstAttribType;
	string InstAttribValue;
};


class NoLotTypeRules
{
public:
	NoLotTypeRules()
	{
		LotType = 0;
		MinLotSize = 0;
	}

	~NoLotTypeRules() {};

	int LotType;
	double MinLotSize;
};

class SecurityDefinition : public FixHeader
{
public:

	SecurityDefinition();
	~SecurityDefinition();

	Contract objContract;

	int MarketSegmentID;
	string MaturityMonthYear;
	string SecuritySubType;
	int UnderlyingProduct;
	string SecurityType;
	string SecurityIDSource;
	int TotNumReports;
	string SecurityUpdateAction;
	string LastUpdateTime;
	int PutOrCall;
	string UserDefinedInstrument;
	int MDSecurityTradingStatus;
	double StrikePrice;
	string StrikeCurrency;
	string Currency;
	string SettlCurrency;
	double MinCabPrice;
	double PriceRatio;
	string MatchAlgorithm;
	double MinTradeVol;
	double MaxTradeVol;
	double MinPriceIncrement;
	double MinPriceIncrementAmount;
	double DisplayFactor;
	int TickRule;
	int MainFraction;
	int SubFraction;
	int PriceDisplayFormat;
	int ContractMultiplierUnit;
	int FlowScheduleType;
	int ContractMultiplier;
	string UnitOfMeasure;
	double UnitOfMeasureQty;
	double DecayQty;
	string DecayStartDate;
	double OriginalContractSize;
	double HighLimitPrice;
	double LowLimitPrice;
	double MaxPriceVariation;
	double TradingReferencePrice;
	int OpenInterestQty;
	int ClearedVolume;
	string SettlPriceType;

	vector<NoUnderlyings> vNoUnderlyings;
	vector<NoLegs> vNoLegs;
	vector<NoMdFeedTypes> vNoMdFeedTypes;
	vector<NoEvents> vNoEvents;
	vector<NoInstAttrib> vNoInstAttrib;
	vector<NoLotTypeRules> vNoLotTypeRules;

	string getString();
	FileList getFiles();
};

class OrderIDEntry
{
public:
	OrderIDEntry();
	~OrderIDEntry();

	__int64 OrderID;
	double LastQty;
};

class MDEntry
{
public:
	MDEntry();
	~MDEntry();

	double MDEntryPx;
	int MDEntrySize;
	long SecurityID;
	long RptSeq;
	string Symbol;
	int NumberOfOrders;
	int MDPriceLevel;
	int MDUpdateAction;
	char MDEntryType;
	double HighLimitPrice;
	double LowLimitPrice;
	double MaxPriceVariation;
	int OpenCloseSettlFlag;
	int TradeID;
	int AggressorSide;
	string TradingReferenceDate;
	string SettlPriceType;
	int ApplID;
};

class MarketData : public FixHeader
{
public:
	MarketData();
	~MarketData();

	Contract objContract;
	vector<MDEntry> vMDEntry;
	vector<OrderIDEntry> vOrderIDEntry;

	int nOffset;
	string stroutputfile;
	string getString();
	string getoutputfile() { return stroutputfile; };
	void buildoutputfile();
};

class ChannelReset : public FixHeader
{
public:
	ChannelReset();
	~ChannelReset();

	Contract objContract;
	vector<MDEntry> vMDEntry;
	
	int nOffset;
	string getString();
	FileList getFiles();
};

class SecurityStatus : public FixHeader
{
public:
	SecurityStatus();
	~SecurityStatus();

	Contract objContract;

	long SecurityID;
	int SecurityTradingStatus;
	int HaltReason;
	int SecurityTradingEvent;

	string getString();
	FileList getFiles(int applID);
};

class NoRelatedSym
{
public:
	NoRelatedSym();
	~NoRelatedSym();

	string Symbol;
	long SecurityID;
	double OrderQty;
	int QuoteType;
	int Side;
};

class QuoteRequest : public FixHeader
{
public:
	QuoteRequest();
	~QuoteRequest();

	Contract objContract;

	string QuoteReqID;
	vector<NoRelatedSym> vNoRelatedSym;

	int nOffset;
	string stroutputfile;
	string getString();
	string getoutputfile() { return stroutputfile; };
	void buildoutputfile();
};

struct Message
{
	const char *buffer;
	int length;
	int applID;

	Message()
	{
		buffer = 0;
		length = 0;
		applID = 0;
	}

	Message(const char *pabuffer, int nalength)
	{
		buffer = pabuffer;
		length = nalength;
		applID = 0;
	}

	Message(const char *pabuffer, int nalength, int naChannel)
	{
		buffer = pabuffer;
		length = nalength;
		applID = naChannel;
	}
};

class ChannelExchange
{
public:
	int ApplID;
	string SecurityExchange; 

	ChannelExchange()
	{
		ApplID = 0;
		SecurityExchange = ""; 
	}

	ChannelExchange(int nApplID, string strSecurityExchange)
	{
		ApplID = nApplID;
		SecurityExchange = strSecurityExchange; 
	}
};

