#include "stdafx.h" 
#include <fstream>
#include <sstream>
#include <stdafx.h> 
#include <shldisp.h>
#include <tlhelp32.h>
#include "zip.h"
#include "CME_Block_Processing.hpp"

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(CME_Block_Processing)

CME_Block_Processing::CME_Block_Processing()
{
}

CME_Block_Processing::~CME_Block_Processing()
{
}


bool CME_Block_Processing::InitData()
{
	fld_InputFile = GetField(e_strInputFile);
	fld_OutputFile = GetField(e_strOutputFile);

	fld_Status = GetField(e_strStatus);	

	m_bIsThreadRunning = false;
	return true;
}

void CME_Block_Processing::fnUncompress( )
{
	if( !IsFieldNew(e_Uncompress) )
		return;

	CString csInputFile = fld_InputFile->GetValueString();
	if( csInputFile.Find(".gz") == -1 )
	{
		fld_Status->SetValueString("Input file name is not valid ..!!");
		return;
	}

	CString csUncompFileName = csInputFile;
	csUncompFileName.Replace(".gz","");
	CString csCommand;
	//csCommand.Format("gzip.exe -d -c \\powervault3\CME_s3\daily\endofday\20150805\EOD_20150805_E.gz > \\powervault3\CME_s3\daily\endofday\20150805\EOD_20150805_E.txt");
	csCommand.Format("gzip.exe -d -c %s > %s", csInputFile, csUncompFileName);
		
	int	iValue = system(csCommand);

	if( iValue == 0 )
	{
		//UpdateUncompressflag(1, index);
		fld_Status->SetValueString("Uncomprssed file successfully.");
	}
	else
	{
		CString csTemp;
		csTemp.Format("error no = %d - path %s, uncompress unsuccessful",iValue, csInputFile);
		fld_Status->SetValueString(csTemp);
	}
}

UINT BLOCKProcessor(void *pVoid)
{
	CME_Block_Processing *pParser = (CME_Block_Processing*)pVoid;
	
	pParser->m_lTotalRead = 0;
	pParser->m_bIsThreadRunning = true;
	pParser->autoParseFile( );
	pParser->m_bIsThreadRunning = false;

	return 0;
}


void CME_Block_Processing::Recalculate()
{	
	if( m_bIsThreadRunning )
	{
		CString csTemp;
		csTemp.Format("Reading rows %d",m_lTotalRead);
		GetField(e_Message)->SetTitle(csTemp);
	}
	else
	{
		GetField(e_Message)->SetTitle("");

		fnUncompress( );

		if (IsFieldNew(e_btnParseFile))
		{
			CString csInputFile = fld_InputFile->GetValueString();		
			string sOutputPath = fld_OutputFile->GetValueString();

			csInputFile.Replace(".gz","");
			string sInputFile = csInputFile.GetString();

			int iFile = PathFileExists(sInputFile.c_str());

			if( iFile != 1 )//file path is not valid
			{
				fld_Status->SetValueString("Input file path is not exist ..!!");
				return;
			}

			iFile = PathFileExists(sOutputPath.c_str());
			if( iFile != 1 )
			{
				CreateDirectory(sOutputPath.c_str(), NULL);
			}

			iFile = PathFileExists(sOutputPath.c_str());
			if( iFile != 1 )
			{
				fld_Status->SetValueString("Output folder is not exist ..!!");
				return;
			}

			COleDateTime dtCurrent = COleDateTime::GetCurrentTime();
			CString strCurrentTime;
			strCurrentTime.Format("%04d/%02d%02d %02d:%02d:%02d", dtCurrent.GetYear(), dtCurrent.GetMonth(), dtCurrent.GetDay(), 
				dtCurrent.GetHour(), dtCurrent.GetMinute(), dtCurrent.GetSecond());

			GetField(e_StartTime)->SetValueString(strCurrentTime);
			GetField(e_EndTime)->SetValueString("");

			fld_Status->SetValueString("File Processing Started ..!!");
			AfxBeginThread(BLOCKProcessor, this);
			//autoParseFile();		
		}
	}
}

void  CME_Block_Processing::autoParseFile()
{
	CString csInputFile = fld_InputFile->GetValueString();		
	csInputFile.Replace(".gz","");
	string fileName = csInputFile.GetString();

	m_clExchangePathList.clear();
	m_clExchangeYearPathList.clear();
	m_clExchangeYearMonthPathList.clear();
	m_clExchangeYearMonthDatePathList.clear();

	string sExchange, sProduct, sProductType, sTradeDate, key, newFilePath;

	int iExchangeIndex(0), iProductIndex(0), iProductTypeIndex(0), iTradeDateIndex(0);

	CString line;
	CStdioFile readInputFile;

	map<string, FILE * > mapProductFiles;
	map<string, FILE * >::iterator itr_mapProductFiles;
	int iOpenFileCount = 0;

	vector<string> recordList;
	FILE *fp = NULL;
	
	bool bstarte = false;
	CString csYearPars;
	csYearPars.Format("%d", GetField(e_ParseYear)->GetValueInt());

	int iCount(0);

	if( readInputFile.Open(fileName.c_str(), CFile::modeRead) )
	{
		int header = 0;
		while (readInputFile.ReadString(line))
		{
			header++;		
			splitString(line, ",", recordList);
			if( header == 1 )
			{
				iTradeDateIndex = indexofColumn(recordList, "TRADE_DATE");
				iExchangeIndex	= indexofColumn(recordList, "EXCH_CODE");
				iProductIndex	= indexofColumn(recordList, "TICKER_SYMBOL");
				iProductTypeIndex = indexofColumn(recordList, "FOI_SYMBOL_IND");
				continue;
			}
			
			sTradeDate = recordList.at(iTradeDateIndex);
			iCount++;

			if( iCount >= 1000 )
			{
				m_lTotalRead += iCount;
				iCount = 0;
				this->RequestExternalUpdate();
			}

			if( GetField(e_ParseYear)->GetValueInt() ^ -1 )
			{
				if( sTradeDate.find(csYearPars) == -1 && !bstarte )
				{
					continue;
				}

				if( sTradeDate.find(csYearPars) == -1 && bstarte )
				{
					break;
				}

				bstarte = true;
			}

			sProduct	 = recordList.at(iProductIndex);
			sProductType = recordList.at(iProductTypeIndex);

			if( !isalpha(sProductType[0]) )
				sExchange = recordList.at(iExchangeIndex + 1);
			else
				sExchange = recordList.at(iExchangeIndex);

			key = sExchange + "_" + sProduct + "_"+ sTradeDate;

			CString csOutputPath = fnCreatePath( sTradeDate.c_str(), sExchange.c_str() );

			itr_mapProductFiles = mapProductFiles.find(key);
			if (itr_mapProductFiles != mapProductFiles.end())
			{				
				fp = itr_mapProductFiles->second;

				if( fp == NULL )
				{
					if( iOpenFileCount >= 500 )
					{
						itr_mapProductFiles = mapProductFiles.begin();
						while( itr_mapProductFiles != mapProductFiles.end() )
						{
							if( itr_mapProductFiles->second != NULL )
							{
								fclose(itr_mapProductFiles->second);
								itr_mapProductFiles->second = NULL;
							}
							itr_mapProductFiles++;
						}

						iOpenFileCount = 0;
					}

					newFilePath = createProductFile(csOutputPath.GetString(), key);
					
					//reopen file
					fp = fopen(newFilePath.c_str(), "a+");
					itr_mapProductFiles->second = fp;
					iOpenFileCount++;
				}

				fputs(line + "\n", fp);
			}
			else
			{
				if( iOpenFileCount >= 500 )
				{
					itr_mapProductFiles = mapProductFiles.begin();
					while( itr_mapProductFiles != mapProductFiles.end() )
					{
						if( itr_mapProductFiles->second != NULL )
						{
							fclose(itr_mapProductFiles->second);
							itr_mapProductFiles->second = NULL;
						}
						itr_mapProductFiles++;
					}

					iOpenFileCount = 0;
				}

				newFilePath = createProductFile(csOutputPath.GetString(), key);
				
				FILE * writeFile = fopen(newFilePath.c_str(), "w");
				mapProductFiles.insert(pair<string, FILE *>(key, writeFile));
				iOpenFileCount++;

				fputs(__strHeader.c_str(), writeFile); // Write Header...
				fputs(line + "\n", writeFile);				
			}
		}

		//Close all open product files
		int iTotalFileCount = mapProductFiles.size();
		itr_mapProductFiles = mapProductFiles.begin();

		while (itr_mapProductFiles != mapProductFiles.end())
		{
			fp = itr_mapProductFiles->second;

			if( fp != NULL )
				fclose(fp);

			itr_mapProductFiles++;
		}

		readInputFile.Close();

		CString csTemp;
		csTemp.Format("%d product file created",iTotalFileCount);
		fld_Status->SetValueString(csTemp);
	}
	else
	{
		fld_Status->SetValueString("Error while Opening File");
	}
}

CString CME_Block_Processing::fnCreatePath( CString csDate, CString csExchange )
{
	CString csYear = csDate.Mid(0,4);
	CString csMonth = csDate.Mid(4,2);
	CString csDay = csDate.Mid(6,2);

	CString csExchangeDate = csExchange+"_"+csDate;

	if( m_clExchangeYearMonthDatePathList.find(csExchangeDate) != m_clExchangeYearMonthDatePathList.end() )
	{
		CString csOutputPath = fld_OutputFile->GetValueString();
		csOutputPath = csOutputPath + "\\" + csExchange + "\\" + csYear + "\\" + csMonth + "\\" + csDate;
		return csOutputPath;
	}		

	CString csYearMonth = csDate.Mid(0,6);
	CString csExchangeYearMonth = csExchange+"_"+csYearMonth;
	if( m_clExchangeYearMonthPathList.find(csExchangeYearMonth) != m_clExchangeYearMonthPathList.end() )
	{
		CString csOutputPath = fld_OutputFile->GetValueString();
		csOutputPath = csOutputPath +  "\\" + csExchange + "\\" + csYear + "\\" + csMonth + "\\" + csDate;
		CreateDirectory(csOutputPath, NULL);

		m_clExchangeYearMonthDatePathList.insert(std::pair<CString,bool>(csExchangeDate, true));
		return csOutputPath;
	}

	CString csExchangeYear = csExchange+"_"+csYear;
	if( m_clExchangeYearPathList.find(csExchangeYear) != m_clExchangeYearPathList.end() )
	{
		CString csOutputPath = fld_OutputFile->GetValueString();
		csOutputPath = csOutputPath + "\\" + csExchange + "\\" + csYear + "\\" + csMonth;
		CreateDirectory(csOutputPath, NULL);

		m_clExchangeYearMonthPathList.insert(std::pair<CString,bool>(csExchangeYearMonth, true));

		csOutputPath = fld_OutputFile->GetValueString();
		csOutputPath = csOutputPath + "\\" + csExchange + "\\" + csYear + "\\" + csMonth + "\\" + csDate;
		CreateDirectory(csOutputPath, NULL);

		m_clExchangeYearMonthDatePathList.insert(std::pair<CString,bool>(csExchangeDate, true));
		return csOutputPath;
	}

	if( m_clExchangePathList.find(csExchange) != m_clExchangePathList.end() )
	{
		CString csOutputPath = fld_OutputFile->GetValueString();
		csOutputPath = csOutputPath + "\\" + csExchange + "\\" + csYear;
		CreateDirectory(csOutputPath, NULL);

		m_clExchangeYearPathList.insert(std::pair<CString,bool>(csExchangeYear, true));

		csOutputPath = fld_OutputFile->GetValueString();
		csOutputPath = csOutputPath + "\\" + csExchange + "\\" + csYear + "\\" + csMonth;
		CreateDirectory(csOutputPath, NULL);

		m_clExchangeYearMonthPathList.insert(std::pair<CString,bool>(csExchangeYearMonth, true));

		csOutputPath = fld_OutputFile->GetValueString();
		csOutputPath = csOutputPath + "\\" + csExchange + "\\" + csYear + "\\" + csMonth + "\\" + csDate;
		CreateDirectory(csOutputPath, NULL);

		m_clExchangeYearMonthDatePathList.insert(std::pair<CString,bool>(csExchangeDate, true));
		return csOutputPath;
	}

	CString csOutputPath = fld_OutputFile->GetValueString();
	csOutputPath = csOutputPath + "\\" + csExchange;
	CreateDirectory(csOutputPath, NULL);

	m_clExchangePathList.insert(std::pair<CString,bool>(csExchange, true));

	csOutputPath = fld_OutputFile->GetValueString();
	csOutputPath = csOutputPath + "\\" + csExchange + "\\" + csYear;
	CreateDirectory(csOutputPath, NULL);

	m_clExchangeYearPathList.insert(std::pair<CString,bool>(csExchangeYear, true));

	csOutputPath = fld_OutputFile->GetValueString();
	csOutputPath = csOutputPath + "\\" + csExchange + "\\" + csYear + "\\" + csMonth;
	CreateDirectory(csOutputPath, NULL);

	m_clExchangeYearMonthPathList.insert(std::pair<CString,bool>(csExchangeYearMonth, true));

	csOutputPath = fld_OutputFile->GetValueString();
	csOutputPath = csOutputPath + "\\" + csExchange + "\\" + csYear + "\\" + csMonth + "\\" + csDate;
	CreateDirectory(csOutputPath, NULL);

	m_clExchangeYearMonthDatePathList.insert(std::pair<CString,bool>(csExchangeDate, true));

	return csOutputPath;
}

void CME_Block_Processing::splitString(CString cStr, string delimiter, vector<string> &result)
{
	result.clear();

	size_t pos = 0;
	string fieldValue, str;

	str = cStr;
	while( (pos = str.find(delimiter)) != string::npos )
	{
		fieldValue = str.substr(0, pos);
		result.push_back(fieldValue);
		str.erase(0, pos + delimiter.length());
	}
	result.push_back(str);

	while( result.size() > m_niInputColumnCount )
	{
		result[enm_TICKER_SYMBOL_DESCRIPTION] = result[enm_TICKER_SYMBOL_DESCRIPTION] + "," + result[enm_MARKET_SECTOR];
		result.erase(result.begin() + enm_MARKET_SECTOR);
	}
}

string CME_Block_Processing::createProductFile(string outputPath, string strkey )
{
	//Creating Product File
	string stnewFilePath = outputPath + "\\" + strkey + ".csv";
	return stnewFilePath;
}

size_t  CME_Block_Processing::indexofColumn(vector<string> vec, string searchKey)
{
	vector<string>::iterator itr = std::find(vec.begin(), vec.end(), searchKey);
	if (itr != vec.end())
	{
		size_t index = distance(vec.begin(), itr);
		return index;
	}
}

//
//bool CME_Block_Processing::parseByExchange(string fileName, string outputPath, string keyExchange, bool parseAllProducts)
//{
//	vector<string> vecProducts;
//	vector<string>::iterator begin, end;
//	
//	string sExchange , sProductType , sProduct , masterFilePath;
//	CString line , headerRow;
//	CStdioFile readInputFile;
//	vector<string> recordList;
//
//	FILE * fpExchangeFile = NULL ;
//
//	recordList.clear();
//
//
//	if (!parseAllProducts)
//	{
//		vecProducts = splitString(keyProduct.c_str(), ",");
//		begin = vecProducts.begin();
//		end = vecProducts.end();
//	}
//
//	if (readInputFile.Open(fileName.c_str(), CFile::modeRead))
//	{
//		masterFilePath = fileNamePrifix + "1" + fileNameSuffix;
//		fpExchangeFile = fopen(masterFilePath.c_str(), "w");
//
//		int header = 0;
//		int fRecord = 0;
//		while (readInputFile.ReadString(line))
//		{
//			header++;
//			if (header == 1)
//			{
//				headerRow = line;
//				fRecord++;
//				fputs(headerRow + "\n", fpExchangeFile);
//				continue;
//			}
//
//			recordList = splitString(line, ",");
//			sProductType = recordList.at(iProductTypeIndex);
//
//			if (!isalpha(sProductType[0]))
//				sExchange = recordList.at(iExchangeIndex + 1);
//			else
//				sExchange = recordList.at(iExchangeIndex);
//
//			if ( fRecord == MAX_ROWS)
//			{
//				fclose(fpExchangeFile);
//				masterFilePath = fileNamePrifix + "2" + fileNameSuffix;
//				fpExchangeFile = fopen(masterFilePath.c_str(), "w");
//				fputs(headerRow + "\n", fpExchangeFile);
//				fRecord = 1;
//			}
//
//			if (sExchange == keyExchange)
//			{
//				if (parseAllProducts)
//				{
//					fRecord++;
//					fputs(line + "\n", fpExchangeFile);
//				}
//				else
//				{
//					sProduct = recordList.at(iProductIndex);
//					if (find(begin, end, sProduct) != end)
//					{
//						fRecord++;
//						fputs(line + "\n", fpExchangeFile);
//					}
//				}
//				
//			}
//		}
//		fclose(fpExchangeFile);
//		readInputFile.Close();
//		return true;
//	}
//	return false;
//}

//
//CString  CME_Block_Processing::getExchangeList(string fileName)
//{
//	vector<string> recordList;
//	string sExchange, sProductType, row ; 
//
//	CString line, exchangeList;
//	CStdioFile readInputFile;
//
//	int header = 0;
//	if (readInputFile.Open(fileName.c_str(), CFile::modeRead))
//	{
//		setExchange.clear();
//		while (readInputFile.ReadString(line))
//		{
//			header++;
//			recordList = splitString(line, ",");
//
//			if (header == 1)
//			{
//				iTradeDateIndex = indexofColumn(recordList, "TRADE_DATE");
//				iExchangeIndex = indexofColumn(recordList, "EXCH_CODE");
//				iProductIndex = indexofColumn(recordList, "TICKER_SYMBOL");
//				iProductTypeIndex = indexofColumn(recordList, "FOI_SYMBOL_IND");
//				continue;
//			}
//
//			sProductType = recordList.at(iProductTypeIndex);
//
//			if (!isalpha(sProductType[0]))
//				sExchange = recordList.at(iExchangeIndex + 1);
//			else
//				sExchange = recordList.at(iExchangeIndex);
//
//			if (setExchange.find(sExchange) == setExchange.end())
//			{
//				setExchange.insert(sExchange);
//				exchangeList = exchangeList + sExchange.c_str() + "|";
//			}
//		}
//		iFileRecords = header - 1;
//		readInputFile.Close();
//		return exchangeList ;
//	}
//	return "";
//}
//
//set<string> CME_Block_Processing::getProductList(string fileName, string keyExchange)
//{
//	string sExchange, sProduct, sProductType, sTradeDate, key, newFilePath;
//
//	CString line;
//	CStdioFile readInputFile;
//
//	set<string> productList;
//
//	vector<string> recordList;
//
//	FILE * fp = NULL;
//	FILE * eFile = NULL;
//
//	recordList.clear();
//	productList.clear();
//
//	if (readInputFile.Open(fileName.c_str(), CFile::modeRead))
//	{
//		int header = 0;
//		while (readInputFile.ReadString(line))
//		{
//			header++;
//			recordList = splitString(line, ",");
//
//			if( header == 1 )
//			{
//				iTradeDateIndex = indexofColumn(recordList, "TRADE_DATE");
//				iExchangeIndex	= indexofColumn(recordList, "EXCH_CODE");
//				iProductIndex	= indexofColumn(recordList, "TICKER_SYMBOL");
//				iProductTypeIndex = indexofColumn(recordList, "FOI_SYMBOL_IND");
//				continue;
//			}
//
//		
//			sProductType = recordList.at(iProductTypeIndex);
//
//			if (!isalpha(sProductType[0]))
//				sExchange = recordList.at(iExchangeIndex + 1);
//			else
//				sExchange = recordList.at(iExchangeIndex);
//
//			if (sExchange == keyExchange)
//			{
//				sProduct = recordList.at(iProductIndex);
//				if ( productList.find(sProduct) == productList.end() )
//					productList.insert(sProduct);
//			}
//		}
//		readInputFile.Close();
//	}//End of If
//
//	return productList;
//}
//
//bool CME_Block_Processing :: parseByProduct(string fileName, string outputPath, string keyProduct)
//{
//	vector<string> vecProducts = splitString(keyProduct.c_str(), ",");
//	vector<string>::iterator begin = vecProducts.begin();
//	vector<string>::iterator end = vecProducts.end();
//
//	string sExchange, sProduct, sProductType, sTradeDate, key, newFilePath, masterFilePath ;
//
//	CString line , headerRow;
//	CStdioFile readInputFile;
//
//	vector<string> recordList;
//
//	FILE * fpExchangeFile = NULL ;
//
//	recordList.clear();
//
//	if (readInputFile.Open(fileName.c_str(), CFile::modeRead))
//	{
//		int header  = 0;
//		int fRecord = 0;
//		masterFilePath = fileNamePrifix + "1" + fileNameSuffix;
//		fpExchangeFile = fopen(masterFilePath.c_str(), "w");
//
//		while (readInputFile.ReadString(line))
//		{
//			header++;
//			if (header == 1)
//			{
//				headerRow = line;
//				fRecord++;
//				fputs(headerRow + "\n", fpExchangeFile);
//				continue;
//			}
//			recordList = splitString(line, ",");
//			sProductType = recordList.at(iProductTypeIndex);
//
//			if (!isalpha(sProductType[0]))
//				sExchange = recordList.at(iExchangeIndex + 1);
//			else
//				sExchange = recordList.at(iExchangeIndex);
//
//			sProduct = recordList.at(iProductIndex);
//
//			if (fRecord  == MAX_ROWS)
//			{
//				fclose(fpExchangeFile);
//				masterFilePath = fileNamePrifix + "2" + fileNameSuffix;
//				fpExchangeFile = fopen(masterFilePath.c_str(), "w");
//				fputs(headerRow + "\n", fpExchangeFile);
//				fRecord = 1;
//			}
//
//			if (find(begin, end, sProduct) != end)
//			{
//				fRecord++;
//				fputs(line + "\n", fpExchangeFile);
//			}
//			
//		}//End While
//
//		fclose(fpExchangeFile);
//		readInputFile.Close();
//		return true;
//	}//End of If
//
//	return false;
//}

/*
CString CME_Block_Processing::unzip(string sInputFile)
{
	size_t index = sInputFile.find_last_of("/\\");
	string filePath = sInputFile.substr(0, index);
	string fileName = sInputFile.substr(index + 1);

	CString fileFolder = fileName.c_str();
	fileFolder = fileFolder.SpanExcluding(".");
	
	CString csFilepath = filePath.c_str();
	CString csFileName = fileName.c_str();

	//CString csSource = csFilepath + csFileName;
	CString csSource = sInputFile.c_str();

	try
	{
		BSTR source = csSource.AllocSysString();

		csFilepath = csFilepath +"\\"+ fileFolder;
		BSTR dest = csFilepath.AllocSysString();

		CreateDirectory(csFilepath, NULL);

		HRESULT        hResult;
		IShellDispatch *pISD;
		Folder         *pToFolder = NULL;
		VARIANT        vDir, vFile, vOpt;

		CoInitialize(NULL);

		hResult = CoCreateInstance(CLSID_Shell, NULL, CLSCTX_INPROC_SERVER, IID_IShellDispatch, (void **)&pISD);

		if (SUCCEEDED(hResult))
		{
			VariantInit(&vDir);
			vDir.vt = VT_BSTR;
			vDir.bstrVal = dest;
			hResult = pISD->NameSpace(vDir, &pToFolder);

			if (hResult != S_FALSE && pToFolder != NULL)//if  (SUCCEEDED(hResult))
			{
				Folder *pFromFolder = NULL;

				VariantInit(&vFile);
				vFile.vt = VT_BSTR;
				vFile.bstrVal = source;//L"C:\\test.txt";				
				HRESULT hFileFound = pISD->NameSpace(vFile, &pFromFolder);

				if (hFileFound == S_FALSE || pFromFolder == NULL)
				{
					pToFolder->Release();
					CString str = "File : " + csSource + " Not Found , unZip unsuccessfull";
					throw str;
				}

				FolderItems *fi = NULL;
				pFromFolder->Items(&fi);
				VariantInit(&vOpt);
				vOpt.vt = VT_I4;
				vOpt.lVal = FOF_NO_UI;//4; // Do not display a progress dialog box

				// Creating a new Variant with pointer to FolderItems to be copied
				VARIANT newV;
				VariantInit(&newV);
				newV.vt = VT_DISPATCH;
				newV.pdispVal = fi;
				hResult = pToFolder->CopyHere(newV, vOpt);

				pFromFolder->Release();
				pToFolder->Release();
			}
			else
			{
				CString str = "File : " + csFilepath + " Not Found , unZip unsuccessfull";
				throw str;
			}

			pISD->Release();
		}
		CoUninitialize();

		CString sInputFilePath;
		CString sFullMask = csFilepath + CString("\\*.csv");

		CFileFind finder;
		bool bFound =  finder.FindFile(sFullMask);
		while ((bFound))
		{
			bFound = finder.FindNextFile();
			if (!finder.IsDots())
				sInputFilePath += finder.GetFilePath();
		}

		return sInputFilePath;
	}
	catch(CString strError)
	{
		SetErrorMessage(ErrorSeverity::e_Error, strError);
		return strError;
	}
}

bool CME_Block_Processing:: zip(string sInputFilePath, string sOutputPath)
{
	try
	{
		// D:\CME Block File\BLOCK_07162015\B001_07162015.csv
		string zipFileName, csvFileName, csvFile, sCommand ,zipFolder;

		size_t index = sInputFilePath.find_last_of("/\\");
		zipFileName = sInputFilePath.substr(0, index);
		zipFolder = zipFileName;

		index = zipFileName.find_last_of("/\\");
		zipFileName = sOutputPath + "\\" + zipFileName.substr(index + 1) + ".zip";

		CString sFullMask = sOutputPath.c_str() + CString("\\*.csv");
		CFileFind finder;
		
		HZIP hz = CreateZip(zipFileName.c_str(), 0);

		bool bFound = finder.FindFile(sFullMask);
		while ((bFound))
		{
			bFound = finder.FindNextFile();
			if (!finder.IsDots())
			{
				csvFile = finder.GetFilePath();
				index = csvFile.find_last_of("/\\");
				csvFileName = csvFile.substr(index + 1); // B001_07162015.csv
				ZipAdd(hz, csvFileName.c_str(), csvFile.c_str());
				
				remove(csvFile.c_str());
			}
				
		}
		CloseZip(hz);

		remove(sInputFilePath.c_str());
		rmdir(zipFolder.c_str());
		
		return true;
	}
	catch (...)
	{
		return  false;
	}	
}*/
