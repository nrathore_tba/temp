
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <sys/stat.h>

#include "afxinet.h"

using namespace std;

BF_NAMESPACE_BEGIN

struct stMDFiles
{
	stMDFiles()
	{
		_strEDOFileName = "";
		_fpOutPutFile = NULL;
	}
	std::string _strEDOFileName;
	std::ofstream * _fpOutPutFile;
};

struct stMDFileDtl
{
	CString csFileName;
	BFDate  FileDate;
	CString csProduct;
	CString csFutOpt;
	CString csExchange;
	CString csYear;
	CString csMonth;
	CString csDay;
	CString csFileStatus;
	unsigned long long ulFileSize;
	stMDFileDtl()
	{
		csFileName = _T("");
		csProduct = _T("");
		csFutOpt = _T("");
		csExchange =  _T("");
		csYear = _T("");
		csMonth = _T("");
		csDay = _T("");
		csFileStatus = _T("");
		ulFileSize = 0;
	}
};
enum enmOperation
{
	OP_Download,
	OP_Process,
	OP_ProcessCompleted,
	OP_None
};
typedef std::vector<stMDFileDtl> MDFileDetail;
struct MDSubscriptionDtl
{
	MDSubscriptionDtl()
	{
		iOrderId  = 0;

		stCUST_NAME = "";
		stCONTACT_EMAIL = "";
		stDEST_ACCOUNT = "";
		stFTP_PASSWORD = "";
		stFTPUserName = "";
		stVENUE_CODE = "";
		stALL_PRODUCTS = "";
		stPROD_CODE = "";
		stMD_CODE = "";
		stEXCH_CODE = "";
		stOutputFileName = "";
		csSubProductCode = "";
		bOrderProcessed = false;
	}

	int iOrderId;

	CString stCUST_NAME;
	CString stCONTACT_EMAIL;
	CString stDEST_ACCOUNT;
	CString stFTP_PASSWORD;
	CString stFTPUserName;
	CString stVENUE_CODE;
	CString stALL_PRODUCTS;
	CString stPROD_CODE;
	CString stMD_CODE;
	CString stEXCH_CODE;
	CString stOutputFileName;
	CString csSubProductCode;
	BFDate  dtStartDate;
	BFDate  dtEndDate;
	bool	bOrderProcessed;

};
typedef std::vector<MDSubscriptionDtl> MDSubscriptionDtlV;

class MessageLog
{
public:
	MessageLog()
	{
		m_strLogFIlePath = "D:\\OTO\\MD\\MDLogFile.txt";
	}
	~MessageLog(){}
	void LogMessage(string strLogMessage);
	inline bool IsFileExist(const string& name)
	{
		struct stat buffer;   
		return (stat (name.c_str(), &buffer) == 0); 
	}
private:
	string m_strLogFIlePath;
};
class ExecutionMDDateRange : public BaseImage
{
public:
	//Threading 
	bool m_bIsTrheadCompleted;
	bool m_bIsThreadRunning;
	enmOperation m_enmOperation;
	COleDateTime m_clNextScheduleDtTime;
	MDSubscriptionDtlV	m_clMDSubV;
	CString m_csFTPUserName;
	enum FieldIDs
	{
		e_sourcePath = 1,
		e_destPath = 2,
		e_calendar =3,
		e_totalSubscriber = 5,
		e_userName = 7,
		e_uploadFiles = 8,
		e_NextTime = 9,
		e_getSubScription = 11,
		e_Status = 12,
		e_uploadAllFiles = 19,
		e_dtFromDate	= 58,
		e_dtToDate		= 59,
		e_Year		= 201,
		e_Month		= 202,
		e_Day		= 216,
		e_SubscrID = 1000,
		e_CUST_NAME = 1050,
		//e_CONTACT_EMAIL = 1100,
		e_DEST_ACCOUNT_FTP_PASSWORD = 1150,
		e_VENUE_CODE = 1200,
		e_ALL_PRODUCTS = 1250,
		e_PROD_CODE = 1300,
		e_MD_CODE	= 1350,
		e_EXCH_CODE = 1400,
		e_StartDate = 1450,
		e_subProductCode = 1900,
		e_EndDate = 2000,
		e_PgDnBase = 90,
		e_PgUpBase = 91,
	
		//Calander Details
		e_totalFiles = 4,
		e_pgUP = 93,
		e_pgDN =92,
		e_fileName = 300,
		e_fileDate = 400,
		e_prodCode = 500,
		e_exchangeCode = 600,
		e_futOpt = 700,
		e_fileSize = 800,
		e_fileStatus = 900
	};

	ExecutionMDDateRange();
	~ExecutionMDDateRange();	
	bool InitData();		
	void Recalculate();
	void LoadSubscription();
	void fnPageUpDown();
	void ManageUpDown();
	void DisplaySubscription();
	void DisplayDownloadFiles();
	void uploadSinglefile();
	void UploadAllFiles();
	void LoadUploadFiles(int iBtnIndex);
	void UploadUserProduct(CString sFTPUser);
	CString GetSourcePath(int iIndex);
	CString GetDestinationPath();
	void LoadUserFileDetails();
	void MDProcessingScheduler();
	const std::string currentDateTime() 
	{
		time_t     now = time(0);
		struct tm  tstruct;
		char       buf[80];
		tstruct = *localtime(&now);
		strftime(buf, sizeof(buf), "%Y%m%d", &tstruct);
		return buf;
	}
	
private:
	bool _bIsProcess;
	MDFileDetail m_vtMDFileDetails;
	
	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;

	//date field
	CString m_csYear;
	CString m_csMonth;
	CString m_csDay;

	int m_numofRows;
	int m_nNumOfRows;
	int m_curTopRow;
	int m_nCurTopRow;
	int m_nTotalFiles;

	CString m_csS3Command;
	CString m_csOutputPath;
	CString m_csUserFTPAccountName;
	CString m_csSourcepath;
	CString m_csDestinationPath;
	CString m_csUserName;
	CString m_csOrderIDS;
};

BF_NAMESPACE_END
