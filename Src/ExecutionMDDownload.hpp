
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <shared/MDDDataHandler.hpp>

BF_NAMESPACE_BEGIN

using namespace std;

struct ScheduleInfo
{
	int		iRetInterval;
	int		iRetryCount;
	bool	bValid;
	int		iStatus;	

	COleDateTime clMainScheduleDtTime;	
	COleDateTime clScheduleDtTime;	
	CString csDate;

	CString  csFilePath;
};

enum enmOperation
{
	OP_Download,
	OP_S3HadoopSync,
	OP_Process,
	OP_ReadThread,
	OP_ProcessCompleted,
	OP_None
};

struct SubscriptionDetail
{
	SubscriptionDetail()
	{		
		iOrderId = 0;
		stCUST_NAME = "";
		//stCONTACT_EMAIL = "";
		stDEST_ACCOUNT = "";
		stFTP_PASSWORD = "";
		//stVENUE_CODE = "";
		stALL_PRODUCTS = "";
		stPROD_CODE = "";
		stMD_CODE = "";
		stEXCH_CODE = "";
		stEXCH_CODE_New = "";
		stAsset_Class = "";
		stFOICode = "";
	}	
	
	int iOrderId;
	CString stCUST_NAME;
	//CString stCONTACT_EMAIL;
	CString stDEST_ACCOUNT;
	CString stFTP_PASSWORD;
	//CString stVENUE_CODE;
	CString stALL_PRODUCTS;
	CString stPROD_CODE;
	CString stMD_CODE;
	CString stEXCH_CODE;
	CString stEXCH_CODE_New;
	CString stAsset_Class;
	CString stFOICode;
};


struct UniqueAccount
{
	UniqueAccount()
	{		
	}

	CString strDestAccount;	
	set<CString> sKeys;
};

typedef std::map<CString, UniqueAccount> SubAccountMap;

typedef std::multimap<std::string, SubscriptionDetail> MDBinarySubDtlMap;
typedef std::vector<SubscriptionDetail> SubscriptionV;

class ExecutionMDDownload : public BaseImage
{
public:
	
	enum FieldIDs
	{
		e_NextDownloadMsg = 50,	
		e_MsgTitle		  = 65,
		e_OutputPath	  = 62,
		e_HadoopS3IN	  = 63,
		e_HadoopS3OUT	  = 66,

		e_ReloadSchedule  = 100,
		e_DateFilter	  = 95,
		e_ManualRun		  = 96,
		
		e_RtryInterval	  = 98,

		e_LocalToHadoopIn = 97,
		e_Upload		  = 99,

		//e_SubType	= 212,
		e_Year		= 201,
		e_Month		= 202,
		e_Day		= 215,
		e_Hour		= 213,
		e_Minute	= 214,

		e_S3CMEBucket = 60,
		e_DestAccount = 58,
		e_S3FTPBucket = 59,

		e_LogMsg	= 4000,
		e_pgUP		= 92,
		e_pgDN		= 93,
	};
	
	ExecutionMDDownload();
	~ExecutionMDDownload();	

	bool InitData();		
	void Recalculate();	
private:

	void ExecutionMDDownload::fnAddMessage( CString csaNewMsg );
	void ExecutionMDDownload::fnDisplayMsg();
	
	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;
	
	int m_numofRows;
	int m_curTopRow;

	BaseField *fld_OutputPath;

public:
	bool m_bIsTrheadCompleted;
	bool m_bIsThreadRunning;
	
	CString m_csMsg;
	CCriticalSection	m_MsgCriticalSec;

	void fnWriteMessage(CString csString )
	{
		CSingleLock clMsgSingleLock(&m_MsgCriticalSec);

		clMsgSingleLock.Lock();

		m_csMsg = csString.GetString();

		clMsgSingleLock.Unlock();
	}

	CString fnReadMessage( )
	{
		CSingleLock clMsgSingleLock(&m_MsgCriticalSec);

		clMsgSingleLock.Lock();

		CString csStr = m_csMsg.GetString();

		clMsgSingleLock.Unlock();

		return csStr;
	}	

	void ExecutionMDDownload::fnGetNextSchedule( );
	void ExecutionMDDownload::fnUpdateScheduleStatus(int iStatus );

	void ExecutionMDDownload::fnRecalc1( );
	//void ExecutionMDDownload::fnCopyS3FiletoLocal( );
	//void ExecutionMDDownload::fnCopyLocalToHadoop( );
			
	void ExecutionMDDownload::fnBinaryUploadProcessor( );
	
	void ExecutionMDDownload::LoadBinarySubscription();
	void ExecutionMDDownload::LoadTextSubscription();

	//void ExecutionMDDownload::UploadSubscription();
	void ExecutionMDDownload::CreateBinarySubscriptionFile();
	void ExecutionMDDownload::CreateTextSubscriptionFile();

	void ExecutionMDDownload::fnInsertDeliveryReport(CString csPrdouct, CString csDestAccount, CString csFileName, unsigned long &lFileSize );
	void ExecutionMDDownload::fnInsertErroReport(CString csPrdouct, CString csDestAccount, CString csFileName, CString csExchnage, CString &csErrorCmd );

	ScheduleInfo m_clScheduleInfo;

	enmOperation m_enmOperation;

	MDBinarySubDtlMap	m_clMDBinaryProdSubDtlMap;

	SubscriptionV m_vSubscriptions;
};

BF_NAMESPACE_END


