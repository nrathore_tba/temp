
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <shared/MDDDataHandler.hpp>

BF_NAMESPACE_BEGIN
using namespace std;

typedef std::map<CString,int> MapFTpConnect;

struct stFTPAccounts
{
	stFTPAccounts()
	{	
		strCustName="";
		strFTPAcc = "";
		strFTPPassword = "";	
		iChecked = 0;
	}	

	CString strCustName;
	CString strFTPAcc;
	CString strFTPPassword;
	int iChecked;	
	MapFTpConnect _mapFtpConnect;
};

class CMEFTPAccounts : public BaseImage
{
public:
	
	enum FieldIDs
	{
		e_Admin		= 71,
		e_GetData	= 72,
		e_Type      = 200,
			
		e_SearchBtn     	= 117,				

		e_DisplayCustomerName	= 2300,
		e_DisplayeFTPAcc 		= 2400,
		e_DisplayFTPPassword	= 2250,
		e_IsChecked				= 2000,
		e_CheckedOKCmd			= 2100,
		e_ConnectOKCmd			= 2130,
		
		e_ftpServerList         =100,
		e_ConnectALL			=101,	
		e_PgUp	= 92,
		e_PgDn	= 93,	

		e_Label_PgNo = 65,
				
	};
	
	CMEFTPAccounts();
	~CMEFTPAccounts();	

	bool InitData();		
	void Recalculate();	
	
	void GetFilterData();
	
	void DisplayGrid();
	void ManageUpDownButton();
	void fnFTPConnect();
	void fnConnectALLFTP();
	std::vector<stFTPAccounts> m_clstFTPAccountsV;

	bool CMEFTPAccounts::fnCheckAdminUpdate( );

private:
		
	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;
	vector<CString> vecFTPServers;
	
	int m_iCurrTopRow;
	
	ListField *fld_Type;
};

BF_NAMESPACE_END


