// Copyright (C) 2002 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#include "stdafx.h"
#include "Email_Sender.hpp"
#include <fields/ListField.hpp>
#include <MailAutomation/MailAutomation.h>

// Put this	after all the include statements

BF_NAMESPACE_USE
	
IMPLEMENT_IMAGE(EMail_Sender)	


const int		__niMsgCouunt = 200;
CString __csMsgArry[__niMsgCouunt] = { "" };

void EMail_Sender::GetWatchDogEvents()
{
	printf("GetWatchDogEvents\n");
	vecEMails.clear();

	CString sql;	
	sql.Format("exec Get_CrashMails");

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		ASSERT(0);
		return;
	}
	
	while(VARIANT_FALSE == set->adoEOF)
	{
			stEmail _stEmail;
			SETSTR(_stEmail.csDate ,		set->Fields->Item[_variant_t("EventDT")]->Value);
			SETLONG(_stEmail.iEventId ,		set->Fields->Item[_variant_t("EventID")]->Value);
			SETSTR(	_stEmail.csProcessName,		set->Fields->Item[_variant_t("ProccessName")]->Value);	
			SETSTR(	_stEmail.csHostName,		set->Fields->Item[_variant_t("hostname")]->Value);
			SETSTR(	_stEmail.csComment,		set->Fields->Item[_variant_t("comment")]->Value);
			SETSTR(	_stEmail.csReported,		set->Fields->Item[_variant_t("Reported")]->Value);
	
		vecEMails.push_back(_stEmail);
		set->MoveNext();
	}
	set->Close();	
}

bool EMail_Sender::InitData()
{
	
	m_curTopRow= 0;
	for(m_numofRows = 0; GetField(e_LogMsg + m_numofRows); m_numofRows++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","UAT-RDS");
	csServerName = strServerName.get_value().c_str();


	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "watchdog","watchdog","watchdog","");

	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();

	return true;
} 


void EMail_Sender::SendEmails()
{
	if(vecEMails.size()<1)
		return;

	printf("SendEmails\n");
	CString csSmtp; 
	CSmtp mail;
	
	mail.SetSMTPServer("email-smtp.us-east-1.amazonaws.com", 587, true);
	mail.SetSecurityType((SMTP_SECURITY_TYPE)USE_TLS);

	mail.SetLogin("AKIAIERFOJMRYCRRAHYQ");
	mail.SetPassword("AvauV+bh9qVBlRQuCI5u9GMG2O1N2hajyhzE7MhyYANC");

	mail.SetSenderMail("sysadmin@thebeastapps.com");		
	mail.AddRecipient("helpdesk@thebeastapps.com");
	
	for(unsigned int i =0; i<vecEMails.size();i++)
	{
		
		CString csBody="",csSubject="";
				
		if (vecEMails[i].iEventId == 4)
		{
			csSubject.Format("Detected crash: %s %s",vecEMails[i].csHostName,vecEMails[i].csProcessName);
			csBody = csSubject;

			csBody+="\n";
			csBody+="Time: ";
			csBody+=vecEMails[i].csDate;
			csBody+="\n";
			csBody+="Host: ";
			csBody+=vecEMails[i].csHostName;
			csBody+="\n";
			csBody+="Comments: ";
			csBody+=vecEMails[i].csComment;
			csBody+="\n";
		}
		else
		{
			csSubject.Format("Memory usage is high: %s %s",vecEMails[i].csHostName,vecEMails[i].csProcessName);
			csBody = csSubject;
			csBody+="\n";
			csBody+="Time: ";
			csBody+=vecEMails[i].csDate;
			csBody+="\n";
			csBody+="Host: ";
			csBody+=vecEMails[i].csHostName;
			csBody+="\n";
			csBody+="Comments: ";
			csBody+=vecEMails[i].csComment;
			csBody+="\n";
		}
		
		fnAddMessage(csSubject);
		mail.AddBody(csBody);
		mail.SetSubject(csSubject);

		try
		{
			mail.Send();
		}
		catch(ECSmtp e)
		{
			CString csMsg;
			csMsg.Format("Exception in Send Mail desc: %s",e.GetErrorText());
			fnAddMessage(csMsg);
		}
		catch(...)
		{
			CString csMsg;
			csMsg.Format("Exception in Send Mail Error no: %d",GetLastError());
			fnAddMessage(csMsg);
		}
	}

}
void EMail_Sender::Recalculate()
{
	printf("Recalculate\n");

	GetWatchDogEvents();
	
	SendEmails();
	
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_pgUP,e_pgDN,200,m_numofRows,m_curTopRow);	
	
	fnDisplayMsg();	
	
	StartRecalcTimer(10, false);
}

void EMail_Sender::fnAddMessage( CString csaNewMsg )
{	
	for( int iI = __niMsgCouunt-1; iI > 0; iI-- )
		__csMsgArry[iI] = __csMsgArry[iI -1];

	CString csMsg;
	csMsg.Format("(%s) %s",currentDateTime().c_str(),csaNewMsg);
	__csMsgArry[0] = csMsg;	
}
void EMail_Sender::fnDisplayMsg()
{
	int iRow = 0;
	for( int iIndex = m_curTopRow; iIndex < __niMsgCouunt; iIndex++, iRow++ )
	{
		if( iRow >= m_numofRows )
			break;

		BaseField * fldComment		= GetField(e_LogMsg + iRow);

		if(__csMsgArry[iIndex].Find("Success")!=-1 || __csMsgArry[iIndex].Find("success")!=-1)
			fldComment->SetForeColor(ColorManager::ColorIndices::eFrameGreen);
		else if(__csMsgArry[iIndex].Find("Error")!=-1 || __csMsgArry[iIndex].Find("error")!=-1)
			fldComment->SetForeColor(ColorManager::ColorIndices::eFrameRed);
		else
			fldComment->SetForeColor(ColorManager::ColorIndices::eFrameWhite) ;



		fldComment->SetValueString( __csMsgArry[iIndex] );
		fldComment->SetNotManual();
	}

	while( iRow < m_numofRows )
	{
		BaseField * fldComment		= GetField(e_LogMsg + iRow);
		fldComment->SetBlankState();
		iRow++;
	}
}
