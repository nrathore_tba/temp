#include <stdafx.h>
#include "MessageUtil.h"
#include <boost/filesystem.hpp>

MessageUtil* MessageUtil::single = NULL;

MessageUtil* MessageUtil::getInstance()
{
    if(!single)
    {
        single = new MessageUtil();
        return single;
    }
    else
    {
        return single;
    }
}

bool MessageUtil::GetFilesInDirectory(std::vector<string> &out, const string &directory, string sfiletype)
{
	if( !(boost::filesystem::exists(directory)))
		return false;

    HANDLE dir;
    WIN32_FIND_DATA file_data;

    if ((dir = FindFirstFile((directory + "/" + sfiletype).c_str(), &file_data)) == INVALID_HANDLE_VALUE)
        return true; /* No files found */

    do {
        const string file_name = file_data.cFileName;
        const string full_file_name = directory + "\\" + file_name;
        const bool is_directory = (file_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;

        if (file_name[0] == '.')
            continue;

        if (is_directory)
		{
            GetFilesInDirectory(out, full_file_name, sfiletype);
		}
		else
		{
			out.push_back(full_file_name);
		}
    } while (FindNextFile(dir, &file_data));

    FindClose(dir);
	return true;
}

bool MessageUtil::GetFilesInDirectory(std::vector<stFile> &out, const string &directory, string sfiletype)
{
	if( !(boost::filesystem::exists(directory)))
		return false;

    HANDLE dir;
    WIN32_FIND_DATA file_data;

    if ((dir = FindFirstFile((directory + "/" + sfiletype).c_str(), &file_data)) == INVALID_HANDLE_VALUE)
        return true; /* No files found */

    do {
        const string file_name = file_data.cFileName;
        const string full_file_name = directory + "\\" + file_name;
        const bool is_directory = (file_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;
		long file_size = file_data.nFileSizeLow;

		stFile objFile;
		objFile.strFileName = file_name;
		objFile.strFilePath = full_file_name;
		objFile.lFileSize = file_data.nFileSizeLow;

        if (file_name[0] == '.')
            continue;

        if (is_directory)
		{
            GetFilesInDirectory(out, full_file_name, sfiletype);
		}
		else
		{
			if( objFile.strFileName.find("_IRS_") == string::npos )
				out.push_back(objFile);
		}
    } while (FindNextFile(dir, &file_data));

    FindClose(dir);
	return true;
}

void MessageUtil::LoadProcessedFiles(string strfolder)
{
	_processedFiles.clear();

	stroutputfolder = strfolder;
	FILE *pFile;
	string sFileName = string(strfolder) + "\\Processed.files";
	if(fopen_s(&pFile, sFileName.c_str() ,"r") == 0)
	{
		char chLine[1024] = {0};
		
		while(fgets(chLine, 1023, pFile))
		{
			size_t length = strlen(chLine) - 1;
			if (chLine[length] == '\n')
				chLine[length] = '\0';
			_processedFiles.insert(chLine);
		}
	}

	if(pFile)
		fclose(pFile);
}

bool MessageUtil::IsFileProcessed(string strfile)
{
	return (_processedFiles.find(strfile) != _processedFiles.end());
}

void MessageUtil::SetProcessedFile(string strfile)
{
	FILE *m_pFile;
	string sFileName = string(stroutputfolder) + "\\Processed.files";
	fopen_s(&m_pFile, sFileName.c_str() ,"a");

	if(m_pFile)
	{
		fwrite(strfile.c_str(), strfile.length(), 1, m_pFile);
		fwrite("\n", 1, 1, m_pFile);
		fclose(m_pFile);
		m_pFile = 0;

		_processedFiles.insert(strfile);
	}
}

void MessageUtil::LogToFile(string strLog)
{
	if( strLogFile.IsEmpty() )
		return;

	FILE *pFile = NULL;
	fopen_s(&pFile, strLogFile ,"a");

	if ( pFile != NULL )
	{
		CTime cTime(CTime::GetCurrentTime());
		CString csPrefix;
		csPrefix.Format("%04d%02d%02d-%02d:%02d:%02d : ", cTime.GetYear(), cTime.GetMonth(), cTime.GetDay(), cTime.GetHour(),cTime.GetMinute(),cTime.GetSecond());
		fprintf(pFile, "%s%s\n", csPrefix, strLog.c_str());
		fclose(pFile);
	}
}

void MessageUtil::getContracts(string filename)
{
	if(contracts.size() > 0)
		return;

	FILE *m_pFile;

	if(fopen_s(&m_pFile, filename.c_str() ,"r") == 0)
	{
		char chLine[1024] = {0};
		char * pch;
		Contract objContract;

		while(fgets(chLine, 1023, m_pFile))
		{
			size_t length = strlen(chLine) - 1;
			if (chLine[length] == '\n')
				chLine[length] = '\0';

			pch = strtok (chLine,",");
			if(pch == NULL)
				continue;
			
			objContract.SecurityID = atoi(pch);

			pch = strtok (NULL,",");
			if(pch == NULL)
				continue;
			
			objContract.ApplID = atoi(pch);

			pch = strtok (NULL,",");
			if(pch == NULL)
				continue;

			objContract.SecurityExchange = pch;

			pch = strtok (NULL,",");
			if(pch == NULL)
				continue;

			objContract.SecurityGroup = pch;

			pch = strtok (NULL,",");
			if(pch == NULL)
				continue;

			objContract.Asset = pch;

			pch = strtok (NULL,",");
			if(pch == NULL)
				continue;

			objContract.CFICode = pch;

			pch = strtok (NULL,",");
			if(pch == NULL)
				continue;

			objContract.Symbol = pch;

			updateContracts(objContract);
		}

		fclose(m_pFile);
	}
}

bool MessageUtil::updateContracts(Contract &objContract)
{
	string strGrpProdKey, strProdCFICodeKey, strAppGroupKey, strShortCFICode;

	// Fill Structures
	ChannelExchange pcEx;
	pcEx.ApplID = objContract.ApplID;
	pcEx.SecurityExchange = objContract.SecurityExchange;
			
	// Prepare Contract Map
	pair<ContractMapItr ,bool> prContract;
	prContract = contracts.insert(pair<int, Contract>(objContract.SecurityID, objContract));

	if(!prContract.second) 
		return false;

	// Prepare Channel Map
	ChannelMap::iterator itrChannel = channelMap.find(objContract.ApplID);
	if(itrChannel == channelMap.end())
		channelMap.insert(pair<int, string>(objContract.ApplID, objContract.SecurityExchange));

	// Prepare Product-Exchange Map
	strGrpProdKey = objContract.SecurityGroup + "_" + objContract.Asset;

	ProductExchangeMap::iterator itrProd = productExchangeMap.find(strGrpProdKey);
	if(itrProd == productExchangeMap.end())
		productExchangeMap.insert(pair<string, ChannelExchange>(strGrpProdKey, pcEx));

	// Prepare GroupToProduct Map
	set<string>::iterator itSet = gGroupToProductSet.find(strGrpProdKey);
	if( itSet == gGroupToProductSet.end() )
	{
		gGroupToProductSet.insert(strGrpProdKey);
		gGroupToProductMap.insert(pair<string, string>(objContract.SecurityGroup, objContract.Asset));
	}

	// ProductCFICodeMap
	if(objContract.CFICode.length() > 1)
	{
		strShortCFICode = objContract.CFICode.substr(0, 2);

		if(strShortCFICode.compare("FM") != 0 && strShortCFICode.compare("OM") != 0 )
		{
			strShortCFICode = objContract.CFICode.substr(0, 1);
		}

		strProdCFICodeKey = strGrpProdKey + "_" + strShortCFICode;
		itSet = gProductCFICodeSet.find(strProdCFICodeKey);
			
		if( itSet == gProductCFICodeSet.end() )
		{
			gProductCFICodeSet.insert(strProdCFICodeKey);
			gProductCFICodeMap.insert(pair<string, string>(strGrpProdKey, strShortCFICode));
		}
	}

	// Prepare ChannelToGroup Map
	strAppGroupKey = to_string((_Longlong)objContract.ApplID) + "_" + objContract.SecurityGroup;
	itSet = gChannelToGroupSet.find(strAppGroupKey);
			
	if( itSet == gChannelToGroupSet.end() )
	{
		gChannelToGroupSet.insert(strAppGroupKey);
		gChannelToGroupMap.insert(pair<int, string>(objContract.ApplID, objContract.SecurityGroup));
	}

	return true;
}