#include <fstream>
#include <sstream>
#include <stdafx.h> 
#include <shldisp.h>
#include <tlhelp32.h>
#include "CMEOrderViewer.hpp"


BF_NAMESPACE_USE
using namespace std;

IMPLEMENT_IMAGE(CMEOrderViewer)

CMEOrderViewer::CMEOrderViewer()
{	
}

//--------------------------------------------------------------------------------------

CMEOrderViewer::~CMEOrderViewer()
{
}

//--------------------------------------------------------------------------------------

bool CMEOrderViewer::InitData()
{
	GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));
	
	m_iCurTopRow = 0;
	for(m_iNumofRows = 0; GetField(e_Order_Id + m_iNumofRows); m_iNumofRows++);

	m_lstFileType	= (ListField*) GetField(e_lstFileType);	
	m_lstOrderType	= (ListField*) GetField(e_lstOrderType);	
	btnGetDetail	= GetField(e_btnGetDetail);
	GetOrderStatus  = GetField(e_OrdrStatus);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString sServerName = "";
	RegString strServerName(pKey, "ServerName","UAT-RDS");
	sServerName = strServerName.get_value().c_str();
	bool bStatus = m_clDBConn.InitDatabase(this, sServerName, "CME","watchdog","watchdog","");
	//bool bResult = m_clDBConn.InitDatabase(this, "10.100.12.91", "CMEdev","watchdog","watchdog","");
	if( !bStatus )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		GetField(e_Status)->SetValueString("Could not initialize datastore!");
		return true;
	}
	m_clpDBInt = m_clDBConn.GetDBInterfaceNew();
	SetOrderData();

	return true;
}

//--------------------------------------------------------------------------------------

void CMEOrderViewer::Recalculate()
{
	if( IsFieldNew( e_btnGetDetail) || IsInitialRecalc() )
	{
		GetOrderData();
	}	

	fnPageUpDown();
	SetOrderData();
}

//--------------------------------------------------------------------------------------

 void  CMEOrderViewer :: GetOrderData()
{	
	CString sFileType	= m_lstFileType->GetDisplayString();
	int		iOrderType	= m_lstOrderType->GetValueInt();
	int iOrderStatus    = GetOrderStatus->GetValueInt();
	//GetOrderStatus->GetDis
	_RecordsetPtr recordSet;
	CString sQuery = "";
	//sQuery.Format("SELECT ORDER_ID,CART_ID,Customer_Id,Order_Type,SubscriptionType,Price,File_Type,DeliveryPref,FTPUserName,PaymentOption,PaymentStatus,OrderStatus,IsOneTimeOrder,DeliveryDate,orderdate,IsFullPayment FROM [CMEdev].[dbo].[CME_Order]");
	//sQuery.Format("EXEC [dbo].Proc_Beast_Get_Subscriptions '%s',%d", csFileType, iOrderType);
	sQuery.Format("EXEC [dbo].Proc_Beast_Get_All_Subscriptions '%s',%d", sFileType, iOrderType);
	
	if( FAILED( m_clpDBInt->GetRecordset(&recordSet, sQuery, false) ) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sQuery);
		GetField(e_Status)->SetValueString(sQuery);
		return ;
	}

	OrderDetails tmpobj ;
	obj_orderdetail.clear();
	while (VARIANT_FALSE == recordSet->adoEOF)
	{
		SETSTR(	tmpobj.sOrderId,		recordSet->Fields->Item[_variant_t("ORDER_ID")]->Value);
		SETSTR(	tmpobj.sProductName,	recordSet->Fields->Item[_variant_t("Category_Name")]->Value);	
		SETSTR(	tmpobj.sCustomerId,		recordSet->Fields->Item[_variant_t("Customer_Id")]->Value);	
		SETLONG(tmpobj.iOrderType,		recordSet->Fields->Item[_variant_t("Order_Type")]->Value);		
		SETSTR(	tmpobj.sPrice,			recordSet->Fields->Item[_variant_t("Price")]->Value);	
		SETSTR(	tmpobj.sFileFormat,		recordSet->Fields->Item[_variant_t("File_Format")]->Value);	
		SETLONG(tmpobj.iDeliveryPref,	recordSet->Fields->Item[_variant_t("DeliveryPref")]->Value);	
		SETSTR(	tmpobj.sFTPUserName,	recordSet->Fields->Item[_variant_t("FTPUserName")]->Value);	
		SETSTR(	tmpobj.sFTPPassword,	recordSet->Fields->Item[_variant_t("FTPPassword")]->Value);			
		SETSTR(	tmpobj.sPaymentOption,	recordSet->Fields->Item[_variant_t("PaymentOption")]->Value);	
		SETLONG(tmpobj.iPaymentStatus,	recordSet->Fields->Item[_variant_t("PaymentStatus")]->Value);	
		SETLONG(tmpobj.iOrderStatus,	recordSet->Fields->Item[_variant_t("OrderStatus")]->Value);		
		SETSTR(	tmpobj.sExchangeCode,	recordSet->Fields->Item[_variant_t("EXCH_CODE")]->Value);	
		SETSTR(	tmpobj.sProductCode,	recordSet->Fields->Item[_variant_t("PROD_CODE")]->Value);	
		SETSTR(	tmpobj.sAllProduct,		recordSet->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);	
		SETSTR(	tmpobj.sAssetClass,		recordSet->Fields->Item[_variant_t("Asset_Class")]->Value);	
		SETSTR(	tmpobj.sFosType,		recordSet->Fields->Item[_variant_t("FOS_Type")]->Value);
		SETSTR(	tmpobj.sProductFullName,recordSet->Fields->Item[_variant_t("Product_Name")]->Value);
		SETBFDATE( tmpobj.StartDate,	recordSet->Fields->Item[_variant_t("Startdate")]->Value);
		SETBFDATE( tmpobj.EndDate,		recordSet->Fields->Item[_variant_t("EndDate")]->Value);
		SETBFDATE( tmpobj.DeliveryDate,	recordSet->Fields->Item[_variant_t("DeliveryDate")]->Value);
		SETSTR(	tmpobj.sPExchangeCode,	recordSet->Fields->Item[_variant_t("Exchange")]->Value);
		SETSTR(	tmpobj.sSubPordCode,	recordSet->Fields->Item[_variant_t("Sub_Product_Code")]->Value);

		tmpobj.sProductFullName = tmpobj.sProductFullName + "--" + tmpobj.sPExchangeCode + "--" + tmpobj.sSubPordCode;

		int iFOS = atoi(tmpobj.sFosType);
		CString csFosNew = "";
		if( iFOS & 2 == 2 )
			csFosNew = "FUT,";

		if( iFOS & 4 == 4 )
			csFosNew += "OPT,";

		if( iFOS & 8 == 8 )
			csFosNew += "FUT_SPD,";

		if( iFOS & 16 == 16 )
			csFosNew += "OPT_SPD,";

		tmpobj.sFosType = csFosNew;
		
		if( iOrderStatus ^ -1)
		{
			if(tmpobj.iOrderStatus == iOrderStatus)
				obj_orderdetail.push_back(tmpobj);
		}
		else
			obj_orderdetail.push_back(tmpobj);

		recordSet->MoveNext();
	}

	recordSet->Close();
	
	CString sMessage = "";
	sMessage.Format("Order Detail: %d Record(s) Found ..!!",obj_orderdetail.size());
	GetField(e_Status)->SetValueString(sMessage);
}

void CMEOrderViewer::fnPageUpDown( )
{
	int iTotalRecord = obj_orderdetail.size();
	
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_btnUp,e_btnDown,iTotalRecord,m_iNumofRows,m_iCurTopRow);	
}

CString _csDelPrf[ ] = { "FTP", "S3" };

void CMEOrderViewer :: SetOrderData()
{
	int iRow = 0 ;
	COleDateTime dtToday = COleDateTime::GetCurrentTime();
	BFDate bfTodayDate;
	bfTodayDate.SetYearMonthDate(dtToday.GetYear(),dtToday.GetMonth() - 1, dtToday.GetDay() ); 
	bool bOrderExpired = false;	
	int	iPenType   = -1;
	
	for(int iIndex = m_iCurTopRow; iIndex < obj_orderdetail.size(); iIndex++, iRow++)
	{	
		if( iRow >= m_iNumofRows )
			break;		

		OrderDetails  &clObjOrder = obj_orderdetail[iIndex];	
		int iOrderId = atoi(clObjOrder.sOrderId);

		ColorManager::ColorIndices enmPenColor =  ColorManager::eDefault;

		if( clObjOrder.iPaymentStatus == 1 && clObjOrder.iOrderStatus == 1)
		{
			if(clObjOrder.iOrderType == 1)										// Order is One time
			{
				enmPenColor = ColorManager::ePositiveChange;					// ColorManager::ePositiveChange													
			}
			else if(clObjOrder.iOrderType == 2)									// Order is Subscription
			{
				if(clObjOrder.EndDate < bfTodayDate)
					enmPenColor = ColorManager::eNegativeChange;				// ColorManager::eNegativeChange
			}
		}

		GetField(e_Order_Id			+ iRow)->SetForeColor(enmPenColor);
		GetField(e_ProductName		+ iRow)->SetForeColor(enmPenColor);
		GetField(e_Customer_Id		+ iRow)->SetForeColor(enmPenColor);
		GetField(e_Order_Type		+ iRow)->SetForeColor(enmPenColor);
		GetField(e_Price			+ iRow)->SetForeColor(enmPenColor);
		GetField(e_FileFormat		+ iRow)->SetForeColor(enmPenColor);
		GetField(e_DeliveryPref		+ iRow)->SetForeColor(enmPenColor);
		GetField(e_FTPUserName		+ iRow)->SetForeColor(enmPenColor);
		GetField(e_PaymentOption	+ iRow)->SetForeColor(enmPenColor);
		GetField(e_PaymentStatus	+ iRow)->SetForeColor(enmPenColor);
		GetField(e_OrderStatus		+ iRow)->SetForeColor(enmPenColor);
		GetField(e_ExchangeCode		+ iRow)->SetForeColor(enmPenColor);
		GetField(e_ProductCode		+ iRow)->SetForeColor(enmPenColor);
		GetField(e_AllProduct		+ iRow)->SetForeColor(enmPenColor);
		GetField(e_AssetClass		+ iRow)->SetForeColor(enmPenColor);
		GetField(e_FosType			+ iRow)->SetForeColor(enmPenColor);
		GetField(e_StartDate		+ iRow)->SetForeColor(enmPenColor);  
		GetField(e_EndDate			+ iRow)->SetForeColor(enmPenColor);
		GetField(e_DeliveryDate		+ iRow)->SetForeColor(enmPenColor);

		GetField(e_Order_Id			+ iRow)->SetValueString(clObjOrder.sOrderId);
		GetField(e_ProductName		+ iRow)->SetValueString(clObjOrder.sProductName);
		GetField(e_Customer_Id		+ iRow)->SetValueString(clObjOrder.sCustomerId);
		GetField(e_Order_Type		+ iRow)->SetValueInt(clObjOrder.iOrderType);
		GetField(e_Price			+ iRow)->SetValueString(clObjOrder.sPrice);
		GetField(e_FileFormat		+ iRow)->SetValueString(clObjOrder.sFileFormat);
		
		if( clObjOrder.iDeliveryPref >= 0 && clObjOrder.iDeliveryPref <= 1 )
			GetField(e_DeliveryPref		+ iRow)->SetValueString(_csDelPrf[clObjOrder.iDeliveryPref]);
		else
			GetField(e_DeliveryPref		+ iRow)->SetValueString("");

		GetField(e_FTPUserName		+ iRow)->SetValueString(clObjOrder.sFTPUserName);
		GetField(e_FTPUserName		+ iRow)->SetToolTipText(clObjOrder.sFTPPassword);
		GetField(e_PaymentOption	+ iRow)->SetValueString(clObjOrder.sPaymentOption);
		GetField(e_PaymentStatus	+ iRow)->SetValueInt(clObjOrder.iPaymentStatus);
		GetField(e_OrderStatus		+ iRow)->SetValueInt(clObjOrder.iOrderStatus);
		GetField(e_ExchangeCode		+ iRow)->SetValueString(clObjOrder.sExchangeCode);
		GetField(e_ProductCode		+ iRow)->SetValueString(clObjOrder.sProductCode);
		GetField(e_ProductCode		+ iRow)->SetToolTipText(clObjOrder.sProductFullName);
		GetField(e_AllProduct		+ iRow)->SetValueString(clObjOrder.sAllProduct);
		GetField(e_AssetClass		+ iRow)->SetValueString(clObjOrder.sAssetClass);
		GetField(e_FosType			+ iRow)->SetValueString(clObjOrder.sFosType);
		
		if(clObjOrder.StartDate.GetYYYYMMDD() == 19010101)
			GetField(e_StartDate		+ iRow)->SetBlankState();
		else
			GetField(e_StartDate		+ iRow)->SetValueDate(clObjOrder.StartDate);

		if(clObjOrder.EndDate.GetYYYYMMDD() == 19010101)
			GetField(e_EndDate		+ iRow)->SetBlankState();
		else
			GetField(e_EndDate		+ iRow)->SetValueDate(clObjOrder.EndDate);

		if(clObjOrder.DeliveryDate.GetYYYYMMDD() == 19010101)
			GetField(e_DeliveryDate		+ iRow)->SetBlankState();
		else
			GetField(e_DeliveryDate		+ iRow)->SetValueDate(clObjOrder.DeliveryDate);
	}
	

	while( iRow < m_iNumofRows )
	{
		GetField(e_Order_Id			+ iRow)->SetBlankState();
		GetField(e_ProductName		+ iRow)->SetBlankState();
		GetField(e_Customer_Id		+ iRow)->SetBlankState();
		GetField(e_Order_Type		+ iRow)->SetBlankState();
		GetField(e_Price			+ iRow)->SetBlankState();
		GetField(e_FileFormat		+ iRow)->SetBlankState();
		GetField(e_DeliveryPref		+ iRow)->SetBlankState();
		GetField(e_FTPUserName		+ iRow)->SetBlankState();
		GetField(e_FTPUserName		+ iRow)->SetToolTipText("");
		GetField(e_PaymentOption	+ iRow)->SetBlankState();
		GetField(e_PaymentStatus	+ iRow)->SetBlankState();
		GetField(e_OrderStatus		+ iRow)->SetBlankState();
		GetField(e_ExchangeCode		+ iRow)->SetBlankState();
		GetField(e_ProductCode		+ iRow)->SetBlankState();
		GetField(e_ProductCode		+ iRow)->SetToolTipText("");
		GetField(e_AllProduct		+ iRow)->SetBlankState();
		GetField(e_AssetClass		+ iRow)->SetBlankState();
		GetField(e_FosType			+ iRow)->SetBlankState();
		GetField(e_StartDate		+ iRow)->SetBlankState();
		GetField(e_EndDate			+ iRow)->SetBlankState();
		GetField(e_DeliveryDate		+ iRow)->SetBlankState();
		iRow++;
	}
	
	CString sPageNumber = "";
	int iRowCount = obj_orderdetail.size();
	if(iRowCount > 0)
	{
		int iCurrPage = m_iCurTopRow / m_iNumofRows + 1;
		int iTotalPage;	
		if( (iRowCount % m_iNumofRows) == 0)
		{
			iTotalPage = (iRowCount/m_iNumofRows);	
		}
		else
		{
		
			iTotalPage = (iRowCount/m_iNumofRows) + 1;	
		}

		if(iTotalPage <= 0)
			iTotalPage = 1;
		sPageNumber.Format("%d/%d",iCurrPage,iTotalPage);
		GetField(e_Label_Navigate)->SetTitle(sPageNumber);
	}
	else
	{
		GetField(e_Label_Navigate)->SetTitle("0/0");
	}	
}