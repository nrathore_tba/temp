#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <shared/MDDDataHandler.hpp>

BF_NAMESPACE_BEGIN

using namespace std;


class ProductDetails
{
public:
	CString strFileName;
	CString strFileType;
	BFDate  FileDate;
	CString strExchange;
	CString strProduct;
	CString strFutOpt;
	CString strSpread;
	CString strFileFormat;
	unsigned long long ulFileSize;

	ProductDetails()
	{
		strFileName = strFileType = strExchange = strProduct = strFutOpt = strSpread = strFileFormat = "";
		ulFileSize  = 0;
	}
};


typedef vector<ProductDetails> vec_ProductDetails;

class CME_Product_Calendar : public BaseImage
{
public:

	enum FieldIDs
	{
		e_lstFileType	= 200,
		e_lstExchange	= 203,		
		e_lstFileFormat = 204,

		e_dtFromDate	= 50,
		e_dtToDate		= 51,

		e_ProductName	= 1320,
		e_btnGetDetails	= 5,
		e_strStatus		= 6,

		e_btnPageUp		= 90,
		e_btnPageDn		= 91,

		e_strFileType	= 1050,
		e_strFileName	= 1000 ,		
		e_strFileDate	= 1100,
		e_strExchange	= 1150,
		e_strProduct	= 1200,
		e_strFutOption	= 1250,
		e_strSpread		= 1300,
		e_strFileFormat	= 1350,
		e_size			= 1400,

		e_Label_Navigate = 65
	};

	CME_Product_Calendar();
	~CME_Product_Calendar();	

	bool InitData();		
	void Recalculate();	

	void fnPageUpDown();

	bool InitDataBase();
	
	void  CME_Product_Calendar::GetProductDetails();
	void  CME_Product_Calendar::DisplayProductDetails();

	vec_ProductDetails m_vecProductDetails;
private:

	DBConnection m_clDBConn;
	DBInterfaceNew	* m_clpDBInt;

	ListField * m_lstExchange;
	ListField *	m_lstFileType;
	ListField *	m_lstFileFormat;

	BaseField * m_strStatus ;
	BaseField * m_strProduct ;

	int m_numofRows;
	int m_curTopRow;
};

BF_NAMESPACE_END


