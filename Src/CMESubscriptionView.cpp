
#include <stdafx.h> 
#include<fstream>
#include<string>
#include "CMESubscriptionView.hpp"
//#include <shared/smtp.hpp>
#include <MailAutomation/MailAutomation.h>
#include "afxsock.h"

#define MAX_ROWS 20


BF_NAMESPACE_USE

using namespace std;

IMPLEMENT_IMAGE(CMESubscriptionView)

CMESubscriptionView::CMESubscriptionView()
{	
}

CMESubscriptionView::~CMESubscriptionView()
{
}

bool CMESubscriptionView::InitData()
{
	
	m_clDBConn.fnAddRequeryTable("CME_Security_Definition_Mst");
	
	//-------------------------------------------------------------------------
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	//-------------------------------------------------------------------------
	CString m_csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	m_csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, m_csServerName, "CME_BCP","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}
	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	//-------------------------------------------------------------------------
	//CString m_csConnect.Format("Driver={SQL Server};Server=beastdb;Failover_Partner=beastdb;Database=CME_BCP;Uid=watchdog;Pwd=watchdog;");

	//m_csConnect.Format("Driver={SQL Server Native Client 10.0};Server=%s;Failover_Partner=beastdb2;Database=%s;Trusted_Connection=yes;",m_clDBConn.GetServerName(), m_clDBConn.GetDatabaseName());	
	//m_csConnect.Format("Provider=SQLOLEDB.1; Server=beastdb; Database=TradeCapture; Integrated Security=SSPI;");
	//m_csConnect.Format("Driver={SQL Server Native Client 10.0};Server=beastdb;Failover_Partner=beastdb;Database=TradeCapture;Uid=watchdog;Pwd=watchdog;");
	//m_csConnect.Format("Driver={SQL Server Native Client 10.0};Server=%s;Failover_Partner=beastdb2;Database=%s;Trusted_Connection=yes;",m_clDBConn.GetServerName(), m_clDBConn.GetDatabaseName());	

	
	//GetConnectionParams();
	
	//fnGetFileDownloded();
		
	return true;
}

void CMESubscriptionView::Recalculate()
{	
	bool bCME_Security_Definition_Mst = false;
	MQDBNHandler *pMsmq = m_clDBConn.GetMSMQ();
	
	if( pMsmq )
	{
		while( pMsmq->HasEvent() )
		{
			const DbEventInfo * event = pMsmq->GetNextEvent();

			if( event->m_table.CompareNoCase("CME_Security_Definition_Mst") == 0 )
			{
				bCME_Security_Definition_Mst = true;
			}	

		}
	}

	
}

void CMESubscriptionView::GetSubscriptionView()
{
	CString csOrderType = GetField(e_OrderFilter)->GetValueString();
	
		CString sql;
		sql.Format("EXEC proc_beast_get_CME_Subscription 's'",csOrderType);

		_RecordsetPtr set;
		if( FAILED(m_clpdbInt->GetRecordset(&set, sql ,false)))
		{
			SetErrorMessage(ErrorSeverity::e_Error, sql);
			return;
		}

		CME_Subscription stSubsciption;
		while( VARIANT_FALSE == set->adoEOF )
		{
			//SETLONG(stSubsciption.Cust_Id ,	set->Fields->Item[_variant_t("Cust_Id")]->Value);
			SETSTR(stSubsciption.csExchange ,	set->Fields->Item[_variant_t("ExchangeCode")]->Value);
			SETSTR(stSubsciption.csProduct ,	set->Fields->Item[_variant_t("ProductCode")]->Value);
			SETSTR(stSubsciption.csFOI ,	set->Fields->Item[_variant_t("FOI")]->Value);
			SETSTR(stSubsciption.csType ,	set->Fields->Item[_variant_t("TYPE")]->Value);
			//SETSTR(stSubsciption.csDateRange ,	set->Fields->Item[_variant_t("csDateRange")]->Value);
			SETSTR(stSubsciption.csVenue ,	set->Fields->Item[_variant_t("Venue")]->Value);
			SETSTR(stSubsciption.csFileFormat ,	set->Fields->Item[_variant_t("File Format")]->Value);
			//SETSTR(stSubsciption.csSubsPeriod ,	set->Fields->Item[_variant_t("csSubsPeriod")]->Value);
			SETSTR(stSubsciption.csDelType ,	set->Fields->Item[_variant_t("Delivery Type")]->Value);
			SETSTR(stSubsciption.csStatus ,	set->Fields->Item[_variant_t("DeliveryStatus")]->Value);

			CME_SubscriptionV.push_back(stSubsciption);
			set->MoveNext();

		}
		set->Close();
}

void CMESubscriptionView::DisplaySubscriptionView()
{
	int iCount = CME_SubscriptionV.size();
	
	CME_Subscription stSubsciption;
	for(int i=0; i < iCount && i < 20; i++)
	{
		stSubsciption = CME_SubscriptionV[i];

		GetField(e_Exchange + i)->SetValueString(stSubsciption.csExchange);	
		GetField(e_Product + i)->SetValueString(stSubsciption.csProduct);			
		GetField(e_FOI + i)->SetValueString(stSubsciption.csFOI);				
		GetField(e_Type + i)->SetValueString(stSubsciption.csType);				
		GetField(e_DateRange + i)->SetValueString(stSubsciption.csDateRange);			
		GetField(e_Venue + i)->SetValueString(stSubsciption.csVenue);				
		GetField(e_FileFormat + i)->SetValueString(stSubsciption.csFileFormat);		
		GetField(e_SubsPeriod + i)->SetValueString(stSubsciption.csSubsPeriod);		
		GetField(e_DeliveryType + i)->SetValueString(stSubsciption.csDelType);		
		GetField(e_Status + i)->SetValueString(stSubsciption.csStatus);		
	}
}