// Copyright (C) 2002 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#include "stdafx.h"
#include "CME_INVOICE.hpp"
#include <fields/ListField.hpp>

// Put this	after all the include statements

BF_NAMESPACE_USE
	
IMPLEMENT_IMAGE(CME_INVOICE)	

bool CME_INVOICE::InitData()
{
	return true;
} 

void CME_INVOICE::Recalculate()
{

}

