
#pragma once

#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <src/LogManager.h>
#include "..\Src\MessageUtil.h"

BF_NAMESPACE_BEGIN

using namespace std;

enum enmImageCode
{
	Img_FixText		= 4054,
	Img_FixBinary	= 4058,
	Img_FixTextAuto = 4084,
};

struct stSchedule
{
	bool bValid;
	int m_nStatus;

	CString strProcessingDate;
	CString strInputFolder;
	CString strOutputFolder;
	CString strSecDefFolder;
	CString strContractFile;
	CString strMstSecDef;

	COleDateTime dtCurrSchTime;
	COleDateTime dtOrigSchTime;
};

enum enmOperation
{
	OP_FTPDownload,
	OP_SecDefGenerate,
	OP_S3Sync,
	OP_ProcessRunning,
	OP_NoFileToProcess,
	OP_ProcessCompleted,
	OP_CreateZip,
	OP_UploadSubscription,
	OP_EndOfDay,
	OP_None
};

struct SubscriptionDetail
{
	SubscriptionDetail()
	{
		iOrderId  = 0;

		stCUST_NAME = "";
		stDEST_ACCOUNT = "";
		stFTP_PASSWORD = "";
		stALL_PRODUCTS = "";
		stPROD_CODE = "";
		stEXCH_CODE = "";
		stFOICode = "";
		stOutputFileName = "";
	}

	int iOrderId;

	CString stCUST_NAME;
	CString stDEST_ACCOUNT;
	CString stFTP_PASSWORD;
	CString stALL_PRODUCTS;
	CString stPROD_CODE;
	CString stEXCH_CODE;
	CString stFOICode;
	CString stOutputFileName;	
};

struct FileGroup
{
	FileGroup()
	{
		iFileCount = 0;
		lFileSize = 0;
	}

	void Increment(long filesize)
	{
		iFileCount++;
		lFileSize += filesize;
	}

	int iFileCount;
	unsigned long lFileSize;
};

struct ReportFile 
{
	ReportFile()
	{
		bProductFile = true;
		csExchange = "";
		lFileSize = 0;
		csFileName = "";
		csFilePath = "";
		csDeliveryTime = "";
		csCheckSum = "";
	}

	bool bProductFile;
	CString csExchange;
	CString csFileName;	
	CString csFilePath;	
	CString csDeliveryTime;		
	CString csCheckSum;		
	unsigned long lFileSize;
};

typedef std::map<CString, FileGroup> FileGroupMap;
typedef FileGroupMap::iterator FileGroupItr;
typedef std::vector<ReportFile> ReportFileV;

struct UniqueAccount
{
	UniqueAccount()
	{
		iStatus = 999;
	}

	CString strDestAccount;
	int iStatus;

	vector<CString> vExchanges;
	set<CString> sKeys;

	// Reports
	FileGroup totalFiles;
	FileGroupMap exchangeGroup;
	ReportFileV vReportFiles;
};

typedef std::vector<SubscriptionDetail> SubscriptionV;
typedef std::map<CString, UniqueAccount> SubAccountMap;

class MDAvroProcessor : public BaseImage
{
public:
	MDAvroProcessor();
	virtual ~MDAvroProcessor();
	
	enum FieldIDs
	{
		e_InputFolder	= 1,
		e_OutputFolder	= 2,
		e_BasePath		= 4,
		
		e_StartProcess	= 10,
		e_StopProcess	= 11,

		e_FtpServer		= 3,
		e_FtpFilePath	= 5,
		e_SecDefParse	= 22,
		e_GenerateSub	= 23,

		e_DefaultDate   = 205,
		e_S3InputBucket = 58,

		e_CutOffHour	= 30,
		e_CutOffMin		= 31,
		e_RetryInterval	= 32,
		e_DownloadMsg	= 29,

		e_CopyFromS3	= 16,
		e_CreateZip		= 17,
		e_RemoveFiles	= 18,
		e_LoadSub		= 19,
		e_UploadALL		= 20,
		e_GetSecDef		= 21,

		e_MessageBase	= 5500,
		e_PgUpMsg		= 5530,
		e_PgDnMsg		= 5531,

		e_PgUpBase		= 90,
		e_PgDnBase		= 91,

		e_DestAccount	= 56,
		e_S3FTPBucket	= 57,

		e_SubscrID		= 1000,
		e_CUST_NAME		= 1050,
		e_FTP_Account	= 1150,
		e_VENUE_CODE	= 1200,
		e_ALL_PRODUCTS	= 1250,
		e_PROD_CODE		= 1300,
		e_EXCH_CODE		= 1400,
		e_OutputFile	= 1450,		
		e_Upload		= 1500,

		e_GenReport		= 2095,
		e_ClearGrid		= 2096,
	};
 
	void Parse();
	void ParseSecDef();
	void CopyS3FiletoLocal();
	void LoadSubscription();
	void fnPageUpDown();
	void DisplaySubscription();
	void GenerateSubscriptions();
	void UploadSubscription(CString csAccount = "");
	void CreateZip();
	void GetScheduleStatus();
	void fnUpdateScheduleStatus( int iStatus );
	bool FtpDownload();
	bool SecDefGenerate();
	void GenerateScheduleInfo();
	void fnGetCheksumList();
	CString GetCheksum(CString strFile);
	CString GenerateReport(UniqueAccount &objAccount);
	void fnInsertErrorReport(CString csDestAccount, CString csFileName, CString csExchnage, CString &csErrorCmd );
	void fnInsertDeliveryReport(CString csDestAccount, ReportFile &objReportFile, CString csAWSCommand );
	void fnSendEmail();

	void MultiThread_Zip();
	void MultiThread_Upload(CString csAccount);

	int m_numofRows;
	int m_curTopRow;
	bool m_bIsThreadRunning;
	bool m_bIsStopThread;
	CString m_csFtpSecDef;
	int m_nScreenID;

	LogManager	m_cLogManager;
	DBConnection	m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;
	
	stSchedule m_objSchedule;
	enmOperation m_enmOperation;

	SubAccountMap m_SubAccountMap;
	SubscriptionV m_vSubscriptions;

	std::map<CString,CString> m_clChecksumMap;
	
	std::vector<stFile> m_vTextFiles;
	
	int m_iCurrentFile;
	int m_nTotalFiles;
	int getNextFile();

private:
	bool InitData();
	void Recalculate();
};

BF_NAMESPACE_END