
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <shared/MDDDataHandler.hpp>

BF_NAMESPACE_BEGIN
using namespace std;

struct stFileData
{
	stFileData()
	{	
		strProductName="";
		strDestAcc = "";
		strFileName = "";	
		strExchange = "";
		strErrorCmd = "";
	}	

	CString strProductName;
	CString strDestAcc;
	CString strFileName;
	CString strExchange;
	CString strErrorCmd;
	
};

class CMEDailyErrorReport : public BaseImage
{
public:
	
	enum FieldIDs
	{
		e_Product           = 200,
		e_DeliveryDate      = 600,
		e_DestAcc           = 500,		
		
		e_SearchBtn     	= 117,				

		e_DisplayProductName = 2300,
		e_DisplayeDestAcc 	 = 2400,
		e_DisplayExchange	 = 2250,
		e_DisplayFileName	 = 2000,
		e_DisplayErrorCmd	= 2100,
		

		e_PgUp	= 92,
		e_PgDn	= 93,	
		e_Label_PgNo = 65,
				
	};
	
	CMEDailyErrorReport();
	~CMEDailyErrorReport();	

	bool InitData();		
	void Recalculate();	

	void InitDB();
	void GetFilterData();
	
	void DisplayGrid();
	void ManageUpDownButton();

	std::vector<stFileData> m_CmeDDReportData;

private:
		
	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;
	
	int m_iCurrTopRow;
	
	ListField *fld_ProductTyp;
};

BF_NAMESPACE_END


