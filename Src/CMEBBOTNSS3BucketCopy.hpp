
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>

#include "afxinet.h"

using namespace std;

BF_NAMESPACE_BEGIN

//#include <Microsoft/Windows/v6.0A/Include/odbcss.h>
//#pragma comment(lib,"/3rdParty/Microsoft/Windows/v6.0A/Lib/Odbcbcp.lib")

//const CString csUserName		= _T("s3://cmeftp/cme1");
//const CString csAccessKeyId		= _T("AKIAJ23QFO2U6LJWTMPQ");
//const CString csSecretAccessKey = _T("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");
//const CString csSourcePath		= _T("s3://cmegroup-main-datamine-prod-staging/daily/bbo/globex/");
//const CString csDestinationPath = _T("s3://cmeftpuat/ftpdata/");
//const CString csSourcePath		= _T("D:\\a*.txt");
//const CString csDestinationPath = _T("C:\\Test\\");

struct stBBOFiles
{
	stBBOFiles()
	{
		_strBBOFileName="";
		_fpOutPutFile =NULL;
	}
	std::string _strBBOFileName;
	std::ofstream * _fpOutPutFile;
};

struct BBOSubscriptionDtl
{
	BBOSubscriptionDtl()
	{
		strProductV.clear();
		strExchangeV.clear();

		fpOut = NULL;
		iFileCount = 0;
		iRowCount = 0;
		iOrderId  = 0;
		iBLOCKType = 0;
		iStatus = 0;
		iDelStatus = 0;

		stCUST_NAME = "";
		stCONTACT_EMAIL = "";
		stDEST_ACCOUNT = "";
		stFTP_PASSWORD = "";
		stVENUE_CODE = "";
		stALL_PRODUCTS = "";
		stPROD_CODE = "";
		stBBO_CODE = "";
		stEXCH_CODE = "";
		stOutputFileName = "";
	}

	int iOrderId;
	int iBLOCKType; // EOD, EDOE
	int iStatus;
	int iDelStatus;	

	std::vector<std::string> strExchangeV;
	std::vector<std::string> strProductV;

	CString stCUST_NAME;
	CString stCONTACT_EMAIL;
	CString stDEST_ACCOUNT;
	CString stFTP_PASSWORD;
	CString stVENUE_CODE;
	CString stALL_PRODUCTS;
	CString stPROD_CODE;
	CString stBBO_CODE;
	CString stEXCH_CODE;
	CString stOutputFileName;

	FILE *fpOut;
	std::vector<stBBOFiles> stVecFiles;
	int iFileCount;
	int iRowCount;
};

typedef std::vector<BBOSubscriptionDtl> BBOSubscriptionDtlV;

class CMEBBOTNSS3BucketCopy : public BaseImage
{
public:

	enum FieldIDs
	{
		e_sourcePath	= 58,
		e_destPath		= 57,

		e_DefaultDate	= 205,

		e_awsCommand = 3,
		e_getSubScription = 11,
		e_Status = 12,
		e_uploadAllFiles = 19,
		
		e_SubscrID = 1000,
		e_CUST_NAME = 1050,
		e_CONTACT_EMAIL = 1100,
		e_DEST_ACCOUNT_FTP_PASSWORD = 1150,
		e_VENUE_CODE = 1200,
		e_ALL_PRODUCTS = 1250,
		e_PROD_CODE = 1300,
		e_BBO_CODE	= 1350,
		e_EXCH_CODE = 1400,
		e_OutputFileName = 1450,
		e_PgDnBase = 90,
		e_PgUpBase = 91
	};

	CMEBBOTNSS3BucketCopy();
	~CMEBBOTNSS3BucketCopy();	

	bool InitData();		
	void Recalculate();

	void LoadSubscription();
	void fnPageUpDown( );
	void DisplaySubscription();

	void uploadSinglefile();
	void UploadAllFiles();	
	
private:
	bool _bIsProcess;
	BBOSubscriptionDtlV	m_clBBOSubV;
	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;

	int m_numofRows;
	int m_curTopRow;

	CString m_csS3Command;
};

BF_NAMESPACE_END
