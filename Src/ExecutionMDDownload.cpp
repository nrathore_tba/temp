#include <fstream>
#include <sstream>
#include <stdafx.h> 
#include <shldisp.h>
#include <tlhelp32.h>
#include "zip.h"
#include "ExecutionMDDownload.hpp"

#define SPLITLINE 20000

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(ExecutionMDDownload)

ExecutionMDDownload::ExecutionMDDownload()
{	
}

ExecutionMDDownload::~ExecutionMDDownload()
{
}

bool ExecutionMDDownload::InitData()
{
	GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));

	m_curTopRow= 0;
	for(m_numofRows = 0; GetField(e_LogMsg + m_numofRows); m_numofRows++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	//---------------------------------------------
	if( !IsRestoreUpdate( ) )
	{
		COleDateTime dtToday = COleDateTime::GetCurrentTime();
		BFDate bfToday;
		bfToday.SetYearMonthDate(dtToday.GetYear(),dtToday.GetMonth() - 1, dtToday.GetDay() );
		GetField(e_DateFilter)->SetValueDate(bfToday); 
	}

	fld_OutputPath = GetField(e_OutputPath);

	m_clScheduleInfo.bValid = false;
	m_clScheduleInfo.iRetInterval = GetField(e_RtryInterval)->GetValueInt() > 0 ? GetField(e_RtryInterval)->GetValueInt(): 10;
	
	m_bIsThreadRunning = false;
	m_bIsTrheadCompleted = true;

	GetField(e_HadoopS3IN)->SetVisible(false);
	GetField(e_LocalToHadoopIn)->SetVisible(false);
	GetField(e_Upload)->SetVisible(false);
	
	m_enmOperation = OP_None;
	return true;
}

const int		__niMsgCouunt = 200;
CString __csMsgArry[__niMsgCouunt] = { "" };

void ExecutionMDDownload::fnAddMessage( CString csaNewMsg )
{	
	for( int iI = __niMsgCouunt-1; iI > 0; iI-- )
		__csMsgArry[iI] = __csMsgArry[iI -1];
	
	__csMsgArry[0] = csaNewMsg;	
}

void ExecutionMDDownload::fnDisplayMsg()
{
	int iRow = 0;
	for( int iIndex = m_curTopRow; iIndex < __niMsgCouunt; iIndex++, iRow++ )
	{
		if( iRow >= m_numofRows )
			break;
		
		BaseField * fldComment		= GetField(e_LogMsg + iRow);
		
		fldComment->SetValueString( __csMsgArry[iIndex] );
		fldComment->SetNotManual();
	}
	
	while( iRow < m_numofRows )
	{
		BaseField * fldComment		= GetField(e_LogMsg + iRow);
		fldComment->SetBlankState();
		iRow++;
	}
}

UINT BinaryUploadProcessor(void *pVoid)
{
	ExecutionMDDownload *pParser = (ExecutionMDDownload*)pVoid;
	
	pParser->m_bIsThreadRunning = true;
	pParser->m_bIsTrheadCompleted = false;
	
	pParser->fnUpdateScheduleStatus( 1 );
	
	pParser->fnBinaryUploadProcessor();

	pParser->fnUpdateScheduleStatus( 2 );
	pParser->m_enmOperation = OP_ProcessCompleted;

	pParser->m_bIsThreadRunning = false;
	pParser->RequestExternalUpdate();

	return 0;
}

void ExecutionMDDownload::Recalculate()
{
	//LoadTextSubscription();
	//CreateTextSubscriptionFile();
	//return;

	if( IsFieldNew(e_RtryInterval) )
		m_clScheduleInfo.iRetInterval = GetField(e_RtryInterval)->GetValueInt() > 0 ? GetField(e_RtryInterval)->GetValueInt(): 10;

	if( m_bIsThreadRunning )
	{
		GetField(e_MsgTitle)->SetTitle(m_csMsg);		
	}
	else
	{
		if( !m_bIsThreadRunning && !m_bIsTrheadCompleted )
		{
			// After completion of thread
			GetField(e_MsgTitle)->SetTitle("Message");
			m_bIsTrheadCompleted = true;
		}

		if( !m_clScheduleInfo.bValid || m_enmOperation == OP_ProcessCompleted || IsFieldNew(e_ReloadSchedule) )
		{		
			fnGetNextSchedule( );
		}

		if( !m_clScheduleInfo.bValid )	
		{
			StartRecalcTimer(60, false);
			return;
		}

		if( m_enmOperation == OP_Process /*|| IsFieldNew(e_Upload)*/ )
		{
			m_enmOperation = OP_Process;
			AfxBeginThread(BinaryUploadProcessor, this);
		}

		/*if( m_enmOperation == OP_S3HadoopSync || IsFieldNew(e_LocalToHadoopIn) )
		{
			fnCopyLocalToHadoop( );
		}

		if( m_enmOperation == OP_Download )
		{
			fnCopyS3FiletoLocal( );
		}*/

		if( m_enmOperation == OP_None )
			fnRecalc1( );

		if( m_enmOperation != OP_None && m_enmOperation != OP_ReadThread )
			StartRecalcTimer(0, false);
	}

	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_pgUP,e_pgDN,200,m_numofRows,m_curTopRow);		
	fnDisplayMsg();	
}

void ExecutionMDDownload::fnRecalc1( )
{
	COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
	long lRecalcTime = 0;

	if( GetField( e_ManualRun )->GetValueInt() == 1 )
	{
		GetField(e_NextDownloadMsg)->SetTitle("Processing Set To Manaul");
	}
	else if( dtCurrentTime >= m_clScheduleInfo.clScheduleDtTime )
	{
		//Need to Process subscription...
		GetField(e_NextDownloadMsg)->SetTitle("Processing File");
		m_enmOperation = OP_Process;//OP_Download;		
		fnAddMessage("Downloading File");
	}
	else if( dtCurrentTime < m_clScheduleInfo.clScheduleDtTime )
	{
		COleDateTimeSpan dtSpan = m_clScheduleInfo.clScheduleDtTime - dtCurrentTime;
		lRecalcTime = dtSpan.GetTotalSeconds();

		CString csTimer,csTmp;

		int iDays =	dtSpan.GetDays();
		int iLeftTotalHr = dtSpan.GetHours();
		int iLeftTotalMinute = dtSpan.GetMinutes();
		int iLeftTotalSecond = dtSpan.GetSeconds();

		if( iDays > 0 )
		{
			csTmp.Format("%d Day and ",iDays);					
			csTimer += csTmp;

			csTmp.Format("%02d:",iLeftTotalHr);					
			csTimer += csTmp;
		}
		else
		{
			if( iLeftTotalHr > 0 )
			{					
				csTmp.Format("%02d:",iLeftTotalHr);					
				csTimer += csTmp;
			}
		}

		csTmp.Format("%02d:",iLeftTotalMinute);					
		csTimer += csTmp;

		csTmp.Format("%02d",iLeftTotalSecond);
		csTimer += csTmp;				

		int iDate	= m_clScheduleInfo.clScheduleDtTime.GetDay();
		int iMonth	= m_clScheduleInfo.clScheduleDtTime.GetMonth();
		int iYear	= m_clScheduleInfo.clScheduleDtTime.GetYear();
		int iHour	= m_clScheduleInfo.clScheduleDtTime.GetHour();
		int iMinute = m_clScheduleInfo.clScheduleDtTime.GetMinute();
		int iSecond = m_clScheduleInfo.clScheduleDtTime.GetSecond();

		CString csStr;
		csStr.Format("Next Download: %d-%d-%d %2d:%2d:%2d [Timer: %s]",iDate,iMonth,iYear,iHour,iMinute,iSecond, csTimer);

		GetField(e_NextDownloadMsg)->SetTitle(csStr);
		GetField(e_NextDownloadMsg)->SetTitleForeColor(ColorManager::eSignalPositiveLight);

		if( lRecalcTime > 4 || lRecalcTime < 0 )
			lRecalcTime = 4;
	}

	if( lRecalcTime )
		StartRecalcTimer(lRecalcTime, false);
}

void ExecutionMDDownload::fnGetNextSchedule( )
{
	m_clScheduleInfo.bValid = false;
	m_enmOperation = OP_None;

	BFDate bfdt = GetField(e_DateFilter)->GetValueDate();

	CString csStrDtFilter;
	csStrDtFilter.Format("%d-%02d-%02d 00:00:00",bfdt.GetYear(), bfdt.GetMonth()+1, bfdt.GetDate());

	CString sql;
	sql.Format("SELECT top 1* from dbo.CME_Subscription_Schedule WHERE FileType ='MD' and SubType = 'FBinary' and IsNull(Status,0) In(0,1) and ScheduleDateTime > '%s' ORDER BY ScheduleDateTime", csStrDtFilter);

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		GetField(e_NextDownloadMsg)->SetTitle("Error while Loading Schedule");
		return;
	}
		
	if( VARIANT_FALSE == set->adoEOF )
	{
		SETLONG( m_clScheduleInfo.iStatus,		set->Fields->Item[_variant_t("Status")]->Value);	
		SETOLEDATE(	m_clScheduleInfo.clScheduleDtTime,	set->Fields->Item[_variant_t("ScheduleDateTime")]->Value);	
		m_clScheduleInfo.clMainScheduleDtTime = m_clScheduleInfo.clScheduleDtTime;

		m_clScheduleInfo.iRetryCount = 0;
		m_clScheduleInfo.csDate.Format("%d%02d%02d", m_clScheduleInfo.clScheduleDtTime.GetYear(), m_clScheduleInfo.clScheduleDtTime.GetMonth(), m_clScheduleInfo.clScheduleDtTime.GetDay() );

		m_clScheduleInfo.bValid = true;
	}

	set->Close();

	if( m_clScheduleInfo.bValid  )
	{
		GetField( e_Year )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetYear() );
		GetField( e_Month )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetMonth() );
		GetField( e_Day )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetDay() );
		GetField( e_Hour )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetHour() );
		GetField( e_Minute )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetMinute() );
	}
	else
	{
		GetField(e_NextDownloadMsg)->SetTitle("No Schedule Loaded");
	}
}

void ExecutionMDDownload::fnUpdateScheduleStatus( int iStatus )
{
	CString csdttime;
	csdttime.Format("%d-%02d-%02d %02d:%02d:00",m_clScheduleInfo.clMainScheduleDtTime.GetYear(), m_clScheduleInfo.clMainScheduleDtTime.GetMonth(), m_clScheduleInfo.clMainScheduleDtTime.GetDay(), m_clScheduleInfo.clMainScheduleDtTime.GetHour(), m_clScheduleInfo.clMainScheduleDtTime.GetMinute());
	
	COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
	CString csTimeStatus;
	csTimeStatus.Format("%d-%02d-%02d %02d:%02d:00",dtCurrentTime.GetYear(), dtCurrentTime.GetMonth(), dtCurrentTime.GetDay(), dtCurrentTime.GetHour(), dtCurrentTime.GetMinute());

	CString sql;
	if( iStatus == 1 )
		sql.Format("UPDATE dbo.CME_Subscription_Schedule SET Status = %d, ProcessStart = '%s' WHERE FileType ='MD' and SubType = 'FBinary' and ScheduleDateTime = '%s'", iStatus, csTimeStatus, csdttime);
	else
		sql.Format("UPDATE dbo.CME_Subscription_Schedule SET Status = %d,   ProcessEnd = '%s' WHERE FileType ='MD' and SubType = 'FBinary' and ScheduleDateTime = '%s'", iStatus, csTimeStatus, csdttime);

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		GetField(e_NextDownloadMsg)->SetTitle("Error while Loading Schedule");
		return;
	}
}

//void ExecutionMDDownload::fnCopyS3FiletoLocal( )
//{
//	CString strBasePath = fld_OutputPath->GetValueString();
//
//	CString csInputBucket = ((ListField*)GetField(e_S3CMEBucket))->GetShortString();
//
//	CString strFullInputPath;
//	strFullInputPath.Format("%s/daily/marketdepth/%s/", csInputBucket, m_clScheduleInfo.csDate);
//
//	CString csDestinationFolder;
//	csDestinationFolder.Format("%s\\%s\\",strBasePath, m_clScheduleInfo.csDate);
//
//	//"s3 cp s3://cmegroup-main-datamine-qa-staging/daily/endofday/20150805/EOD_20150805_E.gz  d:\daily\endofday\20150805\";
//	CString csAWSCommand;
//	csAWSCommand.Format(_T("aws s3 sync --profile cmeprod %s %s"), strFullInputPath, csDestinationFolder);					
//	int iValue = system(csAWSCommand);	
//
//	if( iValue == 0 )
//	{
//		fnAddMessage(csAWSCommand + " File Downloaded successfully");
//		m_enmOperation = OP_S3HadoopSync;
//	}
//	else
//	{
//		m_clScheduleInfo.clScheduleDtTime = COleDateTime::GetCurrentTime();;
//		m_clScheduleInfo.clScheduleDtTime += COleDateTimeSpan(0,0,m_clScheduleInfo.iRetInterval,0);
//		m_clScheduleInfo.iRetryCount++;
//
//		CString csTemp;
//		csTemp.Format("Error cmd %s, File Download schedule after %d minute", csAWSCommand, m_clScheduleInfo.iRetInterval );
//		fnAddMessage( csTemp );
//
//		m_enmOperation = OP_None;
//	}
//}
//
//void ExecutionMDDownload::fnCopyLocalToHadoop( )
//{
//	CString strBasePath = fld_OutputPath->GetValueString();
//
//	CString csOutputBucket = ((ListField*)GetField(e_HadoopS3IN))->GetShortString();
//
//	CString strFullInputPath;
//	strFullInputPath.Format("%s\\%s\\", strBasePath, m_clScheduleInfo.csDate);
//
//	CString csDestinationFolder;
//	csDestinationFolder.Format("%s%s/",csOutputBucket, m_clScheduleInfo.csDate);
//
//	//"s3 cp s3://cmegroup-main-datamine-qa-staging/daily/endofday/20150805/EOD_20150805_E.gz  d:\daily\endofday\20150805\";
//	CString csAWSCommand;
//	csAWSCommand.Format(_T("aws s3 sync %s --profile s3user %s"), strFullInputPath, csDestinationFolder);					
//	int iValue = system(csAWSCommand);	
//
//	if( iValue == 0 )
//	{
//		fnAddMessage(csAWSCommand + " File sync to hadoop successfully");
//		m_enmOperation = OP_None;
//	}
//	else
//	{
//		m_clScheduleInfo.clScheduleDtTime = COleDateTime::GetCurrentTime();;
//		m_clScheduleInfo.clScheduleDtTime += COleDateTimeSpan(0,0,m_clScheduleInfo.iRetInterval,0);
//		m_clScheduleInfo.iRetryCount++;
//
//		CString csTemp;
//		csTemp.Format("Error cmd %s, File sync to hadoop schedule after %d minute", csAWSCommand, m_clScheduleInfo.iRetInterval );
//		fnAddMessage( csTemp );
//
//		m_enmOperation = OP_None;
//	}
//}

void ExecutionMDDownload::fnBinaryUploadProcessor( )
{	
	//---------------------
	CString csTemp = "Creating Binary Subscription";	
	fnAddMessage( csTemp );
	this->RequestExternalUpdate();

	LoadBinarySubscription();

	CreateBinarySubscriptionFile();

	csTemp = "Creating Text Subscription";	
	fnAddMessage( csTemp );
	this->RequestExternalUpdate();

	LoadTextSubscription();

	CreateTextSubscriptionFile();

	csTemp = "Process Completed..";
	fnAddMessage( csTemp );	
	this->RequestExternalUpdate();	
}

void ExecutionMDDownload::LoadBinarySubscription( )
{	
	m_clMDBinaryProdSubDtlMap.clear(); 
		
	/*CString sql;	
	sql.Format("SELECT CUST_NAME, CONTACT_EMAIL, DEST_ACCOUNT, FTP_PASSWORD, VENUE_CODE, ALL_PRODUCTS, PROD_CODE, MD_CODE, EXCH_CODE FROM dbo.subscription_2015 where FILE_TYPE = 'MD' and Active = 1");
	
	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		return;
	}

	while( VARIANT_FALSE == set->adoEOF )
	{
		SubscriptionDetail clbinSub;
				
		SETSTR(	clbinSub.stCUST_NAME,		set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
		SETSTR(	clbinSub.stCONTACT_EMAIL,	set->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);	
		SETSTR(	clbinSub.stDEST_ACCOUNT,	set->Fields->Item[_variant_t("DEST_ACCOUNT")]->Value);	
		SETSTR(	clbinSub.stFTP_PASSWORD,	set->Fields->Item[_variant_t("FTP_PASSWORD")]->Value);	
		SETSTR(	clbinSub.stVENUE_CODE,		set->Fields->Item[_variant_t("VENUE_CODE")]->Value);	
		SETSTR(	clbinSub.stALL_PRODUCTS,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);	

		SETSTR(	clbinSub.stPROD_CODE,	set->Fields->Item[_variant_t("PROD_CODE")]->Value);	
		SETSTR(	clbinSub.stMD_CODE,	set->Fields->Item[_variant_t("MD_CODE")]->Value);	
		SETSTR(	clbinSub.stEXCH_CODE,	set->Fields->Item[_variant_t("EXCH_CODE")]->Value);
		
		m_clMDBinaryProdSubDtlMap.insert( std::pair<string, SubscriptionDetail>(clbinSub.stDEST_ACCOUNT, clbinSub) );

		set->MoveNext();
	}

	set->Close();*/

	CString csFileFormat;

	CString sql;	
	sql.Format("EXEC Proc_Beast_Get_Subscriptions 'MD', 2");
	
	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		return;
	}

	while( VARIANT_FALSE == set->adoEOF )
	{
		SubscriptionDetail clbinSub;
		
		//SETSTR(	clbinSub.stCUST_NAME,		set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
		//SETSTR(	clbinSub.stCONTACT_EMAIL,	set->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);	
		SETSTR(	clbinSub.stDEST_ACCOUNT,		set->Fields->Item[_variant_t("FTPUserName")]->Value);	
		SETSTR(	clbinSub.stFTP_PASSWORD,		set->Fields->Item[_variant_t("FTPPassword")]->Value);
		//SETSTR(	clbinSub.stVENUE_CODE,		set->Fields->Item[_variant_t("VENUE_CODE")]->Value);	
		//SETSTR(	clbinSub.stALL_PRODUCTS,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);	

		//SETSTR(	clbinSub.stPROD_CODE,	set->Fields->Item[_variant_t("PROD_CODE")]->Value);	
		SETSTR(	clbinSub.stMD_CODE,			set->Fields->Item[_variant_t("PROD_CODE")]->Value);	
		SETSTR(	clbinSub.stEXCH_CODE,		set->Fields->Item[_variant_t("EXCH_CODE")]->Value);

		SETSTR(	clbinSub.stAsset_Class,		set->Fields->Item[_variant_t("Asset_Class")]->Value);
		SETSTR(	clbinSub.stEXCH_CODE_New,	set->Fields->Item[_variant_t("Exchange")]->Value);

		SETSTR(	csFileFormat,	set->Fields->Item[_variant_t("FILE_Format")]->Value);
		
		if( csFileFormat == "OTW")
		{
			/*if( clbinSub.stEXCH_CODE == "All Exchanges" )
			{
			}
			else*/
			{
				std::pair <MDBinarySubDtlMap::iterator, MDBinarySubDtlMap::iterator> ret;
				ret = m_clMDBinaryProdSubDtlMap.equal_range(clbinSub.stDEST_ACCOUNT.GetString());

				bool bAlreadyExist = false;
				for (MDBinarySubDtlMap::iterator it = ret.first; it != ret.second; ++it)
				{
					if( it->second.stEXCH_CODE == clbinSub.stEXCH_CODE )
					{
						bAlreadyExist = true;
						break;
					}			
				}

				if( !bAlreadyExist )
				{
					m_clMDBinaryProdSubDtlMap.insert( std::pair<string, SubscriptionDetail>(clbinSub.stDEST_ACCOUNT, clbinSub) );
				}
			}
		}

		set->MoveNext();
	}

	set->Close();

	CString csTemp;
	csTemp.Format("%d MD Subscription loaded", m_clMDBinaryProdSubDtlMap.size() );
	fnAddMessage( csTemp );
}

void ExecutionMDDownload::CreateBinarySubscriptionFile()
{
	CString csOutPath = GetField(e_OutputPath)->GetValueString();
	CString __csDestinationPath = ((ListField*)GetField(e_S3FTPBucket))->GetShortString();

	CreateDirectory(csOutPath, NULL);

	CString csSubscFile;
	csSubscFile.Format("%s\\RawSubscription_%s",csOutPath,m_clScheduleInfo.csDate);

	FILE *fileIn(NULL);
	
	if( fopen_s(&fileIn, csSubscFile, "w") != 0 )
	{
		fnAddMessage( "Error while Opening fileIn" );
		return;
	}

	MDBinarySubDtlMap::iterator itr =  m_clMDBinaryProdSubDtlMap.begin( );

	while( itr != m_clMDBinaryProdSubDtlMap.end() )
	{		
		CString csDestAccount = itr->second.stDEST_ACCOUNT;

		CString csDestinationFolder;
		csDestinationFolder.Format("%s#\"%s%s/MD_Raw/\"#*\n",csDestAccount,__csDestinationPath,  csDestAccount);

		fputs(csDestinationFolder,fileIn);
		itr++;
	}

	fclose( fileIn );

	CString __csSourceHadooPath = ((ListField*)GetField(e_HadoopS3OUT))->GetShortString();

	CString csAWSCommand;
	csAWSCommand.Format(_T("aws s3 cp --profile s3user %s %s"), csSubscFile, __csSourceHadooPath);					
	int iUploadReturn = system(csAWSCommand);	

	if( iUploadReturn == 0 )
	{
		CString stroutput;
		stroutput.Format("cmd %s", csAWSCommand );
		fnAddMessage( stroutput );

		unsigned long lsize = 0;
		fnInsertDeliveryReport("MDBinary", "BinarySubFile", m_clScheduleInfo.csDate, lsize);
	}
	else
	{
		CString stroutput;
		stroutput.Format("Error in upload cmd %s", csAWSCommand );
		fnAddMessage( stroutput );

		fnInsertErroReport("MDBinary", "BinarySubFile", m_clScheduleInfo.csDate, "", csAWSCommand );
	}
}

// Text__________________
bool Sort_Acc(SubscriptionDetail &lhs, SubscriptionDetail &rhs)
{
	if( lhs.stDEST_ACCOUNT.CompareNoCase(rhs.stDEST_ACCOUNT) == 0)
	{
		if( lhs.stALL_PRODUCTS.CompareNoCase(rhs.stALL_PRODUCTS) == 0)
		{
			if( lhs.stEXCH_CODE.CompareNoCase(rhs.stEXCH_CODE) == 0)
			{
				if( lhs.stPROD_CODE.CompareNoCase(rhs.stPROD_CODE) == 0)
				{
					return ( lhs.stFOICode < rhs.stFOICode );				
				}
				return ( lhs.stPROD_CODE < rhs.stPROD_CODE );
			}
			else
				return ( lhs.stEXCH_CODE < rhs.stEXCH_CODE );
		}
		else
			return ( lhs.stALL_PRODUCTS > rhs.stALL_PRODUCTS );
	}
	else
		return ( lhs.stDEST_ACCOUNT < rhs.stDEST_ACCOUNT );
}

void ExecutionMDDownload::LoadTextSubscription()
{
	m_vSubscriptions.clear(); 
	
	CString sql;	
	sql.Format("SELECT CUST_NAME, CONTACT_EMAIL, DEST_ACCOUNT, FTP_PASSWORD, ALL_PRODUCTS, PROD_CODE, MD_CODE, EXCH_CODE, FOI_CODE FROM dbo.subscription_2015 where FILE_TYPE = 'MD' and Active = 1");

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		return;
	}

	int iCount(1);
	while(VARIANT_FALSE == set->adoEOF)
	{
		SubscriptionDetail clEODSub;

		clEODSub.iOrderId = 0;

		SETSTR(	clEODSub.stCUST_NAME,		set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
		SETSTR(	clEODSub.stDEST_ACCOUNT,	set->Fields->Item[_variant_t("DEST_ACCOUNT")]->Value);	
		SETSTR(	clEODSub.stFTP_PASSWORD,	set->Fields->Item[_variant_t("FTP_PASSWORD")]->Value);	
		SETSTR(	clEODSub.stALL_PRODUCTS,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);	

		SETSTR(	clEODSub.stPROD_CODE,	set->Fields->Item[_variant_t("PROD_CODE")]->Value);	
		SETSTR(	clEODSub.stEXCH_CODE,	set->Fields->Item[_variant_t("EXCH_CODE")]->Value);
		SETSTR(	clEODSub.stFOICode,		set->Fields->Item[_variant_t("FOI_CODE")]->Value);

		if( clEODSub.stPROD_CODE.CompareNoCase("null") == 0 )
			clEODSub.stPROD_CODE.Empty();

		if(clEODSub.stALL_PRODUCTS.CompareNoCase("0") == 0)
		{
			if( clEODSub.stFOICode.CompareNoCase("OPT") == 0 )
			{
				m_vSubscriptions.push_back( clEODSub );
				clEODSub.stFOICode = "OPT_SPD";
				m_vSubscriptions.push_back( clEODSub );
			}
			else if( clEODSub.stFOICode.CompareNoCase("FUT") == 0 )
			{
				m_vSubscriptions.push_back( clEODSub );
				clEODSub.stFOICode = "FUT_SPD";
				m_vSubscriptions.push_back( clEODSub );
			}
		}
		else
		{
			clEODSub.stFOICode = "";
			m_vSubscriptions.push_back( clEODSub );
		}

		iCount++;
		set->MoveNext();
	}

	set->Close();	

	sql.Format("EXEC Proc_Beast_Get_Subscriptions 'MD', 2");
	
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		return;
	}

	CString csFormat;
	int nFosType;

	while( VARIANT_FALSE == set->adoEOF )
	{
		SubscriptionDetail clEODSub;
		
		SETLONG( clEODSub.iOrderId,			set->Fields->Item[_variant_t("ORDER_ID")]->Value);
		SETSTR(	clEODSub.stCUST_NAME,		set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
		SETSTR(	clEODSub.stDEST_ACCOUNT,	set->Fields->Item[_variant_t("FTPUserName")]->Value);	
		SETSTR(	clEODSub.stFTP_PASSWORD,	set->Fields->Item[_variant_t("FTPPassword")]->Value);
		SETSTR(	clEODSub.stALL_PRODUCTS,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);	
		SETSTR(	clEODSub.stPROD_CODE,		set->Fields->Item[_variant_t("PROD_CODE")]->Value);	
		SETSTR(	clEODSub.stEXCH_CODE,		set->Fields->Item[_variant_t("Exchange")]->Value);
		SETLONG( nFosType,					set->Fields->Item[_variant_t("FOS_Type")]->Value);

		SETSTR(	csFormat,	set->Fields->Item[_variant_t("File_Format")]->Value);
		
		if( csFormat == "Text")
		{
			if( !clEODSub.stEXCH_CODE.IsEmpty() )
			{
				if( clEODSub.stPROD_CODE.CompareNoCase("null") == 0 )
					clEODSub.stPROD_CODE.Empty();

				bool isAll = clEODSub.stALL_PRODUCTS.CompareNoCase("1") == 0;
				bool isEmpty = clEODSub.stPROD_CODE.IsEmpty();

				if( isAll )
				{
					clEODSub.stFOICode = "";
					m_vSubscriptions.push_back( clEODSub );
				}
				else if( !isEmpty )
				{
					if( nFosType & 2 )
					{
						clEODSub.stFOICode = "FUT";
						m_vSubscriptions.push_back( clEODSub );
					}

					if( nFosType & 4 )
					{
						clEODSub.stFOICode = "OPT";
						m_vSubscriptions.push_back( clEODSub );
					}

					if( nFosType & 8 )
					{
						clEODSub.stFOICode = "FUT_SPD";
						m_vSubscriptions.push_back( clEODSub );
					}

					if( nFosType & 16 )
					{
						clEODSub.stFOICode = "OPT_SPD";
						m_vSubscriptions.push_back( clEODSub );
					}
				}
			}
		}

		set->MoveNext();
	}

	set->Close();

	sort(m_vSubscriptions.begin(), m_vSubscriptions.end(), Sort_Acc);

	CString csTemp;
	csTemp.Format("%d Subscription loaded", m_vSubscriptions.size(), Sort_Acc);
	//DisplaySubscription();
}

void ExecutionMDDownload::CreateTextSubscriptionFile()
{
	SubAccountMap m_SubAccountMap;

	CString csOutPath = GetField(e_OutputPath)->GetValueString();
	//CString __csDestinationPath = ((ListField*)GetField(e_S3FTPBucket))->GetShortString();

	CreateDirectory(csOutPath, NULL);

	CString csSubscFile;
	csSubscFile.Format("%s\\TextSubscription_%s",csOutPath,m_clScheduleInfo.csDate);

	FILE *fileIn(NULL);
	
	if( fopen_s(&fileIn, csSubscFile, "w") != 0 )
	{
		fnAddMessage( "Error while Opening fileIn" );
		return;
	}
		
	CString csSubsrLine;
	csSubsrLine.Format("CustName#DestAccount#ALL#Exchange#ProdCode#FOICode\n");
	fputs(csSubsrLine,fileIn);

	CString csKey;

	for( int iI= 0; iI < m_vSubscriptions.size(); iI++ )
	{		
		SubscriptionDetail &clSub = m_vSubscriptions[iI];

		SubAccountMap::iterator itrAccount = m_SubAccountMap.find((LPCSTR)clSub.stDEST_ACCOUNT);

		if( itrAccount == m_SubAccountMap.end() )
		{
			UniqueAccount objAccount;
			objAccount.strDestAccount = clSub.stDEST_ACCOUNT;
			m_SubAccountMap[objAccount.strDestAccount] = objAccount;
			itrAccount = m_SubAccountMap.find(objAccount.strDestAccount);
		}

		if(clSub.stALL_PRODUCTS == "1")
			csKey.Format("%s", clSub.stEXCH_CODE);
		else
			csKey.Format("%s_%s_%s", clSub.stEXCH_CODE, clSub.stPROD_CODE, clSub.stFOICode);

		set<CString>::iterator itrKey = itrAccount->second.sKeys.find(clSub.stEXCH_CODE);
		if( itrKey != itrAccount->second.sKeys.end() )
			continue;

		itrKey = itrAccount->second.sKeys.find(csKey);
		if( itrKey != itrAccount->second.sKeys.end() )
			continue;

		itrAccount->second.sKeys.insert((LPCSTR)csKey);

		csSubsrLine.Format("%s#%s#%s#%s#%s#%s\n", clSub.stCUST_NAME, clSub.stDEST_ACCOUNT, clSub.stALL_PRODUCTS, clSub.stEXCH_CODE, clSub.stPROD_CODE, clSub.stFOICode);	

		fputs(csSubsrLine,fileIn);
	}

	fclose( fileIn );

	CString __csSourceHadooPath = ((ListField*)GetField(e_HadoopS3OUT))->GetShortString();

	CString csAWSCommand;
	csAWSCommand.Format(_T("aws s3 cp --profile s3user %s %s"), csSubscFile, __csSourceHadooPath);					
	int iUploadReturn = system(csAWSCommand);	

	if( iUploadReturn == 0 )
	{
		CString stroutput;
		stroutput.Format("cmd %s", csAWSCommand );
		fnAddMessage( stroutput );

		unsigned long lsize = 0;
		fnInsertDeliveryReport("MDText", "TextSubFile", m_clScheduleInfo.csDate, lsize);
	}
	else
	{
		CString stroutput;
		stroutput.Format("Error in upload cmd %s", csAWSCommand );
		fnAddMessage( stroutput );

		fnInsertErroReport("MDText", "TextSubFile", m_clScheduleInfo.csDate, "", csAWSCommand );
	}
}

//void ExecutionMDDownload::UploadSubscription()
//{	
//	CString csTemp;
//
//	CString __csDestinationPath = ((ListField*)GetField(e_S3FTPBucket))->GetShortString();
//	CString __csSourceHadooPath = ((ListField*)GetField(e_HadoopS3OUT))->GetShortString();
//	CString __csAccount = ((ListField*)GetField(e_DestAccount))->GetShortString(); // This is Test Account
//
//	MDBinarySubDtlMap::iterator itr =  m_clMDBinaryProdSubDtlMap.begin( );
//
//	while( itr != m_clMDBinaryProdSubDtlMap.end() )
//	{
//		CString csSourcePath;
//		csSourcePath.Format("%s%s/",__csSourceHadooPath, m_clScheduleInfo.csDate);
//
//		CString csDestinationFolder;
//		CString csDestAccount;
//		if( GetField(e_DestAccount)->GetValueInt() == 1 )
//			csDestAccount = itr->second.stDEST_ACCOUNT;
//		else
//			csDestAccount = __csAccount;
//
//		csDestinationFolder.Format("%s%s/MDBinary_%s/",__csDestinationPath,  csDestAccount, m_clScheduleInfo.csDate );
//
//		csTemp.Format("Uploading raw fix to %s Account", csDestAccount);
//		fnWriteMessage( csTemp );
//		this->RequestExternalUpdate();
//
//		CString csAWSCommand;
//		csAWSCommand.Format(_T("aws s3 cp --profile s3user --recursive %s %s"),csSourcePath, csDestinationFolder);					
//		int iUploadReturn = system(csAWSCommand);	
//
//		if( iUploadReturn == 0 )
//		{
//			CString stroutput;
//			stroutput.Format("cmd %s", csAWSCommand );
//			fnAddMessage( stroutput );
//			
//			unsigned long lsize = 0;
//			fnInsertDeliveryReport("MDBinary", csDestAccount, m_clScheduleInfo.csDate, lsize);
//		}
//		else
//		{
//			CString stroutput;
//			stroutput.Format("Error in upload cmd %s", csAWSCommand );
//			fnAddMessage( stroutput );
//
//			fnInsertErroReport("MDBinary", csDestAccount, m_clScheduleInfo.csDate, "", csAWSCommand );
//		}
//
//		itr++;
//
//		if( GetField(e_DestAccount)->GetValueInt() == 0 )
//			break;
//	}
//}

void ExecutionMDDownload::fnInsertDeliveryReport(CString csPrdouct, CString csDestAccount, CString csFileName, unsigned long &lFileSize )
{
	CString csCheckSum = "";

	CString csDeliveryTime;
	COleDateTime oleDateTime = COleDateTime::GetCurrentTime();
	csDeliveryTime.Format("%02d-%02d-%02d %02d:%02d", oleDateTime.GetYear(), oleDateTime.GetMonth(), oleDateTime.GetDay(), oleDateTime.GetHour(), oleDateTime.GetMinute());

	CString csSql;
	csSql.Format("EXEC Proc_Beast_Submit_CME_Daily_Delivery_Report '%s', '%s', '%s', %u, '%s','%s'", csPrdouct, csDestAccount, csFileName, lFileSize, csCheckSum, csDeliveryTime);
	
	_RecordsetPtr set;
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)csSql, true)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSql);
		PrintRawMessage(csSql);
		return;
	}
}

void ExecutionMDDownload::fnInsertErroReport(CString csPrdouct, CString csDestAccount, CString csFileName, CString csExchnage, CString &csErrorCmd )
{	
	CString csSql;
	csSql.Format("EXEC Proc_Beast_Submit_CME_Daily_Error_Report '%s', '%s', '%s', '%s', '%s'", csPrdouct, csDestAccount, csFileName, csExchnage, csErrorCmd);
	
	_RecordsetPtr set;
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)csSql, true)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSql);
		PrintRawMessage(csSql);
		return;
	}
}