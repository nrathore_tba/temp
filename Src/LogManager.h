#pragma once
#include "stdafx.h"
#include <shared/MDDDataHandler.hpp>

#include <map>
using namespace std;

BF_NAMESPACE_BEGIN

#define LOG_MESSAGE_ROWS	15
#define LOG_MESSAGE_COUNT	400

class LogManager
{
public:
	LogManager();
	~LogManager();

	void Init(BaseImage* image, int nMsgBase, int nPgUp, int nPgDn);
	void fnPageUpDown();
	void fnDisplayMessages();
	void fnAddMessage(CString csaNewMsg);

private:

	BaseImage*	m_pImage;

	int m_nMessageBase;
	int m_nPgUp;
	int m_nPgDn;
	int	m_nCurrIndex;		// Log Message Index
	int	m_nCurrTopRow;		// Index of Message Displayed at Top
	
	CString	m_csMsgArray[LOG_MESSAGE_COUNT]; // Log Message Array
};

struct SeqGapInfo
{
	int nAppId;
	int nSecurityId;
	string strFileName;
	__int64 nSeqLast;
	__int64 nSeqNext;
};

class SequenceCheck
{
private:
	static SequenceCheck *single;
	SequenceCheck() 
	{
		single = 0;
	}

public:
	static SequenceCheck* getInstance();

	~SequenceCheck()
	{
	}

	enum FieldIDs
	{
		e_SeqCheck  = 24,
		e_Channel	= 2000,
		e_Security	= 2100,
		e_FileName	= 2050,
		e_Sequence	= 2150,
		e_FileField	= 2097,
	};

	typedef map<int, __int64>  SequenceMap;
	vector<SeqGapInfo> vSeqGaps;

	void SetImage(BaseImage *pImage);
	void SetFile(string strcurrfile);
	void CheckSequence(int applID, __int64 sequence);
	void CheckSequenceSec(int securityId, __int64 sequence);
	void DisplaySeqGap();
	void Clear();
	void GenerateReport();

	BaseImage *m_pImage;
	string m_strcurrfile;

	SequenceMap ChannelSeqMap;
	SequenceMap ContractSeqMap;
};


BF_NAMESPACE_END

#include "afxinet.h"

class CFTPAutomation
{
public:
	CFTPAutomation();
	~CFTPAutomation();

	enum CDOption {CD_Root = 0, CD_OneLevelUp = 1, CD_AddToCurrent = 2, CD_GoToSpecified = 3	};

	bool Connect(CString sFTPServer, CString sUserName, CString sPassword, int nFTPPort, bool bPassiveMode);
	bool Disconnect();
	bool PutFiles(CString sSrcFile, CString sDstFolder);
	bool PutFolder(CString sSrcFolder, CString sDstFolder, CString sFileType);
	bool GetFiles(CString sSrcFile, CString sDstFolder);
	bool GetFolder(CString sSrcFolder, CString sDstFolder, CString sFileType);
	bool DeleteFiles(CString sSrcFile);
	bool DeleteFolder(CString sFolderName);
	bool RenameFiles(CString sOldName, CString sNewName);
	bool ChangeFolder(int iOption, CString sFolderName);
	bool CreateFolder(CString sFolderName);

	CString GetErrorMessage() { return m_szError; };

private:
	CString m_szError;
	CString m_szCurrentDirectory;

	CInternetSession	*m_pInternetSession;
	CFtpConnection		*m_pFtpConnection;

	void FillRemoteFileList(CString csPath, CStringArray &strFileNameArray);
	void FillLocalFileList(CString csPath, CStringArray &strFileNameArray);
	BOOL FillRemoteFilesFromFolder(CString csPath, CStringArray &strFileNameArray, CString csCriteria);
	BOOL FillLocalFilesFromFolder(CString csPath, CStringArray &strFileNameArray, CString csCriteria);
	bool DownloadFile(CString &csSrcFile, CString &csDstFile, CString csDirectory);
	bool UploadFile(CString &csSrcPath, CString &csDstFile, CString csDstPath);
	BOOL CreateLocalDirectory(CString csDirectory);
};