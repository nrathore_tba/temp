#include <stdafx.h>
#include "MessageSchema.h"
#include "MessageUtil.h"
#include <sstream>
#include <numeric>

Contract::Contract() 
{
	SecurityID = 0;
	ApplID = 0;
	SecurityExchange = "NULL";
	SecurityGroup = "";
	Asset = "NULL";
	CFICode = "";
	Symbol = "";
}

Contract::~Contract() 
{
}

string Contract::getContract()
{
	std::stringstream stream;

	stream << SecurityID << "," 
		<< ApplID << "," 
		<< SecurityExchange << "," 
		<< SecurityGroup << ","
		<< Asset << ","
		<< CFICode << ","
		<< Symbol;

	return stream.str();
}

string Contract::getType()
{
	string strCFICode = CFICode.substr(0, 2);
	string strType("");

	if(strCFICode == "FM")
		strType = "_FUT_SPD";
	else if(strCFICode == "OM")
		strType = "_OPT_SPD";
	else
	{
		strCFICode = CFICode.substr(0, 1);

		if(strCFICode == "F")
			strType = "_FUT";
		else if(strCFICode == "O")
			strType = "_OPT";
		else if(strCFICode == "M")
			strType = "_IRS";
	}

	return strType;
}

FixHeader::FixHeader()
{
	MsgTyp = "X";

	MsgSeqNum = 0;
	SendingTime = "";
	TransactTime = "";
	MatchEventIndicator = "00000000";
	TradeDate = "";
}

FixHeader::~FixHeader()
{
}

string FixHeader::calculateTags(string &strBody)
{
	stringstream stream;

	// Calculate BodyLength (Tag 9)
	stream.str("");
	stream << "1128=" << "9" << '\001';
	stream << "9=" << strBody.length() << '\001';
	stream << strBody;
	strBody = stream.str();

	// Calculate CheckSum (Tag 10)
	const unsigned char* iter = reinterpret_cast<const unsigned char*>( strBody.c_str() );
    int checkSum = (std::accumulate( iter, iter + strBody.length(), 0 ))%256;
	stream << "10=" << checkSum << '\001';

	return stream.str();
}

SecurityDefinition::SecurityDefinition() 
{
	MsgSeqNum = 0;
	SendingTime = "";

	MarketSegmentID = 0;
	MaturityMonthYear = "";
	SecuritySubType = "";
	UnderlyingProduct = 0;
	SecurityType = "";
	SecurityIDSource = "";

	TotNumReports = 0;
	SecurityUpdateAction = "";
	LastUpdateTime = "";
	PutOrCall = 0;
	UserDefinedInstrument = "";
	MDSecurityTradingStatus = 0;
	StrikePrice = 0;
	StrikeCurrency = "";
	Currency = "";
	SettlCurrency = "";
	MinCabPrice = 0;
	PriceRatio = 0;
	MatchAlgorithm = "";
	MinTradeVol = 0;
	MaxTradeVol = 0;
	MinPriceIncrement = 0;
	MinPriceIncrementAmount = 0;
	DisplayFactor = 0;
	TickRule = 0;
	MainFraction = 0;
	SubFraction = 0;
	PriceDisplayFormat = 0;
	ContractMultiplierUnit = 0;
	FlowScheduleType = 0;
	ContractMultiplier = 0;
	UnitOfMeasure = "";
	UnitOfMeasureQty = 0;
	DecayQty = 0;
	DecayStartDate = "";
	OriginalContractSize = 0;
	HighLimitPrice = 0;
	LowLimitPrice = 0;
	MaxPriceVariation = 0;
	TradingReferencePrice = 0;
	OpenInterestQty = 0;
	ClearedVolume = 0;
	SettlPriceType = "10000000";
}

SecurityDefinition::~SecurityDefinition() 
{
}

string SecurityDefinition::getString()
{
	std::stringstream stream;

	stream << "35=" << "d" << '\001';
	stream << "49=" << "CME" << '\001';
	stream << "34=" << MsgSeqNum << '\001';
	stream << "52=" << SendingTime << '\001';

	stream << "5799=" << MatchEventIndicator << '\001';

	if( TotNumReports >= 0 )
		stream << "911=" << TotNumReports << '\001';

	if( !SecurityUpdateAction.empty() )
		stream << "980=" << SecurityUpdateAction << '\001';

	if( !LastUpdateTime.empty() )
		stream << "779=" << LastUpdateTime << '\001';

	if( objContract.ApplID > 0 )
		stream << "1180=" << objContract.ApplID << '\001';

	if( MarketSegmentID > 0 )
		stream << "1300=" << MarketSegmentID << '\001';

	if( !objContract.Symbol.empty() )
		stream << "55=" << objContract.Symbol << '\001';

	if( objContract.SecurityID > 0 )
		stream << "48=" << objContract.SecurityID << '\001';

	//if( !objContract.Symbol.empty() )
	//	stream << "107=" << Symbol << '\001';

	if( !SecurityIDSource.empty() )
		stream << "22=" << SecurityIDSource << '\001';

	if( !MaturityMonthYear.empty() )
		stream << "200=" << MaturityMonthYear << '\001';

	if( !objContract.SecurityGroup.empty() )
		stream << "1151=" << objContract.SecurityGroup << '\001';

	if( !objContract.Asset.empty() )
		stream << "6937=" << objContract.Asset << '\001';

	if( !SecurityType.empty() )
		stream << "167=" << SecurityType << '\001';

	if( !objContract.CFICode.empty() )
		stream << "461=" << objContract.CFICode << '\001';

	if( PutOrCall == 0 || PutOrCall == 1)
		stream << "201=" << PutOrCall << '\001';

	if( !SecuritySubType.empty() )
		stream << "762=" << SecuritySubType << '\001';

	if( !UserDefinedInstrument.empty() )
		stream << "9779=" << UserDefinedInstrument << '\001';

	if( UnderlyingProduct > 0 )
		stream << "462=" << UnderlyingProduct << '\001';

	if( !objContract.SecurityExchange.empty() )
		stream << "207=" << objContract.SecurityExchange << '\001';

	if( MDSecurityTradingStatus > 0 )
		stream << "1682=" << MDSecurityTradingStatus << '\001';

	if( StrikePrice > 0 )
		stream << "202=" << StrikePrice << '\001';
	
	if( !StrikeCurrency.empty() )
		stream << "947=" << StrikeCurrency << '\001';
	
	if( !Currency.empty())
		stream << "15=" << Currency << '\001';
	
	if( !SettlCurrency.empty() )
		stream << "120=" << SettlCurrency << '\001';
	
	if( MinCabPrice > 0 )
		stream << "9850=" << MinCabPrice << '\001';
	
	if( PriceRatio > 0 )
		stream << "5770=" << PriceRatio << '\001';
	
	if( !MatchAlgorithm.empty() )
		stream << "1142=" << MatchAlgorithm << '\001';
	
	if( MinTradeVol > 0 )
		stream << "562=" << MinTradeVol << '\001';
	
	if( MaxTradeVol > 0 )
		stream << "1140=" << MaxTradeVol << '\001';
	
	if( MinPriceIncrement > 0 )
		stream << "969=" << MinPriceIncrement << '\001';
	
	if( MinPriceIncrementAmount > 0 )
		stream << "1146=" << MinPriceIncrementAmount << '\001';	
	
	if( DisplayFactor > 0 )
		stream << "9787=" << DisplayFactor << '\001';
	
	if( TickRule > 0 )
		stream << "6350=" << TickRule << '\001';
	
	if( MainFraction > 0 )
		stream << "37702=" << MainFraction<< '\001';
	
	if( SubFraction > 0 )
		stream << "37703=" << SubFraction<< '\001';
	
	if( PriceDisplayFormat > 0 )
		stream << "9800=" << PriceDisplayFormat<< '\001';

	if( vNoUnderlyings.size() )
	{
		stream << "711=" << vNoUnderlyings.size() << '\001';

		for(int iCount(0); iCount < vNoUnderlyings.size(); ++iCount)
		{
			if( !vNoUnderlyings[iCount].UnderlyingSymbol.empty() )
				stream << "311=" << vNoUnderlyings[iCount].UnderlyingSymbol << '\001';
			stream << "309=" << vNoUnderlyings[iCount].UnderlyingSecurityID << '\001';
			stream << "305=" << vNoUnderlyings[iCount].UnderlyingSecurityIDSource << '\001';
		}
	}

	if( vNoLegs.size() )
	{
		stream << "555=" << vNoLegs.size() << '\001';

		for(int iCount(0); iCount < vNoLegs.size(); ++iCount)
		{
			stream << "602=" << vNoLegs[iCount].LegSecurityID << '\001';
			stream << "603=" << vNoLegs[iCount].LegSecurityIDSource << '\001';
			stream << "624=" << vNoLegs[iCount].LegSide << '\001';
			stream << "623=" << vNoLegs[iCount].LegRatioQty << '\001';

			if( vNoLegs[iCount].LegPrice > 0 )
				stream << "566=" << vNoLegs[iCount].LegPrice << '\001';

			if( vNoLegs[iCount].LegOptionDelta > 0 )
				stream << "1017=" << vNoLegs[iCount].LegOptionDelta << '\001';
		}
	}

	if( vNoMdFeedTypes.size() )
	{
		stream << "1141=" << vNoMdFeedTypes.size() << '\001';

		for(int iCount(0); iCount < vNoMdFeedTypes.size(); ++iCount)
		{
			stream << "1022=" << vNoMdFeedTypes[iCount].MDFeedType << '\001';
			stream << "264=" << vNoMdFeedTypes[iCount].MarketDepth << '\001';
		}
	}

	if( vNoEvents.size() )
	{
		stream << "864=" << vNoEvents.size() << '\001';

		for(int iCount(0); iCount < vNoEvents.size(); ++iCount)
		{
			stream << "865=" << vNoEvents[iCount].EventType << '\001';
			stream << "1145=" << vNoEvents[iCount].EventTime << '\001';
		}
	}

	if( vNoInstAttrib.size() )
	{
		stream << "870=" << vNoInstAttrib.size() << '\001';

		for(int iCount(0); iCount < vNoInstAttrib.size(); ++iCount)
		{
			stream << "871=" << vNoInstAttrib[iCount].InstAttribType << '\001';
			stream << "872=" << vNoInstAttrib[iCount].InstAttribValue << '\001';
		}
	}

	if( vNoLotTypeRules.size() )
	{
		stream << "1234=" << vNoLotTypeRules.size() << '\001';

		for(int iCount(0); iCount < vNoLotTypeRules.size(); ++iCount)
		{
			stream << "1093=" << vNoLotTypeRules[iCount].LotType << '\001';
			stream << "1231=" << vNoLotTypeRules[iCount].MinLotSize << '\001';
		}
	}

	if( ContractMultiplierUnit > 0 )
		stream << "1435=" << ContractMultiplierUnit<< '\001';
	
	if( FlowScheduleType >= 0 )
		stream << "1439=" << FlowScheduleType<< '\001';
	
	if( ContractMultiplier > 0 )
		stream << "231=" << ContractMultiplier<< '\001';
	
	if( !UnitOfMeasure.empty() )
		stream << "996=" << UnitOfMeasure<< '\001';
	
	if( UnitOfMeasureQty > 0 )
		stream << "1147=" << UnitOfMeasureQty<< '\001';
	
	if( DecayQty > 0 )
		stream << "5818=" << DecayQty<< '\001';
	
	if( !DecayStartDate.empty() )
		stream << "5819=" << DecayStartDate<< '\001';
	
	if( OriginalContractSize > 0 )
		stream << "5849=" << OriginalContractSize<< '\001';
	
	if( HighLimitPrice > 0 )
		stream << "1149=" << HighLimitPrice<< '\001';
	
	if( LowLimitPrice > 0 )
		stream << "1148=" << LowLimitPrice<< '\001';
	
	if( MaxPriceVariation > 0 )
		stream << "1143=" << MaxPriceVariation<< '\001';
	
	if( TradingReferencePrice > 0 )
		stream << "1150=" << TradingReferencePrice<< '\001';
	
	stream << "731=" << SettlPriceType << '\001';
	
	if( OpenInterestQty > 0 )
		stream << "5792=" << OpenInterestQty<< '\001';
	
	if( ClearedVolume > 0 )
		stream << "5791=" << ClearedVolume<< '\001';

	return calculateTags(stream.str());
}

FileList SecurityDefinition::getFiles()
{
	FileList lstfiles;
	string strfilename = objContract.SecurityExchange + "\\" + objContract.SecurityExchange + "_MD_" + objContract.Asset + objContract.getType();
	lstfiles.push_back(strfilename);
	return lstfiles;
}

OrderIDEntry::OrderIDEntry()
{
	OrderID = 0;
	LastQty = 0;
}

OrderIDEntry::~OrderIDEntry()
{
}

MDEntry::MDEntry()
{
	MDEntryPx = 0.0;
	MDEntrySize = 0;
	SecurityID = 0;
	RptSeq = 0;
	Symbol = "";
	NumberOfOrders = 0;
	MDPriceLevel = 0;
	MDUpdateAction = 0;
	MDEntryType = '0';
	HighLimitPrice = 0.0;
	LowLimitPrice = 0.0;
	MaxPriceVariation = 0.0;
	OpenCloseSettlFlag = 0;
	TradeID = 0;
	AggressorSide = 0;
	SettlPriceType = "10000000";
	TradingReferenceDate = "";
	ApplID = 0;
}

MDEntry::~MDEntry()
{
}

MarketData::MarketData()
{
	stroutputfile = "";
}

MarketData::~MarketData()
{
}

string MarketData::getString()
{
	std::stringstream stream;

	// Header order - 1128, 9, 35, 49, 34, 52, 75, 60, 5799, 10 (Always Last field in message)

	stream << "35=" << MsgTyp << '\001'; 
	stream << "49=" << "CME" << '\001';
	stream << "34=" << MsgSeqNum << '\001';
	stream << "52=" << SendingTime << '\001'; 

	if( !TradeDate.empty() )
		stream << "75=" << TradeDate << '\001';

	stream << "60=" << TransactTime << '\001';
	stream << "5799=" << MatchEventIndicator << '\001';

	stream << "268=" << vMDEntry.size() << '\001';

	for(int iCount(0); iCount < vMDEntry.size(); ++iCount)
	{
		stream << "279=" << vMDEntry[iCount].MDUpdateAction << '\001';
		stream << "269=" << vMDEntry[iCount].MDEntryType << '\001';

		if( vMDEntry[iCount].SecurityID > 0 ) 
			stream << "48=" << vMDEntry[iCount].SecurityID << '\001';

		if( vMDEntry[iCount].RptSeq > 0 )
			stream << "83=" << vMDEntry[iCount].RptSeq << '\001';

		if( !vMDEntry[iCount].Symbol.empty() )
			stream << "107=" << vMDEntry[iCount].Symbol << '\001';

		if( vMDEntry[iCount].MDEntryPx > 0 )
			stream << "270=" << vMDEntry[iCount].MDEntryPx << '\001';

		if( vMDEntry[iCount].MDEntrySize > 0 )
			stream << "271=" << vMDEntry[iCount].MDEntrySize << '\001';

		if( vMDEntry[iCount].NumberOfOrders > 0 )
			stream << "346=" << vMDEntry[iCount].NumberOfOrders << '\001';

		if( vMDEntry[iCount].MDPriceLevel > 0 )
			stream << "1023=" << vMDEntry[iCount].MDPriceLevel << '\001';

		if( vMDEntry[iCount].OpenCloseSettlFlag == 0 || vMDEntry[iCount].OpenCloseSettlFlag == 5 )
			stream << "286=" << vMDEntry[iCount].OpenCloseSettlFlag << '\001';

		stream << "731=" << vMDEntry[iCount].SettlPriceType << '\001';

		if( vMDEntry[iCount].AggressorSide >= 0 )
			stream << "5797=" << vMDEntry[iCount].AggressorSide << '\001';

		if( !vMDEntry[iCount].TradingReferenceDate.empty() )
			stream << "5796=" << vMDEntry[iCount].TradingReferenceDate << '\001';

		if( vMDEntry[iCount].HighLimitPrice > 0 )
			stream << "1149=" << vMDEntry[iCount].HighLimitPrice << '\001';

		if( vMDEntry[iCount].LowLimitPrice > 0 )
			stream << "1148=" << vMDEntry[iCount].LowLimitPrice << '\001';

		if( vMDEntry[iCount].MaxPriceVariation > 0 )
			stream << "1143=" << vMDEntry[iCount].MaxPriceVariation << '\001';

		if( vMDEntry[iCount].ApplID > 0 )
			stream << "1180=" << vMDEntry[iCount].ApplID << '\001';
	}

	if( vOrderIDEntry.size() )
	{
		stream << "37705=" << vOrderIDEntry.size() << '\001';

		for(int jCount(0); jCount < vOrderIDEntry.size(); ++jCount)
			stream << "37=" << vOrderIDEntry[jCount].OrderID << '\001' << "32=" << vOrderIDEntry[jCount].LastQty << '\001';
	}

	return calculateTags(stream.str());
}

void MarketData::buildoutputfile()
{
	stroutputfile = objContract.SecurityExchange + "\\" + objContract.SecurityExchange + "_MD_" + objContract.Asset + objContract.getType();
}

ChannelReset::ChannelReset()
{
}

ChannelReset::~ChannelReset()
{
}

string ChannelReset::getString()
{
	std::stringstream stream;

	stream << "35=" << MsgTyp << '\001'; 
	stream << "49=" << "CME" << '\001';
	stream << "34=" << MsgSeqNum << '\001';
	stream << "52=" << SendingTime << '\001'; 

	if( !TradeDate.empty() )
		stream << "75=" << TradeDate << '\001';

	stream << "60=" << TransactTime << '\001';
	stream << "5799=" << MatchEventIndicator << '\001';

	stream << "268=" << vMDEntry.size() << '\001';

	for(int iCount(0); iCount < vMDEntry.size(); ++iCount)
	{
		stream << "279=" << vMDEntry[iCount].MDUpdateAction << '\001';
		stream << "269=" << vMDEntry[iCount].MDEntryType << '\001';

		if( vMDEntry[iCount].ApplID > 0 )
			stream << "1180=" << vMDEntry[iCount].ApplID << '\001';
	}

	return calculateTags(stream.str());
}

FileList ChannelReset::getFiles()
{
	FileList lstfiles;
	string strfilename("");

	for(int iCount(0); iCount < vMDEntry.size(); ++iCount)
	{
		pair<ChannelToGroupItr, ChannelToGroupItr> prGroups(MessageUtil::getInstance()->gChannelToGroupMap.equal_range(vMDEntry[iCount].ApplID));

		for (ChannelToGroupItr itGroup = prGroups.first; itGroup != prGroups.second; ++itGroup)
		{
			pair<GroupProductItr, GroupProductItr> prProducts(MessageUtil::getInstance()->gGroupToProductMap.equal_range(itGroup->second));

			for (GroupProductItr itProduct = prProducts.first; itProduct != prProducts.second; ++itProduct)
			{
				string sKey = itGroup->second + "_" + itProduct->second;
				pair<ProductCFICodeItr, ProductCFICodeItr> prCFICodes(MessageUtil::getInstance()->gProductCFICodeMap.equal_range(sKey));

				for (ProductCFICodeItr itCFICode = prCFICodes.first; itCFICode != prCFICodes.second; ++itCFICode)
				{
					objContract.CFICode = itCFICode->second;
					strfilename = objContract.SecurityExchange + "\\" + objContract.SecurityExchange + "_MD_" + itProduct->second + objContract.getType();
					lstfiles.push_back(strfilename);
				}
			}
		}
	}

	if(lstfiles.empty())
	{
		strfilename = objContract.SecurityExchange + "\\" + objContract.SecurityExchange + "_MD_" + "NULL";
		lstfiles.push_back(strfilename);
	}
	
	return lstfiles;
}

SecurityStatus::SecurityStatus()
{
	SecurityID = 0;
    SecurityTradingStatus = 0;
    HaltReason = 0;
    SecurityTradingEvent = 0;
}

SecurityStatus::~SecurityStatus()
{
}

string SecurityStatus::getString()
{
	std::stringstream stream;

	stream << "35=" << MsgTyp << '\001';
	stream << "49=" << "CME" << '\001';
	stream << "34=" << MsgSeqNum << '\001';
	stream << "52=" << SendingTime << '\001';

	if( !TradeDate.empty() )
		stream << "75=" << TradeDate << '\001';

	stream << "60=" << TransactTime << '\001';
	stream << "5799=" << MatchEventIndicator << '\001';
		
	if( !objContract.SecurityGroup.empty() )
		stream << "1151=" << objContract.SecurityGroup << '\001';
		
	if( !objContract.Asset.empty() )
		stream << "6937=" << objContract.Asset << '\001';
		
	if( SecurityID > 0 )
		stream << "48=" << SecurityID << '\001';

	if( !objContract.Symbol.empty() )
		stream << "107=" << objContract.Symbol << '\001';
		
	if( SecurityTradingStatus > 0 )
		stream << "326=" << SecurityTradingStatus << '\001';
		
	if( HaltReason >= 0 && HaltReason <= 6)
		stream << "327=" << HaltReason << '\001';
		
	if( SecurityTradingEvent >= 0 && SecurityTradingEvent <= 6)
		stream << "1174=" << SecurityTradingEvent << '\001';
		
	return calculateTags(stream.str());
}

FileList SecurityStatus::getFiles(int applID)
{
	FileList lstfiles;
	string strfilename("");
	
	if( SecurityID > 0 )
	{
		strfilename = objContract.SecurityExchange + "\\" + objContract.SecurityExchange + "_MD_" + objContract.Asset + objContract.getType();
		lstfiles.push_back(strfilename);
	}
	else if( !objContract.Asset.empty() )
	{
		string sKey = objContract.SecurityGroup + "_" + objContract.Asset;

		ProductExchangeMap::iterator itrExchange = MessageUtil::getInstance()->productExchangeMap.find(sKey);
		string strExchange("");

		if(itrExchange != MessageUtil::getInstance()->productExchangeMap.end())
			strExchange = itrExchange->second.SecurityExchange;

		if(!strExchange.empty())
		{
			pair<ProductCFICodeItr, ProductCFICodeItr> prCFICodes(MessageUtil::getInstance()->gProductCFICodeMap.equal_range(sKey));

			for (ProductCFICodeItr itCFICode = prCFICodes.first; itCFICode != prCFICodes.second; ++itCFICode)
			{
				objContract.CFICode = itCFICode->second;
				strfilename = strExchange + "\\" + strExchange + "_MD_" + objContract.Asset + objContract.getType();
				lstfiles.push_back(strfilename);
			}
		}
	}
	else
	{
		pair<GroupProductItr, GroupProductItr> prProducts(MessageUtil::getInstance()->gGroupToProductMap.equal_range(objContract.SecurityGroup));

		for (GroupProductItr itProduct = prProducts.first; itProduct != prProducts.second; ++itProduct)
		{
			string sKey = objContract.SecurityGroup + "_" + itProduct->second;

			ProductExchangeMap::iterator itrExchange = MessageUtil::getInstance()->productExchangeMap.find(sKey);
			string strExchange("");

			if(itrExchange != MessageUtil::getInstance()->productExchangeMap.end())
			{
				if( applID == itrExchange->second.ApplID )
					strExchange = itrExchange->second.SecurityExchange;
			}

			if(strExchange.empty())
				continue;
			
			pair<ProductCFICodeItr, ProductCFICodeItr> prCFICodes(MessageUtil::getInstance()->gProductCFICodeMap.equal_range(sKey));

			for (ProductCFICodeItr itCFICode = prCFICodes.first; itCFICode != prCFICodes.second; ++itCFICode)
			{
				objContract.CFICode = itCFICode->second;
				strfilename = strExchange + "\\" + strExchange + "_MD_" + itProduct->second + objContract.getType();
				lstfiles.push_back(strfilename);
			}
		}

		if(lstfiles.empty())
		{
			strfilename = "NULL\\NULL_MD_NULL";
			lstfiles.push_back(strfilename);
		}
	}
	
	return lstfiles;
}

NoRelatedSym::NoRelatedSym()
{
	Symbol = "";
	SecurityID = 0;
    OrderQty = 0.0;
    QuoteType = 0;
    Side = 0;
}

NoRelatedSym::~NoRelatedSym()
{
}

QuoteRequest::QuoteRequest()
{
	QuoteReqID = "";
}

QuoteRequest::~QuoteRequest()
{
}

string QuoteRequest::getString()
{
	std::stringstream stream;

	stream << "35=" << MsgTyp << '\001';
	stream << "49=" << "CME" << '\001';
	stream << "34=" << MsgSeqNum << '\001';
	stream << "52=" << SendingTime << '\001';

	if( !TradeDate.empty() )
		stream << "75=" << TradeDate << '\001';

	stream << "60=" << TransactTime << '\001';
	stream << "5799=" << MatchEventIndicator << '\001';
		
	if( !QuoteReqID.empty() )
		stream << "131=" << QuoteReqID << '\001';

	stream << "146=" << vNoRelatedSym.size() << '\001';

	for(int iCount(0); iCount < vNoRelatedSym.size(); ++iCount)
	{
		if( !vNoRelatedSym[iCount].Symbol.empty() )
			stream << "55=" << vNoRelatedSym[iCount].Symbol << '\001';

		if( vNoRelatedSym[iCount].SecurityID > 0 )
			stream << "48=" << vNoRelatedSym[iCount].SecurityID << '\001';

		if( !vNoRelatedSym[iCount].Symbol.empty() )
			stream << "107=" << vNoRelatedSym[iCount].Symbol << '\001';

		if( vNoRelatedSym[iCount].QuoteType > 0 )
			stream << "537=" << vNoRelatedSym[iCount].QuoteType << '\001';

		if( vNoRelatedSym[iCount].OrderQty > 0 )
			stream << "38=" << vNoRelatedSym[iCount].OrderQty << '\001';

		if( vNoRelatedSym[iCount].Side > 0 )
			stream << "54=" << vNoRelatedSym[iCount].Side << '\001';
	}

	return calculateTags(stream.str());
}

void QuoteRequest::buildoutputfile()
{
	stroutputfile = objContract.SecurityExchange + "\\" + objContract.SecurityExchange + "_MD_" + objContract.Asset + objContract.getType();
}
