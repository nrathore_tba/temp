
#include "stdafx.h"
#include <fields/FileField.hpp>
#include "CPDF_Tamplate_Generator.h"
#include "CMEInvoiceMasterFinapp.hpp"

BF_NAMESPACE_USE

// This macro will implement a main() function for your application.
// The main function will create an instance of your application
// class, initialize it and run.
IMPLEMENT_IMAGE(CMEInvoiceMasterFinapp)

//Constructor ~ Destructor
CMEInvoiceMasterFinapp::CMEInvoiceMasterFinapp(){		
}

CMEInvoiceMasterFinapp::~CMEInvoiceMasterFinapp(){
}

bool CMEInvoiceMasterFinapp::InitData()
{	
	GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));

	GetField(e_InvoiceFileField)->SetEnabled(false);
	return true;
}

void CMEInvoiceMasterFinapp::Recalculate()
{	
	if( IsFieldNew(e_GenerateInvoice) )
	{		
		GetField(e_Status)->SetValueString("Generating Pdf....");
		GetField(e_Status)->SetBlinkInterval(-1);		
		GetData();
		GenerateInvoice();
		GetField(e_Status)->SetValueString("Pdf Generated Successfully.");
		GetField(e_Status)->SetBlinkInterval(0);				
	}	

}

void CMEInvoiceMasterFinapp::GetData()
{
	m_FileData.clear(); 
	stFileData objFileData;
	for(int iI = e_DisplayDesc,iJ = e_DisplayDetail ;iI<= (e_DisplayDesc + 20); iI++,iJ++)
	{
		objFileData.strDesc  = GetField(iI)->GetValueString(); 
		objFileData.strDetail = GetField(iJ)->GetValueString();
		m_FileData.push_back(objFileData); 
	}
}

void CMEInvoiceMasterFinapp::GenerateInvoice()
{
	_bstr_t strVal;
	_bstr_t strFileName;	
	strFileName = "C:\\Invoice.pdf"; //file Name with path

	CPDF_Template_Generator *CpdfWriter = new CPDF_Template_Generator() ;		
	//CpdfWriter->PDF_PutPngImage("\\\\powervault-1\\Middle-Office\\REALQA\\Templates\\Email\\MOSec.png");  //Logo at Top
	 
	CpdfWriter->OpenColumnWriting();  
	CpdfWriter->FontSize = 6;		
		
	int nDataCount = m_FileData.size();
	int nRow = 0;
	for( ; nRow < nDataCount; nRow++ )
	{				
		strVal = m_FileData.at(nRow).strDesc.Trim();   
		CpdfWriter->PDF_WriteLine(strVal,0);		

		strVal = m_FileData.at(nRow).strDetail.Trim();
		CpdfWriter->PDF_Write(strVal,1);
	}	
		//CpdfWriter->OpenCommonHeader(30,10);
	   //CpdfWriter->CloseCommonHeader();	
			
		CpdfWriter->SaveToFile(strFileName);						

		GetField(e_InvoiceFileField)->SetEnabled(true);
		FileField * fld = (FileField *)GetField(e_InvoiceFileField);		
		fld->SetFileBinary(strFileName);

		delete CpdfWriter;	
		remove(strFileName);	
}

