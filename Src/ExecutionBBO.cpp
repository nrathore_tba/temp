#include <fstream>
#include <sstream>
#include <stdafx.h> 
#include <shldisp.h>
#include <tlhelp32.h>
#include "zip.h"
#include <MailAutomation/MailAutomation.h>
#include "ExecutionBBO.hpp"

#define SPLITLINE 20000

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(ExecutionBBO)

ExecutionBBO::ExecutionBBO()
{	
}

ExecutionBBO::~ExecutionBBO()
{
}

bool ExecutionBBO::InitData()
{
	GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));

	m_curTopRow= 0;
	for(m_numofRows = 0; GetField(e_LogMsg + m_numofRows); m_numofRows++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	//---------------------------------------------
	if( !IsRestoreUpdate( ) )
	{
		BF_Reg_Key_Ptr pKey1(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\CME"));

		int iFTPLocation;
		RegDWORD strServerName(pKey1, "S3FTPLocation",0);
		iFTPLocation = strServerName.get_value();
		GetField(e_S3FTPBucket)->SetValueInt( iFTPLocation );

		int iInputLocation;
		RegDWORD strS3InPutLocation(pKey1, "S3InputLocation",2);
		iInputLocation = strS3InPutLocation.get_value();
		GetField(e_S3CMEBucket)->SetValueInt( iInputLocation );

		COleDateTime dtToday = COleDateTime::GetCurrentTime();
		BFDate bfToday;
		bfToday.SetYearMonthDate(dtToday.GetYear(),dtToday.GetMonth() - 1, dtToday.GetDay() );
		GetField(e_DateFilter)->SetValueDate(bfToday); 
	}

	fld_OutputPath = GetField(e_OutputPath);

	m_clScheduleInfo.bValid = false;
	m_clScheduleInfo.iRetInterval = GetField(e_RtryInterval)->GetValueInt() > 0 ? GetField(e_RtryInterval)->GetValueInt(): 10;
	
	m_bIsThreadRunning = false;
	m_bIsTrheadCompleted = true;
	m_iTotalCount = 0;
	m_iOpenFileCount = 0;

	m_enmOperation = OP_None;

	GetField( e_Process )->SetVisible(false);

	return true;
}

const int		__niMsgCouunt = 200;
CString __csMsgArry[__niMsgCouunt] = { "" };

void ExecutionBBO::fnAddMessage( CString csaNewMsg )
{	
	for( int iI = __niMsgCouunt-1; iI > 0; iI-- )
		__csMsgArry[iI] = __csMsgArry[iI -1];
	
	__csMsgArry[0] = csaNewMsg;	
}

void ExecutionBBO::fnDisplayMsg()
{
	int iRow = 0;
	for( int iIndex = m_curTopRow; iIndex < __niMsgCouunt; iIndex++, iRow++ )
	{
		if( iRow >= m_numofRows )
			break;
		
		BaseField * fldComment		= GetField(e_LogMsg + iRow);
		
		fldComment->SetValueString( __csMsgArry[iIndex] );
		fldComment->SetNotManual();
	}
	
	while( iRow < m_numofRows )
	{
		BaseField * fldComment		= GetField(e_LogMsg + iRow);
		fldComment->SetBlankState();
		iRow++;
	}
}

UINT BBOProcessor(void *pVoid)
{
	ExecutionBBO *pParser = (ExecutionBBO*)pVoid;
	
	pParser->m_iTotalCount = 0;
	pParser->m_bIsThreadRunning = true;
	pParser->m_bIsTrheadCompleted = false;
	
	pParser->fnUpdateScheduleStatus( 1 );
	
	pParser->ProcessBBOFiles();

	pParser->fnUpdateScheduleStatus( 2 );
	pParser->fnSendEmail(2, 0);

	pParser->m_enmOperation = OP_ProcessCompleted;
	pParser->m_clScheduleInfo.bValid  = false;	

	pParser->m_bIsThreadRunning = false;
	pParser->RequestExternalUpdate();

	return 0;
}

void ExecutionBBO::Recalculate()
{
	if( IsFieldNew(e_RtryInterval) )
		m_clScheduleInfo.iRetInterval = GetField(e_RtryInterval)->GetValueInt() > 0 ? GetField(e_RtryInterval)->GetValueInt(): 10;

	if( m_bIsThreadRunning )
	{
		GetField(e_MsgTitle)->SetTitle(m_csMsg);		
	}
	else
	{
		if( !m_bIsThreadRunning && !m_bIsTrheadCompleted )
		{
			// After completion of thread
			GetField(e_MsgTitle)->SetTitle("Message");
			m_bIsTrheadCompleted = true;
		}

		if( !m_clScheduleInfo.bValid || m_enmOperation == OP_ProcessCompleted || IsFieldNew(e_ReloadSchedule) )
		{		
			fnGetNextSchedule( );
		}

		if( !m_clScheduleInfo.bValid )	
		{
			StartRecalcTimer(60, false);
			return;
		}

		if( m_enmOperation == OP_Process || IsFieldNew(e_Process) )
		{			
			AfxBeginThread(BBOProcessor, this);
		}

		if( m_enmOperation == OP_ListFiles )
		{			
			m_clProdFileDtlMap.clear();
			Recurse( m_clScheduleInfo.csGlobextFolder );

			if( m_clProdFileDtlMap.size() == 0 )
			{
				m_clScheduleInfo.clScheduleDtTime = COleDateTime::GetCurrentTime();;
				m_clScheduleInfo.clScheduleDtTime += COleDateTimeSpan(0,0, m_clScheduleInfo.iRetInterval,0);
				m_clScheduleInfo.iRetryCount++;

				CString csTemp;
				csTemp.Format( "No Product File Downloaded, scheduled retry after %d minutes", m_clScheduleInfo.iRetInterval );
				fnAddMessage(csTemp);

				m_enmOperation = OP_None;
			}
			else
			{
				CString csTemp;
				csTemp.Format("%d Product files found, start processing", m_clProdFileDtlMap.size() );
				fnAddMessage( csTemp );
				m_enmOperation = OP_Process;
			}
		}

		if( m_enmOperation == OP_Download )
		{
			fnCopyS3FiletoLocal( );
		}

		if( m_enmOperation == OP_None )
			fnRecalc1( );

		if( m_enmOperation != OP_None )
			StartRecalcTimer(0, false);
	}

	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_pgUP,e_pgDN,200,m_numofRows,m_curTopRow);		
	fnDisplayMsg();	
}

void ExecutionBBO::fnRecalc1( )
{
	COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
	long lRecalcTime = 0;

	if( GetField( e_ManualRun )->GetValueInt() == 1 )
	{
		GetField(e_NextDownloadMsg)->SetTitle("Processing Set To Manaul");
	}
	else if( dtCurrentTime >= m_clScheduleInfo.clScheduleDtTime )
	{
		//Need to Process subscription...
		GetField(e_NextDownloadMsg)->SetTitle("Processing File");
		m_enmOperation = OP_Download;		
		fnAddMessage("Downloading File");
	}
	else if( dtCurrentTime < m_clScheduleInfo.clScheduleDtTime )
	{
		COleDateTimeSpan dtSpan = m_clScheduleInfo.clScheduleDtTime - dtCurrentTime;
		lRecalcTime = dtSpan.GetTotalSeconds();

		CString csTimer,csTmp;

		int iDays =	dtSpan.GetDays();
		int iLeftTotalHr = dtSpan.GetHours();
		int iLeftTotalMinute = dtSpan.GetMinutes();
		int iLeftTotalSecond = dtSpan.GetSeconds();

		if( iDays > 0 )
		{
			csTmp.Format("%d Day and ",iDays);					
			csTimer += csTmp;

			csTmp.Format("%02d:",iLeftTotalHr);					
			csTimer += csTmp;
		}
		else
		{
			if( iLeftTotalHr > 0 )
			{					
				csTmp.Format("%02d:",iLeftTotalHr);					
				csTimer += csTmp;
			}
		}

		csTmp.Format("%02d:",iLeftTotalMinute);					
		csTimer += csTmp;

		csTmp.Format("%02d",iLeftTotalSecond);
		csTimer += csTmp;				

		int iDate	= m_clScheduleInfo.clScheduleDtTime.GetDay();
		int iMonth	= m_clScheduleInfo.clScheduleDtTime.GetMonth();
		int iYear	= m_clScheduleInfo.clScheduleDtTime.GetYear();
		int iHour	= m_clScheduleInfo.clScheduleDtTime.GetHour();
		int iMinute = m_clScheduleInfo.clScheduleDtTime.GetMinute();
		int iSecond = m_clScheduleInfo.clScheduleDtTime.GetSecond();

		CString csStr;
		csStr.Format("Next Download: %d-%d-%d %2d:%2d:%2d [Timer: %s]",iDate,iMonth,iYear,iHour,iMinute,iSecond, csTimer);

		GetField(e_NextDownloadMsg)->SetTitle(csStr);
		GetField(e_NextDownloadMsg)->SetTitleForeColor(ColorManager::eSignalPositiveLight);

		if( lRecalcTime > 4 || lRecalcTime < 0 )
			lRecalcTime = 4;
	}

	if( lRecalcTime )
		StartRecalcTimer(lRecalcTime, false);
}

void ExecutionBBO::fnGetNextSchedule( )
{
	m_clScheduleInfo.bValid = false;
	m_enmOperation = OP_None;

	BFDate bfdt = GetField(e_DateFilter)->GetValueDate();

#ifdef _DEBUG 
	m_clScheduleInfo.clScheduleDtTime = COleDateTime(2015,8,28,0,0,0);	
	m_clScheduleInfo.iStatus = 0;
	m_clScheduleInfo.bValid = true;

	BFDate bfDate = m_clScheduleInfo.clScheduleDtTime.m_dt;
	m_clScheduleInfo.csDate.Format("%d%02d%02d", bfDate.GetYear(), bfDate.GetMonth()+1, bfDate.GetDate() );

#else

	CString csStrDtFilter;
	csStrDtFilter.Format("%d-%02d-%02d 00:00:00",bfdt.GetYear(), bfdt.GetMonth()+1, bfdt.GetDate());

	CString sql;
	sql.Format("SELECT top 1* from dbo.CME_Subscription_Schedule WHERE FileType ='BBO' and IsNull(Status,0) In(0,1) and ScheduleDateTime > '%s' ORDER BY ScheduleDateTime", csStrDtFilter);

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		GetField(e_NextDownloadMsg)->SetTitle("Error while Loading Schedule");
		return;
	}
		
	if( VARIANT_FALSE == set->adoEOF )
	{
		SETLONG( m_clScheduleInfo.iStatus,		set->Fields->Item[_variant_t("Status")]->Value);	
		SETOLEDATE(	m_clScheduleInfo.clScheduleDtTime,	set->Fields->Item[_variant_t("ScheduleDateTime")]->Value);	
		m_clScheduleInfo.clMainScheduleDtTime = m_clScheduleInfo.clScheduleDtTime;

		m_clScheduleInfo.iRetryCount = 0;
		m_clScheduleInfo.csDate.Format("%d%02d%02d", m_clScheduleInfo.clScheduleDtTime.GetYear(), m_clScheduleInfo.clScheduleDtTime.GetMonth(), m_clScheduleInfo.clScheduleDtTime.GetDay() );

		m_clScheduleInfo.bValid = true;
	}

	set->Close();
#endif

	if( m_clScheduleInfo.bValid  )
	{
		GetField( e_Year )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetYear() );
		GetField( e_Month )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetMonth() );
		GetField( e_Day )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetDay() );
		GetField( e_Hour )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetHour() );
		GetField( e_Minute )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetMinute() );
	}
	else
	{
		GetField(e_NextDownloadMsg)->SetTitle("No Schedule Loaded");
	}
}

void ExecutionBBO::fnUpdateScheduleStatus( int iStatus )
{
	CString csdttime;
	csdttime.Format("%d-%02d-%02d %02d:%02d:00",m_clScheduleInfo.clMainScheduleDtTime.GetYear(), m_clScheduleInfo.clMainScheduleDtTime.GetMonth(), m_clScheduleInfo.clMainScheduleDtTime.GetDay(), m_clScheduleInfo.clMainScheduleDtTime.GetHour(), m_clScheduleInfo.clMainScheduleDtTime.GetMinute());
	
	COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
	CString csTimeStatus;
	csTimeStatus.Format("%d-%02d-%02d %02d:%02d:00",dtCurrentTime.GetYear(), dtCurrentTime.GetMonth(), dtCurrentTime.GetDay(), dtCurrentTime.GetHour(), dtCurrentTime.GetMinute());

	CString sql;
	if( iStatus == 1 )
		sql.Format("UPDATE dbo.CME_Subscription_Schedule SET Status = %d, ProcessStart = '%s' WHERE FileType ='BBO' and ScheduleDateTime = '%s'", iStatus, csTimeStatus, csdttime);
	else
		sql.Format("UPDATE dbo.CME_Subscription_Schedule SET Status = %d,   ProcessEnd = '%s' WHERE FileType ='BBO' and ScheduleDateTime = '%s'", iStatus, csTimeStatus, csdttime);

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		GetField(e_NextDownloadMsg)->SetTitle("Error while Loading Schedule");
		return;
	}
}

void ExecutionBBO::fnCopyS3FiletoLocal( )
{
	CString strBasePath = fld_OutputPath->GetValueString();

	CString csInputBucket = ((ListField*)GetField(e_S3CMEBucket))->GetShortString();
	CString csProfile = ((ListField*)GetField(e_S3CMEBucket))->GetLongString();

	CString strFullInputPath;
	strFullInputPath.Format("%s/daily/bbo/globex/%s/", csInputBucket, m_clScheduleInfo.csDate);

	CString csDestinationFolder;
	csDestinationFolder.Format("%s\\globex\\%s\\",strBasePath, m_clScheduleInfo.csDate);
	m_clScheduleInfo.csGlobextFolder = csDestinationFolder;

	//"s3 cp s3://cmegroup-main-datamine-qa-staging/daily/endofday/20150805/EOD_20150805_E.gz  d:\daily\endofday\20150805\";
	CString csAWSCommand;
	csAWSCommand.Format(_T("aws s3 sync --profile %s %s %s"), csProfile, strFullInputPath, csDestinationFolder);					
	int iValue = system(csAWSCommand);	

	if( iValue == 0 )
	{
		fnAddMessage(csAWSCommand + " File Downloaded successfully");
		m_enmOperation = OP_ListFiles;
	}
	else
	{
		m_clScheduleInfo.clScheduleDtTime = COleDateTime::GetCurrentTime();;
		m_clScheduleInfo.clScheduleDtTime += COleDateTimeSpan(0,0,m_clScheduleInfo.iRetInterval,0);
		m_clScheduleInfo.iRetryCount++;

		CString csTemp;
		csTemp.Format("Error cmd %s, Retry download after %d mintures", csAWSCommand, m_clScheduleInfo.iRetInterval );
		fnAddMessage( csTemp );

		m_enmOperation = OP_None;
	}
}

void ExecutionBBO::Recurse(LPCTSTR pstr)
{	
	FileDtl stFileDtl;

	CFileFind finder;

	// build a string with wildcards
	CString strWildcard(pstr);
	strWildcard += _T("*.*");

	// start working for files
	BOOL bWorking = finder.FindFile(strWildcard);

	vector<string> clTStringV;

	while( bWorking )
	{
		bWorking = finder.FindNextFile();

		// skip . and .. files; otherwise, we'd
		// recur infinitely!

		if( finder.IsDots() )
			continue;

		// if it's a directory, recursively search it
		if( finder.IsDirectory() )
		{			
			CString str = finder.GetFilePath();
			//cout << (LPCTSTR) str << endl;
			Recurse(str);
		}
		else
		{
			stFileDtl.strFilePath	= finder.GetFilePath();
			stFileDtl.strFileName	= finder.GetFileName();	
			stFileDtl.strFolderPath = stFileDtl.strFilePath;

			CString csFilePath = stFileDtl.strFilePath.c_str();
			csFilePath.Replace(stFileDtl.strFileName.c_str(), "");

			stFileDtl.strFolderPath = csFilePath;
			stFileDtl.lFileSize		= finder.GetLength();
			//finder.GetD

			CString	strFileName		=  stFileDtl.strFileName.c_str();
			SplitFileNameString(strFileName	, "_", clTStringV );

			if( clTStringV.size() == 4 )
			{
				stFileDtl.strExch_code = clTStringV[0].c_str();
				stFileDtl.strProd_code = clTStringV[1].c_str();
				stFileDtl.strFutOpt = clTStringV[2].c_str();
				stFileDtl.strSpread = "";		

				m_clProdFileDtlMap.insert(std::pair<std::string, FileDtl>(strFileName,stFileDtl));
			}
			else
			{
				stFileDtl.strExch_code = "";
				stFileDtl.strProd_code = "";
				stFileDtl.strFutOpt = "";
				stFileDtl.strSpread = "";	
			}			
		}
	}

	finder.Close();
}


void ExecutionBBO::SplitFileNameString(CString cStrFileName, string delimiter, vector<string> &result)
{
	result.clear();

	size_t pos = 0;
	string fieldValue, str;

	str = cStrFileName;
	while( (pos = str.find(delimiter)) != string::npos )
	{
		fieldValue = str.substr(0, pos);
		if( fieldValue == "null" )
			fieldValue = "";

		result.push_back(fieldValue);
		str.erase(0, pos + delimiter.length());
	}
	result.push_back(str);	
}

void ExecutionBBO::ProcessBBOFiles( )
{	
	CString csTemp = "Loading subscriptions..";
	fnAddMessage( csTemp );
	this->RequestExternalUpdate();

	LoadSubscription();

	//---------------------
	csTemp = "Uploading to Dest account";	
	fnAddMessage( csTemp );
	this->RequestExternalUpdate();

	m_clReportTotalFileV.clear();
	m_clReportExchangeTotalFileV.clear();
	m_clReporFileMapV.clear();

	UploadSubscription();

	//--------------------
	csTemp = "Generating reports.";	
	fnAddMessage( csTemp );
	this->RequestExternalUpdate();

	fnGenerateReports( );

	csTemp = "Process Completed..";
	fnAddMessage( csTemp );
	this->RequestExternalUpdate();	
}

void ExecutionBBO::LoadSubscription( )
{	
	m_clBBOProdSubDtlMap.clear(); 
	m_clBBOExchSubDtlMap.clear(); 
	m_clDistDestAccount.clear();
		
	CString csSingleAccnt = GetField(e_SingleAccount)->GetValueString();

	{
	CString sql;	
#ifdef _DEBUG
	sql.Format("SELECT CUST_NAME, CONTACT_EMAIL, DEST_ACCOUNT, FTP_PASSWORD, VENUE_CODE, ALL_PRODUCTS, PROD_CODE, BBO_CODE, EXCH_CODE FROM dbo.subscription_2015 where FILE_TYPE = 'BBO' and Active = 1 and IsNull(ALL_PRODUCTS,0) = 0 ");
#else
	sql.Format("SELECT CUST_NAME, CONTACT_EMAIL, DEST_ACCOUNT, FTP_PASSWORD, VENUE_CODE, ALL_PRODUCTS, PROD_CODE, BBO_CODE, EXCH_CODE FROM dbo.subscription_2015 where FILE_TYPE = 'BBO' and Active = 1 and IsNull(ALL_PRODUCTS,0) = 0 ");
#endif
	
	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		return;
	}

	while( VARIANT_FALSE == set->adoEOF )
	{
		BBOSubscriptionDtl clBBOSub;
				
		SETSTR(	clBBOSub.stCUST_NAME,		set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
		SETSTR(	clBBOSub.stCONTACT_EMAIL,	set->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);	
		SETSTR(	clBBOSub.stDEST_ACCOUNT,	set->Fields->Item[_variant_t("DEST_ACCOUNT")]->Value);	
		SETSTR(	clBBOSub.stFTP_PASSWORD,	set->Fields->Item[_variant_t("FTP_PASSWORD")]->Value);	
		SETSTR(	clBBOSub.stVENUE_CODE,		set->Fields->Item[_variant_t("VENUE_CODE")]->Value);	
		SETSTR(	clBBOSub.stALL_PRODUCTS,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);	

		SETSTR(	clBBOSub.stPROD_CODE,	set->Fields->Item[_variant_t("PROD_CODE")]->Value);	
		SETSTR(	clBBOSub.stBBO_CODE,	set->Fields->Item[_variant_t("BBO_CODE")]->Value);	
		SETSTR(	clBBOSub.stEXCH_CODE,	set->Fields->Item[_variant_t("EXCH_CODE")]->Value);

		std::pair <BBOSubDtlMap::iterator, BBOSubDtlMap::iterator> ret;
		ret = m_clBBOProdSubDtlMap.equal_range(clBBOSub.stDEST_ACCOUNT.GetString());

		bool bAlreadyExist = false;
		if( GetField(e_DestAccount)->GetValueInt() == 0 && clBBOSub.stDEST_ACCOUNT != csSingleAccnt )
			bAlreadyExist = true;

		for (BBOSubDtlMap::iterator it = ret.first; it != ret.second; ++it)
		{
			if( it->second.stPROD_CODE == clBBOSub.stPROD_CODE )
			{
				bAlreadyExist = true;
				break;
			}
		}

		if( !bAlreadyExist )
		{
			m_clBBOProdSubDtlMap.insert( std::pair<string, BBOSubscriptionDtl>(clBBOSub.stDEST_ACCOUNT, clBBOSub) );
			m_clDistDestAccount[clBBOSub.stDEST_ACCOUNT]= true;
		}

		set->MoveNext();
	}

	set->Close();	
	}

	{
	CString sql;	
	sql.Format("SELECT CUST_NAME, CONTACT_EMAIL, DEST_ACCOUNT, FTP_PASSWORD, VENUE_CODE, ALL_PRODUCTS, PROD_CODE, BBO_CODE, EXCH_CODE FROM dbo.subscription_2015 where FILE_TYPE = 'BBO' and Active = 1 and IsNull(ALL_PRODUCTS,0) = 1");
	
	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		return;
	}

	while( VARIANT_FALSE == set->adoEOF )
	{
		BBOSubscriptionDtl clBBOSub;
				
		SETSTR(	clBBOSub.stCUST_NAME,		set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
		SETSTR(	clBBOSub.stCONTACT_EMAIL,	set->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);	
		SETSTR(	clBBOSub.stDEST_ACCOUNT,	set->Fields->Item[_variant_t("DEST_ACCOUNT")]->Value);	
		SETSTR(	clBBOSub.stFTP_PASSWORD,	set->Fields->Item[_variant_t("FTP_PASSWORD")]->Value);	
		SETSTR(	clBBOSub.stVENUE_CODE,		set->Fields->Item[_variant_t("VENUE_CODE")]->Value);	
		SETSTR(	clBBOSub.stALL_PRODUCTS,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);	

		SETSTR(	clBBOSub.stPROD_CODE,	set->Fields->Item[_variant_t("PROD_CODE")]->Value);	
		SETSTR(	clBBOSub.stBBO_CODE,	set->Fields->Item[_variant_t("BBO_CODE")]->Value);	
		SETSTR(	clBBOSub.stEXCH_CODE,	set->Fields->Item[_variant_t("EXCH_CODE")]->Value);
		
		std::pair <BBOSubDtlMap::iterator, BBOSubDtlMap::iterator> ret;
		ret = m_clBBOExchSubDtlMap.equal_range(clBBOSub.stDEST_ACCOUNT.GetString());

		bool bAlreadyExist = false;
		if( GetField(e_DestAccount)->GetValueInt() == 0 && clBBOSub.stDEST_ACCOUNT != csSingleAccnt )
			bAlreadyExist = true;

		for (BBOSubDtlMap::iterator it = ret.first; it != ret.second; ++it)
		{
			if( it->second.stEXCH_CODE == clBBOSub.stEXCH_CODE )
			{
				bAlreadyExist = true;
				break;
			}
		}

		if( !bAlreadyExist )
		{
			m_clBBOExchSubDtlMap.insert( std::pair<string, BBOSubscriptionDtl>(clBBOSub.stDEST_ACCOUNT, clBBOSub) );
			m_clDistDestAccount[clBBOSub.stDEST_ACCOUNT]= true;
		}

		set->MoveNext();
	}

	set->Close();	
	}

	CString csTemp;
	csTemp.Format("%d BBO_Pord Subscription loaded and %d BBO_Exch Subscription loaded", m_clBBOProdSubDtlMap.size(),  m_clBBOExchSubDtlMap.size() );
	fnAddMessage( csTemp );
}

bool ExecutionBBO::GetFilePathForProduct(const string &stProdCode, string &stFolderPath, string &stFilePath)
{
	FileDtlMap::iterator itr = m_clProdFileDtlMap.begin();

	while( itr != m_clProdFileDtlMap.end() )
	{
		if( itr->second.strProd_code == stProdCode )
		{
			stFilePath = itr->second.strFilePath;
			stFolderPath = itr->second.strFolderPath;
			return true;
		}

		itr++;
	}

	return false;
}

bool ExecutionBBO::GetFilePathForExchange(const string &stExchCode, string &stFilePath, string &stFolderPath, int &iarStart)
{
	FileDtlMap::iterator itr = m_clProdFileDtlMap.begin();

	std::advance(itr, iarStart);

	while( itr != m_clProdFileDtlMap.end() )
	{
		if( itr->second.strExch_code == stExchCode )
		{
			stFilePath = itr->second.strFilePath;
			stFolderPath = itr->second.strFolderPath;
			iarStart++;
			return true;
		}

		iarStart++;
		itr++;
	}

	return false;
}

void ExecutionBBO::UploadSubscription()
{	
	CString csTemp;

	CString __csDestinationPath = ((ListField*)GetField(e_S3FTPBucket))->GetShortString();
	//CString __csAccount = ((ListField*)GetField(e_DestAccount))->GetShortString(); // This is Test Account

	BBOSubDtlMap::iterator itr =  m_clBBOProdSubDtlMap.begin( );

	csTemp.Format("Uploading Product level account");
	fnAddMessage( csTemp );
	this->RequestExternalUpdate();

	CString csPrevDestAccount("");
	while( itr != m_clBBOProdSubDtlMap.end() )
	{
		CString csBBO_code	= itr->second.stBBO_CODE;
		CString csDestAccount = itr->second.stDEST_ACCOUNT;

		CString csDestinationFolder;
		//if( GetField(e_DestAccount)->GetValueInt() == 1 )
			csDestAccount = csDestAccount;
		//else
			//csDestAccount = __csAccount;

		csDestinationFolder.Format("%s%s/",__csDestinationPath,  csDestAccount );		

		string csSourcePath;
		string csFolderPath; 
		if( GetFilePathForProduct( csBBO_code.GetString(), csFolderPath, csSourcePath) )
		{
			CString csAWSCommand;
			csAWSCommand.Format(_T("aws s3 cp \"%s\" --profile s3user \"%s\""),csSourcePath.c_str(), csDestinationFolder);					
			int iUploadReturn = system(csAWSCommand);	

			CString csFileName = csSourcePath.c_str();
			csFileName.Replace(csFolderPath.c_str(),"");
			csFileName.Replace("\\","");

			int iIndex = csFileName.Find("_");
			CString csExchange = csFileName.Left(iIndex);

			if( iUploadReturn == 0 )
			{
				CString stroutput;
				stroutput.Format("cmd %s", csAWSCommand );
				fnAddMessage( stroutput );
				
				unsigned long lsize = GetFileSize(csSourcePath);

				fnInsertDeliveryReport("BBO", csDestAccount, csFileName, csExchange, lsize, csAWSCommand);
			}
			else
			{
				CString stroutput;
				stroutput.Format("Error in upload cmd %s", csAWSCommand );
				fnAddMessage( stroutput );

				fnInsertErroReport("BBO", csDestAccount, csFileName, csExchange, csAWSCommand);
			}	

			this->RequestExternalUpdate();	

			//if( GetField(e_DestAccount)->GetValueInt() == 0 )
				//break;
		}

		itr++;
	}

	csTemp.Format("Uploading exchange level account");
	fnAddMessage( csTemp );
	this->RequestExternalUpdate();
		
	itr =  m_clBBOExchSubDtlMap.begin( );

	while( itr != m_clBBOExchSubDtlMap.end() )
	{
		CString csExch_code	= itr->second.stEXCH_CODE;
		CString csDestAccount = itr->second.stDEST_ACCOUNT;

		CString csDestinationFolder;
		//if( GetField(e_DestAccount)->GetValueInt() == 1 )
			csDestAccount = itr->second.stDEST_ACCOUNT;
		//else
			//csDestAccount = __csAccount;

		csDestinationFolder.Format("%s%s/",__csDestinationPath,  csDestAccount);

		int iStart(0);
		bool bNextFile = true;

		string csSourcePath;
		string csFolderPath; 
		bNextFile = GetFilePathForExchange(csExch_code.GetString(), csSourcePath, csFolderPath, iStart);

		if( bNextFile )
		{
			CString csAWSCommand;
			csAWSCommand.Format(_T("aws s3 sync %s --profile s3user \"%s\"  --exclude \"*\" --include \"%s*.ZIP\""), csFolderPath.c_str(), csDestinationFolder, csExch_code);					
			int iUploadReturn = system(csAWSCommand);	
			
			if( iUploadReturn == 0 )
			{
				CString stroutput;
				stroutput.Format("cmd %s", csAWSCommand );
				fnAddMessage( stroutput );

				while( bNextFile  )
				{
					CString csFileName = csSourcePath.c_str();
					csFileName.Replace(csFolderPath.c_str(),"");
					csFileName.Replace("\\","");

					unsigned long lsize = GetFileSize(csSourcePath);

					fnInsertDeliveryReport("BBO", csDestAccount, csFileName, csExch_code, lsize, csAWSCommand);

					bNextFile = GetFilePathForExchange(csExch_code.GetString(), csSourcePath, csFolderPath, iStart);
				}		
			}
			else
			{
				CString csFileName = csSourcePath.c_str();
				csFileName.Replace(csFolderPath.c_str(),"");
				csFileName.Replace("\\","");

				CString stroutput;
				stroutput.Format("Error in upload cmd %s", csAWSCommand );
				fnAddMessage( stroutput );

				fnInsertErroReport("BBO", csDestAccount, csFileName, csExch_code, csAWSCommand);
			}

			this->RequestExternalUpdate();			

			/*CString csAWSCommand;
			csAWSCommand.Format(_T("aws s3 cp %s --profile s3user %s"),csSourcePath.c_str(), csDestinationFolder);					
			int iUploadReturn = system(csAWSCommand);	

			if( iUploadReturn == 0 )
			{
				CString stroutput;
				stroutput.Format("cmd %s", csAWSCommand );
				fnAddMessage( stroutput );
			}
			else
			{
				CString stroutput;
				stroutput.Format("Error in upload cmd %s", csAWSCommand );
				fnAddMessage( stroutput );
			}

			bNextFile = GetFilePathForExchange(csExch_code.GetString(), csSourcePath, csFolderPath, iStart);

			if( GetField(e_DestAccount)->GetValueInt() == 0 )
				break;*/
		}

		//if( GetField(e_DestAccount)->GetValueInt() == 0 )
			//break;

		itr++;
	}
}

void ExecutionBBO::fnGenerateReports( )
{
	CString __csDestinationPath = ((ListField*)GetField(e_S3FTPBucket))->GetShortString();

	CString strBasePath = fld_OutputPath->GetValueString();

	CString strPath; 
	strPath.Format("%s\\%s_Report", strBasePath, m_clScheduleInfo.csDate);

	if( CreateDirectory(strPath, NULL) == 0 )
	{
		if( GetLastError() == ERROR_PATH_NOT_FOUND )
		{			
			//csTemp = "Error OputPath Not Found";				
		}
	}

	DistItemMap::iterator itrAcc = m_clDistDestAccount.begin();

	while( itrAcc != m_clDistDestAccount.end() )
	{	
		CString  csLine1, csLine;
		int iI = 0;
		for( ; iI < m_clReportTotalFileV.size(); iI++ )
		{
			if( m_clReportTotalFileV[iI].csDestAccount == itrAcc->first  )
			{
				csLine1 = "BBO Files Info\n\n\nFile Count/Size(KB) for all exchanges\n\n_________________________________\n\n";
				csLine.Format("%d\t\t%u\n\n\n", m_clReportTotalFileV[iI].iFileCount, (unsigned long)m_clReportTotalFileV[iI].lFileSize/1024);		
				break;
			}
		}

		if( iI == m_clReportTotalFileV.size()  )
		{
			itrAcc++;
			continue;
		}

		// Open file for that customer 
		FILE *fileOut = NULL;

		CString csDestAcc = strPath+"\\"+itrAcc->first;
		CreateDirectory(csDestAcc, NULL);

		CString csFileName;
		csFileName.Format("%s\\bboDeliveryInfo_%s.rpt", csDestAcc, m_clScheduleInfo.csDate);
								
		if( fopen_s(&fileOut, csFileName, "w") != 0 )
		{			
			itrAcc++;
			continue;
		}

		CString stroutput;
		stroutput.Format("Creating report for %s", itrAcc->first );
		fnAddMessage( stroutput );
		this->RequestExternalUpdate();

		fputs( csLine1, fileOut );
		fputs( csLine, fileOut );

		csLine = "File Count/Size(KB) by exchanges\n\n_________________________________\n\n";

		fputs( csLine, fileOut );

		iI = 0;
		for( ; iI < m_clReportExchangeTotalFileV.size(); iI++ )
		{
			if( m_clReportExchangeTotalFileV[iI].csDestAccount == itrAcc->first  )
			{
				csLine.Format("%s\t\t%d\t\t%u\n", m_clReportExchangeTotalFileV[iI].csExchange, m_clReportExchangeTotalFileV[iI].iFileCount, (unsigned long)m_clReportExchangeTotalFileV[iI].lFileSize/1024);

				fputs( csLine, fileOut );
			}
		}

		csLine = "\n\nClient File size (Bytes) and delivery time\n\n___________________________________\n\n";
		fputs( csLine, fileOut );

		if( m_clReporFileMapV.find( itrAcc->first ) != m_clReporFileMapV.end() )
		{
			std::vector<ReporFile> &clReportFileV = m_clReporFileMapV[itrAcc->first];

			for(int iK = 0; iK < clReportFileV.size(); iK++ )
			{
				csLine.Format("%s\t%u\t%s\n", clReportFileV[iK].csFileName, clReportFileV[iK].lFileSize, clReportFileV[iK].csDeliveryTime);
				fputs( csLine, fileOut );
			}
		}

		fclose( fileOut );
		fileOut = NULL;

		//UPLOAD report files.........
		CString csDestinationFolder;
		csDestinationFolder.Format("%s%s/",__csDestinationPath,  itrAcc->first );	

		CString csAWSCommand;
		csAWSCommand.Format(_T("aws s3 cp \"%s\" --profile s3user \"%s\" "), csFileName, csDestinationFolder );					
		int iUploadReturn = system(csAWSCommand);	

		if( iUploadReturn == 0 )
		{
			CString stroutput;
			stroutput.Format("cmd %s", csAWSCommand );
			fnAddMessage( stroutput );
		}
		else
		{
			CString stroutput;
			stroutput.Format("Error in report upload cmd %s", csAWSCommand );
			fnAddMessage( stroutput );

			fnInsertErroReport("BBO", itrAcc->first, csFileName, "error while uploading report", csAWSCommand);
		}

		itrAcc++;
	}
}

void ExecutionBBO::fnInsertDeliveryReport(CString csPrdouct, CString csDestAccount, CString csFileName, CString csExchangeCode, unsigned long &lFileSize, CString &csAwscmd )
{
	CString csDeliveryTime;
	COleDateTime oleDateTime = COleDateTime::GetCurrentTime();
	csDeliveryTime.Format("%02d-%02d-%02d %02d:%02d", oleDateTime.GetYear(), oleDateTime.GetMonth(), oleDateTime.GetDay(), oleDateTime.GetHour(), oleDateTime.GetMinute());

	int iI = 0;
	for( ; iI < m_clReportTotalFileV.size(); iI++ )
	{
		if( m_clReportTotalFileV[iI].csDestAccount == csDestAccount  )
		{
			m_clReportTotalFileV[iI].iFileCount++;
			m_clReportTotalFileV[iI].lFileSize += lFileSize;
			break;
		}
	}

	if( iI == m_clReportTotalFileV.size() )
	{
		ReportTotalFile clReportTotalFile;

		clReportTotalFile.csDestAccount = csDestAccount;
		clReportTotalFile.iFileCount	= 1;
		clReportTotalFile.lFileSize		= lFileSize;

		m_clReportTotalFileV.push_back( clReportTotalFile );
	}

	// Exchange based reports
	iI = 0;
	for( ; iI < m_clReportExchangeTotalFileV.size(); iI++ )
	{
		if( m_clReportExchangeTotalFileV[iI].csDestAccount == csDestAccount &&   m_clReportExchangeTotalFileV[iI].csExchange ==  csExchangeCode )
		{
			m_clReportExchangeTotalFileV[iI].iFileCount++;
			m_clReportExchangeTotalFileV[iI].lFileSize += lFileSize;
			break;
		}
	}

	if( iI == m_clReportExchangeTotalFileV.size() )
	{
		ReportExchangeTotalFile clReportExchangeTotalFile;

		clReportExchangeTotalFile.csDestAccount = csDestAccount;
		clReportExchangeTotalFile.csExchange	= csExchangeCode;
		clReportExchangeTotalFile.iFileCount	= 1;
		clReportExchangeTotalFile.lFileSize		= lFileSize;

		m_clReportExchangeTotalFileV.push_back( clReportExchangeTotalFile );
	}

	// All file 
	if( m_clReporFileMapV.find( csDestAccount ) == m_clReporFileMapV.end( ) )
	{
		m_clReporFileMapV[csDestAccount];
	}

	ReporFile clReporFile;
	clReporFile.csDestAccount	= csDestAccount;
	clReporFile.csFileName		= csFileName;
	clReporFile.csDeliveryTime	= csDeliveryTime;
	clReporFile.lFileSize		= lFileSize;

	m_clReporFileMapV[csDestAccount].push_back( clReporFile );


	CString csCheckSum = "";

	CString csSql;
	csSql.Format("EXEC Proc_Beast_Submit_CME_Daily_Delivery_Report '%s', '%s', '%s', %u, '%s','%s','%s','%s'", csPrdouct, csDestAccount, csFileName, lFileSize, csCheckSum, csDeliveryTime, csExchangeCode, csAwscmd);
	
	_RecordsetPtr set;
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)csSql, true)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSql);
		PrintRawMessage(csSql);
		return;
	}
}

void ExecutionBBO::fnInsertErroReport(CString csPrdouct, CString csDestAccount, CString csFileName, CString csExchnage, CString &csErrorCmd )
{	
	CString csSql;
	csSql.Format("EXEC Proc_Beast_Submit_CME_Daily_Error_Report '%s', '%s', '%s', '%s', '%s'", csPrdouct, csDestAccount, csFileName, csExchnage, csErrorCmd);
	
	_RecordsetPtr set;
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)csSql, true)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSql);
		PrintRawMessage(csSql);
		return;
	}
}

void ExecutionBBO::fnGetCheksumList( )
{	
	std::map<CString,CString> m_clChecksumMap;

	CString csInputPath = "\\\\powervault3\\CME_s3\\daily\\bbo\\globex\\20150828";
	CString csOuptFile = csInputPath + "\\AllChecksum.txt";

	CString csCMD;
	csCMD.Format("D:\\fciv.exe %s -wp -r -type *.gz -md5 > %s", csInputPath, csOuptFile);
	
	int iValue = system(csCMD);	
	if( iValue != 0 )
	{
		fnAddMessage( "fciv.exe is not found in D drive" );
		return;
	}

	FILE *fileIn(NULL);
	
	if( fopen_s(&fileIn, csOuptFile, "r") != 0 )
	{
		fnAddMessage( "Error while Opening fileIn" );
		return;
	}

	char chline[1024];
	CString csStrLine;

	// Read Header...
	while( fgets(chline, 1024, fileIn) )
	{
		if( strchr(chline, '\n') )
		{
			csStrLine += chline;
			
			if( csStrLine.Find(".gz") ^ -1 )
			{
				int iIndext = csStrLine.Find(" ");
				CString csCheckSum = csStrLine.Left(iIndext);
				CString csFileName = csStrLine.Mid(iIndext, 256);

				m_clChecksumMap.insert(std::pair<CString,CString>(csFileName, csCheckSum));
			}
			
			csStrLine = "";
		}
		else
		{
			csStrLine += chline;
		}		
	}

	fclose(fileIn);
}


const CString	__strSmtp	= "email-smtp.us-east-1.amazonaws.com";
const CString	__strLogin	= "AKIAIERFOJMRYCRRAHYQ";
const CString	__strPassword = "AvauV+bh9qVBlRQuCI5u9GMG2O1N2hajyhzE7MhyYANC"; 
const CString	__strSender	= "cmenotifications@thebeastapps.com";
const int		__iPort = 587;

void  ExecutionBBO::fnSendEmail(int iType, int iRowCount)
{
	CSmtp mail;

	mail.SetSMTPServer(__strSmtp, __iPort, true);
	mail.SetSecurityType((SMTP_SECURITY_TYPE)USE_TLS);		
	mail.SetLogin(__strLogin);
	mail.SetPassword(__strPassword);
			
	//CC
	/*CString strSendTo		= GetField(e_T0)->GetValueString();
	CString strCC			= GetField(e_CC)->GetValueString();
	CString strSubject		= GetField(e_Subject)->GetValueString();
	CString strBody			= GetField(e_Body)->GetValueString();*/
	
	CString strSendTo		= "vcmops@thebeastapps.com,";
	CString strCC			= "mpatel@thebeastapps.com,dmodi@thebeastapps.com,mvpatel@thebeastapps.com,";
	CString strSubject		= "";
	CString strBody			= "";
	CString strTemp			= "";

	//if( iType == 1 )
	//{
	//	strSubject = "BBO Notification";
	//	//m_clScheduleInfo.csFilePath
	//	//strBody.Format("\nBLOCK File Name: %s\n\n",m_clScheduleInfo.csFilePath);

	//	strTemp.Format("Total Row Count: %d\n\n",iRowCount);
	//	strBody += strTemp;

	//	strTemp.Format("Thanks\nBeast Apps Team\n");
	//	strBody += strTemp;
	//}
	//else 
	if( iType == 2 )
	{
		strSubject = "BBO Notification";
		//m_clScheduleInfo.csFilePath
		strBody.Format("\nBBO Schedule Date: %s\n\n",m_clScheduleInfo.csDate);

		strTemp.Format("Status: Processed successfully\n\n",iRowCount);
		strBody += strTemp;

		strTemp.Format("Thanks\nBeast Apps Team\n");
		strBody += strTemp;
	}

	
	//++++++++++++++++++++++++Send To+++++++++++++++++++++++
	if( !strSendTo.IsEmpty() )
	{
		if( strSendTo.Right(1).CompareNoCase(",") != 0 )
		{
			strSendTo = strSendTo + ",";
		}		
	}	
	
	if( strSendTo.Find(",") != -1 )
	{
		while(!strSendTo.IsEmpty())
		{
			CString tmpSendTo = strSendTo.Left(strSendTo.Find(","));			
			strSendTo.Delete(0,strSendTo.Find(",")+1);		
			tmpSendTo.TrimLeft();
			tmpSendTo.TrimRight();

			if(!tmpSendTo.IsEmpty())
				mail.AddRecipient(tmpSendTo);
		}
	}		
	else
	{			
		if( !strSendTo.IsEmpty() )
			mail.AddRecipient(strSendTo);
	}
	//+++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++ CC +++++++++++++++++++++++
	if( !strCC.IsEmpty() )
	{
		if( strCC.Right(1).CompareNoCase(",") != 0 )
		{
			strCC = strCC + ",";
		}		
	}		

	if( strCC.Find(",") != -1 )
	{
		while(!strCC.IsEmpty())
		{
			CString tmpCC = strCC.Left(strCC.Find(","));			
			strCC.Delete(0,strCC.Find(",")+1);		
			tmpCC.TrimLeft();
			tmpCC.TrimRight();

			if(!tmpCC.IsEmpty())
				mail.AddCCRecipient(tmpCC);
		}
	}		
	else
	{			
		if(!strCC.IsEmpty())
			mail.AddCCRecipient(strCC);
	}
	//+++++++++++++++++++++++++++++++++++++++++++++++
		
	mail.SetSenderMail(__strSender);
				
	if( !strBody.IsEmpty() )
		mail.AddBody(strBody);
				
	if( !strSubject.IsEmpty() )
		mail.SetSubject(strSubject);
		
	try
	{			
		mail.Send(NULL);		
		//GetField(e_Status)->SetValueString("Mail Sent successfully.");  	
	}
	catch(ECSmtp e)
	{
		CString stemp;
		stemp.Format("Mail Failed...! - %s", e.GetErrorText());		
		//GetField(e_Status)->SetValueString(stemp); 
		fnAddMessage( stemp );
		return;
	}
	catch(...)
	{
		CString stemp;
		stemp.Format("Mail Failed...! - Error Id: %d", GetLastError());		
		//GetField(e_Status)->SetValueString(stemp);  
		fnAddMessage( stemp );
		return;
	}

	fnAddMessage( "EMail sent successfully");
}