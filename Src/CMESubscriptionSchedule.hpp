
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <shared/MDDDataHandler.hpp>

BF_NAMESPACE_BEGIN
using namespace std;

struct ScheduleDtl
{
	CString csFileType;
	CString csSubType;
	COleDateTime clScheduleDtTime;
	COleDateTime oleStartDtTime;
	COleDateTime oleEndDtTime;
	int iStatus;

	ScheduleDtl( )
	{		
	}
};

typedef std::vector<ScheduleDtl> ScheduleDtlV;

class CMESubscriptionSchedule : public BaseImage
{
public:
	
	enum FieldIDs
	{	
		e_Message	= 61,

		e_FileType	= 200,
		e_Year		= 201,
		e_Month		= 202,
		e_Day		= 203,
		e_SubType	= 212,
		e_Houre		= 213,
		e_Minute	= 214,

		e_FilterFileType= 250,
		e_FilterYear	= 251,
		e_FilterMonth	= 252,
		e_FilterDay		= 253,
		e_FilterStatus	= 254,
		
		e_CreateSchedule = 113,
		e_LoadSchedule	 = 112,

		e_AdminBtn		 = 116,

		e_CreateAllScheduleFromDay		= 119,
		e_DeleteaAllRemainingSchedule	= 120,

		e_PgUpBase = 92,
		e_PgDnBase = 93,

		e_FileTypeBase = 4000,
		e_SubTypeBase = 3000,
		e_DateBase	  = 2000,
		e_TimeBase	  = 2050,
		e_WeekDayBase = 4050,
		e_StatusBase  = 3050,

		e_StartTimeBase = 5000,
		e_EndTimeBase = 5050,
	};
	
	CMESubscriptionSchedule();
	~CMESubscriptionSchedule();	

	bool InitData();		
	void Recalculate();	


	void CMESubscriptionSchedule::ChangeValidatation( );
	void CMESubscriptionSchedule::fnCreateSchedules( );
	void CMESubscriptionSchedule::fnRemoveSchedules( );
	void CMESubscriptionSchedule::LoadSchedule();
	void CMESubscriptionSchedule::fnPageUpDown( );
	void CMESubscriptionSchedule::DisplaySubscription();
	void CMESubscriptionSchedule::fnUpdateScheduleStatus( int iStatus, ScheduleDtl &clScheduleDtl );
	
private:
	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;
	
	int m_numofRows;
	int m_curTopRow;

	ScheduleDtlV m_clScheduleDtlV;

	ListField *fld_FileType;
	ListField *fld_Year;
	ListField *fld_Month;
	ListField *fld_SubType;
	ListField *fld_Houre;
	ListField *fld_Minute;

public:	
	
};

BF_NAMESPACE_END


