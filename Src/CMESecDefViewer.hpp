// Copyright (C) 2000 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#pragma once

#include <shared/BaseImage.hpp>
#include <portalhandler/BFInterestRateBaseApp.hpp>
#include <shared/DBConnection.hpp>

#define _ColumnCount 17

BF_NAMESPACE_BEGIN

struct FileDetail
{
	CString csFileName;
	unsigned long lFileSize;
	int iStatus;
};	


struct stSecDef
{
	long iSec_ID;			
	CString csExch_Code;
	CString csProd_Code;	
	CString csProd_Desc;	
	CString csGrp_Code;
	CString csFOI;
	CString csContractMY;
	CString csSecDefMsg;
	
	stSecDef()
	{
		iSec_ID	= 0;		
		csExch_Code	= "";
		csProd_Code	= "";
		csProd_Desc	= "";
		csGrp_Code	= "";
		csFOI		= "";
		csContractMY = "";
		csSecDefMsg	= "";
	}
};


class SecurityDefViewer : public BaseImage 
{

public:
	enum Field
	{
		e_Message = 61,
		//e_FileType = 200,
		e_SecIdsearch = 100,
		e_Year = 2001,
		e_Month = 2002,
		e_day = 2003,
		e_FilePath = 2005,
		e_statusMsg = 2006,
		e_TotalCount = 2007,
		e_PgNumber = 3022,

		e_WeekYear	=	2008,
		e_WeekMonth	=	2009,
		e_weekDay	=	2010,

		e_GenWeekSecDef	= 2011,

		e_SecDefYear = 2012,
		e_GenSecDefYear = 2013,
		
		e_Sec_IDSrc		= 100,	
		e_Exch_CodeSrc	= 101,
		e_Prod_CodeSrc	= 102,
		e_Prod_DescSrc	= 103,
		e_Grp_CodeSrc	= 104,
		e_FOIsrc		= 105,		
		e_SecDefMsgSrc	= 106,
		
		
		e_Sec_ID	=151,
		e_Exch_Code	=201,
		e_Prod_Code	=251,
		e_Prod_Desc	=301,
		e_Grp_Code	=351,
		e_FOI		=401,
		e_SecDefMsg	=451,
		
		e_LoadSecDef	= 5011,
		e_LoadFromDB	= 5012,
		e_Searchsec		= 5013,
		

		e_PgTopBase = 6015,
		e_PgUpBase	= 6016,
		e_PgDnBase	= 6019,
		e_PgBottombase = 6018,

		
	};

	SecurityDefViewer();
	virtual ~SecurityDefViewer();
	

	bool InitData();
	void Recalculate();
	
	
	int m_numofRows;
	int m_curTopRow;

	int m_numofRowsDB;
	int m_curTopRowDB;
	
	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;
	
	std::vector<FileDetail> m_clFileList;
	std::vector<FileDetail> m_clFileListDB;

	std::map<long, stSecDef> CME_SecDefMap;
	//std::map<long, stSecDef> CME_SecDefMapFilter;
	//std::vector<stSecDef> m_clSecDefinition;
	stSecDef sSecurityDef;
	void ExtractTokensToArray(std::string s, std::string Delimiters);
	void LoadSecDefinition();
	void SearchSecDefinition();

	void GenerateWeekSecDef();
	void GenerateYearSecDef();
	
private:

	//void SecurityDefViewer::InsertIntoDB( );
	void SecurityDefViewer::DisplaySecDefList( );
	//void SecurityDefViewer::fnCreateFilePath( );
	void SecurityDefViewer::fnPageUpDown( );
	//void SecurityDefViewer::LoadFromDB( );
};

BF_NAMESPACE_END
