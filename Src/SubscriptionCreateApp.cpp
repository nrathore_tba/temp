// Copyright (C) 2000 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#include <stdafx.h>

#include "SubscriptionCreateApp.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(ProductMasterFinApp)

ProductMasterFinApp::ProductMasterFinApp()
{
}

ProductMasterFinApp::~ProductMasterFinApp()
{
}

bool ProductMasterFinApp::RealInitData()
{ 
	m_curTopRow = 0;
	for(m_numofRows = 0; GetField(e_AllProdNameBase + m_numofRows); m_numofRows++);

	m_curTopSelRow = 0;
	for(m_numofSelRows = 0; GetField(e_SelProdNameBase + m_numofSelRows); m_numofSelRows++);

	m_curTopSubRow = 0;
	for(m_numofSubRows = 0; GetField(e_SubProdName + m_numofSubRows); m_numofSubRows++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	// DB Connection Completed....

	bLoadedFromDB = true;

	return true;
}

void ProductMasterFinApp::RealRecalculate()
{	
	ProductFromDB( );
	
	FilterEnabledDisabled( );

	if( IsFieldNew(e_ClearSelection) )
	{
		m_csSelectedProductsV.clear();
		m_BFFromDtV.clear();
		m_BFToDtV.clear();
		m_clSubscriptionStV.clear( );	

		m_curTopSubRow = 0;
		m_curTopSelRow = 0;
	}

	if( IsFieldNew( e_Create ) )
	{
		fnCreateSubscription( );
	}
	
	fnPageUpDown( );
	fnDisplayDetails( );

	fnDisplaySubscription( );
}

void ProductMasterFinApp::FilterEnabledDisabled( )
{
	if( GetField(e_FileType)->GetValueInt() == enm_EOD || GetField(e_FileType)->GetValueInt() == enm_BLOCK )
	{
		GetField(e_VenueType)->SetEnabled(true);
		GetField(e_VenueType + 1)->SetEnabled(true);
	}
	else
	{
		GetField(e_VenueType)->SetValueInt(0);		
		GetField(e_VenueType)->SetEnabled(false);

		GetField(e_VenueType + 1)->SetValueInt(0);		
		GetField(e_VenueType + 1)->SetEnabled(false);
	}

	if( GetField(e_FileType)->GetValueInt() == enm_Ref )
	{
		for( int iI = 0; iI < 4; iI++ )
		{
			GetField(e_FOIType + iI)->SetValueInt(0);		
			GetField(e_FOIType + iI)->SetEnabled(false);
		}
		
		GetField(64)->SetVisible(true);
		GetField(e_Contract)->SetNormalState();
		GetField(e_Contract)->SetEnabled(true);
		GetField(e_Contract)->SetVisible(true);
	}
	else
	{
		for( int iI = 0; iI < 4; iI++ )
		{
			GetField(e_FOIType + iI)->SetEnabled(true);
		}

		GetField(64)->SetVisible(false);
		GetField(e_Contract)->SetBlankState();
		GetField(e_Contract)->SetEnabled(false);
		GetField(e_Contract)->SetVisible(false);
	}

	if( GetField(e_FileType)->GetValueInt() == enm_MD )
	{
		GetField(59)->SetVisible(true);
		GetField(e_FileFormatType)->SetEnabled(true);
		GetField(e_FileFormatType+1)->SetEnabled(true);

		GetField(e_FileFormatType)->SetVisible(true);
		GetField(e_FileFormatType+1)->SetVisible(true);

		if( GetField(e_FileFormatType)->GetValueInt() == 1 )
			m_csSelectedProductsV.clear();
	}
	else
	{
		GetField(59)->SetVisible(false);
		GetField(e_FileFormatType)->SetEnabled(false);
		GetField(e_FileFormatType+1)->SetEnabled(false);

		GetField(e_FileFormatType)->SetVisible(false);
		GetField(e_FileFormatType+1)->SetVisible(false);

		GetField(e_FileFormatType)->SetValueInt(0);
		GetField(e_FileFormatType+1)->SetValueInt(0);
	}

	if( GetField(e_OrderType)->GetValueInt( ) == 1 )
	{
		GetField(e_SubscriptionPeriod)->SetNormalState();
		GetField(e_SubscriptionPeriod)->SetEnabled(true);
	}
	else
	{
		GetField(e_SubscriptionPeriod)->SetBlankState();
		GetField(e_SubscriptionPeriod)->SetEnabled(false);
	}
}

bool ProductMasterFinApp::fnCheckExchangeAssets(bool &bExchangeChange )
{
	for( int iI = 0; iI < 9; iI++ )
	{
		if( iI < 4 && IsFieldNew(e_ExchType + iI) )
		{
			bExchangeChange = true;
			return true;
		}

		if( IsFieldNew(e_AssetType + iI) )
			return true;
	}
	
	for( int iI = 0; iI < 4; iI++ )
	{
		if(IsFieldNew(e_FOIType + iI) )
			return true;
	}

	if( IsFieldNew(e_VenueType) || IsFieldNew(e_VenueType + 1) )
		return true;

	if( IsFieldNew(e_ProdFilter) )
		return true;

	if( IsFieldNew(e_FileType) )
		return true;

	return false;
}

CString _mapExch[4] = { "XCME","XCBT","XCEC","XNYM" };
CString _mapAsset[9] = { "COM","ENE","EQU","FX","INT","METAL", "ECO", "REA", "WEA"};

void ProductMasterFinApp::ProductFromDB( )
{
	bool bExchangeChange = false;
	bool bAnyChangeInExchange = fnCheckExchangeAssets(bExchangeChange);

	if( !bAnyChangeInExchange && m_csProductsV.size() > 0 )
		return;

	if( bExchangeChange )
	{
		for( int iI = 0; iI < 9; iI++ )
		{
			GetField(e_AssetType + iI)->SetValueInt(0);
		}
		m_clAssetMap.clear();
	}
		
	m_csProductsV.clear();

	CString csExchCode = "";
	CString csAssetClass = "";
	CString csFOI = "";
	CString csIsSpread = "";
	CString csVenueCode = "";

	for( int iI = 0; iI < 9; iI++ )
	{
		if( iI < 4 && GetField(e_ExchType + iI)->GetValueInt() == 1 )
		{
			if( csExchCode == "" )
				csExchCode.Format("EXCH_CODE in ('%s'",_mapExch[iI]);
			else
				csExchCode += ",'"+_mapExch[iI] +"'";
		}

		if( GetField(e_AssetType + iI)->GetValueInt() == 1 )
		{
			if( csAssetClass == "" )
				csAssetClass.Format("PROD_CATEGORY in ('%s'",_mapAsset[iI]);
			else
				csAssetClass += ",'"+_mapAsset[iI] +"'";
		}
	}

	if( GetField(e_FileType)->GetValueInt() ^ enm_Ref )
	{
		CString csFut, csOpt;
		if( GetField(e_FOIType + 0)->GetValueInt() == 1 && GetField(e_FOIType + 2)->GetValueInt() == 1 )
			csFut.Format("FOI = 'FUT' AND SPREAD_IND in ('0','1')");
		else if( GetField(e_FOIType + 0)->GetValueInt() == 1 )
			csFut.Format("FOI = 'FUT' AND SPREAD_IND = '0'");
		else if( GetField(e_FOIType + 2)->GetValueInt() == 1 )
			csFut.Format("FOI = 'FUT' AND SPREAD_IND = '1'");
		
		if( GetField(e_FOIType + 1)->GetValueInt() == 1 && GetField(e_FOIType + 3)->GetValueInt() == 1 )
			csOpt.Format("FOI = 'OPT' AND SPREAD_IND in ('0','1')");
		else if( GetField(e_FOIType + 1)->GetValueInt() == 1 )
			csOpt.Format("FOI = 'OPT' AND SPREAD_IND = '0'");
		else if( GetField(e_FOIType + 3)->GetValueInt() == 1 )
			csOpt.Format("FOI = 'OPT' AND SPREAD_IND = '1'");
		
		if( csFut.GetLength() > 0 && csOpt.GetLength() > 0 )
			csFOI = "( (" + csFut + ") OR ("+ csOpt + ") )";
		else if( csFut.GetLength() > 0 )
			csFOI = "(" + csFut + ")";
		else if( csOpt.GetLength() > 0 )
			csFOI = "(" + csOpt + ")";		
	}

	if( GetField(e_FileType)->GetValueInt() == enm_EOD || GetField(e_FileType)->GetValueInt() == enm_BLOCK )
	{
		if( GetField(e_VenueType)->GetValueInt() == 1 )
			csVenueCode = "E";

		if( GetField(e_VenueType + 1)->GetValueInt() == 1 )
		{
			if( csVenueCode.GetLength() > 0 )
				csVenueCode = "E,F";
			else
				csVenueCode = "F";
		}

		if( csVenueCode.GetLength() == 0 )
			csVenueCode = "E,F";
	}

	if( csExchCode.GetLength() > 0 )
		csExchCode += ")";

	if( csAssetClass.GetLength() > 0 )
		csAssetClass += ")";

	//if( csFOI.GetLength() > 0 )
	//	csFOI += ")";

	CString sql;
	if( csExchCode.GetLength() > 0 &&  csAssetClass.GetLength() > 0 )
	{
		if( csFOI.GetLength() > 0 )
			sql.Format("SELECT  PROD_NAME,PROD_CODE,VENUE_CODE,EXCH_CODE,PROD_CATEGORY FROM    CME_Products_Mst  [NOLOCK] where %s AND %s AND %s order by PROD_NAME", csExchCode, csAssetClass, csFOI);
		else
			sql.Format("SELECT  PROD_NAME,PROD_CODE,VENUE_CODE,EXCH_CODE,PROD_CATEGORY FROM    CME_Products_Mst  [NOLOCK] where %s AND %s order by PROD_NAME", csExchCode, csAssetClass);
	}
	else if( csExchCode.GetLength() > 0 )	
	{
		if( csFOI.GetLength() > 0 )
			sql.Format("SELECT  PROD_NAME,PROD_CODE,VENUE_CODE,EXCH_CODE,PROD_CATEGORY FROM    CME_Products_Mst  [NOLOCK] where %s AND %s order by PROD_NAME", csExchCode, csFOI);
		else
			sql.Format("SELECT  PROD_NAME,PROD_CODE,VENUE_CODE,EXCH_CODE,PROD_CATEGORY FROM    CME_Products_Mst  [NOLOCK] where %s order by PROD_NAME", csExchCode);
	}
	else if( csAssetClass.GetLength() > 0 )
	{
		if( csFOI.GetLength() > 0 )
			sql.Format("SELECT  PROD_NAME,PROD_CODE,VENUE_CODE,EXCH_CODE,PROD_CATEGORY FROM    CME_Products_Mst  [NOLOCK] where %s AND %s order by PROD_NAME", csAssetClass, csFOI);
		else
			sql.Format("SELECT  PROD_NAME,PROD_CODE,VENUE_CODE,EXCH_CODE,PROD_CATEGORY FROM    CME_Products_Mst  [NOLOCK] where %s order by PROD_NAME", csAssetClass);
	}
	else
	{
		if( csFOI.GetLength() > 0 )
			sql.Format("SELECT  PROD_NAME,PROD_CODE,VENUE_CODE,EXCH_CODE,PROD_CATEGORY FROM    CME_Products_Mst  [NOLOCK] where %s order by PROD_NAME", csFOI);
		else
			sql.Format("SELECT  PROD_NAME,PROD_CODE,VENUE_CODE,EXCH_CODE,PROD_CATEGORY FROM    CME_Products_Mst  [NOLOCK] order by PROD_NAME");
	}
	
	CString csProdFilter = GetField(e_ProdFilter)->GetValueString( );

	_RecordsetPtr set;

	DB_TRY
	{
		if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql, false)))
		{
			SetErrorMessage(ErrorSeverity::e_Error, sql);
			PrintRawMessage(sql);
		}
		
		CString csProd, csVenue, csAsset;
		while( VARIANT_FALSE == set->adoEOF )
		{
			SETSTR(	csProd,	set->Fields->Item[_variant_t("PROD_NAME")]->Value);	
			
			bool bOK = true;
			if( csProdFilter.GetLength() > 0 )
			{
				if( csProd.Find( csProdFilter ) == -1 )
					bOK = false;
			}

			if( csProd.GetLength( ) == 0 )
				bOK = false;

			if( GetField(e_FileType)->GetValueInt() == enm_EOD || GetField(e_FileType)->GetValueInt() == enm_BLOCK ) 
			{
				SETSTR(	csVenue,	set->Fields->Item[_variant_t("VENUE_CODE")]->Value);	
				if( csVenueCode.Find(csVenue) == -1 )
					bOK = false;
			}

			if( bOK )
			{
				SETSTR(	csAsset,	set->Fields->Item[_variant_t("PROD_CATEGORY")]->Value);	
				m_clAssetMap.insert(std::pair<CString,CString>(csAsset,csAsset));					
			}

			if( bOK )
				m_csProductsV.push_back( csProd );	

			set->MoveNext();
		}
		set->Close();
	}
	DB_CATCH(sql);	

	GetField(e_Message)->SetTitle("");

	for( int iI = 0; iI < 9; iI++ )
	{
		std::map<CString,CString>::iterator itr = m_clAssetMap.begin();
		for( ; itr != m_clAssetMap.end(); itr++ )
		{
			if( itr->first == _mapAsset[iI] )
			{
				GetField(e_AssetType+iI)->SetEnabled(true);
				break;
			}
		}

		if( itr == m_clAssetMap.end() )
		{
			GetField(e_AssetType+iI)->SetEnabled(false);
			GetField(e_AssetType+iI)->SetValueInt(0);
		}
	}
	fnValidateSelectedProduct( );
}

void ProductMasterFinApp::fnValidateSelectedProduct( )
{	
	for( int iSel = 0; iSel < m_csSelectedProductsV.size( ); )
	{
		int iI = 0;
		for( ; iI < m_csProductsV.size(); iI++ )
		{
			if( m_csSelectedProductsV[iSel] == m_csProductsV[iI] )
				break;
		}

		if( iI == m_csProductsV.size() )
			m_csSelectedProductsV.erase(m_csSelectedProductsV.begin() + iSel);
		else
			iSel++;
	}
}

void ProductMasterFinApp::fnCreateSubscription( )
{
	m_clSubscriptionStV.clear();

	CString csExchCode = "";
	CString csAssetClass = "";
	CString csFOI = "";
	CString csIsSpread = "";
	CString csVenueCode = "";
	
	for( int iI = 0; iI < 9; iI++ )
	{
		if( iI < 4 && GetField(e_ExchType + iI)->GetValueInt() == 1 )
		{
			if( csExchCode == "" )
				csExchCode.Format("%s",GetField(e_ExchType + iI)->GetTitle());
			else
				csExchCode += ","+GetField(e_ExchType + iI)->GetTitle();
		}

		if( GetField(e_AssetType + iI)->GetValueInt() == 1 )
		{
			if( csAssetClass == "" )
				csAssetClass.Format("%s",GetField(e_AssetType + iI)->GetTitle());
			else
				csAssetClass += ","+GetField(e_AssetType + iI)->GetTitle();
		}
	}

	if( GetField(e_FileType)->GetValueInt() ^ enm_Ref )
	{
		if( GetField(e_FOIType + 0)->GetValueInt() == 1 )
			csFOI.Format("FUT");

		if( GetField(e_FOIType + 1)->GetValueInt() == 1 )
			if( csFOI == "" )
				csFOI.Format("OPT");
			else
				csFOI += ",OPT";

		if( GetField(e_FOIType + 2)->GetValueInt() == 1 )
			if( csFOI == "" )
				csFOI.Format("FUT SPREAD");
			else
				csFOI += ",FUT SPREAD";	

		if( GetField(e_FOIType + 3)->GetValueInt() == 1 )
			if( csFOI == "" )
				csFOI.Format("OPT SPREAD");
			else
				csFOI += ",OPT SPREAD";	
	}

	if( GetField(e_FileType)->GetValueInt() == enm_EOD || GetField(e_FileType)->GetValueInt() == enm_BLOCK )
	{
		if( GetField(e_VenueType)->GetValueInt() == 1 )
			csVenueCode = "Globex";

		if( GetField(e_VenueType + 1)->GetValueInt() == 1 )
		{
			if( csVenueCode.GetLength() > 0 )
				csVenueCode = "Globex,Floor";
			else
				csVenueCode = "Floor";
		}

		//if( csVenueCode.GetLength() == 0 )
		//	csVenueCode = "E,F";
	}

	if( GetField(e_OrderType)->GetValueInt() == 0 && GetField(e_OrderType+1)->GetValueInt() == 0)
	{
		GetField(e_Message)->SetTitle("Please select at least one Subscription/One Time Order");
		return;
	}	

	if( csExchCode.GetLength() == 0 && csAssetClass.GetLength() == 0 )
	{
		GetField(e_Message)->SetTitle("Please select at least Exchange AND/OR  Asset Class ");
		return;
	}		

	if( csExchCode.GetLength() == 0 )
		csExchCode = "All-Exchanges";

	if( csAssetClass.GetLength() == 0 )
		csAssetClass = "All-Assets";

	if( GetField(e_FileType)->GetValueInt() ^ enm_Ref && csFOI.GetLength() == 0 )
	{
		GetField(e_Message)->SetTitle("Please select at least one Futures/Options/SPreads");
		return;
	}

	if( (GetField(e_FileType)->GetValueInt() == enm_EOD || GetField(e_FileType)->GetValueInt() == enm_BLOCK) && csVenueCode.GetLength() == 0 )
	{
		GetField(e_Message)->SetTitle("Please select at least one Globex/Floor");
		return;
	}

	if( GetField(e_FileType)->GetValueInt() == enm_MD && (GetField(e_FileFormatType)->GetValueInt() == 0 && GetField(e_FileFormatType +1)->GetValueInt() == 0) )
	{
		GetField(e_Message)->SetTitle("Please select at least File Format Type");
		return;
	}

	CString csFileFormat = "";
	if( GetField(e_FileType)->GetValueInt() == enm_MD )
	{
		if( GetField(e_FileFormatType)->GetValueInt() == 1 )
			csFileFormat = "OTW(Raw)";

		if( GetField(e_FileFormatType + 1)->GetValueInt() == 1 )
		{
			if( csFileFormat.GetLength() > 0 )
				csFileFormat = "OTW(Raw), Text(Decoded)";
			else
				csFileFormat = "Text(Decoded)";
		}
	}

	/*if( m_csSelectedProductsV.size( ) == 0 )
	{
		GetField(e_Message)->SetTitle("Please select at least one Product");
		return;
	}*/

	if( GetField(e_OrderType+1)->GetValueInt() == 1 )
	{
		for(int iI = 0; iI < m_csSelectedProductsV.size( ); iI++ )
		{
			if( m_BFFromDtV[iI] >= m_BFToDtV[iI] )
			{
				GetField(e_Message)->SetTitle("Please Correct From/To Date");
				return;
			}
		}

		if( m_csSelectedProductsV.size( ) == 0  && m_BFDateAllPordStart >= m_BFDateAllPordEnd )
		{
			GetField(e_Message)->SetTitle("Please Correct From/To Date");
				return;
		}
	}

	GetField(e_Message)->SetTitle("");

	SubscriptionSt clSubSt;
	if( m_csSelectedProductsV.size( ) > 0 )
	{
		for(int iI = 0; iI < m_csSelectedProductsV.size( ); iI++ )
		{
			clSubSt.iFileType = GetField(e_FileType)->GetValueInt();
			clSubSt.iSubPeriod  = GetField(e_SubscriptionPeriod)->GetValueInt();
			
			clSubSt.csProduct	= m_csSelectedProductsV[iI];
			clSubSt.csAsstClass = csAssetClass;
			clSubSt.csExchange	= csExchCode;
			clSubSt.csFOIS		= csFOI;
			clSubSt.csVenue		= csVenueCode;
			clSubSt.csFileFormat= csFileFormat;
			clSubSt.bfFromDt	= m_BFFromDtV[iI];
			clSubSt.bfToDt		= m_BFToDtV[iI];
			clSubSt.csContr		= ((ListField*)GetField(e_Contract))->GetDisplayString();

			if( GetField(e_OrderType)->GetValueInt( ) == 1 )
			{
				clSubSt.iOrderType = 0;
				m_clSubscriptionStV.push_back( clSubSt );
			}

			if( GetField(e_OrderType + 1)->GetValueInt( ) == 1 )
			{
				clSubSt.iOrderType = 1;
				m_clSubscriptionStV.push_back( clSubSt );
			}
		}
	}
	else
	{
		clSubSt.iFileType = GetField(e_FileType)->GetValueInt();
		clSubSt.iSubPeriod  = GetField(e_SubscriptionPeriod)->GetValueInt();

		clSubSt.csProduct	= "All-Products";
		clSubSt.csAsstClass = csAssetClass;
		clSubSt.csExchange	= csExchCode;
		clSubSt.csFOIS		= csFOI;
		clSubSt.csVenue		= csVenueCode;
		clSubSt.csFileFormat= csFileFormat;
		clSubSt.bfFromDt	= m_BFDateAllPordStart;
		clSubSt.bfToDt		= m_BFDateAllPordEnd;

		clSubSt.csContr		= ((ListField*)GetField(e_Contract))->GetDisplayString();

		if( GetField(e_OrderType)->GetValueInt( ) == 1 )
		{
			clSubSt.iOrderType = 0;
			m_clSubscriptionStV.push_back( clSubSt );
		}

		if( GetField(e_OrderType + 1)->GetValueInt( ) == 1 )
		{
			clSubSt.iOrderType = 1;
			m_clSubscriptionStV.push_back( clSubSt );
		}
	}
}

void ProductMasterFinApp::fnPageUpDown( )
{
	int iTotalRecord = m_csProductsV.size( );	
	
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpBase,e_PgDnBase,iTotalRecord,m_numofRows,m_curTopRow);

	iTotalRecord = m_csSelectedProductsV.size( );	
	
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpSelBase,e_PgDnSelBase,iTotalRecord,m_numofSelRows,m_curTopSelRow);

	iTotalRecord = m_clSubscriptionStV.size( );	
	
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpSubBase,e_PgDnSubBase,iTotalRecord,m_numofSubRows,m_curTopSubRow);
}

void ProductMasterFinApp::fnDisplayDetails( )
{
	bool bEnable(true);
	if( GetField(e_FileType)->GetValueInt() == enm_MD && GetField(e_FileFormatType)->GetValueInt() == 1 )
	{
		bEnable = false;
	}

	CString strTemp;
	strTemp.Format("%d Total Products",m_csProductsV.size( ));
	GetField(e_TotalProduct)->SetTitle(strTemp);
	
	int iRow(0);
	for( int iI = m_curTopRow; iI < m_csProductsV.size( ); iI++, iRow++ )
	{
		if( iRow >= m_numofRows )
			break;

		if( IsFieldNew(e_AllProdNameBase + iRow ) )
		{
			if( GetField(e_AllProdNameBase + iRow)->GetValueInt( ) == 1 )
			{
				// select Product...
				bool bAlreadyAdded = false;
				for( int iJ= 0; iJ < m_csSelectedProductsV.size(); iJ++ )
				{
					if( m_csSelectedProductsV[iJ] == m_csProductsV[iI] )
					{
						bAlreadyAdded = true;
						break;
					}
				}

				if( !bAlreadyAdded )
				{
					m_csSelectedProductsV.push_back( m_csProductsV[iI] );
					m_BFFromDtV.push_back( BFDate() );
					m_BFToDtV.push_back( BFDate() );
				}
			}
			else 
			{
				// De select Product...
				bool bAlreadyAdded = false;
				for( int iJ= 0; iJ < m_csSelectedProductsV.size(); iJ++ )
				{
					if( m_csSelectedProductsV[iJ] == m_csProductsV[iI] )
					{
						bAlreadyAdded = true;
						m_csSelectedProductsV.erase(m_csSelectedProductsV.begin()+iJ);
						m_BFFromDtV.erase(m_BFFromDtV.begin()+iJ);
						m_BFToDtV.erase(m_BFToDtV.begin()+iJ);
						break;
					}
				}			
			}
		}

		bool bAlreadyAdded = false;
		for( int iJ= 0; iJ < m_csSelectedProductsV.size(); iJ++ )
		{
			if( m_csSelectedProductsV[iJ] == m_csProductsV[iI] )
			{
				bAlreadyAdded = true;
				break;
			}
		}

		CString cstemp = m_csProductsV[iI] ;
		cstemp.Replace("%", "\\%");
		GetField(e_AllProdNameBase + iRow )->SetValueInt( bAlreadyAdded ? 1: 0 );		
		GetField(e_AllProdNameBase + iRow )->SetTitle( cstemp );
		GetField(e_AllProdNameBase + iRow )->SetToolTipText( m_csProductsV[iI] );
		GetField(e_AllProdNameBase + iRow )->SetEnabled(bEnable);
	}

	while( iRow < m_numofRows )
	{
		GetField(e_AllProdNameBase + iRow )->SetTitle( "" );
		GetField(e_AllProdNameBase + iRow )->SetValueInt(0);
		GetField(e_AllProdNameBase + iRow )->SetToolTipText("");
		GetField(e_AllProdNameBase + iRow )->SetEnabled(false);
		iRow++;
	}
	
	iRow = 0;
	for( int iI = m_curTopSelRow; iI < m_csSelectedProductsV.size( );  )
	{
		if( iRow >= m_numofSelRows )
			break;

		if( IsFieldNew(e_SelProdNameBase + iRow ) )
		{
			m_csSelectedProductsV.erase(m_csSelectedProductsV.begin()+iI);
			m_BFFromDtV.erase(m_BFFromDtV.begin()+iI);
			m_BFToDtV.erase(m_BFToDtV.begin()+iI);

			if( m_csSelectedProductsV.size( ) < m_curTopSelRow )
				m_curTopSelRow -= m_numofSelRows;

			if( m_curTopSelRow < 0 )
				m_curTopSelRow = 0;

			StartRecalcTimer(0);
			iRow = m_numofSelRows;
			break;;
		}

		if( IsFieldNew(e_DateRangeStart + iRow) )
			m_BFFromDtV[iI] = GetField(e_DateRangeStart + iRow)->GetValueDate();		

		if( IsFieldNew(e_DateRangeEnd + iRow) )
			m_BFToDtV[iI] = GetField(e_DateRangeEnd + iRow)->GetValueDate();		

		CString cstemp = m_csSelectedProductsV[iI] ;
		cstemp.Replace("%", "\\%");
		GetField(e_SelProdNameBase + iRow )->SetValueInt(1);
		GetField(e_SelProdNameBase + iRow)->SetTitle( cstemp );
		GetField(e_SelProdNameBase + iRow )->SetToolTipText( m_csSelectedProductsV[iI] );

		if( GetField(e_OrderType + 1)->GetValueInt() == 1 )
		{
			GetField(e_DateRangeStart + iRow)->SetValueDate( m_BFFromDtV[iI] );
			GetField(e_DateRangeEnd + iRow)->SetValueDate( m_BFToDtV[iI] );

			if( m_BFFromDtV[iI] >= m_BFToDtV[iI] )
			{
				GetField(e_DateRangeStart + iRow)->SetNotManual();
				GetField(e_DateRangeStart + iRow)->SetForeColor(ColorManager::eNegativeChange);
			}
			else
				GetField(e_DateRangeStart + iRow)->SetForeColor(ColorManager::eDefault);

			GetField(e_DateRangeStart + iRow)->SetEnabled(true);
			GetField(e_DateRangeEnd + iRow)->SetEnabled(true);
		}
		else
		{
			GetField(e_DateRangeStart + iRow)->SetForeColor(ColorManager::eDefault);
			
			GetField(e_DateRangeStart + iRow)->SetEnabled(false);
			GetField(e_DateRangeEnd + iRow)->SetEnabled(false);
			
			GetField(e_DateRangeStart + iRow)->SetBlankState();
			GetField(e_DateRangeEnd + iRow)->SetBlankState();
		}

		iI++;
		iRow++;
	}
	
	while( iRow < m_numofSelRows )
	{
		GetField(e_SelProdNameBase + iRow )->SetTitle( "" );
		GetField(e_SelProdNameBase + iRow )->SetValueInt(0);
		GetField(e_SelProdNameBase + iRow )->SetToolTipText("");

		GetField(e_DateRangeStart + iRow)->SetEnabled(false);
		GetField(e_DateRangeEnd + iRow)->SetEnabled(false);

		GetField(e_DateRangeStart + iRow )->SetBlankState();
		GetField(e_DateRangeEnd + iRow )->SetBlankState();
		iRow++;
	}	

	// Consider All product selected...
	if( m_csSelectedProductsV.size( ) == 0 )
	{
		iRow  = 0;
		CString cstemp = "ALL PRODUCT";
		GetField(e_SelProdNameBase + iRow )->SetValueInt(1);
		GetField(e_SelProdNameBase + iRow)->SetTitle( cstemp );
		GetField(e_SelProdNameBase + iRow )->SetToolTipText( cstemp );

		if( IsFieldNew(e_DateRangeStart + iRow) )
			m_BFDateAllPordStart = GetField(e_DateRangeStart + iRow)->GetValueDate();		

		if( IsFieldNew(e_DateRangeEnd + iRow) )
			m_BFDateAllPordEnd = GetField(e_DateRangeEnd + iRow)->GetValueDate();	

		if( GetField(e_OrderType + 1)->GetValueInt() == 1 )
		{
			GetField(e_DateRangeStart + iRow)->SetValueDate( m_BFDateAllPordStart );
			GetField(e_DateRangeEnd + iRow)->SetValueDate( m_BFDateAllPordEnd );

			if( m_BFDateAllPordStart >= m_BFDateAllPordEnd )
			{
					GetField(e_DateRangeStart + iRow)->SetForeColor(ColorManager::eNegativeChange);
					GetField(e_DateRangeStart + iRow)->SetNotManual();
			}
			else
				GetField(e_DateRangeStart + iRow)->SetForeColor(ColorManager::eDefault);

			GetField(e_DateRangeStart + iRow)->SetEnabled(true);
			GetField(e_DateRangeEnd + iRow)->SetEnabled(true);
		}
		else
		{
			GetField(e_DateRangeStart + iRow)->SetForeColor(ColorManager::eDefault);
			
			GetField(e_DateRangeStart + iRow)->SetEnabled(false);
			GetField(e_DateRangeEnd + iRow)->SetEnabled(false);
			
			GetField(e_DateRangeStart + iRow)->SetBlankState();
			GetField(e_DateRangeEnd + iRow)->SetBlankState();
		}
	}
}

void ProductMasterFinApp::fnDisplaySubscription( )
{
	int iRow = 0;
	for( int iI = m_curTopSubRow; iI < m_clSubscriptionStV.size( );  iI++, iRow++)
	{
		if( iRow >= m_numofSubRows )
			break;

		GetField(e_SubFileType + iRow)->SetValueInt(m_clSubscriptionStV[iI].iFileType);
		GetField(e_SubOrderType + iRow)->SetValueInt(m_clSubscriptionStV[iI].iOrderType);
		GetField(e_SubProdName + iRow)->SetValueString( m_clSubscriptionStV[iI].csProduct);
		GetField(e_SubAssetClass + iRow)->SetValueString( m_clSubscriptionStV[iI].csAsstClass);
		GetField(e_SubExchange + iRow)->SetValueString( m_clSubscriptionStV[iI].csExchange);
		GetField(e_SubTypes + iRow)->SetValueString( m_clSubscriptionStV[iI].csFOIS);

		if(m_clSubscriptionStV[iI].iOrderType == 1 )
		{
			CString csDate;
			csDate.Format("%02d/%02d/%d-%02d/%02d/%d", m_clSubscriptionStV[iI].bfFromDt.GetDate(), m_clSubscriptionStV[iI].bfFromDt.GetMonth()+1, m_clSubscriptionStV[iI].bfFromDt.GetYear(), m_clSubscriptionStV[iI].bfToDt.GetDate(), m_clSubscriptionStV[iI].bfToDt.GetMonth()+1, m_clSubscriptionStV[iI].bfToDt.GetYear());
			GetField(e_SubDates + iRow)->SetValueString( csDate );
		}
		else
		{
			if( m_clSubscriptionStV[iI].iSubPeriod == 0 )
				GetField(e_SubDates + iRow)->SetValueString( "1 Month" );
			else
				GetField(e_SubDates + iRow)->SetValueString( "12 Month" );
		}

		if( m_clSubscriptionStV[iI].iFileType == enm_Ref )
			GetField(e_SubContracts + iRow)->SetValueString( m_clSubscriptionStV[iI].csContr );
		else if( m_clSubscriptionStV[iI].iFileType == enm_MD )
			GetField(e_SubContracts + iRow)->SetValueString( m_clSubscriptionStV[iI].csFileFormat);
		else if( m_clSubscriptionStV[iI].iFileType == enm_EOD || m_clSubscriptionStV[iI].iFileType == enm_BLOCK )
			GetField(e_SubContracts + iRow)->SetValueString( m_clSubscriptionStV[iI].csVenue );
		else
			GetField(e_SubContracts + iRow)->SetValueString( "" );
	}

	while( iRow < m_numofSubRows )
	{
		GetField(e_SubFileType + iRow)->SetBlankState();
		GetField(e_SubOrderType + iRow)->SetBlankState();

		GetField(e_SubProdName + iRow)->SetBlankState();
		GetField(e_SubAssetClass + iRow)->SetBlankState();
		GetField(e_SubExchange + iRow)->SetBlankState();
		GetField(e_SubTypes + iRow)->SetBlankState();
		GetField(e_SubDates + iRow)->SetBlankState();
		GetField(e_SubContracts + iRow)->SetBlankState();
		iRow++;
	}
}