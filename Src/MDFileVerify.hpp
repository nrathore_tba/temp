// Copyright (C) 2002 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#pragma once


#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <time.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include <time.h>
#include <map>
#include <string>
#include <fstream>


#ifdef _DEBUG
	#pragma comment(lib,"\\Thebeast\\application\\BFImageSharedD.lib")
	#pragma comment(lib,"\\SharedApps\\BFInterestRateSharedD.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFMarketD.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFSvrFieldsD.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFUtilD.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\persistd.lib")
	#pragma comment(lib,"\\SharedApps\\MailAutomationSharedD.lib")
	#pragma comment(lib,"\\3rdParty\\openssl-0.9.8k_WIN32\\lib\\libeay32.lib")
	#pragma comment(lib,"\\3rdParty\\openssl-0.9.8k_WIN32\\lib\\ssleay32.lib")
#else
	#pragma comment(lib,"\\Thebeast\\application\\BFImageShared.lib")
	#pragma comment(lib,"\\SharedApps\\BFInterestRateShared.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFMarket.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFSvrFields.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFUtil.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\persistr.lib")
	#pragma comment(lib,"\\SharedApps\\MailAutomationShared.lib")
	#pragma comment(lib,"\\3rdParty\\openssl-0.9.8k_WIN32\\lib\\libeay32.lib")
	#pragma comment(lib,"\\3rdParty\\openssl-0.9.8k_WIN32\\lib\\ssleay32.lib")
#endif



// It is recommended that you put your application code in the 
// standard BEAST namespace, that will save you from having to
// use the namespace qualifiers. Macros are used to be able to
// disable namespaces. Use the macros on either side of class
// declarations as shown in this example.
BF_NAMESPACE_BEGIN

// An application image must derive from BaseImage. BaseImage
// has a number of virtual functions that we can override, but
// this example only works on the two pure virtual functions.


typedef std::map<std::string,long> MapThirtyFour;

class MDFileVerify: public BaseImage
{
public:
	enum FieldIDs
	{
		e_btStart = 1,
		e_inputpath = 2,
		e_date = 3,
		e_exchange = 4,
		e_logfile = 5,
		e_abspath = 6,
		e_pgUP		= 92,
		e_pgDN		= 93,
		e_LogMsg	= 4000,
	};

	virtual bool InitData();
	virtual void Recalculate();
	
	void fnDisplayMsg();
	void fnAddMessage( CString csaNewMsg );
	const std::string currentDateTime() 
	{
		time_t     now = time(0);
		struct tm  tstruct;
		char       buf[80];
		tstruct = *localtime(&now);
		strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

		return buf;
	}
	bool fnCheck34(std::string & strLine);
	bool fnCheck35(std::string & strLine);
	std::string GetTagValueString(std::string & strLine,std::string & strTag, int pos, int & returnpos);
	std::string GetTagValueString(std::string & strLine,std::string & strTag);
	long GetTagValueLong(std::string & strLine,std::string & strTag,int pos,int & returnpos);
	long GetTagValueLong(std::string & strLine,std::string & strTag);
	bool fnStartProcess();
	bool exist(const std::string& name);
	void get_all_files_names_within_folder(std::string & folder, std::string & csExch);
	void LogMsg(char *buff);
private:
	int m_numofRows;
	int m_curTopRow;

	
	MapThirtyFour _mapThirtyFour;
	long valthrityfour;
	std::ifstream filein;
	std::vector<CString> mVecFileList;
	CString strLogFileName;
	CString strCurrentProcesingFile;
	unsigned long _lineCount;
	int iTag35;
	int iTag34;
};

BF_NAMESPACE_END
