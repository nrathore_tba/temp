#pragma once

#include "MessageSchema.h"

typedef map<string, FILE*>				FilePtrMap;
typedef FilePtrMap::iterator			FilePtrItr;

typedef vector<ChannelReset>			ChannelResetList;
typedef vector<MarketData>				MarketDataList;
typedef vector<SecurityDefinition>		SecurityDefList;
typedef vector<SecurityStatus>			SecurityStatusList;
typedef vector<QuoteRequest>			QuoteRequestList;

typedef ChannelResetList::iterator		ChannelResetListItr;
typedef MarketDataList::iterator		MarketDataListItr;
typedef SecurityDefList::iterator		SecurityDefListItr;
typedef SecurityStatusList::iterator	SecurityStatusListItr;
typedef QuoteRequestList::iterator		QuoteRequestListItr;

typedef map<int, Contract>				ContractMap;
typedef map<int, string>				ChannelMap;
typedef multimap<int, string>			ChannelToGroupMap;
typedef map<string, ChannelExchange>	ProductExchangeMap;
typedef multimap<string, string>		GroupProductMap;
typedef multimap<string, string>		ProductCFICodeMap;

typedef ContractMap::iterator			ContractMapItr;
typedef ChannelToGroupMap::iterator		ChannelToGroupItr;
typedef ProductCFICodeMap::iterator		ProductCFICodeItr;
typedef GroupProductMap::iterator		GroupProductItr;

struct stFile
{
	string strFilePath;
	string strFileName;
	unsigned long lFileSize;
};

class MessageUtil
{
private:
	static MessageUtil *single;
	MessageUtil() 
	{
		single = 0;
		strLogFile = "";
	}

public:
	static MessageUtil* getInstance();

	~MessageUtil()
	{
	}

	bool GetFilesInDirectory(std::vector<string> &out, const string &directory, string sfiletype);
	bool GetFilesInDirectory(std::vector<stFile> &out, const string &directory, string sfiletype);
	
	// Data Holders
	ContractMap contracts;
	ChannelMap channelMap;
	ProductExchangeMap productExchangeMap;
	GroupProductMap gGroupToProductMap;
	ProductCFICodeMap gProductCFICodeMap;
	ChannelToGroupMap gChannelToGroupMap;
	
	set<string> gGroupToProductSet;
	set<string> gProductCFICodeSet;
	set<string> gChannelToGroupSet;

	// Contracts
	void getContracts(string filename);
	bool updateContracts(Contract &contract);

	// Processed Files
	set<string> _processedFiles;
	string stroutputfolder;
	void LoadProcessedFiles(string strfolder);
	bool IsFileProcessed(string strfile);
	void SetProcessedFile(string strfile);

	// Log Files
	CString strLogFile;
	void LogToFile(string strLog);
};
