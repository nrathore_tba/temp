#include <fstream>
#include <sstream>
#include <stdafx.h> 
#include <shldisp.h>
#include <tlhelp32.h>
#include "zip.h"
#include "EODSubscriptionProcess.hpp"

#define SPLITLINE 20000

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(EodDataProcessingFinapp)

EodDataProcessingFinapp::EodDataProcessingFinapp()
{	
}

EodDataProcessingFinapp::~EodDataProcessingFinapp()
{
}

bool EodDataProcessingFinapp::InitData()
{
	GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));

	m_curTopRow= 0;
	for(m_numofRows = 0; GetField(e_SubscrID + m_numofRows); m_numofRows++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	//---------------------------------------------

	COleDateTime dtToday = COleDateTime::GetCurrentTime();
	BFDate bfToday;
	bfToday.SetYearMonthDate(dtToday.GetYear(),dtToday.GetMonth() - 1, dtToday.GetDay() ); 

	GetField(e_DefaultDate)->SetValueDate(bfToday); 

	m_fldpEODTypeList		= (ListField*) GetField(e_EODTypeList);
	m_fldpFileExtensionList = (ListField*) GetField(e_FileExtList);	
	m_fldpEODEODEList		= (ListField*) GetField(e_EODOREODE);	

	GetField(e_Decompress)->SetVisible(false);
	
	m_bIsThreadRunning = false;
	m_iTotalCount = 0;
	m_iOpenFileCount = 0;
	return true;
}

void ParseTemp(std::string strdata, std::vector<std::string> &tmpV)
{	
	std::stringstream ss(strdata);
	std::string item;

	while(std::getline(ss, item, ','))
	{
		tmpV.push_back(item.c_str());			
	}
}

void EodDataProcessingFinapp::LoadSubscription()
{
	m_clEODSubV.clear(); 
	m_clSubFileDtlMap.clear(); 
	m_clProdFileDtlMap.clear();
	m_clExchFileDtlMap.clear();

	CString csEODOREODE = m_fldpEODEODEList->GetShortString();
	
	CString sql;	
	sql.Format("SELECT CUST_NAME, CONTACT_EMAIL, DEST_ACCOUNT, FTP_PASSWORD, VENUE_CODE, ALL_PRODUCTS, PROD_CODE, EOD_CODE, EXCH_CODE FROM dbo.subscription_2015 where FILE_TYPE = '%s' and Active = 1", csEODOREODE);
	
	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		GetField(e_Message)->SetValueString(sql );
		return;
	}

	int iCount(1);
	while(VARIANT_FALSE == set->adoEOF)
	{
		EODSubscriptionDtl clEODSub;

		clEODSub.iOrderId   = iCount * 1000 ;
		clEODSub.iEODType = GetField( e_EODOREODE )->GetValueInt();

		SETSTR(	clEODSub.stCUST_NAME,		set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
		SETSTR(	clEODSub.stCONTACT_EMAIL,	set->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);	
		SETSTR(	clEODSub.stDEST_ACCOUNT,	set->Fields->Item[_variant_t("DEST_ACCOUNT")]->Value);	
		SETSTR(	clEODSub.stFTP_PASSWORD,	set->Fields->Item[_variant_t("FTP_PASSWORD")]->Value);	
		SETSTR(	clEODSub.stVENUE_CODE,		set->Fields->Item[_variant_t("VENUE_CODE")]->Value);	
		SETSTR(	clEODSub.stALL_PRODUCTS,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);	

		SETSTR(	clEODSub.stPROD_CODE,	set->Fields->Item[_variant_t("PROD_CODE")]->Value);	
		SETSTR(	clEODSub.stEOD_CODE,	set->Fields->Item[_variant_t("EOD_CODE")]->Value);	
		SETSTR(	clEODSub.stEXCH_CODE,	set->Fields->Item[_variant_t("EXCH_CODE")]->Value);

		if( m_fldpEODEODEList->GetValueInt() == 0 && m_clProdFileDtlMap.find( clEODSub.stEOD_CODE.GetString() ) == m_clProdFileDtlMap.end() )
		{
			FileDtl clFileDtl;
			m_clProdFileDtlMap[ clEODSub.stEOD_CODE.GetString() ].strFileName = clEODSub.stEOD_CODE.GetString();
		}

		if( m_fldpEODEODEList->GetValueInt() == 1 && m_clExchFileDtlMap.find( clEODSub.stEXCH_CODE.GetString() ) == m_clExchFileDtlMap.end() )
		{
			m_clExchFileDtlMap[ clEODSub.stEXCH_CODE.GetString() ].strFileName = clEODSub.stEXCH_CODE.GetString();
		}

		m_clEODSubV.push_back( clEODSub );
		iCount++;
		set->MoveNext();
	}

	set->Close();	

	CString csTemp;
	csTemp.Format("%d Subscription loaded", m_clEODSubV.size() );
	GetField(e_Message)->SetValueString(csTemp );
}

void EodDataProcessingFinapp::fnPageUpDown( )
{
	int iTotalRecord = m_clEODSubV.size( );
	
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpBase,e_PgDnBase,iTotalRecord,m_numofRows,m_curTopRow);	
}

void EodDataProcessingFinapp::DisplaySubscription()
{	
	int iRow = 0;
	for(int iIndex = m_curTopRow; iIndex < m_clEODSubV.size(); iIndex++, iRow++)
	{
		if( iRow >= m_numofRows )
			break;

		EODSubscriptionDtl &clEODSub= m_clEODSubV[iIndex];

		GetField(e_SubscrID		+ iRow)->SetValueInt(clEODSub.iOrderId);  
		GetField(e_CUST_NAME	+ iRow)->SetValueString(clEODSub.stCUST_NAME);  

		GetField(e_CONTACT_EMAIL + iRow)->SetValueString(clEODSub.stDEST_ACCOUNT);  
		GetField(e_DEST_ACCOUNT_FTP_PASSWORD + iRow)->SetValueString(clEODSub.stDEST_ACCOUNT);  
		GetField(e_VENUE_CODE	+ iRow)->SetValueString(clEODSub.stVENUE_CODE);
		GetField(e_ALL_PRODUCTS + iRow)->SetValueString(clEODSub.stALL_PRODUCTS);
		GetField(e_PROD_CODE	+ iRow)->SetValueString(clEODSub.stPROD_CODE);
		GetField(e_EOD_CODE		+ iRow)->SetValueString(clEODSub.stEOD_CODE);
		GetField(e_EXCH_CODE	+ iRow)->SetValueString(clEODSub.stEXCH_CODE);
		GetField(e_OutputFileName + iRow)->SetValueString(clEODSub.stOutputFileName);	

		std::string strdestAct = clEODSub.stDEST_ACCOUNT.GetString();

		SubFileDtlMap::iterator itr =  m_clSubFileDtlMap.find( clEODSub.stDEST_ACCOUNT.GetString() );

		if( itr != m_clSubFileDtlMap.end() )
		{
			GetField(e_OutputFileName + iRow)->SetValueString( itr->second.strOutPutFileName.c_str() );
			
			if( itr->second.iUploadReturn == 999 )
			{
				GetField(e_Upload + iRow)->SetEnabled( true );
				GetField(e_OutputFileName + iRow)->SetBackColor(ColorManager::eDefault);
			}
			else
			{
				GetField(e_Upload + iRow)->SetEnabled( false );
				GetField(e_OutputFileName + iRow)->SetBackColor(ColorManager::eSignalPositiveDark);
			}
		}
		else
		{
			GetField(e_OutputFileName + iRow)->SetBackColor(ColorManager::eDefault);
			GetField(e_OutputFileName + iRow)->SetValueString("");
			GetField(e_Upload + iRow)->SetEnabled( false );
		}
	}

	while( iRow < m_numofRows )
	{
		GetField(e_SubscrID		+ iRow)->SetBlankState();  
		GetField(e_CUST_NAME	+ iRow)->SetBlankState();  

		GetField(e_CONTACT_EMAIL + iRow)->SetBlankState();  
		GetField(e_DEST_ACCOUNT_FTP_PASSWORD + iRow)->SetBlankState();  
		GetField(e_VENUE_CODE	+ iRow)->SetBlankState();  
		GetField(e_ALL_PRODUCTS + iRow)->SetBlankState();  
		GetField(e_PROD_CODE	+ iRow)->SetBlankState();  
		GetField(e_EOD_CODE		+ iRow)->SetBlankState();  
		GetField(e_EXCH_CODE	+ iRow)->SetBlankState();  
		GetField(e_OutputFileName + iRow)->SetBlankState();  
		GetField(e_OutputFileName + iRow)->SetBackColor(ColorManager::eDefault);

		GetField(e_Upload + iRow)->SetEnabled( false );
		iRow++;
	}
}

//Check PureCMBKUP pc
//CString __csDestinationPath = "s3://beastftp/"; 
//CString __csDestinationPath = "s3://cmeftp/ftpdata/";
void EodDataProcessingFinapp::UploadSubscription()
{	
	CString __csDestinationPath = ((ListField*)GetField(e_S3Bucket))->GetShortString();
	CString __csAccount = ((ListField*)GetField(e_DestAccount))->GetShortString(); // This is Test Account

	if( IsFieldNew( e_UploadALL ) )
	{
		for(int iIndex = 0; iIndex < m_clEODSubV.size(); iIndex++)
		{
			EODSubscriptionDtl &clEODSub	= m_clEODSubV[iIndex];

			SubFileDtlMap::iterator itr =  m_clSubFileDtlMap.find( clEODSub.stDEST_ACCOUNT.GetString() );

			if( itr != m_clSubFileDtlMap.end() )
			{
				CString csSourcePath = itr->second.strOutPutFileName.c_str();
				CString csDestinationFolder;
				if( GetField(e_DestAccount)->GetValueInt() == 1 )
					csDestinationFolder.Format("%s%s/%s/",__csDestinationPath,  itr->second.strDestAccount.c_str(), m_fldpEODEODEList->GetShortString());
				else
					csDestinationFolder.Format("%s%s/%s/",__csDestinationPath,  __csAccount, m_fldpEODEODEList->GetShortString());

				CString csAWSCommand;
				csAWSCommand.Format(_T("aws s3 cp %s --profile s3user %s"),csSourcePath, csDestinationFolder);					
				itr->second.iUploadReturn = system(csAWSCommand);	

				CString stroutput;
				stroutput.Format("cmd %s @@\n", csAWSCommand );
				PrintRawMessage( stroutput );
			}
		}

		GetField(e_Message)->SetValueString( "Subscription Uploaded" );
	}
	else
	{
		int iRow = 0;
		for(int iIndex = m_curTopRow; iIndex < m_clEODSubV.size(); iIndex++, iRow++)
		{
			if( iRow >= m_numofRows )
				break;

			EODSubscriptionDtl &clEODSub= m_clEODSubV[iIndex];

			if( IsFieldNew( e_Upload + iRow ) )
			{
				SubFileDtlMap::iterator itr =  m_clSubFileDtlMap.find( clEODSub.stDEST_ACCOUNT.GetString() );

				if( itr != m_clSubFileDtlMap.end() )
				{
					CString csSourcePath = itr->second.strOutPutFileName.c_str();
					CString csDestinationFolder;
					if( GetField(e_DestAccount)->GetValueInt() == 1 )
						csDestinationFolder.Format("%s%s/%s/",__csDestinationPath,  itr->second.strDestAccount.c_str(), m_fldpEODEODEList->GetShortString());
					else
						csDestinationFolder.Format("%s%s/%s/",__csDestinationPath,  __csAccount, m_fldpEODEODEList->GetShortString());

					CString csAWSCommand;
					csAWSCommand.Format(_T("aws s3 cp %s --profile s3user %s"),csSourcePath, csDestinationFolder);					
					itr->second.iUploadReturn = system(csAWSCommand);	

					CString stroutput;
					stroutput.Format("cmd %s @@\n", csAWSCommand );
					PrintRawMessage( stroutput );

					GetField(e_Message)->SetValueString( "Subscription Uploaded" );
					break;
				}
			}		
		}
	}	
}

UINT EODProcessor(void *pVoid)
{
	EodDataProcessingFinapp *pParser = (EodDataProcessingFinapp*)pVoid;
	
	pParser->m_iTotalCount = 0;
	pParser->m_bIsThreadRunning = true;
	pParser->m_bIsTrheadCompleted = false;
	
	pParser->Parse();

	pParser->m_bIsThreadRunning = false;
	pParser->m_bIsTrheadCompleted = true;
	pParser->RequestExternalUpdate();

	return 0;
}

void EodDataProcessingFinapp::Parse( )
{
	bool bProcess = true;
	CString csTemp;
	CString sFileName = GetField(e_DirectoryInputPath)->GetValueString();
	if( PathFileExists(sFileName) == FALSE )
	{
		// cheking gz file exist..
		csTemp = "File not exist...";
		bProcess = false;
	}

	sFileName.Replace(".gz",".txt");
	if( PathFileExists(sFileName) == FALSE )
	{
		// cheking gz file exist..
		if( bProcess == false )
		{
			csTemp = "gz File not exist...";
		}
		else
		{
			fnUncompressGZ( );				
			bProcess = true;
		}			
	}

	if( bProcess && m_clEODSubV.size() == 0 )
	{
		bProcess = false;
		csTemp = "Please load subscriptions";				
	}

	CString csPath = GetField(e_DirectoryOutputPath)->GetValueString();	
	csPath.Replace("\\E","");
	csPath.Replace("\\P","");
	csPath.Replace("\\F","");
	if( bProcess && CreateDirectory(csPath, NULL) == 0 )
	{		
		if( GetLastError() == ERROR_PATH_NOT_FOUND )
		{			
			csTemp = "Error OputPath Not Found";	
			bProcess = false;
		}
	}

	csPath = GetField(e_DirectoryOutputPath)->GetValueString();	
	if( bProcess && CreateDirectory(csPath, NULL) == 0 )
	{		
		if( GetLastError() == ERROR_PATH_NOT_FOUND )
		{
			csTemp = "Error OputPath Not Found";	
			bProcess = false;
		}
	}

	if( bProcess )
		ReadEODFileCSV( );
	else
		fnWriteMessage( csTemp );
}

void EodDataProcessingFinapp::Recalculate()
{
	if( m_bIsThreadRunning )
	{		
		GetField(e_Message)->SetValueString( fnReadMessage( ) );	
		return;
	}

	if( m_bIsTrheadCompleted )
	{
		GetField(e_Message)->SetValueString( fnReadMessage( ) );	
		m_bIsTrheadCompleted = false;		
	}

	if( IsFieldNew(e_DefaultBasePath) || IsFieldNew(e_DefaultDate) || IsFieldNew(e_EODTypeList) || IsFieldNew(e_EODOREODE) || IsFieldNew(e_FileExtList) )
	{		
		GenerateFileName();
		GetField(e_Message)->SetValueString( "" );	
	}

	if( IsFieldNew(e_Decompress) )
	{
		fnUncompressGZ( );
	}		

	if( IsFieldNew(e_GetEODSub) )
	{
		LoadSubscription();	
	}	

	if( IsFieldNew(	e_DownloadFile ) )
	{
		fnCopyS3FiletoLocal( );
	}
	
	if( IsFieldNew(e_Submit) && !m_bIsThreadRunning)
	{
		COleDateTime dtCurrent = COleDateTime::GetCurrentTime();
		CString strCurrentTime;
		strCurrentTime.Format("%04d/%02d%02d %02d:%02d:%02d", dtCurrent.GetYear(), dtCurrent.GetMonth(), dtCurrent.GetDay(), 
			dtCurrent.GetHour(), dtCurrent.GetMinute(), dtCurrent.GetSecond());

		//GetField(e_StartTime)->SetValueString(strCurrentTime);
		//GetField(e_EndTime)->SetValueString("");

		AfxBeginThread(EODProcessor, this);
	}

	UploadSubscription();

	fnPageUpDown( );
	DisplaySubscription( );
}

void EodDataProcessingFinapp::fnUncompressGZ(  )
{
	CString csFilePath, csUncompFileName, csCommand;

	csFilePath = GetField( e_DirectoryInputPath )->GetValueString( );
	csUncompFileName = csFilePath;
	csUncompFileName.Replace(".gz",".txt");

	//csCommand.Format("gzip.exe -d -c \\powervault3\CME_s3\daily\endofday\20150805\EOD_20150805_E.gz > \\powervault3\CME_s3\daily\endofday\20150805\EOD_20150805_E.txt");
	csCommand.Format("gzip.exe -d -c %s > %s", csFilePath, csUncompFileName);
		
	int	iValue = system(csCommand);

	if( iValue == 0 )
	{
		//UpdateUncompressflag(1, index);
		GetField(e_Message)->SetValueString("Uncomprssed file successfully.");
	}
	else
	{
		CString csTemp;
		csTemp.Format("error no = %d - path %s, uncompress unsuccessful",iValue,csFilePath);
		GetField(e_Message)->SetValueString(csTemp);
	}
}

void EodDataProcessingFinapp::fnCopyS3FiletoLocal( )
{
	CString strBasePath = GetField(e_DefaultBasePath)->GetValueString();

	BFDate  bfDefaultDate = GetField(e_DefaultDate)->GetValueDate(); 
	
	CString csDate;
	csDate.Format("%d%02d%02d",bfDefaultDate.GetYear(),bfDefaultDate.GetMonth() + 1,bfDefaultDate.GetDate());
	
	CString strEodType			 = m_fldpEODTypeList->GetLongString();
	CString strFileExtensionList = m_fldpFileExtensionList->GetShortString();	

	CString csInputBucket = ((ListField*)GetField(e_S3InputBucket))->GetShortString();
	CString csProfile = ((ListField*)GetField(e_S3InputBucket))->GetLongString();

	CString strFullInputPath;
	strFullInputPath.Format("%s/daily/endofday/%s/EOD_%s_%s.%s",csInputBucket, csDate, csDate, strEodType, strFileExtensionList);

	CString csDestinationFolder;
	csDestinationFolder.Format("%s\\%s\\",strBasePath, csDate);

	//"s3 cp s3://cmegroup-main-datamine-qa-staging/daily/endofday/20150805/EOD_20150805_E.gz  d:\daily\endofday\20150805\";
	CString csAWSCommand;
	csAWSCommand.Format(_T("aws s3 cp --profile %s %s %s"), csProfile, strFullInputPath, csDestinationFolder);					
	int iValue = system(csAWSCommand);	

	if( iValue == 0 )
		GetField(e_Message)->SetValueString("File Downloaded successfully");
	else
		GetField(e_Message)->SetValueString("Error while Downloading File");

	CString stroutput;
	stroutput.Format("cmd %s @@\n", csAWSCommand );
	PrintRawMessage( stroutput );
}

void EodDataProcessingFinapp::GenerateFileName( )
{
	CString strBasePath = GetField(e_DefaultBasePath)->GetValueString();
	BFDate  bfDefaultDate = GetField(e_DefaultDate)->GetValueDate(); 
	
	CString csDate;
	csDate.Format("%d%02d%02d",bfDefaultDate.GetYear(),bfDefaultDate.GetMonth() + 1,bfDefaultDate.GetDate());
	
	CString strEodType			 = m_fldpEODTypeList->GetLongString();
	CString strFileExtensionList = m_fldpFileExtensionList->GetShortString();	

	CString strFullInputPath;
	strFullInputPath.Format("%s\\%s\\EOD_%s_%s.%s",strBasePath,csDate,csDate,strEodType,strFileExtensionList);

	GetField(e_DirectoryInputPath)->SetValueString( strFullInputPath );

	CString strFullOutput;
	strFullOutput.Format("%s\\%s_%s\\%s",strBasePath, csDate, m_fldpEODEODEList->GetShortString(), strEodType);
	GetField(e_DirectoryOutputPath)->SetValueString( strFullOutput );
}

#define FSUCCESS 0
void EodDataProcessingFinapp::ReadEODFileCSV( )
{
	CString csTemp;
	CString csFileNamePath = GetField(e_DirectoryInputPath)->GetValueString();
	csFileNamePath.Replace(".gz", ".txt");
	
	FILE *fileIn(NULL);
	
	if( fopen_s(&fileIn, csFileNamePath, "r") != FSUCCESS )
	{
		csTemp = "Error while Opening fileIn";
		fnWriteMessage( csTemp );
		return;
	}

	char chline[2048];
	std::string strLine("");
	int lRecordRead(0);
		
	csTemp = "Reading input File..";
	fnWriteMessage( csTemp );
	this->RequestExternalUpdate();

	// Read Header...
	while( fgets(chline, 2048, fileIn) )
	{
		if(strchr(chline, '\n'))
		{
			strLine += chline;
			
			//ParseMessage(strLine); 
			strLine = "";
			lRecordRead++;
			break;
		}
		else
		{
			strLine += chline;
		}		
	}

	int iCountTT = 0;
	while( fgets(chline, 2048, fileIn) )
	{
		if(strchr(chline, '\n'))
		{
			strLine += chline;
			
			ParseLine(strLine); 

			strLine = "";
			lRecordRead++;
			iCountTT++;

			if( iCountTT == 1000 )
			{
				csTemp.Format("Reading line %d", lRecordRead);
				fnWriteMessage( csTemp );
				this->RequestExternalUpdate();
				iCountTT = 0;
			}
		}
		else
		{
			strLine += chline;
		}
	}

	fclose(fileIn);

	csTemp = "Processing prod/exch File..";	
	fnWriteMessage( csTemp );
	this->RequestExternalUpdate();

	fnCloseAllOpenFiles( );
	
	// Recreate all DEST_ACCOUNT file splittable at 50000...
	csTemp = "Creating subscription File..";
	fnWriteMessage( csTemp );
	this->RequestExternalUpdate();

	m_clSubFileDtlMap.clear();
	
	for( int iI = 0; iI < m_clEODSubV.size(); iI++ )
	{
		std::string strInputFile;
		if( m_fldpEODEODEList->GetValueInt() == 0 )
		{
			if( m_clProdFileDtlMap[ m_clEODSubV[iI].stEOD_CODE.GetString() ].iRowCount == 0 )
				continue;

			strInputFile = m_clProdFileDtlMap[ m_clEODSubV[iI].stEOD_CODE.GetString() ].strFilePath;
		}
		else
		{
			if( m_clExchFileDtlMap[ m_clEODSubV[iI].stEXCH_CODE.GetString() ].iRowCount == 0 )
				continue;

			strInputFile = m_clExchFileDtlMap[ m_clEODSubV[iI].stEXCH_CODE.GetString() ].strFilePath;
		}

		if( m_clSubFileDtlMap.find( m_clEODSubV[iI].stDEST_ACCOUNT.GetString() ) == m_clSubFileDtlMap.end() )
		{
			SubFileDtl &clSubFileDtl = m_clSubFileDtlMap[ m_clEODSubV[iI].stDEST_ACCOUNT.GetString() ];	
			clSubFileDtl.strDestAccount = m_clEODSubV[iI].stDEST_ACCOUNT.GetString();
			clSubFileDtl.iFileCount = 1;
		}

		FILE *fileIn;
		if( fopen_s(&fileIn, strInputFile.c_str(), "r") != FSUCCESS )
		{
			//CString strError = "Error while Opening fileIn";
			//GetField(e_Message)->SetValueString( strError );		
		}
		else
		{
			bool bContinue = true;
			do
			{
				SubFileDtl &clSubFileDtl = m_clSubFileDtlMap[ m_clEODSubV[iI].stDEST_ACCOUNT.GetString() ];

				if( clSubFileDtl.iRowCount == SPLITLINE )
				{
					fclose(clSubFileDtl.fpOut);

					clSubFileDtl.fpOut = NULL;
					clSubFileDtl.iFileCount++;
					clSubFileDtl.iRowCount = 0;
				}

				if( clSubFileDtl.fpOut == NULL )
				{
					CString csBasePath = GetField(e_DirectoryOutputPath)->GetValueString();
					BFDate  bfDefaultDate = GetField(e_DefaultDate)->GetValueDate(); 
					CString csDate;
					csDate.Format("%d%02d%02d",bfDefaultDate.GetYear(),bfDefaultDate.GetMonth() + 1,bfDefaultDate.GetDate());

					CString csOutputFolder;
					csOutputFolder.Format("%s\\%s",csBasePath, clSubFileDtl.strDestAccount.c_str());

					CreateDirectory( csOutputFolder, NULL);

					clSubFileDtl.strFolderPath = csOutputFolder.GetString();

					CString csFileName;
					csFileName.Format("E%03d_%s.csv", clSubFileDtl.iFileCount, csDate);			

					CString csPath;
					csPath.Format("%s\\%s", csOutputFolder, csFileName);

					//clSubFileDtl.strFilePath = csPath.GetString();*

					if( fopen_s(&clSubFileDtl.fpOut, csPath,"w") )
						return;

					clSubFileDtl.iRowCount = 1;
					fputs( __strHeader.c_str(), clSubFileDtl.fpOut );
				}

				bContinue = CreateSubscriptionFile(clSubFileDtl,  strInputFile, fileIn);
			}
			while( bContinue );
		}

		csTemp.Format("Preparing data for subscription %d",iI);
		fnWriteMessage( csTemp );
		this->RequestExternalUpdate();
	}

	SubFileDtlMap::iterator itr = m_clSubFileDtlMap.begin();

	while( itr != m_clSubFileDtlMap.end() )
	{		
		if( itr->second.fpOut != NULL )
		{
			fclose( itr->second.fpOut );
			itr->second.fpOut = NULL;
		}

		itr++;
	}

	csTemp = "Creating zip File..";	
	fnWriteMessage( csTemp );
	this->RequestExternalUpdate();

	itr = m_clSubFileDtlMap.begin();
	BFDate  bfDefaultDate = GetField(e_DefaultDate)->GetValueDate(); 
	CString csDate;
	csDate.Format("%d%02d%02d",bfDefaultDate.GetYear(),bfDefaultDate.GetMonth() + 1,bfDefaultDate.GetDate());

	while( itr != m_clSubFileDtlMap.end() )
	{
		csTemp.Format("Creating zip %s Account",itr->second.strDestAccount.c_str());
		fnWriteMessage( csTemp );
		this->RequestExternalUpdate();

		if( itr->second.iFileCount > 0 )
		{
			CString csZipName;
			csZipName.Format("%s\\%s_%s_%s.zip", itr->second.strFolderPath.c_str(), m_fldpEODEODEList->GetShortString(), csDate, m_fldpEODTypeList->GetLongString());

			itr->second.strOutPutFileName = csZipName.GetString();

			HZIP hz = CreateZip(csZipName,0);

			CString csFilePath;
			CString csFileName;

			for( int iFile = 0; iFile < itr->second.iFileCount; iFile++ )
			{				
				csFileName.Format("E%03d_%s.csv", iFile + 1, csDate);			 

				csFilePath.Format("%s\\%s", itr->second.strFolderPath.c_str(), csFileName);
				ZipAdd(hz, csFileName, csFilePath);			
			}
			CloseZip(hz);

	//		/*HZIP hz = CreateZip("c:\\simple1.zip",0);
	//		ZipAdd(hz, "E001_simple.csv", "\\\\powervault3\\CME_s3\\daily\\endofday\\20150805_Output\\E\\1000\\E001_20150805.txt");
	//		ZipAdd(hz,"E002_simple.csv", "\\\\powervault3\\CME_s3\\daily\\endofday\\20150805_Output\\E\\1000\\E002_20150805.txt");
	//		CloseZip(hz);*/			
		}
		
		itr++;
	}

	csTemp = "Process Completed..";
	fnWriteMessage( csTemp );
	//this->RequestExternalUpdate();
}

bool EodDataProcessingFinapp::CreateSubscriptionFile(SubFileDtl &clSubFileDtl, std::string &strInputFile, FILE *fileIn )
{
	char chline[2048];
	std::string strLine("");

	while( fgets(chline, 2048, fileIn) )
	{
		if(strchr(chline, '\n'))
		{
			strLine += chline;

			fputs(strLine.c_str(), clSubFileDtl.fpOut);
			
			strLine = "";
			clSubFileDtl.iRowCount++;

			if( clSubFileDtl.iRowCount == SPLITLINE )
				return true;
		}
		else
		{
			strLine += chline;
		}
	}

	fclose(fileIn);
	return false;
}

void EodDataProcessingFinapp::ParseLine( std::string &ssLine )
{
	std::stringstream ss(ssLine);
	std::string item;
	
	EodDetail clEodDetail;
	while(std::getline(ss, item, ','))
	{
		if( item == "null" )
			clEodDetail.strArray.push_back("");
		else
			clEodDetail.strArray.push_back(item.c_str());
	}

	while( clEodDetail.strArray.size() > m_niInputColumnCount )
	{
		clEodDetail.strArray[enm_PRODUCT_DESCRIPTION] = clEodDetail.strArray[enm_PRODUCT_DESCRIPTION]+","+clEodDetail.strArray[enm_FO_IND];
		clEodDetail.strArray.erase(clEodDetail.strArray.begin() + enm_FO_IND);
	}

	fnCheckInSubscription( clEodDetail );	
}


bool EodDataProcessingFinapp::fnCheckInSubscription( EodDetail &eodRowData )
{ 
	if( m_fldpEODEODEList->GetValueInt() == 0 )
	{
		//EOD
		std::string stFileName = eodRowData.strArray[enm_TICKER_SYMBOL].c_str();
		
		if( m_clProdFileDtlMap.find( stFileName ) != m_clProdFileDtlMap.end( ) )
		{
			fnAddRowInSubscription( eodRowData, m_clProdFileDtlMap[stFileName] );
		}
	}
	else
	{
		//EODE
		std::string stFileName = eodRowData.strArray[enm_EXCH_MIC_CODE].c_str();

		if( m_clExchFileDtlMap.find( stFileName ) != m_clExchFileDtlMap.end( ) )
		{
			fnAddRowInSubscription( eodRowData, m_clExchFileDtlMap[stFileName] );
		}
	}

	return false;
}

void EodDataProcessingFinapp::fnCloseAllOpenFiles( )
{
	if( m_fldpEODEODEList->GetValueInt() == 0 )
	{
		FileDtlMap::iterator itr = m_clProdFileDtlMap.begin();

		while( itr != m_clProdFileDtlMap.end() )
		{
			if( itr->second.fpOut != NULL )
			{
				fclose( itr->second.fpOut );
				itr->second.fpOut = NULL;
			}
			itr++;
		}	
	}
	else 
	{
		FileDtlMap::iterator itr = m_clExchFileDtlMap.begin();

		while( itr != m_clExchFileDtlMap.end() )
		{
			if( itr->second.fpOut != NULL )
			{	
				fclose( itr->second.fpOut );
				itr->second.fpOut = NULL;
			}

			itr++;
		}	
	}
}

void EodDataProcessingFinapp::fnAddRowInSubscription( EodDetail &clEodDetail, FileDtl &eodSubDtl )
{
	if( eodSubDtl.fpOut == NULL && eodSubDtl.iRowCount == 0 )
	{
		if( m_iOpenFileCount > 500 )
		{
			fnCloseAllOpenFiles( );
			m_iOpenFileCount = 0;
		}
				
		//First time file is created...
		CString csBasePath = GetField(e_DirectoryOutputPath)->GetValueString();
		
		CString csPath;
		csPath.Format("%s\\%s.csv", csBasePath, eodSubDtl.strFileName.c_str());
		
		eodSubDtl.strFilePath = csPath;

		if( fopen_s(&eodSubDtl.fpOut, csPath,"w") )
			return;

		m_iOpenFileCount++;
		eodSubDtl.iRowCount = 1;
		//fputs( __strHeader.c_str(), eodSubDtl.fpOut );

		std::string strMsg;
		clEodDetail.GetRowMessage(strMsg);
		fputs( strMsg.c_str(), eodSubDtl.fpOut);
	}
	else if( eodSubDtl.fpOut == NULL && eodSubDtl.iRowCount > 0 )
	{
		if( m_iOpenFileCount > 500 )
		{
			fnCloseAllOpenFiles( );
			m_iOpenFileCount = 0;
		}

		//First time file is created...
		CString csBasePath = GetField(e_DirectoryOutputPath)->GetValueString();
		
		CString csPath;
		csPath.Format("%s\\%s.csv", csBasePath, eodSubDtl.strFileName.c_str());
		
		eodSubDtl.strFilePath = csPath;

		if( fopen_s(&eodSubDtl.fpOut, csPath,"a+") )
			return;

		m_iOpenFileCount++;
		//eodSubDtl.iRowCount = 1;
		//fputs( __strHeader.c_str(), eodSubDtl.fpOut );

		std::string strMsg;
		clEodDetail.GetRowMessage(strMsg);
		fputs( strMsg.c_str(), eodSubDtl.fpOut);
	}
	else
	{
		std::string strMsg;
		clEodDetail.GetRowMessage(strMsg);
		fputs( strMsg.c_str(), eodSubDtl.fpOut);
		eodSubDtl.iRowCount++;
	}
}
