// Copyright (C) 2000 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#pragma once


#include <portalhandler/BFInterestRateBaseApp.hpp>
#include <shared/DBConnection.hpp>

#define _ColumnCount 17

BF_NAMESPACE_BEGIN

struct FileDetail
{
	CString csFileName;
	CString csPath;
	CString csOutputPath;
	int iStatus;

	CString csFileType;
	CString csExchange;
	CString csFormat;
	int iYear;
	int iMonth;
	int iDay;
	int iUncomp;
	int iProdCal;
	int iCompGZ;
	unsigned long lFileSize;	
	unsigned long lOffset;

	FileDetail() 
	{
		iStatus = 0;
		iUncomp = 0;
	}
};	

class ProductMasterFinApp : public BFInterestRateBaseApp 
{

public:
	enum Field
	{
		e_Message = 61,
		e_FileType = 200,
		e_Year = 201,
		e_Month = 202,
		
		e_Exchange		= 203,		
		e_FileFormat	= 204,
		e_BaseFilePath	= 205,
		
		e_GetFileList		= 111,

		e_LoadFromDB		= 112,
		e_InsertToDB		= 113,

		e_DisplayFileName	= 1000,
		e_DisplayFileSize	= 1050,

		e_DisplayFileNameDB		= 2000,
		e_DisplayFileSizeDB		= 3000,
		e_DisplayFileStatusDB	= 3050,
		
		e_DisplayFilePathDB		= 3150,

		e_UnZip				= 3100,
		e_UnZipAll			= 3128,
		e_RemoveFile		= 3200,

		e_Prod_Cal		= 3250,
		e_Compr_GZ		= 3300,

		e_PgUpBase	= 90,
		e_PgDnBase	= 91,

		e_PgUpBaseDB	= 92,
		e_PgDnBaseDB	= 93,		
	};

	ProductMasterFinApp();
	virtual ~ProductMasterFinApp();
	

	bool RealInitData();
	void RealRecalculate();
	
	
	int m_numofRows;
	int m_curTopRow;

	int m_numofRowsDB;
	int m_curTopRowDB;
	
	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;
	
	std::vector<FileDetail> m_clFileList;
	std::vector<FileDetail> m_clFileListDB;

	CString m_csFileName;
private:

	bool ProductMasterFinApp::InsertIntoDB( );
	void ProductMasterFinApp::DisplayFileList( );
	void ProductMasterFinApp::fnCreateFilePath( );
	void ProductMasterFinApp::fnPageUpDown( );
	void ProductMasterFinApp::LoadFromDB( bool bReload );

	void ProductMasterFinApp::fnUncompress( int index );
	bool ProductMasterFinApp::fnUncompressAll( );
	void ProductMasterFinApp::fnRemoveFile( int index );

	void ProductMasterFinApp::UpdateUncompressflag(int iUncomp, int iIndex);
	bool ProductMasterFinApp::fnCheckUserAction( );
};

BF_NAMESPACE_END
