
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <sys/stat.h>
#include "afxinet.h"

using namespace std;

BF_NAMESPACE_BEGIN

struct OneTimeOrderDtl
{
	int		iOrderId;

	CString sCustName;
	CString sContactMail;
	CString sFTPUserName;
	CString sFTPPassword;
	CString sAllProducts;
	CString sProductCode;
	CString sExchange;
	CString sBlockCode;
	CString sOutputFileName;
	BFDate	OrderStartDate;
	BFDate	OrderEndDate;
	bool	bOrderProcessed;
	OneTimeOrderDtl()
	{
		iOrderId = 0;
		sCustName = sContactMail = sFTPUserName = sFTPPassword = sAllProducts = sProductCode = sExchange = sBlockCode = sOutputFileName = "";
		bOrderProcessed = false;
	}
};

enum enmOperation
{
	OP_Process,
	OP_Manual,
	OP_ProcessRunning,
	OP_ProcessCompleted,
	OP_None
};

struct ProductDetails
{
	CString sFileName;
	CString sFileType;
	BFDate  FileDate;
	CString sExchange;
	CString sProduct;
	CString sFutOpt;
	CString sSpread;
	CString sBlockCode;
	unsigned long long ulFileSize;
	ProductDetails()
	{
		sFileName = sFileType = sExchange = sProduct = sFutOpt = sSpread = sBlockCode = "";
		ulFileSize  = 0;
	}
};

typedef std::vector<OneTimeOrderDtl> OneTimeOrderDetailV;
typedef vector<ProductDetails>		 vec_ProductDetails;

class CMsgLogger
{
public:
	CMsgLogger()
	{
		m_strLogFIlePath = "D:\\OTO\\Block\\BlockLogFile.txt";
	}
	~CMsgLogger(){}
	void LogMessage(CString sMessage);
	inline bool IsFileExist(const string& strName)
	{
		struct stat buffer;   
		return (stat (strName.c_str(), &buffer) == 0); 
	}
private:
	string m_strLogFIlePath;
};

class CMEBlockProcessing : public BaseImage
{
public:

	enum FieldIDs
	{
		e_sourcePath		= 1,
		e_lstS3Bucket		= 2,
		e_Status			= 3,
		e_FTPUser			= 4,
		//e_awsCommand		= 4,
		e_getOTODetail		= 11,
		//e_UserCalDetai	= 12,
		e_UserUploadData	= 13,
		e_OrderID			= 1000,
		e_CUST_NAME			= 1050,
		e_CONTACT_EMAIL		= 1100,
		e_FTP_USER_NAME		= 1150,
		e_ALL_PRODUCTS		= 1250,
		e_PROD_CODE			= 1300,
		e_ExchangeOTO		= 1400,
		e_LogMessage		= 1450,
		e_BlockCode			= 1350,
		e_OrderStartDt		= 1200,
		e_OrderEndDt		= 1205,

		e_PgDnOTO			= 90,
		e_PgUpOTO			= 91,
		e_PgUpMsg			= 92,
		e_PgDnMsg			= 93,		

		e_Order_Navigate	= 80,	
		e_Msg_Navigate		= 81,
		e_NextProcessing	= 82
	};

	CMEBlockProcessing();
	~CMEBlockProcessing();	
	bool InitData();		
	void Recalculate();
	void LoadOTODetail();
	void fnPageUpDown( );
	void DisplayOTODetail();
	void UploadUserProductCalData(CString sFTPUser);
	const std::string currentDateTime() 
	{
		time_t     now = time(0);
		struct tm  tstruct;
		char       buf[80];
		tstruct = *localtime(&now);
		strftime(buf, sizeof(buf), "%Y%m%d", &tstruct);
		return buf;
	}

	unsigned long  GetFileSize(std::string const &path) 
	{
		WIN32_FIND_DATA data;
		HANDLE h = FindFirstFile(path.c_str(), &data);
		if (h == INVALID_HANDLE_VALUE)
			return -1;

		FindClose(h);

		return (data.nFileSizeLow | data.nFileSizeHigh << 32);
	}

	CCriticalSection	m_MsgCriticalSec;

	void fnWriteMessage(CString sMessage, int nType = 1 )
	{
		CSingleLock clMsgSingleLock(&m_MsgCriticalSec);
		clMsgSingleLock.Lock();
		
		if(nType == 1)
		{
			GetField(e_Status)->SetValueString(sMessage);
		}
		else if(nType == 2)
		{
			m_MessageLogger.LogMessage(sMessage);
		}
		else if(nType == 3)
		{
			AddLogMessages(sMessage);
		}
		clMsgSingleLock.Unlock();
	}

	OneTimeOrderDetailV		m_vecOneTimeOrderDetail;
	enmOperation			m_enmOperation;
	CMsgLogger				m_MessageLogger;
	int						m_iMsgCounter;
	CString					m_csLogMsgArry[200];

	void DisplayLogMessages();
	void AddLogMessages( CString csNewMsg );
	void  fnSendEmail(int iType, int iRowCount);
	CString			m_sMailContent;
private:
	
	vec_ProductDetails		m_vecProductDetails;			

	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;

	ListField *	m_lstS3Bucket;
	BaseField * FTP_UserName;
	BaseField * BlockSourcePath;

	int m_iNumofRowsOTO;
	int m_iCurTopRowOTO;

	int m_iNumofRowsMsg;
	int m_iCurTopRowMsg;

	CString m_csFTPUser;
	CString m_sBlockSrcPath;

	BFDate m_MinStartDate;
	BFDate m_MaxEndtDate;

	COleDateTime m_clNextScheduleDtTime;	

	void GetProductDetails(int iIndex);
	void GetUserProductCalDetail(CString sFTPUser);
	CString CreateBlockDirectoryPath(CString sUserName);
	bool ReadBlockFileData(FILE *fileOut, FILE *fileIn, int *iRowCount);
	void BlockProcessingScheduler( );
	void fnInsertDeliveryReport(CString csPrdouct, CString csDestAccount, CString csFileName, unsigned long &lFileSize, CString &csAwscmd );
	void fnInsertErrorReport(CString csPrdouct, CString csDestAccount, CString csFileName, CString csExchnage, CString &csErrorCmd );
	
};

BF_NAMESPACE_END
