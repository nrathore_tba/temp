#include <fstream>
#include <sstream>
#include <stdafx.h> 
#include <shldisp.h>
#include <tlhelp32.h>
#include "CMEFTPAccounts.hpp"
#include <Fields\ListField.hpp>
#include <wininet.h>
#pragma comment(lib, "wininet")

#define MAX_ROWS 30

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(CMEFTPAccounts)

void Log(char *buff)
{
	FILE* fp=NULL;
	fp=fopen("C:\\FTP.txt","a+");
	fprintf(fp,"%s\n",buff);
	fclose(fp);
}


CMEFTPAccounts::CMEFTPAccounts()
{	
}

CMEFTPAccounts::~CMEFTPAccounts()
{
}

bool CMEFTPAccounts::InitData()
{
	m_iCurrTopRow = 0;
	
    GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));
	
	fld_Type = (ListField*)GetField(e_Type);	
	
	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return false;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();	


	//objstFileData._mapFtpConnect.clear();
	StringMapping* pmap  = ((ListField*)GetField(e_ftpServerList))->GetDataMap();


	for(int i=0;i<pmap->GetCount();i++)
	{
		CString csServer;
		pmap->GetString(i,csServer);
		vecFTPServers.push_back(csServer);
	}


	if( !IsRestoreUpdate( ) )
	{
		BF_Reg_Key_Ptr pKey1(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\CME"));

		int iFTPLocation;
		RegDWORD strServerName(pKey1, "S3FTPLocation",0);
		iFTPLocation = strServerName.get_value();
		GetField(e_ftpServerList)->SetValueInt( iFTPLocation );
	}

	return true;
}

void CMEFTPAccounts::Recalculate()
{		
	bool bUpdated = fnCheckAdminUpdate( );

	fnFTPConnect();

	if( IsFieldNew(e_Type) || IsFieldNew(e_GetData) || bUpdated || IsInitialRecalc() )
	{
		GetFilterData();		
	}
	
	if(IsFieldNew(e_ConnectALL))
	{
		fnConnectALLFTP();
	}

	ManageUpDownButton();
	DisplayGrid();
}


void CMEFTPAccounts::fnConnectALLFTP()
{
	for(int i = 0; i < m_clstFTPAccountsV.size(); i++)		
	{
		stFTPAccounts  &objstFileData = m_clstFTPAccountsV.at(i);
		CString csFTPServer = GetField(e_ftpServerList)->GetDisplayString();

		MapFTpConnect::iterator it = objstFileData._mapFtpConnect.find(csFTPServer);
		if(it!=objstFileData._mapFtpConnect.end())
		{
			HINTERNET hInternet=NULL;
			HINTERNET hFtpSession=NULL;

			hInternet = InternetOpen(NULL,INTERNET_OPEN_TYPE_DIRECT,NULL,NULL,0);
			if(hInternet != NULL)
			{
				hFtpSession = InternetConnect(hInternet, csFTPServer.GetBuffer(), INTERNET_DEFAULT_FTP_PORT, objstFileData.strFTPAcc.GetBuffer(), objstFileData.strFTPPassword.GetBuffer(), INTERNET_SERVICE_FTP, 0, 0);

				if(hFtpSession == NULL)
				{
					it->second = -1;
				}
				else
				{	
					it->second = 1;
					InternetCloseHandle(hFtpSession);
				}
				InternetCloseHandle(hInternet);
			}
		}
	}
}
void CMEFTPAccounts::GetFilterData()
{


	m_clstFTPAccountsV.clear();
	
	CString sql;	
	if( fld_Type->GetValueInt() == 2 )//all
	{
		sql.Format("select * from CME_FTP_Accounts order by FTP_ACCOUNT");
	}
	else 
	{
		sql.Format("select * from CME_FTP_Accounts where IsNull(IS_CHECKED,0) = %d order by FTP_ACCOUNT",fld_Type->GetValueInt());		
	}
	
	stFTPAccounts objstFileData;

	_RecordsetPtr set;	
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql, false)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		PrintRawMessage(sql);
		return;
	}

	while( VARIANT_FALSE == set->adoEOF )
	{
		SETSTR(	objstFileData.strCustName,	set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
		SETSTR(	objstFileData.strFTPAcc,	set->Fields->Item[_variant_t("FTP_ACCOUNT")]->Value);	
		SETSTR(	objstFileData.strFTPPassword,	set->Fields->Item[_variant_t("FTP_PASSWORD")]->Value);	
		SETLONG( objstFileData.iChecked,	set->Fields->Item[_variant_t("IS_CHECKED")]->Value);	
	
		
		objstFileData._mapFtpConnect.clear();
		for(unsigned int i=0;i<vecFTPServers.size();i++)
		{
			objstFileData._mapFtpConnect.insert(std::pair<CString,int>(vecFTPServers[i],0));
		}
		
		m_clstFTPAccountsV.push_back( objstFileData );	

		set->MoveNext();
	}
	set->Close();
	
}
void CMEFTPAccounts::fnFTPConnect()
{
	
	const int niDataRowCount = m_clstFTPAccountsV.size();	 
	int iGridRow = 0;

	for(int iDataRow = m_iCurrTopRow; iDataRow < niDataRowCount; iGridRow++, iDataRow++)		
	{
		if( iGridRow >= MAX_ROWS )
			break;

		if( IsFieldNew(e_ConnectOKCmd + iGridRow) )
		{
			stFTPAccounts  &objstFileData = m_clstFTPAccountsV.at(iDataRow);
			CString csFTPServer = GetField(e_ftpServerList)->GetDisplayString();

			MapFTpConnect::iterator it = objstFileData._mapFtpConnect.find(csFTPServer);
			if(it!=objstFileData._mapFtpConnect.end())
			{
				HINTERNET hInternet=NULL;
				HINTERNET hFtpSession=NULL;

				hInternet = InternetOpen(NULL,INTERNET_OPEN_TYPE_DIRECT,NULL,NULL,0);
				if(hInternet != NULL)
				{
					hFtpSession = InternetConnect(hInternet, csFTPServer.GetBuffer(), INTERNET_DEFAULT_FTP_PORT, objstFileData.strFTPAcc.GetBuffer(), objstFileData.strFTPPassword.GetBuffer(), INTERNET_SERVICE_FTP, 0, 0);

					if(hFtpSession == NULL)
					{
						it->second = -1;
					}
					else
					{	
						it->second = 1;
						InternetCloseHandle(hFtpSession);
					}
					InternetCloseHandle(hInternet);
				}
			}
		}
	}
}
void CMEFTPAccounts::ManageUpDownButton()
{
	const int numTrades = m_clstFTPAccountsV.size();
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUp,e_PgDn,numTrades,MAX_ROWS,m_iCurrTopRow);
}

bool CMEFTPAccounts::fnCheckAdminUpdate( )
{
	const int niDataRowCount = m_clstFTPAccountsV.size();	 
	int iGridRow = 0;

	for(int iDataRow = m_iCurrTopRow; iDataRow < niDataRowCount; iGridRow++, iDataRow++)		
	{
		if( iGridRow >= MAX_ROWS )
			break;

		if( IsFieldNew(e_CheckedOKCmd + iGridRow) )
		{
			stFTPAccounts  &objstFileData = m_clstFTPAccountsV.at(iDataRow);
			CString csCustName = objstFileData.strCustName;
			csCustName.Replace("'","''");

			COleDateTime clOleTime = COleDateTime::GetCurrentTime();
			CString strTime;
			strTime.Format("%d-%02d-%02d %02d:%02d:%02d",clOleTime.GetYear(), clOleTime.GetMonth(), clOleTime.GetDay(), clOleTime.GetHour(), clOleTime.GetMinute(), clOleTime.GetSecond());
			
			CString sql;
			sql.Format("Update CME_FTP_Accounts set IS_CHECKED = 1, Checked_DtTime = '%s' where CUST_NAME = '%s' AND FTP_ACCOUNT = '%s' AND FTP_PASSWORD = '%s'", strTime, csCustName, objstFileData.strFTPAcc, objstFileData.strFTPPassword);

			_RecordsetPtr set;	
			if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql)))
			{
				SetErrorMessage(ErrorSeverity::e_Error, sql);
				PrintRawMessage(sql);
			}

			return true;
		}
	}

	return false;
}

void CMEFTPAccounts::DisplayGrid()
{ 
	bool bEnable = (GetField(e_Admin)->GetValueInt() == 1);

    const int niDataRowCount = m_clstFTPAccountsV.size();	 
	int iGridRow = 0;

	stFTPAccounts objstFileData;

	CString csFTPServer = GetField(e_ftpServerList)->GetDisplayString();

	for(int iDataRow = m_iCurrTopRow; iDataRow < niDataRowCount; iGridRow++, iDataRow++)		
	{
		if( iGridRow >= MAX_ROWS )
			break;

		objstFileData = m_clstFTPAccountsV.at(iDataRow);

		m_va[e_DisplayCustomerName + iGridRow].SetValueString( objstFileData.strCustName );
		m_va[e_DisplayeFTPAcc + iGridRow].SetValueString( objstFileData.strFTPAcc );
		m_va[e_DisplayFTPPassword + iGridRow].SetValueString( objstFileData.strFTPPassword );
		m_va[e_IsChecked + iGridRow].SetValueInt( objstFileData.iChecked );		

		if( objstFileData.iChecked == 0 )
		{
			GetField(e_DisplayCustomerName + iGridRow)->SetForeColor(ColorManager::eNegativeChange);
			GetField(e_CheckedOKCmd + iGridRow)->SetEnabled(bEnable);
		}
		else
		{
			GetField(e_DisplayCustomerName + iGridRow)->SetForeColor(ColorManager::ePositiveChange);
			GetField(e_CheckedOKCmd + iGridRow)->SetEnabled(false);
		}

		MapFTpConnect::iterator it = objstFileData._mapFtpConnect.find(csFTPServer);
		if(it!=objstFileData._mapFtpConnect.end())
		{
			if(it->second == 1)
			{
				GetField(e_ConnectOKCmd + iGridRow)->SetTitle("Connected");
				GetField(e_ConnectOKCmd + iGridRow)->SetTitleBackColor(ColorManager::eFrameGreen);
			}
			else if(it->second == -1)
			{
				GetField(e_ConnectOKCmd + iGridRow)->SetTitle("Not Connected");
				GetField(e_ConnectOKCmd + iGridRow)->SetTitleBackColor(ColorManager::eFrameRed);
			}
			else if(it->second == 0)
			{
				GetField(e_ConnectOKCmd + iGridRow)->SetTitle("Check Connect");
				GetField(e_ConnectOKCmd + iGridRow)->SetTitleBackColor(ColorManager::eButtonBack);
			}
		}
		

	}

	//-------- Balnk all other lines of grid -------------------//
	while(iGridRow < MAX_ROWS)
	{
		GetField(e_DisplayCustomerName + iGridRow)->SetValueString("");
		GetField(e_DisplayeFTPAcc + iGridRow)->SetValueString("");
		GetField(e_DisplayFTPPassword + iGridRow)->SetValueString("");
		
		GetField(e_DisplayCustomerName + iGridRow)->SetBlankState();
		GetField(e_DisplayeFTPAcc + iGridRow)->SetBlankState();
		GetField(e_DisplayFTPPassword + iGridRow)->SetBlankState();
		GetField(e_IsChecked + iGridRow)->SetBlankState();

		GetField(e_CheckedOKCmd + iGridRow)->SetEnabled(false);

		GetField(e_ConnectOKCmd + iGridRow)->SetTitle("Check Connect");
		GetField(e_ConnectOKCmd + iGridRow)->SetTitleBackColor(ColorManager::eButtonBack);

		iGridRow++;
	}

	CString csPageNumber;
	int iCurrPage = m_iCurrTopRow / MAX_ROWS + 1;
	int iTotalPage;	
	if( (niDataRowCount%MAX_ROWS) == 0)
	{
		iTotalPage = (niDataRowCount/MAX_ROWS);	
	}
	else
	{
		iTotalPage = (niDataRowCount/MAX_ROWS) + 1;	
	}

	if(iTotalPage <= 0)
		iTotalPage = 1;

	csPageNumber.Format("%d/%d",iCurrPage,iTotalPage);
	GetField(e_Label_PgNo)->SetTitle(csPageNumber);
}