// Copyright (C) 2000 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#pragma once


#include <portalhandler/BFInterestRateBaseApp.hpp>
#include <shared/DBConnection.hpp>

#define _ColumnCount 16

BF_NAMESPACE_BEGIN
//
//struct ProdDetail
//{
//	CString csPROD_NAME;
//	CString csGROUP_CODE;
//	CString csPROD_CODE;
//	CString csEXCH_CODE;
//	CString csFOI;
//	CString csSPREAD_IND;
//	CString csBBO_CODE;
//	CString csMD_CODE;
//	CString csBLOCK_CODE;
//	CString csEOD_CODE;
//	CString csTICK_CODE;
//	CString csVENUE_CODE;
//	CString csPROD_CATEGORY;
//	CString csORIG_EXCH_CODE;  // This is removed in new version of "|"
//	CString csGLOBEX_TRADED;
//	CString csPIT_TRADED;
//	CString csOTC_TRADED;
//};

CString __csTitles[ _ColumnCount ] = {
		"PROD_NAME",
		"GROUP_CODE",
		"PROD_CODE",
		"EXCH_CODE",
		"FOI",
		"SPREAD_IND",
		"BBO_CODE",
		"MD_CODE",
		"BLOCK_CODE",
		"EOD_CODE",
		"TICK_CODE",
		"VENUE_CODE",
		"PROD_CATEGORY",
		//"ORIG_EXCH_CODE",
		"GLOBEX_TRADED",
		"PIT_TRADED",
		"OTC_TRADED"
};

struct ProdDetail
{
	std::vector<std::string> strArray;
};	

typedef std::vector<ProdDetail> ProdDetailV;

class ProductMasterFinApp : public BFInterestRateBaseApp 
{

public:

	enum enmTitle
	{
		enm_PROD_NAME,
		enm_GROUP_CODE,
		enm_PROD_CODE,
		enm_EXCH_CODE,
		enm_FOI,
		enm_SPREAD_IND,
		enm_BBO_CODE,
		enm_MD_CODE,
		enm_BLOCK_CODE,
		enm_EOD_CODE,
		enm_TICK_CODE,
		enm_VENUE_CODE,
		enm_PROD_CATEGORY,
		//enm_ORIG_EXCH_CODE,
		enm_GLOBEX_TRADED,
		enm_PIT_TRADED,
		enm_OTC_TRADED,
	};
	
	enum Field
	{
		e_BasePath = 5100,
		e_FileDate = 5101,
		e_LoadDataFromFile	= 5102,
		e_SubmitToDB = 5103,
		e_S3InputBucket = 5104,
		e_DownloadFile	= 5105,

		e_ProdNameBase	= 1000,
		e_columnOffset  = 50,

		e_LoadDataFromDB	= 103,

		e_PgUpBase	= 90,
		e_PgDnBase	= 91,

		e_ExchangeFilter = 92,
		e_ProductFilter  = 93,
		e_GroupFilter	 = 96,

		e_MDCodeFilter = 97,
		e_BLOCKCodeFilter = 98,
		e_EODCodeFilter = 99,

		e_ProdNameFilter = 100,

		e_TotalRecord	= 94,
		e_CurTopRecord  = 95,
		
	};

	ProductMasterFinApp();
	virtual ~ProductMasterFinApp();
	

	bool RealInitData();
	void RealRecalculate();
	
	ProdDetailV m_clProdDetailV;
	
	int m_numofRows;
	int m_curTopRow;


	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;

	bool bLoadedFromDB;

private:

	void ProductMasterFinApp::ReadProductFromDB( );
	void ProductMasterFinApp::ReadProductFileCSV( );

	void ProductMasterFinApp::fnPageUpDown( );
	void ProductMasterFinApp::fnDisplayDetails( );

	void ProductMasterFinApp::fnDownloadFile( );

};

BF_NAMESPACE_END
