#pragma once


#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <shared/BaseImage.hpp>


BF_NAMESPACE_BEGIN

// Test comment

struct BBOSubscriptionDtl;

struct TNSFileDtl
{
	TNSFileDtl()
	{
		fp=NULL;
		strOutputFileName="";
		_SplitCount=0;
		_vecFileList.clear();
	}
	CString strOutputFileName;
	long _SplitCount;
	long _fileCount;
	FILE *fp;
	std::vector<CString> _vecFileList;
};

struct stTNSMetaData
{	
	std::string _strExch;
	std::string _strText;
	std::ofstream * _fpOutPutFile;
	std::vector<std::string> _strVecFileNames;
	long _count;
	std::string _strFileName;
	stTNSMetaData()
	{
		_strExch ="";
		_strFileName = "";
		_fpOutPutFile=NULL;
		_count=0;
		_strText="";
		_strVecFileNames.clear();

	}
};

class CTNS
{
	std::string _strExchange;
	std::string _strInputFileName;
	std::string _strInputPath;
	std::string _strOutputFilePath;
	std::string _strExceptionFileName;
	std::string _strIniFile;
	long _interval;
	long _partition;
	BaseImage *_pImage;
	std::ifstream fpInputFile;
	std::map<std::string,stTNSMetaData> _mapTNSMetaData;
	std::vector<std::string>_stVecProducts;
	std::ofstream fpExceptionFile;

	std::map<CString,TNSFileDtl> _mapFileDtl;
public:
	CTNS(void)
	{
	  _interval = 10000;
	  _partition = 10000;
	   _strExchange = _strInputFileName=_strInputPath=_strOutputFilePath=_strExceptionFileName=_strIniFile="";
	}
	~CTNS(void);
	bool dirExists(const std::string& dirName_in);
	std::string createUnzip(std::string sInputFile);
	void setProcessParameters();
	void setAutomaticParameters();
	void updateImageFields(long & lCnt);
	bool OpenExceptionFile();
	bool Process();
	void GenereateMetaData();
	bool ConvertRecord(std::string & strKey,std::string & strExch,std::string & strProd, std::string & strFO,std::string & strLine);
	void CloseOpenFiles();
	bool OpenBBOFile(std::string strFileName);
	void createZip();
	void createLastZip();
	void createSingleZip(TNSFileDtl & objTNSFileDtl);
	std::string formatTime(std::string  trdtime);
	void trim2(std::string & str);
	std::string formatEntryDate(std::string line);
	std::string formatPrice(std::string inPrice, std::string decimalLoc);

	const std::string currentDateTime() 
	{
		time_t     now = time(0);
		struct tm  tstruct;
		char       buf[80];
		tstruct = *localtime(&now);
		strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
		return buf;
	}
	void UpdateOutputField(std::string &stroutput);


	std::vector<std::string> split(const std::string& s, char seperator)
	{
		std::vector<std::string> output;
		std::string::size_type prev_pos = 0, pos = 0;
		while((pos = s.find(seperator, pos)) != std::string::npos)
		{
			std::string substring( s.substr(prev_pos, pos-prev_pos) );
			output.push_back(substring);
			prev_pos = ++pos;
		}
		output.push_back(s.substr(prev_pos, pos-prev_pos)); // Last word
		return output;
	}

	void setPartition(long partition)
	{
		_partition = partition;
	}
	void setInerval(long linterval)
	{
		_interval = linterval;
	}
	void SetImage(BaseImage *pImage)
	{
		_pImage = pImage;
	}
	void UploadAllSubscription();
	void UploadSingleSub(BBOSubscriptionDtl &clBlockSub);
	std::vector<std::string> get_all_files_names_within_folder(std::string & folder, std::string & cstime);

	
	void setExchange(std::string strExch)
	{
		_strExchange = strExch;
	}
	
	void SetOutputTSFilePath(std::string & strFilepath)
	{
		if(strFilepath.substr(strFilepath.length()-1,1) == "\\")
			_strOutputFilePath = strFilepath;
		else
			_strOutputFilePath = strFilepath +"\\";
	}
		
	std::string GetTime()
	{
		time_t     now = time(0);
		struct tm  tstruct;
		char       buf[80];
		localtime_s(&tstruct,&now);
		strftime(buf, sizeof(buf), "%Y%m%d", &tstruct);
		return std::string(buf);
	}

	void FlushOpenFiles()
	{
		std::map<std::string,stTNSMetaData>::iterator it = _mapTNSMetaData.begin();
		for(;it!=_mapTNSMetaData.end();it++)
		{
			it->second._fpOutPutFile->flush();
		}
	}

	void WriteFileProcessCount(std::string strFileName,long lcount)
	{
		char buff[100];
		_ltoa(lcount,buff,10);
		if(0== WritePrivateProfileString(strFileName.c_str(),"Count",buff,_strIniFile.c_str()))
		{
			long dError = GetLastError();
			std::cout<<"error no"<<dError<<std::endl;
		}
	}

	long GetFileProcessCount(std::string strFileName)
	{
		char buff[100];
		GetPrivateProfileString(strFileName.c_str(),"Count","0",buff,100,_strIniFile.c_str());
		return atol(buff);
	}

	void GotoLine(std::ifstream& file, long num)
	{
		std::string s;
		for (long i = 1; i <= num; i++)
			std::getline(file, s);
	}


	int GetOutputFileThreshHold(std::string & strKey, int & rem)
	{
		long lCount;
		std::map<std::string,stTNSMetaData>::iterator it;
		it = _mapTNSMetaData.find(strKey);
		if(it != _mapTNSMetaData.end() && (it->second._count >0))
		{
			lCount = it->second._count;
			rem = lCount/_partition;
			return lCount%_partition;
		}
		return 1;
	}
	
};

BF_NAMESPACE_END