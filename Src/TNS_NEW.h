#pragma once


#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <algorithm>
#include <shared/BaseImage.hpp>

BF_NAMESPACE_BEGIN

// Test

struct stTNSMetaData
{
	stTNSMetaData()
	{
		_fpOutPutFile=NULL;
		_count=0;
		_strText="";
		_strFileName = "";

	}
	std::ofstream * _fpOutPutFile;
	long _count;
	std::string _strText;
	std::string _strFileName;
};

class CTNS
{
	std::string _strExchange;
	std::string _strFUTOPT;
	std::string _strProduct;
	std::string _strInputFileName;
	std::string _strInputPath;
	std::string _strOutputFilePath;
	std::string _strExceptionFileName;
	std::string _strIniFile;
	long _interval;
	long _partition;
	BaseImage *_pImage;
	std::ifstream fpInputFile;
	std::map<std::string,stTNSMetaData> _mapTNSMetaData;
	std::vector<std::string>_stVecProducts;
	std::ofstream fpExceptionFile;
public:
	CTNS(void)
	{
	  _interval = 10000;
	  _partition = 10000;
	   _strExchange = _strFUTOPT=_strProduct=_strInputFileName=_strInputPath=_strOutputFilePath=_strExceptionFileName=_strIniFile="";
	}
	~CTNS(void);
	std::string createUnzip(std::string sInputFile);
	void setProcessParameters();
	void updateImageFields(long & lCnt);
	bool OpenExceptionFile();
	bool Process();
	void GenereateMetaData();
	bool ConvertRecord(std::string & strKey,std::string & strLine);
	void CloseOpenFiles();
	bool OpenBBOFile(std::string strFileName);
	void createZip();
	void creatSingleZip(stTNSMetaData & objstTNSMetaData);
	std::string formatTime(std::string  trdtime);
	void trim2(std::string & str);
	std::string formatEntryDate(std::string line);
	std::string formatPrice(std::string inPrice, std::string decimalLoc);


	std::vector<std::string> split(const std::string& s, char seperator)
	{
		std::vector<std::string> output;
		std::string::size_type prev_pos = 0, pos = 0;
		while((pos = s.find(seperator, pos)) != std::string::npos)
		{
			std::string substring( s.substr(prev_pos, pos-prev_pos) );
			output.push_back(substring);
			prev_pos = ++pos;
		}
		output.push_back(s.substr(prev_pos, pos-prev_pos)); // Last word
		return output;
	}

	void setPartition(long partition)
	{
		_partition = partition;
	}
	void setInerval(long linterval)
	{
		_interval = linterval;
	}
	void SetImage(BaseImage *pImage)
	{
		_pImage = pImage;
	}
	void setProduct(std::string strProd)
	{
		if(strProd == "*")
		{
			_stVecProducts.clear();
			_stVecProducts.push_back("*");
		}
		else
		{
			_stVecProducts.clear();
			_stVecProducts = split(strProd,';');
		}
		_strProduct = strProd;
	}
	
	bool IsProductFound(std::string & strProd)
	{
		if(_stVecProducts.size() == 1  && _stVecProducts[0] == "*")
			return true;
		if(std::find(_stVecProducts.begin(), _stVecProducts.end(), strProd)!=_stVecProducts.end())
			return true;
		else
			return false;
	}

	void setExchange(std::string strExch)
	{
		_strExchange = strExch;
	}
	void SetFUTOPT(std::string strFO)
	{
		_strFUTOPT = strFO;
	}

	void SetOutputTSFilePath(std::string & strFilepath)
	{
		if(strFilepath.substr(strFilepath.length()-1,1) == "\\")
			_strOutputFilePath = strFilepath;
		else
			_strOutputFilePath = strFilepath +"\\";
	}
		
	std::string GetTime()
	{
		time_t     now = time(0);
		struct tm  tstruct;
		char       buf[80];
		localtime_s(&tstruct,&now);
		strftime(buf, sizeof(buf), "%Y%m%d", &tstruct);
		return std::string(buf);
	}

	void FlushOpenFiles()
	{
		std::map<std::string,stTNSMetaData>::iterator it = _mapTNSMetaData.begin();
		for(;it!=_mapTNSMetaData.end();it++)
		{
			it->second._fpOutPutFile->flush();
		}
	}

	void WriteFileProcessCount(std::string strFileName,long lcount)
	{
		char buff[100];
		_ltoa(lcount,buff,10);
		if(0== WritePrivateProfileString(strFileName.c_str(),"Count",buff,_strIniFile.c_str()))
		{
			long dError = GetLastError();
			std::cout<<"error no"<<dError<<std::endl;
		}
		std::map<std::string,stTNSMetaData>::iterator it = _mapTNSMetaData.begin();
		for(;it!=_mapTNSMetaData.end();it++)
		{
			*it->second._fpOutPutFile<<it->second._strText.c_str();
			it->second._strText="";
		}
	}

	long GetFileProcessCount(std::string strFileName)
	{
		char buff[100];
		GetPrivateProfileString(strFileName.c_str(),"Count","0",buff,100,_strIniFile.c_str());
		return atol(buff);
	}

	void GotoLine(std::ifstream& file, long num)
	{
		std::string s;
		for (long i = 1; i <= num; i++)
			std::getline(file, s);
	}


	int GetOutputFileThreshHold(std::string & strKey, int & rem)
	{
		long lCount;
		std::map<std::string,stTNSMetaData>::iterator it;
		it = _mapTNSMetaData.find(strKey);
		if(it != _mapTNSMetaData.end() && (it->second._count >0))
		{
			lCount = it->second._count;
			rem = lCount/_partition;
			return lCount%_partition;
		}
		return 1;
	}
	
	/*void UpdateRecord(std::string & strKey)
	{
		std::map<std::string,long>::iterator it;
		it = _mapTNSMetaData.find(strKey);
		if(it != _mapTNSMetaData.end())
			it->second++;
		else
			_mapTNSMetaData.insert(std::pair<std::string,long>(strKey,0));
	}*/

};

BF_NAMESPACE_END