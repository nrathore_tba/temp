// Copyright (C) 2000 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..


#include <stdafx.h>
#include <fstream>
#include <direct.h>
#include "afxsock.h"
#include <shared/DBConnection.hpp>
#include "CMESecDefViewer.hpp"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BF_NAMESPACE_USE

using namespace std;

IMPLEMENT_IMAGE(SecurityDefViewer)

SecurityDefViewer::SecurityDefViewer()
{
}

SecurityDefViewer::~SecurityDefViewer()
{
}

bool SecurityDefViewer::InitData()
{ 
	m_curTopRow= 0;
	for(m_numofRows = 0; GetField(e_Sec_ID + m_numofRows); m_numofRows++);

	//m_curTopRowDB= 0;
//	for(m_numofRowsDB = 0; GetField(e_DisplayFileNameDB + m_numofRowsDB); m_numofRowsDB++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME_BCP","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	// DB Connection Completed....
		
	return true;
}

void SecurityDefViewer::Recalculate()
{	
	//fnCreateFilePath( );

	//InsertIntoDB( );
	//LoadFromDB( );
		
	if(IsFieldNew(e_LoadSecDef))
	{
		LoadSecDefinition();
	}

	if(IsFieldNew(e_Sec_IDSrc))
	{
		SearchSecDefinition();
	}

	if(IsFieldNew(e_GenWeekSecDef))
	{
		GenerateWeekSecDef();
	}

	if(IsFieldNew(e_GenSecDefYear))
	{
		GenerateYearSecDef();
	}

	fnPageUpDown( );
	DisplaySecDefList( );
}

void SecurityDefViewer::fnPageUpDown( )
{
	int iTotalRecord = CME_SecDefMap.size( );

	if(IsFieldNew(e_PgTopBase))m_curTopRow = 0;

	if(IsFieldNew(e_PgBottombase) && iTotalRecord > 20) m_curTopRow = iTotalRecord - 20;

	
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpBase,e_PgDnBase,iTotalRecord,m_numofRows,m_curTopRow);	

}



void SecurityDefViewer::DisplaySecDefList()
{
	if(CME_SecDefMap.size() == 0)return;


	GetField(e_TotalCount)->SetValueInt(CME_SecDefMap.size());

	
	CString csPageNumber;
	int iCurrPage;
	int iTotalPage;

	if(m_curTopRow != 0)
	{
		iCurrPage =( ( (CME_SecDefMap.size())/m_curTopRow) + 1);
	
	}
	else
	{
		iCurrPage = 1;
	}

	//iTotalPage;	
	if( (m_numofRows%(CME_SecDefMap.size())) == 0)
	{
		iTotalPage = ((CME_SecDefMap.size())/m_numofRows);	
	}
	else
	{
		iTotalPage = ((CME_SecDefMap.size())/m_numofRows) + 1;	
	}

	if(iTotalPage <= 0)
		iTotalPage = 1;

	if(iCurrPage > iTotalPage)iCurrPage = iTotalPage;

	csPageNumber.Format("%d/%d",iCurrPage,iTotalPage);
	GetField(e_PgNumber)->SetTitle(csPageNumber);	

	std::map< long, stSecDef>::iterator SecDefitr = CME_SecDefMap.begin();
	std::advance(SecDefitr,m_curTopRow);
	//SecDefitr += m_curTopRow;

	int iRow(0);
	while( SecDefitr != CME_SecDefMap.end() && iRow < 20)
	{
		GetField( e_Sec_ID			+ iRow )->SetValueInt( SecDefitr->second.iSec_ID );
		GetField( e_Exch_Code		+ iRow )->SetValueString( SecDefitr->second.csExch_Code );
		GetField( e_Prod_Code		+ iRow )->SetValueString( SecDefitr->second.csProd_Code );
		GetField( e_Prod_Desc		+ iRow )->SetValueString( SecDefitr->second.csProd_Desc );
		GetField( e_Grp_Code		+ iRow )->SetValueString( SecDefitr->second.csGrp_Code );
		GetField( e_FOI				+ iRow )->SetValueString( SecDefitr->second.csContractMY );
		GetField( e_SecDefMsg		+ iRow )->SetValueString( SecDefitr->second.csSecDefMsg );
		
		SecDefitr++;
		iRow++;
	}


	
	//for( int iI = m_curTopRow; iI < m_clSecDefinition.size(); iI++, iRow++ )
	//{
	//	if( iRow >= m_numofRows )
	//		break;

	//	
	//	GetField( e_Sec_ID			+ iRow )->SetValueString( m_clSecDefinition[iI].csSec_ID );
	//	GetField( e_Exch_Code		+ iRow )->SetValueString( m_clSecDefinition[iI].csExch_Code );
	//	GetField( e_Prod_Code		+ iRow )->SetValueString( m_clSecDefinition[iI].csProd_Code );
	//	GetField( e_Prod_Desc		+ iRow )->SetValueString( m_clSecDefinition[iI].csProd_Desc );
	//	GetField( e_Grp_Code		+ iRow )->SetValueString( m_clSecDefinition[iI].csGrp_Code );
	//	GetField( e_FOI				+ iRow )->SetValueString( m_clSecDefinition[iI].csFOI );
	//	GetField( e_SecDefMsg		+ iRow )->SetValueString( m_clSecDefinition[iI].csSecDefMsg );
	//	
	//}

	while( iRow < m_numofRows )
	{
		GetField( e_Sec_ID			+ iRow )->SetValueString("");	
		GetField( e_Exch_Code		+ iRow )->SetValueString("");	
		GetField( e_Prod_Code		+ iRow )->SetValueString("");	
		GetField( e_Prod_Desc		+ iRow )->SetValueString("");	
		GetField( e_Grp_Code		+ iRow )->SetValueString("");	
		GetField( e_FOI				+ iRow )->SetValueString("");	
		GetField( e_SecDefMsg		+ iRow )->SetValueString("");	
	}
}

void SecurityDefViewer::ExtractTokensToArray(string s, string Delimiters)
{
   int     Hit;
   int     EarliestHit;
   int     DelimiterIndex;
   string  sExtract;
   string  sSubExtract;
   BOOL    MoreTokens = TRUE;
   BOOL    GotToken = FALSE;
   TCHAR   CurrDelimiter;
   string  strTmp;

	while (MoreTokens)
	{
      GotToken = FALSE;
	  EarliestHit = s.length();

      // Trawl the string looking for the leftmost (earliest) hit in
      // the list of valid separators.
      for (DelimiterIndex = 0; 
           DelimiterIndex < Delimiters.length();
           DelimiterIndex++)
	  {
         CurrDelimiter = Delimiters [DelimiterIndex];
		 Hit = s.find(CurrDelimiter);
         if (Hit != -1)
         {
            if (Hit < EarliestHit)
            {
               EarliestHit = Hit;
            }
            GotToken = TRUE;
         }
      }

		if (GotToken)
		{
			sExtract = s.substr(0,EarliestHit);
			s = s.substr(EarliestHit+1,s.length());

			sSubExtract = sExtract.substr(0,sExtract.find("="));

			int Pos = atoi(sSubExtract.c_str()); 

			switch(Pos)
			{
				case 207:
					{
						//strcpy(sSecurityDef.csExch_Code,sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str());
						sSecurityDef.csExch_Code = sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str();
						break;
					}
				case 1151:
					{
						//strcpy(m_CME_SecDefView.Prod_Code,sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str());
						sSecurityDef.csProd_Code = sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str();
						break;
					}
				case 48:
					{
						//strcpy(m_CME_SecDefView.Sec_ID,sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str());
						sSecurityDef.iSec_ID = atoi(sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str());
						break;
					}
				case 107:
					{
						//strcpy(m_CME_SecDefView.Prod_desc,sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str());
						sSecurityDef.csProd_Desc = sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str();
						break;
					}
				case 55:
					{
						//strcpy(m_CME_SecDefView.Group_Code,sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str());
						sSecurityDef.csGrp_Code = sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str();
						break;
					}
				case 461:
					{
						sSecurityDef.csFOI = sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str();
						break;
					}
				case 200:
					{
						sSecurityDef.csContractMY = sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str();
						break;
					}
				//break;
			}
		}
		else
		{
			MoreTokens = FALSE;
		}

		
	}
}

void SecurityDefViewer::LoadSecDefinition()
{
	CString FileName,csLocalPath,csFileName,csTempDate,csTemp,strcmd;

	csLocalPath = GetField(e_FilePath)->GetValueString();
	
	try
	{
		//int iValue(-1);
		//
		//csTempDate.Format("%04d%02d%02d",atoi(GetField(e_Year)->GetDisplayString()),atoi(GetField(e_Month)->GetDisplayString()),atoi(GetField(e_day)->GetDisplayString()));
		//
		//csTemp.Format("Decompressing File subscription_%s.gz Started.....", csTempDate);
		//
		////strcmd.Format("D: && CD \\CME\\ && lzop.exe -1 -d \\\\powervault3\\CME_s3\\historical\\secdef\\secdef.dat_%s.lzo",csTempDate); 
		////strcmd.Format("%s\\lzop.exe -1 -d %s\\subscription_%s.gz",csLocalPath,csLocalPath,csTempDate);
		//strcmd.Format("gzip -d -c %s\\%s\\subscription_%s.gz > %s\\%s\\subscription_%s",csLocalPath,csTempDate,csTempDate,csLocalPath,csTempDate,csTempDate);

		////iValue = system(strcmd);

		////csFileName.Format("subscription_%s.gz",csTempDate);
		////CString str = unzip(csLocalPath,csFileName);

		////csTemp.Format("Decompressing File subscription_%s.gz Sucessful....", csTempDate);

		////csFileName.Format("%s\\subscription_%s.gz",csLocalPath,csTempDate);
		////unzipFile((LPCSTR)csFileName);
				
	}
	catch(...)
	{
		PrintRawMessage("Exception in System Command of Decompressing .gz");
		return;
	}
	
	csTempDate.Format("%04d%02d%02d",atoi(GetField(e_Year)->GetDisplayString()),atoi(GetField(e_Month)->GetDisplayString()),atoi(GetField(e_day)->GetDisplayString()));
	FileName.Format("%s\\%s\\chn0\\secdef.dat_%s",csLocalPath,GetField(e_Year)->GetDisplayString(),csTempDate); 

	CString stsMsg;
	stsMsg.Format("Total Records in File : subscription_%s",csTempDate);
	GetField(e_statusMsg)->SetTitle(stsMsg);
		
	int m_lLinesRead = 0;
	//CStringArray OutputArraytemp;
		
	//std::ifstream file(FileName);
    //std::string str; 

	CME_SecDefMap.clear();

	FILE *m_pFile;
	FILE *m_pSplitFile[50]; 

	if(fopen_s(&m_pFile, FileName ,"r"))
		return ;

	char chLine[1024];
	std::string strLine("");
	int m_lRecordRead = 0;
	
    while(fgets(chLine, 1024, m_pFile))
	{
		if(strchr(chLine, '\n'))
		{
			
			m_lRecordRead++;
			strLine += chLine;
			
			ExtractTokensToArray(strLine.c_str(),"\001");
			sSecurityDef.csSecDefMsg = strLine.c_str();
			//m_clSecDefinition.push_back(sSecurityDef);
			CME_SecDefMap.insert(std::make_pair(sSecurityDef.iSec_ID,sSecurityDef));
			strLine = "";
		}
		else
		{
			strLine += chLine;
		}
	}
	fclose(m_pFile);


}

void SecurityDefViewer::SearchSecDefinition()
{
	long SecId = GetField(e_SecIdsearch)->GetValueInt();

	std::map< long, stSecDef>::iterator SecDefitr = CME_SecDefMap.find(SecId);

	
	if(SecDefitr != CME_SecDefMap.end())
	{
		GetField( e_Sec_IDSrc	)->SetValueInt( SecDefitr->second.iSec_ID );
		GetField( e_Exch_CodeSrc)->SetValueString( SecDefitr->second.csExch_Code );
		GetField( e_Prod_CodeSrc)->SetValueString( SecDefitr->second.csProd_Code );
		GetField( e_Prod_DescSrc)->SetValueString( SecDefitr->second.csProd_Desc );
		GetField( e_Grp_CodeSrc	)->SetValueString( SecDefitr->second.csGrp_Code );
		GetField( e_FOIsrc		)->SetValueString( SecDefitr->second.csContractMY );
		GetField( e_SecDefMsgSrc)->SetValueString( SecDefitr->second.csSecDefMsg );
	}


	//std::map< int, BondCustomer>::iterator custitr = m_clCustListDetail.find( itr->second.TraderId);
}



void SecurityDefViewer::GenerateWeekSecDef()
{
	CString FileName,csLocalPath,csFileName,csTempDate,csTemp,strcmd;
	CString stsMsg;
	csLocalPath = GetField(e_FilePath)->GetValueString();
	
	
	COleDateTime SelDate = COleDateTime((atoi(GetField(e_WeekYear)->GetDisplayString())),(atoi(GetField(e_WeekMonth)->GetDisplayString())),(atoi(GetField(e_weekDay)->GetDisplayString())),0,0,0);
	//BFDate SelDate = BFDate((atoi(GetField(e_Year)->GetDisplayString())),(atoi(GetField(e_Month)->GetDisplayString())),(atoi(GetField(e_day)->GetDisplayString())));
	csTempDate.Format("%04d%02d%02d",atoi(GetField(e_WeekYear)->GetDisplayString()),atoi(GetField(e_WeekMonth)->GetDisplayString()),atoi(GetField(e_weekDay)->GetDisplayString()));

	int DayWeek = SelDate.GetDayOfWeek();

	if(DayWeek != 1)
	{
		stsMsg.Format("Selected date is not Sunday %s",csTempDate);
		GetField(e_statusMsg)->SetTitle(stsMsg);
		return;
	}
	
	int WeekDay;
	int LineCount = 0;
	CString LineBufferRead = "";
	CME_SecDefMap.clear();

	FILE *m_pFileRead;
	FILE *m_pFileWrite;
	//FILE *m_pSplitFile[50]; 
	CString FileNameWrite;
	FileNameWrite.Format("%s\\%s\\chn0\\secdef_w.dat_%s",csLocalPath,GetField(e_WeekYear)->GetDisplayString(),csTempDate); 

	if(fopen_s(&m_pFileWrite, FileNameWrite ,"w"))
	{
		stsMsg.Format("File Not Created : secdef_w.dat_%s",csTempDate);
		GetField(e_statusMsg)->SetTitle(stsMsg);
		return ;
	}

	BFDate bfProcessDt = SelDate.m_dt;

	for(WeekDay = 0; WeekDay < 7;WeekDay++)
	{
	
			//bfProcessDt.OffsetDate(1);
			//csTempDate.Format("%04d%02d%02d",atoi(GetField(e_WeekYear)->GetDisplayString()),atoi(GetField(e_WeekMonth)->GetDisplayString()),(atoi(GetField(e_weekDay)->GetDisplayString())+ WeekDay));
			csTempDate.Format("%04d%02d%02d",bfProcessDt.GetYear(),(bfProcessDt.GetMonth()+1),bfProcessDt.GetDate());
			FileName.Format("%s\\%04d\\chn0\\secdef.dat_%s",csLocalPath,bfProcessDt.GetYear(),csTempDate); 

	
			try
			{
				int iValue(-1);
							
				//csTempDate.Format("%04d%02d%02d",atoi(GetField(e_Year)->GetDisplayString()),atoi(GetField(e_Month)->GetDisplayString()),atoi(GetField(e_day)->GetDisplayString()));
				csTempDate.Format("%04d%02d%02d",bfProcessDt.GetYear(),(bfProcessDt.GetMonth()+1),bfProcessDt.GetDate());

				csTemp.Format("Decompressing File SecDef file Started.....", csTempDate);
							
				strcmd.Format("D:\\lzop.exe -1 -d %s.lzo",FileName);
							
				iValue = system(strcmd);

											
			}
			catch(...)
			{
				PrintRawMessage("Exception in System Command of Decompressing .gz");
				return;
			}
			
			
			
			stsMsg.Format("Total Records in File : secdef.dat_%s",csTempDate);
			GetField(e_statusMsg)->SetTitle(stsMsg);
		
			int m_lLinesRead = 0;
			//CStringArray OutputArraytemp;
		
			//std::ifstream file(FileName);
			//std::string str; 

			if(fopen_s(&m_pFileRead, FileName ,"r"))
			{
				stsMsg.Format("File Not Found : secdef.dat_%s",csTempDate);
				GetField(e_statusMsg)->SetTitle(stsMsg);
				bfProcessDt.OffsetDate(1);
				continue ;
			}

			char chLine[1024];
			std::string strLine("");
			int m_lRecordRead = 0;
	
			while(fgets(chLine, 1024, m_pFileRead))
			{
				if(strchr(chLine, '\n'))
				{
			
					m_lRecordRead++;
					strLine += chLine;
			
					ExtractTokensToArray(strLine.c_str(),"\001");
					sSecurityDef.csSecDefMsg = strLine.c_str();
					//m_clSecDefinition.push_back(sSecurityDef);
					std::map< long, stSecDef>::iterator SecDefitr = CME_SecDefMap.find(sSecurityDef.iSec_ID);

					if(SecDefitr == CME_SecDefMap.end())
					{
						CME_SecDefMap.insert(std::make_pair(sSecurityDef.iSec_ID,sSecurityDef));

						LineBufferRead += sSecurityDef.csSecDefMsg;
						LineCount++;
					}

					if(LineCount == 100)
					{
						fputs(LineBufferRead,m_pFileWrite);
						LineBufferRead = "";
						LineCount = 0;
					}
					
					strLine = "";
				}
				else
				{
					strLine += chLine;
				}
			}
			fclose(m_pFileRead);
			DeleteFile(FileName);
			bfProcessDt.OffsetDate(1);
	}

	fclose(m_pFileWrite);
	stsMsg.Format("Successfully Created : secdef_w.dat_%s",csTempDate);
	GetField(e_statusMsg)->SetTitle(stsMsg);

}


void SecurityDefViewer::GenerateYearSecDef()
{
	CString FileName,csLocalPath,csFileName,csTempDate,csTemp,strcmd;
	CString stsMsg;
	csLocalPath = GetField(e_FilePath)->GetValueString();
	int Year,Month,Day;

	Year = atoi(GetField(e_SecDefYear)->GetDisplayString());

		
		COleDateTime SelDate = COleDateTime(Year,1,1,0,0,0);
		//BFDate SelDate = BFDate((atoi(GetField(e_Year)->GetDisplayString())),(atoi(GetField(e_Month)->GetDisplayString())),(atoi(GetField(e_day)->GetDisplayString())));
		csTempDate.Format("%04d%02d%02d",Year,1,1);

		int DayWeek = SelDate.GetDayOfWeek();

		/*if(DayWeek != 1)
		{
			stsMsg.Format("Selected date is not Sunday %s",csTempDate);
			GetField(e_statusMsg)->SetTitle(stsMsg);
			return;
		}*/
	
		int WeekDay;
		int LineCount = 0;
		CString LineBufferRead = "";
		CME_SecDefMap.clear();

		FILE *m_pFileRead;
		FILE *m_pFileWrite;
		//FILE *m_pSplitFile[50]; 
		CString FileNameWrite;
		/*FileNameWrite.Format("%s\\%04d\\chn0\\secdef_w.dat_%s",csLocalPath,Year,csTempDate); 

		if(fopen_s(&m_pFileWrite, FileNameWrite ,"w"))
		{
			stsMsg.Format("File Not Created : secdef_w.dat_%s",csTempDate);
			GetField(e_statusMsg)->SetTitle(stsMsg);
			return ;
		}*/

		BFDate bfProcessDt = SelDate.m_dt;
		BFDate bfProcessEndDt = BFDate(Year,11,31);

		while(bfProcessDt <= bfProcessEndDt)
		{
				csTempDate.Format("%04d%02d%02d",bfProcessDt.GetYear(),(bfProcessDt.GetMonth()+1),bfProcessDt.GetDate());
				FileNameWrite.Format("%s\\%04d\\chn0\\secdef_w.dat_%s",csLocalPath,bfProcessDt.GetYear(),csTempDate); 

				if(fopen_s(&m_pFileWrite, FileNameWrite ,"w"))
				{
					stsMsg.Format("File Not Created : secdef_w.dat_%s",csTempDate);
					GetField(e_statusMsg)->SetTitle(stsMsg);
					break;
				}
			
				CME_SecDefMap.clear();
				for(WeekDay = DayWeek; WeekDay <= 7;WeekDay++)
				{
	
						//bfProcessDt.OffsetDate(1);
						//csTempDate.Format("%04d%02d%02d",atoi(GetField(e_WeekYear)->GetDisplayString()),atoi(GetField(e_WeekMonth)->GetDisplayString()),(atoi(GetField(e_weekDay)->GetDisplayString())+ WeekDay));
						csTempDate.Format("%04d%02d%02d",bfProcessDt.GetYear(),(bfProcessDt.GetMonth()+1),bfProcessDt.GetDate());
						FileName.Format("%s\\%04d\\chn0\\secdef.dat_%s",csLocalPath,bfProcessDt.GetYear(),csTempDate); 

	
						
						try
						{
							int iValue(-1);
							
							//csTempDate.Format("%04d%02d%02d",atoi(GetField(e_Year)->GetDisplayString()),atoi(GetField(e_Month)->GetDisplayString()),atoi(GetField(e_day)->GetDisplayString()));
							csTempDate.Format("%04d%02d%02d",bfProcessDt.GetYear(),(bfProcessDt.GetMonth()+1),bfProcessDt.GetDate());

							csTemp.Format("Decompressing File SecDef file Started.....", csTempDate);
							
							strcmd.Format("D:\\lzop.exe -1 -d %s.lzo",FileName);
							
							iValue = system(strcmd);

											
						}
						catch(...)
						{
							PrintRawMessage("Exception in System Command of Decompressing .gz");
							return;
						}
						
						
						stsMsg.Format("Total Records in File : secdef.dat_%s",csTempDate);
						GetField(e_statusMsg)->SetTitle(stsMsg);
		
						int m_lLinesRead = 0;
						//CStringArray OutputArraytemp;
		
						//std::ifstream file(FileName);
						//std::string str; 

						if(fopen_s(&m_pFileRead, FileName ,"r"))
						{
							stsMsg.Format("File Not Found : secdef.dat_%s",csTempDate);
							GetField(e_statusMsg)->SetTitle(stsMsg);
							bfProcessDt.OffsetDate(1);
							continue ;
						}

						char chLine[1024];
						std::string strLine("");
						int m_lRecordRead = 0;
	
						while(fgets(chLine, 1024, m_pFileRead))
						{
							if(strchr(chLine, '\n'))
							{
			
								m_lRecordRead++;
								strLine += chLine;
			
								ExtractTokensToArray(strLine.c_str(),"\001");
								sSecurityDef.csSecDefMsg = strLine.c_str();
								//m_clSecDefinition.push_back(sSecurityDef);
								std::map< long, stSecDef>::iterator SecDefitr = CME_SecDefMap.find(sSecurityDef.iSec_ID);

								if(SecDefitr == CME_SecDefMap.end())
								{
									CME_SecDefMap.insert(std::make_pair(sSecurityDef.iSec_ID,sSecurityDef));

									LineBufferRead += sSecurityDef.csSecDefMsg;
									LineCount++;
								}

								if(LineCount == 100)
								{
									fputs(LineBufferRead,m_pFileWrite);
									LineBufferRead = "";
									LineCount = 0;
								}
					
								strLine = "";
							}
							else
							{
								strLine += chLine;
							}
						}
						fclose(m_pFileRead);
						DeleteFile(FileName);
						bfProcessDt.OffsetDate(1);
				}
				
				fclose(m_pFileWrite);
				DayWeek = 1;
		}


		stsMsg.Format("Successfully Created Weekly SecDef for Year : %04d",Year);
		GetField(e_statusMsg)->SetTitle(stsMsg);
	
}