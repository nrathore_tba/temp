#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <vector>

struct stFileData
{
	stFileData()
	{
		strDesc="";
		strDetail="";
	}
	CString strDesc;
	CString strDetail;
};

BF_NAMESPACE_BEGIN
class CMEInvoiceMasterFinapp : public BaseImage
{
public:

	enum FieldIDs
	{
		e_Status		=	108,
    	e_DisplayDesc	=	20004,
		e_DisplayDetail	=	20060,
		
		e_GenerateInvoice =	20103,
		e_InvoiceFileField = 315,
	
		e_PgUp	    	=	20101,
		e_PgDn		    =	20102,
		e_PgLabel       =	65,

	};
	

	DBConnection	m_clDBConn;
	DBInterfaceNew *m_clpdbInt;				
	
	CMEInvoiceMasterFinapp();
	~CMEInvoiceMasterFinapp();	
	
	bool InitData();	
	void Recalculate();	

	void GetData();	
	void GenerateInvoice();	

	std::vector<stFileData> m_FileData;
};

BF_NAMESPACE_END


