
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <shared/MDDDataHandler.hpp>

BF_NAMESPACE_BEGIN
using namespace std;

const string __strHeader = "Trade Date,Exchange Code,Asset Class,Product Code,Clearing Code,Product Description,Product Type,Underlying Product Code,Put/Call,Strike Price,Contract Year,Contract Month,Contract Day,Settlement,Settlement Cabinet Indicator,Open Interest,Total Volume,Globex Volume,Floor Volume,PNT Volume,Block Volume,EFP Volume,EOO Volume,EFR Volume,EFS Volume,EFB Volume,EFM Volume,SUB Volume,OPNT Volume,TAS Volume,TAS Block Volume ,TAM Singapore Volume ,TAM Singapore Block Volume ,TAM London Volume ,TAM London Block Volume ,Globex Open Price,Globex Open Price Bid/Ask Indicator,Globex Open Price Cabinet Indicator,Globex High Price,Globex High Price Bid/Ask Indicator,Globex High Price Cabinet Indicator,Globex Low Price,Globex Low Price Bid/Ask Indicator,Globex Low Price Cabinet Indicator,Globex Close Price,Globex Close Price Bid/Ask Indicator,Globex Close Price Cabinet Indicator,Floor Open Price,Floor Open Price Bid/Ask Indicator,Floor Open Price Cabinet Indicator,Floor Open Second Price,Floor Open Second Price Bid/Ask Indicator,Floor High Price,Floor High Price Bid/Ask Indicator,Floor High Price Cabinet Indicator,Floor Low Price,Floor Low Price Bid/Ask Indicator,Floor Low Price Cabinet Indicator,Floor Close Price,Floor Close Price High Bid/Ask Indicator,Floor Close Price Cabinet Indicator,Floor Close Second Price,Floor Close Second Price Bid/Ask Indicator,Floor Post-Close Price,Floor Post-Close Price Bid/Ask Indicator,Floor Post-Close Second Price,Floor Post-Close Second Price Bid/Ask Indicator,Delta,Implied Volatility,Last Trade Date\n";

// 72 columns..
enum ColumnName
{
	enm_TRADE_DATE,
	enm_LAST_TRADE_DATE, // this column is last column in output..
	enm_EXCH_MIC_CODE,
	enm_QUADRANT_ASSET,
	enm_TICKER_SYMBOL,
	enm_COMMODITY_CODE,
	enm_PRODUCT_DESCRIPTION,
	enm_FO_IND,
	enm_UNDERLYING_CONTRACT,
	enm_PC_IND,
	enm_STRIKE_PRICE,
	enm_CONTRACT_YR,
	enm_CONTRACT_MO,
	enm_CONTRACT_DAY,
	enm_SETL_PRICE,
	enm_SETL_CAB_IND,
	enm_OPEN_INTEREST,
	enm_TOTAL_VOL,
	enm_GLOBEX_VOL,
	enm_EXPIT_VOL,
	enm_PIT_VOL,
	enm_BLOCK_VOL,
	enm_EFP_VOL,
	enm_EOO_VOL,
	enm_EFR_VOL,
	enm_EFS_VOL,
	enm_EFB_VOL,
	enm_EFM_VOL,
	enm_SUB_VOL,
	enm_PNT_VOL,
	enm_TAS_VOL,
	enm_BLOCK_TAS_VOL,
	enm_TAMS_VOL,
	enm_TAMS_BLOCK_VOL,
	enm_TAML_VOL,
	enm_TAML_BLOCK_VOL,
	enm_ETH_OPEN1_PRICE,
	enm_ETH_OPEN1_AB_IND,
	enm_ETH_OPEN1_CAB_IND,
	enm_ETH_HI_PRICE,
	enm_ETH_HI_PRICE_AB_IND,
	enm_ETH_HI_PRICE_CAB_IND,
	enm_ETH_LO_PRICE,
	enm_ETH_LO_PRICE_AB_IND,
	enm_ETH_LO_PRICE_CAB_IND,
	enm_ETH_CLOSE1_PRICE,
	enm_ETH_CLOSE1_AB_IND,
	enm_ETH_CLOSE1_CAB_IND,
	enm_OPEN1_PRICE,
	enm_OPEN1_AB_IND,
	enm_OPEN1_CAB_IND,
	enm_OPEN2_PRICE,
	enm_OPEN2_AB_IND,
	enm_HI_PRICE,
	enm_HI_PRICE_AB_IND,
	enm_HI_PRICE_CAB_IND,
	enm_LO_PRICE,
	enm_LO_PRICE_AB_IND,
	enm_LO_PRICE_CAB_IND,
	enm_CLOSE1_PRICE,
	enm_CLOSE1_AB_IND,
	enm_CLOSE1_CAB_IND,
	enm_CLOSE2_PRICE,
	enm_CLOSE2_AB_IND,
	enm_PCLS_HI_PRICE,
	enm_PCLS_HI_AB_IND,
	enm_PCLS_IND, // This column is not present in output
	enm_PCLS_LO_PRICE,
	enm_PCLS_LO_AB_IND,
	enm_DELTA,
	enm_VOLATILITY,
	enm_END	
};

struct ScheduleInfo
{
	int		iRetInterval;
	int		iRetryCount;
	bool	bValid;
	int		iStatus;
	CString csSubType;	
	COleDateTime clScheduleDtTime;	
	COleDateTime clMainScheduleDtTime;
	CString csInDate;
	CString csOutDate;

	CString  csFilePath;
	CString	 csEOD_FilePath;
	CString  csEODE_FilePath;
};

enum enmOperation
{
	OP_Download,
	OP_Uncompress,
	OP_Process,
	OP_ReadThread,
	OP_ProcessCompleted,
	OP_None
};

struct EodDetail
{
	std::vector<std::string> strArray;

	void GetRowMessage(std::string &msg )
	{
		msg = strArray[enm_TRADE_DATE];
		int iI = 1;
		for( ; iI < enm_EXPIT_VOL; iI++ )
		{
			if( iI == enm_LAST_TRADE_DATE || iI == enm_PCLS_IND )
				continue;

			msg += "," + strArray[iI];
		}

		msg += "," + strArray[enm_PIT_VOL];
		iI++;

		msg += "," + strArray[enm_EXPIT_VOL];
		iI++;

		for( ; iI < strArray.size(); iI++ )
		{
			if( iI == enm_LAST_TRADE_DATE || iI == enm_PCLS_IND )
				continue;

			msg += "," + strArray[iI];
		}

		msg += "," + strArray[enm_LAST_TRADE_DATE] + "\n";
	}
};	

struct EODSubscriptionDtl
{
	EODSubscriptionDtl()
	{		
		//stCUST_NAME = "";
		//stCONTACT_EMAIL = "";
		stDEST_ACCOUNT = "";
		stFTP_PASSWORD = "";
		//stVENUE_CODE = "";
		//stALL_PRODUCTS = "";
		//stPROD_CODE = "";
		stEOD_CODE = "";
		stEXCH_CODE = "";
		iDelPref = 0;// 0. FTP, 1 . S3
	}	
	
	//CString stCUST_NAME;
	//CString stCONTACT_EMAIL;
	CString stDEST_ACCOUNT;
	CString stFTP_PASSWORD;
	//CString stVENUE_CODE;
	//CString stALL_PRODUCTS;
	//CString stPROD_CODE;
	CString stEOD_CODE;
	CString stEXCH_CODE;
	CString stEXCH_PROD_CODE;

	int iDelPref;
};

typedef std::multimap<std::string, EODSubscriptionDtl> EODSubDtlMap;

struct EODSubscriptionFile
{
	EODSubscriptionFile()
	{
		bUploaded = false;
		fpOut = NULL;
		iRowCount = 0;
		iFileCount = 0;
	}
	
	FILE *fpOut;
	bool bUploaded;
	int iRowCount;
	int iFileCount;
	int iUploadReturn;
	CString strOutputFolderPath;	
	CString stDestAccount;
	CString strOutPutFileName;
};

typedef std::map<std::string, EODSubscriptionFile> EODSubFileMap;

struct FileDtl
{
	std::string strFileName;
	std::string strFilePath;
	FILE *fpOut;
	int iRowCount;

	FileDtl( )
	{
		strFileName = "";
		strFilePath = "";
		fpOut = NULL;
		iRowCount = 0;
	}
};

typedef std::map<std::string, FileDtl> FileDtlMap;
//typedef std::map<std::string, bool> DistItemMap;

class ExecutionEOD : public BaseImage
{
public:
	
	enum FieldIDs
	{
		e_NextDownloadMsg = 50,	
		e_MsgTitle		  = 65,
		e_OutputPath	  = 62,
		e_SingleAccount   = 63,

		e_ReloadSchedule  = 100,
		e_DateFilter	  = 95,
		e_ManualRun		  = 96,
		e_Process		  = 97,
		e_RtryInterval	  = 98,
		//e_FilterProdExch  = 99,

		e_SubType	= 212,
		e_Year		= 201,
		e_Month		= 202,
		e_Day		= 215,
		e_Hour		= 213,
		e_Minute	= 214,

		e_S3CMEBucket = 60,
		e_DestAccount = 58,
		e_S3FTPBucket = 59,

		e_LogMsg	= 4000,
		e_pgUP		= 92,
		e_pgDN		= 93,
	};
	
	ExecutionEOD();
	~ExecutionEOD();	

	bool InitData();		
	void Recalculate();	
private:

	void ExecutionEOD::fnAddMessage( CString csaNewMsg );
	void ExecutionEOD::fnDisplayMsg();
	
	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;
	
	int m_numofRows;
	int m_curTopRow;

	static const int m_niInputColumnCount	= 71;
	static const int m_niOutputColumnCount	= 70;

	BaseField *fld_OutputPath;

public:
	bool m_bIsTrheadCompleted;
	bool m_bIsThreadRunning;
	
	CString m_csMsg;
	int m_iTotalCount;
	int m_iOpenFileCount;
	
	CCriticalSection	m_MsgCriticalSec;

	void fnWriteMessage(CString csString )
	{
		CSingleLock clMsgSingleLock(&m_MsgCriticalSec);

		clMsgSingleLock.Lock();

		m_csMsg = csString.GetString();

		clMsgSingleLock.Unlock();
	}

	CString fnReadMessage( )
	{
		CSingleLock clMsgSingleLock(&m_MsgCriticalSec);

		clMsgSingleLock.Lock();

		CString csStr = m_csMsg.GetString();

		clMsgSingleLock.Unlock();

		return csStr;
	}	

	void ExecutionEOD::fnGetNextSchedule( );
	void ExecutionEOD::fnUpdateScheduleStatus(int iStatus );

	void ExecutionEOD::fnRecalc1( );
	void ExecutionEOD::fnCopyS3FiletoLocal( );
	void ExecutionEOD::fnUncompressGZ( );
	void ExecutionEOD::fnCheckFolderPath( );
	void ExecutionEOD::ReadEODFileCSV( );
	void ExecutionEOD::ParseLine( std::string &ssLine );
	void ExecutionEOD::fnAddInFiles( EodDetail &eodRowData  );
	void ExecutionEOD::fnAddRowInSubscription( std::string &strMsg, FileDtl &eodSubDtl, bool bIsProduct );
	void ExecutionEOD::fnCloseAllOpenFiles( );

	void ExecutionEOD::LoadSubscription();
	void ExecutionEOD::ProcessEODProdSubscription();
	void ExecutionEOD::ProcessEODExchSubscription();

	bool ExecutionEOD::CreateSubscriptionFile(EODSubscriptionFile &clSubFileDtl, FILE *fileIn );

	void ExecutionEOD::fnCreateZip( );
	void ExecutionEOD::UploadSubscription();

	void ExecutionEOD::fnInsertDeliveryReport(CString csPrdouct, CString csDestAccount, CString csFileName, unsigned long &lFileSize, CString &csAwscmd );

	void ExecutionEOD::fnInsertErroReport(CString csPrdouct, CString csDestAccount, CString csFileName, CString csExchnage, CString &csErrorCmd );

	void  ExecutionEOD::fnSendEmail(int iType, int iRowCount);

	unsigned long  GetFileSize(std::string const &path) 
	{
		WIN32_FIND_DATA data;
		HANDLE h = FindFirstFile(path.c_str(), &data);
		if (h == INVALID_HANDLE_VALUE)
			return -1;

		FindClose(h);

		return (data.nFileSizeLow | data.nFileSizeHigh << 32);
	}


	ScheduleInfo m_clScheduleInfo;
	enmOperation m_enmOperation;

	FileDtlMap m_clProdFileDtlMap; //Prod
	//FileDtlMap m_clExchFileDtlMap;

	//DistItemMap	m_clEODDistProdMap;
	//DistItemMap	m_clEODDistExchMap;

	EODSubDtlMap m_clEODProdSubDtlMap; //Prod
	EODSubDtlMap m_clEODExchSubDtlMap;
	
	EODSubFileMap m_clEODProdSubFileMap; //Prod	
	EODSubFileMap m_clEODExchSubFileMap;
};

BF_NAMESPACE_END


