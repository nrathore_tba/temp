#include <string>

// Test comment
static const std::string TWENTY = "20";
static const std::string EMPTY_ENTRY_DATE = "        ";
static const std::string DELIM = ",";
static const std::string ONEDIGETTIME = "00:00:0";
static const std::string TWODIGETTIME = "00:00:";
static const std::string THREEDIGETTIME = "00:0";
static const  std::string FOURDIGETTIME = "00:";
static const std::string FIVEDIGETTIME = "0";
static const std::string COL = ":";
static const std::string DOT = ".";
static const std::string EMPTY = "";
static const std::string PRICEZERO = ".0000000";

static const std::string ONEZERO = "0";
static const std::string TWOZERO = "00";
static const std::string THREEZERO = "000";
static const std::string FOURZERO = "0000";
static const std::string FIVEZERO = "00000";
static const std::string SIXZERO = "000000";


static const  int BBO_STANDARD_LENGH = 70;
static const  int BBO_OLD_LENGTH = 64;

static const  int SESSION_INDICATOR = 23;
static const  int ASK_BID_TYPE = 53;
static const  int INDICATIVE_QUOTE = 54;
static const  int MARKET_QUOTE = 55;



static const  int	e_gen_inputfile = 1;
static const  int e_gen_outputfile = 2;
static const int e_local_dn_path = 3;
static const  int e_gen_filetype = 4;
static const  int e_gen_process = 10;
static const  int e_gen_filetes = 15;

static const  int 	e_filter_exchange = 13;
static const  int e_filter_product = 9;
static const  int 	e_filter_FO = 14;
static const int e_download = 20;

static const  int e_filter_status = 12;
static const int e_interval = 17;
static const  int e_output = 16;
static const  int e_tns_part = 18;
static const   int e_UploadAllFiles  =19;
static const   int e_DestAccount = 56;
static const   int	e_S3Bucket = 57;
static const   int  e_download_S3Bucket = 58;
static const int e_baseDate = 205;


static const  int	e_Get_BLOCK_Subscription   = 11;


static const  int	e_DefaultBasePath	  =	100;
static const  int	e_DirectoryInputPath  = 101;
static const  int	e_DirectoryOutputPath = 102;

static const  int	e_DefaultDate   = 205;

static const  int	e_FileExtList   = 210;

static const  int	e_BBO_OR_TNS= 207;

static const  int	e_Decompress	= 214;

static const  int	e_Submit		= 211;

static const  int	e_PgUpBase		= 90;
static const  int	e_PgDnBase		= 91;

static const  int	e_SubscrID		=  1000;
static const  int	e_CUST_NAME		=  1050;
static const  int	e_CONTACT_EMAIL =  1100;
static const  int	e_DEST_ACCOUNT_FTP_PASSWORD = 1150;
static const  int	e_VENUE_CODE	= 1200;
static const  int	e_ALL_PRODUCTS	= 1250;
static const  int	e_PROD_CODE		= 1300;
static const  int	e_BBO_CODE		= 1350;
static const  int	e_EXCH_CODE		= 1400;
static const  int	e_OutputFileName = 1450;	
static const int e_Upload			 = 1500;




