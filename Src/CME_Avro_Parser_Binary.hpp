
#pragma once

#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <src/LogManager.h>

BF_NAMESPACE_BEGIN

class CME_Avro_Parser : public BaseImage
{
public:
	CME_Avro_Parser();
	virtual ~CME_Avro_Parser();
	
	enum FieldIDs
	{
		e_InputFolder	= 1,
		e_OutputFolder	= 2,
		e_ParseSecDef	= 5,
		e_StartProcess	= 10,

		e_StartTime		= 13,
		e_EndTime		= 14,

		e_MessageBase	= 5500,
		e_PgUp			= 5530,
		e_PgDn			= 5531,

	};
 
	void Parse();

	bool		m_bIsThreadRunning;
	LogManager	m_cLogManager;

private:
	bool InitData();
	void Recalculate();
};

BF_NAMESPACE_END