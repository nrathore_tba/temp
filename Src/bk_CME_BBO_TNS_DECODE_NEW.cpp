#include <fstream>
#include <sstream>
#include <stdafx.h> 
#include <shldisp.h>
#include <tlhelp32.h>
#include "zip.h"
#include "CME_BBO_TNS_DECODE_NEW.hpp"

#define SPLITLINE 20000

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(CME_BBO_TNS_DECODE)

CME_BBO_TNS_DECODE::CME_BBO_TNS_DECODE()
{	
}

CME_BBO_TNS_DECODE::~CME_BBO_TNS_DECODE()
{
}

bool CME_BBO_TNS_DECODE::InitData()
{
	GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));

	m_btnViewSubscription		= GetField(e_ViewSubscription);
	m_btnProcessSubscription	= GetField( e_ProcessSubscription);
	m_btnUploadAllFiles			= GetField(e_UploadAllFiles);

	m_btnPageUp					= GetField(e_PgUpBase);
	m_btnPageDown				= GetField(e_PgDnBase);

	m_strDefaultBasePath		= GetField( e_DefaultBasePath);
	m_strDirectoryInputPath		= GetField(e_DirectoryInputPath); 
	m_strDirectoryOutputPath	= GetField( e_DirectoryOutputPath);
	m_strMessage				= GetField( e_Message);


	m_fld_FileExtensionList		= (ListField*) GetField(e_FileExtList);	
	m_fld_BlockOrBlocpList		= (ListField*) GetField(e_Block_Or_Blocp);	

	m_S3Bucket					= (ListField*) GetField(e_S3Bucket) ;
	m_DestAccount				= (ListField*) GetField(e_DestAccount);


	

	m_curTopRow= 0;
	for(m_numofRows = 0; GetField(e_SubscrID + m_numofRows); m_numofRows++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));
	CString csServerName;
	RegString strServerName(pKey, "ServerName","UAT-RDS");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");

	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize data store!");
		return true;
	}

	m_clpDBInt = m_clDBConn.GetDBInterfaceNew();
	//---------------------------------------------

	COleDateTime dtToday = COleDateTime::GetCurrentTime();
	BFDate bfToday;
	bfToday.SetYearMonthDate(dtToday.GetYear(),dtToday.GetMonth() - 1, dtToday.GetDay() ); 

	GetField(e_DefaultDate)->SetValueDate(bfToday); 

	m_bIsThreadRunning = false;
	m_iTotalCount = 0;

	CString path = "D:\\PROJECTS\\CME\\BBO\\Globex";

	m_strDefaultBasePath->SetValueString(path);
	m_strDirectoryInputPath->SetValueString(path);
	m_strDirectoryOutputPath->SetValueString(path);

	return true;
}

void ParseTemp(string strdata, vector<string> &tmpV)
{	
	stringstream ss(strdata);
	string item;

	while(std::getline(ss, item, ','))
	{
		tmpV.push_back(item.c_str());			
	}
}

void CME_BBO_TNS_DECODE::LoadSubscription()
{
	m_cl_vecBBOSubscriptionDtl.clear(); 
	m_clSubFileDtlMap.clear(); 
	m_clProdFileDtlMap.clear();
	m_clExchFileDtlMap.clear();

	CString csBlockOrBlockP = m_fld_BlockOrBlocpList->GetShortString();
	
	CString sql;	
	sql.Format("SELECT CUST_NAME, CONTACT_EMAIL, DEST_ACCOUNT, FTP_PASSWORD, VENUE_CODE, ALL_PRODUCTS, PROD_CODE, BLOCK_CODE, EXCH_CODE FROM dbo.subscription_2015 where FILE_TYPE = '%s' and Active = 1",csBlockOrBlockP);

	_RecordsetPtr set;
	if( FAILED(m_clpDBInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		m_strMessage->SetValueString(sql );
		return;
	}

	int iCount(1);
	while(VARIANT_FALSE == set->adoEOF)
	{
		BBOSubscriptionDtl clBlockSub;

		clBlockSub.iOrderId   = iCount * 1000 ;
		clBlockSub.iBlockType = m_fld_BlockOrBlocpList->GetValueInt();

		SETSTR(	clBlockSub.stCUST_NAME,		set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
		SETSTR(	clBlockSub.stCONTACT_EMAIL,	set->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);	
		SETSTR(	clBlockSub.stDEST_ACCOUNT,	set->Fields->Item[_variant_t("DEST_ACCOUNT")]->Value);	
		SETSTR(	clBlockSub.stFTP_PASSWORD,	set->Fields->Item[_variant_t("FTP_PASSWORD")]->Value);	
		SETSTR(	clBlockSub.stVENUE_CODE,	set->Fields->Item[_variant_t("VENUE_CODE")]->Value);	
		SETSTR(	clBlockSub.stALL_PRODUCTS,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);	

		SETSTR(	clBlockSub.stPROD_CODE,		set->Fields->Item[_variant_t("PROD_CODE")]->Value);	
		SETSTR(	clBlockSub.stBLOCK_CODE,	set->Fields->Item[_variant_t("BLOCK_CODE")]->Value);	
		SETSTR(	clBlockSub.stEXCH_CODE,		set->Fields->Item[_variant_t("EXCH_CODE")]->Value);

		if( m_fld_BlockOrBlocpList->GetValueInt() == 0 && m_clProdFileDtlMap.find( clBlockSub.stBLOCK_CODE.GetString() ) == m_clProdFileDtlMap.end() )
		{
			FileDtl clFileDtl;
			m_clProdFileDtlMap[ clBlockSub.stBLOCK_CODE.GetString() ].strFileName = clBlockSub.stBLOCK_CODE.GetString();
		}

		if( m_fld_BlockOrBlocpList->GetValueInt() == 1 && m_clExchFileDtlMap.find( clBlockSub.stEXCH_CODE.GetString() ) == m_clExchFileDtlMap.end() )
		{
			m_clExchFileDtlMap[ clBlockSub.stEXCH_CODE.GetString() ].strFileName = clBlockSub.stEXCH_CODE.GetString();
		}

		m_cl_vecBBOSubscriptionDtl.push_back( clBlockSub );
		iCount++;
		set->MoveNext();
	}

	set->Close();	

	CString csTemp;
	csTemp.Format("%d Subscription loaded", m_cl_vecBBOSubscriptionDtl.size() );
	m_strMessage->SetValueString(csTemp );
}

void CME_BBO_TNS_DECODE::fnPageUpDown( )
{
	int iTotalRecord = m_cl_vecBBOSubscriptionDtl.size( );
	
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpBase,e_PgDnBase,iTotalRecord,m_numofRows,m_curTopRow);	
}

void CME_BBO_TNS_DECODE::DisplaySubscription()
{	
	int iRow = 0;
	for(unsigned int iIndex = m_curTopRow; iIndex < m_cl_vecBBOSubscriptionDtl.size(); iIndex++, iRow++)
	{
		if( iRow >= m_numofRows )
			break;

		BBOSubscriptionDtl &clBlockSub= m_cl_vecBBOSubscriptionDtl[iIndex];

		GetField(e_SubscrID		+ iRow)->SetValueInt(clBlockSub.iOrderId);  
		GetField(e_CUST_NAME	+ iRow)->SetValueString(clBlockSub.stCUST_NAME);  

		GetField(e_CONTACT_EMAIL + iRow)->SetValueString(clBlockSub.stDEST_ACCOUNT);  
		GetField(e_DEST_AC_FTP_PASSWORD + iRow)->SetValueString(clBlockSub.stDEST_ACCOUNT);  
		GetField(e_VENUE_CODE	+ iRow)->SetValueString(clBlockSub.stVENUE_CODE);
		GetField(e_ALL_PRODUCTS + iRow)->SetValueString(clBlockSub.stALL_PRODUCTS);
		GetField(e_PROD_CODE	+ iRow)->SetValueString(clBlockSub.stPROD_CODE);
		GetField(e_BLOCK_CODE	+ iRow)->SetValueString(clBlockSub.stBLOCK_CODE);
		GetField(e_EXCH_CODE	+ iRow)->SetValueString(clBlockSub.stEXCH_CODE);
		GetField(e_OutputFileName + iRow)->SetValueString(clBlockSub.stOutputFileName);	

		std::string strdestAct = clBlockSub.stDEST_ACCOUNT.GetString();

		SubFileDtlMap::iterator itr =  m_clSubFileDtlMap.find( clBlockSub.stDEST_ACCOUNT.GetString() );

		if( itr != m_clSubFileDtlMap.end() )
		{
			GetField(e_OutputFileName + iRow)->SetValueString( itr->second.strOutPutFileName.c_str() );
			
			if( itr->second.iUploadReturn == 999 )
			{
				GetField(e_Upload + iRow)->SetEnabled( true );
				GetField(e_OutputFileName + iRow)->SetBackColor(ColorManager::eDefault);
			}
			else
			{
				GetField(e_Upload + iRow)->SetEnabled( false );
				GetField(e_OutputFileName + iRow)->SetBackColor(ColorManager::eSignalPositiveDark);
			}
		}
		else
		{
			GetField(e_OutputFileName + iRow)->SetBackColor(ColorManager::eDefault);
			GetField(e_OutputFileName + iRow)->SetValueString("");
			GetField(e_Upload + iRow)->SetEnabled( false );
		}
	}

	while( iRow < m_numofRows )
	{
		GetField(e_SubscrID		+ iRow)->SetBlankState();  
		GetField(e_CUST_NAME	+ iRow)->SetBlankState();  

		GetField(e_CONTACT_EMAIL + iRow)->SetBlankState();  
		GetField(e_DEST_AC_FTP_PASSWORD + iRow)->SetBlankState();  
		GetField(e_VENUE_CODE	+ iRow)->SetBlankState();  
		GetField(e_ALL_PRODUCTS + iRow)->SetBlankState();  
		GetField(e_PROD_CODE	+ iRow)->SetBlankState();  
		GetField(e_BLOCK_CODE	+ iRow)->SetBlankState();  
		GetField(e_EXCH_CODE	+ iRow)->SetBlankState();  
		GetField(e_OutputFileName + iRow)->SetBlankState();  
		GetField(e_OutputFileName + iRow)->SetBackColor(ColorManager::eDefault);

		GetField(e_Upload + iRow)->SetEnabled( false );
		iRow++;
	}
}

//Check PureCMBKUP pc
//CString __csDestinationPath = "s3://beastftp/"; 
//CString __csDestinationPath = "s3://cmeftp/ftpdata/";

void CME_BBO_TNS_DECODE::UploadSubscription()
{	
	CString __csDestinationPath = m_S3Bucket->GetShortString();
	CString __csAccount = m_DestAccount->GetShortString(); // This is Test Account

	if( IsFieldNew( e_UploadAllFiles ) )
	{
		for(unsigned int iIndex = 0; iIndex < m_cl_vecBBOSubscriptionDtl.size(); iIndex++)
		{
			BBOSubscriptionDtl &clBlockSub	= m_cl_vecBBOSubscriptionDtl[iIndex];

			SubFileDtlMap::iterator itr =  m_clSubFileDtlMap.find( clBlockSub.stDEST_ACCOUNT.GetString() );

			if( itr != m_clSubFileDtlMap.end() )
			{
				CString csSourcePath = itr->second.strOutPutFileName.c_str();
				CString csDestinationFolder;
				if( m_DestAccount->GetValueInt() == 1 )
					csDestinationFolder.Format("%s%s/%s/",__csDestinationPath,  itr->second.strDestAccount.c_str(), m_fld_BlockOrBlocpList->GetShortString());
				else
					csDestinationFolder.Format("%s%s/%s/",__csDestinationPath, __csAccount, m_fld_BlockOrBlocpList->GetShortString());

				CString csAWSCommand;
				csAWSCommand.Format(_T("aws s3 cp %s %s"),csSourcePath, csDestinationFolder);					
				itr->second.iUploadReturn = system(csAWSCommand);	

				CString stroutput;
				stroutput.Format("cmd %s @@\n", csAWSCommand, itr->second.iUploadReturn );
				PrintRawMessage( stroutput );
			}
		}

		m_strMessage->SetValueString("Subscription uploaded successfully ..!!");
	}
	else
	{
		int iRow = 0;
		for(unsigned int iIndex = m_curTopRow; iIndex < m_cl_vecBBOSubscriptionDtl.size(); iIndex++, iRow++)
		{
			if( iRow >= m_numofRows )
				break;

			BBOSubscriptionDtl &clBlockSub= m_cl_vecBBOSubscriptionDtl[iIndex];

			if( IsFieldNew( e_Upload + iRow ) )
			{
				SubFileDtlMap::iterator itr =  m_clSubFileDtlMap.find( clBlockSub.stDEST_ACCOUNT.GetString() );

				if( itr != m_clSubFileDtlMap.end() )
				{
					CString csSourcePath = itr->second.strOutPutFileName.c_str();
					CString csDestinationFolder;
					if( m_DestAccount->GetValueInt() == 1 )
						csDestinationFolder.Format("%s%s/%s/",__csDestinationPath,  itr->second.strDestAccount.c_str(), m_fld_BlockOrBlocpList->GetShortString());
					else
						csDestinationFolder.Format("%s%s/%s/",__csDestinationPath,  __csAccount, m_fld_BlockOrBlocpList->GetShortString());

					CString csAWSCommand;
					csAWSCommand.Format(_T("aws s3 cp %s %s"),csSourcePath, csDestinationFolder);					
					itr->second.iUploadReturn = system(csAWSCommand);	

					CString stroutput;
					stroutput.Format("cmd %s @@\n", csAWSCommand, itr->second.iUploadReturn );
					PrintRawMessage( stroutput );

					m_strMessage->SetValueString( "Subscription uploaded successfully ..!!" );
					break;
				}
			}		
		}
	}	
}

UINT BlockProcessor(void *pVoid)
{
	CME_BBO_TNS_DECODE *pParser = (CME_BBO_TNS_DECODE*)pVoid;
	
	pParser->m_iTotalCount = 0;
	pParser->m_bIsThreadRunning = true;
	pParser->m_bIsThreadCompleted = false;
	
	pParser->Parse();

	pParser->m_bIsThreadRunning = false;
	pParser->m_bIsThreadCompleted = true;
	pParser->RequestExternalUpdate();

	return 0;
}

void CME_BBO_TNS_DECODE::Parse( )
{
	bool bProcess = true;
	CString csTemp;
	CString sFileName = m_strDirectoryInputPath->GetValueString();
	if( PathFileExists(sFileName) == FALSE )
	{
		// cheking gz file exist..
		csTemp = "File not exist...";
		bProcess = false;
	}

	sFileName.Replace(".zip",".txt");
	if( PathFileExists(sFileName) == FALSE )
	{
		// cheking gz file exist..
		if( bProcess == false )
		{
			csTemp = "gz File not exist...";
		}
		else
		{
			fnUncompressGZ( );				
			bProcess = true;
		}			
	}

	if( bProcess && m_cl_vecBBOSubscriptionDtl.size() == 0 )
	{
		bProcess = false;
		csTemp = "Please load subscriptions";				
	}

	CString csPath = m_strDirectoryOutputPath->GetValueString();	
	
	if( bProcess && CreateDirectory(csPath, NULL) == 0 )
	{		
		if( GetLastError() == ERROR_PATH_NOT_FOUND )
		{			
			csTemp = "Error OputPath Not Found";	
			bProcess = false;
		}
	}

	csPath = m_strDirectoryOutputPath->GetValueString();	
	if( bProcess && CreateDirectory(csPath, NULL) == 0 )
	{		
		if( GetLastError() == ERROR_PATH_NOT_FOUND )
		{
			csTemp = "Error OputPath Not Found";	
			bProcess = false;
		}
	}

	if( bProcess )
		ReadBlockCSVFile( );
	else
		fnWriteMessage( csTemp );
}

void CME_BBO_TNS_DECODE::Recalculate()
{
	if( m_bIsThreadRunning )
	{		
		m_strMessage->SetValueString( fnReadMessage() );	
		return;
	}

	if( m_bIsThreadCompleted )
	{
		m_strMessage->SetValueString( fnReadMessage() );	
		m_bIsThreadCompleted = false;		
	}

	if( IsFieldNew(e_DefaultBasePath) || IsFieldNew(e_DefaultDate)  || IsFieldNew(e_Block_Or_Blocp) || IsFieldNew(e_FileExtList) )
	{		
		GenerateFileName();
		m_strMessage->SetValueString( "" );	
	}

	if( IsFieldNew(e_ViewSubscription) )
	{
		LoadSubscription();	
	}	
	
	if( IsFieldNew(e_ProcessSubscription) && !m_bIsThreadRunning)
	{
		COleDateTime dtCurrent = COleDateTime::GetCurrentTime();
		CString strCurrentTime;
		strCurrentTime.Format("%04d/%02d%02d %02d:%02d:%02d", dtCurrent.GetYear(), dtCurrent.GetMonth(), dtCurrent.GetDay(), 
			dtCurrent.GetHour(), dtCurrent.GetMinute(), dtCurrent.GetSecond());

		AfxBeginThread(BlockProcessor, this);
	}

	//UploadSubscription();

	fnPageUpDown( );
	DisplaySubscription( );
}

void CME_BBO_TNS_DECODE::fnUncompressGZ(  )
{
	CString csFilePath, csUncompFileName, csCommand;

	csFilePath = m_strDirectoryInputPath->GetValueString( );

	csFilePath =   csFilePath.Mid(1);
	csFilePath.Replace("\\","\\\\");
	csUncompFileName = csFilePath;
	csUncompFileName.Replace(".zip",".txt");



	//csCommand.Format("gzip.exe -d -c \\\\powervault3\\\\CME_s3\\\\daily\\\\blocks\\\\20150801\\\\BLOCKS_20150801.gz > \\\\powervault3\\\\CME_s3\\\\daily\\\\blocks\\\\20150801\\\\BLOCKS_20150801.txt");
	csCommand.Format("gzip.exe -d -c %s > %s", csFilePath, csUncompFileName);
		
	int	iValue = system(csCommand);

	if( iValue == 0 )
	{
		//UpdateUncompressflag(1, index);
		m_strMessage->SetValueString("Uncompressed file successfully.");
	}
	else
	{
		CString csTemp;
		csTemp.Format("error no = %d - path %s, uncompress unsuccessful",iValue,csFilePath);
		m_strMessage->SetValueString(csTemp);
	}
}

void CME_BBO_TNS_DECODE::GenerateFileName( )
{
	CString strBasePath = m_strDefaultBasePath->GetValueString();
	BFDate  bfDefaultDate = GetField(e_DefaultDate)->GetValueDate(); 
	
	CString csDate;
	csDate.Format("%d%02d%02d",bfDefaultDate.GetYear(),bfDefaultDate.GetMonth() + 1,bfDefaultDate.GetDate());
	
	
	CString strFileExtensionList = m_fld_FileExtensionList->GetShortString();	
	CString strBlockOrBlockPFile = m_fld_BlockOrBlocpList->GetDisplayString();//change
	CString strFileName  = strBlockOrBlockPFile + "S_" + csDate + "." + strFileExtensionList ;

	CString strFullInputPath;
	strFullInputPath.Format("%s\\%s\\%s",strBasePath,csDate,strFileName);


	m_strDirectoryInputPath->SetValueString( strFullInputPath );

	CString strFullOutput;
	strFullOutput.Format("%s\\%s\\%s",strBasePath,csDate,m_fld_BlockOrBlocpList->GetShortString());
	m_strDirectoryOutputPath->SetValueString( strFullOutput );

}


#define FSUCCESS 0
void CME_BBO_TNS_DECODE::ReadBlockCSVFile( )
{
	CString csTemp;
	CString csFileNamePath = m_strDirectoryInputPath->GetValueString();
	csFileNamePath.Replace(".zip", ".txt");
	
	FILE *fileIn(NULL);
	
	if( fopen_s(&fileIn, csFileNamePath, "r") != FSUCCESS )
	{
		csTemp = "Error while Opening fileIn";
		fnWriteMessage( csTemp );
		return;
	}

	char chline[2048];
	std::string strLine("");
	int lRecordRead(0);
		
	csTemp = "Reading input File..";
	fnWriteMessage( csTemp );
	this->RequestExternalUpdate();

	// Read Header...
	while( fgets(chline, 2048, fileIn) )
	{
		if(strchr(chline, '\n'))
		{
			strLine += chline;
			
			//ParseMessage(strLine); 
			strLine = "";
			lRecordRead++;
			break;
		}
		else
		{
			strLine += chline;
		}		
	}

	int iCountTT = 0;
	while( fgets(chline, 2048, fileIn) )
	{
		if(strchr(chline, '\n'))
		{
			strLine += chline;
			
			ParseLine(strLine); 

			strLine = "";
			lRecordRead++;
			iCountTT++;

			if( iCountTT == 1000 )
			{
				csTemp.Format("Reading line %d", lRecordRead);
				fnWriteMessage( csTemp );
				this->RequestExternalUpdate();
				iCountTT = 0;
			}
		}
		else
		{
			strLine += chline;
		}
	}

	fclose(fileIn);

	csTemp = "Processing prod/each File..";	
	fnWriteMessage( csTemp );
	this->RequestExternalUpdate();

	if( m_fld_BlockOrBlocpList->GetValueInt() == 0 )
	{
		FileDtlMap::iterator itr = m_clProdFileDtlMap.begin();

		while( itr != m_clProdFileDtlMap.end() )
		{
			if( itr->second.fpOut != NULL )
			{
				fclose( itr->second.fpOut );
				itr->second.fpOut = NULL;
			}
			itr++;
		}	
	}
	else 
	{
		FileDtlMap::iterator itr = m_clExchFileDtlMap.begin();

		while( itr != m_clExchFileDtlMap.end() )
		{
			if( itr->second.fpOut != NULL )
			{	
				fclose( itr->second.fpOut );
				itr->second.fpOut = NULL;
			}

			itr++;
		}	
	}

	// Recreate all DEST_ACCOUNT file splittable at 50000...
	csTemp = "Creating Subscription File..";
	fnWriteMessage( csTemp );
	this->RequestExternalUpdate();

	m_clSubFileDtlMap.clear();
	
	for(unsigned int iI = 0; iI < m_cl_vecBBOSubscriptionDtl.size(); iI++ )
	{
		std::string strInputFile;
		if( m_fld_BlockOrBlocpList->GetValueInt() == 0 )
		{
			if( m_clProdFileDtlMap[ m_cl_vecBBOSubscriptionDtl[iI].stBLOCK_CODE.GetString() ].iRowCount == 0 )
				continue;

			strInputFile = m_clProdFileDtlMap[ m_cl_vecBBOSubscriptionDtl[iI].stBLOCK_CODE.GetString() ].strFilePath;
		}
		else
		{
			if( m_clExchFileDtlMap[ m_cl_vecBBOSubscriptionDtl[iI].stEXCH_CODE.GetString() ].iRowCount == 0 )
				continue;

			strInputFile = m_clExchFileDtlMap[ m_cl_vecBBOSubscriptionDtl[iI].stEXCH_CODE.GetString() ].strFilePath;
		}

		if( m_clSubFileDtlMap.find( m_cl_vecBBOSubscriptionDtl[iI].stDEST_ACCOUNT.GetString() ) == m_clSubFileDtlMap.end() )
		{
			SubFileDtl &clSubFileDtl = m_clSubFileDtlMap[ m_cl_vecBBOSubscriptionDtl[iI].stDEST_ACCOUNT.GetString() ];	
			clSubFileDtl.strDestAccount = m_cl_vecBBOSubscriptionDtl[iI].stDEST_ACCOUNT.GetString();
			clSubFileDtl.iFileCount = 1;
		}

		FILE *fileIn;
		if( fopen_s(&fileIn, strInputFile.c_str(), "r") != FSUCCESS )
		{
			//CString strError = "Error while Opening fileIn";
			//GetField(e_Message)->SetValueString( strError );		
		}
		else
		{
			bool bContinue = true;
			do
			{
				SubFileDtl &clSubFileDtl = m_clSubFileDtlMap[ m_cl_vecBBOSubscriptionDtl[iI].stDEST_ACCOUNT.GetString() ];

				if( clSubFileDtl.iRowCount == SPLITLINE )
				{
					fclose(clSubFileDtl.fpOut);

					clSubFileDtl.fpOut = NULL;
					clSubFileDtl.iFileCount++;
					clSubFileDtl.iRowCount = 0;
				}

				if( clSubFileDtl.fpOut == NULL )
				{
					CString csBasePath = m_strDirectoryOutputPath->GetValueString();
					BFDate  bfDefaultDate = GetField(e_DefaultDate)->GetValueDate(); 
					CString csDate;
					csDate.Format("%d%02d%02d",bfDefaultDate.GetYear(),bfDefaultDate.GetMonth() + 1,bfDefaultDate.GetDate());

					CString csOutputFolder;
					csOutputFolder.Format("%s\\%s",csBasePath, clSubFileDtl.strDestAccount.c_str());

					CreateDirectory( csOutputFolder, NULL);

					clSubFileDtl.strFolderPath = csOutputFolder.GetString();

					CString csFileName;
					csFileName.Format("E%03d_%s.csv", clSubFileDtl.iFileCount, csDate);			

					CString csPath;
					csPath.Format("%s\\%s", csOutputFolder, csFileName);

					//clSubFileDtl.strFilePath = csPath.GetString();*

					if( fopen_s(&clSubFileDtl.fpOut, csPath,"w") )
						return;

					clSubFileDtl.iRowCount = 1;
					fputs( __strHeader.c_str(), clSubFileDtl.fpOut );
				}

				bContinue = CreateSubscriptionFile(clSubFileDtl,  strInputFile, fileIn);
			}
			while( bContinue );
		}

		csTemp.Format("Preparing data for subscription %d",iI);
		fnWriteMessage( csTemp );
		this->RequestExternalUpdate();
	}

	SubFileDtlMap::iterator itr = m_clSubFileDtlMap.begin();

	while( itr != m_clSubFileDtlMap.end() )
	{		
		if( itr->second.fpOut != NULL )
		{
			fclose( itr->second.fpOut );
			itr->second.fpOut = NULL;
		}

		itr++;
	}

	csTemp = "Creating zip File..";	
	fnWriteMessage( csTemp );
	this->RequestExternalUpdate();

	itr = m_clSubFileDtlMap.begin();
	BFDate  bfDefaultDate = GetField(e_DefaultDate)->GetValueDate(); 
	CString csDate;
	csDate.Format("%d%02d%02d",bfDefaultDate.GetYear(),bfDefaultDate.GetMonth() + 1,bfDefaultDate.GetDate());

	while( itr != m_clSubFileDtlMap.end() )
	{
		csTemp.Format("Creating zip %s Account",itr->second.strDestAccount.c_str());
		fnWriteMessage( csTemp );
		this->RequestExternalUpdate();

		if( itr->second.iFileCount > 0 )
		{
			CString csZipName;
			csZipName.Format("%s\\BLOCK_%s.zip", itr->second.strFolderPath.c_str(), csDate);

			itr->second.strOutPutFileName = csZipName.GetString();

			HZIP hz = CreateZip(csZipName,0);

			CString csFilePath;
			CString csFileName;

			for( int iFile = 0; iFile < itr->second.iFileCount; iFile++ )
			{				
				csFileName.Format("E%03d_%s.csv", iFile + 1, csDate);			 

				csFilePath.Format("%s\\%s", itr->second.strFolderPath.c_str(), csFileName);
				ZipAdd(hz, csFileName, csFilePath);			
			}
			CloseZip(hz);

	//		/*HZIP hz = CreateZip("c:\\simple1.zip",0);
	//		ZipAdd(hz, "E001_simple.csv", "\\\\powervault3\\CME_s3\\daily\\endofday\\20150805_Output\\E\\1000\\E001_20150805.txt");
	//		ZipAdd(hz,"E002_simple.csv", "\\\\powervault3\\CME_s3\\daily\\endofday\\20150805_Output\\E\\1000\\E002_20150805.txt");
	//		CloseZip(hz);*/			
		}
		
		itr++;
	}

	csTemp = "Process completed successfully ..!!";
	fnWriteMessage( csTemp );
	//this->RequestExternalUpdate();
}


bool CME_BBO_TNS_DECODE::CreateSubscriptionFile(SubFileDtl &clSubFileDtl, std::string &strInputFile, FILE *fileIn )
{
	char chline[2048];
	string strLine("");

	while( fgets(chline, 2048, fileIn) )
	{
		if(strchr(chline, '\n'))
		{
			strLine += chline;

			fputs(strLine.c_str(), clSubFileDtl.fpOut);
			
			strLine = "";
			clSubFileDtl.iRowCount++;

			if( clSubFileDtl.iRowCount == SPLITLINE )
				return true;
		}
		else
		{
			strLine += chline;
		}
	}

	fclose(fileIn);
	return false;
}



void CME_BBO_TNS_DECODE::ParseLine( std::string &ssLine )
{
	std::stringstream ss(ssLine);
	std::string item;
	
	BBODetail clBBODetail;
	while(std::getline(ss, item, ','))
	{
		if( item == "null" )
			clBBODetail.strRecord.push_back("");
		else
			clBBODetail.strRecord.push_back(item.c_str());
	}

	while( clBBODetail.strRecord.size() > m_niInputColumnCount )
	{
		clBBODetail.strRecord[enm_TICKER_SYMBOL_DESCRIPTION] = clBBODetail.strRecord[enm_TICKER_SYMBOL_DESCRIPTION]+","+clBBODetail.strRecord[enm_FOI_SYMBOL_IND];
		clBBODetail.strRecord.erase(clBBODetail.strRecord.begin() + enm_FOI_SYMBOL_IND);
	}

	fnCheckInSubscription( clBBODetail );	
}


bool CME_BBO_TNS_DECODE::fnCheckInSubscription( BBODetail &blockRowData )
{ 
	if( m_fld_BlockOrBlocpList->GetValueInt() == 0 )
	{
		//BLOCK
		std::string stFileName = blockRowData.strRecord[enm_TICKER_SYMBOL].c_str();
		
		if( m_clProdFileDtlMap.find( stFileName ) != m_clProdFileDtlMap.end( ) )
		{
			fnAddRowInSubscription( blockRowData, m_clProdFileDtlMap[stFileName] );
		}
	}
	else
	{
		//BLOCP
		string stFileName = blockRowData.strRecord[enm_EXCH_CODE].c_str();

		if( m_clExchFileDtlMap.find( stFileName ) != m_clExchFileDtlMap.end( ) )
		{
			fnAddRowInSubscription( blockRowData, m_clExchFileDtlMap[stFileName] );
		}
	}

	return false;
}


void CME_BBO_TNS_DECODE::fnAddRowInSubscription( BBODetail &clBBODetail, FileDtl &blockSubDtl )
{
	if( blockSubDtl.fpOut == NULL )
	{
		CString csBasePath = m_strDefaultBasePath->GetValueString();
		
		CString csPath;
		csPath.Format("%s\\%s.csv", csBasePath, blockSubDtl.strFileName.c_str());
		
		blockSubDtl.strFilePath = csPath;

		if( fopen_s(&blockSubDtl.fpOut, csPath,"w") )
			return;

		blockSubDtl.iRowCount = 1;
		//fputs( __strHeader.c_str(), eodSubDtl.fpOut );

		std::string strMsg;
		clBBODetail.GetRowMessage(strMsg);
		fputs( strMsg.c_str(), blockSubDtl.fpOut);
	}
	else
	{
		std::string strMsg;
		clBBODetail.GetRowMessage(strMsg);
		fputs( strMsg.c_str(), blockSubDtl.fpOut);
		blockSubDtl.iRowCount++;
	}
}
