// Copyright (C) 2000 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#pragma once

#include <shared/BaseImage.hpp>
#include <portalhandler/BFInterestRateBaseApp.hpp>
#include <shared/DBConnection.hpp>

#define _ColumnCount 17

BF_NAMESPACE_BEGIN

struct FileDetail
{
	CString csFileName;
	unsigned long lFileSize;
	int iStatus;
};	


struct stSubsciption
{
	CString csCUST_NAME;			
	CString csCONTACT_EMAIL;
	CString csDEST_ACCOUNT;	
	CString csFTP_PASSWORD;	
	COleDateTime oledtEXPIRATION_DATE;
	double dACTIVE;
	COleDateTime oledtCREATE_TIMESTAMP;
	CString csVENUE_CODE;
	CString csFILE_TYPE;		
	double  dALL_PRODUCTS;
	CString csPROD_CODE;	
	CString csBBO_CODE;		
	CString csMD_CODE;		
	CString csBLOCK_CODE;
	CString csEOD_CODE;		
	CString csTICK_CODE;			
	CString csEXCH_CODE;
	CString csFOI_CODE;

	stSubsciption()
	{
		csCUST_NAME			= "";		
		csCONTACT_EMAIL		= "";
		csDEST_ACCOUNT		= "";
		csFTP_PASSWORD		= "";
		dACTIVE				= 0;
		csVENUE_CODE		= "";
		csFILE_TYPE			= "";
		dALL_PRODUCTS		= 0;
		csPROD_CODE			= "";
		csBBO_CODE			= "";
		csMD_CODE			= "";
		csBLOCK_CODE		= "";
		csEOD_CODE			= "";
		csTICK_CODE			= "";
		csEXCH_CODE			= "";
		csFOI_CODE			= "";
	}
};


class SubscriptionMaster : public BaseImage 
{

public:
	enum Field
	{
		e_Product = 2001,
		e_FTPAccount = 2002,

		e_statusMsg = 2006,
		e_TotalCount = 2007,
		e_PgNumber = 3022,


		e_CUST_NAME			=151,
		e_CONTACT_EMAIL		=201,
		e_DEST_ACCOUNT		=251,
		e_FTP_PASSWORD		=301,
		e_EXPIRATION_DATE	=351,
		e_ACTIVE			=401,
		e_CREATE_TIMESTAMP	=451,
		e_VENUE_CODE		=501,
		e_FILE_TYPE			=551,
		e_ALL_PRODUCTS		=601,
		e_PROD_CODE			=651,
		e_BBO_CODE			=701,
		e_MD_CODE			=751,
		e_BLOCK_CODE		=801,
		e_EOD_CODE			=851,
		e_TICK_CODE			=901,
		e_EXCH_CODE			=951,
		e_FOI_CODE			=1001,

		e_ReloadSubscr		= 5012,

		e_PgUpBase	= 6016,
		e_PgDnBase	= 6019,
	};

	SubscriptionMaster();
	virtual ~SubscriptionMaster();


	bool InitData();
	void Recalculate();

	int m_numofRows;
	int m_curTopRow;

	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;

	std::vector<stSubsciption> m_clSubscriptionV;	
private:

	void SubscriptionMaster::DisplaySubscriptionList( );
	void SubscriptionMaster::fnPageUpDown( );	
	void SubscriptionMaster::LoadSubscription( );

	ListField *fld_Product;
	ListField *fld_FtpAccont;
};

BF_NAMESPACE_END
