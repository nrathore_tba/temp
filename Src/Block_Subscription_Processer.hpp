
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <shared/MDDDataHandler.hpp>

BF_NAMESPACE_BEGIN

using namespace std;

const string __strHeader = "Trade Date,Trade Time,Report Time,Contract Symbol,Product Code,Product Type,Description,Market Sector,Asset Class,Contract Year,Contract Month,Strike Price,Put/Call,Exchange Code,Trade Price,Trade Quantity,Trade Source,Spread Type,Spread Description,Contract Symbol 2,Product Code 2,Asset Class 2,Market Sector 2,Description 2,Product Type 2,Contract Year 2,Contract Month 2,Strike Price 2,Put/Call 2,Exchange Code 2,Trade Price 2,Trade Quantity 2,Contract Symbol 3,Product Code 3,Asset Class 3,Market Sector 3,Description 3,Product Type 3,Contract Year 3,Contract Month 3,Strike Price 3,Put/Call 3,Exchange Code 3,Contract Symbol 4,Product Code 4,Asset Class 4,Market Sector 4,Description 4,Product Type 4,Contract Year 4,Contract Month 4,Strike Price 4,Put/Call 4,Exchange Code 4,\n";

const CString __csOPColumnName[] =
{
	"TRADE_DATE",
	"TRADE_TIME",
	"CALL_TIME",
	"CONTRACT_SYMBOL",
	"TICKER_SYMBOL",
	"FOI_SYMBOL_IND",
	"TICKER_SYMBOL_DESCRIPTION",
	"MARKET_SECTOR",
	"ASSET_CLASS",
	"CONTRACT_YEAR",
	"CONTRACT_MONTH",
	"STRIKE_PRICE",
	"PUT_CALL",
	"EXCH_CODE",
	"TRADE_PRICE",
	"TRADE_QUANTITY",
	"SOURCE",
	"SPREAD_TYPE",
	"SPREAD_DESCRIPTION",
	"CONTRACT_SYMBOL_2",
	"TICKER_SYMBOL_2",
	"TICKER_SYMBOL_DESCRIPTION_2",
	"MARKET_SECTOR_2",
	"ASSET_CLASS_2",
	"FOI_SYMBOL_IND_2",
	"CONTRACT_YEAR_2",
	"CONTRACT_MONTH_2",
	"STRIKE_PRICE_2",
	"PUT_CALL_2",
	"EXCH_CODE_2",
	"TRADE_PRICE_2",
	"TRADE_QUANTITY_2",
	"CONTRACT_SYMBOL_3",
	"TICKER_SYMBOL_3",
	"TICKER_SYMBOL_DESCRIPTION_3",
	"MARKET_SECTOR_3",
	"ASSET_CLASS_3",
	"FOI_SYMBOL_IND_3",
	"CONTRACT_YEAR_3",
	"CONTRACT_MONTH_3",
	"STRIKE_PRICE_3",
	"PUT_CALL_3",
	"EXCH_CODE_3",
	"CONTRACT_SYMBOL_4",
	"TICKER_SYMBOL_4",
	"TICKER_SYMBOL_DESCRIPTION_4",
	"MARKET_SECTOR_4",
	"ASSET_CLASS_4",
	"FOI_SYMBOL_IND_4",
	"CONTRACT_YEAR_4",
	"CONTRACT_MONTH_4",
	"STRIKE_PRICE_4",
	"PUT_CALL_4",
	"EXCH_CODE_4"
};


// 54 columns..
enum ColumnName
{
	enm_TRADE_DATE,
	enm_TRADE_TIME,
	enm_CALL_TIME,
	enm_CONTRACT_SYMBOL,
	enm_TICKER_SYMBOL,
	enm_FOI_SYMBOL_IND,
	enm_TICKER_SYMBOL_DESCRIPTION,
	enm_MARKET_SECTOR,
	enm_ASSET_CLASS,
	enm_CONTRACT_YEAR,
	enm_CONTRACT_MONTH,
	enm_STRIKE_PRICE,
	enm_PUT_CALL,
	enm_EXCH_CODE,
	enm_TRADE_PRICE,
	enm_TRADE_QUANTITY,
	enm_SOURCE,
	enm_SPREAD_TYPE,
	enm_SPREAD_DESCRIPTION,
	enm_CONTRACT_SYMBOL_2,
	enm_TICKER_SYMBOL_2,
	enm_TICKER_SYMBOL_DESCRIPTION_2,
	enm_MARKET_SECTOR_2,
	enm_ASSET_CLASS_2,
	enm_FOI_SYMBOL_IND_2,
	enm_CONTRACT_YEAR_2,
	enm_CONTRACT_MONTH_2,
	enm_STRIKE_PRICE_2,
	enm_PUT_CALL_2,
	enm_EXCH_CODE_2,
	enm_TRADE_PRICE_2,
	enm_TRADE_QUANTITY_2,
	enm_CONTRACT_SYMBOL_3,
	enm_TICKER_SYMBOL_3,
	enm_TICKER_SYMBOL_DESCRIPTION_3,
	enm_MARKET_SECTOR_3,
	enm_ASSET_CLASS_3,
	enm_FOI_SYMBOL_IND_3,
	enm_CONTRACT_YEAR_3,
	enm_CONTRACT_MONTH_3,
	enm_STRIKE_PRICE_3,
	enm_PUT_CALL_3,
	enm_EXCH_CODE_3,
	enm_CONTRACT_SYMBOL_4,
	enm_TICKER_SYMBOL_4,
	enm_TICKER_SYMBOL_DESCRIPTION_4,
	enm_MARKET_SECTOR_4,
	enm_ASSET_CLASS_4,
	enm_FOI_SYMBOL_IND_4,
	enm_CONTRACT_YEAR_4,
	enm_CONTRACT_MONTH_4,
	enm_STRIKE_PRICE_4,
	enm_PUT_CALL_4,
	enm_EXCH_CODE_4,
	enm_END	
};

struct BlockDetail
{
	vector<string> strRecord;

	void GetRowMessage(string &msg )
	{
		msg = strRecord[enm_TRADE_DATE];
		for( int iI = 1; iI < strRecord.size(); iI++ )
		{
			msg += "," + strRecord[iI];
		}
		msg += "\n";		
	}
};	

typedef vector<BlockDetail> vec_BlockDetail;

struct BlockSubscriptionDtl
{
	BlockSubscriptionDtl()
	{
		fpOut = NULL;
		iRowCount = 0;
		iOrderId  = 0;
		iBlockType = 0;

		stCUST_NAME = "";
		stCONTACT_EMAIL = "";
		stDEST_ACCOUNT = "";
		stFTP_PASSWORD = "";
		stVENUE_CODE = "";
		stALL_PRODUCTS = "";
		stPROD_CODE = "";
		stBLOCK_CODE = "";
		stEXCH_CODE = "";
		stOutputFileName = "";
	}

	FILE *fpOut;
	int iRowCount;
	int iOrderId;
	int iBlockType; // BLOCK , BLOCKP

	CString stCUST_NAME;
	CString stCONTACT_EMAIL;
	CString stDEST_ACCOUNT;
	CString stFTP_PASSWORD;
	CString stVENUE_CODE;
	CString stALL_PRODUCTS;
	CString stPROD_CODE;
	CString stBLOCK_CODE;
	CString stEXCH_CODE;
	CString stOutputFileName;	
};

struct FileDtl
{
	string strFileName;
	string strFilePath;
	FILE *fpOut;
	int iRowCount;

	FileDtl( )
	{
		strFileName = "";
		strFilePath = "";
		fpOut = NULL;
		iRowCount = 0;
	}
};

struct SubFileDtl
{
	string strDestAccount;
	string strFileName;
	string strFolderPath;

	string strOutPutFileName;

	FILE *fpOut;
	int iRowCount;
	int iFileCount;
	int iUploadReturn;

	SubFileDtl( )
	{
		strFileName = "";
		strFolderPath = "";
		fpOut = NULL;
		iRowCount = 0;
		iFileCount = 0;
		iUploadReturn = 999;
	}
};

typedef map<string, SubFileDtl> SubFileDtlMap;

typedef map<string, FileDtl> FileDtlMap;
typedef vector<BlockSubscriptionDtl> vec_BlockSubscriptionDtl;

class Block_Subscription_Processer : public BaseImage
{
public:

	enum FieldIDs
	{
		e_Message   = 11,

		e_S3InputBucket = 58,
		e_DestAccount = 56,
		e_S3Bucket = 57,

		e_DefaultBasePath	  =	100,
		e_DirectoryInputPath  = 101,
		e_DirectoryOutputPath = 102,

		e_DefaultDate   = 205,
		e_FileExtList   = 210,
		e_Block_Or_Blocp = 207,

		e_DownloadFile	= 221,

		e_UploadAllFiles		= 216,
		e_ViewSubscription	 = 220,
		e_ProcessSubscription		= 211,

		e_PgUpBase		= 90,
		e_PgDnBase		= 91,

		e_SubscrID		=  1000,
		e_CUST_NAME		=  1050,
		e_CONTACT_EMAIL =  1100,
		e_DEST_AC_FTP_PASSWORD = 1150,
		e_VENUE_CODE	= 1200,
		e_ALL_PRODUCTS	= 1250,
		e_PROD_CODE		= 1300,
		e_BLOCK_CODE		= 1350,
		e_EXCH_CODE		= 1400,
		e_OutputFileName= 1450,		
		e_Upload		= 1500,

	};

	Block_Subscription_Processer();
	~Block_Subscription_Processer();	

	bool InitData();		
	void Recalculate();	

	void GenerateFileName();

	static const int m_niInputColumnCount = 55;
	static const int m_niOutputColumnCount = 55;

	FileDtlMap m_clProdFileDtlMap;
	FileDtlMap m_clExchFileDtlMap;

	vec_BlockSubscriptionDtl	m_cl_vecBlockSubscriptionDtl;
	SubFileDtlMap m_clSubFileDtlMap;

	void fnUncompressGZ( );
	void ReadBlockCSVFile( );
	void ParseLine( string &ssLine );

	bool fnCheckInSubscription( BlockDetail   &blockRowData  );
	void fnAddRowInSubscription( BlockDetail &clBlockDetail, FileDtl &blockSubDtl );

	void LoadSubscription();
	void fnPageUpDown( );
	void DisplaySubscription();

	void UploadSubscription();

	bool CreateSubscriptionFile(SubFileDtl &clSubFileDtl, std::string &strInputFile, FILE *fileIn );

	void Block_Subscription_Processer::fnCopyS3FiletoLocal( );

	void Block_Subscription_Processer::fnCloseAllopenFile( );

private:
	
	BaseField * m_strDefaultBasePath ;
	BaseField * m_strDirectoryInputPath ; 
	BaseField * m_strDirectoryOutputPath ;
	BaseField * m_strMessage ;

	BaseField * m_btnViewSubscription ;
	BaseField * m_btnProcessSubscription ;
	BaseField * m_btnUploadAllFiles;

	BaseField * m_btnPageUp ;
	BaseField * m_btnPageDown ;

	

	ListField * m_fld_FileExtensionList; //gz or zip
	ListField * m_fld_BlockOrBlocpList; //BLOCK OR BLOP

	ListField * m_S3Bucket ;
	ListField * m_DestAccount ;

	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpDBInt;

	int m_numofRows;
	int m_curTopRow;

public:

	int m_iOpenFileCount;
	bool m_bIsThreadCompleted;
	bool m_bIsThreadRunning;

	CString m_csMsg;
	int m_iTotalCount;

	void Parse();

	CCriticalSection	m_MsgCriticalSec;

	void fnWriteMessage(CString csString )
	{
		CSingleLock clMsgSingleLock(&m_MsgCriticalSec);

		clMsgSingleLock.Lock();

		m_csMsg = csString.GetString();

		clMsgSingleLock.Unlock();
	}

	CString fnReadMessage( )
	{
		CSingleLock clMsgSingleLock(&m_MsgCriticalSec);

		clMsgSingleLock.Lock();

		CString csStr = m_csMsg.GetString();

		clMsgSingleLock.Unlock();

		return csStr;
	}

};

BF_NAMESPACE_END


