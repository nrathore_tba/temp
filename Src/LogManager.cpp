#include "stdafx.h"
#include "LogManager.h"
#include <Shared/DBConnection.hpp>
#include <fields/FileField.hpp>
#include "MessageUtil.h"

BF_NAMESPACE_USE

LogManager::LogManager(void)
{
	m_pImage = NULL;
	m_nCurrTopRow = 0;
	m_nCurrIndex = -1;
}

LogManager::~LogManager(void)
{
}

void LogManager::Init(BaseImage* image, int nMsgBase, int nPgUp, int nPgDn)
{
	m_pImage = image;

	m_nMessageBase = nMsgBase;
	m_nPgUp = nPgUp;
	m_nPgDn = nPgDn;
}

void LogManager::fnPageUpDown()
{
	if( m_pImage->IsFieldNew(m_nPgUp) )
		m_nCurrTopRow -= LOG_MESSAGE_ROWS;
	
	if( m_pImage->IsFieldNew(m_nPgDn) )
		m_nCurrTopRow += LOG_MESSAGE_ROWS;
	
	if( m_nCurrTopRow >= LOG_MESSAGE_COUNT )
		m_nCurrTopRow = LOG_MESSAGE_COUNT - 1;
	
	if( m_nCurrTopRow < 0 )
		m_nCurrTopRow = 0;
	
	if( m_nCurrTopRow == 0 )
		m_pImage->GetField(m_nPgUp)->SetEnabled(false);
	else
		m_pImage->GetField(m_nPgUp)->SetEnabled(true);
	
	if( (m_nCurrTopRow + LOG_MESSAGE_ROWS) >= LOG_MESSAGE_COUNT )
		m_pImage->GetField(m_nPgDn)->SetEnabled(false);
	else
		m_pImage->GetField(m_nPgDn)->SetEnabled(true);
}

void LogManager::fnAddMessage( CString csaNewMsg )
{
	COleDateTime clTemp = COleDateTime::GetCurrentTime();

	CString csTime;
	csTime.Format("%04d/%02d/%02d %02d:%02d:%02d - %s", clTemp.GetYear(),clTemp.GetMonth(),clTemp.GetDay(),clTemp.GetHour(),clTemp.GetMinute(),clTemp.GetSecond(),csaNewMsg);

	++m_nCurrIndex;

	if(m_nCurrIndex >= LOG_MESSAGE_COUNT)
		m_nCurrIndex = 0;

	m_csMsgArray[m_nCurrIndex] = csTime;
}

void LogManager::fnDisplayMessages()
{
	int iRow = 0;
	int nIndex = m_nCurrIndex - m_nCurrTopRow;

	if(nIndex < 0)
		nIndex = LOG_MESSAGE_COUNT + nIndex;

	for( iRow = 0; iRow < LOG_MESSAGE_ROWS; iRow++ )
	{
		if(nIndex < 0)
			nIndex = LOG_MESSAGE_COUNT - 1;

		if( m_csMsgArray[nIndex].IsEmpty() )
			break;

		if( (m_nCurrTopRow + iRow) >=  LOG_MESSAGE_COUNT)
			break;

		m_pImage->GetField(m_nMessageBase + iRow)->SetValueString( m_csMsgArray[nIndex] );
		m_pImage->GetField(m_nMessageBase + iRow)->SetNotManual();

		nIndex--;
	}

	while(iRow < LOG_MESSAGE_ROWS)
	{
		m_pImage->GetField(m_nMessageBase + iRow)->SetBlankState();
		iRow++;
	}	
}
#define FTP_NOOFRETRY		2
#define FTP_RETRYDELAY		10
#define FTP_BUFFERSIZE		4096

CFTPAutomation::CFTPAutomation()
{
	m_pFtpConnection = NULL;
	m_pInternetSession = NULL;

	m_szError = "";
	m_szCurrentDirectory = "";
}

CFTPAutomation::~CFTPAutomation()
{
	Disconnect();
}

bool CFTPAutomation::Connect(CString sFTPServer, CString sUserName, CString sPassword, int nFTPPort, bool bPassiveMode)
{
	CString strObject;
	INTERNET_PORT nPort;
	DWORD dwServiceType;

	// if there's already a connection open, close it first
	if (m_pFtpConnection != NULL)
	{
		m_pFtpConnection->Close();
		delete m_pFtpConnection;
		m_pFtpConnection = NULL;
	}

	if (m_pInternetSession != NULL)
	{
		m_pInternetSession->Close();
		delete m_pInternetSession;
		m_pInternetSession = NULL;
	}

	m_pInternetSession = new CInternetSession("TaskAutomation", 1, PRE_CONFIG_INTERNET_ACCESS);

	// Alert the user if the internet session could
	// not be started and close app
	if (!m_pInternetSession)
	{
		m_szError = "Cannot open an Internet Session. Please check your internet configuration.";
		return false;
	}

	CString strFtpURL = sFTPServer;

	if (!AfxParseURL(strFtpURL, dwServiceType, sFTPServer, strObject, nPort))
	{
		strFtpURL = _T("ftp://") +	strFtpURL;

		if (!AfxParseURL(strFtpURL, dwServiceType, sFTPServer, strObject, nPort))
		{
			m_szError = "Invalid FTP URL";
			return false;
		}
	}

	// Now open a FTP connection to the server
	if ((dwServiceType == INTERNET_SERVICE_FTP) && !sFTPServer.IsEmpty())
	{
		int nRetries = FTP_NOOFRETRY;
		while(nRetries > 0)
		{
			nRetries--;
			try
			{
				int nTimeout = 15 * (FTP_NOOFRETRY - nRetries + 1);
				m_pInternetSession->SetOption(INTERNET_OPTION_CONNECT_TIMEOUT, nTimeout * 1000);
				m_pInternetSession->SetOption(INTERNET_OPTION_RECEIVE_TIMEOUT, nTimeout * 1000);
				m_pInternetSession->SetOption(INTERNET_OPTION_SEND_TIMEOUT, nTimeout * 1000);
				m_pFtpConnection = m_pInternetSession->GetFtpConnection(sFTPServer, sUserName, sPassword, nFTPPort, bPassiveMode);
				nRetries = 0;
				m_szError.Empty();
			}
			catch (CInternetException* pEx)
			{
				DWORD dwLength = 255, dwError;
				
				CString strLastResponce;
				InternetGetLastResponseInfo(&dwError, strLastResponce.GetBuffer(dwLength), &dwLength);
				strLastResponce.ReleaseBuffer();
				strLastResponce.Remove('\n');
				strLastResponce.Remove('\r');

				if (!strLastResponce.IsEmpty())
					m_szError = strLastResponce;

				// catch errors from WinINet
				dwLength = 255;
				CString strInfo;
				if (pEx->GetErrorMessage(strInfo.GetBuffer(dwLength), dwLength))
				{
					strInfo.ReleaseBuffer();
					strInfo.Remove('\n');
					strInfo.Remove('\r');
					m_szError = strInfo;

					// show wait dialog
					if (nRetries > 0)
					{
						Sleep(FTP_RETRYDELAY * 1000);
					}
					else
					{
						if (!strLastResponce.IsEmpty())
						{
							strLastResponce += "\r\n";
						}
						
						strLastResponce += strInfo;
						m_szError = strLastResponce;
					}
				}
				else
				{
					m_szError = "An exception occurred when attempting to create an FTP connection. Please check your internet configuration and make sure your environment is set up with WININET.DLL in the path.";
					nRetries = 0;
				}
				pEx->Delete();

				m_pFtpConnection = NULL;
			}
		}
	}
	else
	{
		m_szError = "Invalid FTP URL.";
	}

	if(m_pFtpConnection)
	{
		m_pFtpConnection->GetCurrentDirectory(m_szCurrentDirectory);
	}
	
	return (m_pFtpConnection != NULL);
}

bool CFTPAutomation::Disconnect()
{
	// clean up any objects that are still lying around
	if (m_pFtpConnection != NULL)
	{
		m_pFtpConnection->Close();
		delete m_pFtpConnection;
		m_pFtpConnection = NULL;
	}

	if (m_pInternetSession != NULL)
	{
		m_pInternetSession->Close();
		delete m_pInternetSession;
		m_pInternetSession = NULL;
	}

	return true;
}


bool CFTPAutomation::GetFiles(CString sSrcFile, CString sDstFolder)
{
	if (m_pFtpConnection == NULL)
		return false;

	CString strPrevDirectory;

	// remember current directory
	if (!m_pFtpConnection->GetCurrentDirectory(strPrevDirectory))
	{
		return FALSE;
	}

	CStringArray strFileNames;
	CString strFileName, strPath;
	strPath = m_szCurrentDirectory + "/" + sSrcFile;
	FillRemoteFileList(strPath,strFileNames);

	int nPos = strPath.ReverseFind('/');
	if (nPos != -1)
	{
		strPath = strPath.Left(nPos);
	}	

	bool bResult = true;

	for (int iCount(0); iCount < strFileNames.GetSize(); ++iCount)
	{
		strFileName = strFileNames[iCount];

		nPos = strFileName.ReverseFind('/');
		if (nPos != -1)
			strFileName = strFileName.Mid(nPos+1);
	
		if(!DownloadFile(strFileName, sDstFolder + "\\" + strFileName, strPath))
		{
			bResult = false;
		}
	}

	// go back to parent directory
	if (!m_pFtpConnection->SetCurrentDirectory(strPrevDirectory))
	{
		//m_szError.Format("Directory '%s' not found.", strPrevDirectory); 
		return false;
	}

	return bResult;
}

bool CFTPAutomation::ChangeFolder(int iOption, CString sFolderName)
{
	if (m_pFtpConnection == NULL)
		return false;

	if(iOption == CD_Root)
	{
		m_szCurrentDirectory = "/";
	}
	else if(iOption == CD_OneLevelUp)
	{
		int nPos = m_szCurrentDirectory.ReverseFind('/');
		if (nPos != -1)
		{
			m_szCurrentDirectory = m_szCurrentDirectory.Left(nPos);
		}
	}
	else if(iOption == CD_AddToCurrent)
	{
		sFolderName.TrimLeft("/");
		sFolderName = "/" + sFolderName;
		m_szCurrentDirectory += sFolderName;
	}
	else if(iOption == CD_GoToSpecified)
	{
		m_szCurrentDirectory = sFolderName;
	}

	if(!m_pFtpConnection->SetCurrentDirectory(m_szCurrentDirectory))
	{
		m_szError.Format("Directory '%s' not found.", m_szCurrentDirectory); 
		return false;
	}

	return true;
}


bool CFTPAutomation::DownloadFile(CString &csSrcFile, CString &csDstFile, CString csDirectory)
{
	try
	{
		// only a directory name was specified
		if (csDstFile.Right(1) == "\\")
		{
			// try to create directory structure
			CreateLocalDirectory(csDstFile);
			return true;
		}

		// split folder and file (if complete path is given)
		CString csCurrDirectory("");
		int nPos = csSrcFile.ReverseFind('/');
		if (nPos != -1)
		{
			csDirectory = csSrcFile.Left(nPos+1);
			csSrcFile = csSrcFile.Mid(nPos+1);
		}

		// set current directory
		if (!m_pFtpConnection->SetCurrentDirectory(csDirectory))
		{
			m_szError.Format("Directory '%s' not found.", csDirectory); 
			return false; 
		}

		// open destination file
		CFile m_File;
		if (m_File.Open(csDstFile, CFile::modeCreate | CFile::modeWrite, NULL) == FALSE)
		{
			m_szError.Format("Can not create file %s", csDstFile); 
			return false;
		}
		
		CInternetFile* pInternetFile = m_pFtpConnection->OpenFile(csSrcFile, GENERIC_READ);
		if (!pInternetFile)
		{
			m_szError.Format("Can not open file %s", csSrcFile); 
			return false;
		}
		
		char buffer[FTP_BUFFERSIZE];
		unsigned int nRead = FTP_BUFFERSIZE;
		while (nRead == FTP_BUFFERSIZE)
		{
			// read remote data into buffer
			nRead = pInternetFile->Read(buffer, FTP_BUFFERSIZE);
			// write buffer to data file
			m_File.Write(buffer, nRead);
		}

		// close the file
		m_File.Close();
		// close internet file
		pInternetFile->Close();
		delete pInternetFile;

		return true;
	}
	catch (CInternetException* pEx)
	{
		// catch errors from WinINet
		DWORD dwLength = 255;
		CString strInfo;
		if (pEx->GetErrorMessage(strInfo.GetBuffer(dwLength), dwLength))
		{
			strInfo.ReleaseBuffer();
			strInfo.Remove('\n');
			strInfo.Remove('\r');
			m_szError = strInfo;
		}
		else
		{
			m_szError = "An exception occurred when downloading the file.";
		}
		return false;
	}
}

BOOL CFTPAutomation::CreateLocalDirectory(CString csDirectory)
{
	CString strResult = csDirectory;
	
	CString strDir;
	BOOL bResult;
	// create directory structure one part at a time
	while (strResult != "")
	{
		strDir += strResult.Left(strResult.Find("\\")+1);
		strResult = strResult.Mid(strResult.Find("\\")+1);
		bResult = CreateDirectory(strDir, 0);
	}
	return bResult;
}


void CFTPAutomation::FillRemoteFileList(CString csPath, CStringArray &strFileNameArray)
{
	int nPos = csPath.Find("*");
	if (nPos != -1)
	{
		CString csCriteria(csPath);
		nPos = csCriteria.ReverseFind('/');
		if (nPos != -1)
		{
			csPath = csPath.Left(nPos);
			csCriteria = csCriteria.Mid(nPos+1);
		}	
		
		CFtpFileFind ftpFind(m_pFtpConnection);
		CString strPrevDirectory;

		// remember current directory
		if (!m_pFtpConnection->GetCurrentDirectory(strPrevDirectory))
		{
			return;
		}
		
		// go to requested directory
		if (!m_pFtpConnection->SetCurrentDirectory(csPath))
		{
			return;
		}

		BOOL bContinue = ftpFind.FindFile(csCriteria);
		if (!bContinue)
		{
			// the directory is empty; just close up and return.
			ftpFind.Close();

			// go back to parent directory
			m_pFtpConnection->SetCurrentDirectory(!strPrevDirectory.IsEmpty() ? strPrevDirectory : "/");

			return;
		}

		// add all files in csPath
		while (bContinue)
		{
			bContinue = ftpFind.FindNextFile();

			if (ftpFind.IsDots())
				continue;
			
			// is selected item a directory ?
			if (!ftpFind.IsDirectory())
			{
				CString strFileName = ftpFind.GetFileName();
				strFileNameArray.Add(strFileName);
			}
		}
		ftpFind.Close();

		// go back to parent directory
		m_pFtpConnection->SetCurrentDirectory(!strPrevDirectory.IsEmpty() ? strPrevDirectory : "/");
	}
	else
	{
		strFileNameArray.Add(csPath);
	}
}

SequenceCheck* SequenceCheck::single = NULL;

SequenceCheck* SequenceCheck::getInstance()
{
    if(!single)
    {
        single = new SequenceCheck();
        return single;
    }
    else
    {
        return single;
    }
}

void SequenceCheck::SetImage(BaseImage *pImage)
{
	m_pImage = pImage;
}

void SequenceCheck::SetFile(string strcurrfile)
{
	m_strcurrfile = strcurrfile;
}

void SequenceCheck::CheckSequence(int applID, __int64 sequence)
{
	if( m_pImage->GetField(e_SeqCheck)->GetValueInt() == 0 )
		return;

	SequenceMap::iterator itrSeq = ChannelSeqMap.find(applID);

	if( itrSeq != ChannelSeqMap.end() )
	{
		__int64 nLastSeq =  itrSeq->second;

		if(sequence != (itrSeq->second+1))
		{
			SeqGapInfo objSeqGap;
			objSeqGap.nAppId = applID;
			objSeqGap.nSecurityId = 0;
			objSeqGap.strFileName = m_strcurrfile;
			objSeqGap.nSeqLast = itrSeq->second;
			objSeqGap.nSeqNext = sequence;
			vSeqGaps.push_back(objSeqGap);
			DisplaySeqGap();
			m_pImage->RequestExternalUpdate();

			CString strLog;
			strLog.Format("Sequence Gap found for channel %d in %s, from %ld to %ld", 
				objSeqGap.nAppId, objSeqGap.strFileName.c_str(), objSeqGap.nSeqLast, objSeqGap.nSeqNext);
			MessageUtil::getInstance()->LogToFile(strLog.GetString());
		}
	}
	
	ChannelSeqMap[applID] = sequence;
}

void SequenceCheck::CheckSequenceSec(int securityId, __int64 sequence)
{
	if( m_pImage->GetField(e_SeqCheck)->GetValueInt() == 0 )
		return;

	SequenceMap::iterator itrSeq = ContractSeqMap.find(securityId);

	if( itrSeq != ContractSeqMap.end() )
	{
		__int64 nLastSeq =  itrSeq->second;

		if(sequence != (itrSeq->second + 1))
		{
			SeqGapInfo objSeqGap;
			objSeqGap.nAppId = 0;
			objSeqGap.nSecurityId = securityId;
			objSeqGap.strFileName = m_strcurrfile;
			objSeqGap.nSeqLast = itrSeq->second;
			objSeqGap.nSeqNext = sequence;
			vSeqGaps.push_back(objSeqGap);
			DisplaySeqGap();
			m_pImage->RequestExternalUpdate();

			CString strLog;
			strLog.Format("Sequence Gap found for Security %d in %s, from %ld to %ld", 
				objSeqGap.nSecurityId, objSeqGap.strFileName.c_str(), objSeqGap.nSeqLast, objSeqGap.nSeqNext);
			MessageUtil::getInstance()->LogToFile(strLog.GetString());
		}
	}
	
	ContractSeqMap[securityId] = sequence;
}

void SequenceCheck::DisplaySeqGap()
{
	int iRow = 0;
	CString strTemp;
	CString strTemp1;
	CString strTemp2;

	for(int iIndex = 0; iIndex < vSeqGaps.size(); iIndex++, iRow++)
	{
		if( iRow >= 10 )
			break;

		SeqGapInfo &objSeqInfo = vSeqGaps[iIndex];

		strTemp1.Format("%ld", objSeqInfo.nSeqLast);
		strTemp2.Format("%ld", objSeqInfo.nSeqNext);

		strTemp.Format("%s - %s", strTemp1, strTemp2);

		m_pImage->GetField(e_Channel	+ iRow)->SetValueInt(objSeqInfo.nAppId);  
		m_pImage->GetField(e_Security	+ iRow)->SetValueInt(objSeqInfo.nSecurityId);  
		m_pImage->GetField(e_FileName	+ iRow)->SetValueString(objSeqInfo.strFileName.c_str());  
		m_pImage->GetField(e_Sequence   + iRow)->SetValueString(strTemp);  
	}

	while( iRow < 10 )
	{
		m_pImage->GetField(e_Channel	+ iRow)->SetBlankState(); 
		m_pImage->GetField(e_Security	+ iRow)->SetBlankState(); 
		m_pImage->GetField(e_FileName	+ iRow)->SetBlankState();  
		m_pImage->GetField(e_Sequence   + iRow)->SetBlankState();  
		iRow++;
	}
}

void SequenceCheck::Clear()
{
	vSeqGaps.clear();
	ChannelSeqMap.clear();
	ContractSeqMap.clear();
	DisplaySeqGap();
	m_pImage->RequestExternalUpdate();
}

void SequenceCheck::GenerateReport()
{
	if( vSeqGaps.size() <= 0 )
	{
		m_pImage->GetField(e_FileField)->SetEnabled(FALSE);
		return;
	}

	CString csFileName("C:\\SequenceCheck.xls");
	CFile file;
	
	if(file.Open(csFileName,CFile::modeCreate|CFile::modeWrite))
	{
		CString csWriteData;

		csWriteData.Format("Channel\tSecurity Id\tFileName\tSequence Gap\n\n");
		file.Write(csWriteData.GetBuffer(0),csWriteData.GetLength());

		SeqGapInfo objSeqGap;
		CString csChannel, csSecId;
		CString strTemp;
		CString strTemp1;
		CString strTemp2;
		
		for(int iDataRow = 0; iDataRow < vSeqGaps.size(); iDataRow++)		
		{
			objSeqGap = vSeqGaps[iDataRow];

			csChannel.Format("%d", objSeqGap.nAppId);
			csSecId.Format("%d", objSeqGap.nSecurityId);
			strTemp1.Format("%ld", objSeqGap.nSeqLast);
			strTemp2.Format("%ld", objSeqGap.nSeqNext);

			strTemp.Format("%s - %s", strTemp1, strTemp2);

			csWriteData.Format("%s\t%s\t%s\t%s\n", csChannel, csSecId, objSeqGap.strFileName.c_str(), strTemp);
			file.Write(csWriteData.GetBuffer(0),csWriteData.GetLength());
		}	

		csWriteData.ReleaseBuffer();
		file.Close();				
		 
		FileField * fld = (FileField*)m_pImage->GetField(e_FileField);
		fld->SetFileBinary(csFileName);
		m_pImage->GetField(e_FileField)->SetEnabled(TRUE);
		file.Remove(csFileName);
	}
}