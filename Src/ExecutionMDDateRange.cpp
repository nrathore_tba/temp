
#include <stdafx.h> 
#include "ExecutionMDDateRange.hpp"
#include <MailAutomation/MailAutomation.h>
#include "afxsock.h"

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(ExecutionMDDateRange)

ExecutionMDDateRange::ExecutionMDDateRange()
{	
	m_nTotalFiles = 0;
	m_csOutputPath = _T("");
	m_csUserFTPAccountName = _T("");
	m_csDestinationPath = _T("");
	m_csSourcepath = _T("");
	m_csUserName = _T("");
	m_csFTPUserName = _T("");
}

ExecutionMDDateRange::~ExecutionMDDateRange()
{
}

bool ExecutionMDDateRange::InitData()
{
	cout<<"Start InitData()"<<endl;
	m_clNextScheduleDtTime = COleDateTime::GetCurrentTime();
		
	GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));

	m_curTopRow= 0;
	for(m_numofRows = 0; GetField(e_SubscrID + m_numofRows); m_numofRows++);

	m_nCurTopRow= 0;
	for(m_nNumOfRows = 0; GetField(e_fileName + m_nNumOfRows); m_nNumOfRows++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","UAT-RDS");
	m_csSourcepath = GetField(e_sourcePath)->GetValueString();
	m_csDestinationPath = GetField(e_destPath)->GetDisplayString();
	csServerName = strServerName.get_value().c_str();
	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");

	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	if( !IsRestoreUpdate() )
	{
		BF_Reg_Key_Ptr pKey1(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\CME"));

		int iFTPLocation;
		RegDWORD strServerName(pKey1, "S3FTPLocation",0);
		iFTPLocation = strServerName.get_value();
		GetField(e_destPath)->SetValueInt( iFTPLocation );
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();

	cout<<"End InitData"<<endl;

	m_bIsThreadRunning = false;
	m_bIsTrheadCompleted = true;
	m_enmOperation = OP_None;
	return true;
}
UINT OTOEODExecution(void *pVoid)
{
	ExecutionMDDateRange *pOTOMDExecution = (ExecutionMDDateRange*)pVoid;
	
	pOTOMDExecution->m_bIsThreadRunning = true;
	pOTOMDExecution->m_bIsTrheadCompleted = false;
	//pOTOMDExecution->AddMessage("MD OTO Order processing starts...");
	
	pOTOMDExecution->LoadSubscription();
	for(int nIndex = 0; nIndex < pOTOMDExecution->m_clMDSubV.size(); nIndex++)
	{
		MDSubscriptionDtl &clMDOTO = pOTOMDExecution->m_clMDSubV[nIndex];
		if(!clMDOTO.bOrderProcessed)
		{
			pOTOMDExecution->m_csFTPUserName = clMDOTO.stFTPUserName;
			pOTOMDExecution->UploadUserProduct(clMDOTO.stFTPUserName);
		}
	}

	pOTOMDExecution->m_enmOperation = OP_ProcessCompleted;

	pOTOMDExecution->m_bIsThreadRunning = false;
	pOTOMDExecution->LoadSubscription();
	//pOTOMDExecution->AddMessage("MD OTO Order processing finished...");
	pOTOMDExecution->RequestExternalUpdate();

	return 0;
}
void ExecutionMDDateRange::MDProcessingScheduler()
{
	COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
	long lRecalcTime = 0;

	if( dtCurrentTime >= m_clNextScheduleDtTime)
	{
		// Process the OTO order	
		//LoadSubscription();
		//LoadAllUserDetails();
		GetField(e_NextTime)->SetTitle("OTO EOD Order Processing");
		//m_clNextScheduleDtTime = COleDateTime::GetCurrentTime();
		//m_clNextScheduleDtTime += COleDateTimeSpan(0,0,2,0);
		m_enmOperation = OP_Process;
	}
	else if( dtCurrentTime < m_clNextScheduleDtTime )
	{
		COleDateTimeSpan dtSpan = m_clNextScheduleDtTime - dtCurrentTime;
		lRecalcTime = dtSpan.GetTotalSeconds();

		CString csTimer,csTmp;

		int iDays =	dtSpan.GetDays();
		int iLeftTotalHr = dtSpan.GetHours();
		int iLeftTotalMinute = dtSpan.GetMinutes();
		int iLeftTotalSecond = dtSpan.GetSeconds();

		if( iDays > 0 )
		{
			csTmp.Format("%d Day and ",iDays);					
			csTimer += csTmp;

			csTmp.Format("%02d:",iLeftTotalHr);					
			csTimer += csTmp;
		}
		else
		{
			if( iLeftTotalHr > 0 )
			{					
				csTmp.Format("%02d:",iLeftTotalHr);					
				csTimer += csTmp;
			}
		}

		csTmp.Format("%02d:",iLeftTotalMinute);					
		csTimer += csTmp;

		csTmp.Format("%02d",iLeftTotalSecond);
		csTimer += csTmp;				

		int iDate	= m_clNextScheduleDtTime.GetDay();
		int iMonth	= m_clNextScheduleDtTime.GetMonth();
		int iYear	= m_clNextScheduleDtTime.GetYear();
		int iHour	= m_clNextScheduleDtTime.GetHour();
		int iMinute = m_clNextScheduleDtTime.GetMinute();
		int iSecond = m_clNextScheduleDtTime.GetSecond();

		CString csStr;
		csStr.Format("Next EOD OTO Processing: %d-%d-%d %2d:%2d:%2d [Timer: %s]",iDate,iMonth,iYear,iHour,iMinute,iSecond, csTimer);

		GetField(e_NextTime)->SetTitle(csStr);
		GetField(e_NextTime)->SetTitleForeColor(ColorManager::eSignalPositiveLight);

		StartRecalcTimer(0, false);
	}
}
void ExecutionMDDateRange::Recalculate()
{
	cout<<"Start Recalculate()"<<endl;
	/*if(IsFieldNew(e_calendar))
	{
		m_csUserName = GetField(e_userName)->GetValueString();
		if(m_csUserName.IsEmpty())
		{
			GetField(e_Status)->SetValueString(_T("User name is required..."));
			return;
		}
		LoadUserFileDetails();
	}*/
	if(IsFieldNew(e_uploadFiles))
	{
		m_csUserName = GetField(e_userName)->GetValueString();
		if(m_csUserName.IsEmpty())
		{
			GetField(e_Status)->SetValueString(_T("User name is required..."));
			return;
		}
		LoadUserFileDetails();
		UploadAllFiles();
	}
	if(IsFieldNew(e_sourcePath))
	{
		m_csSourcepath = GetField(e_sourcePath)->GetValueString();
	}
	if(IsFieldNew(e_destPath))
	{
		m_csDestinationPath = GetField(e_destPath)->GetDisplayString();
	}
	if(IsFieldNew(e_getSubScription))
	{
		LoadSubscription();
	}
	if( m_bIsThreadRunning )
	{
		GetField(e_Status)->SetValueString("OTO EOD Order Processing...");
		GetField(e_getSubScription)->SetEnabled(false);
		GetField(e_uploadFiles)->SetEnabled(false);
	}
	else
	{
		if( !m_bIsThreadRunning && !m_bIsTrheadCompleted )
		{
			m_bIsTrheadCompleted = true;
		}
		if(m_enmOperation == OP_ProcessCompleted)
		{
			m_enmOperation = OP_None;
			m_clNextScheduleDtTime = COleDateTime::GetCurrentTime();
			m_clNextScheduleDtTime += COleDateTimeSpan(0,0,2,0);
			GetField(e_getSubScription)->SetEnabled(true);
			GetField(e_uploadFiles)->SetEnabled(true);
		}
		if(m_enmOperation == OP_Process)
		{
			AfxBeginThread(OTOEODExecution, this);
		}
		if( m_enmOperation == OP_None )
			MDProcessingScheduler( );
		if( m_enmOperation != OP_None )
			StartRecalcTimer(0, false);
	}
		
	fnPageUpDown();
	ManageUpDown();
	DisplaySubscription();
	DisplayDownloadFiles();
	cout<<"End Recalculate"<<endl;
}
void ExecutionMDDateRange::LoadSubscription()
{
	m_clMDSubV.clear(); 
	int nTotalSubscriber = 0;
	CString sql;	
	sql.Format("EXEC Proc_Beast_Get_Subscriptions 'MD', 1");

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		ASSERT(0);
		return;
	}
	int iCount(1);
	while(VARIANT_FALSE == set->adoEOF)
	{
		MDSubscriptionDtl clBlockSub;

		SETLONG(clBlockSub.iOrderId,		 set->Fields->Item[_variant_t("ORDER_ID")]->Value)
		SETSTR(	clBlockSub.stCUST_NAME,		 set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
		SETSTR(	clBlockSub.stCONTACT_EMAIL,	 set->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);	
		SETSTR(	clBlockSub.stFTP_PASSWORD,	 set->Fields->Item[_variant_t("FTPPassword")]->Value);
		SETSTR(	clBlockSub.stFTPUserName,	 set->Fields->Item[_variant_t("FTPUserName")]->Value);
		SETSTR(	clBlockSub.stALL_PRODUCTS,	 set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);	
		SETSTR(	clBlockSub.stPROD_CODE,		 set->Fields->Item[_variant_t("PROD_CODE")]->Value);	
		SETSTR(	clBlockSub.stEXCH_CODE,		 set->Fields->Item[_variant_t("Exchange")]->Value);
		SETSTR(	clBlockSub.csSubProductCode, set->Fields->Item[_variant_t("Sub_Product_Code")]->Value);
		SETBFDATE(clBlockSub.dtStartDate,	 set->Fields->Item[_variant_t("Startdate")]->Value);
		SETBFDATE(clBlockSub.dtEndDate,	     set->Fields->Item[_variant_t("EndDate")]->Value);
					
		m_clMDSubV.push_back( clBlockSub );
		iCount++;
		set->MoveNext();
	}
	set->Close();
	nTotalSubscriber = m_clMDSubV.size();
	if(nTotalSubscriber == 0 )
		GetField(e_Status)->SetValueString(_T("No subscription found..."));
	else
		GetField(e_Status)->SetValueString(_T("All subscription loaded..."));
	GetField(e_totalSubscriber)->SetValueInt(nTotalSubscriber);
}
void ExecutionMDDateRange::UploadUserProduct(CString sFTPUser)
{
	m_vtMDFileDetails.clear();
	for(int iIndex = 0; iIndex < m_clMDSubV.size(); iIndex++)
	{
		MDSubscriptionDtl &clMDOTO = m_clMDSubV[iIndex];
		if(clMDOTO.stFTPUserName == sFTPUser)
		{	
			LoadUploadFiles(iIndex);
		}
	}
	if(m_clMDSubV.size() > 0)
	{
		//CString sMessage = "";
		//sMessage.Format("Product Detail for User[%s]: %d Record(s) Found ..!!",sFTPUser, m_vecProductDetails.size());
		//m_MessageLogger.LogMessage(sMessage);
		//AddLogMessages(sMessage);
		//GetField(e_Status)->SetValueString(sMessage);
	}
	for(int iIndex = 0; iIndex < m_vtMDFileDetails.size(); iIndex++)
	{
		m_csS3Command.Format(_T("aws s3 cp %s --profile s3user %s"), GetSourcePath(iIndex), GetDestinationPath());
		int nRet = system(m_csS3Command);
		if( nRet == 0 )
		{
			GetField(e_Status)->SetValueString(_T("File uploaded successfully..."));
			m_vtMDFileDetails[iIndex].csFileStatus = _T("Uploaded successfully");
			CString sTemp = "";
			CString sTemp1 = "";
			for(int nIndex = 0; nIndex < m_clMDSubV.size(); nIndex++)
			{
				MDSubscriptionDtl &clMDOTO = m_clMDSubV[iIndex];
				if(clMDOTO.stFTPUserName == sFTPUser)
				{
					clMDOTO.bOrderProcessed = true;
					sTemp1.Format("%d",clMDOTO.iOrderId);
					sTemp += sTemp1 + ",";
				}
			}
			if(!sTemp.IsEmpty())
			{
				sTemp.SetAt(sTemp.GetLength()-1, ' ');
				sTemp.Trim();
				time_t     now = time(0);
				struct tm  tstruct;
				char       buf[80];
				tstruct = *localtime(&now);
				
				CString sQuery = "";
				CString sCurrentTime = "";

				strftime (buf,80,"%Y-%m-%d %H:%M:%S",&tstruct);
				sCurrentTime.Format("%s", buf);

				sQuery.Format("Update CME_Order set DeliveryDate = '%s' , OrderStatus = 5 where order_id in (%s)",sCurrentTime, sTemp );
				
				_RecordsetPtr set;
				if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sQuery, true)))
				{
					CString sErrorMessage = _T("");
					SetErrorMessage(ErrorSeverity::e_Error, sQuery);
					sErrorMessage.Format("Error in executing query (%s)", sQuery);
					//m_MessageLogger.LogMessage(sErrorMessage);
					GetField(e_Status)->SetValueString(sErrorMessage);
					return;
				}
			}
		}
		else
		{
			CString csTemp;
			csTemp.Format(_T("Error while executing command %s "), m_csS3Command);
			GetField(e_Status)->SetValueString(_T(csTemp));
			m_vtMDFileDetails[iIndex].csFileStatus = _T("Uploading fail");
		}
	}
}
void ExecutionMDDateRange::LoadUserFileDetails()
{
	m_vtMDFileDetails.clear();
	BOOL bUserFound = FALSE;
	CString csOrderId(_T(""));
	for(int nIndex = 0; nIndex < m_clMDSubV.size(); nIndex++)
	{
		if(m_clMDSubV[nIndex].stFTPUserName == m_csUserName)
		{
			LoadUploadFiles(nIndex);
			csOrderId.Format((_T("%d,")),m_clMDSubV[nIndex].iOrderId);
			m_csOrderIDS = m_csOrderIDS + csOrderId;  
			bUserFound = TRUE;
		}
	}
	m_csOrderIDS.TrimRight(',');
	if(bUserFound == FALSE)
	{
		GetField(e_Status)->SetValueString(_T("Given user not found..."));
	}
}
void ExecutionMDDateRange::UploadAllFiles()
{
	for(int iIndex = 0; iIndex < m_vtMDFileDetails.size(); iIndex++)
	{
		m_csS3Command.Format(_T("aws s3 cp %s --profile s3user %s"), GetSourcePath(iIndex), GetDestinationPath());
		int nRet = system(m_csS3Command);
		if( nRet == 0 )
		{
			GetField(e_Status)->SetValueString(_T("File uploaded successfully..."));
			m_vtMDFileDetails[iIndex].csFileStatus = _T("Uploaded successfully");
		}
		else
		{
			CString csTemp;
			csTemp.Format(_T("Error while executing command %s "), m_csS3Command);
			GetField(e_Status)->SetValueString(_T(csTemp));
			m_vtMDFileDetails[iIndex].csFileStatus = _T("Uploading fail");
		}
	}
}
CString ExecutionMDDateRange::GetSourcePath(int iIndex)
{
	CString csFileDate, csFileName, csMonth, csDay, csYear;
	CString csOutputFilePath(_T(""));
	CString csFinalSourcePath(_T(""));
	csFileDate.Format(_T("%d%02d%02d"),m_vtMDFileDetails[iIndex].FileDate.GetYear(), m_vtMDFileDetails[iIndex].FileDate.GetMonth()+1, m_vtMDFileDetails[iIndex].FileDate.GetDate());
	csMonth.Format(_T("%02d"), m_vtMDFileDetails[iIndex].FileDate.GetMonth()+1);
	csDay.Format(_T("%02d"), m_vtMDFileDetails[iIndex].FileDate.GetDate());
	csYear.Format(_T("%d"),m_vtMDFileDetails[iIndex].FileDate.GetYear());
	csOutputFilePath = csOutputFilePath + m_vtMDFileDetails[iIndex].csExchange + _T("\\") + csYear + _T("\\") + csMonth + _T("\\");
	csOutputFilePath = csOutputFilePath + csFileDate + _T("\\") +  m_vtMDFileDetails[iIndex].csFileName;
	csFinalSourcePath.Format(_T("%s%s"),m_csSourcepath, csOutputFilePath);
	return csFinalSourcePath;
}
CString ExecutionMDDateRange::GetDestinationPath()
{
	CString csFinalDestPath(_T(""));
	csFinalDestPath.Format(_T("%s%s/"),m_csDestinationPath, m_csUserFTPAccountName);
	return csFinalDestPath;
}
void ExecutionMDDateRange::LoadUploadFiles(int iBtnIndex)
{
	//m_vtMDFileDetails.clear();
	BFDate  bfFromDt;
	BFDate  bfToDt;
	CString csExchange;
	CString csProduct;

	if( m_clMDSubV.size() > 0)
	{
		csExchange  = m_clMDSubV[iBtnIndex].stEXCH_CODE;
		csProduct   = m_clMDSubV[iBtnIndex].csSubProductCode;

		bfFromDt = m_clMDSubV[iBtnIndex].dtStartDate;
		bfToDt	 = m_clMDSubV[iBtnIndex].dtEndDate;
	}

	if( bfFromDt > bfToDt )
	{
		 GetField(e_Status)->SetValueString("Date range is not correct: Please check... ");
		 return;
	}

	CString csFromDt, csToDt;
	csFromDt.Format("%d-%02d-%02d",	bfFromDt.GetYear(), bfFromDt.GetMonth()+1, bfFromDt.GetDate());
	csToDt.Format("%d-%02d-%02d",	bfToDt.GetYear(), bfToDt.GetMonth()+1, bfToDt.GetDate());

	
	CString csSQL;
	csSQL.Format(_T("SELECT * FROM dbo.CME_Product_Calendar WHERE FileType = 'MD' AND Exchange = '%s' AND product = '%s' AND FileDate >= '%s' AND FileDate <= '%s' order by FileDate"),csExchange, csProduct, csFromDt,csToDt);
	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, csSQL, false)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSQL);
		ASSERT(0);
		return;
	}
	
	while (VARIANT_FALSE == set->adoEOF)
	{
		stMDFileDtl structMDDtl;

		SETSTR(structMDDtl.csFileName,		 set->Fields->Item[_variant_t("FileName")]->Value);
		SETBFDATE(structMDDtl.FileDate,	 set->Fields->Item[_variant_t("FileDate")]->Value);
		SETSTR(structMDDtl.csExchange,		 set->Fields->Item[_variant_t("Exchange")]->Value);	
		SETSTR(structMDDtl.csYear,		     set->Fields->Item[_variant_t("Year")]->Value);	
		SETSTR(structMDDtl.csMonth,		 set->Fields->Item[_variant_t("Month")]->Value);
		SETSTR(structMDDtl.csDay,		     set->Fields->Item[_variant_t("DAY")]->Value);	
		SETSTR(structMDDtl.csProduct,		 set->Fields->Item[_variant_t("Product")]->Value);	
		SETSTR(structMDDtl.csFutOpt,		 set->Fields->Item[_variant_t("FutOpt")]->Value);
		SETLONGLONG(structMDDtl.ulFileSize, set->Fields->Item[_variant_t("Size")]->Value);
		m_vtMDFileDetails.push_back(structMDDtl);

		set->MoveNext();
	}
	set->Close();
	CString csTotalRecord(_T("")); 
	csTotalRecord.Format(_T("%d Record Found"),m_nTotalFiles = m_vtMDFileDetails.size());
	GetField(e_Status)->SetValueString(csTotalRecord);
	GetField(e_totalFiles)->SetValueInt(m_nTotalFiles);
	if(m_nTotalFiles == 0)
	{
		GetField(e_Status)->SetValueString(_T("No record found"));
		return;
	}
}

void ExecutionMDDateRange::DisplayDownloadFiles()
{
	int iRow = 0;
	for(int iIndex = m_nCurTopRow; iIndex < m_vtMDFileDetails.size(); iIndex++, iRow++)
	{
		if( iRow >= m_nNumOfRows )
			break;

		stMDFileDtl  &ObjFileDetail = m_vtMDFileDetails[iIndex];

		GetField(e_fileName	    + iRow)->SetValueString(ObjFileDetail.csFileName);  
		GetField(e_fileDate	    + iRow)->SetValueDate(ObjFileDetail.FileDate);  
		GetField(e_prodCode	    + iRow)->SetValueString(ObjFileDetail.csProduct);
		GetField(e_exchangeCode	+ iRow)->SetValueString(ObjFileDetail.csExchange);  
		GetField(e_futOpt       + iRow)->SetValueString(ObjFileDetail.csFutOpt);
		GetField(e_fileSize	    + iRow)->SetValueInt(ObjFileDetail.ulFileSize);
		GetField(e_fileStatus   + iRow)->SetValueString(ObjFileDetail.csFileStatus);	
	}

	while( iRow < m_nNumOfRows )
	{
		GetField(e_fileName	    + iRow)->SetBlankState();  
		GetField(e_fileDate	    + iRow)->SetBlankState();
		GetField(e_prodCode	    + iRow)->SetBlankState(); 
		GetField(e_exchangeCode	+ iRow)->SetBlankState();   
		GetField(e_futOpt       + iRow)->SetBlankState(); 
		GetField(e_fileSize	    + iRow)->SetBlankState();
		GetField(e_fileStatus   + iRow)->SetBlankState();

		iRow++;
	}
}
void ExecutionMDDateRange::fnPageUpDown()
{
	int iTotalRecord = m_clMDSubV.size();

	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpBase,e_PgDnBase,iTotalRecord,m_numofRows,m_curTopRow);	
}
void ExecutionMDDateRange::ManageUpDown()
{
	int iTotalRecord = m_vtMDFileDetails.size();

	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_pgUP, e_pgDN, iTotalRecord, m_nNumOfRows,m_nCurTopRow);	
}

void ExecutionMDDateRange::DisplaySubscription()
{	
	int iRow = 0;
	for(unsigned int iIndex = m_curTopRow; iIndex < m_clMDSubV.size(); iIndex++, iRow++)
	{
		if( iRow >= m_numofRows )
			break;

		MDSubscriptionDtl &clMDSub= m_clMDSubV[iIndex];

		GetField(e_SubscrID		+ iRow)->SetValueInt(clMDSub.iOrderId);  
		GetField(e_CUST_NAME	+ iRow)->SetValueString(clMDSub.stCUST_NAME);  
		GetField(e_DEST_ACCOUNT_FTP_PASSWORD + iRow)->SetValueString(clMDSub.stFTP_PASSWORD);  
		GetField(e_ALL_PRODUCTS + iRow)->SetValueString(clMDSub.stALL_PRODUCTS);
		GetField(e_PROD_CODE	+ iRow)->SetValueString(clMDSub.stPROD_CODE);
		GetField(e_MD_CODE		+ iRow)->SetValueString(clMDSub.stFTPUserName); 
		GetField(e_EXCH_CODE	+ iRow)->SetValueString(clMDSub.stEXCH_CODE);
		GetField(e_subProductCode	+ iRow)->SetValueString(clMDSub.csSubProductCode);
		GetField(e_StartDate	+ iRow)->SetValueDate(clMDSub.dtStartDate);
		GetField(e_EndDate  	+ iRow)->SetValueDate(clMDSub.dtEndDate);
	}

	while( iRow < m_numofRows )
	{
		GetField(e_SubscrID		+ iRow)->SetBlankState();  
		GetField(e_CUST_NAME	+ iRow)->SetBlankState();  
		GetField(e_DEST_ACCOUNT_FTP_PASSWORD + iRow)->SetBlankState();  
		GetField(e_ALL_PRODUCTS + iRow)->SetBlankState();  
		GetField(e_PROD_CODE	+ iRow)->SetBlankState();  
		GetField(e_MD_CODE		+ iRow)->SetBlankState(); 
		GetField(e_EXCH_CODE	+ iRow)->SetBlankState();
		GetField(e_subProductCode	+ iRow)->SetBlankState();
		GetField(e_StartDate	+ iRow)->SetBlankState();
		GetField(e_EndDate  	+ iRow)->SetBlankState();
		iRow++;
	}
}

void MessageLog::LogMessage(string strMessage)
{
	HWND hwnd = NULL;
	const SECURITY_ATTRIBUTES *psa = NULL;
	FILE *pLogFile = NULL;
	errno_t err;
	const char *plogFilePath = m_strLogFIlePath.c_str();
	if(IsFileExist(m_strLogFIlePath))
	{
		err = fopen_s(&pLogFile, plogFilePath ,"a+");
		if (err == 0)
		{
			COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
			CString csTimeStamp;
			csTimeStamp.Format("%d-%02d-%02d %02d:%02d:%02d",dtCurrentTime.GetYear(), dtCurrentTime.GetMonth(), dtCurrentTime.GetDay(), dtCurrentTime.GetHour(), dtCurrentTime.GetMinute(), dtCurrentTime.GetSecond());
			fprintf(pLogFile, "%s\t%s\n", csTimeStamp, strMessage.c_str());
			fclose(pLogFile);
		}
	}
	else
	{
		SHCreateDirectoryEx(hwnd, "D:\\OTO\\MD\\", psa);
		err = fopen_s(&pLogFile, plogFilePath ,"w");
		CString csFileHeader(_T("TimeStamp\t\t\tMessage"));
		if(err == 0)
		{
			fprintf(pLogFile, "%s\n", csFileHeader);
		}
		 fclose(pLogFile);
	}
}