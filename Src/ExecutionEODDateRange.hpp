
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <sys/stat.h>

#include "afxinet.h"

using namespace std;

BF_NAMESPACE_BEGIN

struct stEODFiles
{
	stEODFiles()
	{
		_strEDOFileName = "";
		_fpOutPutFile = NULL;
	}
	std::string _strEDOFileName;
	std::ofstream * _fpOutPutFile;
};

struct stEODFileDtl
{
	CString csFileName;
	BFDate  FileDate;
	CString csProduct;
	CString csFutOpt;
	CString csExchange;
	CString csYear;
	CString csMonth;
	CString csDay;
	CString csFileType;
	unsigned long long ulFileSize;
	stEODFileDtl()
	{
		csFileName = _T("");
		csProduct = _T("");
		csFutOpt = _T("");
		csExchange =  _T("");
		csYear = _T("");
		csMonth = _T("");
		csDay = _T("");
		csFileType = _T("");
		ulFileSize = 0;
	}
};
typedef std::vector<stEODFileDtl> EODFileDetail;
struct EODSubscriptionDtl
{
	EODSubscriptionDtl()
	{

		iFileCount = 0;
		iRowCount = 0;
		iOrderId  = 0;

		stCUST_NAME = "";
		stCONTACT_EMAIL = "";
		stDEST_ACCOUNT = "";
		stFTP_PASSWORD = "";
		stFTPUserName = "";
		stVENUE_CODE = "";
		stALL_PRODUCTS = "";
		stPROD_CODE = "";
		stEOD_CODE = "";
		stEXCH_CODE = "";
		stOutputFileName = "";
		csSubProductCode = "";
		bProcessed = FALSE;
	}

	int iOrderId;

	CString stCUST_NAME;
	CString stCONTACT_EMAIL;
	CString stDEST_ACCOUNT;
	CString stFTP_PASSWORD;
	CString stFTPUserName;
	CString stVENUE_CODE;
	CString stALL_PRODUCTS;
	CString stPROD_CODE;
	CString stEOD_CODE;
	CString stEXCH_CODE;
	CString stOutputFileName;
	CString csSubProductCode;
	BFDate  dtStartDate;
	BFDate  dtEndDate;
	BOOL bProcessed;
	
	int iFileCount;
	int iRowCount;
};

enum enmOperation
{
	OP_Download,
	OP_Process,
	OP_ProcessCompleted,
	OP_None
};
class MessageLog
{
public:
	MessageLog()
	{
		m_strLogFIlePath = "D:\\OTO\\EOD\\EODLogFile.txt";
	}
	~MessageLog(){}
	void LogMessage(string strLogMessage);
	inline bool IsFileExist(const string& name)
	{
		struct stat buffer;   
		return (stat (name.c_str(), &buffer) == 0); 
	}
private:
	string m_strLogFIlePath;
};
typedef std::vector<EODSubscriptionDtl> EODSubscriptionDtlV;
class ExecutionEODDateRange : public BaseImage
{
public:
	//Threading 
	bool m_bIsTrheadCompleted;
	bool m_bIsThreadRunning;
	enmOperation m_enmOperation;
	//Resource IDs
	enum FieldIDs
	{
		e_sourcePath = 1,
		e_destPath = 2,
		e_calendarDetail = 3,
		e_userName = 4,
		e_uploadFiles = 5,
		e_exchangeList = 6,
		e_product= 7,
		e_Timer = 8,
		e_getSubScription = 11,
		e_Status = 12,
		e_dtFromDate	= 58,
		e_dtToDate		= 59,
		e_Year		= 201,
		e_Month		= 202,
		e_Day		= 216,
		e_LogMsg = 100,
		e_SubscrID = 1000,
		e_CUST_NAME = 1050,
		//e_CONTACT_EMAIL = 1100,
		e_DEST_ACCOUNT_FTP_PASSWORD = 1150,
		e_VENUE_CODE = 1200,
		e_ALL_PRODUCTS = 1250,
		e_PROD_CODE = 1300,
		e_EOD_CODE	= 1350,
		e_EXCH_CODE = 1400,
		e_OutputFileName = 1450,
		e_PgDnBase = 90,
		e_PgUpBase = 91,
		e_startDate = 1700,
		e_endDate = 1800,
		e_FileType = 900,
		e_SubProductCode = 1900,
		//Calander Details
		e_pgUP = 93,
		e_pgDN =92,
		e_fileName = 300,
		e_fileDate = 400,
		e_prodCode = 500,
		e_exchangeCode = 600,
		e_futOpt = 700,
		e_fileSize = 800
	};
	//Threading Operation
	int m_nRecordIndex; //To access in thread
	CWinThread* m_pThreads;
	ExecutionEODDateRange();
	~ExecutionEODDateRange();	
	bool InitData();		
	void Recalculate();
	void LoadSubscription();
	void fnPageUpDown();
	void ManageUpDown();
	void DisplaySubscription();
	void DisplayDownloadFiles();
	void uploadSinglefile();
	void LoadUploadFiles(int iBtnIndex);
	CString GetSourcePath(int iIndex);
	CString GetS3SourcePath(int iIndex);
	CString GetLocalDirectory();
	CString GetDestinationPath();
	void ReadEODFiles();
	void LoadUserDetails();
	void LoadAllFiles(int iIndex);
	void UploadZip();
	CString GetCurrentTime();
	void SetTotalFiles();
	void ChangeOrderStatus();
	void FileCopyS3ToLocal();
	void CreateZipFile();
	void EODProcessingScheduler();
	void LoadAllUserDetails();
	//Message
	void AddMessage(CString csaNewMsg);
	void DisplayMsg();
	//
	void InsertDeliveryReport(CString csPrdouct, CString csDestAccount, CString csFileName, unsigned long &lFileSize, CString &csAwscmd );
	void InsertErroReport(CString csPrdouct, CString csDestAccount, CString csFileName, CString csExchnage, CString &csErrorCmd );
	void FnSendEmail(int iType, int iRowCount);
	CString m_sMailContent;
	unsigned long  GetFileSize(std::string const &path) 
	{
		WIN32_FIND_DATA data;
		HANDLE h = FindFirstFile(path.c_str(), &data);
		if (h == INVALID_HANDLE_VALUE)
			return -1;

		FindClose(h);

		return (data.nFileSizeLow | data.nFileSizeHigh << 32);
	}
	const std::string currentDateTime() 
	{
		time_t     now = time(0);
		struct tm  tstruct;
		char       buf[80];
		tstruct = *localtime(&now);
		strftime(buf, sizeof(buf), "%Y%m%d", &tstruct);
		return buf;
	}
	
private:
	bool m_bIsProcess;
	EODFileDetail m_vtEODFileDetails;
	EODSubscriptionDtlV	m_clEODSubV;
	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;

	//date field
	CString m_csYear;
	CString m_csMonth;
	CString m_csDay;

	int m_numofRows;
	int m_nNumOfRows;
	int m_curTopRow;
	int m_nCurTopRow;
	int m_nTotalFiles;
	
	CString m_csS3Command;
	CString m_csOutputPath;
	CString m_csSourcePath;
	CString m_csDestinationPath;
	CString m_csFtpUserName;
	CString m_csFromDate;
	CString m_csToDate;
	CString m_csUserName;
	CString m_csZipUploadPath;
	//CString m_csOrderIDS;
	BFDate  m_bfFromDt;
	BFDate  m_bfToDt;
	int m_nFileCounter;
	int m_nOrederId;
	COleDateTime m_clNextScheduleDtTime;

	MessageLog m_MessageLogger;
};

BF_NAMESPACE_END
