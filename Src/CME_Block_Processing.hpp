#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <shared/MDDDataHandler.hpp>

using namespace std;

const string __strHeader = "Trade Date,Trade Time,Report Time,Contract Symbol,Product Code,Product Type,Description,Market Sector,Asset Class,Contract Year,Contract Month,Strike Price,Put/Call,Exchange Code,Trade Price,Trade Quantity,Trade Source,Spread Type,Spread Description,Contract Symbol 2,Product Code 2,Asset Class 2,Market Sector 2,Description 2,Product Type 2,Contract Year 2,Contract Month 2,Strike Price 2,Put/Call 2,Exchange Code 2,Trade Price 2,Trade Quantity 2,Contract Symbol 3,Product Code 3,Asset Class 3,Market Sector 3,Description 3,Product Type 3,Contract Year 3,Contract Month 3,Strike Price 3,Put/Call 3,Exchange Code 3,Contract Symbol 4,Product Code 4,Asset Class 4,Market Sector 4,Description 4,Product Type 4,Contract Year 4,Contract Month 4,Strike Price 4,Put/Call 4,Exchange Code 4,\n";

#define MAX_ROWS 2500

BF_NAMESPACE_BEGIN

// 54 columns..
enum ColumnName
{
	enm_TRADE_DATE,
	enm_TRADE_TIME,
	enm_CALL_TIME,
	enm_CONTRACT_SYMBOL,
	enm_TICKER_SYMBOL,
	enm_FOI_SYMBOL_IND,
	enm_TICKER_SYMBOL_DESCRIPTION,
	enm_MARKET_SECTOR,
	enm_ASSET_CLASS,
	enm_CONTRACT_YEAR,
	enm_CONTRACT_MONTH,
	enm_STRIKE_PRICE,
	enm_PUT_CALL,
	enm_EXCH_CODE,
	enm_TRADE_PRICE,
	enm_TRADE_QUANTITY,
	enm_SOURCE,
	enm_SPREAD_TYPE,
	enm_SPREAD_DESCRIPTION,
	enm_CONTRACT_SYMBOL_2,
	enm_TICKER_SYMBOL_2,
	enm_TICKER_SYMBOL_DESCRIPTION_2,
	enm_MARKET_SECTOR_2,
	enm_ASSET_CLASS_2,
	enm_FOI_SYMBOL_IND_2,
	enm_CONTRACT_YEAR_2,
	enm_CONTRACT_MONTH_2,
	enm_STRIKE_PRICE_2,
	enm_PUT_CALL_2,
	enm_EXCH_CODE_2,
	enm_TRADE_PRICE_2,
	enm_TRADE_QUANTITY_2,
	enm_CONTRACT_SYMBOL_3,
	enm_TICKER_SYMBOL_3,
	enm_TICKER_SYMBOL_DESCRIPTION_3,
	enm_MARKET_SECTOR_3,
	enm_ASSET_CLASS_3,
	enm_FOI_SYMBOL_IND_3,
	enm_CONTRACT_YEAR_3,
	enm_CONTRACT_MONTH_3,
	enm_STRIKE_PRICE_3,
	enm_PUT_CALL_3,
	enm_EXCH_CODE_3,
	enm_CONTRACT_SYMBOL_4,
	enm_TICKER_SYMBOL_4,
	enm_TICKER_SYMBOL_DESCRIPTION_4,
	enm_MARKET_SECTOR_4,
	enm_ASSET_CLASS_4,
	enm_FOI_SYMBOL_IND_4,
	enm_CONTRACT_YEAR_4,
	enm_CONTRACT_MONTH_4,
	enm_STRIKE_PRICE_4,
	enm_PUT_CALL_4,
	enm_EXCH_CODE_4,
};

typedef std::map<CString, bool> PathList;

class CME_Block_Processing : public BaseImage
{
public:

	CME_Block_Processing();
	~CME_Block_Processing();

	bool InitData();
	void Recalculate();

	enum FieldIDs
	{
		e_Message	= 61,
		e_StartTime	= 95,
		e_EndTime	= 96,

		e_strInputFile	= 1,
		e_strOutputFile = 2,

		e_btnParseFile	= 3,
		e_strStatus		= 4,

		e_listExchnage	= 5,
		e_strProduct	= 6,

		e_ParseYear		= 13,

		e_Uncompress	= 20,
	};

	//Parse block file automettically and create hierarchy wise files
	

	//Parse block file base on  Exchange Code
	//bool parseByExchange(string fileName, string outputPath, string keyExchange , bool parseAllProducts );
	
	//Parse block file base on  Product Code
	//bool parseByProduct(string fileName, string outputPath, string keyProduct);

	//Split string by delimiter and store in vector 
	void CME_Block_Processing::splitString(CString cStr, string delimiter, vector<string> &result);

	//Return index of searckKey in given vector
	size_t indexofColumn(vector<string> v, string searchKey);

	//Base on hierarchy , create product folder and file
	string createProductFile(string outputPath, string strkey );

	//return List of Exchange exist in given file
	//CString getExchangeList(string fileName);
	
	//return List of Product exist in given file
	//set<string> getProductList(string fileName, string keyExchange);
	
	////Unzip the Folder and return csv filename
	//CString unzip(string sInputFile);

	//bool zip(string sInputFilePath , string sOutputPath);
	
	static const int m_niInputColumnCount = 55;
private:
	BaseField * fld_InputFile;
	BaseField * fld_OutputFile;
	BaseField * fld_Status;

	//set<string> setExchange;
	//set<string>::const_iterator itrSetExchange;

	CString fnCreatePath( CString csDate, CString csExchange );	
	void	fnUncompress( );
		
	PathList m_clExchangePathList;
	PathList m_clExchangeYearPathList;
	PathList m_clExchangeYearMonthPathList;
	PathList m_clExchangeYearMonthDatePathList;
public:
	bool	m_bIsThreadRunning;
	int		m_lTotalRead;

	void autoParseFile();
};
BF_NAMESPACE_END