// Copyright (C) 2002 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#include "stdafx.h"
#include "CME_BBO_TNS_DECODE_NEW.hpp"
#include <fields/ListField.hpp>
#include "BBO.h"
#include "TNS.h"
#include <time.h>

// Put this	after all the include statements

BF_NAMESPACE_USE


IMPLEMENT_IMAGE(CME_BBO_TNS_DECODE)	

time_t start,end;

static UINT StartProcess(LPVOID lparam)
{

	time (&start);

	CME_BBO_TNS_DECODE *pImage = (CME_BBO_TNS_DECODE*)lparam;

	pImage->GetField(e_gen_process)->SetEnabled(false);

	CString csFileType = pImage->GetField(e_BBO_OR_TNS)->GetDisplayString();
	pImage->GetField(e_output)->SetValueString("");
	pImage->GetField(e_filter_status)->SetValueString("");
	
	pImage->RequestExternalUpdate();

	if(csFileType == "BBO")
	{
		CBBO _objBBO;
		_objBBO.SetImage(pImage);
		_objBBO.setProcessParameters();
		_objBBO.Process();
	}
	else 
	{	
	
		CTNS _ctns;
		_ctns.SetImage(pImage);
		_ctns.setProcessParameters();
		_ctns.Process();

	}

	time (&end);
	double dif = difftime (end,start);
	char buff[100];
	sprintf (buff,"\r\n Elasped time is %.2lf seconds.", dif );
	pImage->GetField(e_filter_status)->SetValueString("DONE !!!");
	CString strOutput = pImage->GetField(e_output)->GetValueString();
	strOutput += buff;

	pImage->GetField(e_output)->SetValueString(strOutput);
	pImage->GetField(e_gen_process)->SetEnabled(true);
	pImage->RequestExternalUpdate();
	

	return true;
}

bool CME_BBO_TNS_DECODE::InitData()
{
	
	GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));

	m_curTopRow= 0;
	for(m_numofRows = 0; GetField(e_SubscrID + m_numofRows); m_numofRows++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","UAT-RDS");
	csServerName = strServerName.get_value().c_str();
	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");

	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	//---------------------------------------------

	COleDateTime dtToday = COleDateTime::GetCurrentTime();
	BFDate bfToday;
	bfToday.SetYearMonthDate(dtToday.GetYear(),dtToday.GetMonth() - 1, dtToday.GetDay() ); 

	GetField(e_DefaultDate)->SetValueDate(bfToday); 
	m_fldpFileExtensionList = (ListField*) GetField(e_FileExtList);	

	CString path = "\\\\powervault3\\CME_s3\\daily\\bbo\\globex";

	GetField(e_DefaultBasePath)->SetValueString(path);
	GetField(e_DirectoryInputPath)->SetValueString(path);
	GetField(e_DirectoryOutputPath)->SetValueString(path);

	return true;
} 

void CME_BBO_TNS_DECODE::Recalculate()
{
	if( IsFieldNew(e_DefaultBasePath) || IsFieldNew(e_DefaultDate)  || IsFieldNew(e_FileExtList) )
	{		
		GenerateFileName();		
	}

	if( IsFieldNew(e_Decompress) )
	{
		fnUncompressGZ( );
	}	

	if( IsFieldNew(e_Submit) )
	{
		bool bProcess = true;
		CString sFileName = GetField(e_DirectoryInputPath)->GetValueString();
		if( PathFileExists(sFileName) == FALSE )
		{
			// cheking gz file exist..
			GetField(e_Message)->SetValueString("File not exist...");
			bProcess = false;
		}

		sFileName.Replace(".gz",".txt");
		if( PathFileExists(sFileName) == FALSE )
		{
			// cheking gz file exist..
			if( bProcess == false )
			{
				GetField(e_Message)->SetValueString("gz File not exist...");
			}
			else
			{
				fnUncompressGZ( );				
				bProcess = true;
			}			
		}

		if( bProcess )
		{
			AfxBeginThread(StartProcess,(LPVOID)this);	
		}
	}



	if( IsFieldNew(e_Get_BLOCK_Subscription) )
	{
		LoadSubscription();	
	}	

	fnPageUpDown( );
	DisplaySubscription( );
}


void CME_BBO_TNS_DECODE::LoadSubscription()
{
	m_clBBOSubV.clear(); 
	
	CString sql;	
	sql.Format("SELECT TOP 100 CUST_NAME, CONTACT_EMAIL, DEST_ACCOUNT, FTP_PASSWORD, VENUE_CODE, ALL_PRODUCTS, PROD_CODE, BBO_CODE, EXCH_CODE FROM dbo.subscription_2015 where FILE_TYPE = 'BBO' and Active = 1");
	//change
	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		ASSERT(0);
		return;
	}
	/*
	CUST_NAME	ADDRESS_1	ADDRESS_2	CITY	REGION	COUNTRY	ZIP	CONTACT_PHONE	CONTACT_NAME	CONTACT_EMAIL	DEST_ACCOUNT	FTP_PASSWORD	EXPIRATION_DATE	ACTIVE	CREATE_TIMESTAMP	VENUE_CODE	FILE_TYPE	ALL_PRODUCTS	PROD_CODE	BBO_CODE	MD_CODE	BLOCK_CODE	EOD_CODE	TICK_CODE	EXCH_CODE
	Zoheb_Test	null	null	null	null	null	null	NULL	Zoheb Khalid	zoheb.khalid@cmegroup.com	inhouse_ftp	inhouse_ftp	1899-12-30 00:00:00.000	1	2014-12-05 11:42:00.000	null	BLOCK	0	OG	OG	OG	OG	OG	OG	XCEC
	*/
	int iCount(1);
	while(VARIANT_FALSE == set->adoEOF)
	{
		BBOSubscriptionDtl clBlockSub;

		clBlockSub.iOrderId   = iCount * 1000 ;
		clBlockSub.iBLOCKType = GetField( e_BBO_OR_TNS )->GetValueInt();

		SETSTR(	clBlockSub.stCUST_NAME,		set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
		SETSTR(	clBlockSub.stCONTACT_EMAIL,	set->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);	
		SETSTR(	clBlockSub.stDEST_ACCOUNT,	set->Fields->Item[_variant_t("DEST_ACCOUNT")]->Value);	
		SETSTR(	clBlockSub.stFTP_PASSWORD,	set->Fields->Item[_variant_t("FTP_PASSWORD")]->Value);	
		SETSTR(	clBlockSub.stVENUE_CODE,	set->Fields->Item[_variant_t("VENUE_CODE")]->Value);	
		SETSTR(	clBlockSub.stALL_PRODUCTS,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);	
		SETSTR(	clBlockSub.stPROD_CODE,		set->Fields->Item[_variant_t("PROD_CODE")]->Value);	
		SETSTR(	clBlockSub.stBBO_CODE,	set->Fields->Item[_variant_t("BLOCK_CODE")]->Value);	//change
		SETSTR(	clBlockSub.stEXCH_CODE,		set->Fields->Item[_variant_t("EXCH_CODE")]->Value);

		if( clBlockSub.stEXCH_CODE.GetLength() > 0 )
			clBlockSub.strExchangeV.push_back( clBlockSub.stEXCH_CODE.GetString() );

		if( clBlockSub.stBBO_CODE.GetLength() > 0 ) 
			clBlockSub.strProductV.push_back( clBlockSub.stBBO_CODE.GetString() ); 

		m_clBBOSubV.push_back( clBlockSub );
		iCount++;
		set->MoveNext();
	}

	set->Close();	
}

void CME_BBO_TNS_DECODE::fnPageUpDown( )
{
	int iTotalRecord = m_clBBOSubV.size( );

	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpBase,e_PgDnBase,iTotalRecord,m_numofRows,m_curTopRow);	
}

void CME_BBO_TNS_DECODE::DisplaySubscription()
{	
	int iRow = 0;
	for(unsigned int iIndex = m_curTopRow; iIndex < m_clBBOSubV.size(); iIndex++, iRow++)
	{
		if( iRow >= m_numofRows )
			break;

		BBOSubscriptionDtl &clEODSub= m_clBBOSubV[iIndex];

		GetField(e_SubscrID		+ iRow)->SetValueInt(clEODSub.iOrderId);  
		GetField(e_CUST_NAME	+ iRow)->SetValueString(clEODSub.stCUST_NAME);  

		GetField(e_CONTACT_EMAIL + iRow)->SetValueString(clEODSub.stDEST_ACCOUNT);  
		GetField(e_DEST_ACCOUNT_FTP_PASSWORD + iRow)->SetValueString(clEODSub.stDEST_ACCOUNT);  
		GetField(e_VENUE_CODE	+ iRow)->SetValueString(clEODSub.stVENUE_CODE);
		GetField(e_ALL_PRODUCTS + iRow)->SetValueString(clEODSub.stALL_PRODUCTS);
		GetField(e_PROD_CODE	+ iRow)->SetValueString(clEODSub.stPROD_CODE);
		GetField(e_BBO_CODE		+ iRow)->SetValueString(clEODSub.stBBO_CODE); //change
		GetField(e_EXCH_CODE	+ iRow)->SetValueString(clEODSub.stEXCH_CODE);
		GetField(e_OutputFileName + iRow)->SetValueString(clEODSub.stOutputFileName);		
	}

	while( iRow < m_numofRows )
	{
		GetField(e_SubscrID		+ iRow)->SetBlankState();  
		GetField(e_CUST_NAME	+ iRow)->SetBlankState();  

		GetField(e_CONTACT_EMAIL + iRow)->SetBlankState();  
		GetField(e_DEST_ACCOUNT_FTP_PASSWORD + iRow)->SetBlankState();  
		GetField(e_VENUE_CODE	+ iRow)->SetBlankState();  
		GetField(e_ALL_PRODUCTS + iRow)->SetBlankState();  
		GetField(e_PROD_CODE	+ iRow)->SetBlankState();  
		GetField(e_BBO_CODE		+ iRow)->SetBlankState();  //change
		GetField(e_EXCH_CODE	+ iRow)->SetBlankState();  
		GetField(e_OutputFileName + iRow)->SetBlankState();  

		iRow++;
	}
}

void CME_BBO_TNS_DECODE::GenerateFileName( )
{
	CString strBasePath = GetField(e_DefaultBasePath)->GetValueString();
	BFDate  bfDefaultDate = GetField(e_DefaultDate)->GetValueDate(); 
	int iYr		= bfDefaultDate.GetYear();
	int iMonth	= bfDefaultDate.GetMonth() + 1;
	int iDay	= bfDefaultDate.GetDate();

	CString csDate;
	csDate.Format("%d%02d%02d",bfDefaultDate.GetYear(),bfDefaultDate.GetMonth() + 1,bfDefaultDate.GetDate());

	CString strFileExtensionList = m_fldpFileExtensionList->GetShortString();	
	CString strBlockOrTNSFile = GetField(e_BBO_OR_TNS)->GetDisplayString();//change
	CString strFileName  ="BBOS_" + csDate + "." + strFileExtensionList ;

	CString strFullInputPath;
	strFullInputPath.Format("%s\\%s\\%s",strBasePath,csDate,strFileName);

	GetField(e_DirectoryInputPath)->SetValueString( strFullInputPath );

	CString strFullOutput;
	
	strFullOutput.Format("%s\\%s\\%s\\Output",strBasePath,csDate,strBlockOrTNSFile);
	GetField(e_DirectoryOutputPath)->SetValueString( strFullOutput );
}

void CME_BBO_TNS_DECODE::fnUncompressGZ()
{
	CString csFilePath, csUncompFileName, csCommand;

	csFilePath = GetField( e_DirectoryInputPath )->GetValueString( );

	csFilePath =   csFilePath.Mid(1);
	csFilePath.Replace("\\","\\\\");
	csUncompFileName = csFilePath;
	csUncompFileName.Replace(".gz",".txt");

	//csCommand.Format("gzip.exe -d -c \\\\powervault3\\\\CME_s3\\\\daily\\\\blocks\\\\20150801\\\\BLOCKS_20150801.gz > \\\\powervault3\\\\CME_s3\\\\daily\\\\blocks\\\\20150801\\\\BLOCKS_20150801.txt");
	csCommand.Format("gzip.exe -d -c %s > %s", csFilePath, csUncompFileName);

	int	iValue = system(csCommand);

	if( iValue == 0 )
	{
		//UpdateUncompressflag(1, index);
		GetField(e_Message)->SetValueString("Uncomprssed file successfully.");
	}
	else
	{
		CString csTemp;
		csTemp.Format("error no = %d - path %s, uncompress unsuccessful",iValue,csFilePath);
		GetField(e_Message)->SetValueString(csTemp);
	}
}