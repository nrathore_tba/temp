// Copyright (C) 2000 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#include <stdafx.h>

#include <direct.h>
#include <string.h>

#include <numeric>
#include <functional>
#include "CMEMDProcessManager.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(CMEMDProcessManager)

CMEMDProcessManager::CMEMDProcessManager()
{
}

CMEMDProcessManager::~CMEMDProcessManager()
{
}

bool CMEMDProcessManager::RealInitData()
{ 
	m_curTopRow= 0;
	for(m_numofRows = 0; GetField(e_DisplayFileName + m_numofRows); m_numofRows++);

	m_curTopRowDB= 0;
	for(m_numofRowsDB = 0; GetField(e_DisplayFileNameDB + m_numofRowsDB); m_numofRowsDB++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME_BCP","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	// DB Connection Completed....
	m_bIsThreadRunning = false;
	m_lTotalRead = 0;
	m_iTotalMessage = 0;
	return true;
}

UINT MDProcessor(void *pVoid)
{
	CMEMDProcessManager *pParser = (CMEMDProcessManager*)pVoid;
	
	pParser->m_lTotalRead = 0;
	pParser->m_bIsThreadRunning = true;
	pParser->Parse();
	pParser->m_bIsThreadRunning = false;

	return 0;
}

void CMEMDProcessManager::fnCreateDirectory( )
{
	CString csFileDateName;
	csFileDateName = GetField(e_OutputDate)->GetValueString( );

	int iYear = GetField(e_Year)->GetValueInt();
	int iMonth = GetField(e_Month)->GetValueInt();
	int iStatus = GetField(e_Status)->GetValueInt();
	CString csFileType = GetField(e_FileType)->GetDisplayString();	
	CString csExchange = GetField(e_Exchange)->GetDisplayString();
	CString csFormat = GetField(e_FileFormat)->GetDisplayString();

	CString csBasePath = GetField(e_OutputBasePath)->GetValueString();

	CString csTemp;
	csTemp.Format("\\%02d",iMonth);
	
	csBasePath = csBasePath + csTemp;	
	CreateDirectory( csBasePath , NULL );	

	csTemp.Format("\\%s",csFileDateName);
	csBasePath = csBasePath + csTemp;	
	CreateDirectory( csBasePath , NULL );	

	csBasePath = csBasePath + "\\";

	m_csBasePath = csBasePath.GetString();	
	m_csFileDate = csFileDateName.GetString();
	GetField(e_OutputBasePath)->SetToolTipText( csBasePath );
}

void CMEMDProcessManager::RealRecalculate()
{		
	if( IsFieldNew(e_StartProcess) && !m_bIsThreadRunning)
	{
		fnCreateDirectory( );

		COleDateTime dtCurrent = COleDateTime::GetCurrentTime();
		CString strCurrentTime;
		strCurrentTime.Format("%04d/%02d%02d %02d:%02d:%02d", dtCurrent.GetYear(), dtCurrent.GetMonth(), dtCurrent.GetDay(), 
			dtCurrent.GetHour(), dtCurrent.GetMinute(), dtCurrent.GetSecond());
		
		GetField(e_StartTime)->SetValueString(strCurrentTime);
		GetField(e_EndTime)->SetValueString("");

		AfxBeginThread(MDProcessor, this);
	}

	if( m_bIsThreadRunning )
	{		
	}
	else
	{
		LoadFileListFromDB( );
		//LoadSecDefFromDB( );
		LoadSecDefFromFile( );

		if( IsFieldNew(e_FileList) )
		{
			int iIndex = GetField(e_FileList)->GetValueInt();
			CString csPath = m_clFileListDB[iIndex].csFilePath;
			//csPath.Replace("\\*.lzo","");

			GetField(e_UncomparessedPath)->SetValueString(csPath);

			csPath.Replace("\\chn0","");
			csPath.Replace("CME","CME\\ff_XCME");
			GetField(e_OutputBasePath)->SetValueString(csPath);

			CString csFileDateName;
			csFileDateName = GetField(e_FileList)->GetDisplayString();
		
			csFileDateName.Replace("_chn0.dat.lzo","");
			csFileDateName.Replace("_chn0.txt.lzo","");
			csFileDateName.Replace("md_ff_cme_","");			
			GetField(e_OutputDate)->SetValueString(csFileDateName);

			// weekly Sec def path...
			csPath = m_clFileListDB[iIndex].csFilePath;
			csPath.Replace("CME","CME\\secdef");
			
			BFDate bfDate = m_clFileListDB[iIndex].bfFileDate;
			int iValue = bfDate.GetDayOfWeek();
			bfDate.OffsetDate(-iValue);

			CString csFileName;
			csFileName.Format("secdef_w.dat_%d%02d%02d",bfDate.GetYear(), bfDate.GetMonth()+1, bfDate.GetDate() );		

			CString csSecdefpath;
			csSecdefpath.Format("%s\\%s",csPath,csFileName);
			GetField(e_SecDefFilePath)->SetValueString( csSecdefpath );
		}
	}
	
	fnPageUpDown( );
	DisplayFileList( );
}

void CMEMDProcessManager::Parse()
{
	m_iActualOpenFile = 0;
	m_clSecIDMap.clear();

	CString csFileName;
	csFileName = GetField(e_UncomparessedPath)->GetValueString();

	csFileName = csFileName + "\\" + GetField(e_FileList)->GetDisplayString();
	csFileName.Replace(".lzo","");

	FILE *m_pFile;
	
	PrintRawMessage(csFileName);
	if(fopen_s(&m_pFile, csFileName ,"r"))
	{
		PrintRawMessage("Cannot open File\n");
		CString csTemp;
		csTemp.Format("Cannot open File %s",csFileName);
		GetField(e_Message)->SetTitle( csTemp );
		StartRecalcTimer(0);
		return;
	}

	long lOffset(0);
	UpdateFileStatus(1, lOffset);
//	fseek(m_pFile,lOffset,SEEK_SET);

	char chLine[8192];
	std::string strLine("");
	int lRecordRead = 0;

	PrintRawMessage("111\n");
	while(fgets(chLine, 8192, m_pFile))
	{
		if(strchr(chLine, '\n'))
		{
			strLine += chLine;

			ParseMessage(strLine);

			strLine = "";
			lRecordRead++;
		}
		else
		{
			strLine += chLine;
		}

		if( m_iTotalMessage >= 10000 )
		{
			fnWriteMessageinOutputFile();
			m_iTotalMessage = 0;
		}

		if( lRecordRead >= 1000)
		{
			m_lTotalRead += 1000;
			lRecordRead = 0;

			CString csTemp;
			csTemp.Format("%d Records",m_lTotalRead);
			GetField(e_Message)->SetTitle( csTemp );
			StartRecalcTimer(0);
		}		
	}

	if( m_iTotalMessage > 0 )
	{
		fnWriteMessageinOutputFile();
		m_iTotalMessage = 0;
	}

	OpenFileMap::iterator ofpItr = m_clOpenFileMap.begin();
	while( ofpItr != m_clOpenFileMap.end() )
	{
		if( ofpItr->second.pFileOut != NULL )
		{
			fclose(ofpItr->second.pFileOut);
			ofpItr->second.pFileOut = NULL;
		}
		ofpItr++;
	}
	m_clOpenFileMap.clear();

	lOffset = -1;//EOF
	UpdateFileStatus(2, lOffset);
	fclose(m_pFile);	

	CString strCurrentTime;

	COleDateTime dtCurrent = COleDateTime::GetCurrentTime();
	strCurrentTime.Format("%04d/%02d%02d %02d:%02d:%02d", dtCurrent.GetYear(), dtCurrent.GetMonth(), dtCurrent.GetDay(), 
	dtCurrent.GetHour(), dtCurrent.GetMinute(), dtCurrent.GetSecond());

	GetField(e_EndTime)->SetValueString(strCurrentTime);
	StartRecalcTimer(0);
}


using namespace std;

void CMEMDProcessManager::ParseMessage( std::string &strLine )
{
	try
	{
		const char * pch;
		pch = strstr(strLine.c_str(),"35=");
		char MessageChar = pch[3];

		switch( MessageChar )
		{
		case 'X':

			ParseIncreamentalRefreshMessage( strLine );
			//printf("x Message");
			break;

		case 'd':

			ParseSecDefMessage( strLine );
			//printf("d Message");
			break;

		case 'f':

			ParseSecStatusMessage( strLine );
			//printf("f Message");
			break;

		case 'R':

			ParseQuoteRequestMessage( strLine );
			//printf("f Message");
			break;

		default:
			return;
		}
	}
	catch(...)
	{		
	}
}

bool CMEMDProcessManager::fnPushdMessage( std::string strFileName, long &lSecId )
{	
	SecIDMap::iterator itr = m_clSecIDMap.find(strFileName);
	if( itr == m_clSecIDMap.end() )
	{
		if( m_clSecDefMap.find(lSecId) !=  m_clSecDefMap.end( ) )
		{
			std::map<long, bool> clTempMap;
			clTempMap.insert(std::pair<long,bool>(lSecId,true));
			m_clSecIDMap.insert(std::pair<std::string, std::map<long, bool> >(strFileName, clTempMap ) );
			return true;
		}
	}
	else 
	{
		if( m_clSecDefMap.find(lSecId) ==  m_clSecDefMap.end( ) )
			return false;

		if( itr->second.find(lSecId) == itr->second.end() )
		{
			itr->second.insert(std::pair<long,bool>(lSecId,true));
			return true;
		}	

		return false;
	}

	return false;
}

void CMEMDProcessManager::ParseIncreamentalRefreshMessage( std::string &strLine )
{
	try
	{
		const char * pch = strstr(strLine.c_str(),"268=1\01");
		if( pch != NULL )
		{			
			// Single Message
			long lSecId = ParseSecurityIDMessage(strLine);
			std::string strFileName = fnGetFileNameFromSecId( lSecId );

			if( fnPushdMessage( strFileName, lSecId ) )
			{				
				m_clMessageMap[strFileName].push_back(m_clSecDefMap.find(lSecId)->second.stffMsg);		
			}

			m_clMessageMap[strFileName].push_back(strLine);
			m_iTotalMessage++;
		}
		else
		{
			// Multiple Message
			pch = strstr(strLine.c_str(),"268=");

			std::string stMdEntryCnt;
			int i = 4;
			while (pch[i] != '\01' )
			{
				stMdEntryCnt += pch[i];
				i++;
			}

			int iHeaderSize = (pch+i) - strLine.c_str();
			std::string strHeader = strLine.substr(0,iHeaderSize);

			int iFooterSize = 0;
			std::string strFooter;

			int iEntryCnt = atoi(stMdEntryCnt.c_str());
			pch = strstr(pch,"279=");

			MessageMap clMsgMap;

			int iMessageSize = iHeaderSize;
			int iCount(0);
			for( int iI = 0; iI < iEntryCnt; iI++ )
			{
				const char * pch1 = NULL;
				if( pch != NULL )
					pch1 = strstr(pch+4,"279=");

				if( pch1 != NULL )
				{
					int iMessageSizeNew = (pch1 - pch);

					std::string strMessage = strLine.substr(iMessageSize, iMessageSizeNew);

					long lSecId = ParseSecurityIDMessage(strMessage);
					std::string strFileName = fnGetFileNameFromSecId( lSecId );

					if( fnPushdMessage( strFileName, lSecId ) )
					{						
						m_clMessageMap[strFileName].push_back(m_clSecDefMap.find(lSecId)->second.stffMsg);
					}

					clMsgMap[strFileName].push_back(strMessage);
					iMessageSize += iMessageSizeNew;
				}
				else
				{
					// This means we are at last message, we must have footer..
					pch1 = strstr(pch+4,"10=");
					if( pch1 != NULL )
					{
						int iMessageSizeNew = (pch1 - pch);

						std::string strMessage = strLine.substr(iMessageSize, iMessageSizeNew);
						long lSecId = ParseSecurityIDMessage(strMessage);
						std::string strFileName = fnGetFileNameFromSecId( lSecId );

						if( fnPushdMessage( strFileName, lSecId ) )
						{
							m_clMessageMap[strFileName].push_back(m_clSecDefMap.find(lSecId)->second.stffMsg);
						}

						clMsgMap[strFileName].push_back(strMessage);
						iMessageSize += iMessageSizeNew;

						iFooterSize = strLine.length() - iMessageSize;
						strFooter = strLine.substr(iMessageSize, iFooterSize);

						iMessageSize += iFooterSize;

						pch1 = NULL;
					}
					else
					{
						//("NULL END");
					}
				}

				pch = pch1;
				iCount++;

				if( pch == NULL )
					break;		
			}

			ASSERT( iCount==iEntryCnt );

			//----------------------------------------
			MessageMap::iterator itr = clMsgMap.begin();
			std::string strFullMsg;
						
			while( itr != clMsgMap.end() )
			{
				strFullMsg = strHeader;
				
				if( clMsgMap.size() > 1 )
				{
					std::string tag268 = "268=";					
					tag268.append( stMdEntryCnt );		

					std::ostringstream sstream;
					
					sstream <<"268=" << itr->second.size();
					std::string tag268Rep = sstream.str();
					
					strFullMsg.replace(strFullMsg.find(tag268), sizeof(tag268)-1, tag268Rep);

					for(int iI = 0; iI < itr->second.size(); iI++ )
					{
						strFullMsg.append( itr->second[iI] );
					}

					const unsigned char* iter = reinterpret_cast<const unsigned char*>( strFullMsg.c_str() );
					int checkSum = (std::accumulate( iter, iter + strFullMsg.length(), 0 ))%256;
					
					sstream.clear();

					sstream <<"10=" << checkSum << "\001";
					std::string strNewFooter = sstream.str();

					strFullMsg.append( strNewFooter );
				}
				else
				{
					for(int iI = 0; iI < itr->second.size(); iI++ )
					{
						strFullMsg.append( itr->second[iI] );
					}

					strFullMsg.append( strFooter );
				}

				m_clMessageMap[itr->first].push_back(strFullMsg);
				m_iTotalMessage += itr->second.size();
				itr++;
			}
		}
	}
	catch(...)
	{		
	}
}

long CMEMDProcessManager::ParseSecurityIDMessage( std::string &strMessage )
{
	const char * pch = strstr(strMessage.c_str(),"48=");
	if( pch == NULL )
		return 0;

	std::string stSecId;
	int i = 3;
	while (pch[i] != '\01' )
	{
		stSecId += pch[i];
		i++;
	}

	long lSecId = atoi(stSecId.c_str());
	return lSecId;
}

//____Parsing security defintion message...
void CMEMDProcessManager::ParseSecDefMessage( std::string &strLine )
{
	const char *pch = strstr(strLine.c_str(),"1151=");

	std::string stProdCode;
	int i = 5;
	while (pch[i] != '\01' )
	{
		stProdCode += pch[i];
		i++;
	}

	pch = strstr(strLine.c_str(),"48=");
	std::string stSecId;
	i = 3;
	while (pch[i] != '\01' )
	{
		stSecId += pch[i];
		i++;
	}


	pch = strstr(strLine.c_str(),"55=");
	std::string stGroupCode;
	i = 3;
	while (pch[i] != '\01' )
	{
		stGroupCode += pch[i];
		i++;
	}

	pch = strstr(strLine.c_str(),"207=");
	std::string stExchange;
	i = 4;
	while (pch[i] != '\01' )
	{
		stExchange += pch[i];
		i++;
	}

	pch = strstr(strLine.c_str(),"107=");
	std::string stProdDescription;
	i = 4;
	while (pch[i] != '\01' )
	{
		stProdDescription += pch[i];
		i++;
	}

	pch = strstr(strLine.c_str(),"461=");
	std::string stFOIType;
	i = 4;
	while (pch[i] != '\01' )
	{
		stFOIType += pch[i];
		i++;
	}

	std::string stFOI = "";
	std::string stFOITYPEName = "";
	int iSpread = 0;

	if( stFOIType.size() > 3 )
	{
		if( stFOIType[0] == 'F' && stFOIType[1] == 'M')
		{
			stFOI = "FUT";
			stFOITYPEName = "_FUT_SPD";
			iSpread = 1;
		}
		else if( stFOIType[0] == 'F' )
		{
			stFOI = "FUT";
			stFOITYPEName = "_FUT";
			iSpread = 0;
		}
		else if( stFOIType[0] == 'O' && stFOIType[1] == 'M')
		{
			stFOI = "OPT";
			stFOITYPEName = "_OPT_SPD";
			iSpread = 1;
		}
		else if( stFOIType[0] == 'O' )
		{
			stFOI = "OPT";
			stFOITYPEName = "_OPT";
			iSpread = 0;
		}
	}

	//--------
	//Persist sec def if not present...
	long lSecId = atoi(stSecId.c_str());
	if( m_clSecDefMap.find(lSecId) == m_clSecDefMap.end() )
	{
		SecDef clSecDef;
		clSecDef.iSpread = iSpread;
		clSecDef.csFUIType = stFOI.c_str();
		clSecDef.csProductCode	= stProdCode.c_str();
		clSecDef.stffMsg = strLine;
	}
	else if( m_clSecDefMap[lSecId].csProductCode == "NOTFOUND" )
	{
		m_clSecDefMap[lSecId].iSpread = iSpread;
		m_clSecDefMap[lSecId].csFUIType = stFOI.c_str();
		m_clSecDefMap[lSecId].csProductCode	= stProdCode.c_str();
		m_clSecDefMap[lSecId].stffMsg = strLine;
	}

	/*if( m_clSecDefMap.find(lSecId) == m_clSecDefMap.end() || m_clSecDefMap[lSecId].csProductCode == "NOTFOUND" )
	{
		bool bFound = fnGetSecDefFromMaster( lSecId );

		if( !bFound )
		{
			CString sql;
			sql.Format("EXEC [Proc_Beast_Submit_CME_Security_Definition_Mst_Single] '%s','%s','%s','%s','%s','%s',%d, %d, '%s'", stGroupCode.c_str(), stExchange.c_str(), stProdCode.c_str(), stProdDescription.c_str(), stSecId.c_str(), stFOI.c_str(), iSpread, GetUpdateUserID(),  GetField(e_FileList)->GetDisplayString());

			_RecordsetPtr set;
			if( FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql)) )
			{
				SetErrorMessage(ErrorSeverity::e_Error, sql);
				ASSERT(0);
			}

			bool bFound = fnGetSecDefFromMaster( lSecId );
			ASSERT( bFound );
		}	
	}*/
		
	std::string strFileName("XCME_MD_");
	strFileName.append(stProdCode).append(stFOITYPEName);

	m_clMessageMap[strFileName].push_back(strLine);
	m_iTotalMessage++;
}

//____Parsing security status message...
void CMEMDProcessManager::ParseSecStatusMessage( std::string &strLine )
{	
	const char *pch = strstr(strLine.c_str(),"48=");
	std::string stSecId;
	int i = 3;
	while (pch[i] != '\01' )
	{
		stSecId += pch[i];
		i++;
	}

	long lSecId = atoi( stSecId.c_str() );

	std::string strFileName = fnGetFileNameFromSecId(lSecId );

	if( fnPushdMessage( strFileName, lSecId ) )
	{
		m_clMessageMap[strFileName].push_back(m_clSecDefMap.find(lSecId)->second.stffMsg);		
	}
		
	m_clMessageMap[strFileName].push_back(strLine);
	m_iTotalMessage++;
}

//Pares order quote Message..
void CMEMDProcessManager::ParseQuoteRequestMessage( std::string &strLine )
{
	try
	{
		const char * pch = strstr(strLine.c_str(),"146=1\01");
		if( pch != NULL )
		{			
			// Single Message
			long lSecId = ParseSecurityIDMessage(strLine);
			std::string strFileName = fnGetFileNameFromSecId( lSecId );

			if( fnPushdMessage( strFileName, lSecId ) )
			{				
				m_clMessageMap[strFileName].push_back(m_clSecDefMap.find(lSecId)->second.stffMsg);		
			}

			m_clMessageMap[strFileName].push_back(strLine);
			m_iTotalMessage++;
		}
		else
		{
			// Multiple Message
			pch = strstr(strLine.c_str(),"55=");

			std::string stMdEntryCnt;
			int i = 3;
			while (pch[i] != '\01' )
			{
				stMdEntryCnt += pch[i];
				i++;
			}

			int iHeaderSize = (pch+i) - strLine.c_str();
			std::string strHeader = strLine.substr(0,iHeaderSize);

			int iFooterSize = 0;
			std::string strFooter;

			int iEntryCnt = atoi(stMdEntryCnt.c_str());
			pch = strstr(pch,"55=");

			MessageMap clMsgMap;

			int iMessageSize = iHeaderSize;
			int iCount(0);
			for( int iI = 0; iI < iEntryCnt; iI++ )
			{
				const char * pch1 = NULL;
				if( pch != NULL )
					pch1 = strstr(pch+3,"55=");

				if( pch1 != NULL )
				{
					int iMessageSizeNew = (pch1 - pch);

					std::string strMessage = strLine.substr(iMessageSize, iMessageSizeNew);
					long lSecId = ParseSecurityIDMessage(strMessage);

					std::string strFileName = fnGetFileNameFromSecId( lSecId );

					if( fnPushdMessage( strFileName, lSecId ) )
					{
						m_clMessageMap[strFileName].push_back(m_clSecDefMap.find(lSecId)->second.stffMsg);		
					}

					clMsgMap[strFileName].push_back(strMessage);
					iMessageSize += iMessageSizeNew;
				}
				else
				{
					// This means we are at last message, we must have footer..
					pch1 = strstr(pch+3,"10=");
					if( pch1 != NULL )
					{
						int iMessageSizeNew = (pch1 - pch);

						std::string strMessage = strLine.substr(iMessageSize, iMessageSizeNew);
						long lSecId = ParseSecurityIDMessage(strMessage);
						std::string strFileName = fnGetFileNameFromSecId( lSecId );

						if( fnPushdMessage( strFileName, lSecId ) )
						{
							m_clMessageMap[strFileName].push_back(m_clSecDefMap.find(lSecId)->second.stffMsg);		
						}

						clMsgMap[strFileName].push_back(strMessage);
						iMessageSize += iMessageSizeNew;

						iFooterSize = strLine.length() - iMessageSize;
						strFooter = strLine.substr(iMessageSize, iFooterSize);

						iMessageSize += iFooterSize;

						pch1 = NULL;
					}
					else
					{
						//("NULL END");
					}
				}

				pch = pch1;
				iCount++;

				if( pch == NULL )
					break;		
			}

			ASSERT( iCount==iEntryCnt );

			//----------------------------------------
			MessageMap::iterator itr = clMsgMap.begin();
			std::string strFullMsg;
			while( itr != clMsgMap.end() )
			{
				strFullMsg = strHeader;

				for(int iI = 0; iI < itr->second.size(); iI++ )
				{
					strFullMsg.append( itr->second[iI] );
				}

				strFullMsg.append( strFooter );

				m_clMessageMap[itr->first].push_back(strFullMsg);
				m_iTotalMessage += itr->second.size();
				itr++;
			}
		}
	}
	catch(...)
	{		
	}
}

void CMEMDProcessManager::fnWriteMessageinOutputFile()
{
	MessageMap::iterator itr = m_clMessageMap.begin();
	std::string strAllMsg;
	while( itr != m_clMessageMap.end() )
	{		
		FILE *pFileOut = fnGetOutputFilePointer( itr->first );
		if( pFileOut != NULL )
		if( itr->second.size() == 1 )
		{
			fputs(itr->second[0].c_str(), pFileOut);
		}
		else
		{
			strAllMsg = itr->second[0];
			int iCount(1);
			for(int iI = 1; iI < itr->second.size(); iI++ )
			{				
				strAllMsg.append( itr->second[iI] );
				iCount++;

				if( iCount == 100 )
				{
					fputs(strAllMsg.c_str(), pFileOut);
					strAllMsg.clear();					
					iCount = 0;
				}
			}

			if( iCount > 0 )
				fputs(strAllMsg.c_str(), pFileOut);
		}
		
		itr++;
	}
	m_clMessageMap.clear();
}

std::string CMEMDProcessManager::fnGetFileNameFromSecId( const long &laSecId )
{
	bool bSecdefPresent = true;
	if( m_clSecDefMap.find(laSecId) == m_clSecDefMap.end() )
	{
		bSecdefPresent = false;//fnGetSecDefFromMaster( laSecId );
	}
		
	if( !bSecdefPresent )
	{
		m_clSecDefMap[laSecId].csProductCode = "NOTFOUND";
		//m_clSecDefMap[laSecId].csGroupCode = "";
		m_clSecDefMap[laSecId].csFUIType = "";
		m_clSecDefMap[laSecId].iSpread = 0;
		m_clSecDefMap[laSecId].stffMsg = "";
	}
	
	CString csFileName;
	if( m_clSecDefMap.find(laSecId)->second.iSpread == 0 )
	{
		csFileName = "XCME_MD_"+ m_clSecDefMap.find(laSecId)->second.csProductCode + "_" + m_clSecDefMap.find(laSecId)->second.csFUIType;
	}
	else
	{
		csFileName = "XCME_MD_"+ m_clSecDefMap.find(laSecId)->second.csProductCode + "_" + m_clSecDefMap.find(laSecId)->second.csFUIType + "_SPD";
	}
	
	return csFileName.GetString();
}

FILE * CMEMDProcessManager::fnGetOutputFilePointer( const std::string &strOutputFileName )
{
	if( m_clOpenFileMap.find(strOutputFileName) == m_clOpenFileMap.end() )
	{
		if( m_iActualOpenFile >= 500 )
		{
			OpenFileMap::iterator ofpItr = m_clOpenFileMap.begin();
			while( ofpItr != m_clOpenFileMap.end() )
			{
				if( ofpItr->second.pFileOut != NULL )
				{
					fclose(ofpItr->second.pFileOut);
					ofpItr->second.pFileOut = NULL;
				}
				ofpItr++;
			}

			m_iActualOpenFile = 0;
			//UpdateFileSeekOffset( ftell(m_pFile) );
		}

		OpneFiles clOpneFiles;
		clOpneFiles.csFileName = strOutputFileName;

		std::string csPath = m_csBasePath;
		csPath.append(strOutputFileName);
		csPath.append("_");
		csPath.append(m_csFileDate);

		if(fopen_s(&clOpneFiles.pFileOut, csPath.c_str(),"w"))
			return NULL;

		m_clOpenFileMap[strOutputFileName] = clOpneFiles;
		m_iActualOpenFile++;
	}
	else if( m_clOpenFileMap[strOutputFileName].pFileOut == NULL )
	{
		if( m_iActualOpenFile >= 500 )
		{
			OpenFileMap::iterator ofpItr = m_clOpenFileMap.begin();
			while( ofpItr != m_clOpenFileMap.end() )
			{
				if( ofpItr->second.pFileOut != NULL )
				{
					fclose(ofpItr->second.pFileOut);
					ofpItr->second.pFileOut = NULL;
				}
				ofpItr++;
			}

			m_iActualOpenFile = 0;
			//UpdateFileSeekOffset( ftell(m_pFile) );
		}

		//reopen file
		OpneFiles clOpneFiles;
		clOpneFiles.csFileName = strOutputFileName;

		std::string csPath = m_csBasePath;
		csPath.append(strOutputFileName);
		csPath.append("_");
		csPath.append(m_csFileDate);

		if(fopen_s(&clOpneFiles.pFileOut, csPath.c_str(),"a+"))
			return NULL;

		m_clOpenFileMap[strOutputFileName].pFileOut = clOpneFiles.pFileOut;
		m_iActualOpenFile++;
	}

	return m_clOpenFileMap[strOutputFileName].pFileOut;	
}

/*
bool CMEMDProcessManager::fnGetSecDefFromMaster(const long &laSecId)
{
	CString sql;	
	sql.Format("EXEC [Proc_Beast_Get_CME_Security_Definition_mst_new] '%d'",laSecId);
	
	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		ASSERT(0);
		return false;
	}

	SecDef clSecDef;
	CString csSecID;
	bool breturn = false;

	if(VARIANT_FALSE == set->adoEOF)
	{
		//SETSTR(	clSecDef.csGroupCode,set->Fields->Item[_variant_t("Group_Code")]->Value);	
		SETSTR(	clSecDef.csProductCode,	set->Fields->Item[_variant_t("ProductCode")]->Value);	
		//SETLONG(	lSecID,				set->Fields->Item[_variant_t("SecurityId")]->Value);	
		SETSTR(	clSecDef.csFUIType,		set->Fields->Item[_variant_t("FOItype")]->Value);				
		SETLONG(clSecDef.iSpread,		set->Fields->Item[_variant_t("SpreadIn")]->Value);	
		
		//long lSecId( atoi(csSecID) );
		m_clSecDefMap[laSecId] = clSecDef;
		breturn = true;
	}
	
	return breturn;
}*/

void CMEMDProcessManager::LoadFileListFromDB( )
{
	if( !IsFieldNew(e_LoadFromDB) )
		return;

	m_clFileListDB.clear();

	int iYear = GetField(e_Year)->GetValueInt();
	int iMonth = GetField(e_Month)->GetValueInt();
	int iStatus = GetField(e_Status)->GetValueInt();
	CString csFileType = GetField(e_FileType)->GetDisplayString();	
	CString csExchange = GetField(e_Exchange)->GetDisplayString();
	CString csFormat = GetField(e_FileFormat)->GetDisplayString();

	CString sql;
	sql.Format("SELECT * FROM CME_Historical_Files  where FileType = '%s' AND [Year] = %d	AND [Month] = %d AND	Exchange = '%s' And	Format = '%s'",csFileType,iYear,iMonth, csExchange,csFormat);

	_RecordsetPtr set;

	CString csList = "", csTemp = "";

	DB_TRY
	{
		if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql, false)))
		{
			SetErrorMessage(ErrorSeverity::e_Error, sql);
			PrintRawMessage(sql);
		}

		FileDetail clFileDetail;	
		int iDay,iMonth,iYear;

		while( VARIANT_FALSE == set->adoEOF )
		{
			SETSTR(	clFileDetail.csFileName,	set->Fields->Item[_variant_t("FileName")]->Value);	
			SETLONGLONG(clFileDetail.lFileSize,	set->Fields->Item[_variant_t("Size")]->Value);	
			SETLONG(clFileDetail.iStatus,		set->Fields->Item[_variant_t("Status")]->Value);
			SETSTR(clFileDetail.csFilePath,		set->Fields->Item[_variant_t("Path")]->Value);

			SETLONG(iYear,		set->Fields->Item[_variant_t("Year")]->Value);
			SETLONG(iMonth,		set->Fields->Item[_variant_t("Month")]->Value);
			SETLONG(iDay,		set->Fields->Item[_variant_t("FileDate")]->Value);

			clFileDetail.bfFileDate = BFDate(iYear, iMonth-1, iDay);
			
			if( clFileDetail.iStatus == 0 || clFileDetail.iStatus == 1 )
			{
				if( csList.GetLength() ==  0 )
				{
					csList.Format("%s",clFileDetail.csFileName);
				}
				else
				{
					csTemp.Format("|%s",clFileDetail.csFileName);
					csList = csList + csTemp;
				}			
				m_clFileListDB.push_back( clFileDetail );	
			}

			set->MoveNext();
		}
		set->Close();
	}
	DB_CATCH(sql);	

	((ListField*)GetField(e_FileList))->SetDataString(csList);
}
//
//void CMEMDProcessManager::LoadSecDefFromDB( )
//{
//	if( !IsFieldNew(e_LoadSecDefDB) )
//		return;
//
//	m_clSecDefMap.clear();
//
//	COleDateTime dtCurrent = COleDateTime::GetCurrentTime();
//	CString strCurrentTime;
//	strCurrentTime.Format("%04d/%02d%02d %02d:%02d:%02d", dtCurrent.GetYear(), dtCurrent.GetMonth(), dtCurrent.GetDay(), 
//		dtCurrent.GetHour(), dtCurrent.GetMinute(), dtCurrent.GetSecond());
//
//	GetField(e_StartTime)->SetValueString(strCurrentTime);
//	GetField(e_EndTime)->SetValueString("");
//
//	CString sql;
//	sql.Format("SELECT ProductCode,SecurityId,FOItype,SpreadIn FROM dbo.CME_Security_Definition_Stg");
//
//	_RecordsetPtr set;
//		
//	DB_TRY
//	{
//		if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql, false)))
//		{
//			SetErrorMessage(ErrorSeverity::e_Error, sql);
//			PrintRawMessage(sql);
//		}
//
//		SecDef clSecDef;	
//		long lSecId;
//
//		while( VARIANT_FALSE == set->adoEOF )
//		{
//			//SETSTR(	clSecDef.csGroupCode,set->Fields->Item[_variant_t("Group_Code")]->Value);	
//			SETSTR(	clSecDef.csProductCode,	set->Fields->Item[_variant_t("ProductCode")]->Value);	
//			SETLONG( lSecId,				set->Fields->Item[_variant_t("SecurityId")]->Value);	
//			SETSTR(	clSecDef.csFUIType,		set->Fields->Item[_variant_t("FOItype")]->Value);				
//			SETLONG( clSecDef.iSpread,		set->Fields->Item[_variant_t("SpreadIn")]->Value);	
//
//			m_clSecDefMap[lSecId] = clSecDef;	
//			set->MoveNext();
//		}
//		set->Close();
//	}
//	DB_CATCH(sql);	
//
//	dtCurrent = COleDateTime::GetCurrentTime();
//	strCurrentTime;
//	strCurrentTime.Format("%04d/%02d%02d %02d:%02d:%02d", dtCurrent.GetYear(), dtCurrent.GetMonth(), dtCurrent.GetDay(), dtCurrent.GetHour(), dtCurrent.GetMinute(), dtCurrent.GetSecond());		
//
//	GetField(e_EndTime)->SetValueString(strCurrentTime);
//}

#define FSUCCESS 0
void CMEMDProcessManager::LoadSecDefFromFile( )
{
	if( !IsFieldNew(e_LoadSecDefDB) )
		return;

	m_clSecDefMap.clear();

	COleDateTime dtCurrent = COleDateTime::GetCurrentTime();
	CString strCurrentTime;
	strCurrentTime.Format("%04d/%02d%02d %02d:%02d:%02d", dtCurrent.GetYear(), dtCurrent.GetMonth(), dtCurrent.GetDay(), 
		dtCurrent.GetHour(), dtCurrent.GetMinute(), dtCurrent.GetSecond());

	GetField(e_StartTime)->SetValueString(strCurrentTime);
	GetField(e_EndTime)->SetValueString("");

	CString csSecdefFileNamePath = GetField(e_SecDefFilePath)->GetValueString();
		
	FILE *fileIn(NULL);
	
	if( fopen_s(&fileIn, csSecdefFileNamePath, "r") != FSUCCESS )
	{
		GetField(e_Message)->SetTitle("Error while Opening fileIn");		
		return;
	}

	char chline[2048];
	std::string strLine("");
	int lRecordRead(0);
		
	while( fgets(chline, 2048, fileIn) )
	{
		if(strchr(chline, '\n'))
		{
			strLine += chline;
			
			ParseSecdefLine(strLine); 

			strLine = "";
			lRecordRead++;
		}
		else
		{
			strLine += chline;
		}
	}

	fclose(fileIn);

	dtCurrent = COleDateTime::GetCurrentTime();
	strCurrentTime;
	strCurrentTime.Format("%04d/%02d%02d %02d:%02d:%02d", dtCurrent.GetYear(), dtCurrent.GetMonth(), dtCurrent.GetDay(), dtCurrent.GetHour(), dtCurrent.GetMinute(), dtCurrent.GetSecond());		

	GetField(e_EndTime)->SetValueString(strCurrentTime);
}

void CMEMDProcessManager::ParseSecdefLine(std::string &strLine )
{
	const char *pch = strstr(strLine.c_str(),"1151=");

	if( pch == NULL )
		return;

	std::string stProdCode;
	int i = 5;
	while (pch[i] != '\01' )
	{
		stProdCode += pch[i];
		i++;
	}

	pch = strstr(strLine.c_str(),"48=");
	std::string stSecId;
	i = 3;
	while (pch[i] != '\01' )
	{
		stSecId += pch[i];
		i++;
	}

/*	pch = strstr(strLine.c_str(),"55=");
	std::string stGroupCode;
	i = 3;
	while (pch[i] != '\01' )
	{
		stGroupCode += pch[i];
		i++;
	}

	pch = strstr(strLine.c_str(),"207=");
	std::string stExchange;
	i = 4;
	while (pch[i] != '\01' )
	{
		stExchange += pch[i];
		i++;
	}

	pch = strstr(strLine.c_str(),"107=");
	std::string stProdDescription;
	i = 4;
	while (pch[i] != '\01' )
	{
		stProdDescription += pch[i];
		i++;
	}*/

	pch = strstr(strLine.c_str(),"461=");
	std::string stFOIType;
	i = 4;
	while (pch[i] != '\01' )
	{
		stFOIType += pch[i];
		i++;
	}

	std::string stFOI = "";
	std::string stFOITYPEName = "";
	int iSpread = 0;

	if( stFOIType.size() > 3 )
	{
		if( stFOIType[0] == 'F' && stFOIType[1] == 'M')
		{
			stFOI = "FUT";
			stFOITYPEName = "_FUT_SPD";
			iSpread = 1;
		}
		else if( stFOIType[0] == 'F' )
		{
			stFOI = "FUT";
			stFOITYPEName = "_FUT";
			iSpread = 0;
		}
		else if( stFOIType[0] == 'O' && stFOIType[1] == 'M')
		{
			stFOI = "OPT";
			stFOITYPEName = "_OPT_SPD";
			iSpread = 1;
		}
		else if( stFOIType[0] == 'O' )
		{
			stFOI = "OPT";
			stFOITYPEName = "_OPT";
			iSpread = 0;
		}
	}

	long lSecId = atoi(stSecId.c_str());

	if( m_clSecDefMap.find( lSecId ) == m_clSecDefMap.end() )
	{
		const char *pch = strstr(strLine.c_str(),"\00110=");
		if( pch == NULL )
			fnAddExtratag( strLine );

		SecDef clSecDef;
		clSecDef.csProductCode = stProdCode.c_str();
		clSecDef.csFUIType = stFOI.c_str();
		clSecDef.iSpread = iSpread;
		clSecDef.stffMsg = strLine;
		m_clSecDefMap.insert(std::make_pair(lSecId,clSecDef));
	}
}

void CMEMDProcessManager::fnAddExtratag(std::string &strLine)
{
	int iFind = strLine.find("\n");
	if( iFind > -1 )
		strLine.replace(strLine.find("\n"), sizeof("\n")-1, "");

	int iSize = strLine.size();

	std::ostringstream sstream;
	sstream << "9=" << iSize << "\001" << strLine;	

	strLine = sstream.str();

	const unsigned char* iter = reinterpret_cast<const unsigned char*>( strLine.c_str() );
	int checkSum = (std::accumulate( iter, iter + strLine.length(), 0 ))%256;

	sstream <<"10=" << checkSum << "\001" << "\n";

	strLine = sstream.str();
}

void CMEMDProcessManager::UpdateFileStatus( int iStatus, long &lOffset )
{
	int iStatusDB(0);
	CString csFileName = GetField(e_FileList)->GetDisplayString();	

	CString csPath = m_csBasePath.c_str();

	if( iStatus == 1 )
	{			
		if( 1 )
		{
			CString sql;
			sql.Format("Select SeekOffset, Status From CME_Historical_Files where FileName = '%s'", csFileName);

			_RecordsetPtr set;

			DB_TRY
			{
				if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql, false)))
				{
					SetErrorMessage(ErrorSeverity::e_Error, sql);
					PrintRawMessage(sql);
				}

				if( VARIANT_FALSE == set->adoEOF )
				{
					SETLONG(lOffset,		set->Fields->Item[_variant_t("SeekOffset")]->Value);	
					SETLONG(iStatusDB,		set->Fields->Item[_variant_t("Status")]->Value);	
				}
				set->Close();
			}
			DB_CATCH(sql);	
		}		
	}

	if( iStatus == 2 || iStatusDB == 0 )
	{		
		COleDateTime dtCurrent = COleDateTime::GetCurrentTime();
		CString strCurrentTime;
		strCurrentTime.Format("%04d-%02d-%02d %02d:%02d:%02d", dtCurrent.GetYear(), dtCurrent.GetMonth(), dtCurrent.GetDay(),dtCurrent.GetHour(), dtCurrent.GetMinute(), dtCurrent.GetSecond());
	
		CString sql;
		if( iStatus == 2 )
			sql.Format("UPDATE CME_Historical_Files set Status = %d, OutputPath = '%s', SeekOffset = %d, ProcessEnd = '%s' WHERE FileName = '%s'",iStatus, csPath, lOffset, strCurrentTime, csFileName);
		else
			sql.Format("UPDATE CME_Historical_Files set Status = %d, OutputPath = '%s', SeekOffset = %d, ProcessStart = '%s' WHERE FileName = '%s'",iStatus, csPath, lOffset, strCurrentTime, csFileName);

		_RecordsetPtr set;

		DB_TRY
		{
			if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql)))
			{
				SetErrorMessage(ErrorSeverity::e_Error, sql);
				PrintRawMessage(sql);
			}

		}
		DB_CATCH(sql);
	}	
}

void CMEMDProcessManager::UpdateFileSeekOffset( long lOffset )
{	
	CString csFileName = GetField(e_FileList)->GetDisplayString();	

	CString sql;
	sql.Format("UPDATE CME_Historical_Files SET SeekOffset = %d where FileName = '%s'",lOffset, csFileName);

	_RecordsetPtr set;

	DB_TRY
	{
		if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql)))
		{
			SetErrorMessage(ErrorSeverity::e_Error, sql);
			PrintRawMessage(sql);
		}

	}
	DB_CATCH(sql);	
}

void CMEMDProcessManager::fnPageUpDown( )
{
	int iTotalRecord = m_clFileList.size( );
	
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpBase,e_PgDnBase,iTotalRecord,m_numofRows,m_curTopRow);	

	iTotalRecord = m_clFileListDB.size( );
	
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpBaseDB,e_PgDnBaseDB,iTotalRecord,m_numofRowsDB,m_curTopRowDB);	
}

CString __csStatus[3] = { "Pending","Running","Processed"};

void CMEMDProcessManager::DisplayFileList( )
{
	int iRow(0);
	for( int iI = m_curTopRow; iI < m_clFileList.size(); iI++, iRow++ )
	{
		if( iRow >= m_numofRows )
			break;

		GetField( e_DisplayFileName + iRow )->SetValueString( m_clFileList[iI].csFileName );
	}

	while( iRow < m_numofRows )
	{
		GetField( e_DisplayFileName + iRow )->SetValueString("");
		iRow++;
	}


	iRow = 0;
	for( int iI = m_curTopRowDB; iI < m_clFileListDB.size(); iI++, iRow++ )
	{
		if( iRow >= m_numofRowsDB )
			break;

		GetField( e_DisplayFileNameDB + iRow )->SetValueString( m_clFileListDB[iI].csFileName );
		GetField( e_DisplayFileSizeDB + iRow )->SetValueInt( m_clFileListDB[iI].lFileSize );
		GetField( e_DisplayFileStatusDB + iRow )->SetValueString( __csStatus[m_clFileListDB[iI].iStatus] );
	}

	while( iRow < m_numofRowsDB )
	{
		GetField( e_DisplayFileNameDB + iRow )->SetValueString("");
		GetField( e_DisplayFileSizeDB + iRow )->SetBlankState();
		GetField( e_DisplayFileStatusDB + iRow )->SetBlankState();
		iRow++;
	}
}