
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>

BF_NAMESPACE_BEGIN
using namespace std;

struct stTemplate
{
	stTemplate()
	{
		iEventId = 0;
		strEventType = "";
		strSubject = "";	
		strBody = "";	
	}
	int iEventId;
	CString strEventType;
	CString strSubject;	
	CString strBody;	
};

class CMECustNotificationFinapp : public BaseImage
{
public:
	
	enum FieldIDs
	{		
		e_Message   = 11,
		e_Status	= 100,

		e_DateFilter= 50,
		e_EventyId	= 51,

		e_SmtpServer = 200,
		e_Login	     = 201,
		e_Password   = 202,
		e_T0	     = 203,
		e_From	     = 204,
		e_CC	     = 205,

		e_Subject    = 206,

		e_Body       = 207,

		e_Port       = 700,
	
		e_SendBtn     = 500,
		e_ClearBtn    = 501,		


		e_NewTempEventType   = 52,
		e_NewTempSubject     = 209,
		e_NewTempBody        = 208,
		e_NewTempAddbutton   = 502,	
		e_NewTempMessage     = 13,	
	};
	
	CMECustNotificationFinapp();
	~CMECustNotificationFinapp();	

	bool InitData();		
	void Recalculate();	

	bool m_bConfirm;
	bool m_bAddEmailTempConfirm;

	std::vector<stTemplate> m_EmailTemplateData;
		
private:

	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;

	void	fnSendEmail( );	
	void	fnClearField( );
	void    fnInitDB();
	void    fnLoadEventSubAndBodyList();
	void    fnAddEmailTemplate();
};

BF_NAMESPACE_END


