
#include <stdafx.h> 
#include "CMEBlockProcessing.hpp"
#include <MailAutomation/MailAutomation.h>
#include "afxsock.h"
#include "zip.h"


BF_NAMESPACE_USE

IMPLEMENT_IMAGE(CMEBlockProcessing)

CMEBlockProcessing::CMEBlockProcessing()
{	
	fnWriteMessage("Started", 2);
	m_sBlockSrcPath		= _T("s3://historical_proc/Blocks");
	m_iNumofRowsOTO = 0;
	m_iCurTopRowOTO = 0;
	m_iMsgCounter	= 0;
	m_csFTPUser		 = _T("");
	m_sMailContent   = _T("");
}

CMEBlockProcessing::~CMEBlockProcessing()
{
	//fnWriteMessage("Closed", 2);
	//m_MessageLogger.LogMessage(m_sErrorMessage);
}

void CMEBlockProcessing::AddLogMessages( CString csNewMsg )
{	
	if(m_iMsgCounter < 200)
		m_iMsgCounter++;

	for( int iI = m_iMsgCounter-1; iI > 0; iI-- )
		m_csLogMsgArry[iI] = m_csLogMsgArry[iI -1];
	
	m_csLogMsgArry[0] = csNewMsg;	
}

void CMEBlockProcessing::DisplayLogMessages()
{
	int iRow = 0;
	for( int iIndex = m_iCurTopRowMsg; iIndex < m_iMsgCounter; iIndex++, iRow++ )
	{
		if( iRow >= m_iNumofRowsMsg )
			break;
		
		BaseField * fldComment		= GetField(e_LogMessage + iRow);
		
		fldComment->SetValueString( m_csLogMsgArry[iIndex] );
		fldComment->SetNotManual();
	}
	
	while( iRow < m_iNumofRowsMsg )
	{
		BaseField * fldComment		= GetField(e_LogMessage + iRow);
		fldComment->SetBlankState();
		iRow++;
	}

	CString sPageNumber = "";
	if(m_iMsgCounter > 0)
	{
		int iCurrPage = m_iCurTopRowMsg / m_iNumofRowsMsg + 1;
		int iTotalPage;	
		if( (m_iMsgCounter % m_iNumofRowsMsg) == 0)
		{
			iTotalPage = (m_iMsgCounter/m_iNumofRowsMsg);	
		}
		else
		{
		
			iTotalPage = (m_iMsgCounter/m_iNumofRowsMsg) + 1;	
		}

		if(iTotalPage <= 0)
			iTotalPage = 1;
		sPageNumber.Format("%d/%d",iCurrPage,iTotalPage);
		GetField(e_Msg_Navigate)->SetTitle(sPageNumber);
	}
	else
	{
		GetField(e_Msg_Navigate)->SetTitle("0/0");
	}
}

UINT BlockOTOProcessor(void *pVoid)
{
	CMEBlockProcessing *pOTOBlockProcesser = (CMEBlockProcessing*)pVoid;
	try
	{	
		pOTOBlockProcesser->m_enmOperation = OP_ProcessRunning;
		pOTOBlockProcesser->LoadOTODetail();
		CString sMessage = "";
		sMessage.Format("One Time Order Detail: %d Record(s) Found ..!!",pOTOBlockProcesser->m_vecOneTimeOrderDetail.size());
		pOTOBlockProcesser->fnWriteMessage(sMessage, 2);
		pOTOBlockProcesser->fnWriteMessage(sMessage, 3);
		pOTOBlockProcesser->fnWriteMessage(sMessage);
		pOTOBlockProcesser->m_sMailContent = _T("");
		for(int i = 0; i < pOTOBlockProcesser->m_vecOneTimeOrderDetail.size(); i++)
		{
			OneTimeOrderDtl &clBlockOTO = pOTOBlockProcesser->m_vecOneTimeOrderDetail[i];
			if(!clBlockOTO.bOrderProcessed)
				pOTOBlockProcesser->UploadUserProductCalData(clBlockOTO.sFTPUserName);
		}
		if(!pOTOBlockProcesser->m_sMailContent.IsEmpty())
			pOTOBlockProcesser->fnSendEmail(1,10);
		pOTOBlockProcesser->m_sMailContent = _T("");
		pOTOBlockProcesser->LoadOTODetail();
		pOTOBlockProcesser->m_enmOperation = OP_ProcessCompleted;
		pOTOBlockProcesser->RequestExternalUpdate();
		return 0;
	}
	catch(...)
	{
	}
}

bool CMEBlockProcessing::InitData()
{
	cout<<"Start InitData()"<<endl;

	fnWriteMessage("Start InitData()", 2);

	GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));

	m_iCurTopRowOTO  = 0;
	for(m_iNumofRowsOTO	= 0; GetField(e_OrderID + m_iNumofRowsOTO);	 m_iNumofRowsOTO++);
	m_iCurTopRowMsg= 0;
	for(m_iNumofRowsMsg = 0; GetField(e_LogMessage + m_iNumofRowsMsg); m_iNumofRowsMsg++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","UAT-RDS");
	GetField(e_sourcePath)->SetValueString(m_sBlockSrcPath);
	m_lstS3Bucket	= (ListField*) GetField(e_lstS3Bucket);	
	FTP_UserName    = GetField(e_FTPUser);
	BlockSourcePath = GetField(e_sourcePath);

	csServerName = strServerName.get_value().c_str();
	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");

	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		fnWriteMessage("Could not initialize datastore!", 2);
		return true;
	}

	if( !IsRestoreUpdate() )
	{
		BF_Reg_Key_Ptr pKey1(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\CME"));

		int iFTPLocation;
		RegDWORD strServerName(pKey1, "S3FTPLocation",0);
		iFTPLocation = strServerName.get_value();
		m_lstS3Bucket->SetValueInt( iFTPLocation );
	}

	m_clNextScheduleDtTime = COleDateTime::GetCurrentTime();
	//m_clNextScheduleDtTime += COleDateTimeSpan(0,0,0,20);
	
	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	
	fnWriteMessage("End InitData()", 2);
	m_enmOperation = OP_None;
	for(int i = 0; i < 200; i++)
	{
		m_csLogMsgArry[i] = "";
	}
	cout<<"End InitData"<<endl;
	return true;
}

void CMEBlockProcessing::Recalculate()
{
	cout<<"Start Recalculate()"<<endl;
	
	if(IsFieldNew(e_getOTODetail))
	{
		if(m_enmOperation == OP_ProcessRunning)
		{
			fnWriteMessage("Schedule OTO block process running...");
			return;
		}
		LoadOTODetail();
		CString sMessage = "";
		sMessage.Format("One Time Order Detail: %d Record(s) Found ..!!",m_vecOneTimeOrderDetail.size());
		fnWriteMessage(sMessage, 2);
		fnWriteMessage(sMessage, 3);
		fnWriteMessage(sMessage);
		fnWriteMessage("User OTO detail Loaded.", 2);
	}
	if(IsFieldNew(e_FTPUser))
	{
		m_csFTPUser = FTP_UserName->GetDisplayString();
	}

	/*if(IsFieldNew(e_UserCalDetai))
	{
		if(m_csFTPUser.IsEmpty())
		{
			GetField(e_Status)->SetValueString(_T("User name is required..."));
			return;
		}
		m_MessageLogger.LogMessage("Product Calendar details loading...");
		GetUserProductCalDetail(m_csFTPUser);
		m_MessageLogger.LogMessage("Product Calendar detail loaded.");
	}*/

	if(IsFieldNew(e_UserUploadData))
	{
		if(m_csFTPUser.IsEmpty())
		{
			fnWriteMessage("User name is required...");
			return;
		}
		if(m_enmOperation == OP_ProcessRunning)
		{
			fnWriteMessage("Schedule OTO block process running...");
			fnWriteMessage("Schedule OTO block process running...", 3);
			return;
		}
		m_enmOperation = OP_Manual;
		GetField(e_NextProcessing)->SetTitle(_T("Manual update"));
		GetField(e_NextProcessing)->SetTitleForeColor(ColorManager::eSignalPositiveLight);
		LoadOTODetail();
		CString sMessage = "";
		sMessage.Format("One Time Order Detail: %d Record(s) Found ..!!",m_vecOneTimeOrderDetail.size());
		fnWriteMessage(sMessage, 2);
		fnWriteMessage(sMessage, 3);
		fnWriteMessage(sMessage);
		UploadUserProductCalData(m_csFTPUser);
		if(!m_sMailContent.IsEmpty())
			fnSendEmail(1,10);
		m_sMailContent = _T("");
		LoadOTODetail();
		m_enmOperation = OP_ProcessCompleted;
	}

	if( m_enmOperation == OP_ProcessCompleted )
	{		
		m_clNextScheduleDtTime = COleDateTime::GetCurrentTime();
		m_clNextScheduleDtTime += COleDateTimeSpan(0,1,0,0);
		//m_clNextScheduleDtTime += COleDateTimeSpan(0,0,0,20);
		m_enmOperation = OP_None;
	}

	if( m_enmOperation == OP_Process )
	{
		AfxBeginThread(BlockOTOProcessor, this);
	}

	fnPageUpDown();
	DisplayOTODetail();

	if( m_enmOperation == OP_None )
			BlockProcessingScheduler( );

	if( m_enmOperation != OP_None )
			StartRecalcTimer(0, false);
	

	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpMsg,e_PgDnMsg,m_iMsgCounter,m_iNumofRowsMsg,m_iCurTopRowMsg);		
	DisplayLogMessages();	

	cout<<"End Recalculate"<<endl;
}

void CMEBlockProcessing::LoadOTODetail()
{
	if(m_vecOneTimeOrderDetail.size() > 0)
		m_vecOneTimeOrderDetail.clear(); 

	CString sQuery = "";
	sQuery.Format("EXEC Proc_Beast_Get_Subscriptions 'BLOCK', 1");

	_RecordsetPtr recordSet;
	if( FAILED(m_clpdbInt->GetRecordset(&recordSet, sQuery, false)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, sQuery);
		ASSERT(0);
		return;
	}
	
	while(VARIANT_FALSE == recordSet->adoEOF)
	{
		OneTimeOrderDtl clBlockOTO;

		SETLONG(clBlockOTO.iOrderId,			recordSet->Fields->Item[_variant_t("ORDER_ID")]->Value);
		SETSTR(	clBlockOTO.sCustName,			recordSet->Fields->Item[_variant_t("CUST_NAME")]->Value);	
		SETSTR(	clBlockOTO.sContactMail,		recordSet->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);
		SETSTR(	clBlockOTO.sFTPUserName,		recordSet->Fields->Item[_variant_t("FTPUserName")]->Value);
		SETSTR(	clBlockOTO.sFTPPassword,		recordSet->Fields->Item[_variant_t("FTPPassword")]->Value);	
		SETSTR(	clBlockOTO.sAllProducts,		recordSet->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);	
		SETSTR(	clBlockOTO.sProductCode,		recordSet->Fields->Item[_variant_t("PROD_CODE")]->Value);
		SETSTR(	clBlockOTO.sBlockCode,			recordSet->Fields->Item[_variant_t("Sub_Product_Code")]->Value);	
		SETSTR(	clBlockOTO.sExchange,			recordSet->Fields->Item[_variant_t("Exchange")]->Value);
		SETBFDATE( clBlockOTO.OrderStartDate,	recordSet->Fields->Item[_variant_t("Startdate")]->Value);
		SETBFDATE( clBlockOTO.OrderEndDate,		recordSet->Fields->Item[_variant_t("EndDate")]->Value);
		clBlockOTO.bOrderProcessed = false;
		m_vecOneTimeOrderDetail.push_back( clBlockOTO );		
		recordSet->MoveNext();
	}
	recordSet->Close();
}

void CMEBlockProcessing::fnPageUpDown()
{
	int iTotalRecord = m_vecOneTimeOrderDetail.size();
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpOTO,e_PgDnOTO,iTotalRecord,m_iNumofRowsOTO,m_iCurTopRowOTO);	
}

void CMEBlockProcessing::DisplayOTODetail()
{	
	int iRow = 0;
	for(unsigned int iIndex = m_iCurTopRowOTO; iIndex < m_vecOneTimeOrderDetail.size(); iIndex++, iRow++)
	{
		if( iRow >= m_iNumofRowsOTO )
			break;

		OneTimeOrderDtl &clBlockOTO = m_vecOneTimeOrderDetail[iIndex];

		GetField(e_OrderID			+ iRow)->SetValueInt(clBlockOTO.iOrderId);  
		GetField(e_CUST_NAME		+ iRow)->SetValueString(clBlockOTO.sCustName);  
		GetField(e_CONTACT_EMAIL	+ iRow)->SetValueString(clBlockOTO.sContactMail);  
		GetField(e_FTP_USER_NAME	+ iRow)->SetValueString(clBlockOTO.sFTPUserName);
		GetField(e_FTP_USER_NAME	+ iRow)->SetToolTipText(clBlockOTO.sFTPPassword);
		GetField(e_ALL_PRODUCTS		+ iRow)->SetValueString(clBlockOTO.sAllProducts);
		GetField(e_PROD_CODE		+ iRow)->SetValueString(clBlockOTO.sProductCode);
		GetField(e_ExchangeOTO		+ iRow)->SetValueString(clBlockOTO.sExchange);
		GetField(e_BlockCode		+ iRow)->SetValueString(clBlockOTO.sBlockCode);  
		GetField(e_OrderStartDt		+ iRow)->SetValueDate(clBlockOTO.OrderStartDate);
		GetField(e_OrderEndDt		+ iRow)->SetValueDate(clBlockOTO.OrderEndDate);
	}

	while( iRow < m_iNumofRowsOTO )
	{
		GetField(e_OrderID			+ iRow)->SetBlankState();  
		GetField(e_CUST_NAME		+ iRow)->SetBlankState();  
		GetField(e_CONTACT_EMAIL	+ iRow)->SetBlankState();  
		GetField(e_FTP_USER_NAME	+ iRow)->SetBlankState();  
		GetField(e_ALL_PRODUCTS		+ iRow)->SetBlankState();  
		GetField(e_PROD_CODE		+ iRow)->SetBlankState();  
		GetField(e_ExchangeOTO		+ iRow)->SetBlankState(); 
		GetField(e_BlockCode		+ iRow)->SetBlankState();
		GetField(e_OrderStartDt		+ iRow)->SetBlankState();  
		GetField(e_OrderEndDt		+ iRow)->SetBlankState();  

		iRow++;
	}

	CString sPageNumber = "";
	int iRowCount = m_vecOneTimeOrderDetail.size();
	if(iRowCount > 0)
	{
		int iCurrPage = m_iCurTopRowOTO / m_iNumofRowsOTO + 1;
		int iTotalPage;	
		if( (iRowCount % m_iNumofRowsOTO) == 0)
		{
			iTotalPage = (iRowCount/m_iNumofRowsOTO);	
		}
		else
		{
		
			iTotalPage = (iRowCount/m_iNumofRowsOTO) + 1;	
		}

		if(iTotalPage <= 0)
			iTotalPage = 1;
		sPageNumber.Format("%d/%d",iCurrPage,iTotalPage);
		GetField(e_Order_Navigate)->SetTitle(sPageNumber);
	}
	else
	{
		GetField(e_Order_Navigate)->SetTitle("0/0");
	}
}

bool CMEBlockProcessing::ReadBlockFileData(FILE *fileOut, FILE *fileIn, int *iRowCount)
{
	char chReadBuffer[2048];
	std::string strReadLine("");

	while( fgets(chReadBuffer, 2048, fileIn) )
	{
		if(strchr(chReadBuffer, '\n'))
		{
			strReadLine += chReadBuffer;

			fputs(strReadLine.c_str(), fileOut);
			
			strReadLine = "";
			(*iRowCount)++;

			if( (*iRowCount) == 50000 )
				return true;
		}
		else
		{
			strReadLine += chReadBuffer;
		}
	}

	fclose(fileIn);
	return false;
}

void CMEBlockProcessing::UploadUserProductCalData(CString sFTPUser)
{
	try
	{
	GetUserProductCalDetail(sFTPUser);
	m_sBlockSrcPath			 = BlockSourcePath->GetDisplayString();
	CString sFilePath		 = _T("");
	CString sFileName		 = _T("");
	CString sSourcePathFinal = _T("");
	CString sAwsCommand		 = _T("");
	int iWriteLineCount		 = 0;
	int	iFileCount			 = 0;
	CString sErrorMessage	 = _T("");
	CString	sFileStoragePath = CreateBlockDirectoryPath(sFTPUser);
	if(sFileStoragePath == _T("-1"))
	{
		fnWriteMessage("Error in creating file storage path");
		sErrorMessage = "Error in creating Directory structure: "+sFileStoragePath;
		fnWriteMessage(sErrorMessage, 3);
		fnWriteMessage(sErrorMessage, 2);
		return;
	}
	FILE *fileOut(NULL);	
	
	for(int iIndex = 0; iIndex < m_vecProductDetails.size(); iIndex++)
	{
		ProductDetails  &clObjProduct = m_vecProductDetails[iIndex];

		sSourcePathFinal = m_sBlockSrcPath + _T("/")+clObjProduct.sExchange+_T("/");
		sFilePath.Format(_T("%d/%02d/%d/"), clObjProduct.FileDate.GetYear(), clObjProduct.FileDate.GetMonth()+1, clObjProduct.FileDate.GetYYYYMMDD());	
		sSourcePathFinal   += sFilePath + clObjProduct.sFileName;

		/////////////// Download the file from s3 bucket & store it into local storage		
		
		sAwsCommand = "aws s3 cp "+sSourcePathFinal+" --profile s3user "+ sFileStoragePath;
		//sAwsCommand.Format(_T("aws s3 cp %s --profile s3user %s"),sSourcePathFinal,sFileStoragePath);
		fnWriteMessage(sAwsCommand, 2);
		//m_MessageLogger.LogMessage(m_sErrorMessage);
		int nRet = system(sAwsCommand);
		if( nRet != 0 )
		{
			sErrorMessage = "Unable to download file from s3 bucket: "+sSourcePathFinal;
			fnWriteMessage(sErrorMessage, 2);
			fnWriteMessage(sErrorMessage);
			continue;
		}
		///////////////////////////////////////////////////////////////////////////

		sSourcePathFinal   = sFileStoragePath + _T("\\") + clObjProduct.sFileName;

		FILE *fileIn(NULL);
		char chReadBuffer[2048];
		std::string strHeaderInfo("");
		
		if( fopen_s(&fileIn, sSourcePathFinal, "r") != 0 )
		{
			sErrorMessage = "Error in opening file: "+sSourcePathFinal;
			fnWriteMessage(sErrorMessage, 2);
			fnWriteMessage(sErrorMessage);
			continue;
		}
		else
		{
			// Read Header information ...
			while( fgets(chReadBuffer, 2048, fileIn) )
			{
				if( strchr(chReadBuffer, '\n') )
				{
					strHeaderInfo += chReadBuffer;
					break;
				}
				else
				{
					strHeaderInfo += chReadBuffer;
				}		
			}

			bool bContinue = true;
			do
			{
				if(iWriteLineCount == 50000)
				{
					fclose(fileOut);
					fileOut = NULL;
					iFileCount++;
					iWriteLineCount = 0;
				}

				if( fileOut == NULL )
				{
					if(iFileCount == 0)
						iFileCount = 1;

					sFileName.Format("Block_%03d_%d_%d.csv", iFileCount, m_MinStartDate.GetYYYYMMDD(), m_MaxEndtDate.GetYYYYMMDD());	
					//sFilePath.Format("%s\\%s", sFileStoragePath, sFileName);	
					sFilePath = sFileStoragePath+"\\"+sFileName;

					if( fopen_s(&fileOut, sFilePath,"w") )
					{
						//sErrorMessage.Format("Error in creating file: %s", sFilePath);
						sErrorMessage = "Error in creating file: "+sFilePath;
						fnWriteMessage(sErrorMessage, 2);
						//m_MessageLogger.LogMessage(m_sErrorMessage);
						return;
					}

					iWriteLineCount++;
					fputs( strHeaderInfo.c_str(), fileOut );
				}
				bContinue = ReadBlockFileData(fileOut, fileIn, &iWriteLineCount);
			}
			while(bContinue);
			DeleteFile(sSourcePathFinal);
		}
	}

	if( fileOut != NULL )
	{
		fclose( fileOut );
		fileOut = NULL;
	}

	if(iFileCount > 0)
	{
		CString sZipFileName = "";
		CString sZipFilePath = "";
		sZipFileName.Format("Block_%d_%d.zip", m_MinStartDate.GetYYYYMMDD(), m_MaxEndtDate.GetYYYYMMDD());	
		sZipFilePath = sFileStoragePath+"\\"+sZipFileName;

		HZIP hz = CreateZip(sZipFilePath,0);
		//sErrorMessage.Format("Zip file created: %s", sZipFilePath);
		sErrorMessage = "Zip file created: "+sZipFilePath;
		fnWriteMessage(sErrorMessage, 2);
		//m_MessageLogger.LogMessage(m_sErrorMessage);
		for( int iFile = 1; iFile <= iFileCount; iFile++ )
		{	 
			sFileName.Format("Block_%03d_%d_%d.csv", iFile, m_MinStartDate.GetYYYYMMDD(), m_MaxEndtDate.GetYYYYMMDD());

			//sFilePath.Format("%s\\%s", sFileStoragePath, sFileName);
			sFilePath = sFileStoragePath+"\\"+sFileName;
			ZipAdd(hz, sFileName, sFilePath );
			//sErrorMessage.Format("csv file (%s) added in Zip file", sFilePath);
			sErrorMessage = "csv file ("+ sFilePath + ") added in Zip file"; 
			fnWriteMessage(sErrorMessage, 2);
			//m_MessageLogger.LogMessage(m_sErrorMessage);
		}
		CloseZip(hz);

		CString sDestPathFinal   = m_lstS3Bucket->GetDisplayString();

		sDestPathFinal			+= sFTPUser+_T("/blocks/");
		//sAwsCommand.Format(_T("aws s3 cp %s --profile s3user %s"),sZipFilePath,sDestPathFinal);
		sAwsCommand = "aws s3 cp "+sZipFilePath+" --profile s3user "+ sDestPathFinal;

		int nRet = system(sAwsCommand);
		if( nRet == 0 )
		{
			fnWriteMessage("File Uploaded successfully");
			fnWriteMessage("File Uploaded successfully", 2);

			//sErrorMessage.Format("File Uploaded successfully: %s", sAwsCommand);
			sErrorMessage = "File Uploaded successfully: "+sAwsCommand;
			fnWriteMessage(sErrorMessage, 3);

			unsigned long lFileSize = GetFileSize(sZipFilePath.GetString());
			fnInsertDeliveryReport("OTO Block", sFTPUser, sZipFileName, lFileSize, sAwsCommand);

			CString sTemp = "";
			CString sTemp1 = "";
			for(unsigned int iIndex = m_iCurTopRowOTO; iIndex < m_vecOneTimeOrderDetail.size(); iIndex++)
			{
				OneTimeOrderDtl &clBlockOTO = m_vecOneTimeOrderDetail[iIndex];
				if(clBlockOTO.sFTPUserName == sFTPUser)
				{
					clBlockOTO.bOrderProcessed = true;
					sTemp1.Format("%d",clBlockOTO.iOrderId);
					sTemp += sTemp1 + ",";
				}
			}
			if(!sTemp.IsEmpty())
			{
				sTemp.SetAt(sTemp.GetLength()-1, ' ');
				sTemp.Trim();
				time_t     now = time(0);
				struct tm  tstruct;
				char       buf[80];
				tstruct = *localtime(&now);
				
				CString sQuery = "";
				CString sCurrentTime = "";

				strftime (buf,80,"%Y-%m-%d %H:%M:%S",&tstruct);
				sCurrentTime.Format("%s", buf);

				//sQuery.Format("Update CME_Order set DeliveryDate = '%s' , OrderStatus = 5 where order_id in (%s)",sCurrentTime, sTemp );
				sQuery = "Update CME_Order set DeliveryDate = '"+sCurrentTime+"' , OrderStatus = 5 where order_id in ("+sTemp+")";

				_RecordsetPtr set;
				if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sQuery, true)))
				{
					SetErrorMessage(ErrorSeverity::e_Error, sQuery);
					sErrorMessage = "Error in executing query ("+sQuery+")";
					fnWriteMessage(sErrorMessage, 2);
					fnWriteMessage(sErrorMessage);
					return;
				}
			}
		}
		else
		{
			fnWriteMessage("Error in uploading Zip file.");
			sErrorMessage = "Error in uploading file: "+sAwsCommand;
			fnInsertErrorReport("OTO Block", sFTPUser, sZipFileName, "", sAwsCommand);
			fnWriteMessage(sErrorMessage, 2);
			fnWriteMessage(sErrorMessage, 3);
		}
	}
	}
	catch(...)
	{
		fnWriteMessage("Crash UploadUserProductCalData", 2);
		//m_MessageLogger.LogMessage(m_sErrorMessage);
	}
}

void  CMEBlockProcessing::GetProductDetails(int iIndex)
{
	OneTimeOrderDtl &clBlockOTO = m_vecOneTimeOrderDetail[iIndex];
	
	CString sExchange		= clBlockOTO.sExchange;
	CString sProductName	= clBlockOTO.sBlockCode;	
	BFDate  bfStartDt		= clBlockOTO.OrderStartDate;
	BFDate  bfEndDt			= clBlockOTO.OrderEndDate;
	CString	sErrorMessage	= _T("");
	if( bfStartDt > bfEndDt )
	{
		fnWriteMessage("Invalid Start & End date range for selected order");
		//GetField(e_Status)->SetValueString(m_sStatusMsg);
		fnWriteMessage("Invalid Start & End date range for selected order", 2);
		//m_MessageLogger.LogMessage(m_sErrorMessage);
		return;
	}
	
	CString sStartDt = "", sEndDt = "";
	sStartDt.Format("%d-%02d-%02d",	bfStartDt.GetYear(), bfStartDt.GetMonth()+1, bfStartDt.GetDate());
	sEndDt.Format("%d-%02d-%02d",	bfEndDt.GetYear(), bfEndDt.GetMonth()+1, bfEndDt.GetDate());

	CString sQuery = "";
	sQuery.Format("SELECT * FROM dbo.CME_Product_Calendar WHERE FileType = 'Blocks' AND Product = '%s' AND Exchange ='%s' AND FileDate >= '%s' AND FileDate <= '%s' order by FileDate", sProductName, sExchange, sStartDt, sEndDt);
	_RecordsetPtr recordSet;
	if( FAILED( m_clpdbInt->GetRecordset(&recordSet, sQuery, false) ) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sQuery);
		//sErrorMessage.Format("Error in query: %s", sQuery);
		sErrorMessage = "Error in executing query ("+sQuery+")";
		fnWriteMessage(sErrorMessage);
		//GetField(e_Status)->SetValueString(m_sStatusMsg);
		fnWriteMessage(sErrorMessage, 2);
		//m_MessageLogger.LogMessage(m_sErrorMessage);
		return;
	}
		
	while (VARIANT_FALSE == recordSet->adoEOF)
	{
		ProductDetails objProduct ;

		SETSTR(	objProduct.sFileName,		recordSet->Fields->Item[_variant_t("FileName")]->Value);
		SETSTR(	objProduct.sFileType,		recordSet->Fields->Item[_variant_t("FileType")]->Value);
		SETBFDATE( objProduct.FileDate,		recordSet->Fields->Item[_variant_t("FileDate")]->Value);
		SETSTR(	objProduct.sExchange,		recordSet->Fields->Item[_variant_t("Exchange")]->Value);
		SETSTR(	objProduct.sProduct,		recordSet->Fields->Item[_variant_t("Product")]->Value);
		SETSTR(	objProduct.sFutOpt,			recordSet->Fields->Item[_variant_t("FutOpt")]->Value);
		SETSTR(	objProduct.sSpread,			recordSet->Fields->Item[_variant_t("Spread")]->Value);
		objProduct.sBlockCode				= clBlockOTO.sBlockCode;
		SETLONGLONG(objProduct.ulFileSize,	recordSet->Fields->Item[_variant_t("Size")]->Value);

		m_vecProductDetails.push_back(objProduct);

		recordSet->MoveNext();
	}
	recordSet->Close();
}

void CMEBlockProcessing::GetUserProductCalDetail(CString sFTPUser)
{
	if(m_vecProductDetails.size() > 0)
		m_vecProductDetails.clear();

	int iFirstTime = 0;
	for(int iIndex = 0; iIndex < m_vecOneTimeOrderDetail.size(); iIndex++)
	{
		OneTimeOrderDtl &clBlockOTO = m_vecOneTimeOrderDetail[iIndex];
		
		if(clBlockOTO.sFTPUserName == sFTPUser)
		{	
			if(iFirstTime == 0)
			{
				m_MinStartDate = clBlockOTO.OrderStartDate;
				m_MaxEndtDate  = clBlockOTO.OrderEndDate;
				iFirstTime = 1;
			}
			if(clBlockOTO.OrderStartDate < m_MinStartDate)
					m_MinStartDate = clBlockOTO.OrderStartDate;

			if(clBlockOTO.OrderEndDate > m_MaxEndtDate)
					m_MaxEndtDate = clBlockOTO.OrderEndDate;

			GetProductDetails(iIndex);
		}
	}
	if(m_vecOneTimeOrderDetail.size() > 0)
	{
		CString sMessage = "";
		sMessage.Format("Product Detail for User[%s]: %d Record(s) Found ..!!",sFTPUser, m_vecProductDetails.size());
		fnWriteMessage(sMessage, 2);
		fnWriteMessage(sMessage, 3);
		fnWriteMessage(sMessage);
	}
}

CString CMEBlockProcessing::CreateBlockDirectoryPath(CString sUserName)
{
	std::string sTime = currentDateTime();
	CString csTime = sTime.c_str();
	CString sPath = _T("");
	//sPath.Format(_T("D:\\OTO\\Block\\%s\\%s"), sTime.c_str(), sUserName);
	sPath = "D:\\OTO\\Block\\"+csTime+"\\"+sUserName;
	struct stat buffer;    
	if (stat(sPath, &buffer) == 0)
		return sPath;
	else
	{
		HWND hwnd = NULL;
		//const SECURITY_ATTRIBUTES *psa = NULL;
		if(SHCreateDirectoryEx(hwnd, sPath, NULL) != ERROR_SUCCESS)
			return _T("-1");
	}
	return sPath;
}

void CMsgLogger::LogMessage(CString sMessage)
{
	FILE *pLogFile = NULL;
	errno_t err;
	const char *plogFilePath = m_strLogFIlePath.c_str();
		
	if(!IsFileExist("D:\\OTO\\Block"))
	{
		HWND hwnd = NULL;
		//const SECURITY_ATTRIBUTES *psa = NULL;
		SHCreateDirectoryEx(hwnd, "D:\\OTO\\Block\\", NULL);
	}

	if(IsFileExist(m_strLogFIlePath))
	{
		err = fopen_s(&pLogFile, plogFilePath ,"a+");
	}
	else
	{
		 err = fopen_s(&pLogFile, plogFilePath ,"w");
		 CString csFileHeader(_T("TimeStamp\t\t\tMessage"));
		 if(err == 0)
		 {
			fprintf(pLogFile, "%s\n", csFileHeader);
		 }
	}
	if (err == 0)
	{
		COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
		CString csTimeStamp, csFinalMsg;
		csTimeStamp.Format("%d-%02d-%02d %02d:%02d:%02d",dtCurrentTime.GetYear(), dtCurrentTime.GetMonth(), dtCurrentTime.GetDay(), dtCurrentTime.GetHour(), dtCurrentTime.GetMinute(), dtCurrentTime.GetSecond());
		csFinalMsg = "OTO Block Processing: "+csTimeStamp+"\t"+sMessage+"\n";
		//fprintf(pLogFile, "OTO Block Processing: %s\t%s\n", csTimeStamp, sMessage);
		fprintf(pLogFile,csFinalMsg);
		fclose(pLogFile);
	}
}

void CMEBlockProcessing::BlockProcessingScheduler( )
{
	COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
	long lRecalcTime = 0;
	if( m_enmOperation == OP_Manual )
	{
		GetField(e_NextProcessing)->SetTitle("Processing Set To Manaul");
	}
	else if( dtCurrentTime >= m_clNextScheduleDtTime)
	{
		// Process the OTO order
		GetField(e_NextProcessing)->SetTitle("OTO Order Processing");
		m_enmOperation = OP_Process;
		fnWriteMessage("Processing scheduled OTO for Block", 3);
	}
	else if( dtCurrentTime < m_clNextScheduleDtTime )
	{
		COleDateTimeSpan dtSpan = m_clNextScheduleDtTime - dtCurrentTime;
		lRecalcTime = dtSpan.GetTotalSeconds();

		CString csTimer,csTmp;

		int iDays =	dtSpan.GetDays();
		int iLeftTotalHr = dtSpan.GetHours();
		int iLeftTotalMinute = dtSpan.GetMinutes();
		int iLeftTotalSecond = dtSpan.GetSeconds();

		if( iDays > 0 )
		{
			csTmp.Format("%d Day and ",iDays);					
			csTimer += csTmp;

			csTmp.Format("%02d:",iLeftTotalHr);					
			csTimer += csTmp;
		}
		else
		{
			if( iLeftTotalHr > 0 )
			{					
				csTmp.Format("%02d:",iLeftTotalHr);					
				csTimer += csTmp;
			}
		}

		csTmp.Format("%02d:",iLeftTotalMinute);					
		csTimer += csTmp;

		csTmp.Format("%02d",iLeftTotalSecond);
		csTimer += csTmp;				

		int iDate	= m_clNextScheduleDtTime.GetDay();
		int iMonth	= m_clNextScheduleDtTime.GetMonth();
		int iYear	= m_clNextScheduleDtTime.GetYear();
		int iHour	= m_clNextScheduleDtTime.GetHour();
		int iMinute = m_clNextScheduleDtTime.GetMinute();
		int iSecond = m_clNextScheduleDtTime.GetSecond();

		CString csStr;
		//csStr.Format("Next Block OTO Processing: %02d-%02d-%d %02d:%02d:%02d [Timer: %s]",iDate,iMonth,iYear,iHour,iMinute,iSecond, csTimer);
		csStr.Format("Next Block OTO Processing: %02d-%02d-%d %02d:%02d:%02d ",iDate,iMonth,iYear,iHour,iMinute,iSecond);
		csStr += "Timer: "+csTimer;

		GetField(e_NextProcessing)->SetTitle(csStr);
		GetField(e_NextProcessing)->SetTitleForeColor(ColorManager::eSignalPositiveLight);

		if( lRecalcTime > 4 || lRecalcTime < 0 )
			lRecalcTime = 4;
	}

	if( lRecalcTime )
		StartRecalcTimer(lRecalcTime, false);
}

void CMEBlockProcessing::fnInsertDeliveryReport(CString csPrdouct, CString csDestAccount, CString csFileName, unsigned long &lFileSize, CString &csAwscmd )
{
	CString csCheckSum = "";
	CString csExchange = "";
	CString csFileSize = "";
	CString csDeliveryTime;
	COleDateTime oleDateTime = COleDateTime::GetCurrentTime();
	csDeliveryTime.Format("%02d-%02d-%02d %02d:%02d", oleDateTime.GetYear(), oleDateTime.GetMonth(), oleDateTime.GetDay(), oleDateTime.GetHour(), oleDateTime.GetMinute());
	csFileSize.Format("%ld", lFileSize);

	CString csSql;
	csSql.Format("EXEC Proc_Beast_Submit_CME_Daily_Delivery_Report '%s', '%s', '%s', %u, '%s','%s','%s','%s'", csPrdouct, csDestAccount, csFileName, lFileSize, csCheckSum, csDeliveryTime, csExchange, csAwscmd);
	
	_RecordsetPtr set;
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)csSql, true)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSql);
		PrintRawMessage(csSql);
		return;
	}
	//csExchange.Format("Success-> Destination Account:\t%s\t\tFile Name:\t%s\t\tFile Size:\t%ld\n", csDestAccount, csFileName, lFileSize);
	csExchange = "Success-> Destination Account:\t"+csDestAccount+"\t\tFile Name:\t"+csFileName+"t\tFile Size:\t"+csFileSize+"\n";
	m_sMailContent += csExchange;
}

void CMEBlockProcessing::fnInsertErrorReport(CString csPrdouct, CString csDestAccount, CString csFileName, CString csExchnage, CString &csErrorCmd )
{	
	CString csSql;
	csSql.Format("EXEC Proc_Beast_Submit_CME_Daily_Error_Report '%s', '%s', '%s', '%s', '%s'", csPrdouct, csDestAccount, csFileName, csExchnage, csErrorCmd);
	
	_RecordsetPtr set;
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)csSql, true)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSql);
		PrintRawMessage(csSql);
		return;
	}
	//csSql.Format("Error-> Destination Account:\t%s\t\tAws Command:\t%s\n", csDestAccount, csErrorCmd);
	csSql = "Error-> Destination Account:\t"+csDestAccount+"\t\tAws Command:\t"+csErrorCmd+"\n";
	m_sMailContent += csSql;
}

const CString	__strSmtp	= "email-smtp.us-east-1.amazonaws.com";
const CString	__strLogin	= "AKIAIERFOJMRYCRRAHYQ";
const CString	__strPassword = "AvauV+bh9qVBlRQuCI5u9GMG2O1N2hajyhzE7MhyYANC"; 
const CString	__strSender	= "cmenotifications@thebeastapps.com";
const int		__iPort = 587;

void  CMEBlockProcessing::fnSendEmail(int iType, int iRowCount)
{
	//printf("fnSendEmail Start\n");
	CSmtp mail;

	mail.SetSMTPServer(__strSmtp, __iPort, true);
	mail.SetSecurityType((SMTP_SECURITY_TYPE)USE_TLS);		
	mail.SetLogin(__strLogin);
	mail.SetPassword(__strPassword);
			
	//CC
	/*CString strSendTo		= GetField(e_T0)->GetValueString();
	CString strCC			= GetField(e_CC)->GetValueString();
	CString strSubject		= GetField(e_Subject)->GetValueString();
	CString strBody			= GetField(e_Body)->GetValueString();*/
	
	CString strSendTo		= "vcmops@thebeastapps.com,";
	CString strCC			= "mpatel@thebeastapps.com,njethawa@thebeastapps.com,mvpatel@thebeastapps.com,";
	//CString strSendTo		= "njethawa@thebeastapps.com,";
	//CString strCC			= "mpatel@thebeastapps.com,";
	CString strSubject		= "";
	CString strBody			= "";
	CString strTemp			= "";

	if( iType == 1 )
	{
		strSubject = "OTO Block Notification";
		//m_clScheduleInfo.csFilePath
		/*strBody.Format("%s\n",m_sMailContent);

		strTemp.Format("Total Row Count: %d\n\n",iRowCount);
		strBody += strTemp;*/

		//strTemp.Format("\n\nThanks\nBeast Apps Team\n");
		m_sMailContent += "\n\nThanks\nBeast Apps Team\n";
		strBody = m_sMailContent;
	}

	
	//++++++++++++++++++++++++Send To+++++++++++++++++++++++
	if( !strSendTo.IsEmpty() )
	{
		if( strSendTo.Right(1).CompareNoCase(",") != 0 )
		{
			strSendTo = strSendTo + ",";
		}		
	}	
	
	if( strSendTo.Find(",") != -1 )
	{
		while(!strSendTo.IsEmpty())
		{
			CString tmpSendTo = strSendTo.Left(strSendTo.Find(","));			
			strSendTo.Delete(0,strSendTo.Find(",")+1);		
			tmpSendTo.TrimLeft();
			tmpSendTo.TrimRight();

			if(!tmpSendTo.IsEmpty())
				mail.AddRecipient(tmpSendTo);
		}
	}		
	else
	{			
		if( !strSendTo.IsEmpty() )
			mail.AddRecipient(strSendTo);
	}
	//+++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++ CC +++++++++++++++++++++++
	if( !strCC.IsEmpty() )
	{
		if( strCC.Right(1).CompareNoCase(",") != 0 )
		{
			strCC = strCC + ",";
		}		
	}		

	if( strCC.Find(",") != -1 )
	{
		while(!strCC.IsEmpty())
		{
			CString tmpCC = strCC.Left(strCC.Find(","));			
			strCC.Delete(0,strCC.Find(",")+1);		
			tmpCC.TrimLeft();
			tmpCC.TrimRight();

			if(!tmpCC.IsEmpty())
				mail.AddCCRecipient(tmpCC);
		}
	}		
	else
	{			
		if(!strCC.IsEmpty())
			mail.AddCCRecipient(strCC);
	}
	//+++++++++++++++++++++++++++++++++++++++++++++++
		
	mail.SetSenderMail(__strSender);
				
	if( !strBody.IsEmpty() )
		mail.AddBody(strBody);
				
	if( !strSubject.IsEmpty() )
		mail.SetSubject(strSubject);
		
	try
	{			
		mail.Send(NULL);
		fnWriteMessage("Mail Sent successfully.");
		//GetField(e_Status)->SetValueString(m_sStatusMsg);
	}
	catch(ECSmtp e)
	{
		CString sTemp;
		//sTemp.Format("Mail Failed...! - %s", e.GetErrorText());	
		sTemp = "Mail Failed...! - "+e.GetErrorText();
		fnWriteMessage(sTemp);
		fnWriteMessage(sTemp, 3);
		m_sMailContent = _T("");
		return;
	}
	catch(...)
	{
		CString sTemp;
		sTemp.Format("Mail Failed...! - Error Id: %d", GetLastError());	
		fnWriteMessage(sTemp);
		fnWriteMessage(sTemp, 3);
		m_sMailContent = _T("");
		return;
	}
	fnWriteMessage("EMail sent successfully", 3);
	m_sMailContent = _T("");
	//printf("fnSendEmail End\n");
}