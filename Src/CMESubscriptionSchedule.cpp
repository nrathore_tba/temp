#include <stdafx.h> 
#include "CMESubscriptionSchedule.hpp"

#define SPLITLINE 20000

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(CMESubscriptionSchedule)

CMESubscriptionSchedule::CMESubscriptionSchedule()
{	
}

CMESubscriptionSchedule::~CMESubscriptionSchedule()
{
}

bool CMESubscriptionSchedule::InitData()
{
	GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));

	m_curTopRow = 0;
	for(m_numofRows = 0; GetField(e_FileTypeBase + m_numofRows); m_numofRows++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	//---------------------------------------------

	fld_FileType = (ListField*)GetField(e_FileType);
	fld_Year = (ListField*)GetField(e_Year);
	fld_Month = (ListField*)GetField(e_Month);
	fld_SubType = (ListField*)GetField(e_SubType);
	fld_Houre = (ListField*)GetField(e_Houre);
	fld_Minute = (ListField*)GetField(e_Minute);

	if( !IsRestoreUpdate( ) )
	{
		COleDateTime dtToday = COleDateTime::GetCurrentTime();
		BFDate bfToday;
		bfToday.SetYearMonthDate(dtToday.GetYear(),dtToday.GetMonth() - 1, dtToday.GetDay() );
		
		fld_Year->SetValueInt(bfToday.GetYear()); 
		fld_Month->SetValueInt(bfToday.GetMonth()+1); 

		GetField(e_FilterYear)->SetValueInt(bfToday.GetYear()); 
		GetField(e_FilterMonth)->SetValueInt(bfToday.GetMonth()+1);
		GetField(e_FilterDay)->SetValueInt(bfToday.GetDate());
	}

	return true;
}

void CMESubscriptionSchedule::ChangeValidatation( )
{
	if( fld_FileType->GetValueInt() == 0 )
	{
		fld_SubType->SetNormalState();
		fld_SubType->SetEnabled(true);
		fld_SubType->SetDataString("FText~Fix Text|FBinary~Fix Binary");

		if( fld_SubType->GetValueInt() > 1 )
			fld_SubType->SetValueInt(0);
	}
	else if( fld_FileType->GetValueInt() == 1 )
	{
		fld_SubType->SetNormalState();
		fld_SubType->SetEnabled(true);
		fld_SubType->SetDataString("E~Early|P~Preliminary|F~Final");
	}
	else
	{
		fld_SubType->SetBlankState();
		fld_SubType->SetEnabled(false);
	}

	if( GetField(e_AdminBtn)->GetValueInt() == 1 )
	{
		GetField(e_CreateSchedule)->SetEnabled( true );
		GetField(e_CreateAllScheduleFromDay)->SetVisible( true );
		GetField(e_DeleteaAllRemainingSchedule)->SetVisible( true );		
	}
	else
	{
		GetField(e_CreateSchedule)->SetEnabled( false );
		GetField(e_CreateAllScheduleFromDay)->SetVisible( false );
		GetField(e_DeleteaAllRemainingSchedule)->SetVisible( false );		
	}
}

void CMESubscriptionSchedule::Recalculate()
{
	if( IsFieldNew(e_CreateSchedule) )
	{
		fnCreateSchedules( );

		GetField(e_AdminBtn)->SetValueInt(0);
		GetField(e_CreateSchedule)->SetEnabled( false );
		GetField(e_CreateAllScheduleFromDay)->SetValueInt(0);
	}

	if( IsFieldNew(e_DeleteaAllRemainingSchedule) )
	{
		fnRemoveSchedules( );

		GetField(e_AdminBtn)->SetValueInt(0);
		GetField(e_CreateSchedule)->SetEnabled( false );
	}

	ChangeValidatation( );

	if( IsFieldNew(e_LoadSchedule) || IsInitialRecalc() || IsUserUpdate() )
	{
		LoadSchedule();	
	}	
	
	fnPageUpDown( );
	DisplaySubscription( );
}

void CMESubscriptionSchedule::fnCreateSchedules( )
{
	CString csFileType = fld_FileType->GetShortString();
	BFDate bfDate;
	if( GetField(e_Day)->GetValueInt() == 0 )
		bfDate = BFDate(fld_Year->GetValueInt(), fld_Month->GetValueInt()-1, 1);
	else
		bfDate = BFDate(fld_Year->GetValueInt(), fld_Month->GetValueInt()-1, GetField(e_Day)->GetValueInt());

	CString csSubType;

	if( fld_SubType->IsBlankState() )
		csSubType = "";
	else
		csSubType = fld_SubType->GetShortString();
	
	while( fld_Month->GetValueInt() == bfDate.GetMonth()+1 )
	{
		CString csDate;
		csDate.Format("%d-%02d-%02d %02d:%02d:00.000",bfDate.GetYear(), bfDate.GetMonth()+1, bfDate.GetDate() , fld_Houre->GetValueInt(), fld_Minute->GetValueInt() );

		CString sql;
		sql.Format("EXEC Proc_Beast_Create_CME_Subscription_Schedule '%s','%s','%s'", csFileType, csSubType, csDate);

		_RecordsetPtr set;
		if( FAILED(m_clpdbInt->GetRecordset(&set, sql)) )
		{
			SetErrorMessage(ErrorSeverity::e_Error, sql);
			return;
		}

		bfDate.OffsetDate(1);

		if( GetField(e_Day)->GetValueInt() ^ 0 && GetField(e_CreateAllScheduleFromDay)->GetValueInt() == 0 )
			break;		
	}
}

void CMESubscriptionSchedule::fnRemoveSchedules( )
{
	CString csFileType = fld_FileType->GetShortString();

	CString csSubType;
	if( fld_SubType->IsBlankState() )
		csSubType = "";
	else
		csSubType = fld_SubType->GetShortString();

	COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
	BFDate bfDate = dtCurrentTime.m_dt;
	bfDate.OffsetDate(1);

	CString csDate;
	csDate.Format("%d-%02d-%02d 00:00:00",bfDate.GetYear(), bfDate.GetMonth()+1, bfDate.GetDate());
		
	CString sql;
	sql.Format("DELETE FROM CME_Subscription_Schedule WHERE FileType = '%s' AND SubType = '%s' and ScheduleDateTime >= '%s'", csFileType, csSubType, csDate);

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		return;
	}
}

void CMESubscriptionSchedule::LoadSchedule()
{
	m_clScheduleDtlV.clear();

	CString csFileType = ((ListField*)GetField(e_FilterFileType))->GetShortString();
	int iStatus = GetField(e_FilterStatus)->GetValueInt();

	BFDate bfStartDate;
	BFDate bfEndDate;
	CString csScheduleStart, csScheduleEnd;
	if( GetField(e_FilterDay)->GetValueInt() == 0 )
	{
		bfStartDate = BFDate(GetField(e_FilterYear)->GetValueInt(), GetField(e_FilterMonth)->GetValueInt()-1, 1);
		bfEndDate = bfStartDate;
		bfEndDate.OffsetMonth(1);		
	}
	else
	{
		bfStartDate = BFDate(GetField(e_FilterYear)->GetValueInt(), GetField(e_FilterMonth)->GetValueInt()-1, GetField(e_FilterDay)->GetValueInt());
		bfEndDate = bfStartDate;
		bfEndDate.OffsetDate(1);
	}
		
	csScheduleStart.Format("%d-%02d-%02d 00:00:00.000",bfStartDate.GetYear(), bfStartDate.GetMonth()+1, bfStartDate.GetDate() );
	csScheduleEnd.Format("%d-%02d-%02d 00:00:00.000",bfEndDate.GetYear(), bfEndDate.GetMonth()+1, bfEndDate.GetDate() );
		
	CString sql;	
	if( csFileType == "ALL" )
	{
		if( iStatus == -1 )
			sql.Format("SELECT * from dbo.CME_Subscription_Schedule WHERE ScheduleDateTime >= '%s' And ScheduleDateTime < '%s'", csScheduleStart, csScheduleEnd);
		else
			sql.Format("SELECT * from dbo.CME_Subscription_Schedule WHERE ScheduleDateTime >= '%s' And ScheduleDateTime < '%s' AND IsNULL([status],0) = %d", csScheduleStart, csScheduleEnd, iStatus);
	}
	else
	{	
		if( iStatus == -1 )
			sql.Format("SELECT * from dbo.CME_Subscription_Schedule WHERE FileType = '%s' AND ScheduleDateTime >= '%s' And ScheduleDateTime < '%s'", csFileType, csScheduleStart, csScheduleEnd);
		else
			sql.Format("SELECT * from dbo.CME_Subscription_Schedule WHERE FileType = '%s' AND ScheduleDateTime >= '%s' And ScheduleDateTime < '%s' AND IsNULL([status],0) = %d", csFileType, csScheduleStart, csScheduleEnd, iStatus);
	}
	
	sql += " Order By ScheduleDateTime";

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		return;
	}

	ScheduleDtl clScheduleDtl;
	
	while(VARIANT_FALSE == set->adoEOF)
	{	
		SETSTR(	clScheduleDtl.csFileType,	set->Fields->Item[_variant_t("FileType")]->Value);	
		SETSTR(	clScheduleDtl.csSubType,	set->Fields->Item[_variant_t("SubType")]->Value);	
		SETLONG(clScheduleDtl.iStatus,		set->Fields->Item[_variant_t("Status")]->Value);	
		SETOLEDATE(	clScheduleDtl.clScheduleDtTime,		set->Fields->Item[_variant_t("ScheduleDateTime")]->Value);

		SETOLEDATE(	clScheduleDtl.oleStartDtTime,	set->Fields->Item[_variant_t("ProcessStart")]->Value);
		SETOLEDATE(	clScheduleDtl.oleEndDtTime,		set->Fields->Item[_variant_t("ProcessEnd")]->Value);
		
		m_clScheduleDtlV.push_back( clScheduleDtl );
		set->MoveNext();
	}

	set->Close();	

	CString csTemp;
	csTemp.Format("%d Schedule loaded", m_clScheduleDtlV.size() );
	GetField(e_Message)->SetTitle(csTemp );
}


void CMESubscriptionSchedule::fnUpdateScheduleStatus( int iStatus, ScheduleDtl &clScheduleDtl )
{
	//if( iStatus ^ 1 && iStatus ^ 3 )
	//	return;

	CString csdttime;
	csdttime.Format("%d-%02d-%02d %02d:%02d:00",clScheduleDtl.clScheduleDtTime.GetYear(), clScheduleDtl.clScheduleDtTime.GetMonth(), clScheduleDtl.clScheduleDtTime.GetDay(), clScheduleDtl.clScheduleDtTime.GetHour(), clScheduleDtl.clScheduleDtTime.GetMinute());
	
	COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
	CString csTimeStatus;
	csTimeStatus.Format("%d-%02d-%02d %02d:%02d:00",dtCurrentTime.GetYear(), dtCurrentTime.GetMonth(), dtCurrentTime.GetDay(), dtCurrentTime.GetHour(), dtCurrentTime.GetMinute());

	CString sql;
	sql.Format("UPDATE dbo.CME_Subscription_Schedule SET Status = %d, ProcessEnd = '%s' WHERE FileType ='%s' and SubType = '%s' and ScheduleDateTime = '%s'", iStatus, csTimeStatus, clScheduleDtl.csFileType, clScheduleDtl.csSubType, csdttime);

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		return;
	}

	clScheduleDtl.iStatus = iStatus;
}

void CMESubscriptionSchedule::fnPageUpDown( )
{
	int iTotalRecord = m_clScheduleDtlV.size( );
	
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpBase,e_PgDnBase,iTotalRecord,m_numofRows,m_curTopRow);	
}

void CMESubscriptionSchedule::DisplaySubscription()
{	
	bool bAdmin = (GetField(e_AdminBtn)->GetValueInt() == 1);
	CString csTime;
	int iRow = 0;
	for(int iIndex = m_curTopRow; iIndex < m_clScheduleDtlV.size(); iIndex++, iRow++)
	{
		if( iRow >= m_numofRows )
			break;

		ScheduleDtl &clsche = m_clScheduleDtlV[iIndex];

		if( IsFieldNew( e_StatusBase + iRow) )
			fnUpdateScheduleStatus( GetField(e_StatusBase + iRow)->GetValueInt(), clsche );

		GetField(e_FileTypeBase	+ iRow)->SetValueString(clsche.csFileType);  
		GetField(e_SubTypeBase	+ iRow)->SetValueString(clsche.csSubType);

		BFDate bfDate(clsche.clScheduleDtTime.GetYear(), clsche.clScheduleDtTime.GetMonth()-1,clsche.clScheduleDtTime.GetDay());
		csTime.Format("%02d:%02d:00",clsche.clScheduleDtTime.GetHour(), clsche.clScheduleDtTime.GetMinute());

		GetField(e_DateBase + iRow)->SetValueDate(bfDate);  
		GetField(e_TimeBase + iRow)->SetValueString(csTime);  
		int iWeekDay(bfDate.GetDayOfWeek());

		if( iWeekDay == 0 || iWeekDay == 6 )
		{
			GetField(e_WeekDayBase + iRow)->SetForeColor(ColorManager::eNegativeChange);
		}
		else
		{
			GetField(e_WeekDayBase + iRow)->SetForeColor(ColorManager::eDefault);
		}

		GetField(e_WeekDayBase + iRow)->SetValueInt(iWeekDay);  

		GetField(e_StatusBase + iRow)->SetEnabled(bAdmin);
		GetField(e_StatusBase + iRow)->SetValueInt(clsche.iStatus);
		GetField(e_StatusBase + iRow)->SetNotManual();

		if( clsche.iStatus == 1 || clsche.iStatus == 2 )
		{
			csTime.Format("%d-%02d-%02d %02d:%02d:%02d", clsche.oleStartDtTime.GetYear(), clsche.oleStartDtTime.GetMonth(), clsche.oleStartDtTime.GetDay(), clsche.oleStartDtTime.GetHour(), clsche.oleStartDtTime.GetMinute(), clsche.oleStartDtTime.GetSecond());
			GetField(e_StartTimeBase + iRow)->SetValueString( csTime );
		}
		else
		{
			GetField(e_StartTimeBase + iRow)->SetBlankState();
		}

		if( clsche.iStatus == 2 )
		{
			csTime.Format("%d-%02d-%02d %02d:%02d:%02d", clsche.oleEndDtTime.GetYear(), clsche.oleEndDtTime.GetMonth(), clsche.oleEndDtTime.GetDay(), clsche.oleEndDtTime.GetHour(), clsche.oleEndDtTime.GetMinute(), clsche.oleStartDtTime.GetSecond());
			GetField(e_EndTimeBase + iRow)->SetValueString( csTime );
		}
		else
		{
			GetField(e_EndTimeBase + iRow)->SetBlankState();
		}
	}

	while( iRow < m_numofRows )
	{
		GetField(e_FileTypeBase	+ iRow)->SetBlankState();  
		GetField(e_SubTypeBase	+ iRow)->SetBlankState();  

		GetField(e_DateBase + iRow)->SetBlankState();  
		GetField(e_TimeBase + iRow)->SetBlankState();  
		GetField(e_WeekDayBase + iRow)->SetBlankState();  
		GetField(e_StatusBase + iRow)->SetBlankState(); 

		GetField(e_StartTimeBase + iRow)->SetBlankState();
		GetField(e_EndTimeBase + iRow)->SetBlankState();
		iRow++;
	}
}