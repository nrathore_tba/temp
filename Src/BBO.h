#pragma once



#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <limits>
#include <map>
#include <vector>
#include <algorithm>
#include <shared/BaseImage.hpp>

BF_NAMESPACE_BEGIN

// Test comment

struct BBOSubscriptionDtl;

struct BBOFileDtl
{
	BBOFileDtl()
	{
		fp=NULL;
		strOutputFileName="";
		_SplitCount=0;
		_vecFileList.clear();
	}
	CString strOutputFileName;
	long _SplitCount;
	long _fileCount;
	FILE *fp;
	std::vector<CString> _vecFileList;
};

struct stBBOMetaData
{
	std::string _strExch;
	std::string _strProd;
	std::string _strFO;
	std::string _strText;
	std::ofstream * _fpOutPutFile;
	std::vector<std::string> _strVecFileNames;
	long _count;
	stBBOMetaData()
	{
		_strExch ="";
		_strProd="";
		_strFO ="";
		_fpOutPutFile=NULL;
		_count=0;
		_strText="";
		_strVecFileNames.clear();
	}
};

class CBBO
{
	std::string _strInputFileName;
	std::string _strInputPath;
	std::string _strOutputFilePath;
	std::string _strExceptionFileName;
	std::string _strIniFile;
	std::string _strExchange;	
	long _interval;
	BaseImage *_pImage;
	std::ifstream fpInputFile;
	std::map<std::string,stBBOMetaData> _mapBBOMetaData;
	std::vector<std::string>_stVecProducts;
	std::ofstream fpExceptionFile;
	std::map<CString,BBOFileDtl> _mapFileDtl;

public:
	CBBO(void)
	{
		  _interval = 10000;
		 _strExchange = _strInputFileName=_strInputPath=_strOutputFilePath=_strExceptionFileName=_strIniFile="";
	}

	~CBBO(void);
	bool dirExists(const std::string& dirName_in);
	void setExchange(std::string &strExch) {_strExchange =strExch; }
	void setProcessParameters();
	std::string createUnzip(std::string sInputFile);
	void updateImageFields(long & lCnt);
	bool OpenExceptionFile();
	bool Process();
	void GenereateMetaData();
	bool WriteRecord(std::string &strKey,std::string &strExch, std::string & strProd, std::string & strFO,std::string & strLine);
	void CloseOpenFiles();
	bool OpenInputBBOFile(std::string strFileName);
	std::string formatTime(std::string  trdtime);
	void trim2(std::string & str);
	std::string formatEntryDate(std::string line);
	std::string formatPrice(std::string inPrice, std::string decimalLoc);
	void createZip();
	void UpdateOutputField(std::string &stroutput);
	void UploadAllSubscription();
	void UploadSingleSub(BBOSubscriptionDtl &clBlockSub);
	std::vector<std::string> get_all_files_names_within_folder(std::string & folder, std::string & cstime);
	const std::string currentDateTime() 
	{
		time_t     now = time(0);
		struct tm  tstruct;
		char       buf[80];
		tstruct = *localtime(&now);
		strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
		return buf;
	}
	std::vector<std::string> split(const std::string& s, char seperator)
	{
		std::vector<std::string> output;
		std::string::size_type prev_pos = 0, pos = 0;
		while((pos = s.find(seperator, pos)) != std::string::npos)
		{
			std::string substring( s.substr(prev_pos, pos-prev_pos) );
			output.push_back(substring);
			prev_pos = ++pos;
		}
		output.push_back(s.substr(prev_pos, pos-prev_pos)); // Last word
		return output;
	}
	void setInerval(long linterval)
	{
		_interval = linterval;
	}
	void SetImage(BaseImage *pImage)
	{
		_pImage = pImage;
	}
	void SetOutputBBOFilePath(std::string & strFilepath)
	{
		if(strFilepath.substr(strFilepath.length()-1,1) == "\\")
			_strOutputFilePath = strFilepath;
		else
			_strOutputFilePath = strFilepath +"\\";
	}
	std::string GetTime()
	{
		time_t     now = time(0);
		struct tm  tstruct;
		char       buf[80];
		localtime_s(&tstruct,&now);
		strftime(buf, sizeof(buf), "%Y%m%d", &tstruct);
		return std::string(buf);
	}
	void FlushOpenFiles()
	{
		std::map<std::string,stBBOMetaData>::iterator it = _mapBBOMetaData.begin();
		for(;it!=_mapBBOMetaData.end();it++)
		{
			it->second._fpOutPutFile->flush();
		}
	}
	void WriteFileProcessCount(std::string strFileName,long lcount)
	{
		char buff[100];
		_ltoa(lcount,buff,10);
		WritePrivateProfileString(strFileName.c_str(),"Count",buff,_strIniFile.c_str()) ;
		
	}
	long GetFileProcessCount(std::string strFileName)
	{
		char buff[100];
		GetPrivateProfileString(strFileName.c_str(),"Count","0",buff,100,_strIniFile.c_str());
		return atol(buff);
	}

	void GotoLine(std::ifstream& file, long num)
	{
		std::string s;
		for (long i = 1; i <= num; i++)
			std::getline(file, s);
	}

};

BF_NAMESPACE_END