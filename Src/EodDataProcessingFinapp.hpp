
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <shared/MDDDataHandler.hpp>

BF_NAMESPACE_BEGIN
using namespace std;

const string __strHeader = "Trade Date,Exchange Code,Asset Class,Product Code,Clearing Code,Product Description,Product Type ,Underlying Product Code,Put/Call ,Strike Price,Contract Year,Contract Month,Contract Day,Settlement,Settlement Cabinet Indicator,Open Interest,Total Volume,Globex Volume,Floor Volume,PNT Volume,Block Volume ,EFP Volume,EOO Volume,EFR Volume,EFS Volume,EFB Volume,EFM Volume,SUB Volume,OPNT Volume,TAS Volume,TAS Block Volume ,TAM Singapore Volume ,TAM Singapore Block Volume ,TAM London Volume ,TAM London Block Volume ,Globex Open Price,Globex Open Price Bid/Ask Indicator,Globex Open Price Cabinet Indicator,Globex High Price,Globex High Price Bid/Ask Indicator,Globex High Price Cabinet Indicator,Globex Low Price,Globex Low Price Bid/Ask Indicator,Globex Low Price Cabinet Indicator,Globex Close Price,Globex Close Price Bid/Ask Indicator,Globex Close Price Cabinet Indicator,Floor Open Price,Floor Open Price Bid/Ask Indicator,Floor Open Price Cabinet Indicator,Floor Open Second Price,Floor Open Second Price Bid/Ask Indicator,Floor High Price,Floor High Price Bid/Ask Indicator,Floor High Price Cabinet Indicator,Floor Low Price,Floor Low Price Bid/Ask Indicator,Floor Low Price Cabinet Indicator,Floor Close Price,Floor Close Price High Bid/Ask Indicator,Floor Close Price Cabinet Indicator,Floor Close Second Price,Floor Close Second Price Bid/Ask Indicator,Floor Post-Close Price,Floor Post-Close Price Bid/Ask Indicator,Floor Post-Close Second Price,Floor Post-Close Second Price Bid/Ask Indicator,Delta,Implied Volatility,Last Trade Date,\n";

// 72 columns..
enum ColumnName
{
	enm_TRADE_DATE,
	enm_LAST_TRADE_DATE, // this column present At last in output..
	enm_EXCH_MIC_CODE,
	enm_QUADRANT_ASSET,
	enm_TICKER_SYMBOL,
	enm_COMMODITY_CODE,
	enm_PRODUCT_DESCRIPTION,
	enm_FO_IND,
	enm_UNDERLYING_CONTRACT,
	enm_PC_IND,
	enm_STRIKE_PRICE,
	enm_CONTRACT_YR,
	enm_CONTRACT_MO,
	enm_CONTRACT_DAY,
	enm_SETL_PRICE,
	enm_SETL_CAB_IND,
	enm_OPEN_INTEREST,
	enm_TOTAL_VOL,
	enm_GLOBEX_VOL,
	enm_EXPIT_VOL,
	enm_PIT_VOL,
	enm_BLOCK_VOL,
	enm_EFP_VOL,
	enm_EOO_VOL,
	enm_EFR_VOL,
	enm_EFS_VOL,
	enm_EFB_VOL,
	enm_EFM_VOL,
	enm_SUB_VOL,
	enm_PNT_VOL,
	enm_TAS_VOL,
	enm_BLOCK_TAS_VOL,
	enm_TAMS_VOL,
	enm_TAMS_BLOCK_VOL,
	enm_TAML_VOL,
	enm_TAML_BLOCK_VOL,
	enm_ETH_OPEN1_PRICE,
	enm_ETH_OPEN1_AB_IND,
	enm_ETH_OPEN1_CAB_IND,
	enm_ETH_HI_PRICE,
	enm_ETH_HI_PRICE_AB_IND,
	enm_ETH_HI_PRICE_CAB_IND,
	enm_ETH_LO_PRICE,
	enm_ETH_LO_PRICE_AB_IND,
	enm_ETH_LO_PRICE_CAB_IND,
	enm_ETH_CLOSE1_PRICE,
	enm_ETH_CLOSE1_AB_IND,
	enm_ETH_CLOSE1_CAB_IND,
	enm_OPEN1_PRICE,
	enm_OPEN1_AB_IND,
	enm_OPEN1_CAB_IND,
	enm_OPEN2_PRICE,
	enm_OPEN2_AB_IND,
	enm_HI_PRICE,
	enm_HI_PRICE_AB_IND,
	enm_HI_PRICE_CAB_IND,
	enm_LO_PRICE,
	enm_LO_PRICE_AB_IND,
	enm_LO_PRICE_CAB_IND,
	enm_CLOSE1_PRICE,
	enm_CLOSE1_AB_IND,
	enm_CLOSE1_CAB_IND,
	enm_CLOSE2_PRICE,
	enm_CLOSE2_AB_IND,
	enm_PCLS_HI_PRICE,
	enm_PCLS_HI_AB_IND,
	enm_PCLS_IND,
	enm_PCLS_LO_PRICE,
	enm_PCLS_LO_AB_IND,
	enm_DELTA,
	enm_VOLATILITY,
	enm_END	
};


typedef std::map<CString, bool> PathList;

class EodDataProcessingFinapp : public BaseImage
{
public:
	
	enum FieldIDs
	{		
		e_Status	= 50,

		e_Message	= 61,
		e_StartTime	= 95,
		e_EndTime	= 96,
	
		e_InputFilePath		= 100,
		e_OutputFolderPath	= 101,
		
		e_ParseYear			= 102,
		e_Type				= 103,

		e_HiddenOutputFolder = 104,

		e_ParseFile			= 110,		
		e_Uncompress		= 111,
	};
	
	EodDataProcessingFinapp();
	~EodDataProcessingFinapp();	

	bool InitData();		
	void Recalculate();	

	/*void GenerateFileName(CString &strInputFile, CString &strOutputFile,CString &strUnzipFile);
	CString unzip(CString csFilepath,CString csFileName);
		
	void ParseFile(CString strInputFile);
	CString GetFileName(CString strInputFile);
	void ReadFile(CString strFilePath,CString strDate);

	void ExtractExchangeProductCol(CString s);
	void ExtractTokens(CString s, int iAll=0);	
	void GetExchanegProduct();

	std::vector<CString> m_ExchangeList;
	std::vector<stFileData> m_AllDetailVec;

	std::vector<stExchangeProduct> m_ExchangeProductFilterList;	*/

	static const int m_niInputColumnCount = 72;
	static const int m_niOutputColumnCount = 70;
private:

	void	fnUncompress( );	
	void	splitString(CString cStr, string delimiter, vector<string> &result);
	size_t	indexofColumn(vector<string> v, string searchKey);
	string	createProductFile(string outputPath, string strkey );
	void	GetRowMessage(vector<string> recordList , string &msg );

	ListField *fld_Type;
	ListField *fld_ParseYear;
	BaseField *fld_InputFilePath;
	BaseField *fld_HidOutputDir;
	BaseField *fld_Status;

	CString fnCreatePath( CString csDate, CString csExchange );	
		
	PathList m_clExchangePathList;
	PathList m_clExchangeYearPathList;
	PathList m_clExchangeYearMonthPathList;
	PathList m_clExchangeYearMonthDatePathList;		

public:
	void	autoParseFile( );

	bool	m_bIsThreadRunning;
	int		m_lTotalRead;	
};

BF_NAMESPACE_END


