
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>

//#ifdef _DEBUG
//	#pragma comment(lib,"\\Thebeast\\shared\\persistd.lib")
//	#pragma comment(lib,"\\SharedApps\\BFInterestRateSharedD.lib")
//#else
//	#pragma comment(lib,"\\SharedApps\\BFInterestRateShared.lib")
//	#pragma comment(lib,"\\Thebeast\\shared\\persistd.lib")
//#endif

BF_NAMESPACE_BEGIN

using namespace std;

struct FTPMessages
{
	CString csDateTime;
	CString csAction;
	CString csStatus;
	int nError;
};

struct ParserDetails
{
	string sGroupCode;
	string sExchange;
	string sProduct;
	string sSecurity;
	string sProductDesc;
	string sFOIType;
	int nSpreadIn;
};
class CMEHistoricalDataParser : public BaseImage
{
public:
	
	enum FieldIDs
	{
		e_FilePath=1,
		e_FileName=2,
		e_AutoParsed=3,
		e_ManualParsed=4,
		e_CompressedFilePath=6,
		e_CompressedFilename=7,
		e_Decompressed=8,
		e_ParsedFileLocation=11,
		e_lzopexeLocation=12,
		e_Message=13,

		//Status Message Field
		e_Message_DateTime	= 350,
		e_Message_Action	= 450,
		e_Message_Status	= 550
		

	};
	
	CMEHistoricalDataParser();
	~CMEHistoricalDataParser();	
	
	bool InitData();		
	void Recalculate();	

private:


	DBInterfaceNew	*m_clpdbInt;
	DBConnection	m_clDBConn;
	_RecordsetPtr m_recordSet;

	vector<ParserDetails> m_vcParserDetails;
	ParserDetails m_ParserDetailsRow;

	map<CString,FILE *> m_FileMap;
	map<CString,FILE *>::iterator m_Mapit;

	FILE *m_pFile;

	std::vector<FTPMessages> m_vMessages;
	FTPMessages m_Message;

	int nAutoDecompressed,nAutoParsedFlag;
	int nNumofSecurityId;
	string msecurityId,mExchange,mProduct,mProductDesc,mGroup,mHeader,mFooter;

	string mstrLine;
	int m_operation;
	int nevent;
	int m_TotalLines,m_iEvent;
	CString m_csFileName;
	//Initialize DataBase
	bool InitDataBase();

	//Read File line by line
	bool fnReadFile();

	//Open File in read mode
	bool fnOpenFile();

	//Check security id in memory
	bool fnCheckInMemory(string,string);

	//Check Security id in Stg table
	bool fnCheckInStg(string,string);

	//Check Security id in Master
	bool fnCheckInMaster(string,string);

	//Create Folder Structure and File
	bool fnCreateFolderStructure(string,string,string,string,int);

	void fnStoreInStg(string,string,string,string,string,string,int);
	
	void ExtractTokensToArray(string,string);

	//Get previous day file name
	void GetFileName();

	//submit values if 35=d
	bool fnSubmit_d_Values(string);
	void ExtractTokensToMultipleSecurity(string,string);

	//File Decompressed
	bool fnDecompressed();

	//File Parsed
	bool fnFileParsed();

	//for msg
	void AddMessage(FTPMessages &clarFTPMessages);
	void DisplayMessages();
};
BF_NAMESPACE_END