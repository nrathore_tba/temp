#include <fstream>
#include <sstream>
#include <stdafx.h> 
#include <shldisp.h>
#include <tlhelp32.h>
#include "CMEPriceMst.hpp"
#include <Fields\ListField.hpp>

#define MAX_ROWS 30

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(CMEPriceMst)

CMEPriceMst::CMEPriceMst()
{	
}

CMEPriceMst::~CMEPriceMst()
{
}

bool CMEPriceMst::InitData()
{
	m_iCurrTopRow = 0;
	
    GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));
	
	fld_ProductList = (ListField*)GetField(e_ProductList);	
	
	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return false;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();		
	return true;
}

void CMEPriceMst::Recalculate()
{	

	if( IsFieldNew(e_ProductList) || IsFieldNew(e_GetData) || IsInitialRecalc() )
	{
		GetFilterData();		
	}
		
	ManageUpDownButton();
	DisplayGrid();
}

CString _csFutOpt[ ] = { "", "", "FUT", "", "OPT", "", "FUT,OPT"};


void CMEPriceMst::GetFilterData()
{
	m_clPriceDtlV.clear();
	
	CString sql;	
	sql.Format("select * from CME_Product_Prices_Pakage_Mst where category_id = %d order by IsProcessed", fld_ProductList->GetValueInt());
		
	stPriceDtl objstPriceDtl;

	_RecordsetPtr set;	
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql, false)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		PrintRawMessage(sql);
		return;
	}

	while( VARIANT_FALSE == set->adoEOF )
	{
		SETLONG( objstPriceDtl.iPackageId,	set->Fields->Item[_variant_t("Package_Id")]->Value);	
		SETSTR(	objstPriceDtl.strProdName,	set->Fields->Item[_variant_t("Prod_Name")]->Value);	
		SETSTR(	objstPriceDtl.strExchange,	set->Fields->Item[_variant_t("Exchange")]->Value);	
		SETSTR(	objstPriceDtl.strAssetClass,	set->Fields->Item[_variant_t("AssetClass")]->Value);	
		SETLONG( objstPriceDtl.iDailyAmt,	set->Fields->Item[_variant_t("Daily_Amt")]->Value);	
		SETLONG( objstPriceDtl.iMonthlyAmt,	set->Fields->Item[_variant_t("Montly_Amt")]->Value);	
		SETLONG( objstPriceDtl.iYearlyAmt,	set->Fields->Item[_variant_t("Yearly_Amt")]->Value);			
		SETLONG( objstPriceDtl.iType,	set->Fields->Item[_variant_t("type")]->Value);
		SETLONG( objstPriceDtl.iFileFormat,	set->Fields->Item[_variant_t("IsProcessed")]->Value);

		if( objstPriceDtl.iType < 0 )
			objstPriceDtl.iType = 0;
			
		m_clPriceDtlV.push_back( objstPriceDtl );	

		set->MoveNext();
	}
	set->Close();
	
}

void CMEPriceMst::ManageUpDownButton()
{
	const int numTrades = m_clPriceDtlV.size();
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUp,e_PgDn,numTrades,MAX_ROWS,m_iCurrTopRow);
}

void CMEPriceMst::DisplayGrid()
{ 
    const int niDataRowCount = m_clPriceDtlV.size();	 
	int iGridRow = 0;

	stPriceDtl objstPriceDtl;
	for(int iDataRow = m_iCurrTopRow; iDataRow < niDataRowCount; iGridRow++, iDataRow++)		
	{
		if( iGridRow >= MAX_ROWS )
			break;

		objstPriceDtl = m_clPriceDtlV.at(iDataRow);

		m_va[e_PackageIdBase + iGridRow].SetValueInt( objstPriceDtl.iPackageId );
		m_va[e_PrdNameBase + iGridRow].SetValueString( objstPriceDtl.strProdName );
		m_va[e_ExchagneBase + iGridRow].SetValueString( objstPriceDtl.strExchange );
		m_va[e_AssetClassBase + iGridRow].SetValueString( objstPriceDtl.strAssetClass );		
		m_va[e_DailyAmtBase + iGridRow].SetValueInt( objstPriceDtl.iDailyAmt );		
		m_va[e_MonthlyAmtBase + iGridRow].SetValueInt( objstPriceDtl.iMonthlyAmt );		
		m_va[e_YarlyAmtBase + iGridRow].SetValueInt( objstPriceDtl.iYearlyAmt );	
		m_va[e_TypeBase + iGridRow].SetValueString( _csFutOpt[objstPriceDtl.iType] );

		if( objstPriceDtl.iFileFormat == 0 && fld_ProductList->GetValueInt() == 2)
		{
			// Over the wire
			GetField(e_PrdNameBase + iGridRow)->SetForeColor(ColorManager::eNegativeChange);
		}
		else
		{
			// Text
			GetField(e_PrdNameBase + iGridRow)->SetForeColor(ColorManager::eDefault);
		}
	}

	//-------- Balnk all other lines of grid -------------------//
	while(iGridRow < MAX_ROWS)
	{		
		GetField(e_PackageIdBase + iGridRow)->SetBlankState();
		GetField(e_PrdNameBase + iGridRow)->SetBlankState();
		GetField(e_ExchagneBase + iGridRow)->SetBlankState();
		GetField(e_AssetClassBase + iGridRow)->SetBlankState();

		GetField(e_DailyAmtBase + iGridRow)->SetBlankState();
		GetField(e_TypeBase + iGridRow)->SetBlankState();

		GetField(e_MonthlyAmtBase + iGridRow)->SetBlankState();
		GetField(e_YarlyAmtBase + iGridRow)->SetBlankState();

		iGridRow++;
	}

	CString csPageNumber;
	int iCurrPage = m_iCurrTopRow / MAX_ROWS + 1;
	int iTotalPage;	
	if( (niDataRowCount%MAX_ROWS) == 0)
	{
		iTotalPage = (niDataRowCount/MAX_ROWS);	
	}
	else
	{
		iTotalPage = (niDataRowCount/MAX_ROWS) + 1;	
	}

	if(iTotalPage <= 0)
		iTotalPage = 1;

	csPageNumber.Format("%d/%d",iCurrPage,iTotalPage);
	GetField(e_Label_PgNo)->SetTitle(csPageNumber);
}