
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include "afxinet.h"
#include <tlhelp32.h>
#include <string>

BF_NAMESPACE_BEGIN

struct CME_Subscription
{
	int Cust_Id;
	CString csExchange;
	CString csProduct;
	CString csFOI;
	CString csType;
	CString csDateRange;
	CString csVenue;
	CString csFileFormat;
	CString csSubsPeriod;
	CString csDelType;
	CString csStatus;

	CME_Subscription()
	{
		Cust_Id	= 0;
		csExchange = "";
		csProduct = "";
		csFOI = "";
		csType = "";
		csDateRange = "";
		csVenue = "";
		csFileFormat = "";
		csSubsPeriod = "";
		csDelType = "";
		csStatus = "";
	}
};

class CMESubscriptionView : public BaseImage
{
public:
	
	enum FieldIDs
	{
		e_OrderFilter = 1003,
		

		e_Exchange		= 151,
		e_Product		= 201,
		e_FOI			= 251,
		e_Type			= 301,
		e_DateRange		= 351,
		e_Venue			= 401,
		e_FileFormat	= 451,
		e_SubsPeriod	= 501,
		e_DeliveryType	= 551,
		e_Status		= 601,

	};
	
	CMESubscriptionView();
	~CMESubscriptionView();	
	
	bool InitData();		
	void Recalculate();	

	//void GetConnectionParams();
	void GetSubscriptionView();
	void DisplaySubscriptionView();
	
	std::multimap<int, CME_Subscription> CME_SubscriptionMap;
	std::vector<CME_Subscription> CME_SubscriptionV;

private:

	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;

};

BF_NAMESPACE_END


