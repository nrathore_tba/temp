
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <shared/MDDDataHandler.hpp>

BF_NAMESPACE_BEGIN

using namespace std;

//const string __strHeader = "Trade Date,Trade Time,Report Time,Contract Symbol,Product Code,Product Type,Description,Market Sector,Asset Class,Contract Year,Contract Month,Strike Price,Put/Call,Exchange Code,Trade Price,Trade Quantity,Trade Source,Spread Type,Spread Description,Contract Symbol 2,Product Code 2,Asset Class 2,Market Sector 2,Description 2,Product Type 2,Contract Year 2,Contract Month 2,Strike Price 2,Put/Call 2,Exchange Code 2,Trade Price 2,Trade Quantity 2,Contract Symbol 3,Product Code 3,Asset Class 3,Market Sector 3,Description 3,Product Type 3,Contract Year 3,Contract Month 3,Strike Price 3,Put/Call 3,Exchange Code 3,Contract Symbol 4,Product Code 4,Asset Class 4,Market Sector 4,Description 4,Product Type 4,Contract Year 4,Contract Month 4,Strike Price 4,Put/Call 4,Exchange Code 4\n";
  const string __strHeader = "Trade Date,Trade Time,Report Time,Contract Symbol,Product Code,Asset Class,Market Sector,Description,Product Type,Contract Year,Contract Month,Strike Price,Put/Call,Exchange Code,Trade Price,Trade Quantity,Trade Source,Spread Type,Spread Description,Contract Symbol 2,Product Code 2,Asset Class 2,Market Sector 2,Description 2,Product Type 2,Contract Year 2,Contract Month 2,Strike Price 2,Put/Call 2,Exchange Code 2,Trade Price 2,Trade Quantity 2,Contract Symbol 3,Product Code 3,Asset Class 3,Market Sector 3,Description 3,Product Type 3,Contract Year 3,Contract Month 3,Strike Price 3,Put/Call 3,Exchange Code 3,Contract Symbol 4,Product Code 4,Asset Class 4,Market Sector 4,Description 4,Product Type 4,Contract Year 4,Contract Month 4,Strike Price 4,Put/Call 4,Exchange Code 4\n";


// 54 columns..
enum ColumnName
{
	enm_TRADE_DATE,
	enm_TRADE_TIME,
	enm_CALL_TIME,
	enm_CONTRACT_SYMBOL,
	enm_TICKER_SYMBOL,
	enm_FOI_SYMBOL_IND,//Product Type
	enm_TICKER_SYMBOL_DESCRIPTION,//Description
	enm_MARKET_SECTOR,//Market Sector
	enm_ASSET_CLASS,//Asset class
	enm_CONTRACT_YEAR,
	enm_CONTRACT_MONTH,
	enm_STRIKE_PRICE,
	enm_PUT_CALL,
	enm_EXCH_CODE,
	enm_TRADE_PRICE,
	enm_TRADE_QUANTITY,
	enm_SOURCE,
	enm_SPREAD_TYPE,
	enm_SPREAD_DESCRIPTION,
	enm_CONTRACT_SYMBOL_2,
	enm_TICKER_SYMBOL_2,
	enm_TICKER_SYMBOL_DESCRIPTION_2, //Descritpion 2
	enm_MARKET_SECTOR_2, // Market sector 2
	enm_ASSET_CLASS_2, // Asset Class 2
	enm_FOI_SYMBOL_IND_2,
	enm_CONTRACT_YEAR_2,
	enm_CONTRACT_MONTH_2,
	enm_STRIKE_PRICE_2,
	enm_PUT_CALL_2,
	enm_EXCH_CODE_2,
	enm_TRADE_PRICE_2,
	enm_TRADE_QUANTITY_2,
	enm_CONTRACT_SYMBOL_3,
	enm_TICKER_SYMBOL_3,
	enm_TICKER_SYMBOL_DESCRIPTION_3, //Descritpion 3
	enm_MARKET_SECTOR_3, // Market sector 3
	enm_ASSET_CLASS_3,  // Asset Class 3
	enm_FOI_SYMBOL_IND_3,
	enm_CONTRACT_YEAR_3,
	enm_CONTRACT_MONTH_3,
	enm_STRIKE_PRICE_3,
	enm_PUT_CALL_3,
	enm_EXCH_CODE_3,
	enm_CONTRACT_SYMBOL_4,
	enm_TICKER_SYMBOL_4,
	enm_TICKER_SYMBOL_DESCRIPTION_4, //Descritpion 4
	enm_MARKET_SECTOR_4, //  Market sector 4
	enm_ASSET_CLASS_4, // Asset Class 4
	enm_FOI_SYMBOL_IND_4,
	enm_CONTRACT_YEAR_4,
	enm_CONTRACT_MONTH_4,
	enm_STRIKE_PRICE_4,
	enm_PUT_CALL_4,
	enm_EXCH_CODE_4,
	enm_END	
};


struct ScheduleInfo
{
	int		iRetInterval;
	int		iRetryCount;
	bool	bValid;
	int		iStatus;	

	COleDateTime clMainScheduleDtTime;	
	COleDateTime clScheduleDtTime;	
	CString csDate;
	CString csDateBlock;

	CString  csFilePath;
	CString	 csBLOCP_FilePath;
	CString  csBLOCK_FilePath;
};

enum enmOperation
{
	OP_Download,
	OP_Uncompress,
	OP_Process,
	OP_ReadThread,
	OP_ProcessCompleted,
	OP_None
};

struct BlockDetail
{
	std::vector<std::string> strArray;

	void GetRowMessage(string &msg )
	{
		msg = strArray[enm_TRADE_DATE];
		int iI = 1;
		for( ; iI < enm_FOI_SYMBOL_IND; iI++ )
		{
			msg += "," + strArray[iI];
		}	

		msg += "," + strArray[enm_ASSET_CLASS];
		iI++;

		msg += "," + strArray[enm_MARKET_SECTOR];
		iI++;

		msg += "," + strArray[enm_TICKER_SYMBOL_DESCRIPTION];
		iI++;

		msg += "," + strArray[enm_FOI_SYMBOL_IND];
		iI++;

		for( ; iI < enm_TICKER_SYMBOL_DESCRIPTION_2; iI++ )
		{
			msg += "," + strArray[iI];
		}	

		msg += "," + strArray[enm_ASSET_CLASS_2];
		iI++;

		msg += "," + strArray[enm_MARKET_SECTOR_2];
		iI++;

		msg += "," + strArray[enm_TICKER_SYMBOL_DESCRIPTION_2];
		iI++;


		for( ; iI < enm_TICKER_SYMBOL_DESCRIPTION_3; iI++ )
		{
			msg += "," + strArray[iI];
		}	

		msg += "," + strArray[enm_ASSET_CLASS_3];
		iI++;

		msg += "," + strArray[enm_MARKET_SECTOR_3];
		iI++;

		msg += "," + strArray[enm_TICKER_SYMBOL_DESCRIPTION_3];
		iI++;

		for( ; iI < enm_TICKER_SYMBOL_DESCRIPTION_4; iI++ )
		{
			msg += "," + strArray[iI];
		}	

		msg += "," + strArray[enm_ASSET_CLASS_4];
		iI++;

		msg += "," + strArray[enm_MARKET_SECTOR_4];
		iI++;

		msg += "," + strArray[enm_TICKER_SYMBOL_DESCRIPTION_4];
		iI++;

		for( ; iI < strArray.size(); iI++ )
		{
			msg += "," + strArray[iI];
		}	

		msg += "\n";
	}
};	

typedef std::vector<BlockDetail> BlockDetailV;


struct BLOCKSubscriptionDtl
{
	BLOCKSubscriptionDtl()
	{		
		stCUST_NAME = "";
		stCONTACT_EMAIL = "";
		stDEST_ACCOUNT = "";
		stFTP_PASSWORD = "";
		stVENUE_CODE = "";
		stALL_PRODUCTS = "";
		stPROD_CODE = "";
		stBLOCK_CODE = "";
		stEXCH_CODE = "";
	}	
	
	CString stCUST_NAME;
	CString stCONTACT_EMAIL;
	CString stDEST_ACCOUNT;
	CString stFTP_PASSWORD;
	CString stVENUE_CODE;
	CString stALL_PRODUCTS;
	CString stPROD_CODE;
	CString stBLOCK_CODE;
	CString stEXCH_CODE;	
	CString stEXCH_PROD_CODE;	
};

typedef std::multimap<std::string, BLOCKSubscriptionDtl> BLOCKSubDtlMap;

struct BLOCKSubscriptionFile
{
	BLOCKSubscriptionFile()
	{
		bUploaded = false;
		fpOut = NULL;
		iRowCount = 0;
		iFileCount = 0;
	}
	
	FILE *fpOut;
	bool bUploaded;
	int iRowCount;
	int iFileCount;
	int iUploadReturn;
	CString strOutputFolderPath;	
	CString stDestAccount;
	CString strOutPutFileName;
};

typedef std::map<std::string, BLOCKSubscriptionFile> BLOCKSubFileMap;

struct FileDtl
{
	std::string strFileName;
	std::string strFilePath;
	FILE *fpOut;
	int iRowCount;

	FileDtl( )
	{
		strFileName = "";
		strFilePath = "";
		fpOut = NULL;
		iRowCount = 0;
	}
};

typedef std::map<std::string, FileDtl> FileDtlMap;
//typedef std::map<std::string, bool> DistItemMap;

class ExecutionBLOCKS : public BaseImage
{
public:
	
	enum FieldIDs
	{
		e_NextDownloadMsg = 50,	
		e_MsgTitle		  = 65,
		e_OutputPath	  = 62,
		e_SingleAccount   = 63,

		e_ReloadSchedule  = 100,
		e_DateFilter	  = 95,
		e_ManualRun		  = 96,
		e_Process		  = 97,
		e_RtryInterval	  = 98,
		//e_FilterProdExch  = 99,

		//e_SubType	= 212,
		e_Year		= 201,
		e_Month		= 202,
		e_Day		= 215,
		e_Hour		= 213,
		e_Minute	= 214,

		e_S3CMEBucket = 60,
		e_DestAccount = 58,
		e_S3FTPBucket = 59,

		e_LogMsg	= 4000,
		e_pgUP		= 92,
		e_pgDN		= 93,
	};
	
	ExecutionBLOCKS();
	~ExecutionBLOCKS();	

	bool InitData();		
	void Recalculate();	
private:

	void ExecutionBLOCKS::fnAddMessage( CString csaNewMsg );
	void ExecutionBLOCKS::fnDisplayMsg();
	
	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;
	
	int m_numofRows;
	int m_curTopRow;

	static const int m_niInputColumnCount	= 54;
	static const int m_niOutputColumnCount	= 54;

	BaseField *fld_OutputPath;

public:
	bool m_bIsTrheadCompleted;
	bool m_bIsThreadRunning;
	
	CString m_csMsg;
	int m_iTotalCount;
	int m_iOpenFileCount;
	
	CCriticalSection	m_MsgCriticalSec;

	void fnWriteMessage(CString csString )
	{
		CSingleLock clMsgSingleLock(&m_MsgCriticalSec);

		clMsgSingleLock.Lock();

		m_csMsg = csString.GetString();

		clMsgSingleLock.Unlock();
	}

	CString fnReadMessage( )
	{
		CSingleLock clMsgSingleLock(&m_MsgCriticalSec);

		clMsgSingleLock.Lock();

		CString csStr = m_csMsg.GetString();

		clMsgSingleLock.Unlock();

		return csStr;
	}	

	void ExecutionBLOCKS::fnGetNextSchedule( );
	void ExecutionBLOCKS::fnUpdateScheduleStatus(int iStatus );

	void ExecutionBLOCKS::fnRecalc1( );
	void ExecutionBLOCKS::fnCopyS3FiletoLocal( );
	void ExecutionBLOCKS::fnUncompressGZ( );
	void ExecutionBLOCKS::fnCheckFolderPath( );
	void ExecutionBLOCKS::ReadBLOCKFileCSV( );
	void ExecutionBLOCKS::ParseLine( std::string &ssLine );
	void ExecutionBLOCKS::fnAddInFiles( BlockDetail &blockRowData  );
	void ExecutionBLOCKS::fnAddRowInSubscription( std::string &strMsg, FileDtl &blockSubDtl, bool bIsProduct );
	void ExecutionBLOCKS::fnCloseAllOpenFiles( );

	void ExecutionBLOCKS::LoadSubscription();
	void ExecutionBLOCKS::ProcessBlockProdSubscription();
	void ExecutionBLOCKS::ProcessBlockExchSubscription();

	bool ExecutionBLOCKS::CreateSubscriptionFile(BLOCKSubscriptionFile &clSubFileDtl, FILE *fileIn );

	void ExecutionBLOCKS::fnCreateZip( );
	void ExecutionBLOCKS::UploadSubscription();

	void ExecutionBLOCKS::fnInsertDeliveryReport(CString csPrdouct, CString csDestAccount, CString csFileName, unsigned long &lFileSize, CString &csAwscmd );
	void ExecutionBLOCKS::fnInsertErroReport(CString csPrdouct, CString csDestAccount, CString csFileName, CString csExchnage, CString &csErrorCmd );

	void  ExecutionBLOCKS::fnSendEmail(int iType, int iRowCount);

	unsigned long  GetFileSize(std::string const &path) 
	{
		WIN32_FIND_DATA data;
		HANDLE h = FindFirstFile(path.c_str(), &data);
		if (h == INVALID_HANDLE_VALUE)
			return -1;

		FindClose(h);

		return (data.nFileSizeLow | data.nFileSizeHigh << 32);
	}

	ScheduleInfo m_clScheduleInfo;
	enmOperation m_enmOperation;

	FileDtlMap m_clProdFileDtlMap; //Prod
	//FileDtlMap m_clExchFileDtlMap;

	//DistItemMap	m_clBlockDistProdMap;
	//DistItemMap	m_clBlockDistExchMap;

	BLOCKSubDtlMap m_clBlockProdSubDtlMap; //Prod
	BLOCKSubDtlMap m_clBlockExchSubDtlMap;
	
	BLOCKSubFileMap m_clBlockProdSubFileMap; //Prod	
	BLOCKSubFileMap m_clBlockExchSubFileMap;
};

BF_NAMESPACE_END


