// Copyright (C) 2000 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#include <stdafx.h>

#include <direct.h>

#include "CMEFileProcessManager.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(ProductMasterFinApp)

ProductMasterFinApp::ProductMasterFinApp()
{
}

ProductMasterFinApp::~ProductMasterFinApp()
{
}

bool ProductMasterFinApp::RealInitData()
{ 
	m_curTopRow= 0;
	for(m_numofRows = 0; GetField(e_DisplayFileName + m_numofRows); m_numofRows++);

	m_curTopRowDB= 0;
	for(m_numofRowsDB = 0; GetField(e_DisplayFileNameDB + m_numofRowsDB); m_numofRowsDB++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME_BCP","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	// DB Connection Completed....
		
	GetField(e_InsertToDB)->SetEnabled(false);

	m_csFileName=GetField(e_BaseFilePath)->GetValueString();

	CString strUserName = this->GetUserName();
	GetField(900000)->SetValueString(strUserName);
	AddInfoField(GetField(900000));
	return true;
}

void ProductMasterFinApp::fnCreateFilePath( )
{
	if( !IsFieldNew(e_GetFileList) )
		return;

	m_clFileList.clear();

	int iFileType = GetField(e_FileType)->GetValueInt();
	CString csFileName = GetField(e_BaseFilePath)->GetValueString();

	PrintRawMessage(csFileName);

	WIN32_FIND_DATA ffd;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	LARGE_INTEGER filesize;

	hFind = FindFirstFile(csFileName, &ffd);

	if (INVALID_HANDLE_VALUE == hFind) 
	{
		//DisplayErrorBox(TEXT("FindFirstFile"));
		//return dwError;
		PrintRawMessage("Error FindFirstFile");
		GetField(e_Message)->SetTitle("Error FindFirstFile, please check path");
		return;
	} 

	// List all the files in the directory with some info about them.
	CString csDate;
	csDate.Format("_%d%02d",GetField(e_Year)->GetValueInt(),GetField(e_Month)->GetValueInt());

	FileDetail clFileDetail;
	clFileDetail.csPath		= csFileName;
	clFileDetail.csFileType = GetField(e_FileType)->GetDisplayString();
	clFileDetail.csExchange = GetField(e_Exchange)->GetDisplayString();
	clFileDetail.csFormat   = GetField(e_FileFormat)->GetDisplayString();
	clFileDetail.iYear		= GetField(e_Year)->GetValueInt();
	clFileDetail.iMonth		= GetField(e_Month)->GetValueInt();

	clFileDetail.csPath.Replace("\\*.lzo","");
	do
	{
		if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			_tprintf(TEXT("%s   <DIR>\n"), ffd.cFileName);
		}
		else
		{
			filesize.LowPart = ffd.nFileSizeLow;
			filesize.HighPart = ffd.nFileSizeHigh;
			
			//_tprintf(TEXT("%s, %ld, %ld,   %ld bytes\n"), ffd.cFileName, filesize.LowPart, filesize.HighPart, filesize.QuadPart);
			//PrintRawMessage(ffd.cFileName);
			clFileDetail.csFileName = ffd.cFileName;
			clFileDetail.lFileSize = ((unsigned long)filesize.QuadPart)/1024;

			//_tprintf(TEXT("       %ld, %u, \n"), clFileDetail.lFileSize, clFileDetail.lFileSize);
			int iInd = clFileDetail.csFileName.Find(csDate);
			if( iInd != -1 )
			{	
				CString strDate = clFileDetail.csFileName.Mid(iInd+7, 2);
				clFileDetail.iDay = atoi( strDate );
				m_clFileList.push_back( clFileDetail );
			}
		}
	}
	while (FindNextFile(hFind, &ffd) != 0);	

	CString csTemp;
	csTemp.Format("%d File Found",m_clFileList.size());
	GetField(e_Message)->SetTitle(csTemp);

	GetField(e_InsertToDB)->SetEnabled(true);
}

void ProductMasterFinApp::RealRecalculate()
{	
	

	if(IsFieldNew(e_FileFormat)||IsFieldNew(e_Exchange)|| IsFieldNew(e_Year))
	{

		// \\uat-ss1\RLC\cme\2006\*.lzo
		
		if(GetField(e_FileFormat)->GetDisplayString().CompareNoCase("rlc")==0)
		{
		
			CString csFileName="";
			CString csExchange=GetField(e_Exchange)->GetDisplayString();//xnym
			csExchange.MakeLower();

			csFileName.Format("\\\\uat-ss1\\RLC\\%s\\%s\\*.lzo", csExchange.Mid(1,csExchange.GetLength()),GetField(e_Year)->GetDisplayString() );//nym
			GetField(e_BaseFilePath)->SetValueString(csFileName);
		}
		else
			GetField(e_BaseFilePath)->SetValueString(m_csFileName);

	}
	fnCreateFilePath( );
	if( IsFieldNew(e_FileType) || IsFieldNew(e_Year) || IsFieldNew(e_Month) || IsFieldNew(e_Exchange) || IsFieldNew(e_FileFormat) || IsFieldNew(e_BaseFilePath) )
		GetField(e_InsertToDB)->SetEnabled(false);
	

	bool bReload = InsertIntoDB( );
	if( IsFieldNew(e_UnZipAll) )
	{
		bReload |= fnUncompressAll( );
	}
	else
	{
		bReload |= fnCheckUserAction( );
	}

	LoadFromDB( bReload );
		
	fnPageUpDown( );
	DisplayFileList( );
}

void ProductMasterFinApp::LoadFromDB( bool bReload )
{
	if( !IsFieldNew(e_LoadFromDB) && !bReload)
		return;

	m_clFileListDB.clear();

	int iYear = GetField(e_Year)->GetValueInt();
	int iMonth = GetField(e_Month)->GetValueInt();
	CString csFileType = GetField(e_FileType)->GetDisplayString();	
	CString csExchange = GetField(e_Exchange)->GetDisplayString();
	CString csFormat = GetField(e_FileFormat)->GetDisplayString();

	CString sql;
	sql.Format("select * from CME_Historical_Files  where FileType = '%s' AND [Year] = %d	AND [Month] = %d AND	Exchange = '%s' And	Format = '%s'",csFileType,iYear,iMonth, csExchange,csFormat );

	_RecordsetPtr set;

	DB_TRY
	{
		if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql, false)))
		{
			SetErrorMessage(ErrorSeverity::e_Error, sql);
			PrintRawMessage(sql);
		}

		FileDetail clFileDetail;
		clFileDetail.csFileType = csFileType;
		clFileDetail.csExchange = csExchange;
		clFileDetail.csFormat = csFormat;
		clFileDetail.iYear	  = iYear;
		clFileDetail.iMonth   = iMonth;

		while( VARIANT_FALSE == set->adoEOF )
		{
			SETSTR(	clFileDetail.csFileName,	set->Fields->Item[_variant_t("FileName")]->Value);	
			SETSTR(	clFileDetail.csPath,		set->Fields->Item[_variant_t("Path")]->Value);	
			SETSTR(	clFileDetail.csOutputPath,	set->Fields->Item[_variant_t("OutputPath")]->Value);	
			SETLONGLONG(clFileDetail.lFileSize,	set->Fields->Item[_variant_t("Size")]->Value);	

			SETLONG(clFileDetail.iStatus,	set->Fields->Item[_variant_t("Status")]->Value);	
			SETLONG(clFileDetail.lOffset,	set->Fields->Item[_variant_t("SeekOffset")]->Value);

			SETLONG(clFileDetail.iDay,		set->Fields->Item[_variant_t("FileDate")]->Value);	
			SETLONG(clFileDetail.iUncomp,	set->Fields->Item[_variant_t("Uncomp")]->Value);	

			SETLONG(clFileDetail.iProdCal,	set->Fields->Item[_variant_t("ProductCalendar")]->Value);	
			SETLONG(clFileDetail.iCompGZ,	set->Fields->Item[_variant_t("ProductZip")]->Value);	
						
			m_clFileListDB.push_back( clFileDetail );	
			set->MoveNext();
		}
		set->Close();
	}
	DB_CATCH(sql);	
}

bool ProductMasterFinApp::InsertIntoDB( )
{
	if( !IsFieldNew(e_InsertToDB) )
		return false;
	
	for( int iI = 0; iI < m_clFileList.size(); iI++ )
	{
		CString sql;
		sql.Format("EXEC [Proc_Beast_Submit_CME_Historical_File] '%s', %d, %d, %d, '%s', '%s', '%s','%s',%d",m_clFileList[iI].csFileType,m_clFileList[iI].iYear,m_clFileList[iI].iMonth, m_clFileList[iI].iDay, m_clFileList[iI].csFileName, m_clFileList[iI].csExchange, m_clFileList[iI].csFormat, m_clFileList[iI].csPath, m_clFileList[iI].lFileSize);

		_RecordsetPtr set;

		DB_TRY
		{
			if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql)))
			{
				SetErrorMessage(ErrorSeverity::e_Error, sql);
				PrintRawMessage(sql);
			}

		}
		DB_CATCH(sql);	
	}

	return true;
}

void ProductMasterFinApp::fnPageUpDown( )
{
	int iTotalRecord = m_clFileList.size( );
	
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpBase,e_PgDnBase,iTotalRecord,m_numofRows,m_curTopRow);	

	iTotalRecord = m_clFileListDB.size( );
	
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpBaseDB,e_PgDnBaseDB,iTotalRecord,m_numofRowsDB,m_curTopRowDB);	
}

void ProductMasterFinApp::DisplayFileList( )
{
	int iRow(0);
	for( int iI = m_curTopRow; iI < m_clFileList.size(); iI++, iRow++ )
	{
		if( iRow >= m_numofRows )
			break;

		GetField( e_DisplayFileName + iRow )->SetValueString( m_clFileList[iI].csFileName );
		GetField( e_DisplayFileSize + iRow )->SetValueInt( m_clFileList[iI].lFileSize );
	}

	while( iRow < m_numofRows )
	{
		GetField( e_DisplayFileName + iRow )->SetValueString("");
		GetField( e_DisplayFileSize + iRow )->SetBlankState();
		iRow++;
	}


	iRow = 0;
	for( int iI = m_curTopRowDB; iI < m_clFileListDB.size(); iI++, iRow++ )
	{
		if( iRow >= m_numofRowsDB )
			break;
		
		GetField( e_DisplayFileNameDB + iRow )->SetValueString( m_clFileListDB[iI].csFileName );
		GetField( e_DisplayFileSizeDB + iRow )->SetValueInt( m_clFileListDB[iI].lFileSize );
		GetField( e_DisplayFileStatusDB + iRow )->SetValueInt( m_clFileListDB[iI].iStatus );
		GetField( e_DisplayFilePathDB + iRow )->SetValueString( m_clFileListDB[iI].csPath );

		GetField( e_Prod_Cal + iRow )->SetValueInt( m_clFileListDB[iI].iProdCal );
		GetField( e_Compr_GZ + iRow )->SetValueInt( m_clFileListDB[iI].iCompGZ );

		CString csTemp;
		csTemp.Format("offset = %ld",m_clFileListDB[iI].lOffset );

		GetField( e_DisplayFilePathDB + iRow )->SetToolTipText( csTemp );

		if( m_clFileListDB[iI].iUncomp == 1 )
			GetField(e_UnZip + iRow)->SetEnabled(false);
		else
			GetField(e_UnZip + iRow)->SetEnabled(true);
	}

	int iCount = 0;
	for( int iIndex = m_curTopRowDB; iIndex < m_clFileListDB.size(); iIndex++ )
	{
		if( m_clFileListDB[iIndex].iUncomp == 1 )
			iCount++;
	}
	if(iCount == m_clFileListDB.size())
		GetField(e_UnZipAll )->SetEnabled(false);
	else
		GetField(e_UnZipAll )->SetEnabled(true);

	while( iRow < m_numofRowsDB )
	{
		GetField( e_DisplayFileNameDB + iRow )->SetValueString("");
		GetField( e_DisplayFileSizeDB + iRow )->SetBlankState();
		GetField( e_DisplayFileStatusDB + iRow )->SetBlankState();
		GetField( e_DisplayFilePathDB + iRow )->SetBlankState();

		GetField( e_DisplayFilePathDB + iRow )->SetToolTipText("");
		iRow++;
	}
}

bool ProductMasterFinApp::fnCheckUserAction( )
{
	int iRow = 0;
	for( int iI = m_curTopRowDB; iI < m_clFileListDB.size(); iI++, iRow++ )
	{
		if( IsFieldNew( e_UnZip + iRow) )
		{
			fnUncompress( iI);
			return true;
		}

		if( IsFieldNew( e_RemoveFile + iRow) )
		{
			fnRemoveFile( iI );
			return true;
		}
	}
	return false;
}

bool ProductMasterFinApp::fnUncompressAll( )
{
	for( int iIndex = 0; iIndex < m_clFileListDB.size(); iIndex++ )
	{
		fnUncompress( iIndex);
	}
	return true;
}

//void ProductMasterFinApp::fnUncompress( int index )
//{
//	CString csPath = m_clFileListDB[index].csPath;
//	
//	CString csFilePath;
//	csFilePath.Format("%s\\%s",csPath,m_clFileListDB[index].csFileName);
//
//	CString strcmd;
//	strcmd.Format("D:\\lzop.exe -1 -d %s",csFilePath);
//
//	int		iValue = system(strcmd);
//
//	if( iValue == 0 )
//	{
//		UpdateUncompressflag(1, index);
//		GetField(e_Message)->SetTitle("Uncomprssed file successfully.");
//	}
//	else
//	{
//		CString csTemp;
//		csTemp.Format("error no = %d - path %s, uncompress unsuccessful",iValue,csFilePath);
//		GetField(e_Message)->SetTitle(csTemp);
//	}
//}

 void ProductMasterFinApp::fnUncompress( int index )
{
	CString csPath = m_clFileListDB[index].csPath;
	
	CString csFilePath;
	csFilePath.Format("%s\\%s",csPath,m_clFileListDB[index].csFileName);

	CString remianingFile = csFilePath;
	remianingFile.Replace(".lzo","");
	
	CString strcmd;
	strcmd.Format("D:\\lzop.exe -1 -df %s",csFilePath);

	int	iValue = system(strcmd);

	if( iValue == 0 )
	{
		UpdateUncompressflag(1, index);
		GetField(e_Message)->SetTitle("Uncompressed file successfully.");
	}
	else
	{
		CString csTemp;
		csTemp.Format("error no = %d - path %s, uncompress unsuccessful",iValue,csFilePath);
		GetField(e_Message)->SetTitle(csTemp);
	}
}
void ProductMasterFinApp::UpdateUncompressflag(int iUncomp, int iIndex)
{
	int iYear = m_clFileListDB[iIndex].iYear;
	int iMonth = m_clFileListDB[iIndex].iMonth;
	CString csFileType = m_clFileListDB[iIndex].csFileType;	
	CString csExchange = m_clFileListDB[iIndex].csExchange;
	CString csFormat = m_clFileListDB[iIndex].csFormat;

	CString sql;
	sql.Format("Update CME_Historical_Files set Uncomp = %d where FileType = '%s' AND [Year] = %d	AND [Month] = %d AND	Exchange = '%s' And	Format = '%s' And FileName = '%s'",iUncomp, csFileType, iYear, iMonth, csExchange, csFormat, m_clFileListDB[iIndex].csFileName );

	_RecordsetPtr set;

	DB_TRY
	{
		if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql)))
		{
			SetErrorMessage(ErrorSeverity::e_Error, sql);
			PrintRawMessage(sql);
		}	
	}
	DB_CATCH(sql);	
}

void ProductMasterFinApp::fnRemoveFile( int index )
{
	CString csPath = m_clFileListDB[index].csPath;
	
	CString csFilePath;
	csFilePath.Format("%s\\%s",csPath,m_clFileListDB[index].csFileName);
	csFilePath.Replace(".lzo","");
		
	if( DeleteFile(csFilePath) == 0 )	 
	{
		CString csTemp;
		csTemp.Format("error %d, while removing File %s", GetLastError(), csFilePath);
		GetField(e_Message)->SetTitle(csTemp);
	 }
	 else
	 {
	    UpdateUncompressflag(0, index);
		GetField(e_Message)->SetTitle("file removed successfully.");	
	 }
}