#include <stdafx.h>
#include "CME_Avro_Parser_Binary.hpp"
#include "..\Src\MessageParser.h"
#include <boost/filesystem.hpp>

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(CME_Avro_Parser)

avro::ValidSchema loadSchema(const char* filename)
{
	std::ifstream ifs(filename);
	avro::ValidSchema result;
	avro::compileJsonSchema(ifs, result);
	return result;
}

std::string fnGetmdMessage(std::vector<uint8_t> &mdMessage )
{
	std::string strTemp;
	strTemp = "";

	for( int iI = 0; iI < mdMessage.size(); iI++)
	{
		unsigned char c = (unsigned char) mdMessage[iI];
		strTemp += c;
	}

	return strTemp;
}

CME_Avro_Parser::CME_Avro_Parser()
{
	m_bIsThreadRunning = false;
}

CME_Avro_Parser::~CME_Avro_Parser()
{
}

bool CME_Avro_Parser::InitData()
{
	GetField(e_InputFolder)->SetValueString("D:\\20150111");
	GetField(e_OutputFolder)->SetValueString("D:\\20150111_Output");

#ifdef _DEBUG

	GetField(e_InputFolder)->SetValueString("D:\\Beast Projects\\CME\\20150111");
	GetField(e_OutputFolder)->SetValueString("D:\\Beast Projects\\CME\\Final_Binary");

	GetField(e_ParseSecDef)->SetValueInt(0);

#endif

	m_cLogManager.Init(this, e_MessageBase, e_PgUp, e_PgDn);
	int nFileCount = _setmaxstdio(2048);
	return true;
}

UINT AvroProcessor(void *pVoid)
{
	CME_Avro_Parser *pParser = (CME_Avro_Parser*)pVoid;
	
	pParser->m_bIsThreadRunning = true;
	pParser->Parse();
	pParser->m_bIsThreadRunning = false;

	return 0;
}

void CME_Avro_Parser::Parse()
{
	avro::ValidSchema cpxSchema;

	try
	{
		cpxSchema = loadSchema("cme.json");
	}
	catch(exception e)
	{
		m_cLogManager.fnAddMessage(e.what());
		RequestExternalUpdate();
		return;
	}

	string inputfolder = GetField(e_InputFolder)->GetValueString();
	string outputfolder = GetField(e_OutputFolder)->GetValueString();

	if(inputfolder.empty())
	{
		m_cLogManager.fnAddMessage("Invalid input path");
		RequestExternalUpdate();
		return;
	}

	if(outputfolder.empty())
	{
		m_cLogManager.fnAddMessage("Invalid output path");
		RequestExternalUpdate();
		return;
	}

	std::vector<string> vAvroFiles;
	if( !DataUtil::getInstance()->GetFilesInDirectory(vAvroFiles, inputfolder, "*.avro"))
	{
		m_cLogManager.fnAddMessage("Error in reading input directory");
		RequestExternalUpdate();
		return;
	}

	if( !(boost::filesystem::exists(outputfolder)))
	{
		bool bSuccess = boost::filesystem::create_directories(outputfolder);
		if( !bSuccess )
		{
			m_cLogManager.fnAddMessage("Error in creating output directory");
			RequestExternalUpdate();
			return;
		}
	}

	DataUtil::getInstance()->LoadProcessedFiles(outputfolder);

	// Start -----------------------------
	MDMessageConvertor objMDMessageConvertor;

	m_cLogManager.fnAddMessage("Loading Security Definitions...");
	RequestExternalUpdate();

	objMDMessageConvertor.setup(inputfolder, outputfolder);

	m_cLogManager.fnAddMessage("Security Definitions loaded...");
	RequestExternalUpdate();

	// Process -----------------------------
	for(int iCount(0); iCount < vAvroFiles.size(); ++iCount)
	{
		if(DataUtil::getInstance()->IsFileProcessed(vAvroFiles[iCount]))
			continue;

		avro::DataFileReader<c::Message> dfr(vAvroFiles[iCount].c_str(), cpxSchema);
		c::Message c2;
		string strmessage;

		CString strlog;
		strlog.Format("MDMessageDecoderMapper: Processing File (%d/%d)  %s", (iCount+1), vAvroFiles.size(), vAvroFiles[iCount].c_str());
		m_cLogManager.fnAddMessage(strlog);
		PrintRawMessage(strlog);
		RequestExternalUpdate();

		while (dfr.read(c2)) 
		{
			strmessage = fnGetmdMessage( c2.mdMsg );
			objMDMessageConvertor.convertMessageBinary(Message(strmessage.c_str(), strmessage.length(), c2.mdpChannelName.c_str()));
		}

		DataUtil::getInstance()->SetProcessedFile(vAvroFiles[iCount]);
	}

	// Close -----------------------------
	objMDMessageConvertor.close();

	DataUtil::getInstance()->CloseProcessedFile();

	m_cLogManager.fnAddMessage("Process completed...");
	
	COleDateTime dtCurrent = COleDateTime::GetCurrentTime();
	CString strCurrentTime;
	strCurrentTime.Format("%04d/%02d%02d %02d:%02d:%02d", dtCurrent.GetYear(), dtCurrent.GetMonth(), dtCurrent.GetDay(), 
		dtCurrent.GetHour(), dtCurrent.GetMinute(), dtCurrent.GetSecond());
	GetField(e_EndTime)->SetValueString(strCurrentTime);

	RequestExternalUpdate();
}

void CME_Avro_Parser::Recalculate()
{
	if(IsFieldNew(e_StartProcess) && !m_bIsThreadRunning)
	{
		COleDateTime dtCurrent = COleDateTime::GetCurrentTime();
		CString strCurrentTime;
		strCurrentTime.Format("%04d/%02d%02d %02d:%02d:%02d", dtCurrent.GetYear(), dtCurrent.GetMonth(), dtCurrent.GetDay(), 
			dtCurrent.GetHour(), dtCurrent.GetMinute(), dtCurrent.GetSecond());
		GetField(e_StartTime)->SetValueString(strCurrentTime);
		GetField(e_EndTime)->SetValueString("");

		AfxBeginThread(AvroProcessor, this);
	}

	m_cLogManager.fnPageUpDown();
	m_cLogManager.fnDisplayMessages();
}
