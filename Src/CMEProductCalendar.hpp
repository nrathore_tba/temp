
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <shared/MDDDataHandler.hpp>

BF_NAMESPACE_BEGIN
using namespace std;

struct stFileNameParseData
{
	stFileNameParseData()
	{
		strFileName = "";
		strProduct="";
		strFutOpt = "";
		strSpread = "";			
		lFileSize = 1;
	}

	CString strFileName;
	CString strProduct;
	CString strFutOpt;
	CString strSpread;
	CString strPath;
	unsigned long long lFileSize;
	int iDay;
	int iMonth;
	int iYear;
	BFDate bfDate;
	CString csFileType;
	CString csExchange;
	CString csFormat;
	bool bDateCorrect;
};

struct FileDetail
{
	CString csFileName;
	CString csPath;
	CString csOutputPath;
	int iProudctCalender;
	int iProudctZip;

	CString csFileType;
	CString csExchange;
	CString csFormat;
	int iYear;
	int iMonth;
	int iDay;	

	FileDetail() 
	{
		iProudctCalender = 0;		
		iProudctZip = 0;		
	}
};	


class CMEProductCalendar : public BaseImage
{
public:
	
	enum FieldIDs
	{
		e_BasePath	= 501,		
		e_Message	= 500,		

		e_FileType  = 200,
		e_Year      = 201,
		e_Month     = 202,		
		e_Exchange	= 203,		
		e_FileFormat= 204,
		e_Day		= 205,

		e_ClearList		= 117,
		e_LoadProcessFileList = 116,
		e_ProcessFileList = 502,

		e_FullFilePath	= 503,
		
		e_LoadFileListBtn	= 112,
		e_ProductCalendar	= 113,
		e_Compressgz		= 114,
		e_RemoveFiles		= 115,
	
		e_DisplayFileType	= 2400,
		e_DisplayeExch		= 2250,
		e_DisplayFormat		= 2300,
		e_DisplayDate		= 2350,
		e_DisplayFileName	 = 2000,
		e_DisplayFileProduct = 2050,
		e_DisplayFileFutOpt  = 2100,
		e_DisplayFileSpread  = 2150,
		e_DisplayFileSize	 = 2200,		

		e_PgUp	= 92,
		e_PgDn	= 93,	

		e_Label_PgNo = 65,
				
	};
	
	CMEProductCalendar();
	~CMEProductCalendar();	

	bool InitData();		
	void Recalculate();	

	void CMEProductCalendar::fnCompressAllFile( );
	void CMEProductCalendar::fnRemoveFile( );
	void CMEProductCalendar::fnLoadProcessFileList( );
	void CMEProductCalendar::Recurse(LPCTSTR pstr);
	void CMEProductCalendar::SplitFileNameString(CString cStrFileName, string delimiter, vector<string> &result);

	void InitDB();
	void CreateProductCalnderInDB();
	
	void DisplayGrid();
	void ManageUpDownButton();
	
	bool m_bAnyIncorrectDate;
	std::vector<stFileNameParseData> m_FileFilterData;	
	std::vector<FileDetail> m_clProcessFileListDB;

public:

	int m_iCurrentFile;
	int m_nTotalFiles;

	void CMEProductCalendar::MultiThread_Zip();

	int CMEProductCalendar::getNextFile();

private:
		
	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;
	
	int m_iCurrTopRow;
	
	ListField *fld_PorcessListfld;
};

BF_NAMESPACE_END


