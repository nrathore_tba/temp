
#include <stdafx.h> 
#include "CMEHistoricalDataParser.hpp"

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(CMEHistoricalDataParser)

#define FILENAME "md_ff_cme_YYYYMMDD_chn0.dat"

#define MAX_ROWS 14

enum Action{e_None=1,e_decompressed = 1,e_Parsed = 2};
CMEHistoricalDataParser::CMEHistoricalDataParser()
{		
}

CMEHistoricalDataParser::~CMEHistoricalDataParser()
{
}

bool CMEHistoricalDataParser::InitData()
{	
	InitDataBase();
	m_vcParserDetails.clear();
	m_FileMap.empty();
	GetFileName();
	nNumofSecurityId=0;
	mHeader="";
	mFooter="";
	m_TotalLines=0;
	m_iEvent=0;

	GetField(e_CompressedFilePath)->SetValueString("\\\\192.168.32.235\\CME_s3\\historical\\fix");
	GetField(e_FilePath)->SetValueString("\\\\192.168.32.235\\CME_s3\\historical\\fix");
	GetField(e_ParsedFileLocation)->SetValueString("\\\\192.168.32.235\\CME_s3\\FolderStructure");
	GetField(e_lzopexeLocation)->SetValueString("\\\\192.168.32.235\\CME_s3\\historical\\fix");

	if(GetField(e_AutoParsed)->GetValueInt() == 0)
	{
		GetField(e_ManualParsed)->SetEnabled(true);
		GetField(e_Decompressed)->SetEnabled(true);
	}
	else
	{
		GetField(e_ManualParsed)->SetEnabled(false);
		GetField(e_Decompressed)->SetEnabled(false);
		nAutoDecompressed=1;
	}
	CString strUserName = this->GetUserName();
	GetField(900000)->SetValueString(strUserName);
	AddInfoField(GetField(900000));
	DisplayMessages();
	return true;
}

void CMEHistoricalDataParser::Recalculate()
{
	bool bRead = false;
	if(m_operation==e_Decompressed)
	{
		fnDecompressed();
		m_operation=e_None;
		nevent=0;
	}
	if(IsFieldNew(e_Decompressed))
	{
		m_operation=e_Decompressed;
		m_Message.nError = 0;
		m_Message.csAction = "Decompression";
		m_Message.csStatus = _T("File is Decompressing...");
		AddMessage(m_Message);
		nevent=1;
	}
	if(m_iEvent==2)
		bRead=fnReadFile();
	if(m_iEvent==1)
	{
		if(fnOpenFile())
			bRead=fnReadFile();
		else
		{
			m_Message.nError = 1;
			m_Message.csAction = "Parsing";
			m_Message.csStatus = _T("File is not exist");
			AddMessage(m_Message);
			
		}
	}
	if( bRead )
		m_iEvent = 2;
	else
		m_iEvent = -1;
	if(IsFieldNew(e_ManualParsed))
	{
		m_operation=e_Parsed;
		m_Message.nError = 0;
		m_Message.csAction = "Parsing";
		m_Message.csStatus = _T("File is Parsing...");
		AddMessage(m_Message);
		m_iEvent=1;
	}
	if(nAutoParsedFlag==1)
	{
		if(fnFileParsed())
			nAutoParsedFlag=0;
	}
	if(nAutoDecompressed==1)
	{
		if(fnDecompressed())
		{
			nAutoDecompressed=0;
			nAutoParsedFlag=1;
		}
	}
	
	if(IsFieldNew(e_CompressedFilename)&&GetField(e_AutoParsed)->GetValueInt() == 1)
	{
		nAutoDecompressed=1;
	}
	if(IsFieldNew(e_FileName)&&GetField(e_AutoParsed)->GetValueInt() == 1)
	{
		nAutoParsedFlag=1;
	}
	if(IsFieldNew(e_AutoParsed))
	{
		if(GetField(e_AutoParsed)->GetValueInt() == 0)
		{
			GetField(e_ManualParsed)->SetEnabled(true);
			GetField(e_Decompressed)->SetEnabled(true);
			nAutoDecompressed=0;
			nAutoParsedFlag=0;
		}
		else
		{
			GetField(e_ManualParsed)->SetEnabled(false);
			GetField(e_Decompressed)->SetEnabled(false);
			nAutoDecompressed=1;
		}
	}
	DisplayMessages();
	if(m_iEvent==2||m_iEvent==1)//nAutoParsedFlag==1 ||nAutoDecompressed==1||nevent==1)
		StartRecalcTimer(1,false);
}

bool CMEHistoricalDataParser::InitDataBase()
{
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));
	RegString strServerName(pKey, "ServerName","10.100.12.91");
	CString csServerName = strServerName.get_value().c_str();
	
	if(m_clDBConn.InitDatabase(this, csServerName, "CME_BCP","watchdog","watchdog","0", false))
	{
		m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
		m_Message.nError = 0;
		m_Message.csAction = "DB Operation";
		m_Message.csStatus = _T("Database connected successfully...");
		AddMessage(m_Message);
		return true;
	}
	else
	{
		m_Message.nError = 1;
		m_Message.csAction = "DB Operation";
		m_Message.csStatus = _T("Database connection Failed...");
		AddMessage(m_Message);
		return false;
	}
}
bool CMEHistoricalDataParser::fnDecompressed()
{
	string sCompressedFilePath=GetField(e_CompressedFilePath)->GetValueString();
	string sCompressedFileName=GetField(e_CompressedFilename)->GetValueString();
	CString sDecompressCommand;

	string slzopExeLocation=GetField(e_lzopexeLocation)->GetValueString();
	sDecompressCommand.Format("%s\\lzop.exe -1 -d %s\\%s",slzopExeLocation.c_str(),sCompressedFilePath.c_str(),sCompressedFileName.c_str());

	CString csCompressedFileName=GetField(e_CompressedFilename)->GetValueString();
	CString csCompressedFilePath,csCompressedFilePath1;
	FILE *pFile;

	csCompressedFilePath.Format("%s\\%s",GetField(e_FilePath)->GetValueString(),csCompressedFileName);
	csCompressedFilePath.Trim(".lzo");

	csCompressedFilePath1.Format("%s\\%s",GetField(e_CompressedFilePath)->GetValueString(),csCompressedFileName);

	if(!fopen_s(&pFile, csCompressedFilePath ,"r"))
	{
		fclose(pFile);
		m_Message.nError = 0;
		m_Message.csAction = "Decompression";
		m_Message.csStatus = _T("File Already Decompressed");
		AddMessage(m_Message);
		return true;

	}
	else if(fopen_s(&pFile, csCompressedFilePath1 ,"r"))
	{
		m_Message.nError = 1;
		m_Message.csAction = "Decompression";
		m_Message.csStatus = _T("File is not exist");
		AddMessage(m_Message);
		if(nAutoDecompressed==1)
		{
			nAutoDecompressed=0;
			nAutoParsedFlag=1;
		}
		return false;
	}
	else if(system(sDecompressCommand)==0)
	{
		m_Message.nError = 0;
		m_Message.csAction = "Decompression";
		m_Message.csStatus = _T("File decompressed successfully...");
		AddMessage(m_Message);
		
		return true;
	}
	else
	{
		m_Message.nError = 1;
		m_Message.csAction = "Decompression";
		m_Message.csStatus = _T("File is not decompressed successfully");
		AddMessage(m_Message);

		return false;
	}
}
bool CMEHistoricalDataParser::fnFileParsed()
{
//	CString csFileName=GetField(e_FileName)->GetValueString();
//	CString csFilePath;
//	FILE *pFile;
//
//	csFilePath.Format("%s\\%s",GetField(e_FilePath)->GetValueString(),csFileName);
//	
//	if(fopen_s(&pFile, csFilePath ,"r"))
//	{
//		m_Message.nError = 1;
//		m_Message.csAction = "Parsing";
//		m_Message.csStatus = _T("File is not exist");
//		AddMessage(m_Message);
//		nAutoParsedFlag=0;
//		return false;
//	}
//	else if(fnReadFile())
//	{
//
//		m_Message.nError = 0;
//		m_Message.csAction = "Parsing";
//		m_Message.csStatus = _T("File is Parsed Successfully...");
//		AddMessage(m_Message);
//		return true;
//	}
//	else
//	{
//		m_Message.nError = 1;
//		m_Message.csAction = "Parsing";
//		m_Message.csStatus = _T("File is not Parsed");
//		AddMessage(m_Message);
//		return false;
//	}
	return false;
}
void CMEHistoricalDataParser::GetFileName()
{
	COleDateTime dtTodayDate;

	dtTodayDate = COleDateTime::GetCurrentTime();
	CString csFileName = FILENAME;
	
	int month=0,day=0,year=0;
	CString csTempDate;
	if(dtTodayDate.GetDay()!=1)
		csTempDate.Format("%04d%02d%02d", dtTodayDate.GetYear(), dtTodayDate.GetMonth(), dtTodayDate.GetDay()-1);
	else
	{
		month=dtTodayDate.GetMonth()-1;
		year=dtTodayDate.GetYear();
		if(month==8)
			day=31;
		else if(month==2)
		{
			if(year%4==0)
				day=29;
			else
				day=28;
		}
		else if(month%2==0)
			day=30;
		else if(month%2!=0)
			day=31;

		if((month+1)==1)
			year=year-1;
		csTempDate.Format("%04d%02d%02d",year, month, day);
	}
	csFileName.Replace("YYYYMMDD", csTempDate);

	GetField(e_FileName)->SetValueString(csFileName);
	GetField(e_CompressedFilename)->SetValueString(csFileName+".lzo");
	
}
bool CMEHistoricalDataParser::fnOpenFile()
{
	m_csFileName=GetField(e_FileName)->GetValueString();//"md_ff_cme_20150111_chn0.dat";
	CString csFilePath;
	csFilePath.Format("%s\\%s",GetField(e_FilePath)->GetValueString(),m_csFileName);
	if(fopen_s(&m_pFile, csFilePath ,"r"))
		return false;
	else
		return true;

}
bool CMEHistoricalDataParser::fnReadFile()
{
	
	char chLine[1024];
	mstrLine="";
	int nCount=0;
	while(fgets(chLine, 1024, m_pFile))
	{
		mHeader="";
		mFooter="";
		
		if(strchr(chLine, '\n'))
		{
			nCount++;
			m_TotalLines++;
			mstrLine += chLine;

			if(strstr(mstrLine.c_str(),"35=d"))
			{
				fnSubmit_d_Values(mstrLine);
			}
			else
			{
				ExtractTokensToArray(mstrLine, "\01");
				if(nNumofSecurityId<=1)
				{
					if(!fnCheckInMemory(mstrLine,msecurityId))
					{
						if(!fnCheckInStg(mstrLine,msecurityId))
						{
							if(!fnCheckInMaster(mstrLine,msecurityId))
							{
							}
						}
					}
				}
				mstrLine = "";
				nNumofSecurityId=0;
			}
			if(nCount==100)
			{
				CString csStr;
				csStr.Format("%d Lines Processing....", (int)m_TotalLines);
				GetField(e_Message)->SetValueString( csStr );		
				return (nCount == 100);
			}
		}
		else
		{
			mstrLine += chLine;
		}
	}
	if(fclose(m_pFile) == EOF)
	{
		m_Message.nError = 1;
		m_Message.csAction = "Parsing";
		m_Message.csStatus = _T("File is not Parsed");
		AddMessage(m_Message);
		return false;
	}
	else
	{
		m_Message.nError = 0;
		m_Message.csAction = "Parsing";
		m_Message.csStatus = _T("File is Parsed Successfully...");
		AddMessage(m_Message);
		CString csStr;
		csStr.Format("%d Lines Processed", (int)m_TotalLines);
		GetField(e_Message)->SetValueString( csStr );		
		//return true;
		
	}
	for(m_Mapit=m_FileMap.begin();m_Mapit!=m_FileMap.end();m_Mapit++)
	{
		fclose(m_Mapit->second);
	}
	//return true;
}
bool CMEHistoricalDataParser::fnCheckInMemory(string strLine,string securityId)
{
	vector<ParserDetails>::iterator itParser;
	for(itParser=m_vcParserDetails.begin();itParser!= m_vcParserDetails.end();itParser++)
	{
		if(securityId.compare(itParser->sSecurity)==0)
		{
			fnCreateFolderStructure(strLine,itParser->sExchange,itParser->sProduct,itParser->sFOIType,itParser->nSpreadIn);
			return true;
		}
	}
	return false;
}
bool CMEHistoricalDataParser::fnCheckInStg(string strLine,string securityId)
{
	CString csQuery;
	csQuery.Format("Proc_beast_get_CME_Security_Definition_Stg '%s'",securityId.c_str());
	if( FAILED(m_clpdbInt->GetRecordset(&m_recordSet, csQuery, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, csQuery);
		ASSERT(0);
	}
	while(VARIANT_FALSE == m_recordSet->adoEOF)
	{
		SETSTR(m_ParserDetailsRow.sGroupCode,m_recordSet->Fields->Item[_variant_t("Group_Code")]->Value);
		SETSTR(m_ParserDetailsRow.sExchange,m_recordSet->Fields->Item[_variant_t("ExchangeCode")]->Value);
		SETSTR(m_ParserDetailsRow.sProduct,m_recordSet->Fields->Item[_variant_t("ProductCode")]->Value);
		SETSTR(m_ParserDetailsRow.sSecurity,m_recordSet->Fields->Item[_variant_t("SecurityId")]->Value);
		SETSTR(m_ParserDetailsRow.sFOIType,m_recordSet->Fields->Item[_variant_t("FOItype")]->Value);
		SETSTR(m_ParserDetailsRow.sProductDesc,m_recordSet->Fields->Item[_variant_t("ProductDesc")]->Value);
		m_ParserDetailsRow.nSpreadIn=m_recordSet->Fields->Item[_variant_t("SpreadIn")]->Value;
		m_vcParserDetails.push_back(m_ParserDetailsRow);

		fnCreateFolderStructure(strLine,m_ParserDetailsRow.sExchange,m_ParserDetailsRow.sProduct,m_ParserDetailsRow.sFOIType,m_ParserDetailsRow.nSpreadIn);
		return true;

		m_recordSet->MoveNext();
	}
	return false;
}
bool CMEHistoricalDataParser::fnCheckInMaster(string strLine,string securityId)
{
	CString csQuery;
	
	csQuery.Format("Proc_beast_get_CME_Security_Definition_mst '%s'",securityId.c_str());
	
	if( FAILED(m_clpdbInt->GetRecordset(&m_recordSet, csQuery, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, csQuery);
		ASSERT(0);
	}
	while(VARIANT_FALSE == m_recordSet->adoEOF)
	{
		SETSTR(m_ParserDetailsRow.sGroupCode,m_recordSet->Fields->Item[_variant_t("Group_Code")]->Value);
		SETSTR(m_ParserDetailsRow.sExchange,m_recordSet->Fields->Item[_variant_t("ExchangeCode")]->Value);
		SETSTR(m_ParserDetailsRow.sProduct,m_recordSet->Fields->Item[_variant_t("ProductCode")]->Value);
		SETSTR(m_ParserDetailsRow.sSecurity,m_recordSet->Fields->Item[_variant_t("SecurityId")]->Value);
		SETSTR(m_ParserDetailsRow.sFOIType,m_recordSet->Fields->Item[_variant_t("FOItype")]->Value);
		SETSTR(m_ParserDetailsRow.sProductDesc,m_recordSet->Fields->Item[_variant_t("ProductDesc")]->Value);
		m_ParserDetailsRow.nSpreadIn=m_recordSet->Fields->Item[_variant_t("SpreadIn")]->Value;
		
		m_vcParserDetails.push_back(m_ParserDetailsRow);

		fnStoreInStg(m_ParserDetailsRow.sGroupCode,m_ParserDetailsRow.sExchange,m_ParserDetailsRow.sProduct,m_ParserDetailsRow.sProductDesc,m_ParserDetailsRow.sSecurity,m_ParserDetailsRow.sFOIType,m_ParserDetailsRow.nSpreadIn);
		fnCreateFolderStructure(strLine,m_ParserDetailsRow.sExchange,m_ParserDetailsRow.sProduct,m_ParserDetailsRow.sFOIType,m_ParserDetailsRow.nSpreadIn);

		return true;

		m_recordSet->MoveNext();
	}
	return false;
}
void CMEHistoricalDataParser::fnStoreInStg(string cGroupCode,string sExchange,string sProduct,string sProductDesc,string sSecurityId,string sFOItype,int nSpreadIn)
{
	CString sql;


	sql.Format("EXEC Proc_beast_submit_CME_Security_Definition_Stg '%s','%s','%s','%s','%s','%s',%d",cGroupCode.c_str(),sExchange.c_str(),sProduct.c_str(),sProductDesc.c_str(),sSecurityId.c_str(),sFOItype.c_str(),nSpreadIn);

	if (FAILED(m_clDBConn.GetDBInterfaceNew()->GetRecordset(&m_recordSet, (LPCSTR)sql)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		ASSERT(0);
	}
}
bool CMEHistoricalDataParser::fnCreateFolderStructure(string strLine,string chExchange,string chProduct,string chFOIType,int nSpreadIn)
{
	CString csFolder,csCommand;
	if(nSpreadIn==1)
		chFOIType.append("Spread");

	string sParsedLocation=GetField(e_ParsedFileLocation)->GetValueString();
	csFolder.Format("%s\\%s\\%s\\%s",sParsedLocation.c_str(),chExchange.c_str(),chProduct.c_str(),chFOIType.c_str());
	csCommand.Format("mkdir -R %s",csFolder);
	

	CString csFileDate=m_csFileName.Mid(10,8);

	CString temp;
	temp.Format("%s_%s_%s_%s",chExchange.c_str(),chProduct.c_str(),chFOIType.c_str(),csFileDate);

	CString csFileName=csFolder+"\\"+temp+".txt";
	
	FILE *fp=NULL;

	if(m_FileMap.find(csFileName)==m_FileMap.end())
	{
		system(csCommand);
		if(fopen_s(&fp, csFileName ,"w"))
			return false;	
		fputs(strLine.c_str(),fp);
		m_FileMap.insert(std::pair<CString,FILE *>(csFileName,fp));
	}
	else
	{
		fp=m_FileMap[csFileName];
		fputs(strLine.c_str(),fp);
	}

	return true;
}

void CMEHistoricalDataParser::ExtractTokensToArray(string s, string Delimiters)
{
   int     Hit;
   int     EarliestHit;
   int     DelimiterIndex;
   string  sExtract;
   string  sSubExtract;
   BOOL    MoreTokens = TRUE;
   BOOL    GotToken = FALSE;
   TCHAR   CurrDelimiter;
  
   string sInbetweenlines="";

   sInbetweenlines=s.c_str();
	string sHeader,sFooter;
	sHeader="";
	sFooter="";
	int startpos=0,endpos=0;
	while (MoreTokens)
	{
      GotToken = FALSE;
	  EarliestHit = s.length();

      // Trawl the string looking for the leftmost (earliest) hit in
      // the list of valid separators.
      for (DelimiterIndex = 0; DelimiterIndex < Delimiters.length(); DelimiterIndex++)
	  {
         CurrDelimiter = Delimiters [DelimiterIndex];
		 Hit = s.find(CurrDelimiter);
         if (Hit != -1)
         {
            if (Hit < EarliestHit)
            {
               EarliestHit = Hit;
            }
            GotToken = TRUE;
         }
      }

		if (GotToken)
		{
			sExtract = s.substr(0,EarliestHit);
			s = s.substr(EarliestHit+1,s.length());

			if(strstr(sExtract.c_str(),"268="))
				sHeader=sHeader.append("268=1").append("\01");
			else
				sHeader=sHeader.append(sExtract).append("\01");
			sFooter=sExtract;
			sSubExtract = sExtract.substr(0,sExtract.find("="));

			int Pos = atoi(sSubExtract.c_str()); 

			switch(Pos)
			{
				case 207:
					mExchange=sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str();
					break;
				case 1151:
					mProduct=sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str();
					break;
				case 107:
					mProductDesc=sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str();
					break;
				case 55:
					mGroup=sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str();
					break;
				case 268:
					nNumofSecurityId=atoi(sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str());
					//mHeader=sHeader;
					if(nNumofSecurityId>1)
					{
						mHeader=sHeader;
						startpos=mHeader.length();
						endpos=sInbetweenlines.length()-mHeader.length()-2;
						//if(endpos<0)
						sInbetweenlines=sInbetweenlines.substr(startpos,endpos);

					}
					break;
				case 48:
					if(nNumofSecurityId<=1)
					{
						msecurityId=sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str();
					}
					break;
				case 10:
					if(nNumofSecurityId>1)
					{
						mFooter=sFooter;
						endpos=sInbetweenlines.length()-mFooter.length();
						sInbetweenlines=sInbetweenlines.substr(0,endpos);
						sInbetweenlines.append("\n");
						ExtractTokensToMultipleSecurity(sInbetweenlines,"\01");
						//nNumofSecurityId=0;
					}
					break;
			}
		}
		else
		{
			MoreTokens = FALSE;
		}
	}
}
bool CMEHistoricalDataParser::fnSubmit_d_Values(string sline)
{
	ExtractTokensToArray(sline, "\01");

	CString sql;
	sql.Format("EXEC proc_beast_get_CME_Products_Mst '%s','%s','%s'",mGroup.c_str(),mExchange.c_str(),mProduct.c_str());

	
	if( FAILED(m_clpdbInt->GetRecordset(&m_recordSet, sql ,false)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		return false;
	}

	while( VARIANT_FALSE == m_recordSet->adoEOF )
	{
		SETSTR(m_ParserDetailsRow.sFOIType ,	m_recordSet->Fields->Item[_variant_t("FOI")]->Value);
		m_ParserDetailsRow.nSpreadIn=	m_recordSet->Fields->Item[_variant_t("SPREAD_IND")]->Value;
		m_recordSet->MoveNext();
	}
	m_recordSet->Close();

	m_ParserDetailsRow.sExchange=mExchange.c_str();
	m_ParserDetailsRow.sProduct=mProduct.c_str();
	m_ParserDetailsRow.sGroupCode=mGroup.c_str();
	m_ParserDetailsRow.sProductDesc=mProductDesc.c_str();
	m_ParserDetailsRow.sSecurity=msecurityId.c_str();

	m_vcParserDetails.push_back(m_ParserDetailsRow);

	CString csFileName=GetField(e_FileName)->GetValueString();

	sql="";
	sql.Format("EXEC proc_beast_submit_CME_Security_Definition_Mst_single '%s','%s','%s','%s','%s','%s',%d,%d,'%s'",mGroup.c_str(),mExchange.c_str(),mProduct.c_str(),mProductDesc.c_str(),msecurityId.c_str(),m_ParserDetailsRow.sFOIType.c_str(),m_ParserDetailsRow.nSpreadIn,115,csFileName);

	
	if( FAILED(m_clpdbInt->GetRecordset(&m_recordSet, sql ,false)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		return false;
	}
	sql="";
	sql.Format("EXEC Proc_beast_submit_CME_Security_Definition_Stg '%s','%s','%s','%s','%s','%s',%d",mGroup.c_str(),mExchange.c_str(),mProduct.c_str(),mProductDesc.c_str(),msecurityId.c_str(),m_ParserDetailsRow.sFOIType.c_str(),m_ParserDetailsRow.nSpreadIn);

	
	if( FAILED(m_clpdbInt->GetRecordset(&m_recordSet, sql ,false)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		return false;
	}
	return true;

}
void CMEHistoricalDataParser::ExtractTokensToMultipleSecurity(string s,string Delimiters)
{
	int     Hit;
   int     EarliestHit;
   int     DelimiterIndex;
   string  sExtract;
   string  sSubExtract;
   BOOL    MoreTokens = TRUE;
   BOOL    GotToken = FALSE;
   TCHAR   CurrDelimiter;
 
	string sMidLine="";
	msecurityId="";
	string sHeader=mHeader.c_str();
	string sFooter=mFooter.c_str();
	while (MoreTokens)
	{
      GotToken = FALSE;
	  EarliestHit = s.length();

      // Trawl the string looking for the leftmost (earliest) hit in
      // the list of valid separators.
      for (DelimiterIndex = 0; DelimiterIndex < Delimiters.length(); DelimiterIndex++)
	  {
         CurrDelimiter = Delimiters [DelimiterIndex];
		 Hit = s.find(CurrDelimiter);
         if (Hit != -1)
         {
            if (Hit < EarliestHit)
            {
               EarliestHit = Hit;
            }
            GotToken = TRUE;
         }
      }

		if (GotToken)
		{
			sExtract = s.substr(0,EarliestHit);
			s = s.substr(EarliestHit+1,(s.length()-EarliestHit+1));

			
			sSubExtract = sExtract.substr(0,sExtract.find("="));
			sMidLine=sMidLine+sExtract+"\01";
			
			int Pos = atoi(sSubExtract.c_str()); 

			int len=s.length();
			if(len<=1)
			{
				Pos=279;
			}
			switch(Pos)
			{

				case 279:
					if(msecurityId!="")
					{
						if(len>1)
							sMidLine=sMidLine.substr(0,sMidLine.length()-sExtract.length()-1);

						sMidLine=sHeader.append(sMidLine).append(sFooter).append("\01\n");
						if(!fnCheckInMemory(sMidLine,msecurityId))
						{
							if(!fnCheckInStg(sMidLine,msecurityId))
							{
								if(!fnCheckInMaster(sMidLine,msecurityId))
								{
								}
							}
						}
						sMidLine="";
						sMidLine.append(sExtract).append("\01");;
						msecurityId="";
						sHeader=mHeader.c_str();
						sFooter=mFooter.c_str();
					}
					break;
				case 48:
					msecurityId=sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str();
					break;
			}
		}
		else
		{
			MoreTokens = FALSE;
		}
	}
}

void CMEHistoricalDataParser::AddMessage(FTPMessages &clarFTPMessages)
{
	COleDateTime dtCurrentTime(COleDateTime::GetCurrentTime());

	clarFTPMessages.csDateTime.Format("%02d/%02d/%d %02d:%02d:%02d", dtCurrentTime.GetMonth(), dtCurrentTime.GetDay(),
		dtCurrentTime.GetYear(),dtCurrentTime.GetHour(),dtCurrentTime.GetMinute(),dtCurrentTime.GetSecond());

	m_vMessages.push_back( clarFTPMessages );	

	int iCnt = m_vMessages.size();
	if( iCnt > 100 )
		m_vMessages.erase(m_vMessages.begin(), m_vMessages.begin() + (iCnt - 20));
}

void CMEHistoricalDataParser::DisplayMessages()
{
	const int niDataRowCount = m_vMessages.size();

	FTPMessages stMessage;	
	int iGridRow = 0;

	for(int iDataRow = 0; iDataRow < niDataRowCount; iGridRow++, iDataRow++)		
	{
		if( iGridRow >= MAX_ROWS )
			break;

		stMessage = m_vMessages[niDataRowCount-iDataRow-1];

		GetField(e_Message_DateTime + iGridRow)->SetValueString(stMessage.csDateTime);
		GetField(e_Message_Action + iGridRow)->SetValueString(stMessage.csAction);
		GetField(e_Message_Status + iGridRow)->SetValueString(stMessage.csStatus);

		GetField(e_Message_Status + iGridRow)->SetToolTipText(stMessage.csStatus);

		if(stMessage.nError)
		{
			GetField(e_Message_DateTime + iGridRow)->SetForeColor(ColorManager::eAttribStaleBack);
			GetField(e_Message_Action + iGridRow)->SetForeColor(ColorManager::eAttribStaleBack);
			GetField(e_Message_Status + iGridRow)->SetForeColor(ColorManager::eAttribStaleBack);
		}
		else
		{
			GetField(e_Message_DateTime + iGridRow)->SetForeColor(ColorManager::eAttribGreenOnBlackFore);
			GetField(e_Message_Action + iGridRow)->SetForeColor(ColorManager::eAttribGreenOnBlackFore);
			GetField(e_Message_Status + iGridRow)->SetForeColor(ColorManager::eAttribGreenOnBlackFore);
		}
	}

	while(iGridRow < MAX_ROWS)
	{
		GetField(e_Message_DateTime + iGridRow)->SetValueString("");
		GetField(e_Message_Action + iGridRow)->SetValueString("");
		GetField(e_Message_Status + iGridRow)->SetValueString("");
		iGridRow++;
	}
}
