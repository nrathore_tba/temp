
#include <stdafx.h> 
#include "CMEBBOTNSS3BucketCopy.hpp"
#include <MailAutomation/MailAutomation.h>
#include "afxsock.h"

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(CMEBBOTNSS3BucketCopy)

CMEBBOTNSS3BucketCopy::CMEBBOTNSS3BucketCopy()
{	
}

CMEBBOTNSS3BucketCopy::~CMEBBOTNSS3BucketCopy()
{
}

bool CMEBBOTNSS3BucketCopy::InitData()
{
	cout<<"Start InitData()"<<endl;

	COleDateTime dtToday = COleDateTime::GetCurrentTime();
	BFDate bfToday;
	bfToday.SetYearMonthDate(dtToday.GetYear(),dtToday.GetMonth() - 1, dtToday.GetDay() ); 
	
	GetField(e_DefaultDate)->SetValueDate(bfToday); 

	GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));

	m_curTopRow= 0;
	for(m_numofRows = 0; GetField(e_SubscrID + m_numofRows); m_numofRows++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","UAT-RDS");
	csServerName = strServerName.get_value().c_str();
	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");

	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();

	cout<<"End InitData"<<endl;
	return true;
}
void CMEBBOTNSS3BucketCopy::Recalculate()
{
	cout<<"Start Recalculate()"<<endl;

	if( IsFieldNew(e_getSubScription) )
	{ 
		LoadSubscription();
	}

	if(IsFieldNew(e_uploadAllFiles))
	{	
		UploadAllFiles();
	}
	
	fnPageUpDown( );
	DisplaySubscription( );

	cout<<"End Recalculate"<<endl;
}
void CMEBBOTNSS3BucketCopy::LoadSubscription()
{
	m_clBBOSubV.clear(); 
	
	CString sql;	
	sql.Format("SELECT CUST_NAME, CONTACT_EMAIL, DEST_ACCOUNT, FTP_PASSWORD, VENUE_CODE, ALL_PRODUCTS, PROD_CODE, BBO_CODE, EXCH_CODE FROM dbo.subscription_2015 where FILE_TYPE = 'BBO' and Active = 1");
	
	//change
	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		ASSERT(0);
		return;
	}

	BFDate bfDate = GetField(e_DefaultDate)->GetValueDate();
	CString csCurrentDateTime;
	csCurrentDateTime.Format("%d%02d%02d",bfDate.GetYear(), bfDate.GetMonth()+1,bfDate.GetDate());
	CString csYY = csCurrentDateTime.Mid(2,2);
	CString csMMDD = csCurrentDateTime.Mid(4,4);

	int iCount(1);
	while(VARIANT_FALSE == set->adoEOF)
	{
		BBOSubscriptionDtl clBlockSub;

		clBlockSub.iOrderId   = iCount * 1000 ;
		//clBlockSub.iBLOCKType = GetField( e_gen_filetype )->GetValueInt();

		SETSTR(	clBlockSub.stCUST_NAME,		set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
		SETSTR(	clBlockSub.stCONTACT_EMAIL,	set->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);	
		SETSTR(	clBlockSub.stDEST_ACCOUNT,	set->Fields->Item[_variant_t("DEST_ACCOUNT")]->Value);	
		SETSTR(	clBlockSub.stFTP_PASSWORD,	set->Fields->Item[_variant_t("FTP_PASSWORD")]->Value);	
		SETSTR(	clBlockSub.stVENUE_CODE,	set->Fields->Item[_variant_t("VENUE_CODE")]->Value);	
		SETSTR(	clBlockSub.stALL_PRODUCTS,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);	
		SETSTR(	clBlockSub.stPROD_CODE,		set->Fields->Item[_variant_t("PROD_CODE")]->Value);	
		SETSTR(	clBlockSub.stBBO_CODE,	set->Fields->Item[_variant_t("BBO_CODE")]->Value);	//change
		SETSTR(	clBlockSub.stEXCH_CODE,		set->Fields->Item[_variant_t("EXCH_CODE")]->Value);

		if( clBlockSub.stEXCH_CODE.GetLength() > 0 )
			clBlockSub.strExchangeV.push_back( clBlockSub.stEXCH_CODE.GetString() );

		if( clBlockSub.stBBO_CODE.GetLength() > 0 ) 
			clBlockSub.strProductV.push_back( clBlockSub.stBBO_CODE.GetString() ); 
		if(clBlockSub.stALL_PRODUCTS == "1")
			clBlockSub.stOutputFileName = _T("All Product Files");
		else
		{
			CString csProsuctFile = clBlockSub.stEXCH_CODE + "_" + clBlockSub.stPROD_CODE + _T("_FUT");
			if(clBlockSub.stVENUE_CODE == _T("F"))
				csProsuctFile = csProsuctFile + "_" + _T("_FUT"); 
			if(clBlockSub.stVENUE_CODE == _T("X"))
				csProsuctFile = csProsuctFile + "_" + _T("_OPT");
						
			clBlockSub.stOutputFileName = csProsuctFile + "_" + csYY + csMMDD + ".ZIP";
			//clBlockSub.stOutputFileName = csProsuctFile + "_" + _T("150828") + ".zip";
		}

		m_clBBOSubV.push_back( clBlockSub );
		iCount++;
		set->MoveNext();
	}

	set->Close();	
}

void CMEBBOTNSS3BucketCopy::UploadAllFiles()
{
	BFDate bfDate = GetField(e_DefaultDate)->GetValueDate();

	CString csInputS3 = ((ListField*)GetField(e_sourcePath))->GetShortString();
	CString csProfile = ((ListField*)GetField(e_sourcePath))->GetLongString();

	CString csSourcePath;
	csSourcePath.Format("%s/daily/bbo/globex/",csInputS3);

	CString csDestinationPath;
	csDestinationPath = ((ListField*)GetField(e_destPath))->GetShortString();

	int nTotalSubscriber = 0;
	CString csCurrentDateTime;
	csCurrentDateTime.Format("%d%02d%02d",bfDate.GetYear(), bfDate.GetMonth()+1,bfDate.GetDate());

	CString csSourceS3Path = csSourcePath + csCurrentDateTime + _T("/");
	
	CString csDestinationFTPAccount;
	nTotalSubscriber = m_clBBOSubV.size();

	for(int iIndex = 0; iIndex < nTotalSubscriber; iIndex++)
	{
		csDestinationFTPAccount = csDestinationPath + m_clBBOSubV[iIndex].stDEST_ACCOUNT + _T("/");
		CString sExchange = m_clBBOSubV[iIndex].stEXCH_CODE + _T("*.ZIP");

		if( m_clBBOSubV[iIndex].stOutputFileName == _T("All Product Files") )
		{
			m_csS3Command.Format(_T("aws s3 cp --recursive --profile %s %s %s --profile s3user --exclude \"*\" --include \"%s\""), csProfile, csSourceS3Path,csDestinationFTPAccount, sExchange);
			system(m_csS3Command);
		}
		else	
		{
			CString sSourceUserFTP;
			sSourceUserFTP.Format(_T("%s%s"),csSourceS3Path,m_clBBOSubV[iIndex].stOutputFileName);
			m_csS3Command.Format(_T("aws s3 cp --profile %s %s --profile s3user %s"), csProfile, sSourceUserFTP, csDestinationFTPAccount);
			system(m_csS3Command);
		}
		GetField(e_awsCommand)->SetValueString(m_csS3Command);
	}
}
void CMEBBOTNSS3BucketCopy::fnPageUpDown()
{
	int iTotalRecord = m_clBBOSubV.size();

	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpBase,e_PgDnBase,iTotalRecord,m_numofRows,m_curTopRow);	
}

void CMEBBOTNSS3BucketCopy::DisplaySubscription()
{	
	int iRow = 0;
	for(unsigned int iIndex = m_curTopRow; iIndex < m_clBBOSubV.size(); iIndex++, iRow++)
	{
		if( iRow >= m_numofRows )
			break;

		BBOSubscriptionDtl &clEODSub= m_clBBOSubV[iIndex];

		GetField(e_SubscrID		+ iRow)->SetValueInt(clEODSub.iOrderId);  
		GetField(e_CUST_NAME	+ iRow)->SetValueString(clEODSub.stCUST_NAME);  

		GetField(e_CONTACT_EMAIL + iRow)->SetValueString(clEODSub.stCONTACT_EMAIL);  
		GetField(e_DEST_ACCOUNT_FTP_PASSWORD + iRow)->SetValueString(clEODSub.stDEST_ACCOUNT);  
		GetField(e_VENUE_CODE	+ iRow)->SetValueString(clEODSub.stVENUE_CODE);
		GetField(e_ALL_PRODUCTS + iRow)->SetValueString(clEODSub.stALL_PRODUCTS);
		GetField(e_PROD_CODE	+ iRow)->SetValueString(clEODSub.stPROD_CODE);
		GetField(e_BBO_CODE		+ iRow)->SetValueString(clEODSub.stBBO_CODE); //change
		GetField(e_EXCH_CODE	+ iRow)->SetValueString(clEODSub.stEXCH_CODE);
		GetField(e_OutputFileName + iRow)->SetValueString(clEODSub.stOutputFileName);		
	}

	while( iRow < m_numofRows )
	{
		GetField(e_SubscrID		+ iRow)->SetBlankState();  
		GetField(e_CUST_NAME	+ iRow)->SetBlankState();  

		GetField(e_CONTACT_EMAIL + iRow)->SetBlankState();  
		GetField(e_DEST_ACCOUNT_FTP_PASSWORD + iRow)->SetBlankState();  
		GetField(e_VENUE_CODE	+ iRow)->SetBlankState();  
		GetField(e_ALL_PRODUCTS + iRow)->SetBlankState();  
		GetField(e_PROD_CODE	+ iRow)->SetBlankState();  
		GetField(e_BBO_CODE		+ iRow)->SetBlankState();  //change
		GetField(e_EXCH_CODE	+ iRow)->SetBlankState();  
		GetField(e_OutputFileName + iRow)->SetBlankState();  

		iRow++;
	}
}
