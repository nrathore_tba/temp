
#include <stdafx.h> 
#include <fstream>
#include <sstream>
#include <stdafx.h> 
#include <shldisp.h>
#include <tlhelp32.h>
#include "ExecutionEODDateRange.hpp"
#include <MailAutomation/MailAutomation.h>
#include "afxsock.h"
#include "zip.h"

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(ExecutionEODDateRange)

const int		__niMsgCouunt = 200;
CString __csMsgArry[__niMsgCouunt] = { "" };

void ExecutionEODDateRange::AddMessage( CString csaNewMsg )
{	
	for( int iI = __niMsgCouunt-1; iI > 0; iI-- )
		__csMsgArry[iI] = __csMsgArry[iI -1];
	
	__csMsgArry[0] = csaNewMsg;	
}

void ExecutionEODDateRange::DisplayMsg()
{
	int iRow = 0;
	for( int iIndex = m_nCurTopRow; iIndex < __niMsgCouunt; iIndex++, iRow++ )
	{
		if( iRow >= m_nNumOfRows )
			break;
		
		BaseField * fldComment		= GetField(e_LogMsg + iRow);
		
		fldComment->SetValueString( __csMsgArry[iIndex] );
		fldComment->SetNotManual();
	}
	
	while( iRow < m_nNumOfRows )
	{
		BaseField * fldComment		= GetField(e_LogMsg + iRow);
		fldComment->SetBlankState();
		iRow++;
	}
}
UINT OTOEODExecution(void *pVoid)
{
	ExecutionEODDateRange *pOTOEODExecution = (ExecutionEODDateRange*)pVoid;
	
	
	pOTOEODExecution->m_bIsThreadRunning = true;
	pOTOEODExecution->m_bIsTrheadCompleted = false;
	pOTOEODExecution->AddMessage("EOD OTO Order processing starts...");
	
	pOTOEODExecution->LoadSubscription();
	pOTOEODExecution->LoadAllUserDetails();

	pOTOEODExecution->m_enmOperation = OP_ProcessCompleted;
	
	if(!pOTOEODExecution->m_sMailContent.IsEmpty())
	{
		//pOTOEODExecution->FnSendEmail(1,10);
		//pOTOEODExecution->m_sMailContent = _T("");
	}

	pOTOEODExecution->m_bIsThreadRunning = false;
	pOTOEODExecution->LoadSubscription();
	pOTOEODExecution->AddMessage("EOD OTO Order processing finished...");
	pOTOEODExecution->RequestExternalUpdate();

	return 0;
}
ExecutionEODDateRange::ExecutionEODDateRange()
{
	m_MessageLogger.LogMessage("EODImage: Started");
	m_nTotalFiles = 0;
	m_nRecordIndex = 0;
	m_nOrederId = 0;
	m_csOutputPath = _T("");
	m_csSourcePath = _T("");
	m_csDestinationPath = _T("");
	m_csFtpUserName = _T("");
	m_nFileCounter = 1;
	//m_csOrderIDS = _T("");
	m_sMailContent = _T("");
	m_bIsProcess = false;
}

ExecutionEODDateRange::~ExecutionEODDateRange()
{
	m_MessageLogger.LogMessage("EODImage Closed");
}

bool ExecutionEODDateRange::InitData()
{
	m_MessageLogger.LogMessage("EODImage: Initialize");
	m_clNextScheduleDtTime = COleDateTime::GetCurrentTime();
	cout<<"Start InitData()"<<endl;

	GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));

	m_curTopRow= 0;
	for(m_numofRows = 0; GetField(e_SubscrID + m_numofRows); m_numofRows++);

	m_nCurTopRow= 0;
	for(m_nNumOfRows = 0; GetField(e_LogMsg + m_nNumOfRows); m_nNumOfRows++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","UAT-RDS");
	m_csSourcePath = GetField(e_sourcePath)->GetValueString();
	m_csDestinationPath = GetField(e_destPath)->GetDisplayString();
	csServerName = strServerName.get_value().c_str();
	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");

	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	if( !IsRestoreUpdate() )
	{
		BF_Reg_Key_Ptr pKey1(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\CME"));

		int iFTPLocation;
		RegDWORD strServerName(pKey1, "S3FTPLocation",0);
		iFTPLocation = strServerName.get_value();
		GetField(e_destPath)->SetValueInt( iFTPLocation );
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();

	m_bIsThreadRunning = false;
	m_bIsTrheadCompleted = true;
	m_enmOperation = OP_None;

	cout<<"End InitData"<<endl;
	return true;
}

void ExecutionEODDateRange::Recalculate()
{
	cout<<"Start Recalculate()"<<endl;
	if(IsFieldNew(e_getSubScription))
	{
		LoadSubscription();
		m_vtEODFileDetails.clear();
		m_MessageLogger.LogMessage("EODImage: Subscription Loaded");
	}
	if(IsFieldNew(e_sourcePath))
	{
		CString csPathChange(_T(""));
		m_csSourcePath = GetField(e_sourcePath)->GetValueString();
		csPathChange.Format(_T("EODImage: Source path changed to %s"), m_csSourcePath);
		AddMessage(csPathChange);
		m_MessageLogger.LogMessage(csPathChange.GetString());
	}
	if(IsFieldNew(e_destPath))
	{
		CString csPathChange(_T(""));
		m_csDestinationPath = GetField(e_destPath)->GetDisplayString();
		csPathChange.Format(_T("EODImage: Destination path changed to %s"), m_csSourcePath);
		m_MessageLogger.LogMessage(csPathChange.GetString());
		AddMessage(csPathChange);
	}
	if(IsFieldNew(e_uploadFiles))
	{
		m_MessageLogger.LogMessage("Calendar details load...");
		m_csFtpUserName = GetField(e_userName)->GetValueString();
		if(m_csFtpUserName.IsEmpty())
		{
			GetField(e_Status)->SetValueString(_T("User name is required..."));
			return;
		}
		LoadUserDetails();
		m_MessageLogger.LogMessage("Calendar details loaded");
		if(m_nTotalFiles == 0)
		{
			CString csFilesNotFound(_T(""));
			csFilesNotFound.Format(_T("File not Found for User %s"), m_csFtpUserName);
			return;
		}
		else
		{
			FileCopyS3ToLocal();
			ReadEODFiles();
			if(m_bIsProcess == true)
			{
				CreateZipFile();
				UploadZip();
				m_bIsProcess = false;
				m_csFtpUserName = _T("");
			}
			else
			{
				CString csFileNotFound(_T(""));
				csFileNotFound.Format(_T("File not found for user %s"), m_csFtpUserName);
				m_MessageLogger.LogMessage(csFileNotFound.GetString());
				//AddMessage(csFileNotFound);
			}
			LoadSubscription();
			m_vtEODFileDetails.clear();
			m_nTotalFiles = 0;
			m_csFtpUserName = _T("");
		}

	}
	if( m_bIsThreadRunning )
	{
		GetField(e_Status)->SetValueString("OTO EOD Order Processing...");
		GetField(e_getSubScription)->SetEnabled(false);
		GetField(e_uploadFiles)->SetEnabled(false);
	}
	else
	{
		if( !m_bIsThreadRunning && !m_bIsTrheadCompleted )
		{
			m_bIsTrheadCompleted = true;
		}
		if(m_enmOperation == OP_ProcessCompleted)
		{
			m_enmOperation = OP_None;
			m_clNextScheduleDtTime = COleDateTime::GetCurrentTime();
			m_clNextScheduleDtTime += COleDateTimeSpan(0,0,60,0);
			GetField(e_getSubScription)->SetEnabled(true);
			GetField(e_uploadFiles)->SetEnabled(true);
		}
		if(m_enmOperation == OP_Process)
		{
			AfxBeginThread(OTOEODExecution, this);
		}
		if( m_enmOperation == OP_None )
			EODProcessingScheduler( );
		if( m_enmOperation != OP_None )
			StartRecalcTimer(0, false);
	}
		
	fnPageUpDown();
	ManageUpDown();
	DisplaySubscription();
	//DisplayDownloadFiles();
	DisplayMsg();	
	cout<<"End Recalculate"<<endl;
}

void ExecutionEODDateRange::LoadSubscription()
{
	m_clEODSubV.clear(); 
	int nTotalSub = 0;
	CString sql;	
	sql.Format("EXEC Proc_Beast_Get_Subscriptions 'EOD', 1");
	//change
	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		ASSERT(0);
		return;
	}
	int iCount(1);
	while(VARIANT_FALSE == set->adoEOF)
	{
		EODSubscriptionDtl clBlockSub;

		SETLONG(clBlockSub.iOrderId,		set->Fields->Item[_variant_t("ORDER_ID")]->Value);
		SETSTR(clBlockSub.stCUST_NAME,		set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
		SETSTR(clBlockSub.stCONTACT_EMAIL,	set->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);	
		SETSTR(clBlockSub.stFTP_PASSWORD,	set->Fields->Item[_variant_t("FTPPassword")]->Value);
		SETSTR(clBlockSub.stFTPUserName,	set->Fields->Item[_variant_t("FTPUserName")]->Value);
		SETSTR(clBlockSub.stALL_PRODUCTS,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);	
		SETSTR(clBlockSub.stPROD_CODE,		set->Fields->Item[_variant_t("PROD_CODE")]->Value);	
		SETSTR(clBlockSub.stEXCH_CODE,		set->Fields->Item[_variant_t("Exchange")]->Value);
		SETSTR(	clBlockSub.csSubProductCode, set->Fields->Item[_variant_t("Sub_Product_Code")]->Value);
		SETBFDATE(clBlockSub.dtStartDate,	set->Fields->Item[_variant_t("Startdate")]->Value);
		SETBFDATE(clBlockSub.dtEndDate,	    set->Fields->Item[_variant_t("EndDate")]->Value);
			
		m_clEODSubV.push_back( clBlockSub );
		set->MoveNext();
	}
	set->Close();
	nTotalSub = m_clEODSubV.size();
	if(nTotalSub <= 0)
	{
		GetField(e_Status)->SetValueString(_T("No subscription found..."));
		AddMessage("No subscription found...");
	}
	else
	{
		GetField(e_Status)->SetValueString(_T("All subscription loaded..."));
	}
}
CString ExecutionEODDateRange::GetDestinationPath()
{
	CString csFinalDestinationPath(_T(""));
	csFinalDestinationPath.Format(_T("%s%s/%s/"),m_csDestinationPath, m_csFtpUserName, _T("EOD"));
	return csFinalDestinationPath;
}
CString ExecutionEODDateRange::GetSourcePath(int iIndex)
{
	CString csFulloutputPath(_T(""));
	CString csFinalSourcePath(_T(""));
	int nYear, nMonth, nDay;
	CString csFileDate, csFileName, csMonth, csDay, csYear;
	csFileDate.Format(_T("%d%02d%02d"),m_vtEODFileDetails[iIndex].FileDate.GetYear(), m_vtEODFileDetails[iIndex].FileDate.GetMonth()+1, m_vtEODFileDetails[iIndex].FileDate.GetDate());
	csMonth.Format(_T("%02d"), m_vtEODFileDetails[iIndex].FileDate.GetMonth()+1);
	csDay.Format(_T("%02d"), m_vtEODFileDetails[iIndex].FileDate.GetDate());
	csYear.Format(_T("%d"),m_vtEODFileDetails[iIndex].FileDate.GetYear());
	csFulloutputPath = csFulloutputPath + m_vtEODFileDetails[iIndex].csExchange + _T("\\") + csYear + _T("\\") + csMonth + _T("\\");
	csFulloutputPath = csFulloutputPath + csFileDate + _T("\\") +  m_vtEODFileDetails[iIndex].csFileName;
	csFinalSourcePath.Format(_T("%s%s"),m_csSourcePath, csFulloutputPath);
	return csFinalSourcePath;
}
//S3 Source Path
CString ExecutionEODDateRange::GetS3SourcePath(int iIndex)
{
	CString csFulloutputPath(_T(""));
	CString csFinalSourcePath(_T(""));
	int nYear, nMonth, nDay;
	CString csFileDate, csFileName, csMonth, csDay, csYear;
	csFileDate.Format(_T("%d%02d%02d"),m_vtEODFileDetails[iIndex].FileDate.GetYear(), m_vtEODFileDetails[iIndex].FileDate.GetMonth()+1, m_vtEODFileDetails[iIndex].FileDate.GetDate());
	csMonth.Format(_T("%02d"), m_vtEODFileDetails[iIndex].FileDate.GetMonth()+1);
	csDay.Format(_T("%02d"), m_vtEODFileDetails[iIndex].FileDate.GetDate());
	csYear.Format(_T("%d"),m_vtEODFileDetails[iIndex].FileDate.GetYear());
	csFulloutputPath = csFulloutputPath + m_vtEODFileDetails[iIndex].csExchange + _T("/") + csYear + _T("/") + csMonth + _T("/");
	csFulloutputPath = csFulloutputPath + csFileDate + _T("/") +  m_vtEODFileDetails[iIndex].csFileName;
	csFinalSourcePath.Format(_T("%s%s"),m_csSourcePath, csFulloutputPath);
	return csFinalSourcePath;
}
CString ExecutionEODDateRange::GetCurrentTime()
{
	COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
	CString csCurrentTime;
	csCurrentTime.Format("%04d%02d%02d",dtCurrentTime.GetYear(), dtCurrentTime.GetMonth(), dtCurrentTime.GetDay());
	return csCurrentTime;
}
void ExecutionEODDateRange::SetTotalFiles()
{
	CString csTotalRecord(_T("")); 
	csTotalRecord.Format(_T("Total files found : %d"),m_nTotalFiles = m_vtEODFileDetails.size());
	GetField(e_Status)->SetValueString(csTotalRecord);
}
void ExecutionEODDateRange::ReadEODFiles()
{
	m_MessageLogger.LogMessage("Start ReadEODFiles()");
	if(m_nTotalFiles == 0)
		return;
	BOOL bFileRead = FALSE;
	FILE *fileOut(NULL);
	FILE *fileIn(NULL);
	int iCountTT = 1;
	CString csDirectory;
	HWND hwnd = NULL;
	const SECURITY_ATTRIBUTES *psa = NULL;
	csDirectory.Format(_T("D:\\OTO\\EOD\\%s\\%s\\"),m_csFtpUserName, GetCurrentTime());
	CString csLocalPath;
	csLocalPath.Format(_T("Local path for file generation : %s"),csDirectory);
	m_MessageLogger.LogMessage(csLocalPath.GetString());
	for(int iIndex = 0; iIndex < m_vtEODFileDetails.size(); iIndex++)
	{
		CString csFileCounterName(_T(""));
		csFileCounterName.Format(_T("%02d"),m_nFileCounter);
		CString csSplitFileName(_T(""));
		std::string strHeader(_T(""));
		csSplitFileName = _T("EOD_") + csFileCounterName + _T("_") + m_csFromDate + _T("_") + m_csToDate + _T(".csv");
		CString csFilePath = csDirectory + m_vtEODFileDetails[iIndex].csFileName; //GetSourcePath(iIndex);
		CString csInputFile(_T(""));
		csInputFile.Format(_T("Input file Path : %s"),csFilePath);
		m_MessageLogger.LogMessage(csInputFile.GetString());
		CString csTemp = _T("");
		errno_t err;
		char chline[2048];
		std::string strLine("");
		CString csFullPath(_T(""));
		csFullPath.Format(_T("%s%s"), csDirectory, csSplitFileName);

		
		//err =fopen_s(&fileIn, "C:\\XCBT\\Test.txt", "r");
		err =fopen_s(&fileIn, csFilePath, "r");
		if(err != 0)
		{
			csTemp.Format(_T("Error while Opening file: %s"),csFilePath);
			GetField(e_Status)->SetValueString(csTemp);
			m_MessageLogger.LogMessage(csTemp.GetString());
			AddMessage(csTemp);
			continue;
		}
		else
		{
			csTemp = "Reading input File..";
			GetField(e_Status)->SetValueString(csTemp);
			m_MessageLogger.LogMessage(csTemp.GetString());
			while( fgets(chline, 2048, fileIn) )
			{
				if(strchr(chline, '\n'))
				{
					strHeader = chline;
					strLine = "";
					break;
				}
			}
		}
		
		if(bFileRead == FALSE)
		{
			if( SHCreateDirectoryEx(hwnd, csDirectory, psa) == 0 )
			{	
				csTemp = "Full Path not found. Full path created";	
				GetField(e_Status)->SetValueString(csTemp);
				m_MessageLogger.LogMessage(csTemp.GetString());
			}
		
			err = fopen_s(&fileOut, csFullPath, "w");
			if(err != 0)
			{
				csTemp.Format(_T("Error while Opening file: %s"),csFilePath);
				GetField(e_Status)->SetValueString(csTemp);
				m_MessageLogger.LogMessage(csTemp.GetString());
				AddMessage(csTemp);

				return;
			}
			else
			{
				csTemp.Format(_T("%s file open to write data"),csFullPath);
				GetField(e_Status)->SetValueString(csTemp);
				m_MessageLogger.LogMessage(csTemp.GetString());
				fputs(strHeader.c_str(),fileOut);
				// Read header from the file.
			}
			bFileRead = TRUE;
		}

		strLine = ("");
		int lRecordRead(0);
		
		while( fgets(chline, 2048, fileIn) )
		{
			if(strchr(chline, '\n'))
			{
				strLine += chline;
						
				lRecordRead++;
				iCountTT++;
				fputs(strLine.c_str(),fileOut);
				strLine = "";
				if( iCountTT == 50000 )
				{
					csTemp.Format(_T("Created File: %s"),csSplitFileName);
					GetField(e_Status)->SetValueString(csTemp);
					m_MessageLogger.LogMessage(csTemp.GetString());
					fclose(fileOut);
					m_nFileCounter++;
					csFileCounterName.Format(_T("%02d"), m_nFileCounter);
					m_MessageLogger.LogMessage(csFileCounterName.GetString());
					AddMessage(csTemp);
					fileOut = NULL;
				}
				if(fileOut == NULL)
				{
					iCountTT = 1;
					csSplitFileName = _T("EOD_") + csFileCounterName + _T("_") + m_csFromDate + _T("_") + m_csToDate + _T(".csv");
					csFullPath.Format(_T("%s%s"), csDirectory, csSplitFileName);
					err =fopen_s(&fileOut, csFullPath, "w");
					if(err != 0)
					{
						csTemp.Format(_T("Error while Opening file: %s"),csFilePath);
						GetField(e_Status)->SetValueString(csTemp);
						m_MessageLogger.LogMessage(csTemp.GetString());
						AddMessage(csTemp);
						return;
					}
					else
					{
						csTemp = "Reading input File..";
						GetField(e_Status)->SetValueString(csTemp);
						m_MessageLogger.LogMessage(csTemp.GetString());
						fputs(strHeader.c_str(),fileOut);						
					}
				}
			}
		}
		fclose(fileIn);
		DeleteFile(csFilePath);
	}
	if(fileOut != NULL)
	{
		fclose(fileOut);
		m_bIsProcess = true;
	}
	m_MessageLogger.LogMessage("End ReadEODFiles()");
}
void ExecutionEODDateRange::CreateZipFile()
{
	CString csZipName;
	CString csDirectory;
	csDirectory.Format(_T("D:\\OTO\\EOD\\%s\\%s\\"),m_csFtpUserName, GetCurrentTime());
	csZipName.Format(_T("EOD_%s_%s.zip"),m_csFromDate, m_csToDate);
	m_MessageLogger.LogMessage(csZipName.GetString());
	HZIP hz = CreateZip(csDirectory + csZipName, 0);
	CString sFilePath = "";
	CString sFileName = "";
	for( int nFile = 1; nFile <= m_nFileCounter; nFile++ )
	{	 
		sFileName.Format("EOD_%02d_%s_%s.csv", nFile, m_csFromDate, m_csToDate);

		sFilePath.Format("%s\\%s", csDirectory, sFileName);
		ZipAdd(hz, sFileName, sFilePath);
		m_MessageLogger.LogMessage(sFilePath.GetString());
	}
	m_nFileCounter = 1;
	m_csZipUploadPath = csDirectory + csZipName;
	CloseZip(hz);
	GetField(e_Status)->SetValueString(_T("Zip file created..."));
	CString csZipFile;
	csZipFile.Format(_T("%s created"),csZipName);
	m_MessageLogger.LogMessage(csZipFile.GetString());
	AddMessage(csZipFile);
}
void ExecutionEODDateRange::FileCopyS3ToLocal()
{
	m_MessageLogger.LogMessage("Start FileCopyS3ToLocal()");
	if(m_nTotalFiles == 0)
	{
		GetField(e_Status)->SetValueString(_T("Files are not found "));
		m_MessageLogger.LogMessage("Return FileCopyS3ToLocal()");
		return;
	}
	CString csLocalDestinationPath = GetLocalDirectory();
	for(int iIndex = 0; iIndex < m_vtEODFileDetails.size(); iIndex++)
	{
		CString csSourcePath = GetS3SourcePath(iIndex);
		CString csCommand(_T(""));
		m_csS3Command.Format(_T("aws s3 cp --profile s3user %s --profile s3user %s"), csSourcePath, csLocalDestinationPath);
		int nRet = system(m_csS3Command);
		if( nRet == 0 )
		{
			GetField(e_Status)->SetValueString(_T("File downloaded successfully at local path..."));
			CString csTemp;
			csTemp.Format(_T("%s file  downloaded successfully..."), m_vtEODFileDetails[iIndex].csFileName);
			m_MessageLogger.LogMessage(csTemp.GetString());
			//AddMessage(csTemp);

		}
		else
		{
			CString csTemp;
			csTemp.Format(_T("Error while executing command %s "), m_csS3Command);
			//InsertErroReport("OTO EOD", m_csFtpUserName, m_vtEODFileDetails[iIndex].csFileName, "", m_csS3Command);
			GetField(e_Status)->SetValueString(_T(csTemp));
			m_MessageLogger.LogMessage(csTemp.GetString());
			AddMessage(csTemp);
		}
	}
	m_MessageLogger.LogMessage("End FileCopyS3ToLocal()");
}
void ExecutionEODDateRange::LoadUserDetails()
{
	m_vtEODFileDetails.clear();
	BOOL bUserFound = FALSE;
	int iFirst = 0;
	for(int nIndex = 0; nIndex < m_clEODSubV.size(); nIndex++)
	{
		if(m_clEODSubV[nIndex].stFTPUserName == m_csFtpUserName)
		{
			if(iFirst == 0)
			{
				m_bfFromDt = m_clEODSubV[nIndex].dtStartDate;
				m_bfToDt = m_clEODSubV[nIndex].dtEndDate;
				iFirst = 1;
			}
			LoadUploadFiles(nIndex);
			bUserFound = TRUE;
		}
	}
	if(bUserFound == FALSE)
	{
		GetField(e_Status)->SetValueString(_T("Given user not found..."));
	}
}
void ExecutionEODDateRange::LoadAllUserDetails()
{
	m_vtEODFileDetails.clear();
	BOOL bUserFound = FALSE;
	int iFirst = 0;
	CString csUserName = _T("");
	for(int nIndex = 0; nIndex < m_clEODSubV.size(); nIndex++)
	{
		csUserName =  m_clEODSubV[nIndex].stFTPUserName;
		csUserName.Trim(' ');
		for(int nUIndex = 0; nUIndex < m_clEODSubV.size(); nUIndex++)
		{
			if(!csUserName.IsEmpty())
			{
				if(m_clEODSubV[nUIndex].stFTPUserName == csUserName && m_clEODSubV[nUIndex].bProcessed == FALSE)
				{
					if(iFirst == 0)
					{
						m_bfFromDt = m_clEODSubV[nUIndex].dtStartDate;
						m_bfToDt = m_clEODSubV[nUIndex].dtEndDate;
						m_csFtpUserName = m_clEODSubV[nUIndex].stFTPUserName;
						bUserFound = TRUE;
						iFirst = 1;
					}
					LoadUploadFiles(nUIndex);
					m_clEODSubV[nUIndex].bProcessed = TRUE;
				
				}
				
			}
		}
		if(bUserFound == TRUE)
		{
			CString csTotalRecord(_T(""));
			csTotalRecord.Format(_T("Total files found : %d for user %s"),m_nTotalFiles = m_vtEODFileDetails.size(), csUserName);
			m_MessageLogger.LogMessage(csTotalRecord.GetString());
			AddMessage(csTotalRecord);
			bUserFound = FALSE;
			iFirst = 0;
			FileCopyS3ToLocal();
			ReadEODFiles();
			if(m_bIsProcess == true)
			{
				CreateZipFile();
				UploadZip();
				m_bIsProcess = false;
				m_csFtpUserName = _T("");
			}
			else
			{
				CString csTotalRecord(_T(""));
				csTotalRecord.Format(_T("File not found for user %s"), csUserName);
				m_MessageLogger.LogMessage(csTotalRecord.GetString());
				//AddMessage(csTotalRecord);
			}
			m_vtEODFileDetails.clear();
			m_csFtpUserName = _T("");
		}
	}
}
void ExecutionEODDateRange::ChangeOrderStatus()
{
	CString csOrderId(_T(""));
	CString csTemp(_T(""));
	for(int nIndex = 0; nIndex < m_clEODSubV.size(); nIndex++)
	{
		if(m_clEODSubV[nIndex].stFTPUserName == m_csFtpUserName)
		{
			csTemp.Format((_T("%d,")),m_clEODSubV[nIndex].iOrderId);
			csOrderId = csOrderId + csTemp;  
		}
	}
	if(!csOrderId.IsEmpty())
	{
		csOrderId.SetAt(csOrderId.GetLength()-1, ' ');
		csOrderId.Trim();
	}
	m_MessageLogger.LogMessage(csOrderId.GetString());
	COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
	CString csTimeStatus;
	csTimeStatus.Format("%d-%02d-%02d %02d:%02d:00",dtCurrentTime.GetYear(), dtCurrentTime.GetMonth(), dtCurrentTime.GetDay(), dtCurrentTime.GetHour(), dtCurrentTime.GetMinute());
	CString csSql = _T("");
	csSql.Format(_T("update CME_Order set DeliveryDate = '%s' , OrderStatus = 5 where order_id in (%s)"),csTimeStatus, csOrderId);
	m_MessageLogger.LogMessage(csSql.GetString());
	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, csSql, false)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSql);
		ASSERT(0);
		return;
	}
	m_MessageLogger.LogMessage("Order status changed in table");
	AddMessage("Order status changed in table");
}
void ExecutionEODDateRange::UploadZip()
{
	if(m_csZipUploadPath.IsEmpty())
	{
		GetField(e_Status)->SetValueString(_T("Zip file not found..."));
		m_MessageLogger.LogMessage("ZIP file not created...");
		return;
	}
	m_csS3Command.Format(_T("aws s3 cp %s --profile s3user %s"), m_csZipUploadPath, GetDestinationPath());
	int nRet = system(m_csS3Command);
	if( nRet == 0 )
	{
		CString csUploaded(_T(""));
		csUploaded.Format(_T("ZIP file uploaded successfully at %s location"), m_csS3Command);
		GetField(e_Status)->SetValueString(_T("File uploaded successfully..."));
		m_MessageLogger.LogMessage(csUploaded.GetString());

		AddMessage(csUploaded);
		unsigned long lsize = GetFileSize(m_csZipUploadPath.GetString());
		CString csZipFileName= m_csZipUploadPath.Mid(m_csZipUploadPath.ReverseFind('\\')+1);
		//InsertDeliveryReport("OTO EOD", m_csFtpUserName, csZipFileName, lsize, m_csS3Command);
		ChangeOrderStatus();
	}
	else
	{
		CString csTemp;
		csTemp.Format(_T("Error while executing command %s "), m_csS3Command);
		//InsertErroReport("OTO EOD", m_csFtpUserName, m_csZipUploadPath, "", m_csS3Command);
		GetField(e_Status)->SetValueString(_T(csTemp));
		m_MessageLogger.LogMessage(csTemp.GetString());
		AddMessage(csTemp);
	}
}
//Function called by LoadUserDetails() to load all the file for given ftp user 
void ExecutionEODDateRange::LoadUploadFiles(int iBtnIndex)
{
	BFDate  bfFromDt;
	BFDate  bfToDt;
	CString csExchange;
	CString csSubProduct;
	if( m_clEODSubV.size() > 0)
	{
		csExchange  = m_clEODSubV[iBtnIndex].stEXCH_CODE;
		csSubProduct   = m_clEODSubV[iBtnIndex].csSubProductCode;

		bfFromDt = m_clEODSubV[iBtnIndex].dtStartDate;
		bfToDt	 = m_clEODSubV[iBtnIndex].dtEndDate;
		//m_csFtpUserName = m_clEODSubV[iBtnIndex].stFTPUserName;
	}
	
	if( bfFromDt > bfToDt )
	{
		 GetField(e_Status)->SetValueString("Date range is not correct: Please check... ");
		 return;
	}
	if(bfFromDt < m_bfFromDt)
	{
		m_bfFromDt = bfFromDt;
	}
	if(bfToDt > m_bfToDt)
	{
		m_bfToDt = bfToDt;
	}
	CString csMaxFromDt, csMaxToDt;
	csMaxFromDt.Format("%d-%02d-%02d",	m_bfFromDt.GetYear(), m_bfFromDt.GetMonth()+1, m_bfFromDt.GetDate());
	csMaxToDt.Format("%d-%02d-%02d",	m_bfToDt.GetYear(), m_bfToDt.GetMonth()+1, m_bfToDt.GetDate());
	m_csFromDate = csMaxFromDt;
	m_csToDate = csMaxToDt;
	CString csFromDt, csToDt;
	csFromDt.Format("%d-%02d-%02d",	bfFromDt.GetYear(), bfFromDt.GetMonth()+1, bfFromDt.GetDate());
	csToDt.Format("%d-%02d-%02d",	bfToDt.GetYear(), bfToDt.GetMonth()+1, bfToDt.GetDate());
	m_csFromDate.Remove(_T('-'));
	m_csToDate.Remove(_T('-'));

	CString csSQL;
	csSQL.Format(_T("SELECT * FROM dbo.CME_Product_Calendar WHERE FileType = 'EOD' AND Exchange = '%s' AND product = '%s' AND FileDate >= '%s' AND FileDate <= '%s' order by FileDate"),csExchange, csSubProduct, csFromDt,csToDt);
	//csSQL.Format(_T("SELECT * FROM dbo.CME_Product_Calendar WHERE FileType = 'EOD' AND Exchange = 'XCBT' AND product = 'BO' AND FileDate >= '2015-08-01' AND FileDate <= '2015-08-15' order by FileDate"));
	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, csSQL, false)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSQL);
		ASSERT(0);
		return;
	}
	
	while (VARIANT_FALSE == set->adoEOF)
	{
		stEODFileDtl structEODDtl;

		SETSTR(structEODDtl.csFileName,		 set->Fields->Item[_variant_t("FileName")]->Value);
		SETBFDATE(structEODDtl.FileDate,	 set->Fields->Item[_variant_t("FileDate")]->Value);
		SETSTR(structEODDtl.csExchange,		 set->Fields->Item[_variant_t("Exchange")]->Value);	
		SETSTR(structEODDtl.csYear,		     set->Fields->Item[_variant_t("Year")]->Value);	
		SETSTR(structEODDtl.csMonth,		 set->Fields->Item[_variant_t("Month")]->Value);
		SETSTR(structEODDtl.csDay,		     set->Fields->Item[_variant_t("DAY")]->Value);	
		SETSTR(structEODDtl.csProduct,		 set->Fields->Item[_variant_t("Product")]->Value);	
		SETSTR(structEODDtl.csFutOpt,		 set->Fields->Item[_variant_t("FutOpt")]->Value);
		SETSTR(structEODDtl.csFileType,		 set->Fields->Item[_variant_t("FileType")]->Value);
		SETLONGLONG(structEODDtl.ulFileSize, set->Fields->Item[_variant_t("Size")]->Value);
		m_vtEODFileDetails.push_back(structEODDtl);

		set->MoveNext();
	}
	set->Close();
	CString csTotalRecord(_T("")); 
	csTotalRecord.Format(_T("Total files found : %d"),m_nTotalFiles = m_vtEODFileDetails.size());
	GetField(e_Status)->SetValueString(csTotalRecord);
	if(m_nTotalFiles == 0)
		return;
}
void ExecutionEODDateRange::DisplayDownloadFiles()
{
	int iRow = 0;
	for(int iIndex = m_nCurTopRow; iIndex < m_vtEODFileDetails.size(); iIndex++, iRow++)
	{
		if( iRow >= m_nNumOfRows )
			break;

		stEODFileDtl  &ObjFileDetail = m_vtEODFileDetails[iIndex];

		GetField(e_fileName	    + iRow)->SetValueString(ObjFileDetail.csFileName);  
		GetField(e_fileDate	    + iRow)->SetValueDate(ObjFileDetail.FileDate);  
		GetField(e_prodCode	    + iRow)->SetValueString(ObjFileDetail.csProduct);
		GetField(e_exchangeCode	+ iRow)->SetValueString(ObjFileDetail.csExchange);  
		GetField(e_futOpt       + iRow)->SetValueString(ObjFileDetail.csFutOpt);
		GetField(e_fileSize	    + iRow)->SetValueInt(ObjFileDetail.ulFileSize);
		GetField(e_FileType 	+ iRow)->SetValueString(ObjFileDetail.csFileType);		
	}

	while( iRow < m_nNumOfRows )
	{
		GetField(e_fileName	    + iRow)->SetBlankState();  
		GetField(e_fileDate	    + iRow)->SetBlankState();
		GetField(e_prodCode	    + iRow)->SetBlankState(); 
		GetField(e_exchangeCode	+ iRow)->SetBlankState();   
		GetField(e_futOpt       + iRow)->SetBlankState(); 
		GetField(e_fileSize	    + iRow)->SetBlankState(); 
		GetField(e_FileType     + iRow)->SetBlankState(); 	
		iRow++;
	}
}
void ExecutionEODDateRange::fnPageUpDown()
{
	int iTotalRecord = m_clEODSubV.size();

	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpBase,e_PgDnBase,iTotalRecord,m_numofRows,m_curTopRow);	
}
void ExecutionEODDateRange::ManageUpDown()
{
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_pgUP,e_pgDN,200,m_nNumOfRows,m_nCurTopRow);
}

void ExecutionEODDateRange::DisplaySubscription()
{	
	int iRow = 0;
	for(unsigned int iIndex = m_curTopRow; iIndex < m_clEODSubV.size(); iIndex++, iRow++)
	{
		if( iRow >= m_numofRows )
			break;

		EODSubscriptionDtl &clEODSub= m_clEODSubV[iIndex];

		GetField(e_SubscrID		+ iRow)->SetValueInt(clEODSub.iOrderId);  
		GetField(e_CUST_NAME	+ iRow)->SetValueString(clEODSub.stCUST_NAME);  
		GetField(e_DEST_ACCOUNT_FTP_PASSWORD + iRow)->SetValueString(clEODSub.stFTP_PASSWORD);  
		GetField(e_ALL_PRODUCTS + iRow)->SetValueString(clEODSub.stALL_PRODUCTS);
		GetField(e_PROD_CODE	+ iRow)->SetValueString(clEODSub.stPROD_CODE);
		GetField(e_EOD_CODE		+ iRow)->SetValueString(clEODSub.stFTPUserName); 
		GetField(e_EXCH_CODE	+ iRow)->SetValueString(clEODSub.stEXCH_CODE);
		GetField(e_SubProductCode	+ iRow)->SetValueString(clEODSub.csSubProductCode);
		GetField(e_startDate	+ iRow)->SetValueDate(clEODSub.dtStartDate);
		GetField(e_endDate  	+ iRow)->SetValueDate(clEODSub.dtEndDate);
	}

	while( iRow < m_numofRows )
	{
		GetField(e_SubscrID		+ iRow)->SetBlankState();  
		GetField(e_CUST_NAME	+ iRow)->SetBlankState();  
		GetField(e_DEST_ACCOUNT_FTP_PASSWORD + iRow)->SetBlankState();  
		GetField(e_ALL_PRODUCTS + iRow)->SetBlankState();  
		GetField(e_PROD_CODE	+ iRow)->SetBlankState();  
		GetField(e_EOD_CODE		+ iRow)->SetBlankState(); 
		GetField(e_EXCH_CODE	+ iRow)->SetBlankState();
		GetField(e_SubProductCode + iRow)->SetBlankState();
		GetField(e_startDate	+ iRow)->SetBlankState(); 
		GetField(e_endDate  	+ iRow)->SetBlankState(); 
		iRow++;
	}
}

CString ExecutionEODDateRange::GetLocalDirectory()
{
	CString csDirectory;
	CString csTemp;
	HWND hwnd = NULL;
	const SECURITY_ATTRIBUTES *psa = NULL;
	csDirectory.Format(_T("D:\\OTO\\EOD\\%s\\%s\\"),m_csFtpUserName, GetCurrentTime());
	if( SHCreateDirectoryEx(hwnd, csDirectory, psa) == 0 )
	{	
		csTemp = "Full Path not found. Full path created";	
		GetField(e_Status)->SetValueString(csTemp);
		m_MessageLogger.LogMessage(csTemp.GetString());
	}
	return csDirectory;
}
void MessageLog::LogMessage(string strMessage)
{
	HWND hwnd = NULL;
	const SECURITY_ATTRIBUTES *psa = NULL;
	FILE *pLogFile = NULL;
	errno_t err;
	const char *plogFilePath = m_strLogFIlePath.c_str();
	if(IsFileExist(m_strLogFIlePath))
	{
		err = fopen_s(&pLogFile, plogFilePath ,"a+");
		if (err == 0)
		{
			COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
			CString csTimeStamp;
			csTimeStamp.Format("%d-%02d-%02d %02d:%02d:%02d",dtCurrentTime.GetYear(), dtCurrentTime.GetMonth(), dtCurrentTime.GetDay(), dtCurrentTime.GetHour(), dtCurrentTime.GetMinute(), dtCurrentTime.GetSecond());
			fprintf(pLogFile, "%s\t%s\n", csTimeStamp, strMessage.c_str());
			fclose(pLogFile);
		}
	}
	else
	{
		SHCreateDirectoryEx(hwnd, "D:\\OTO\\EOD\\", psa);
		err = fopen_s(&pLogFile, plogFilePath ,"w");
		CString csFileHeader(_T("TimeStamp\t\t\tMessage"));
		if(err == 0)
		{
			fprintf(pLogFile, "%s\n", csFileHeader);
			fclose(pLogFile);
		}
	}
}
void ExecutionEODDateRange::EODProcessingScheduler()
{
	COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
	long lRecalcTime = 0;

	if( dtCurrentTime >= m_clNextScheduleDtTime)
	{
		// Process the OTO order	
		//LoadSubscription();
		//LoadAllUserDetails();
		GetField(e_Timer)->SetTitle("OTO EOD Order Processing");
		//m_clNextScheduleDtTime = COleDateTime::GetCurrentTime();
		//m_clNextScheduleDtTime += COleDateTimeSpan(0,0,2,0);
		m_enmOperation = OP_Process;
	}
	else if( dtCurrentTime < m_clNextScheduleDtTime )
	{
		COleDateTimeSpan dtSpan = m_clNextScheduleDtTime - dtCurrentTime;
		lRecalcTime = dtSpan.GetTotalSeconds();

		CString csTimer,csTmp;

		int iDays =	dtSpan.GetDays();
		int iLeftTotalHr = dtSpan.GetHours();
		int iLeftTotalMinute = dtSpan.GetMinutes();
		int iLeftTotalSecond = dtSpan.GetSeconds();

		if( iDays > 0 )
		{
			csTmp.Format("%d Day and ",iDays);					
			csTimer += csTmp;

			csTmp.Format("%02d:",iLeftTotalHr);					
			csTimer += csTmp;
		}
		else
		{
			if( iLeftTotalHr > 0 )
			{					
				csTmp.Format("%02d:",iLeftTotalHr);					
				csTimer += csTmp;
			}
		}

		csTmp.Format("%02d:",iLeftTotalMinute);					
		csTimer += csTmp;

		csTmp.Format("%02d",iLeftTotalSecond);
		csTimer += csTmp;				

		int iDate	= m_clNextScheduleDtTime.GetDay();
		int iMonth	= m_clNextScheduleDtTime.GetMonth();
		int iYear	= m_clNextScheduleDtTime.GetYear();
		int iHour	= m_clNextScheduleDtTime.GetHour();
		int iMinute = m_clNextScheduleDtTime.GetMinute();
		int iSecond = m_clNextScheduleDtTime.GetSecond();

		CString csStr;
		csStr.Format("Next EOD OTO Processing: %d-%d-%d %2d:%2d:%2d [Timer: %s]",iDate,iMonth,iYear,iHour,iMinute,iSecond, csTimer);

		GetField(e_Timer)->SetTitle(csStr);
		GetField(e_Timer)->SetTitleForeColor(ColorManager::eSignalPositiveLight);

		StartRecalcTimer(0, false);
	}
}
void ExecutionEODDateRange::InsertDeliveryReport(CString csPrdouct, CString csDestAccount, CString csFileName, unsigned long &lFileSize, CString &csAwscmd )
{
	CString csCheckSum = "";
	CString csExchange = "";

	CString csDeliveryTime;
	COleDateTime oleDateTime = COleDateTime::GetCurrentTime();
	csDeliveryTime.Format("%02d-%02d-%02d %02d:%02d", oleDateTime.GetYear(), oleDateTime.GetMonth(), oleDateTime.GetDay(), oleDateTime.GetHour(), oleDateTime.GetMinute());

	CString csSql;
	csSql.Format("EXEC Proc_Beast_Submit_CME_Daily_Delivery_Report '%s', '%s', '%s', %u, '%s','%s','%s','%s'", csPrdouct, csDestAccount, csFileName, lFileSize, csCheckSum, csDeliveryTime, csExchange, csAwscmd);
	
	_RecordsetPtr set;
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)csSql, true)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSql);
		PrintRawMessage(csSql);
		return;
	}
	csExchange.Format("Success-> Destination Account:\t%s\t\tFile Name:\t%s\t\tFile Size:\t%ld\n", csDestAccount, csFileName, lFileSize);
	m_sMailContent += csExchange;
}

void ExecutionEODDateRange::InsertErroReport(CString csPrdouct, CString csDestAccount, CString csFileName, CString csExchnage, CString &csErrorCmd )
{	
	CString csSql;
	csSql.Format("EXEC Proc_Beast_Submit_CME_Daily_Error_Report '%s', '%s', '%s', '%s', '%s'", csPrdouct, csDestAccount, csFileName, csExchnage, csErrorCmd);
	
	_RecordsetPtr set;
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)csSql, true)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSql);
		PrintRawMessage(csSql);
		return;
	}
	csSql.Format("Error-> Destination Account:\t%s\t\tAws Command:\t%s\n", csDestAccount, csErrorCmd);
	m_sMailContent += csSql;
}

const CString	__strSmtp	= "email-smtp.us-east-1.amazonaws.com";
const CString	__strLogin	= "AKIAIERFOJMRYCRRAHYQ";
const CString	__strPassword = "AvauV+bh9qVBlRQuCI5u9GMG2O1N2hajyhzE7MhyYANC"; 
const CString	__strSender	= "cmenotifications@thebeastapps.com";
const int		__iPort = 587;

void  ExecutionEODDateRange::FnSendEmail(int iType, int iRowCount)
{
	CSmtp mail;

	mail.SetSMTPServer(__strSmtp, __iPort, true);
	mail.SetSecurityType((SMTP_SECURITY_TYPE)USE_TLS);		
	mail.SetLogin(__strLogin);
	mail.SetPassword(__strPassword);
			
	//CC
	/*CString strSendTo		= GetField(e_T0)->GetValueString();
	CString strCC			= GetField(e_CC)->GetValueString();
	CString strSubject		= GetField(e_Subject)->GetValueString();
	CString strBody			= GetField(e_Body)->GetValueString();*/
	
	CString strSendTo		= "vcmops@thebeastapps.com,";
	CString strCC			= "mdonga@thebeastapps.com,";
	CString strSubject		= "";
	CString strBody			= "";
	CString strTemp			= "";

	if( iType == 1 )
	{
		strSubject = "OTO EOD Notification";
		//m_clScheduleInfo.csFilePath
		//strBody.Format("\nOTO EOD File Name: %s\n\n",m_clScheduleInfo.csFilePath);

		//strTemp.Format("Total Row Count: %d\n\n",iRowCount);
		//strBody += strTemp;

		strTemp.Format("\n\nThanks\nBeast Apps Team\n");
		m_sMailContent += strTemp;
		strBody = m_sMailContent;
	}
	
	//++++++++++++++++++++++++Send To+++++++++++++++++++++++
	if( !strSendTo.IsEmpty() )
	{
		if( strSendTo.Right(1).CompareNoCase(",") != 0 )
		{
			strSendTo = strSendTo + ",";
		}		
	}	
	
	if( strSendTo.Find(",") != -1 )
	{
		while(!strSendTo.IsEmpty())
		{
			CString tmpSendTo = strSendTo.Left(strSendTo.Find(","));			
			strSendTo.Delete(0,strSendTo.Find(",")+1);		
			tmpSendTo.TrimLeft();
			tmpSendTo.TrimRight();

			if(!tmpSendTo.IsEmpty())
				mail.AddRecipient(tmpSendTo);
		}
	}		
	else
	{			
		if( !strSendTo.IsEmpty() )
			mail.AddRecipient(strSendTo);
	}
	//+++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++ CC +++++++++++++++++++++++
	if( !strCC.IsEmpty() )
	{
		if( strCC.Right(1).CompareNoCase(",") != 0 )
		{
			strCC = strCC + ",";
		}		
	}		

	if( strCC.Find(",") != -1 )
	{
		while(!strCC.IsEmpty())
		{
			CString tmpCC = strCC.Left(strCC.Find(","));			
			strCC.Delete(0,strCC.Find(",")+1);		
			tmpCC.TrimLeft();
			tmpCC.TrimRight();

			if(!tmpCC.IsEmpty())
				mail.AddCCRecipient(tmpCC);
		}
	}		
	else
	{			
		if(!strCC.IsEmpty())
			mail.AddCCRecipient(strCC);
	}
	//+++++++++++++++++++++++++++++++++++++++++++++++
		
	mail.SetSenderMail(__strSender);
				
	if( !strBody.IsEmpty() )
		mail.AddBody(strBody);
				
	if( !strSubject.IsEmpty() )
		mail.SetSubject(strSubject);
		
	try
	{			
		mail.Send(NULL);		
		//GetField(e_Status)->SetValueString("Mail Sent successfully.");  	
	}
	catch(ECSmtp e)
	{
		CString stemp;
		stemp.Format("Mail Failed...! - %s", e.GetErrorText());		
		//GetField(e_Status)->SetValueString(stemp); 
		AddMessage( stemp );
		return;
	}
	catch(...)
	{
		CString stemp;
		stemp.Format("Mail Failed...! - Error Id: %d", GetLastError());		
		//GetField(e_Status)->SetValueString(stemp);  
		AddMessage( stemp );
		return;
	}

	AddMessage( "EMail sent successfully");
}