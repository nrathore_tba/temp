
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <shared/MDDDataHandler.hpp>

BF_NAMESPACE_BEGIN
using namespace std;

typedef std::map<CString,int> MapFTpConnect;

struct stPriceDtl
{
	stPriceDtl()
	{	
		iPackageId	= 0;
		strProdName = "";
		strExchange = "";	
		strAssetClass = "";	
		iDailyAmt	= 0;
		iFileFormat = 0;	
	}	

	int iPackageId;
	CString strProdName;
	CString strExchange;
	CString strAssetClass;
	int iDailyAmt;
	int iMonthlyAmt;	
	int iYearlyAmt;
	int iType;
	int iFileFormat;	
};

class CMEPriceMst : public BaseImage
{
public:
	
	enum FieldIDs
	{
		e_GetData		= 72,
		e_ProductList    = 200,
				
		e_PackageIdBase = 2500,
		e_PrdNameBase	= 2300,
		e_ExchagneBase	= 2400,
		e_AssetClassBase= 2250,
		e_DailyAmtBase	= 2000,
		e_MonthlyAmtBase= 2100,
		e_YarlyAmtBase	= 2130,
		e_TypeBase		= 2450,
		
		e_PgUp	= 92,
		e_PgDn	= 93,	

		e_Label_PgNo = 65,				
	};
	
	CMEPriceMst();
	~CMEPriceMst();	

	bool InitData();		
	void Recalculate();	
	
	void GetFilterData();
	
	void DisplayGrid();
	void ManageUpDownButton();
	void fnFTPConnect();
	void fnConnectALLFTP();
	std::vector<stPriceDtl> m_clPriceDtlV;

	bool CMEPriceMst::fnCheckAdminUpdate( );

private:
		
	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;
	vector<CString> vecFTPServers;
	
	int m_iCurrTopRow;
	
	ListField *fld_ProductList;
};

BF_NAMESPACE_END


