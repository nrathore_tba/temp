
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>

#define _ColumnCount 17
using namespace std;
BF_NAMESPACE_BEGIN

struct FileDetail
{
	CString csFileName;
	CString csFilePath;
	unsigned long lFileSize;
	int iStatus;
};	

typedef std::map<std::string, std::vector<std::string>> MessageMap;


class CMErlcProcessManager : public BaseImage
{
public:	
	enum FieldIDs
	{
		e_Message = 61,
		e_FileType = 200,
		e_Year = 201,
		e_Month = 202,
		
		e_Exchange		= 203,		
		e_FileFormat	= 204,
		e_Status		= 205,
		
		e_UncomparessedPath	= 100,
		e_FileList			= 101,

		e_OutputBasePath	= 102,
		e_OutputDate		= 103,

		e_LoadFromDB		= 112,
		e_StartProcess		= 113,		

		e_DisplayFileName	= 1000,
		e_DisplayFileSize	= 1050,

		e_DisplayFileNameDB = 2000,
		e_DisplayFileSizeDB	= 3000,
		e_DisplayFileStatusDB = 3050,

		e_PgUpBase	= 90,
		e_PgDnBase	= 91,

		e_PgUpBaseDB	= 92,
		e_PgDnBaseDB	= 93,		

		e_StartTime	= 95,
		e_EndTime	= 96,
	};
	
	CMErlcProcessManager();
	~CMErlcProcessManager();	
	
	bool InitData();		
	void Recalculate();	

private:
	int m_numofRowsDB;
	int m_curTopRowDB;
	
	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;
	
	map<CString,FILE *> m_FileMap;
	map<CString,FILE *>::iterator m_Mapit;	

	std::vector<FileDetail> m_clFileListDB;
	
	CString csExchange,csProdCode,csFOIType,csDate;
	CString m_csFolder;	

	int m_iStatus;	

private:
	void DisplayFileList( );	
	void fnPageUpDown( );
	void LoadFromDB( );
	bool fnOpenFile( FILE **m_pFile);

	bool fnParseCommonFields(string);
	bool fnWriteInFile(string);
	void fnUpdateFileStatus();
	CString fnGetCurrentDateTime();

public:
	bool	fnParse();
	bool	m_bIsThreadRunning;
	unsigned long long	m_lTotalRead;
	int		m_iOpenFileCount;

};

BF_NAMESPACE_END


