
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>

#include "afxinet.h"

using namespace std;

BF_NAMESPACE_BEGIN

struct SubscribeCustumer
{
	int csCustomerID;
	CString csCustomerName;
	CString csFirstName;
	CString csLastName;
	CString csUserName;
	CString csEmail;
	CString csS3AccessKey;
	CString csS3SecretKey;
	CString csFTPUrl;
	CString csFTPUserName;
	CString csFTPPassword;
	CString csDeliveryPref;
	CString csCustomerAcctNum;
	CString csCMEAcctNumber;
	CString csHDLA;
	SubscribeCustumer()
	{
		csCustomerID = 0;
		csCustomerName = _T("");
		csFirstName = _T("");
		csLastName = _T("");
		csUserName = _T("");
		csEmail = _T("");
		csS3AccessKey = _T("");
		csS3SecretKey = _T("");
		csFTPUrl = _T("");
		csFTPUserName = _T("");
		csFTPPassword = _T("");
		csDeliveryPref = _T("");
		csCustomerAcctNum = _T("");
		csCMEAcctNumber = _T("");
		csHDLA = _T("");
	}
};
typedef vector<SubscribeCustumer> SubscribeCustomerList;
class CMECustomerSubscription : public BaseImage
{
public:

	enum FieldIDs
	{
		e_Subscription = 1,
		e_Search = 2,
		e_Mail = 3,
		e_CustomerID = 100,
		e_CustomerName = 200,
		e_FirstName = 300,
		e_LastName = 400,
		e_UserName = 500,
		e_Email = 600,
		e_S3AccessKey = 700,
		e_S3SecretKey = 800,
		e_FTPUrl = 900,
		e_FTPUserName = 1000,
		e_FTPPassword = 1100,
		e_DeliveryPref = 1200,
		e_CustomerAcctNum = 1300,
		e_CMEAcctNumber = 1400,
		e_HDLA = 1500,
		e_Up = 91,
		e_Dn = 90
	};

	CMECustomerSubscription();
	~CMECustomerSubscription();	

	bool InitData();		
	void Recalculate();
	void InitDatabase();
	void DisplaySubscribeCustomer();
	void GetSubscribercustomerList();
	void ManageUpDown();
	void SearchEmail();
		
private:
	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;
	SubscribeCustomerList m_vecSubscribeCustumerList;

	int m_curTopRow;
	int m_numofRows;

};

BF_NAMESPACE_END
