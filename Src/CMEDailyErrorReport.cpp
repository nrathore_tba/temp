#include <fstream>
#include <sstream>
#include <stdafx.h> 
#include <shldisp.h>
#include <tlhelp32.h>
#include "CMEDailyErrorReport.hpp"

#define MAX_ROWS 27

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(CMEDailyErrorReport)

CMEDailyErrorReport::CMEDailyErrorReport()
{	
}

CMEDailyErrorReport::~CMEDailyErrorReport()
{
}

bool CMEDailyErrorReport::InitData()
{
	m_iCurrTopRow =0;
	
    GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));
	
	fld_ProductTyp = (ListField*)GetField(e_Product);

	if( !IsRestoreUpdate() )
	{
		COleDateTime dtToday = COleDateTime::GetCurrentTime();

		BFDate bfToday;
		bfToday.SetYearMonthDate(dtToday.GetYear(),dtToday.GetMonth() - 1, dtToday.GetDay() ); 
		GetField(e_DeliveryDate)->SetValueDate(bfToday); 		
		
		fld_ProductTyp->SetValueInt(0); 
	}

	InitDB();	
	
	return true;
}

void CMEDailyErrorReport::InitDB()
{
	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return ;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	// DB Connection Completed....
}

void CMEDailyErrorReport::Recalculate()
{		
	if( IsFieldNew(e_SearchBtn) || IsFieldNew(e_Product) || IsFieldNew(e_DeliveryDate) || IsFieldNew(e_DestAcc) || IsInitialRecalc() )
	{
		GetFilterData();		
	}

	ManageUpDownButton();
	DisplayGrid();
}

void CMEDailyErrorReport::GetFilterData()
{
	m_CmeDDReportData.clear();

	CString strFromDate, strToDate;
    BFDate bfDeliveryDate = GetField(e_DeliveryDate)->GetValueDate(); 		
	
	strFromDate .Format("%d-%02d-%02d 00:00:00",bfDeliveryDate.GetYear(), bfDeliveryDate.GetMonth() + 1, bfDeliveryDate.GetDate()); 

	bfDeliveryDate.OffsetDate(1);
	strToDate .Format("%d-%02d-%02d 00:00:00",bfDeliveryDate.GetYear(), bfDeliveryDate.GetMonth() + 1, bfDeliveryDate.GetDate()); 
	
	CString strProduct = fld_ProductTyp->GetShortString();  
	CString strDestAcc = GetField(e_DestAcc)->GetValueString();  

	CString sql;
	CString strPercent="%";	

	if( strProduct != "ALL"  && strDestAcc.GetLength() > 0 )
	{
		sql.Format("select * from CME_Daily_Error_Report where ProductName = '%s' AND DestAccount = '%s' AND Record_Create_DtTime >= '%s' and Record_Create_DtTime < '%s' order by Record_Create_DtTime desc", strProduct, strDestAcc, strFromDate, strToDate);
	}
	else if( strProduct != "ALL"  && strDestAcc.GetLength() == 0 )
	{
		sql.Format("select * from CME_Daily_Error_Report where ProductName = '%s' AND  Record_Create_DtTime >= '%s' and Record_Create_DtTime < '%s' order by Record_Create_DtTime desc",strProduct, strFromDate, strToDate);
	}
	else if( strProduct == "ALL"  && strDestAcc.GetLength() > 0 )
	{
		sql.Format("select * from CME_Daily_Error_Report where DestAccount = '%s' AND  Record_Create_DtTime >= '%s' and Record_Create_DtTime < '%s' order by Record_Create_DtTime desc", strDestAcc, strFromDate, strToDate);
	}
	else
	{
		sql.Format("select * from CME_Daily_Error_Report where Record_Create_DtTime >= '%s' and Record_Create_DtTime < '%s' order by Record_Create_DtTime desc", strFromDate, strToDate);
	}
	
	stFileData objstFileData;

	_RecordsetPtr set;	
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql, false)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		PrintRawMessage(sql);
		return;
	}

	while( VARIANT_FALSE == set->adoEOF )
	{
		SETSTR(	objstFileData.strProductName,	set->Fields->Item[_variant_t("ProductName")]->Value);	
		SETSTR(	objstFileData.strDestAcc,	set->Fields->Item[_variant_t("DestAccount")]->Value);	
		SETSTR(	objstFileData.strFileName,	set->Fields->Item[_variant_t("FileName")]->Value);	
		SETSTR(	objstFileData.strExchange,	set->Fields->Item[_variant_t("ExchangeCode")]->Value);	
		SETSTR(	objstFileData.strErrorCmd,	set->Fields->Item[_variant_t("ErrorCmd")]->Value);		
	
		m_CmeDDReportData.push_back( objstFileData );	

		set->MoveNext();
	}
	set->Close();
	
}

void CMEDailyErrorReport::ManageUpDownButton()
{
	const int numTrades = m_CmeDDReportData.size();
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUp,e_PgDn,numTrades,MAX_ROWS,m_iCurrTopRow);
}

void CMEDailyErrorReport::DisplayGrid()
{ 
    const int niDataRowCount = m_CmeDDReportData.size();	 
	int iGridRow = 0;

	stFileData objstFileData;

	for(int iDataRow = m_iCurrTopRow; iDataRow < niDataRowCount; iGridRow++, iDataRow++)		
	{
		if( iGridRow >= MAX_ROWS )
			break;

		objstFileData = m_CmeDDReportData.at(iDataRow);

		m_va[e_DisplayProductName + iGridRow].SetValueString(objstFileData.strProductName.Trim());
		m_va[e_DisplayeDestAcc + iGridRow].SetValueString(objstFileData.strDestAcc.Trim());
		m_va[e_DisplayFileName + iGridRow].SetValueString(objstFileData.strFileName.Trim());
		m_va[e_DisplayExchange + iGridRow].SetValueString(objstFileData.strExchange.Trim());
		m_va[e_DisplayErrorCmd + iGridRow].SetValueString(objstFileData.strErrorCmd.Trim());

		GetField(e_DisplayErrorCmd + iGridRow)->SetToolTipText(objstFileData.strErrorCmd);
		
	}

	//-------- Balnk all other lines of grid -------------------//
	while(iGridRow < MAX_ROWS)
	{
		GetField(e_DisplayProductName + iGridRow)->SetValueString("");
		GetField(e_DisplayeDestAcc + iGridRow)->SetValueString("");
		GetField(e_DisplayFileName + iGridRow)->SetValueString("");
		GetField(e_DisplayExchange + iGridRow)->SetValueString("");
		GetField(e_DisplayErrorCmd + iGridRow)->SetValueString("");
		GetField(e_DisplayErrorCmd + iGridRow)->SetToolTipText("");

		GetField(e_DisplayProductName + iGridRow)->SetBlankState();
		GetField(e_DisplayeDestAcc + iGridRow)->SetBlankState();
		GetField(e_DisplayFileName + iGridRow)->SetBlankState();
		GetField(e_DisplayExchange + iGridRow)->SetBlankState();
		GetField(e_DisplayErrorCmd + iGridRow)->SetBlankState();
		
		iGridRow++;
	}

	CString csPageNumber;
	int iCurrPage = m_iCurrTopRow / MAX_ROWS + 1;
	int iTotalPage;	
	if( (niDataRowCount%MAX_ROWS) == 0)
	{
		iTotalPage = (niDataRowCount/MAX_ROWS);	
	}
	else
	{
		iTotalPage = (niDataRowCount/MAX_ROWS) + 1;	
	}

	if(iTotalPage <= 0)
		iTotalPage = 1;

	csPageNumber.Format("%d/%d",iCurrPage,iTotalPage);
	GetField(e_Label_PgNo)->SetTitle(csPageNumber);
}