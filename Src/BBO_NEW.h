#pragma once



#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <limits>
#include <map>
#include <vector>
#include <algorithm>




#include <shared/BaseImage.hpp>

BF_NAMESPACE_BEGIN

// Test
struct stBBOMetaData
{
	std::ofstream * _fpOutPutFile;
	std::string _strKey;
	std::string _strProd;
	std::string _strExch;
	std::string _strFUTOPT;
	std::string _strFileName;
	long _count;
	std::string _strText;
	stBBOMetaData()
	{
		_fpOutPutFile=NULL;
		_count=0;
		_strText="";
		_strKey="";
		_strProd="";
		_strExch="";
		_strFileName="";
		_strFUTOPT ="";
	}
};

class CBBO
{
	std::string _strExchange;
	std::string _strFUTOPT;
	std::string _strProduct;
	std::string _strInputFileName;
	std::string _strInputPath;
	std::string _strOutputFilePath;
	std::string _strExceptionFileName;
	std::string _strIniFile;
	long _interval;
	BaseImage *_pImage;
	std::ifstream fpInputFile;
	std::map<std::string,stBBOMetaData> _mapBBOMetaData;
	std::vector<std::string>_stVecProducts;
	std::ofstream fpExceptionFile;

public:
	CBBO(void)
	{
		  _interval = 10000;
		  _strExchange = _strFUTOPT=_strProduct=_strInputFileName=_strInputPath=_strOutputFilePath=_strExceptionFileName=_strIniFile="";
	}

	~CBBO(void);

	void setProcessParameters();
	std::string createUnzip(std::string sInputFile);
	void updateImageFields(long & lCnt);
	bool OpenExceptionFile();
	bool Process();
	void GenereateMetaData();
	//bool WriteRecord(std::string &strKey,std::string & strLine);
	bool WriteRecord(std::string &strKey,std::string & strLine,std::string &strExch,std::string &strProd,std::string &strFutOpt)
	void CloseOpenFiles();
	bool OpenInputBBOFile(std::string strFileName);
	std::string formatTime(std::string  trdtime);
	void trim2(std::string & str);
	std::string formatEntryDate(std::string line);
	std::string formatPrice(std::string inPrice, std::string decimalLoc);
	void createZip();
	

	std::vector<std::string> split(const std::string& s, char seperator)
	{
		std::vector<std::string> output;
		std::string::size_type prev_pos = 0, pos = 0;
		while((pos = s.find(seperator, pos)) != std::string::npos)
		{
			std::string substring( s.substr(prev_pos, pos-prev_pos) );
			output.push_back(substring);
			prev_pos = ++pos;
		}
		output.push_back(s.substr(prev_pos, pos-prev_pos)); // Last word
		return output;
	}
	void setInerval(long linterval)
	{
		_interval = linterval;
	}
	void SetImage(BaseImage *pImage)
	{
		_pImage = pImage;
	}
	void setProduct(std::string strProd)
	{
		if(strProd == "*")
		{
			_stVecProducts.clear();
			_stVecProducts.push_back("*");
		}
		else
		{
			_stVecProducts.clear();
			_stVecProducts = split(strProd,';');
		}
		_strProduct = strProd;
	}
	bool IsProductFound(std::string & strProd)
	{
		if(_stVecProducts.size() == 1  && _stVecProducts[0] == "*")
			return true;
		if(std::find(_stVecProducts.begin(), _stVecProducts.end(), strProd)!=_stVecProducts.end())
			return true;
		else
			return false;
	}
	void setExchange(std::string strExch)
	{
		_strExchange = strExch;
	}
	void SetFUTOPT(std::string strFO)
	{
		_strFUTOPT = strFO;
	}
	void SetOutputBBOFilePath(std::string & strFilepath)
	{
		if(strFilepath.substr(strFilepath.length()-1,1) == "\\")
			_strOutputFilePath = strFilepath;
		else
			_strOutputFilePath = strFilepath +"\\";
	}
	std::string GetTime()
	{
		time_t     now = time(0);
		struct tm  tstruct;
		char       buf[80];
		localtime_s(&tstruct,&now);
		strftime(buf, sizeof(buf), "%Y%m%d", &tstruct);
		return std::string(buf);
	}
	void FlushOpenFiles()
	{
		std::map<std::string,stBBOMetaData>::iterator it = _mapBBOMetaData.begin();
		for(;it!=_mapBBOMetaData.end();it++)
		{
			it->second._fpOutPutFile->flush();
		}
	}
	void WriteFileProcessCount(std::string strFileName,long lcount)
	{
		char buff[100];
		_ltoa(lcount,buff,10);
		WritePrivateProfileString(strFileName.c_str(),"Count",buff,_strIniFile.c_str()) ;
		std::map<std::string,stBBOMetaData>::iterator it = _mapBBOMetaData.begin();
		for(;it!=_mapBBOMetaData.end();it++)
		{
			*it->second._fpOutPutFile<<it->second._strText.c_str();
			it->second._strText="";
		}
	}
	long GetFileProcessCount(std::string strFileName)
	{
		char buff[100];
		GetPrivateProfileString(strFileName.c_str(),"Count","0",buff,100,_strIniFile.c_str());
		return atol(buff);
	}

	void GotoLine(std::ifstream& file, long num)
	{
		std::string s;
		for (long i = 1; i <= num; i++)
			std::getline(file, s);
	}

	/*void UpdateRecord(std::string & strKey)
	{
		std::map<std::string,long>::iterator it;
		it = _stMDBBODetails.find(strKey);
		if(it != _stMDBBODetails.end())
			it->second++;
		else
			_stMDBBODetails.insert(std::pair<std::string,long>(strKey,0));
	}*/
};

BF_NAMESPACE_END