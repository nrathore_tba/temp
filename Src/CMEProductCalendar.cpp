#include <fstream>
#include <sstream>
#include <stdafx.h> 
#include <shldisp.h>
#include <tlhelp32.h>
#include "CMEProductCalendar.hpp"

#define MAX_ROWS 27

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(CMEProductCalendar)

CCriticalSection gCriticalSection;

CMEProductCalendar::CMEProductCalendar()
{	
}

CMEProductCalendar::~CMEProductCalendar()
{
}

bool CMEProductCalendar::InitData()
{
	m_iCurrTopRow = 0;
	
    GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));

	COleDateTime dtToday = COleDateTime::GetCurrentTime();
	BFDate bfToday;
	bfToday.SetYearMonthDate(dtToday.GetYear(),dtToday.GetMonth() - 1, dtToday.GetDay() ); 
		
	fld_PorcessListfld = (ListField*)GetField(e_ProcessFileList);
	fld_PorcessListfld->SetValueInt(-1);

	InitDB();

	m_iCurrentFile = 0;
	m_nTotalFiles = 0;

	m_bAnyIncorrectDate = false;
	return true;
}

void CMEProductCalendar::InitDB()
{
	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return ;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	// DB Connection Completed....
}

void CMEProductCalendar::Recalculate()
{	
	if( IsFieldNew(e_FileType) || IsFieldNew(e_Year) || IsFieldNew(e_Month)  || IsFieldNew(e_FileFormat) || IsFieldNew(e_Exchange) || IsFieldNew(e_ProcessFileList) )
	{
		// we donot want false data to go in...
		m_FileFilterData.clear(); 
		m_bAnyIncorrectDate = false;
		GetField( e_Message )->SetValueString("");
	}

	if( GetField(e_FileType)->GetValueInt() == 0 && GetField(e_FileFormat)->GetValueInt() < 2 )
	{
		GetField(e_LoadProcessFileList)->SetVisible( true );
		fld_PorcessListfld->SetVisible( true );
		//GetField(e_FileFormat)->SetVisible( true );
		GetField(e_ClearList)->SetVisible( true );
		//GetField(59)->SetVisible( true );				
	}
	else
	{
		GetField(e_LoadProcessFileList)->SetVisible( false );
		fld_PorcessListfld->SetVisible( false );
		//GetField(e_FileFormat)->SetVisible( false );
		GetField(e_ClearList)->SetVisible( false );
		//GetField(59)->SetVisible( false );
	}

	if( GetField(e_FileType)->GetValueInt() == 0 && GetField(e_FileFormat)->GetValueInt() == 2 )
	{
		GetField(56)->SetVisible( true );	
		GetField(e_Day)->SetVisible(true);
	}
	else
	{
		GetField(56)->SetVisible( false );	
		GetField(e_Day)->SetVisible(false);
		GetField(e_Day)->SetValueInt(0);
	}

	if( GetField(e_FileType)->GetValueInt() == 1 )
		((ListField*)GetField(e_Year))->SetDataString("1972=1972|1973|1974|1975|1976|1977|1978|1979|1980|1981|1982|1983|1984|1985|1986|1987|1988|1989|1990|1991|1992|1993|1994|1995|1996|1997|1998|1999|2000|2001|2002|2003|2004|2005|2006|2007|2008|2009|2010|2011|2012|2013|2014|2015|2016|2017|2018|2019|2020");
	else
		((ListField*)GetField(e_Year))->SetDataString("2005=2005|2006|2007|2008|2009|2010|2011|2012|2013|2014|2015|2016|2017|2018|2019|2020");

	if( IsFieldNew(e_LoadProcessFileList) )
	{
		fnLoadProcessFileList( );		
	}

	if( IsFieldNew(e_ClearList) )
	{
		m_clProcessFileListDB.clear( );
		fld_PorcessListfld->SetDataString("");
	}

	bool bEnable = ( (m_clProcessFileListDB.size( ) == 0) || (GetField(e_FileType)->GetValueInt() > 0) );

	GetField(e_FileType)->SetEnabled( bEnable );
	GetField(e_Year)->SetEnabled( bEnable );
	GetField(e_Month)->SetEnabled( bEnable );
	GetField(e_Exchange)->SetEnabled( bEnable );
	GetField(e_FileFormat)->SetEnabled( bEnable );

	if( IsFieldNew(e_LoadFileListBtn) )
	{		
		m_bAnyIncorrectDate = false;
		m_FileFilterData.clear(); 		

		if( GetField(e_FileType)->GetValueInt() == 0 )
		{	
			if( GetField(e_FileFormat)->GetValueInt() < 2 )
			{
				LPCTSTR pstr = GetField( e_BasePath )->GetValueString();
				Recurse(pstr);
			}
			else if( GetField(e_FileFormat)->GetValueInt() == 2 )
			{
				LPCTSTR pstr = GetField( e_FullFilePath )->GetValueString();
				Recurse(pstr);
			}
		}
		else
		{	
			LPCTSTR pstr = GetField( e_BasePath )->GetValueString();

			CString csFullPath;
			if( ((ListField*)GetField(e_Month))->GetValueInt() == 0 )// All month
				csFullPath.Format("%s\\%s\\%s",pstr, ((ListField*)GetField(e_Exchange))->GetShortString(),((ListField*)GetField(e_Year))->GetShortString());
			else
				csFullPath.Format("%s\\%s\\%s\\%s",pstr, ((ListField*)GetField(e_Exchange))->GetShortString(),((ListField*)GetField(e_Year))->GetShortString(),((ListField*)GetField(e_Month))->GetShortString());
			GetField( e_BasePath )->SetToolTipText(csFullPath);

			Recurse(csFullPath.GetString());			
		}

		CString csTemp;
		if( m_bAnyIncorrectDate )
			csTemp.Format("Dates are wrong, %d Product Files.", m_FileFilterData.size());
		else
			csTemp.Format("%d Product Files.", m_FileFilterData.size());

		GetField( e_Message )->SetValueString( csTemp );
	}

	if( IsFieldNew(e_ProductCalendar) )
	{
		CreateProductCalnderInDB();
	}	

	if( IsFieldNew(e_Compressgz) )
	{
		fnCompressAllFile();
	}

	if( IsFieldNew(e_RemoveFiles) )
	{
		fnRemoveFile();
	}

	if( GetField(e_FileType)->GetValueInt() == 0 && GetField(e_FileFormat)->GetValueInt() < 2 )
	{
		int iIndex = fld_PorcessListfld->GetValueInt();
		if( iIndex ^ -1 && iIndex <= m_clProcessFileListDB.size() )
		{
			GetField(e_BasePath)->SetValueString( m_clProcessFileListDB[iIndex].csOutputPath );	
			GetField( e_ProductCalendar )->SetEnabled(m_clProcessFileListDB[iIndex].iProudctCalender == 0 );
			GetField( e_Compressgz )->SetEnabled(m_clProcessFileListDB[iIndex].iProudctZip == 0 );
			GetField( e_RemoveFiles )->SetEnabled( m_clProcessFileListDB[iIndex].iProudctZip == 1 );
		}
		else
		{
			GetField(e_BasePath)->SetBlankState();
			GetField( e_ProductCalendar )->SetEnabled( false );
			GetField( e_Compressgz )->SetEnabled( false );
			GetField( e_RemoveFiles )->SetEnabled( false );
		}

		GetField(e_FullFilePath)->SetVisible(false);
	}
	else if( GetField(e_FileType)->GetValueInt() == 0 && GetField(e_FileFormat)->GetValueInt() == 2 )
	{
		//\\10.101.7.10\e$\daily\marketdepth\20150913_Output;
		CString csBasePath = GetField(e_BasePath)->GetValueString( );

		CString csPath;
		if( GetField(e_Exchange)->GetValueInt() == 0 )
			csPath.Format("%s\\%d%02d%02d_Output",csBasePath, GetField(e_Year)->GetValueInt(), GetField(e_Month)->GetValueInt(), GetField(e_Day)->GetValueInt());
		else
			csPath.Format("%s\\%d%02d%02d_Output\\%s",csBasePath, GetField(e_Year)->GetValueInt(), GetField(e_Month)->GetValueInt(), GetField(e_Day)->GetValueInt(), ((ListField*)GetField(e_Exchange))->GetShortString());

		GetField(e_FullFilePath)->SetValueString(csPath);

		GetField(e_FullFilePath)->SetVisible(true);
		GetField( e_ProductCalendar )->SetEnabled( true );
		GetField( e_Compressgz )->SetEnabled( false );
		GetField( e_RemoveFiles )->SetEnabled( false );
	}
	else
	{
		GetField(e_FullFilePath)->SetVisible(false);
		GetField( e_ProductCalendar )->SetEnabled( true );
		GetField( e_Compressgz )->SetEnabled( true );
		GetField( e_RemoveFiles )->SetEnabled( true );
	}
		
	ManageUpDownButton();
	DisplayGrid();
}

bool Sort_File(stFileNameParseData &lhs, stFileNameParseData &rhs)
{
	return ( lhs.lFileSize > rhs.lFileSize );
}

unsigned int __stdcall ZipWorkerThread(void *pVoid)
{
	CMEProductCalendar *pParser = (CMEProductCalendar*)pVoid;
	pParser->MultiThread_Zip();
	return 0;
}

void CMEProductCalendar::MultiThread_Zip()
{
	CString strCmd, strIn, strOut;
	int iCount(0);

	do
	{
		CSingleLock lock(&gCriticalSection);
		lock.Lock();

		iCount = getNextFile();

		if( iCount >= 0 )
		{
			//CString strlog;
			//strlog.Format("Zip Creator: Processing File (%d/%d)  %s", (iCount+1), m_vTextFiles.size(), m_vTextFiles[iCount].strFilePath.c_str());
			//m_cLogManager.fnAddMessage(strlog);
			//RequestExternalUpdate();
		}

		lock.Unlock();

		if( iCount >= 0 )
		{
			CString csFilePath =  m_FileFilterData[iCount].strPath; 
			CString csFileOut = csFilePath + ".gz"; 

			CString strcmd;
			strcmd.Format("gzip.exe -c %s > %s", csFilePath, csFileOut);
			int		iValue = system(strcmd);

			//strIn = m_vTextFiles[iCount].strFilePath.c_str();
			//strOut = strIn + ".gz";

			//if( !(boost::filesystem::exists(strOut.GetString())) )
			{
				strCmd.Format("gzip.exe -c \"%s\" > \"%s\"", strIn, strOut);
				int iValue = system(strCmd);
				if( iValue != 0 )
				{
					//strCmd.Format("Zip conversion of %s failed", strOut);
					//m_cLogManager.fnAddMessage(strCmd);
					//MessageUtil::getInstance()->LogToFile((LPCSTR)strCmd);
				}
			}
		}

	}while(iCount >= 0);
}

int CMEProductCalendar::getNextFile()
{
	int nResult(-1);
	
	if(m_iCurrentFile != -1)
	{
		nResult = m_iCurrentFile;
		m_iCurrentFile++;
		if( m_iCurrentFile >= m_nTotalFiles )
			m_iCurrentFile = -1;
	}

	return nResult;
}


void CMEProductCalendar::fnCompressAllFile( )
{
	/*CString strcmd;
	for( int iI = 0; iI < m_FileFilterData.size(); iI++ )
	{
		CString csFilePath =  m_FileFilterData[iI].strPath; 
		CString csFileOut = csFilePath + ".gz"; 

		strcmd.Format("gzip.exe -c %s > %s", csFilePath, csFileOut);
		int		iValue = system(strcmd);

		if( iValue == 0 )
		{
			//Success
		}
		else
		{
			//fail
		}
	}*/

	sort(m_FileFilterData.begin(), m_FileFilterData.end(), Sort_File);

	int nTotalThreads;
	BF_Reg_Key_Ptr pKey1(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\CME"));
	RegDWORD strCPUCore(pKey1, "MDCPUCore", 5);
	nTotalThreads = strCPUCore.get_value();

	HANDLE *handle = new HANDLE[nTotalThreads];

	m_iCurrentFile = 0;
	m_nTotalFiles = m_FileFilterData.size();

	for(int iCount(0); iCount < nTotalThreads; ++iCount)
	{
		handle[iCount] = (HANDLE)_beginthreadex(0, 0, &ZipWorkerThread, this, 0, 0);
	}

	WaitForMultipleObjects(nTotalThreads, handle, true, INFINITE);

	for(int iCount(0); iCount < nTotalThreads; ++iCount)
		CloseHandle(handle[iCount]);

	if( GetField(e_FileType)->GetValueInt() == 0 )
	{
		CString sql;
		sql.Format("UPDATE CME_BCP.dbo.CME_Historical_Files SET ProductZip = 1 WHERE [fileName] = '%s'", fld_PorcessListfld->GetShortString() );

		_RecordsetPtr set;
		if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql, true)))
		{
			SetErrorMessage(ErrorSeverity::e_Error, sql);
			PrintRawMessage(sql);
			return;
		}

		CString csTemp;
		csTemp.Format("%s Product Zip File Set", fld_PorcessListfld->GetShortString());
		GetField( e_Message )->SetValueString( csTemp );

		int iIndex = fld_PorcessListfld->GetValueInt();
		m_clProcessFileListDB[iIndex].iProudctZip = 1;
	}
}

void CMEProductCalendar::fnRemoveFile( )
{
	CString csFolder = GetField(e_BasePath)->GetValueString(  );

	for( int iI = 0; iI < m_FileFilterData.size(); iI++ )
	{
		CString csFilePath = m_FileFilterData[iI].strPath;
		
		if( DeleteFile(csFilePath) == 0 )	 
		{
			CString csTemp;
			csTemp.Format("error while removing File %s", csFilePath);
			GetField(e_Message)->SetValueString(csTemp);
			break;
		}
		else
		{		
			CString csTemp;
			csTemp.Format("removing File %s successfully", csFilePath);
			GetField(e_Message)->SetValueString(csTemp);
		}
	}
}

void CMEProductCalendar::Recurse(LPCTSTR pstr)
{	
	stFileNameParseData stFileDtl;

	stFileDtl.csFileType = GetField(e_FileType)->GetDisplayString();	
	stFileDtl.csExchange = GetField(e_Exchange)->GetDisplayString();	

	if( GetField(e_FileType)->GetValueInt() == 0 )
		stFileDtl.csFormat	 = GetField(e_FileFormat)->GetDisplayString();
	else
		stFileDtl.csFormat	 = "";

	CFileFind finder;

	// build a string with wildcards
	CString strWildcard(pstr);
	strWildcard += _T("\\*.*");

	// start working for files
	BOOL bWorking = finder.FindFile(strWildcard);

	vector<string> clTStringV;
	while (bWorking)
	{
		bWorking = finder.FindNextFile();

		// skip . and .. files; otherwise, we'd
		// recur infinitely!

		if (finder.IsDots())
			continue;

		// if it's a directory, recursively search it
		if (finder.IsDirectory())
		{			
			CString str = finder.GetFilePath();
			//cout << (LPCTSTR) str << endl;
			Recurse(str);
		}
		else
		{
			stFileDtl.strPath		= finder.GetFilePath();
			stFileDtl.strFileName	= finder.GetFileName();			
			stFileDtl.lFileSize		= finder.GetLength();

			CString	strFileName		=  stFileDtl.strFileName;
			SplitFileNameString(strFileName	, "_", clTStringV );

			CString csTempDate;
			if( clTStringV.size() == 3 )
			{
				stFileDtl.strProduct = clTStringV[1].c_str();
				stFileDtl.strFutOpt = "";//clTStringV[2].c_str();
				stFileDtl.strSpread = "";
				
				csTempDate = clTStringV[2].c_str();
				csTempDate.Replace(".csv","");				
			}
			else if( clTStringV.size() == 4 )
			{
				stFileDtl.strProduct = clTStringV[1].c_str();
				stFileDtl.strFutOpt = clTStringV[2].c_str();
				stFileDtl.strSpread = "";
				
				csTempDate = clTStringV[3].c_str();
				csTempDate.Replace(".csv","");				
			}
			else if( clTStringV.size() == 5 )
			{
				if(  GetField(e_FileFormat)->GetValueInt() == 2  )
					stFileDtl.csExchange = clTStringV[0].c_str();

				stFileDtl.strProduct = clTStringV[2].c_str();
				stFileDtl.strFutOpt = clTStringV[3].c_str();
				stFileDtl.strSpread = "";

				csTempDate = clTStringV[4].c_str();
				csTempDate.Replace(".csv","");	
			}
			else if( clTStringV.size() == 6 )
			{
				if(  GetField(e_FileFormat)->GetValueInt() == 2  )
					stFileDtl.csExchange = clTStringV[0].c_str();

				stFileDtl.strProduct = clTStringV[2].c_str();
				stFileDtl.strFutOpt = clTStringV[3].c_str();
				stFileDtl.strSpread = clTStringV[4].c_str();

				csTempDate = clTStringV[5].c_str();
				csTempDate.Replace(".csv","");		
			}

			CString Year  = csTempDate.Mid(0,4);
			CString Month = csTempDate.Mid(4,2);
			CString day   = csTempDate.Mid(6,2);

			stFileDtl.iYear = atoi(Year);
			stFileDtl.iMonth = atoi(Month);
			stFileDtl.iDay = atoi(day);

			if( stFileDtl.iYear == 0 || stFileDtl.iMonth == 0 || stFileDtl.iDay <= 0 || stFileDtl.iMonth > 12 || stFileDtl.iDay > 31 )
			{
				stFileDtl.bfDate = BFDate( );
				stFileDtl.bDateCorrect = false;
			}
			else
			{
				stFileDtl.bfDate = BFDate(stFileDtl.iYear, stFileDtl.iMonth-1, stFileDtl.iDay);
				stFileDtl.bDateCorrect = true;
			}
			
			if( GetField(e_FileType)->GetValueInt() == 0 && GetField(e_FileFormat)->GetValueInt() == 2 ) 
			{
				if( strFileName.Find(".gz") ^ -1 )
					m_FileFilterData.push_back(stFileDtl); 
			}
			else
			{
				if( strFileName.Find(".gz") == -1 )
					m_FileFilterData.push_back(stFileDtl); 
			}
		}
	}

	finder.Close();
}

void CMEProductCalendar::SplitFileNameString(CString cStrFileName, string delimiter, vector<string> &result)
{
	result.clear();

	size_t pos = 0;
	string fieldValue, str;

	str = cStrFileName;
	while( (pos = str.find(delimiter)) != string::npos )
	{
		fieldValue = str.substr(0, pos);
		if( fieldValue == "null" )
			fieldValue = "";

		result.push_back(fieldValue);
		str.erase(0, pos + delimiter.length());
	}
	result.push_back(str);	
}

void CMEProductCalendar::CreateProductCalnderInDB()
{
	int	iUserId = GetCreatorID();

	DB_TRY
	{
		for(int iI = 0; iI < m_FileFilterData.size(); iI++ )
		{
			stFileNameParseData &data = m_FileFilterData[iI];

			CString csDate;
			csDate.Format("'%d-%02d-%02d'",data.iYear, data.iMonth, data.iDay);

			CString csSql;
			csSql.Format("EXEC Proc_Beast_submit_CME_Product_Calendar '%s', %d, %d, %d, '%s','%s','%s','%s','%s','%s',%u, %d, ",data.csFileType, data.iYear, data.iMonth, data.iDay, data.csExchange, 
				data.csFormat, data.strFileName, data.strProduct, data.strFutOpt, data.strSpread, data.lFileSize, iUserId);
			csSql += csDate;

			_RecordsetPtr set;
			if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)csSql, true)))
			{
				SetErrorMessage(ErrorSeverity::e_Error, csSql);
				PrintRawMessage(csSql);
				return;
			}
		}
	}
	DB_CATCH("CreateProductCalnderInDB");

	if( GetField(e_FileType)->GetValueInt() == 0 && GetField(e_FileFormat)->GetValueInt() < 2 )
	{
		CString sql;
		sql.Format("UPDATE CME_BCP.dbo.CME_Historical_Files SET ProductCalendar = 1 where [fileName] = '%s'", fld_PorcessListfld->GetShortString() );

		_RecordsetPtr set;
		if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql, true)))
		{
			SetErrorMessage(ErrorSeverity::e_Error, sql);
			PrintRawMessage(sql);
			return;
		}

		int iIndex = fld_PorcessListfld->GetValueInt();
		m_clProcessFileListDB[iIndex].iProudctCalender = 1;
	}
	else if( m_FileFilterData.size() > 0 && GetField(e_FileFormat)->GetValueInt() == 2)
	{
		// This is for avro files new entry should be created from here..
		CString sql;
		sql.Format("EXEC [Proc_Beast_Submit_CME_Historical_File] '%s', %d, %d, %d, '%s', '%s', '%s','%s',%d",m_FileFilterData[0].csFileType,m_FileFilterData[0].iYear,m_FileFilterData[0].iMonth, m_FileFilterData[0].iDay, "Daily Avro Files", "ALL", "Avro", "Avro file Path", 0);

		_RecordsetPtr set;

		DB_TRY
		{
			if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql)))
			{
				SetErrorMessage(ErrorSeverity::e_Error, sql);
				PrintRawMessage(sql);
			}

		}
		DB_CATCH(sql);
	}

	CString csTemp;
	csTemp.Format("%s Product calender Set.", fld_PorcessListfld->GetShortString());
	GetField( e_Message )->SetValueString( csTemp );
}

void CMEProductCalendar::fnLoadProcessFileList( )
{
	m_clProcessFileListDB.clear();

	int iYear = GetField(e_Year)->GetValueInt();
	int iMonth = GetField(e_Month)->GetValueInt();
	CString csFileType = GetField(e_FileType)->GetDisplayString();	
	CString csExchange = GetField(e_Exchange)->GetDisplayString();
	CString csFormat = GetField(e_FileFormat)->GetDisplayString();

	CString sql;
	sql.Format("select * from cme_bcp.dbo.CME_Historical_Files  where FileType = '%s' AND [Year] = %d	AND [Month] = %d AND Exchange = '%s' And Format = '%s' and [Status] = 2",csFileType,iYear,iMonth, csExchange,csFormat );

	_RecordsetPtr set;
	CString csFileList = "", csTemp;


	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql, false)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		PrintRawMessage(sql);
	}

	FileDetail clFileDetail;
	clFileDetail.csFileType = csFileType;
	clFileDetail.csExchange = csExchange;
	clFileDetail.csFormat	= csFormat;
	clFileDetail.iYear		= iYear;
	clFileDetail.iMonth		= iMonth;

	while( VARIANT_FALSE == set->adoEOF )
	{
		SETSTR(	clFileDetail.csFileName,	set->Fields->Item[_variant_t("FileName")]->Value);	
		SETSTR(	clFileDetail.csPath,		set->Fields->Item[_variant_t("Path")]->Value);	
		SETSTR(	clFileDetail.csOutputPath,	set->Fields->Item[_variant_t("OutputPath")]->Value);	

		SETLONG(clFileDetail.iProudctCalender,	set->Fields->Item[_variant_t("ProductCalendar")]->Value);	
		SETLONG(clFileDetail.iProudctZip,	set->Fields->Item[_variant_t("ProductZip")]->Value);	

		SETLONG(clFileDetail.iDay,			set->Fields->Item[_variant_t("FileDate")]->Value);	

		if( csFileList.GetLength() == 0 )
		{
			csFileList.Format("%s",clFileDetail.csFileName);
		}
		else
		{
			csTemp.Format("|%s",clFileDetail.csFileName);
			csFileList = csFileList + csTemp;
		}

		m_clProcessFileListDB.push_back( clFileDetail );	

		set->MoveNext();
	}
	set->Close();
	
	int iIndex = fld_PorcessListfld->GetValueInt();
	if( iIndex >= 0 && iIndex <=  m_clProcessFileListDB.size() )
	{
		GetField(e_BasePath)->SetValueString( m_clProcessFileListDB[iIndex].csOutputPath );	
	}		
	else
	{
		fld_PorcessListfld->SetValueInt(0);

		if( m_clProcessFileListDB.size() > 0 )
		{
			iIndex = 0;
			fld_PorcessListfld->SetValueInt(0);
			GetField(e_BasePath)->SetValueString( m_clProcessFileListDB[0].csOutputPath );
		}
		else
		{
			GetField(e_BasePath)->SetValueString("");
			fld_PorcessListfld->SetValueInt(-1);
			iIndex = -1;
		}
	}

	fld_PorcessListfld->SetDataString(csFileList);	

	csTemp.Format("%d Process Files", m_clProcessFileListDB.size());
	GetField( e_Message )->SetValueString( csTemp );
}

void CMEProductCalendar::ManageUpDownButton()
{
	const int numTrades = m_FileFilterData.size();
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUp,e_PgDn,numTrades,MAX_ROWS,m_iCurrTopRow);
}

void CMEProductCalendar::DisplayGrid()
{ 
    const int niDataRowCount = m_FileFilterData.size();	 
	int iGridRow = 0;

	for(int iDataRow = m_iCurrTopRow; iDataRow < niDataRowCount; iGridRow++, iDataRow++)		
	{
		if( iGridRow >= MAX_ROWS )
			break;

		stFileNameParseData &stFileDtl = m_FileFilterData.at(iDataRow);

		m_va[e_DisplayFileType + iGridRow].SetValueString(stFileDtl.csFileType);
		m_va[e_DisplayeExch + iGridRow].SetValueString(stFileDtl.csExchange);
		m_va[e_DisplayFormat + iGridRow].SetValueString(stFileDtl.csFormat);
		m_va[e_DisplayDate + iGridRow].SetValueDate(stFileDtl.bfDate);

		if( stFileDtl.bDateCorrect )
			GetField(e_DisplayDate + iGridRow)->SetForeColor(ColorManager::eDefault);
		else
			GetField(e_DisplayDate + iGridRow)->SetForeColor(ColorManager::eSignalNegativeLight);

		m_va[e_DisplayFileName + iGridRow].SetValueString(stFileDtl.strFileName.Trim());
		m_va[e_DisplayFileProduct + iGridRow].SetValueString(stFileDtl.strProduct.Trim());
		m_va[e_DisplayFileFutOpt + iGridRow].SetValueString(stFileDtl.strFutOpt.Trim());
		m_va[e_DisplayFileSpread + iGridRow].SetValueString(stFileDtl.strSpread.Trim());
		m_va[e_DisplayFileSize + iGridRow].SetValueInt(stFileDtl.lFileSize);		
		
	}

	//-------- Balnk all other lines of grid -------------------//
	while(iGridRow < MAX_ROWS)
	{
		GetField(e_DisplayFileType + iGridRow)->SetValueString("");
		GetField(e_DisplayeExch + iGridRow)->SetValueString("");
		GetField(e_DisplayFormat + iGridRow)->SetValueString("");
		GetField(e_DisplayDate + iGridRow)->SetBlankState();

		GetField(e_DisplayFileName + iGridRow)->SetValueString("");
		GetField(e_DisplayFileProduct + iGridRow)->SetValueString("");
		GetField(e_DisplayFileFutOpt + iGridRow)->SetValueString("");
		GetField(e_DisplayFileSpread + iGridRow)->SetValueString("");

		GetField(e_DisplayFileSize + iGridRow)->SetBlankState();

		iGridRow++;
	}

	CString csPageNumber;
	int iCurrPage = m_iCurrTopRow / MAX_ROWS + 1;
	int iTotalPage;	
	if( (niDataRowCount%MAX_ROWS) == 0)
	{
		iTotalPage = (niDataRowCount/MAX_ROWS);	
	}
	else
	{
		iTotalPage = (niDataRowCount/MAX_ROWS) + 1;	
	}

	if(iTotalPage <= 0)
		iTotalPage = 1;

	csPageNumber.Format("%d/%d",iCurrPage,iTotalPage);
	GetField(e_Label_PgNo)->SetTitle(csPageNumber);
}