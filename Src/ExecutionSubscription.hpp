
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <shared/MDDDataHandler.hpp>

BF_NAMESPACE_BEGIN

using namespace std;


struct ScheduleInfo
{
	int		iRetInterval;
	int		iRetryCount;
	bool	bValid;
	int		iStatus;	

	COleDateTime clMainScheduleDtTime;	
	COleDateTime clScheduleDtTime;	
	CString csDate;
	//CString csDateBlock;

	CString  csFilePath;
	/*CString	 csBLOCP_FilePath;
	CString  csBLOCK_FilePath;*/
};

enum enmOperation
{
	OP_Download,
	OP_Uncompress,
	OP_Process,	
	OP_ProcessCompleted,
	OP_None
};

enum ColumnName
{
	enm_CUST_NAME,
	enm_ADDRESS_1,
	enm_ADDRESS_2,
	enm_CITY,
	enm_REGION,
	enm_COUNTRY,
	enm_ZIP,
	enm_CONTACT_PHONE,
	enm_CONTACT_NAME,
	enm_CONTACT_EMAIL,
	enm_DEST_ACCOUNT,
	enm_FTP_PASSWORD,
	enm_EXPIRATION_DATE,
	enm_ACTIVE,
	enm_CREATE_TIMESTAMP,
	enm_VENUE_CODE,
	enm_FILE_TYPE,
	enm_ALL_PRODUCTS,
	enm_PROD_CODE,
	enm_BBO_CODE,
	enm_MD_CODE,
	enm_BLOCK_CODE,
	enm_EOD_CODE,
	enm_TICK_CODE,
	enm_EXCH_CODE,
	enm_FOI_CODE,
	enm_END,
};

struct SubkDetail
{
	std::vector<std::string> strArray;

	void GetDBParam( string &strDBmsg )
	{
		strDBmsg  = "'" + strArray[enm_CUST_NAME] + "'";
		for( int iI = 1; iI < strArray.size(); iI++ )
		{
			if( iI == enm_ACTIVE || iI ==  enm_ALL_PRODUCTS )
			{
				strDBmsg  += "," + strArray[iI];
			}
			else
			{
				strDBmsg  += ",'" + strArray[iI] + "'";
			}
		}
	}
};

typedef std::vector<SubkDetail> SubkDetailV;

class ExecutionSUBSCR : public BaseImage
{
public:
	
	enum FieldIDs
	{
		e_NextDownloadMsg = 50,	
		e_MsgTitle		  = 65,
		e_OutputPath	  = 62,

		e_ReloadSchedule  = 100,
		e_DateFilter	  = 95,
		e_ManualRun		  = 96,
		e_Process		  = 97,
		e_RtryInterval	  = 98,
		e_FilterProdExch  = 99,

		//e_SubType	= 212,
		e_Year		= 201,
		e_Month		= 202,
		e_Day		= 215,
		e_Hour		= 213,
		e_Minute	= 214,

		e_S3CMEBucket = 60,
		e_DestAccount = 58,
		e_S3FTPBucket = 59,

		e_LogMsg	= 4000,
		e_pgUP		= 92,
		e_pgDN		= 93,
	};
	
	ExecutionSUBSCR();
	~ExecutionSUBSCR();	

	bool InitData();		
	void Recalculate();	
private:

	void ExecutionSUBSCR::fnAddMessage( CString csaNewMsg );
	void ExecutionSUBSCR::fnDisplayMsg();
	
	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;
	
	int m_numofRows;
	int m_curTopRow;

	static const int m_niInputColumnCount	= enm_END;
	static const int m_niOutputColumnCount	= enm_END;

	BaseField *fld_OutputPath;


public:
	bool m_bIsTrheadCompleted;
	bool m_bIsThreadRunning;
	
	CString m_csMsg;
	int m_iTotalCount;
	int m_iOpenFileCount;
	
	CCriticalSection	m_MsgCriticalSec;

	void fnWriteMessage(CString csString )
	{
		CSingleLock clMsgSingleLock(&m_MsgCriticalSec);

		clMsgSingleLock.Lock();

		m_csMsg = csString.GetString();

		clMsgSingleLock.Unlock();
	}

	CString fnReadMessage( )
	{
		CSingleLock clMsgSingleLock(&m_MsgCriticalSec);

		clMsgSingleLock.Lock();

		CString csStr = m_csMsg.GetString();

		clMsgSingleLock.Unlock();

		return csStr;
	}	

	void ExecutionSUBSCR::fnGetNextSchedule( );
	void ExecutionSUBSCR::fnUpdateScheduleStatus(int iStatus );

	void ExecutionSUBSCR::fnRecalc1( );
	void ExecutionSUBSCR::fnCopyS3FiletoLocal( );
	void ExecutionSUBSCR::fnUncompressGZ( );
	void ExecutionSUBSCR::fnCheckFolderPath( );
	void ExecutionSUBSCR::ReadSUBSCRFileCSV( );
	
	void ExecutionSUBSCR::fnInsertDeliveryReport(CString csPrdouct, CString csDestAccount, CString csFileName, unsigned long &lFileSize  );
	void ExecutionSUBSCR::fnInsertErroReport(CString csPrdouct, CString csDestAccount, CString csFileName, CString csExchnage, CString &csErrorCmd );

	unsigned long  GetFileSize(std::string const &path) 
	{
		WIN32_FIND_DATA data;
		HANDLE h = FindFirstFile(path.c_str(), &data);
		if (h == INVALID_HANDLE_VALUE)
			return -1;

		FindClose(h);

		return (data.nFileSizeLow | data.nFileSizeHigh << 32);
	}

	ScheduleInfo m_clScheduleInfo;
	enmOperation m_enmOperation;

	SubkDetailV m_clSubkDetailV;

	void ExecutionSUBSCR::ParseLine( std::string &ssLine, int iRownumber );
	
};

BF_NAMESPACE_END


