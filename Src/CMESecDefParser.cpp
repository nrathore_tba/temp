
#include <stdafx.h> 
#include<fstream>
#include<string>
#include "CMESecDefParser.hpp"
//#include <shared/smtp.hpp>
#include <MailAutomation/MailAutomation.h>
#include "afxsock.h"

#define FTP_NOOFRETRY		1
#define FTP_RETRYDELAY		3
#define FTP_BUFFERSIZE		4096

#define MAX_ROWS 20
#define FILENAME1 "secdef.dat_YYYYMMDD"

#define USETHREDING

BF_NAMESPACE_USE

using namespace std;

IMPLEMENT_IMAGE(CMESecDefParser)

enum CDOption { CD_Root = 0, CD_OneLevelUp = 1, CD_AddToCurrent	= 2, CD_GoToSpecified = 3 };
enum Actions { E_None = 0, E_Connect = 1, E_AutoConnect = 2, E_ConnectDownload = 3, E_ConnectUpload = 4 };


void WatchTime(COleDateTime cTime)
{
	CString csTime;
	csTime.Format("%2d/%2d/%d %02d:%02d:%02d", cTime.GetMonth(),cTime.GetDay(),cTime.GetYear(),
		cTime.GetHour(),cTime.GetMinute(),cTime.GetSecond());
	csTime.MakeLower();
}

CMESecDefParser::CMESecDefParser():m_hdbc(SQL_NULL_HDBC)
,m_henv(SQL_NULL_HENV)
{	
	AfxOleInit();
	AfxSocketInit();
	m_nOperation = E_None;
}

CMESecDefParser::~CMESecDefParser()
{
}

bool CMESecDefParser::InitData()
{
	m_bThredRunning = false;
	m_iRetryCountDownload = 0;
	m_iOperation = 0;
	Sec_MsgLenghth = 0;
	GetField(e_Port)->SetValueString("21");
	//GetField(e_Message)->SetVisible(false);

	m_clDBConn.fnAddRequeryTable("CME_Security_Definition_Mst");
	
	//-------------------------------------------------------------------------
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString m_csCMEServer;
	RegString strEDIServerName(pKey, "CMEServer","No");
	m_csCMEServer = strEDIServerName.get_value().c_str();

	if( m_csCMEServer.CompareNoCase("YES") != 0 )
	{
		GetField(125)->SetVisible(false);

		GetField(e_EmailNotification)->SetValueInt(0);
		GetField(e_EmailNotification)->SetEnabled(false);

		GetField(e_DoManual)->SetValueInt(1); 
		GetField(e_DoManual)->SetEnabled(false);
	}
	
	//-------------------------------------------------------------------------
	CString m_csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	m_csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, m_csServerName, "CME_BCP","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}
	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	//-------------------------------------------------------------------------
	BF_Reg_Key_Ptr pKey1(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application"));

	CString csSmtpServer;
	RegString strSmtpServer(pKey1, "CMESMTPServer","beasttestnu4.vcmpartners.com");// for amazon -->"email-smtp.us-east-1.amazonaws.com"
	csSmtpServer = strSmtpServer.get_value().c_str();

	GetField(e_MailServer)->SetValueString(csSmtpServer);

	//-------------------------------------------------------------------------	
	//m_csConnect.Format("Driver={SQL Server Native Client 10.0};Server=beastdb;Database=TradeCapture;Uid=watchdog;Pwd=watchdog;");

	//m_csConnect.Format("DRIVER={SQL Server};SERVER=%s;Trusted_Connection=yes; DATABASE=CME_BCP;Uid=watchdog;Pwd=watchdog;",m_csServerName);//, csServer, csDatabaseName);
	m_csConnect.Format("Driver={SQL Server};Server=beastdb;Failover_Partner=beastdb;Database=CME_BCP;Uid=watchdog;Pwd=watchdog;");

	//m_csConnect.Format("Driver={SQL Server Native Client 10.0};Server=%s;Failover_Partner=beastdb2;Database=%s;Trusted_Connection=yes;",m_clDBConn.GetServerName(), m_clDBConn.GetDatabaseName());	
	//m_csConnect.Format("Provider=SQLOLEDB.1; Server=beastdb; Database=TradeCapture; Integrated Security=SSPI;");
	//m_csConnect.Format("Driver={SQL Server Native Client 10.0};Server=beastdb;Failover_Partner=beastdb;Database=TradeCapture;Uid=watchdog;Pwd=watchdog;");
	//m_csConnect.Format("Driver={SQL Server Native Client 10.0};Server=%s;Failover_Partner=beastdb2;Database=%s;Trusted_Connection=yes;",m_clDBConn.GetServerName(), m_clDBConn.GetDatabaseName());	

	
	//GetConnectionParams();
	
	//fnGetFileDownloded();
		
	if( GetField(e_DoManual)->GetValueInt() == 0 )
		m_nOperation = E_AutoConnect;
	else
		m_nOperation = E_None;

	GetField(e_Update)->SetWidth(0);

	RequestExternalUpdate();

	GetField(e_dntTouch)->SetVisible(false);
	//PrintRawMessage("InitData End");
	return true;
}

void CMESecDefParser::fnAddMessage(stMessages &clarFTPMessages)
{
	//COleDateTime dtCurrentTime(COleDateTime::GetCurrentTime());
	time_t tp = time(0);
	struct tm *ptm = gmtime ( &tp );
	time_t tp1 = mktime(ptm);
	
	COleDateTime dtCurrentTime = COleDateTime(tp1);
	dtCurrentTime.operator-(COleDateTimeSpan(0,ptm->tm_isdst,0,0));

	clarFTPMessages.csDateTime.Format("%02d/%02d/%d %02d:%02d:%02d", dtCurrentTime.GetMonth(), dtCurrentTime.GetDay(),
		dtCurrentTime.GetYear(),dtCurrentTime.GetHour(),dtCurrentTime.GetMinute(),dtCurrentTime.GetSecond());

	m_vMessages.push_back( clarFTPMessages );	

	int iCnt = m_vMessages.size();
	if( iCnt > 100 )
		m_vMessages.erase(m_vMessages.begin(), m_vMessages.begin() + (iCnt - 20));
}


//---------------------------------------------------------------------------------------------
//
DWORD WINAPI Run1(LPVOID lpData)
{
	CMESecDefParser *clpCMESecDefParser = (CMESecDefParser*)lpData;

	clpCMESecDefParser->fnDoThreadProcessing( );
	return 0;
}
///////////////////////////////

void CMESecDefParser::Recalculate()
{	

	//std::string strLine = "";
	//ExtractTokensToArray(strLine.c_str(),"\001");

	//PrintRawMessage("Recalc Start");
	if( m_bThredRunning )
	{
		CString csTemp;
		csTemp.Format("File Rows Read: %d Processed : %d Max_Len : %d",m_lTotalRead,m_lTotalProcess,Sec_MsgLenghth);
		GetField(e_Message)->SetValueString(csTemp);
		GetField(e_Message)->SetBlinkInterval(-1);
		//return;
	}
	else
	{
		GetField(e_Message)->SetBlinkInterval(0);
		GetField(e_Message)->SetValueString("Message");
	}
	
	
	//GetField(e_Message)->SetVisible(false);

	bool bConnectionFailed = false;
	bool bCME_Security_Definition_Mst = false;
	MQDBNHandler *pMsmq = m_clDBConn.GetMSMQ();
	
	if( pMsmq )
	{
		while( pMsmq->HasEvent() )
		{
			const DbEventInfo * event = pMsmq->GetNextEvent();

			if( event->m_table.CompareNoCase("CME_Security_Definition_Mst") == 0 )
			{
				bCME_Security_Definition_Mst = true;
			}	

		}
	}

	if(bCME_Security_Definition_Mst)
	{
		stMessages stMessage1;				
		stMessage1.csAction = "Requery";
		stMessage1.csStatus = "CME_Security_Definition_Mst table";
		stMessage1.nError = 0;
		//fnGetFileDownloded();
		fnAddMessage(stMessage1);
	}
	/////////

#ifndef USETHREDING
	if( m_iOperation ^ 0 )
	{
		bool bProcess = (m_iOperation==1);
		m_iOperation = 0;


		m_lEParsing = 0;
		m_lEDecompress = 0;
		m_lTotalReadDisplay = 0;
		m_lTotalRead = 0;	
		m_lTotalProcess = 0;	
		

		BFDate clBFDate = GetField(e_ManualParsingDate)->GetValueDate();
		CString csFileName =  GetFileName(clBFDate,1);
		SubmitSECDEFBCP(csFileName);

		CString csTemp;
		stMessages stMessageThrd;	
		stMessageThrd.csAction = "Thread";
		
		csTemp.Format("Error in parsing = %d",m_lEParsing);stMessageThrd.csStatus = csTemp; fnAddMessage(stMessageThrd);
		csTemp.Format("Total Row Read = %d",m_lTotalRead);stMessageThrd.csStatus = csTemp; fnAddMessage(stMessageThrd);
		csTemp.Format("Total Row Processed successfull = %d",stMessageThrd.csStatus = csTemp; fnAddMessage(stMessageThrd);

		GetField(e_Message)->SetBlinkInterval(0);

		RequestExternalUpdate();
	}
#endif
	//------------------------------
	//------------------------------
	
	if(IsFieldNew(e_ManualParser))
	{
		BFDate clBFDate = GetField(e_ManualParsingDate)->GetValueDate();
		CString csFileName =  GetFileName(clBFDate,1);

		CString csTemp;

		/*csTemp.Format("Decompressing File %s Started.....", csFileName);
		stMessages stMessage;	
		stMessage.csAction = "SecDef Parsing";
		stMessage.nError = 0;
		stMessage.csStatus = csTemp;
		fnAddMessage(stMessage);*/

		m_iOperation = 1;

		GetField(e_Message)->SetBlinkInterval(-1);


		//BFDate clBFDate = GetField(e_ManualParsingDate)->GetValueDate();
		//CString csFileName =  GetFileName(clBFDate,1);
		//SubmitSECDEFBCP(csFileName);
	}
	

	#ifdef USETHREDING
	if( m_iOperation ^ 0 )
	{
		m_bThredRunning = true;

		CreateThread((LPSECURITY_ATTRIBUTES)NULL,
					0,
					(LPTHREAD_START_ROUTINE)(Run1),
					(LPVOID)this,
					0, 
					0);		

	}
	#else 
	
		if( m_iOperation ^ 0 )
			StartRecalcTimer(1, false);
	#endif

	DisplayMessages();

}

void CMESecDefParser::fnDoThreadProcessing( )
{
	bool bProcess = (m_iOperation==1);
	m_iOperation = 0;


	m_lEParsing = 0;
	m_lEDecompress = 0;
	m_lTotalReadDisplay = 0;
	m_lTotalRead = 0;	
	m_lTotalProcess = 0;	
	Sec_MsgLenghth = 0;

	BFDate clBFDate = GetField(e_ManualParsingDate)->GetValueDate();
	CString csFileName =  GetFileName(clBFDate,1);
	SubmitSECDEFBCP(csFileName);

	CString csTemp;
	stMessages stMessageThrd;	
	stMessageThrd.csAction = "SecDef Process Over";
	stMessageThrd.nError = 0;	
	//csTemp.Format("Error in parsing = %d",m_lEParsing);stMessageThrd.csStatus = csTemp; fnAddMessage(stMessageThrd);
	csTemp.Format("Total Row Read = %d",m_lTotalRead);stMessageThrd.csStatus = csTemp; fnAddMessage(stMessageThrd);
	csTemp.Format("Total Row Processed successfull = %d",m_lTotalProcess);stMessageThrd.csStatus = csTemp; fnAddMessage(stMessageThrd);
	csTemp.Format("Largest SecDef MsgLength = %d",Sec_MsgLenghth);stMessageThrd.csStatus = csTemp; fnAddMessage(stMessageThrd);

	GetField(e_Message)->SetBlinkInterval(0);

	m_bThredRunning = false;
	RequestExternalUpdate();
}

void CMESecDefParser::DisplayMessages()
{
	const int niDataRowCount = m_vMessages.size();

	stMessages stMessage;	
	int iGridRow = 0;

	for(int iDataRow = 0; iDataRow < niDataRowCount; iGridRow++, iDataRow++)		
	{
		if( iGridRow >= MAX_ROWS )
			break;

		stMessage = m_vMessages[niDataRowCount-iDataRow-1];

		GetField(e_Message_DateTime + iGridRow)->SetValueString(stMessage.csDateTime);
		GetField(e_Message_Action + iGridRow)->SetValueString(stMessage.csAction);
		GetField(e_Message_Status + iGridRow)->SetValueString(stMessage.csStatus);

		GetField(e_Message_Status + iGridRow)->SetToolTipText(stMessage.csStatus);

		if(stMessage.nError)
		{
			GetField(e_Message_DateTime + iGridRow)->SetForeColor(ColorManager::eAttribStaleBack);
			GetField(e_Message_Action + iGridRow)->SetForeColor(ColorManager::eAttribStaleBack);
			GetField(e_Message_Status + iGridRow)->SetForeColor(ColorManager::eAttribStaleBack);
		}
		else
		{
			GetField(e_Message_DateTime + iGridRow)->SetForeColor(ColorManager::eAttribGreenOnBlackFore);
			GetField(e_Message_Action + iGridRow)->SetForeColor(ColorManager::eAttribGreenOnBlackFore);
			GetField(e_Message_Status + iGridRow)->SetForeColor(ColorManager::eAttribGreenOnBlackFore);
		}
	}

	while(iGridRow < MAX_ROWS)
	{
		GetField(e_Message_DateTime + iGridRow)->SetValueString("");
		GetField(e_Message_Action + iGridRow)->SetValueString("");
		GetField(e_Message_Status + iGridRow)->SetValueString("");

		iGridRow++;
	}
}

void CMESecDefParser::SendMailOnError(stMessages &stMessage, CString csFileName, bool bExternalAlso /*= false*/)
{
	if( GetField(e_EmailNotification)->GetValueInt() == 0 )
		return;

#ifdef _DEBUG
	return;
#endif
	
	PrintRawMessage("Sendig error mail");

	CString csSmtp = GetField(e_MailServer)->GetValueString();

	PrintRawMessage("Sendig error mail at " + csSmtp);
	//-----------------------------------------------------
	CSmtp mail;

	if( csSmtp.Find("vcmpartners") != -1 )
	{
		// Vcmserver
		//mail.SetSMTPServer("VCMSMTP2.vcmpartners.com", 25, false);		// expected
		mail.SetSMTPServer(csSmtp, 25, false);		
		mail.SetSecurityType((SMTP_SECURITY_TYPE)NO_SECURITY);
	}
	else
	{
		//Amazon server
		//mail.SetSMTPServer("email-smtp.us-east-1.amazonaws.com", 587, true); // expected
		mail.SetSMTPServer(csSmtp, 587, true);
		mail.SetSecurityType((SMTP_SECURITY_TYPE)USE_TLS);

		mail.SetLogin("AKIAI3L6YZS4V6BVF7JQ");
		mail.SetPassword("AiXvk+i3LTkPxFdkB2iF181DcpYMbco9RWFSPNQGUnjO");
	}

	mail.SetSenderMail("sysadmin@thebeastapps.com");		
	
	// To
	CString sSendTo = GetField(e_InternalEmail)->GetValueString();
	CString csToExtMail = GetField(e_ExternalEmail)->GetValueString();

	if( bExternalAlso && !csToExtMail.IsEmpty())
		sSendTo = csToExtMail + ";"+sSendTo;

	sSendTo += ";";

	while(!sSendTo.IsEmpty())
	{
		CString tmpSendTo = sSendTo.Left(sSendTo.Find(";"));			
		sSendTo.Delete(0,sSendTo.Find(";")+1);		
		tmpSendTo.TrimLeft();
		tmpSendTo.TrimRight();
		mail.AddRecipient(tmpSendTo);
	}

	//cc		
	CString csCCMail = "vcmops@thebeastapps.com";
	mail.AddCCRecipient(csCCMail);


	char hostname[1024];
	hostname[1023] = '\0';
	gethostname(hostname, 1023);

	//
	CString sBody="This is body";
	

	CString strSubject, strBody("");
	strSubject.Format("Trumid Exchange Data - Error in %s", stMessage.csAction);

	strBody += "Hi,\n\nAn error occurred during one of the Trumid FTP operation as below.\n\n";
	strBody += "Server - ";
	strBody += CString(hostname) +"\n";
	strBody += "Action - ";
	strBody += stMessage.csAction;

	if(stMessage.csAction == "FTP Connection")
	{
		strBody += "\nFTP Server - ";
		strBody += GetField(e_Server)->GetValueString();
	}
	
	if(stMessage.csAction == "File Download")
	{
		strBody += "\nFile to Download - ";
		strBody += csFileName;
		strBody += "\nDownload From - ";
		strBody += GetField(e_File_Path)->GetValueString();
		strBody += "\nDownload To - ";
		strBody += GetField(e_Local_Dn_Path)->GetValueString();
	}
	
	strBody += "\nError Description - ";
	strBody += stMessage.csStatus;
	strBody += "\n\nThe Beast Apps\n\n\n";
	
	mail.AddBody(strBody);
	mail.SetSubject(strSubject);


	PrintRawMessage("About to Sendig error mail");
	try
	{
		mail.Send();//this);
	}
	catch(ECSmtp e)
	{
		stMessages stMessage;				
		stMessage.csAction = "Send Mail";
		stMessage.csStatus = e.GetErrorText();
		stMessage.nError = 1;

		fnAddMessage(stMessage);
	}
	catch(...)
	{
		int id = GetLastError();
		stMessages stMessage;				
		stMessage.csAction = "Send Mail";
		stMessage.csStatus.Format("error ID %d", id);
		stMessage.nError = 1;

		fnAddMessage(stMessage);
	}

	//.........................................................................................
}

CString CMESecDefParser::GetFileName(BFDate &clBFDate,int index)
{
	CString csFileName = "";
	CString csTempDate;
	if(index == 1)
	{
		csFileName = FILENAME1;
		csTempDate.Format("%04d%02d%02d", clBFDate.GetYear(), clBFDate.GetMonth()+1, clBFDate.GetDate());
	}
	
	csFileName.Replace("YYYYMMDD", csTempDate);
	return csFileName;
}


//----------------------------------------------------------------------------------------------------//
//----------------------------------------------------------------------------------------------------//


//----------------------------------------------------------------------------------------------------//
//----------------------------------------------------------------------------------------------------//

RETCODE CMESecDefParser::ConnectBCP()
{
	RETCODE retcode;
	// Allocate the ODBC environment and save handle.
	retcode = SQLAllocHandle (SQL_HANDLE_ENV, NULL, &m_henv);
	
	// Notify ODBC that this is an ODBC 3.0 app.
	retcode = SQLSetEnvAttr(m_henv, SQL_ATTR_ODBC_VERSION,(SQLPOINTER) SQL_OV_ODBC3,SQL_IS_INTEGER);
	
	// Allocate ODBC connection handle, set bulk copy mode, and 
	// then connect.
	m_hdbc = SQL_NULL_HDBC;
	retcode = SQLAllocHandle(SQL_HANDLE_DBC, m_henv, &m_hdbc);
	
	// Enable bulk copy prior to connecting on allocated hdbc.
	SQLSetConnectAttr(m_hdbc, SQL_COPT_SS_BCP, (SQLPOINTER) SQL_BCP_ON,SQL_IS_INTEGER);
	
	SQLRETURN sRet = SQLDriverConnect(m_hdbc, NULL, (SQLTCHAR*)(LPCSTR)m_csConnect, SQL_NTS, 
		m_ConnStrOut, MAX_CONN_OUT, &m_cbConnStrOut, SQL_DRIVER_COMPLETE);
	
	return sRet;
}
//
RETCODE CMESecDefParser::InitSECDEFBCP()
{
	RETCODE retcode;
	
	// Initialize bulk copy. V_Trumid_AllBatchUpdateBondView
	if( (retcode = bcp_init(m_hdbc, "v_CME_Security_Definition_StgBcp", NULL, NULL, DB_IN)) == FAIL)
	{
		ExtractODBCerror();
		return retcode;
	}
	
	if( (retcode = bcp_control(m_hdbc, BCPHINTS, (void*)"FIRE_TRIGGERS")) == FAIL)
		return retcode;
	retcode = bcp_bind(m_hdbc, (LPCBYTE) &m_CME_SecDefView.Group_Code,0, SQL_VARLEN_DATA, (LPCBYTE)"",1,SQLCHARACTER,1);
	retcode = bcp_bind(m_hdbc, (LPCBYTE) &m_CME_SecDefView.Exch_Code,0, SQL_VARLEN_DATA, (LPCBYTE)"",1,SQLCHARACTER,2);
	retcode = bcp_bind(m_hdbc, (LPCBYTE) &m_CME_SecDefView.Prod_Code,0, SQL_VARLEN_DATA, (LPCBYTE)"",1,SQLCHARACTER,3);
	retcode = bcp_bind(m_hdbc, (LPCBYTE) &m_CME_SecDefView.Prod_desc,0, SQL_VARLEN_DATA, (LPCBYTE)"",1,SQLCHARACTER,4);
	//retcode = bcp_bind(m_hdbc, (LPCBYTE) &m_CME_SecDefView.Sec_ID,0, SQL_VARLEN_DATA, (LPCBYTE)"",1,SQLCHARACTER,5);
	retcode = bcp_bind(m_hdbc, (LPCBYTE) &m_CME_SecDefView.Sec_ID	,0, sizeof(DBINT),		NULL,	0, SQLINT4,5);
	//retcode = bcp_bind(m_hdbc, (LPCBYTE) &m_CME_SecDefView.Sec_ID,0, SQL_VARLEN_DATA, (LPCBYTE)"",1,SQLINT1,5);
	retcode = bcp_bind(m_hdbc, (LPCBYTE) &m_CME_SecDefView.FOI_Type,0, SQL_VARLEN_DATA, (LPCBYTE)"",1,SQLCHARACTER,6);
	retcode = bcp_bind(m_hdbc, (LPCBYTE) &m_CME_SecDefView.Spread_In,0, SQL_VARLEN_DATA, (LPCBYTE)"",1,SQLINTN,7);
	retcode = bcp_bind(m_hdbc, (LPCBYTE) &m_CME_SecDefView.FileName,0, SQL_VARLEN_DATA, (LPCBYTE)"",1,SQLCHARACTER,8);
	return retcode;
}
////
//
////

bool CMESecDefParser::ExtractODBCerror()
{
	SQLSMALLINT i = 0;
	SQLINTEGER native;
	SQLCHAR state[ 7 ];
	SQLCHAR text[256];
	SQLSMALLINT len;
	SQLRETURN ret;
	
	bool bRet = false;
	do
	{
		ret = SQLGetDiagRec(SQL_HANDLE_DBC, m_hdbc, ++i, state, &native, text, sizeof(text), &len );
		if( SQL_SUCCEEDED(ret) )
		{
			if( !bRet )
			{
				for( int j = 0; j < sizeof(BCP::sm_FatalSQLStates) / sizeof(char *); ++j )
					if( !::stricmp((char*)&state[0], BCP::sm_FatalSQLStates[j]) )
					{
						bRet = true;
						break;
					}
			}
			
			BFReport(BFE_SERVICE_ERROR,"ODBCerror: " << (char*)&state[0]  <<" :" << i << " :" <<  native << " :" << (char*)&text[0] << "\n");
			SetErrorMessage(ErrorSeverity::e_Error, (char*)&text[0]);
			PrintRawMessage((char*)&text[0]);
		}
	}while( ret == SQL_SUCCESS );
	
	return bRet;
}
////
void CMESecDefParser::DisconnectBCP()
{
	if ( m_hdbc == SQL_NULL_HDBC )
		return;
	
	// Free the ODBC connections to the database.
	SQLDisconnect(m_hdbc);
	
	SQLFreeConnect(m_hdbc);
	m_hdbc = SQL_NULL_HDBC;
	
	SQLFreeEnv(m_henv);
	m_henv = SQL_NULL_HENV;
}
//
void CMESecDefParser::SubmitSECDEFBCP(CString sFileName)
{
	CString csTempDate,strcmd,csTemp,csProcessStart;
	CString csLocalPath = GetField(e_File_Path)->GetValueString();
	CString csCopyPath = GetField(e_Local_Dn_Path)->GetValueString();
	stMessages stMessageStart;	
	try
	{
		int iValue(-1);
		
		BFDate clBFDate = GetField(e_ManualParsingDate)->GetValueDate();
		csTempDate.Format("%04d%02d%02d",clBFDate.GetYear(), clBFDate.GetMonth()+1, clBFDate.GetDate());
		
		csTemp.Format("Decompressing File secdef.dat_%s.lzo Started.....", csTempDate);
		stMessageStart.csAction = "SecDef File Decompress";
		stMessageStart.nError = 0;
		stMessageStart.csStatus = csTemp;
		fnAddMessage(stMessageStart);
		csProcessStart = stMessageStart.csDateTime;
		

		//strcmd.Format("D: && CD \\CME\\ && lzop.exe -1 -d \\\\powervault3\\CME_s3\\historical\\secdef\\secdef.dat_%s.lzo",csTempDate); 
		strcmd.Format("%s\\lzop.exe -1 -d %s\\secdef.dat_%s.lzo",csLocalPath,csCopyPath,csTempDate);

		iValue = system(strcmd);

		if(iValue == 0)
		{
			csTemp.Format("%s Failed....",strcmd);
			stMessageStart.csStatus = csTemp;
			fnAddMessage(stMessageStart);
		}
		else
		{
			csTemp.Format("Decompressing File secdef.dat_%s.lzo Sucessful....", csTempDate);
			stMessageStart.csStatus = csTemp;
			fnAddMessage(stMessageStart);
		}
	
		
	}
	catch(...)
	{
		PrintRawMessage("Exception in System Command of Decompressing LZO");
	
		stMessages stMessageCMD;
		stMessageCMD.csStatus.Format("Exception in System Command of Decompressing LZO :: %s",sFileName);
		stMessageCMD.csAction = "Database Truncate";
		stMessageCMD.nError = 0;
		fnAddMessage(stMessageCMD);
		SetErrorMessage(ErrorSeverity::e_Error,stMessageCMD.csStatus);
		SendMailOnError(stMessageCMD,sFileName);
		return;
	}

	///Truncate Stagig Tables/////////
	CString sql;
	sql.Format("proc_beast_truncate_CME_Security_Definition_StgBcp");
	
	_RecordsetPtr set11;
	CString Error = "";
	DB_TRY
	{
		HRESULT hr = m_clpdbInt->GetRecordset(&set11, (LPCSTR)sql,Error);
		if (FAILED(hr))
		{
			_com_error err(hr);
			LPCTSTR errMsg = err.ErrorMessage();
						
			stMessages stMessage11;
			stMessage11.csStatus.Format("proc_beast_truncate_CME_Security_Definition_StgBcp Failed :: %s ::SQL Error :: %s",CString(errMsg),Error);
			stMessage11.csAction = "Database Truncate";
			stMessage11.nError = 1;
			fnAddMessage(stMessage11);
			SetErrorMessage(ErrorSeverity::e_Error,sql);
			SendMailOnError(stMessage11,sFileName);
			PrintRawMessage("Database Truncate - proc_beast_truncate_CME_Security_Definition_StgBcp Failed");
			return;
		}
		set11->Close();
	}
	DB_CATCH("proc_beast_truncate_CME_Security_Definition_StgBcp");
	///////////////////////////////////////////////////
	
	DisconnectBCP();
	RETCODE ret1 = ConnectBCP();
	RETCODE ret2 = InitSECDEFBCP();
	
	

	sFileName.Format("secdef.dat_%s",csTempDate);

	CString FileName;
	//FileName.Format("\\\\purecmbkup\\EDI_Trumid\\%s", sFileName); 
	FileName.Format("%s\\%s",csCopyPath,sFileName); 
		
	int m_lLinesRead = 0;
	CStringArray OutputArraytemp;
			

	FILE *m_pFile;
	FILE *m_pSplitFile[50]; 

	if(fopen_s(&m_pFile, FileName ,"r"))
		return ;

	char chLine[1024];
	std::string strLine("");
	std::string strGEP_Code("");
	int m_lRecordRead = 0;
	m_lTotalRead = 0;
	while(fgets(chLine, 1024, m_pFile))
	{
		if(strchr(chLine, '\n'))
		{
			m_lTotalRead++;
			m_lRecordRead++;
			strLine += chLine;
			
			if(Sec_MsgLenghth < strLine.length())
			{
				Sec_MsgLenghth =  strLine.length();
			}
			//////////////////////
			OutputArraytemp.RemoveAll();
			ExtractTokensToArray(strLine.c_str(),"\001");
			strcpy(m_CME_SecDefView.FileName ,sFileName);	
			RETCODE  ret = bcp_sendrow(m_hdbc);	

			//Commented due to info found in TAG 461
			/*strGEP_Code="";
			strGEP_Code.append(m_CME_SecDefView.Group_Code);
			strGEP_Code.append(m_CME_SecDefView.Exch_Code);
			strGEP_Code.append(m_CME_SecDefView.Prod_Code);
			if(FindSecDefType(strGEP_Code))
			{
				strcpy(m_CME_SecDefView.FileName ,sFileName);	
				RETCODE  ret = bcp_sendrow(m_hdbc);	
			}
			else
			{
				stMessages stMessage;
				stMessage.csStatus.Format(" %s : Security Def Type Not Found for Grp:'%s', Exc:'%s', Prd:'%s'",sFileName,m_CME_SecDefView.Group_Code,m_CME_SecDefView.Exch_Code,m_CME_SecDefView.Prod_Code);
				stMessage.csAction = "Sec_DefView Submit";
				stMessage.nError = 0;
				fnAddMessage(stMessage);
			}*/
			//////////////////////
			strLine = "";
		}
		else
		{
			strLine += chLine;
		}

		if(m_lRecordRead == 5000)
		{
			if( bcp_batch(m_hdbc) == -1)
			{
				ExtractODBCerror();
				GetField(e_Message)->SetValueString("Sec_Def File could not be submit");
				GetField(e_Message)->SetVisible(true);
		
				stMessages stMessage;
				stMessage.csStatus.Format(" %s Sec_Def File could not be submit",sFileName);
				stMessage.csAction = "Sec_DefView Submit";
				stMessage.nError = 1;
				fnAddMessage(stMessage);
			}
			m_lTotalProcess = m_lTotalRead;
			m_lRecordRead = 0;
			RequestExternalUpdate();
		}
	}

	fclose(m_pFile);

		
	if( bcp_batch(m_hdbc) == -1)
	{
		ExtractODBCerror();
		GetField(e_Message)->SetValueString("Sec_Def File could not be submit");
		GetField(e_Message)->SetVisible(true);
		
		stMessages stMessage;
		stMessage.csStatus.Format(" %s Sec_Def File could not be submit",sFileName);
		stMessage.csAction = "Sec_DefView Submit";
		stMessage.nError = 1;
		fnAddMessage(stMessage);
	}
	DisconnectBCP();
	
	m_lTotalProcess = m_lTotalRead;
	m_lRecordRead = 0;
	RequestExternalUpdate();

	///Updating Master Tables/////////
	sql.Format("proc_beast_submit_CME_Security_Definition_Mst");
	
	_RecordsetPtr set12;
	Error = "";
	DB_TRY
	{
		HRESULT hr = m_clpdbInt->GetRecordset(&set12, (LPCSTR)sql,Error);
		if (FAILED(hr))
		{
			_com_error err(hr);
			LPCTSTR errMsg = err.ErrorMessage();
						
			stMessages stMessage12;
			stMessage12.csStatus.Format("proc_beast_submit_CME_Security_Definition_Mst Failed :: %s ::SQL Error :: %s",CString(errMsg),Error);
			stMessage12.csAction = "Database Update";
			stMessage12.nError = 1;
			fnAddMessage(stMessage12);
			SetErrorMessage(ErrorSeverity::e_Error,sql);
			SendMailOnError(stMessage12,sFileName);
			PrintRawMessage("Database Update - proc_beast_submit_CME_Security_Definition_Mst Failed");
			return;
		}
		set12->Close();
	}
	DB_CATCH("proc_beast_submit_CME_Security_Definition_Mst");
	///////////////////////////////////////////////////

	stMessages stMessage;
	stMessage.csStatus.Format(" %s : Processed..",sFileName);
	stMessage.csAction = "Sec_DefView Submit";
	stMessage.nError = 0;
	fnAddMessage(stMessage);

	SubmitFileName(sFileName,csProcessStart,stMessage.csDateTime);
}

void CMESecDefParser::SubmitFileName(CString csFileName, CString csProcessStart, CString csProcessEnd)
{
	int UserId = GetUpdateUserID();
	
	CString sql;
	sql.Format("EXEC Proc_Beast_Submit_SecDef_FileDetails '%s', '%d', '%s', '%s', 1", csFileName,UserId, csProcessStart,csProcessEnd);
	_RecordsetPtr set;

	DB_TRY
	{
		if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql)))
		{
			SetErrorMessage(ErrorSeverity::e_Error, sql);
			PrintRawMessage(sql);
		}

		
	}
	DB_CATCH(sql);	
}

bool CMESecDefParser::GetFiledetails(CString csFileName)
{
	CString sql;
	sql.Format("EXEC proc_beast_get_CME_SecDefFileDetails '%s'", csFileName);
	_RecordsetPtr set;
	bool FileDone = false;
	DB_TRY
	{
		if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql)))
		{
			SetErrorMessage(ErrorSeverity::e_Error, sql);
			PrintRawMessage(sql);
		}

		while(VARIANT_FALSE == set->adoEOF)
		{
			SETLONG(FileDone,set->Fields->Item[_variant_t("IsProcessed")]->Value);
			FileDone = true;
		}
	}
	DB_CATCH(sql);	
}

void CMESecDefParser::ExtractTokensToArray(string s, string Delimiters)
{
   int     Hit;
   int     EarliestHit;
   int     DelimiterIndex;
   string  sExtract;
   string  sSubExtract;
   BOOL    MoreTokens = TRUE;
   BOOL    GotToken = FALSE;
   TCHAR   CurrDelimiter;
   string  strTmp;

	while (MoreTokens)
	{
      GotToken = FALSE;
	  EarliestHit = s.length();

      // Trawl the string looking for the leftmost (earliest) hit in
      // the list of valid separators.
      for (DelimiterIndex = 0; 
           DelimiterIndex < Delimiters.length();
           DelimiterIndex++)
	  {
         CurrDelimiter = Delimiters [DelimiterIndex];
		 Hit = s.find(CurrDelimiter);
         if (Hit != -1)
         {
            if (Hit < EarliestHit)
            {
               EarliestHit = Hit;
            }
            GotToken = TRUE;
         }
      }

		if (GotToken)
		{
			sExtract = s.substr(0,EarliestHit);
			s = s.substr(EarliestHit+1,s.length());

			sSubExtract = sExtract.substr(0,sExtract.find("="));

			int Pos = atoi(sSubExtract.c_str()); 

			switch(Pos)
			{
				case 207:
					strcpy(m_CME_SecDefView.Exch_Code,sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str());
					break;
				case 1151:
					strcpy(m_CME_SecDefView.Prod_Code,sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str());
					break;
				case 48:
					{
					//strcpy(m_CME_SecDefView.Sec_ID,sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str());
						m_CME_SecDefView.Sec_ID = atoi(sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str());
					break;
					}
				case 107:
					strcpy(m_CME_SecDefView.Prod_desc,sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str());
					break;
				case 55:
					strcpy(m_CME_SecDefView.Group_Code,sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str());
					break;
				case 461:
					{
						sExtract = sExtract.substr((sExtract.find("=")+1),2).c_str();
						if(sExtract.find("F") == 0)
						{
							strcpy(m_CME_SecDefView.FOI_Type,"FUT");
							if(sExtract == "FM")
							{
								m_CME_SecDefView.Spread_In = 1;
							}
							else
							{
								m_CME_SecDefView.Spread_In = 0;
							}

						}
						else if(sExtract.find("O") == 0)
						{
							strcpy(m_CME_SecDefView.FOI_Type,"OPT");
							if(sExtract == "OM")
							{
								m_CME_SecDefView.Spread_In = 1;
							}
							else
							{
								m_CME_SecDefView.Spread_In = 0;
							}
						}
					}
					//strcpy(m_CME_SecDefView.FOI_Type,sExtract.substr((sExtract.find("=")+1),sExtract.length()).c_str());
					break;
			}
		}
		else
		{
			MoreTokens = FALSE;
		}
	}
}

bool CMESecDefParser::FindSecDefType(std::string GEP_Code)
{
	bool found = false;
	std::map<std::string, CME_SecDefType>::const_iterator it = SecDefTypeMap.find(GEP_Code);
	std::map<std::string, CME_SecDefType>::const_iterator it_end = SecDefTypeMap.end();

	if( it != it_end )
	{
		strcpy(m_CME_SecDefView.FOI_Type , (*it).second.FOI.c_str());
		m_CME_SecDefView.Spread_In = (*it).second.Sprd_In;
		
		found = true;
	}
	else
	{
		CME_SecDefType SDef_Type;
		CString sql;
		sql.Format("EXEC proc_beast_get_CME_Products_Mst '%s','%s','%s'",m_CME_SecDefView.Group_Code,m_CME_SecDefView.Exch_Code,m_CME_SecDefView.Prod_Code);

		_RecordsetPtr set;
		if( FAILED(m_clpdbInt->GetRecordset(&set, sql ,false)))
		{
			SetErrorMessage(ErrorSeverity::e_Error, sql);
			return false;
		}

		while( VARIANT_FALSE == set->adoEOF )
		{
			SDef_Type.GEP_Code.append(m_CME_SecDefView.Group_Code);
			SDef_Type.GEP_Code.append(m_CME_SecDefView.Exch_Code);
			SDef_Type.GEP_Code.append(m_CME_SecDefView.Prod_Code);

			SETSTR(SDef_Type.FOI ,	set->Fields->Item[_variant_t("FOI")]->Value);
			SETLONG(SDef_Type.Sprd_In ,	set->Fields->Item[_variant_t("SPREAD_IND")]->Value);

			strcpy(m_CME_SecDefView.FOI_Type , SDef_Type.FOI.c_str());
			m_CME_SecDefView.Spread_In = SDef_Type.Sprd_In;

			SecDefTypeMap.insert(std::map<std::string, CME_SecDefType>::value_type(SDef_Type.GEP_Code,SDef_Type));
			set->MoveNext();

			found = true;
		}
		set->Close();
	}

	return found;
}

void CMESecDefParser::GetFileProcessSchedule()
{

}