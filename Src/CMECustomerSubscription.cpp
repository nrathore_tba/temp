
#include <stdafx.h> 
#include "CMECustomerSubscription.hpp"
#include <MailAutomation/MailAutomation.h>
#include "afxsock.h"

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(CMECustomerSubscription)

CMECustomerSubscription::CMECustomerSubscription()
{
	m_curTopRow = 0;
	m_numofRows = 20;
}

CMECustomerSubscription::~CMECustomerSubscription()
{
}

bool CMECustomerSubscription::InitData()
{
	cout<<"Start InitData()"<<endl;
	InitDatabase();
	
	cout<<"End InitData"<<endl;
	return true;
}
void CMECustomerSubscription::Recalculate()
{
	cout<<"Start Recalculate()"<<endl;
	if(IsFieldNew(e_Subscription))
	{
		GetSubscribercustomerList();
	}
	if(IsFieldNew(e_Search))
	{
		SearchEmail();
	}
	ManageUpDown();
	DisplaySubscribeCustomer();
	cout<<"End Recalculate"<<endl;
}
void CMECustomerSubscription::InitDatabase()
{
	cout<<"Start InitDatabase()"<<endl;
	//Database connection...
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize database server!");
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	cout<<"End InitDatabase()"<<endl;
}
void CMECustomerSubscription::ManageUpDown()
{
	int nTotalRecord = m_vecSubscribeCustumerList.size();
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_Up, e_Dn, nTotalRecord, m_numofRows, m_curTopRow);
}
void CMECustomerSubscription::GetSubscribercustomerList()
{
	cout<<"Start GetSubscribercustomerList()"<<endl;
	int nRecordCount = 0;
	m_vecSubscribeCustumerList.clear();

	CString sql;
	sql.Format(_T("SELECT CustomerID, CUST_NAME, FirstName, LastName, UserName, CONTACT_EMAIL, S3AccessKey, S3SecretKey, FTPUrl, FTPUserName, FTPPassword, DeliveryPref, CustomerAcctNum, CMEAcctNumber, HDLA from dbo.CME_Customer"));

	_RecordsetPtr set;
	DB_TRY
	{
		if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql)))
		{
			SetErrorMessage(ErrorSeverity::e_Error," CME_Customer");
			ASSERT(0);
			return;
		}
		
		SubscribeCustumer temp;

		while(VARIANT_FALSE == set->adoEOF)
		{
			SETLONG(temp.csCustomerID,   set->Fields->Item[_variant_t("CustomerID")]->Value);
			SETSTR(temp.csCustomerName,  set->Fields->Item[_variant_t("CUST_NAME")]->Value);
			SETSTR(temp.csFirstName,  set->Fields->Item[_variant_t("FirstName")]->Value);
			SETSTR(temp.csLastName,  set->Fields->Item[_variant_t("LastName")]->Value);
			SETSTR(temp.csUserName,  set->Fields->Item[_variant_t("UserName")]->Value);
			SETSTR(temp.csEmail,  set->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);
			SETSTR(temp.csS3AccessKey,  set->Fields->Item[_variant_t("S3AccessKey")]->Value);
			SETSTR(temp.csS3SecretKey,  set->Fields->Item[_variant_t("S3SecretKey")]->Value);
			SETSTR(temp.csFTPUrl,  set->Fields->Item[_variant_t("FTPUrl")]->Value);
			SETSTR(temp.csFTPUserName,  set->Fields->Item[_variant_t("FTPUserName")]->Value);
			SETSTR(temp.csFTPPassword,  set->Fields->Item[_variant_t("FTPPassword")]->Value);
			SETSTR(temp.csDeliveryPref,  set->Fields->Item[_variant_t("DeliveryPref")]->Value);
			SETSTR(temp.csCustomerAcctNum,  set->Fields->Item[_variant_t("CustomerAcctNum")]->Value);
			SETSTR(temp.csCMEAcctNumber,  set->Fields->Item[_variant_t("CMEAcctNumber")]->Value);
			SETSTR(temp.csHDLA,  set->Fields->Item[_variant_t("HDLA")]->Value);
			
			m_vecSubscribeCustumerList.push_back(temp);
			set->MoveNext();
		}
		set->Close();
	}
	DB_CATCH("VolmaxBonds_OrderCurve");
}
void CMECustomerSubscription::SearchEmail()
{
	CString csEmail = GetField(e_Mail)->GetValueString();

	m_vecSubscribeCustumerList.clear();

	CString sql;
	sql.Format(_T("SELECT CustomerID, CUST_NAME, FirstName, LastName, UserName, CONTACT_EMAIL, S3AccessKey, S3SecretKey, FTPUrl, FTPUserName, FTPPassword, DeliveryPref, CustomerAcctNum, CMEAcctNumber, HDLA from dbo.CME_Customer where CONTACT_EMAIL = '%s'"), csEmail);

	_RecordsetPtr set;
	DB_TRY
	{
		if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql)))
		{
			SetErrorMessage(ErrorSeverity::e_Error," CME_Customer");
			ASSERT(0);
			return;
		}
		
		SubscribeCustumer temp;

		while(VARIANT_FALSE == set->adoEOF)
		{
			SETLONG(temp.csCustomerID,   set->Fields->Item[_variant_t("CustomerID")]->Value);
			SETSTR(temp.csCustomerName,  set->Fields->Item[_variant_t("CUST_NAME")]->Value);
			SETSTR(temp.csFirstName,  set->Fields->Item[_variant_t("FirstName")]->Value);
			SETSTR(temp.csLastName,  set->Fields->Item[_variant_t("LastName")]->Value);
			SETSTR(temp.csUserName,  set->Fields->Item[_variant_t("UserName")]->Value);
			SETSTR(temp.csEmail,  set->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);
			SETSTR(temp.csS3AccessKey,  set->Fields->Item[_variant_t("S3AccessKey")]->Value);
			SETSTR(temp.csS3SecretKey,  set->Fields->Item[_variant_t("S3SecretKey")]->Value);
			SETSTR(temp.csFTPUrl,  set->Fields->Item[_variant_t("FTPUrl")]->Value);
			SETSTR(temp.csFTPUserName,  set->Fields->Item[_variant_t("FTPUserName")]->Value);
			SETSTR(temp.csFTPPassword,  set->Fields->Item[_variant_t("FTPPassword")]->Value);
			SETSTR(temp.csDeliveryPref,  set->Fields->Item[_variant_t("DeliveryPref")]->Value);
			SETSTR(temp.csCustomerAcctNum,  set->Fields->Item[_variant_t("CustomerAcctNum")]->Value);
			SETSTR(temp.csCMEAcctNumber,  set->Fields->Item[_variant_t("CMEAcctNumber")]->Value);
			SETSTR(temp.csHDLA,  set->Fields->Item[_variant_t("HDLA")]->Value);
			
			m_vecSubscribeCustumerList.push_back(temp);
			set->MoveNext();
		}
		set->Close();
	}
	DB_CATCH("VolmaxBonds_OrderCurve");

}
void CMECustomerSubscription::DisplaySubscribeCustomer()
{
	int iRow = 0;
	for(int iIndex = m_curTopRow ; iIndex < m_vecSubscribeCustumerList.size(); iIndex++, iRow++)
	{
		if (iRow >= m_numofRows)
			break;

		GetField(e_CustomerID + iRow)->SetValueInt(m_vecSubscribeCustumerList[iIndex].csCustomerID);
		GetField(e_CustomerName + iRow)->SetValueString(m_vecSubscribeCustumerList[iIndex].csCustomerName);
		GetField(e_FirstName + iRow)->SetValueString(m_vecSubscribeCustumerList[iIndex].csFirstName);
		GetField(e_LastName + iRow)->SetValueString(m_vecSubscribeCustumerList[iIndex].csLastName);
		GetField(e_UserName + iRow)->SetValueString(m_vecSubscribeCustumerList[iIndex].csUserName);
		GetField(e_Email + iRow)->SetValueString(m_vecSubscribeCustumerList[iIndex].csEmail);
		GetField(e_S3AccessKey + iRow)->SetValueString(m_vecSubscribeCustumerList[iIndex].csS3AccessKey);
		GetField(e_S3SecretKey + iRow)->SetValueString(m_vecSubscribeCustumerList[iIndex].csS3SecretKey);
		GetField(e_FTPUrl + iRow)->SetValueString(m_vecSubscribeCustumerList[iIndex].csFTPUrl);
		GetField(e_FTPUserName + iRow)->SetValueString(m_vecSubscribeCustumerList[iIndex].csFTPUserName);
		GetField(e_FTPPassword + iRow)->SetValueString(m_vecSubscribeCustumerList[iIndex].csFTPPassword);
		GetField(e_DeliveryPref + iRow)->SetValueString(m_vecSubscribeCustumerList[iIndex].csDeliveryPref);
		GetField(e_CustomerAcctNum + iRow)->SetValueString(m_vecSubscribeCustumerList[iIndex].csCustomerAcctNum);
		GetField(e_CMEAcctNumber + iRow)->SetValueString(m_vecSubscribeCustumerList[iIndex].csCMEAcctNumber);
		GetField(e_HDLA + iRow)->SetValueString(m_vecSubscribeCustumerList[iIndex].csHDLA);
	}
	while(iRow  < m_numofRows)
	{
		GetField(e_CustomerID		+ iRow)->SetBlankState();  
		GetField(e_CustomerName	+ iRow)->SetBlankState();  
		GetField(e_FirstName + iRow)->SetBlankState();  
		GetField(e_LastName + iRow)->SetBlankState();  
		GetField(e_UserName	+ iRow)->SetBlankState();  
		GetField(e_Email + iRow)->SetBlankState();  
		GetField(e_S3AccessKey	+ iRow)->SetBlankState();  
		GetField(e_S3SecretKey		+ iRow)->SetBlankState();
		GetField(e_FTPUrl	+ iRow)->SetBlankState();  
		GetField(e_FTPUserName + iRow)->SetBlankState();
		GetField(e_FTPPassword	+ iRow)->SetBlankState();  
		GetField(e_DeliveryPref + iRow)->SetBlankState();  
		GetField(e_CustomerAcctNum	+ iRow)->SetBlankState();  
		GetField(e_CMEAcctNumber		+ iRow)->SetBlankState();
		GetField(e_HDLA	+ iRow)->SetBlankState();  

		iRow++;
	}
}