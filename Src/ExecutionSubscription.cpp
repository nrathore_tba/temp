#include <fstream>
#include <sstream>
#include <stdafx.h> 
#include <shldisp.h>
#include <tlhelp32.h>
#include "zip.h"
#include "ExecutionSubscription.hpp"

#define SPLITLINE 50000

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(ExecutionSUBSCR)

ExecutionSUBSCR::ExecutionSUBSCR()
{	
}

ExecutionSUBSCR::~ExecutionSUBSCR()
{
}

bool ExecutionSUBSCR::InitData()
{
	GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));

	m_curTopRow= 0;
	for(m_numofRows = 0; GetField(e_LogMsg + m_numofRows); m_numofRows++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	//---------------------------------------------
	if( !IsRestoreUpdate( ) )
	{
		BF_Reg_Key_Ptr pKey1(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\CME"));

		int iInputLocation;
		RegDWORD strS3InPutLocation(pKey1, "S3InputLocation",2);
		iInputLocation = strS3InPutLocation.get_value();
		GetField(e_S3CMEBucket)->SetValueInt( iInputLocation );

		COleDateTime dtToday = COleDateTime::GetCurrentTime();
		BFDate bfToday;
		bfToday.SetYearMonthDate(dtToday.GetYear(),dtToday.GetMonth() - 1, dtToday.GetDay() );
		GetField(e_DateFilter)->SetValueDate(bfToday); 
	}

	fld_OutputPath = GetField(e_OutputPath);

	m_clScheduleInfo.bValid = false;
	m_clScheduleInfo.iRetInterval = GetField(e_RtryInterval)->GetValueInt() > 0 ? GetField(e_RtryInterval)->GetValueInt(): 5;
	
	m_bIsThreadRunning = false;
	m_bIsTrheadCompleted = true;
	m_iTotalCount = 0;
	m_iOpenFileCount = 0;

	m_enmOperation = OP_None;
	return true;
}

const int		__niMsgCouunt = 200;
CString __csMsgArry[__niMsgCouunt] = { "" };

void ExecutionSUBSCR::fnAddMessage( CString csaNewMsg )
{	
	for( int iI = __niMsgCouunt-1; iI > 0; iI-- )
		__csMsgArry[iI] = __csMsgArry[iI -1];
	
	__csMsgArry[0] = csaNewMsg;	
}

void ExecutionSUBSCR::fnDisplayMsg()
{
	int iRow = 0;
	for( int iIndex = m_curTopRow; iIndex < __niMsgCouunt; iIndex++, iRow++ )
	{
		if( iRow >= m_numofRows )
			break;
		
		BaseField * fldComment		= GetField(e_LogMsg + iRow);
		
		fldComment->SetValueString( __csMsgArry[iIndex] );
		fldComment->SetNotManual();
	}
	
	while( iRow < m_numofRows )
	{
		BaseField * fldComment		= GetField(e_LogMsg + iRow);
		fldComment->SetBlankState();
		iRow++;
	}
}

UINT SubscriptionProcessor(void *pVoid)
{
	ExecutionSUBSCR *pParser = (ExecutionSUBSCR*)pVoid;
	
	pParser->m_iTotalCount = 0;
	pParser->m_bIsThreadRunning = true;
	pParser->m_bIsTrheadCompleted = false;
	
	pParser->fnUpdateScheduleStatus( 1 );
	
	pParser->ReadSUBSCRFileCSV();
	
	pParser->fnUpdateScheduleStatus( 2 );
	pParser->m_enmOperation = OP_ProcessCompleted;
	pParser->m_clScheduleInfo.bValid = false;
	
	pParser->m_bIsThreadRunning = false;
	pParser->RequestExternalUpdate();

	return 0;
}

void ExecutionSUBSCR::Recalculate()
{
	if( IsFieldNew(e_RtryInterval) )
		m_clScheduleInfo.iRetInterval = GetField(e_RtryInterval)->GetValueInt() > 0 ? GetField(e_RtryInterval)->GetValueInt(): 5;

	if( m_bIsThreadRunning )
	{
		GetField(e_MsgTitle)->SetTitle(m_csMsg);		
	}
	else
	{
		if( !m_bIsThreadRunning && !m_bIsTrheadCompleted )
		{
			// After completion of thread
			GetField(e_MsgTitle)->SetTitle("Message");
			m_bIsTrheadCompleted = true;
		}

		if( !m_clScheduleInfo.bValid || m_enmOperation == OP_ProcessCompleted || IsFieldNew(e_ReloadSchedule) )
		{		
			fnGetNextSchedule( );			
		}

		if( !m_clScheduleInfo.bValid )	
		{
			StartRecalcTimer(60, false);
			return;
		}

		if( m_enmOperation == OP_Process || IsFieldNew(e_Process) )
		{		
			AfxBeginThread(SubscriptionProcessor, this);
		}

		if( m_enmOperation == OP_Uncompress )
		{
			fnUncompressGZ( );
		}

		if( m_enmOperation == OP_Download )
		{
			fnCopyS3FiletoLocal( );
		}

		if( m_enmOperation == OP_None )
			fnRecalc1( );

		if( m_enmOperation != OP_None  )
			StartRecalcTimer(0, false);
	}

	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_pgUP,e_pgDN,200,m_numofRows,m_curTopRow);		
	fnDisplayMsg();	
}

void ExecutionSUBSCR::ReadSUBSCRFileCSV( )
{
	m_clSubkDetailV.clear();

	FILE *fileIn(NULL);
	
	if( fopen_s(&fileIn, m_clScheduleInfo.csFilePath, "r") != 0 )
	{
		fnAddMessage( "Error while Opening fileIn" );
		return;
	}

	char chline[2048];
	std::string strLine("");	
	int lRecordRead(0);
	int iCountTT(0);
		
	CString csTemp = "Reading input File..";
	fnAddMessage( csTemp );
	this->RequestExternalUpdate();

	// Read Header...
	while( fgets(chline, 2048, fileIn) )
	{
		if( strchr(chline, '\n') )
		{
			strLine += chline;
			
			//ParseMessage(strLine); 
			strLine = "";			
			break;
		}
		else
		{
			strLine += chline;
		}		
	}
	
	while( fgets(chline, 2048, fileIn) )
	{
		if(strchr(chline, '\n'))
		{
			strLine += chline;

			int pos = strLine.find('\n');
			strLine.erase(pos, 1);
			
			ParseLine(strLine, lRecordRead); 

			strLine = "";
			lRecordRead++;
			iCountTT++;

			if( iCountTT >= 500 )
			{
				csTemp.Format("Reading line %d", lRecordRead);
				fnWriteMessage( csTemp );
				this->RequestExternalUpdate();
				iCountTT = 0;
			}
		}
		else
		{
			strLine += chline;
		}
	}

	fclose(fileIn);

	csTemp.Format("Total line %d", lRecordRead);
	fnAddMessage(csTemp);
	fnAddMessage("truncating old subscrition");
	this->RequestExternalUpdate();

	if( 1 )
	{
		CString sql;
		sql.Format("EXEC Proc_Beast_Backup_Subscription_2015");

		_RecordsetPtr set;
		if( FAILED(m_clpdbInt->GetRecordset(&set, sql)) )
		{
			SetErrorMessage(ErrorSeverity::e_Error, sql);
			fnAddMessage("Error while truncating subscrition in DB");
			return;
		}	
	}

	fnAddMessage("uploading new subscrition to DB");
	fnWriteMessage( "Uploading...");
	this->RequestExternalUpdate();
	iCountTT = 0;
	int iErrorCnt(0);

	for( int iRow = 0; iRow < m_clSubkDetailV.size(); iRow++ )
	{
		if( iCountTT >= 100 )
		{
			csTemp.Format("uploading line %d", iRow);
			fnWriteMessage( csTemp );
			this->RequestExternalUpdate();
			iCountTT = 0;
		}

		string strDBparam;
		m_clSubkDetailV[iRow].GetDBParam( strDBparam );

		CString sql;
		sql.Format("EXEC Proc_Beast_Submit_subscription_2015 %s ", strDBparam.c_str());

		_RecordsetPtr set;
		if( FAILED(m_clpdbInt->GetRecordset(&set, sql)) )
		{
			SetErrorMessage(ErrorSeverity::e_Error, sql);
			CString csTemp;
			csTemp.Format("Error in uploading Row = %d, sql = %s",iRow,sql);
			fnAddMessage(csTemp);
			iErrorCnt++;
		}	

		iCountTT++;
	}

	if( iErrorCnt == 0 )
		csTemp.Format("uploaded successful");
	else
		csTemp.Format("error in total row = %d, OPS please check", iErrorCnt);

	fnAddMessage(csTemp);
}


void ExecutionSUBSCR::ParseLine( std::string &ssLine, int iRownumber )
{
	std::stringstream ss(ssLine);
	std::string item;
	
	SubkDetail clSubkDetail;
	while(std::getline(ss, item, ','))
	{
		clSubkDetail.strArray.push_back(item.c_str());
	}

	if( clSubkDetail.strArray.size() != m_niInputColumnCount )
	{
		CString csStr;
		csStr.Format("Error in Row Number = %d, where Columns %d, expected columns %d", iRownumber, clSubkDetail.strArray.size(), m_niInputColumnCount);
		fnAddMessage(csStr);
		return;
	}

	CString cscustName = clSubkDetail.strArray[enm_CUST_NAME].c_str();
	if( cscustName.Find("'") != -1 )
	{
		cscustName.Replace("'", "''");
		clSubkDetail.strArray[enm_CUST_NAME] = cscustName.GetString();
	}

	CString csenmEmail = clSubkDetail.strArray[enm_CONTACT_EMAIL].c_str();
	if( csenmEmail.Find("'") != -1 )
	{
		csenmEmail.Replace("'","");
		clSubkDetail.strArray[enm_CONTACT_EMAIL] = csenmEmail.GetString();
	}

	m_clSubkDetailV.push_back( clSubkDetail );	
}


void ExecutionSUBSCR::fnRecalc1( )
{
	COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
	long lRecalcTime = 0;

	if( GetField( e_ManualRun )->GetValueInt() == 1 )
	{
		GetField(e_NextDownloadMsg)->SetTitle("Processing Set To Manaul");
	}
	else if( dtCurrentTime >= m_clScheduleInfo.clScheduleDtTime )
	{
		//Need to Process subscription...
		GetField(e_NextDownloadMsg)->SetTitle("Processing File");
		m_enmOperation = OP_Download;		
		fnAddMessage("Downloading File");
	}
	else if( dtCurrentTime < m_clScheduleInfo.clScheduleDtTime )
	{
		COleDateTimeSpan dtSpan = m_clScheduleInfo.clScheduleDtTime - dtCurrentTime;
		lRecalcTime = dtSpan.GetTotalSeconds();

		CString csTimer,csTmp;

		int iDays =	dtSpan.GetDays();
		int iLeftTotalHr = dtSpan.GetHours();
		int iLeftTotalMinute = dtSpan.GetMinutes();
		int iLeftTotalSecond = dtSpan.GetSeconds();

		if( iDays > 0 )
		{
			csTmp.Format("%d Day and ",iDays);					
			csTimer += csTmp;

			csTmp.Format("%02d:",iLeftTotalHr);					
			csTimer += csTmp;
		}
		else
		{
			if( iLeftTotalHr > 0 )
			{					
				csTmp.Format("%02d:",iLeftTotalHr);					
				csTimer += csTmp;
			}
		}

		csTmp.Format("%02d:",iLeftTotalMinute);					
		csTimer += csTmp;

		csTmp.Format("%02d",iLeftTotalSecond);
		csTimer += csTmp;				

		int iDate	= m_clScheduleInfo.clScheduleDtTime.GetDay();
		int iMonth	= m_clScheduleInfo.clScheduleDtTime.GetMonth();
		int iYear	= m_clScheduleInfo.clScheduleDtTime.GetYear();
		int iHour	= m_clScheduleInfo.clScheduleDtTime.GetHour();
		int iMinute = m_clScheduleInfo.clScheduleDtTime.GetMinute();
		int iSecond = m_clScheduleInfo.clScheduleDtTime.GetSecond();

		CString csStr;
		csStr.Format("Next Download: %d-%d-%d %2d:%2d:%2d [Timer: %s]",iDate,iMonth,iYear,iHour,iMinute,iSecond, csTimer);

		GetField(e_NextDownloadMsg)->SetTitle(csStr);
		GetField(e_NextDownloadMsg)->SetTitleForeColor(ColorManager::eSignalPositiveLight);

		if( lRecalcTime > 4 || lRecalcTime < 0 )
			lRecalcTime = 4;
	}

	if( lRecalcTime )
		StartRecalcTimer(lRecalcTime, false);
}

void ExecutionSUBSCR::fnGetNextSchedule( )
{
	m_clScheduleInfo.bValid = false;
	m_enmOperation = OP_None;

	BFDate bfdt = GetField(e_DateFilter)->GetValueDate();

#ifdef __Tets0//_DEBUG 
	m_clScheduleInfo.clScheduleDtTime = COleDateTime(2015,9,10,0,0,0);

	m_clScheduleInfo.iStatus = 0;
	m_clScheduleInfo.bValid = true;

	BFDate bfDate = m_clScheduleInfo.clScheduleDtTime.m_dt;

	m_clScheduleInfo.csDate.Format("%d%02d%02d", bfDate.GetYear(), bfDate.GetMonth()+1, bfDate.GetDate() );
#else

	CString csStrDtFilter;
	csStrDtFilter.Format("%d-%02d-%02d 00:00:00",bfdt.GetYear(), bfdt.GetMonth()+1, bfdt.GetDate());

	CString sql;
	sql.Format("SELECT top 1* from dbo.CME_Subscription_Schedule WHERE FileType ='Subscription' and IsNull(Status,0) In(0,1) and ScheduleDateTime > '%s' ORDER BY ScheduleDateTime", csStrDtFilter);

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		GetField(e_NextDownloadMsg)->SetTitle("Error while Loading Schedule");
		return;
	}
		
	if( VARIANT_FALSE == set->adoEOF )
	{
		SETLONG( m_clScheduleInfo.iStatus,		set->Fields->Item[_variant_t("Status")]->Value);	
		SETOLEDATE(	m_clScheduleInfo.clScheduleDtTime,	set->Fields->Item[_variant_t("ScheduleDateTime")]->Value);	
		m_clScheduleInfo.clMainScheduleDtTime = m_clScheduleInfo.clScheduleDtTime;

		m_clScheduleInfo.iRetryCount = 0;
		m_clScheduleInfo.csDate.Format("%d%02d%02d", m_clScheduleInfo.clScheduleDtTime.GetYear(), m_clScheduleInfo.clScheduleDtTime.GetMonth(), m_clScheduleInfo.clScheduleDtTime.GetDay() );		
		
		m_clScheduleInfo.bValid = true;
	}

	set->Close();
#endif

	if( m_clScheduleInfo.bValid  )
	{
		GetField( e_Year )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetYear() );
		GetField( e_Month )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetMonth() );
		GetField( e_Day )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetDay() );
		GetField( e_Hour )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetHour() );
		GetField( e_Minute )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetMinute() );
	}
	else
	{
		GetField(e_NextDownloadMsg)->SetTitle("No Schedule Loaded");
	}
}

void ExecutionSUBSCR::fnUpdateScheduleStatus( int iStatus )
{
	CString csdttime;
	csdttime.Format("%d-%02d-%02d %02d:%02d:00",m_clScheduleInfo.clMainScheduleDtTime.GetYear(), m_clScheduleInfo.clMainScheduleDtTime.GetMonth(), m_clScheduleInfo.clMainScheduleDtTime.GetDay(), m_clScheduleInfo.clMainScheduleDtTime.GetHour(), m_clScheduleInfo.clMainScheduleDtTime.GetMinute());
	
	COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
	CString csTimeStatus;
	csTimeStatus.Format("%d-%02d-%02d %02d:%02d:00",dtCurrentTime.GetYear(), dtCurrentTime.GetMonth(), dtCurrentTime.GetDay(), dtCurrentTime.GetHour(), dtCurrentTime.GetMinute());

	CString sql;
	if( iStatus == 1 )
		sql.Format("UPDATE dbo.CME_Subscription_Schedule SET Status = %d, ProcessStart = '%s' WHERE FileType ='Subscription' and ScheduleDateTime = '%s'", iStatus, csTimeStatus, csdttime);
	else
		sql.Format("UPDATE dbo.CME_Subscription_Schedule SET Status = %d,   ProcessEnd = '%s' WHERE FileType ='Subscription' and ScheduleDateTime = '%s'", iStatus, csTimeStatus, csdttime);

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		GetField(e_NextDownloadMsg)->SetTitle("Error while Loading Schedule");
		return;
	}
}

void ExecutionSUBSCR::fnCopyS3FiletoLocal( )
{
	CString strBasePath = fld_OutputPath->GetValueString();

	CString csInputBucket = ((ListField*)GetField(e_S3CMEBucket))->GetShortString();
	CString csProfile	  = ((ListField*)GetField(e_S3CMEBucket))->GetLongString();

	CString strFullInputPath;
	strFullInputPath.Format("%s/daily/subscriptionfile/%s/subscription_%s.gz", csInputBucket, m_clScheduleInfo.csDate, m_clScheduleInfo.csDate);

	CString csDestinationFolder;
	csDestinationFolder.Format("%s\\%s\\",strBasePath, m_clScheduleInfo.csDate);

	//"s3 cp s3://cmegroup-main-datamine-qa-staging/daily/endofday/20150805/EOD_20150805_E.gz  d:\daily\endofday\20150805\";
	CString csAWSCommand;
	csAWSCommand.Format(_T("aws s3 cp --profile %s %s %s"), csProfile, strFullInputPath, csDestinationFolder);					
	int iValue = system(csAWSCommand);	

	if( iValue == 0 )
	{
		fnAddMessage(csAWSCommand + " File Downloaded successfully");
		m_enmOperation = OP_Uncompress;
	}
	else
	{
		m_clScheduleInfo.clScheduleDtTime = COleDateTime::GetCurrentTime();;
		m_clScheduleInfo.clScheduleDtTime += COleDateTimeSpan(0,0,m_clScheduleInfo.iRetInterval,0);
		m_clScheduleInfo.iRetryCount++;

		CString csTemp;
		csTemp.Format("Error cmd %s, File Download schedule after %d minute", csAWSCommand, m_clScheduleInfo.iRetInterval );
		fnAddMessage( csTemp );

		m_enmOperation = OP_None;
	}
}

void ExecutionSUBSCR::fnUncompressGZ( )
{
	CString strBasePath = fld_OutputPath->GetValueString();

	CString csFilePath, csUncompFileName, csCommand;

	csFilePath.Format("%s\\%s\\subscription_%s.gz", strBasePath, m_clScheduleInfo.csDate, m_clScheduleInfo.csDate);
	csUncompFileName = csFilePath;
	csUncompFileName.Replace(".gz",".txt");

	//csCommand.Format("gzip.exe -d -c \\powervault3\CME_s3\daily\endofday\20150805\EOD_20150805_E.gz > \\powervault3\CME_s3\daily\endofday\20150805\EOD_20150805_E.txt");
	csCommand.Format("gzip.exe -d -c %s > %s", csFilePath, csUncompFileName);
		
	int	iValue = system(csCommand);

	if( iValue == 0 )
	{
		fnAddMessage(csCommand + " Uncomprssed file successfully");
		m_clScheduleInfo.csFilePath = csUncompFileName;
		m_enmOperation = OP_Process;
	}
	else if( PathFileExists(csUncompFileName) == FALSE )
	{
		fnAddMessage("Uncomprssed file Already exist");
		m_clScheduleInfo.csFilePath = csUncompFileName;
		m_enmOperation = OP_Process;
	}
	else
	{
		m_clScheduleInfo.clScheduleDtTime = COleDateTime::GetCurrentTime();;
		m_clScheduleInfo.clScheduleDtTime += COleDateTimeSpan(0,0,m_clScheduleInfo.iRetInterval,0);

		CString csTemp;
		csTemp.Format("error no = %d, cme = %s, - path %s, uncompress unsuccessful, File Download schedule after %d minute",iValue, csCommand, csFilePath, m_clScheduleInfo.iRetInterval);
		fnAddMessage( csTemp );

		m_enmOperation = OP_None;
	}
}



void ExecutionSUBSCR::fnInsertDeliveryReport(CString csPrdouct, CString csDestAccount, CString csFileName, unsigned long &lFileSize  )
{
	CString csCheckSum = "";

	CString csDeliveryTime;
	COleDateTime oleDateTime = COleDateTime::GetCurrentTime();
	csDeliveryTime.Format("%02d-%02d-%02d %02d:%02d", oleDateTime.GetYear(), oleDateTime.GetMonth(), oleDateTime.GetDay(), oleDateTime.GetHour(), oleDateTime.GetMinute());

	CString csSql;
	csSql.Format("EXEC Proc_Beast_Submit_CME_Daily_Delivery_Report '%s', '%s', '%s', %u, '%s','%s'", csPrdouct, csDestAccount, csFileName, lFileSize, csCheckSum, csDeliveryTime);
	
	_RecordsetPtr set;
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)csSql, true)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSql);
		PrintRawMessage(csSql);
		return;
	}
}

void ExecutionSUBSCR::fnInsertErroReport(CString csPrdouct, CString csDestAccount, CString csFileName, CString csExchnage, CString &csErrorCmd )
{	
	CString csSql;
	csSql.Format("EXEC Proc_Beast_Submit_CME_Daily_Error_Report '%s', '%s', '%s', '%s', '%s'", csPrdouct, csDestAccount, csFileName, csExchnage, csErrorCmd);
	
	_RecordsetPtr set;
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)csSql, true)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSql);
		PrintRawMessage(csSql);
		return;
	}
}