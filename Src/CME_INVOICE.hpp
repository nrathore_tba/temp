// Copyright (C) 2002 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#pragma once


#include <shared/BaseImage.hpp>

#ifdef _DEBUG
	#pragma comment(lib,"\\Thebeast\\application\\BFImageSharedD.lib")
	#pragma comment(lib,"\\SharedApps\\BFInterestRateSharedD.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFMarket.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFSvrFieldsD.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFUtilD.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\persistr.lib")
#else
	#pragma comment(lib,"\\Thebeast\\application\\BFImageShared.lib")
	#pragma comment(lib,"\\SharedApps\\BFInterestRateShared.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFMarketD.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFSvrFields.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFUtilD.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\persistd.lib")
#endif



// It is recommended that you put your application code in the 
// standard BEAST namespace, that will save you from having to
// use the namespace qualifiers. Macros are used to be able to
// disable namespaces. Use the macros on either side of class
// declarations as shown in this example.
BF_NAMESPACE_BEGIN

// An application image must derive from BaseImage. BaseImage
// has a number of virtual functions that we can override, but
// this example only works on the two pure virtual functions.
class CME_INVOICE: public BaseImage
{
public:

	enum FieldID
	{
		/*e_gen_inputfile = 1,
		e_gen_outputfile = 2,
		e_gen_filetype = 4,
		e_gen_process = 10,
		e_gen_filetes = 15,

		e_filter_exchange = 13,
		e_filter_product = 9,
		e_filter_FO = 14,
		
		e_filter_status = 12,*/

	};

	virtual bool InitData();
	virtual void Recalculate();

private:
	
};

BF_NAMESPACE_END
