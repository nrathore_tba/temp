// Copyright (C) 2000 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..


#include <stdafx.h>
#include <fstream>
#include <direct.h>
#include "afxsock.h"
#include <shared/DBConnection.hpp>
#include "CMEDailySubscriptionManager.hpp"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BF_NAMESPACE_USE

using namespace std;

IMPLEMENT_IMAGE(SubscriptionMaster)

SubscriptionMaster::SubscriptionMaster()
{
}

SubscriptionMaster::~SubscriptionMaster()
{
}

bool SubscriptionMaster::InitData()
{ 
	m_curTopRow= 0;
	for(m_numofRows = 0; GetField(e_CUST_NAME + m_numofRows); m_numofRows++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	// DB Connection Completed....
	
	fld_Product		= (ListField*)GetField(e_Product);
	fld_FtpAccont	= (ListField*)GetField(e_FTPAccount);
	return true;
}

void SubscriptionMaster::Recalculate()
{	
	if( IsFieldNew(e_Product) )
		fld_FtpAccont->SetValueInt(0);
		
	if( IsFieldNew(e_ReloadSubscr) ||  IsFieldNew(e_Product) || IsFieldNew(e_FTPAccount) || IsInitialRecalc() )
		LoadSubscription();

	fnPageUpDown( );
	DisplaySubscriptionList( );
}

void SubscriptionMaster::fnPageUpDown( )
{
	int iTotalRecord = m_clSubscriptionV.size( );
	if( IsFieldNew(e_Product) || IsFieldNew(e_FTPAccount) )
		m_curTopRow = 0;

	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpBase, e_PgDnBase, iTotalRecord, m_numofRows, m_curTopRow);	
}

void SubscriptionMaster::DisplaySubscriptionList()
{		
	CString csTemp;
	csTemp.Format("Total Rows = %d", m_clSubscriptionV.size());

	GetField(e_statusMsg)->SetTitle( csTemp );

	CString csPgNum;
	csPgNum.Format("%2d/%2d",(m_curTopRow/20)+1,(m_clSubscriptionV.size()/20));
	
	GetField(e_PgNumber)->SetTitle(csPgNum);
	
	int iRow(0);
	for( int iI = m_curTopRow; iI < m_clSubscriptionV.size(); iI++, iRow++ )
	{
		if( iRow >= m_numofRows )
			break;
		
		GetField( e_CUST_NAME + iRow )->SetValueString( m_clSubscriptionV[iI].csCUST_NAME );
		GetField( e_CONTACT_EMAIL + iRow )->SetValueString( m_clSubscriptionV[iI].csCONTACT_EMAIL );		
		GetField( e_DEST_ACCOUNT + iRow )->SetValueString( m_clSubscriptionV[iI].csDEST_ACCOUNT );		
		GetField( e_FTP_PASSWORD + iRow )->SetValueString( m_clSubscriptionV[iI].csFTP_PASSWORD);
		GetField( e_EXPIRATION_DATE + iRow )->SetValueDate( BFDate(m_clSubscriptionV[iI].oledtEXPIRATION_DATE.m_dt) );
		
		GetField( e_ACTIVE + iRow )->SetValueDouble( m_clSubscriptionV[iI].dACTIVE );
		GetField( e_CREATE_TIMESTAMP + iRow )->SetValueDate( BFDate(m_clSubscriptionV[iI].oledtCREATE_TIMESTAMP.m_dt) );
		GetField( e_VENUE_CODE + iRow )->SetValueString( m_clSubscriptionV[iI].csVENUE_CODE );
		GetField( e_FILE_TYPE + iRow )->SetValueString( m_clSubscriptionV[iI].csFILE_TYPE );
		
		GetField( e_ALL_PRODUCTS + iRow )->SetValueDouble( m_clSubscriptionV[iI].dALL_PRODUCTS );

		GetField( e_PROD_CODE + iRow )->SetValueString( m_clSubscriptionV[iI].csPROD_CODE );
		GetField( e_BBO_CODE + iRow )->SetValueString( m_clSubscriptionV[iI].csBBO_CODE );
		GetField( e_MD_CODE + iRow )->SetValueString( m_clSubscriptionV[iI].csMD_CODE );
		GetField( e_BLOCK_CODE + iRow )->SetValueString( m_clSubscriptionV[iI].csBLOCK_CODE );
		GetField( e_EOD_CODE + iRow )->SetValueString( m_clSubscriptionV[iI].csEOD_CODE );
		GetField( e_TICK_CODE + iRow )->SetValueString( m_clSubscriptionV[iI].csTICK_CODE );
		GetField( e_EXCH_CODE + iRow )->SetValueString( m_clSubscriptionV[iI].csEXCH_CODE );

		GetField( e_FOI_CODE + iRow )->SetValueString( m_clSubscriptionV[iI].csFOI_CODE );
	}

	while( iRow < m_numofRows )
	{
		GetField( e_CUST_NAME + iRow)->SetValueString("");		
		GetField( e_CONTACT_EMAIL + iRow)->SetValueString("");			
		GetField( e_DEST_ACCOUNT + iRow)->SetValueString("");			
		GetField( e_FTP_PASSWORD + iRow)->SetValueString("");

		GetField( e_EXPIRATION_DATE + iRow)->SetBlankState();
		GetField( e_ACTIVE + iRow)->SetBlankState();				
		
		GetField( e_CREATE_TIMESTAMP + iRow)->SetBlankState();		
		GetField( e_VENUE_CODE + iRow)->SetValueString("");			
		GetField( e_FILE_TYPE + iRow)->SetValueString("");				
		GetField( e_ALL_PRODUCTS + iRow)->SetBlankState();			

		GetField( e_PROD_CODE + iRow)->SetValueString("");				
		GetField( e_BBO_CODE + iRow)->SetValueString("");				
		GetField( e_MD_CODE + iRow)->SetValueString("");				
		GetField( e_BLOCK_CODE + iRow)->SetValueString("");			
		GetField( e_EOD_CODE + iRow)->SetValueString("");				
		GetField( e_TICK_CODE + iRow)->SetValueString("");				
		GetField( e_EXCH_CODE + iRow)->SetValueString("");

		GetField( e_FOI_CODE + iRow)->SetValueString("");
		iRow++;
	}	
}

void SubscriptionMaster::LoadSubscription( )
{
	std::map<CString,bool> clProdMap;
	std::map<CString,bool> clFTPAccoutnMap;

	m_clSubscriptionV.clear();

	CString sql;

	if( fld_Product->GetValueInt() == 0 && fld_FtpAccont->GetValueInt() == 0 )
	{
		sql.Format("SELECT * from dbo.subscription_2015");
	}
	else if( fld_Product->GetValueInt() == 0 )
	{
		sql.Format("SELECT * from dbo.subscription_2015 where Dest_Account = '%s'", fld_FtpAccont->GetShortString() );
	}
	else if( fld_FtpAccont->GetValueInt() == 0 )
	{
		sql.Format("SELECT * from dbo.subscription_2015 where File_Type = '%s'", fld_Product->GetShortString() );
	}
	else
	{
		sql.Format("SELECT * from dbo.subscription_2015 where File_Type = '%s' AND Dest_Account = '%s' ", fld_Product->GetShortString(), fld_FtpAccont->GetShortString() );
	}

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		return;
	}

	stSubsciption clstSubsciption;
	
	while(VARIANT_FALSE == set->adoEOF)
	{	
		SETSTR(	clstSubsciption.csCUST_NAME,		set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
		SETSTR(	clstSubsciption.csCONTACT_EMAIL,	set->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);	
		SETSTR(clstSubsciption.csDEST_ACCOUNT,		set->Fields->Item[_variant_t("DEST_ACCOUNT")]->Value);	
		SETSTR(	clstSubsciption.csFTP_PASSWORD,		set->Fields->Item[_variant_t("FTP_PASSWORD")]->Value);	
				
		SETDOUBLE(clstSubsciption.dACTIVE,			set->Fields->Item[_variant_t("ACTIVE")]->Value);
		SETDOUBLE(clstSubsciption.dALL_PRODUCTS,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);	

		SETOLEDATE(	clstSubsciption.oledtEXPIRATION_DATE,		set->Fields->Item[_variant_t("EXPIRATION_DATE")]->Value);	
		SETOLEDATE(	clstSubsciption.oledtCREATE_TIMESTAMP,		set->Fields->Item[_variant_t("CREATE_TIMESTAMP")]->Value);

		SETSTR(	clstSubsciption.csVENUE_CODE,	set->Fields->Item[_variant_t("VENUE_CODE")]->Value);	
		SETSTR(	clstSubsciption.csFILE_TYPE,	set->Fields->Item[_variant_t("FILE_TYPE")]->Value);	
		
		SETSTR(	clstSubsciption.csPROD_CODE,	set->Fields->Item[_variant_t("PROD_CODE")]->Value);	
		SETSTR(	clstSubsciption.csBBO_CODE,		set->Fields->Item[_variant_t("BBO_CODE")]->Value);	
		SETSTR(	clstSubsciption.csMD_CODE,		set->Fields->Item[_variant_t("MD_CODE")]->Value);	
		SETSTR(	clstSubsciption.csBLOCK_CODE,	set->Fields->Item[_variant_t("BLOCK_CODE")]->Value);	
		SETSTR(	clstSubsciption.csEOD_CODE,		set->Fields->Item[_variant_t("EOD_CODE")]->Value);	
		SETSTR(	clstSubsciption.csTICK_CODE,	set->Fields->Item[_variant_t("TICK_CODE")]->Value);	
		SETSTR(	clstSubsciption.csEXCH_CODE,	set->Fields->Item[_variant_t("EXCH_CODE")]->Value);	
		SETSTR(	clstSubsciption.csFOI_CODE,		set->Fields->Item[_variant_t("FOI_CODE")]->Value);

		clProdMap[ clstSubsciption.csFILE_TYPE ] = true;
		clFTPAccoutnMap[ clstSubsciption.csDEST_ACCOUNT ] = true;
		
		m_clSubscriptionV.push_back( clstSubsciption );
		set->MoveNext();
	}

	set->Close();	

	//----------------------------------------
	if( fld_Product->GetValueInt() == 0 && fld_FtpAccont->GetValueInt() == 0 )
	{
		std::map<CString,bool>::iterator itr = clProdMap.begin();

		CString csProdData = "ALL=0";

		while( itr != clProdMap.end() )
		{
			csProdData = csProdData + "|" + itr->first;
			itr++;
		}

		fld_Product->SetDataString(csProdData);
	}

	if( fld_FtpAccont->GetValueInt() == 0 )
	{
		//-----------------------------------
		std::map<CString,bool>::iterator itr1 = clFTPAccoutnMap.begin();

		CString csFTPAccount = "ALL=0";

		while( itr1 != clFTPAccoutnMap.end() )
		{
			csFTPAccount = csFTPAccount + "|" + itr1->first;
			itr1++;
		}

		((ListField*)GetField(e_FTPAccount))->SetDataString(csFTPAccount);
	}
}
												

