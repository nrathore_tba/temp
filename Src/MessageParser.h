#pragma once

#include <fstream>
#include "cme.hh"
#include "Encoder.hh"
#include "Decoder.hh"
#include "ValidSchema.hh"
#include "Compiler.hh"
#include "DataFile.hh"

#include "MessageSchema.h"
#include "MessageUtil.h"
#include "mktdata/MessageHeader.hpp"
#include "mktdata/ChannelReset4.hpp"
#include "mktdata/MDInstrumentDefinitionFuture27.hpp"
#include "mktdata/MDInstrumentDefinitionSpread29.hpp"
#include "mktdata/SecurityStatus30.hpp"
#include "mktdata/MDIncrementalRefreshBook32.hpp"
#include "mktdata/MDIncrementalRefreshDailyStatistics33.hpp"
#include "mktdata/MDIncrementalRefreshLimitsBanding34.hpp"
#include "mktdata/MDIncrementalRefreshSessionStatistics35.hpp"
#include "mktdata/MDIncrementalRefreshTrade36.hpp"
#include "mktdata/MDIncrementalRefreshVolume37.hpp"
#include "mktdata/MDInstrumentDefinitionOption41.hpp"
#include "mktdata/MDIncrementalRefreshTradeSummary42.hpp"
#include "mktdata/QuoteRequest39.hpp"

using namespace std;
using namespace mktdata;

class MarketDataDecoderUtil 
{
public:
	
	static sbe_uint32_t getPacketSequenceNumber(const char *buffer);
	static sbe_uint64_t getPacketSentEpochTime(const char *buffer);
	static sbe_uint16_t getMsgSize(const char *buffer, int offset);
};

class Enrichable 
{
public:
	Enrichable() {};
	~Enrichable() {};

	double GetValue(PRICENULL prc);
	double GetValue(PRICE prc);
	double GetValue(mktdata::FLOAT prc);
	double GetValue(DecimalQty qty);
	string getTradeDate(__int64 time);
	string getTransactionTime(__int64 time);
	void setMatchEventIndicator(string &strIndicator, MatchEventIndicator indicator);
	void setSettlPriceType(string &strSettlPriceType, SettlPriceType settlPriceType);
	void setInstAttribValue(string &strInstAttribValue, InstAttribValue instValue);
	void enrichIncrementalRefresh(Contract &contract);
	//void enrichChannel(Contract &contract, string lookup, bool bGroup);
};

class FutureContractTemplate27Decoder : public Enrichable 
{
public:
	FutureContractTemplate27Decoder() {};
	~FutureContractTemplate27Decoder() {};

	void decode(Message &message, SecurityDefList &listContract, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr) ;
};

class SpreadContractTemplate29Decoder : public Enrichable 
{
public:
	SpreadContractTemplate29Decoder() {};
	~SpreadContractTemplate29Decoder() {};

	void decode(Message &message, SecurityDefList &listContract, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr) ;
};

class OptionContractTemplate41Decoder : public Enrichable 
{
public:
	OptionContractTemplate41Decoder() {};
	~OptionContractTemplate41Decoder() {};

	void decode(Message &message, SecurityDefList &listContract, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr) ;
};

class ChannelReset4MsgDecoder : public Enrichable 
{
public:
	ChannelReset4MsgDecoder();
	~ChannelReset4MsgDecoder();

	void decode(Message &message, ChannelResetList &listChannelReset, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr);

private:
};

class SecurityStatus30Decoder : public Enrichable 
{
public:
	SecurityStatus30Decoder();
	~SecurityStatus30Decoder();

	void decode(Message &message, SecurityStatusList &listSecurityStatus, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr);

private:

	string MSGTYPE;
};

class IncrementalRefresh32Decoder : public Enrichable 
{
public:
	IncrementalRefresh32Decoder();
	~IncrementalRefresh32Decoder();

	void decode(Message &message, MarketDataList &listMktData, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr);

private:

	string MSGTYPE;
};

class IncrementalRefresh33Decoder : public Enrichable 
{
public:
	IncrementalRefresh33Decoder();
	~IncrementalRefresh33Decoder();

	void decode(Message &message, MarketDataList &listMktData, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr);

private:

	string MSGTYPE;
};

class IncrementalRefresh34Decoder : public Enrichable 
{
public:
	IncrementalRefresh34Decoder();
	~IncrementalRefresh34Decoder();

	void decode(Message &message, MarketDataList &listMktData, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr);

private:

	string MSGTYPE;
};

class IncrementalRefresh35Decoder : public Enrichable 
{
public:
	IncrementalRefresh35Decoder();
	~IncrementalRefresh35Decoder();

	void decode(Message &message, MarketDataList &listMktData, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr);

private:

	string MSGTYPE;
};

class IncrementalRefresh36Decoder : public Enrichable 
{
public:
	IncrementalRefresh36Decoder();
	~IncrementalRefresh36Decoder();

	void decode(Message &message, MarketDataList &listMktData, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr);

private:

	string MSGTYPE;
};

class IncrementalRefresh37Decoder : public Enrichable 
{
public:
	IncrementalRefresh37Decoder();
	~IncrementalRefresh37Decoder();

	void decode(Message &message, MarketDataList &listMktData, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr);

private:

	string MSGTYPE;
};

class QuoteRequest39Decoder : public Enrichable 
{
public:
	QuoteRequest39Decoder();
	~QuoteRequest39Decoder();

	void decode(Message &message, QuoteRequestList &listQuoteRequest, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr);

private:

	string MSGTYPE;
};

class IncrementalRefresh42Decoder : public Enrichable 
{
public:
	IncrementalRefresh42Decoder();
	~IncrementalRefresh42Decoder();

	void decode(Message &message, MarketDataList &listMktData, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr);

private:

	string MSGTYPE;
};

class MDMessageConvertor
{
public:
	MDMessageConvertor();
	~MDMessageConvertor();

	void setup(string inputfolder, string outputfolder, string contractFile, string secdefFile);
	void close();
	void convertMessage(Message &message);
	void convertMessageBinary(Message &message);
	int getSize() { return files.size(); };

private:

	int BINARY_PACKET_HEADER;

	string stroutputfolder, stroutdate;
	FilePtrMap files;
	
	string m_strContractFile;
	FILE *m_pContactFile;

	string m_strMstSecDefFile;
	FILE *m_pMstSecDefFile;

	int getTemplateId(const char *buffer, int offset, int bufferLength);
	void writeToFile(string &strfilename, string &fixMessage);

	FutureContractTemplate27Decoder futureContractTemplate27Decoder;
	OptionContractTemplate41Decoder optionContractTemplate41Decoder;
	SpreadContractTemplate29Decoder spreadContractTemplate29Decoder;

	ChannelReset4MsgDecoder	channelReset4Decoder;
	SecurityStatus30Decoder	securityStatus30Decoder;
	IncrementalRefresh32Decoder	incrementalRefresh32Decoder;
	IncrementalRefresh33Decoder	incrementalRefresh33Decoder;
	IncrementalRefresh34Decoder	incrementalRefresh34Decoder;
	IncrementalRefresh35Decoder	incrementalRefresh35Decoder;
	IncrementalRefresh36Decoder	incrementalRefresh36Decoder;
	IncrementalRefresh37Decoder	incrementalRefresh37Decoder;
	QuoteRequest39Decoder	quoteRequest39Decoder;
	IncrementalRefresh42Decoder	incrementalRefresh42Decoder;
};

class SecDefMessageConvertor
{
public:
	SecDefMessageConvertor();
	~SecDefMessageConvertor();

	void setup(string contractFile);
	void close();

	void convertInstrumentDef(Message &message);

private:

	int BINARY_PACKET_HEADER;
	
	string m_strContractFile;
	FILE *m_pContactFile;
	
	FutureContractTemplate27Decoder futureContractTemplate27Decoder;
	OptionContractTemplate41Decoder optionContractTemplate41Decoder;
	SpreadContractTemplate29Decoder spreadContractTemplate29Decoder;
};