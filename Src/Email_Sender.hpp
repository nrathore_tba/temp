// Copyright (C) 2002 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#pragma once


#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <time.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include <time.h>


#ifdef _DEBUG
	#pragma comment(lib,"\\Thebeast\\application\\BFImageSharedD.lib")
	#pragma comment(lib,"\\SharedApps\\BFInterestRateSharedD.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFMarketD.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFSvrFieldsD.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFUtilD.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\persistd.lib")
	#pragma comment(lib,"\\SharedApps\\MailAutomationSharedD.lib")
	#pragma comment(lib,"\\3rdParty\\openssl-0.9.8k_WIN32\\lib\\libeay32.lib")
	#pragma comment(lib,"\\3rdParty\\openssl-0.9.8k_WIN32\\lib\\ssleay32.lib")
#else
	#pragma comment(lib,"\\Thebeast\\application\\BFImageShared.lib")
	#pragma comment(lib,"\\SharedApps\\BFInterestRateShared.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFMarket.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFSvrFields.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFUtil.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\persistr.lib")
	#pragma comment(lib,"\\SharedApps\\MailAutomationShared.lib")
	#pragma comment(lib,"\\3rdParty\\openssl-0.9.8k_WIN32\\lib\\libeay32.lib")
	#pragma comment(lib,"\\3rdParty\\openssl-0.9.8k_WIN32\\lib\\ssleay32.lib")
#endif



// It is recommended that you put your application code in the 
// standard BEAST namespace, that will save you from having to
// use the namespace qualifiers. Macros are used to be able to
// disable namespaces. Use the macros on either side of class
// declarations as shown in this example.
BF_NAMESPACE_BEGIN

// An application image must derive from BaseImage. BaseImage
// has a number of virtual functions that we can override, but
// this example only works on the two pure virtual functions.

struct stEmail
{
	CString csDate;
	int iEventId;
	CString csProcessName;
	CString csHostName;
	CString csComment;
	CString csReported;
};

class EMail_Sender: public BaseImage
{
public:
	enum FieldIDs
	{
		e_pgUP		= 92,
		e_pgDN		= 93,
		e_LogMsg	= 4000,
	};

	virtual bool InitData();
	virtual void Recalculate();
	void SendEmails();
	void GetWatchDogEvents();
	void fnDisplayMsg();
	void fnAddMessage( CString csaNewMsg );
	const std::string currentDateTime() 
	{
		time_t     now = time(0);
		struct tm  tstruct;
		char       buf[80];
		tstruct = *localtime(&now);
		strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

		return buf;
	}

private:
	int m_numofRows;
	int m_curTopRow;	

	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;
	std::vector<stEmail> vecEMails;
};

BF_NAMESPACE_END
