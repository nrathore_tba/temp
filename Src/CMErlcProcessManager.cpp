
#include <stdafx.h> 
#include "CMErlcProcessManager.hpp"

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(CMErlcProcessManager)

CMErlcProcessManager::CMErlcProcessManager()
{		
}

CMErlcProcessManager::~CMErlcProcessManager()
{
}

bool CMErlcProcessManager::InitData()
{	
	//m_curTopRow= 0;
	//for(m_numofRows = 0; GetField(e_DisplayFileName + m_numofRows); m_numofRows++);

	m_curTopRowDB= 0;
	for(m_numofRowsDB = 0; GetField(e_DisplayFileNameDB + m_numofRowsDB); m_numofRowsDB++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME_BCP","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	// DB Connection Completed....
	
	m_bIsThreadRunning = false;
	m_lTotalRead = 0;	
	m_iOpenFileCount = 0;

	CString strUserName = this->GetUserName();
	GetField(900000)->SetValueString(strUserName);
	AddInfoField(GetField(900000));

	return true;
}

UINT MDRLCProcessor(void *pVoid)
{
	CMErlcProcessManager *pParser = (CMErlcProcessManager*)pVoid;
	
	pParser->m_lTotalRead = 0;
	pParser->m_iOpenFileCount = 0;
	pParser->m_bIsThreadRunning = true;
	pParser->fnParse();
	pParser->m_bIsThreadRunning = false;

	return 0;
}

void CMErlcProcessManager::Recalculate()
{
	if( IsFieldNew(e_StartProcess) && !m_bIsThreadRunning)
	{
		COleDateTime dtCurrent = COleDateTime::GetCurrentTime();
		CString strCurrentTime;
		strCurrentTime.Format("%04d-%02d-%02d %02d:%02d:%02d", dtCurrent.GetYear(), dtCurrent.GetMonth(), dtCurrent.GetDay(), 
			dtCurrent.GetHour(), dtCurrent.GetMinute(), dtCurrent.GetSecond());
		
		GetField(e_StartTime)->SetValueString(strCurrentTime);
		GetField(e_EndTime)->SetValueString("");

		AfxBeginThread(MDRLCProcessor, this);
	}

	if( m_bIsThreadRunning )
	{		
	}
	else
	{
		if( IsFieldNew(e_LoadFromDB) )
			LoadFromDB( );

		if( IsFieldNew(e_FileList) )
		{
			int iIndex = GetField(e_FileList)->GetValueInt();
			CString csPath = m_clFileListDB[iIndex].csFilePath;
			csPath.Replace("\\*.lzo","");

			GetField(e_UncomparessedPath)->SetValueString(csPath);
			GetField(e_OutputBasePath)->SetValueString(csPath);

			CString csFileDateName;
			//md_rlc_nym_20080125-r-00116.lzo
			csFileDateName = GetField(e_FileList)->GetDisplayString();

			CString csExchange=GetField(e_Exchange)->GetDisplayString();//xnym
			
			csExchange.Format("md_rlc_%s_", + csExchange.Mid(1,csExchange.GetLength()) );//nym
	
			csExchange.MakeLower();
			csFileDateName.Replace(csExchange,"");//20080125-r-00116.lzo
			csFileDateName=csFileDateName.Mid(0,8);
			
			GetField(e_OutputDate)->SetValueString(csFileDateName);

		}
	}

	fnPageUpDown( );
	DisplayFileList( );	
}

bool CMErlcProcessManager::fnOpenFile(FILE **m_pFile)
{
	CString csFilePath="",csFormat="",csExchange="",csYear="",csMonth="";
	CString csFileName=GetField(e_FileList)->GetDisplayString();
	csFileName.Replace(".lzo","");
	csFilePath.Format("%s\\%s",GetField(e_UncomparessedPath)->GetValueString(),csFileName);

	if(fopen_s(&(*m_pFile), csFilePath ,"r"))
		return false;

	//if( csFormat,GetField(e_Year)->GetValueInt() == 2008 && (GetField(e_Month)->GetValueInt() == 9 || GetField(e_Month)->GetValueInt() == 10) )
		GetField(e_OutputBasePath)->SetValueString("E:\\RLC\\rlc_");
	//else
	//	GetField(e_OutputBasePath)->SetValueString("\\\\uat-ss1\\RLC\\rlc_");
	
	csFormat = GetField(e_OutputBasePath)->GetValueString();
	
	csFormat += GetField(e_Exchange)->GetDisplayString();

	CreateDirectory(csFormat,NULL);

	csYear.Format("%s\\%s",csFormat,GetField(e_Year)->GetDisplayString());
	CreateDirectory(csYear,NULL);
	GetField(e_OutputBasePath)->SetValueString(csYear);
	csMonth.Format("%s\\%s",csYear,GetField(e_Month)->GetDisplayString());
	CreateDirectory(csMonth,NULL);

	m_csFolder.Format("%s\\%s",csMonth,GetField(e_OutputDate)->GetValueString());
	CreateDirectory(m_csFolder,NULL);
	return true;
}

bool CMErlcProcessManager::fnParse()
{
	FILE *m_pFile;
	if( !fnOpenFile(&m_pFile) )
	{
		GetField(e_Message)->SetTitle("Error while opening file");
		RequestExternalUpdate( );
		return false;
	}

	m_iStatus = 1;
	fnUpdateFileStatus();

	int row = 0;
	char chLine[1024];
	string sLine;
	string mstrLine = "";
	CString rowLine = "";
	
	while( fgets(chLine, 1024, m_pFile) )
	{
	
		if(strchr(chLine, '\n') )
		{
			mstrLine += chLine;
			sLine = mstrLine;

			if( fnParseCommonFields(mstrLine) )
			{
				fnWriteInFile(sLine);
				row++;
				if( row >= 100000 )
				{
					mstrLine = "";
					m_lTotalRead += row;

					CString csTotalRecordes;
					csTotalRecordes.Format("Reading records %u",m_lTotalRead);
					GetField(e_Message)->SetTitle(csTotalRecordes);

					RequestExternalUpdate( );
					row = 0;
				}
			}
			mstrLine="";
		}
		else if( strchr(chLine, ',') )
		{
			rowLine = chLine ;
			CString exchangeValue = rowLine.Mid(0, rowLine.Find(","));

			CString selectedExchangeValue = GetField(e_Exchange)->GetDisplayString();
			selectedExchangeValue = selectedExchangeValue.Mid(1).MakeLower();
			if (exchangeValue == selectedExchangeValue)
			{
				rowLine  += "\n";	
			

			//mstrLine += chLine;
			sLine = rowLine;

			if( fnParseCommonFields(sLine) )
			{
				fnWriteInFile(sLine);
				row++;
				if( row >= 100000 )
				{
					mstrLine = "";
					m_lTotalRead += row;

					CString csTotalRecordes;
					csTotalRecordes.Format("Reading records %u",m_lTotalRead);
					GetField(e_Message)->SetTitle(csTotalRecordes);

					RequestExternalUpdate( );
					row = 0;
				}
			}
			mstrLine="";
			}
		}
		else
		{
			mstrLine += chLine;
		}
	}
	
	fclose(m_pFile);

	for(m_Mapit = m_FileMap.begin(); m_Mapit != m_FileMap.end(); m_Mapit++)
	{
		if( m_Mapit->second != NULL )			
		{
			fclose(m_Mapit->second);
			m_Mapit->second = NULL;
		}
	}
		
	m_lTotalRead += row;
	printf("\n total records : %u",m_lTotalRead);
	CString csTotalRecordes;
	csTotalRecordes.Format("Total Record %u",m_lTotalRead);
	GetField(e_Message)->SetTitle(csTotalRecordes);

	GetField(e_EndTime)->SetValueString( fnGetCurrentDateTime() );	

	m_iStatus = 2;
	fnUpdateFileStatus();

	RequestExternalUpdate( );
	LoadFromDB( );	
	
}

bool CMErlcProcessManager::fnParseCommonFields(string strLine)
{
	CString csTemp = strLine.c_str();

	csExchange ="",csProdCode = "",csFOIType = "",csDate = "";
	
	if( csTemp.Find(",") )
	{
		csExchange = csTemp.Mid(0, csTemp.Find(","));
		csTemp.Replace(csExchange  +",","");

		csProdCode = csTemp.Mid(0, csTemp.Find(","));
		csTemp.Replace(csProdCode + ",","");

		csFOIType = csTemp.Mid(0, csTemp.Find(","));
		csTemp.Replace(csFOIType + ",","");

		csDate = csTemp.Mid(0, csTemp.Find(","));
		csTemp.Replace(csDate + ",","");
		return true;
	}
	return false;
}

bool CMErlcProcessManager::fnWriteInFile(string sLine)
{
	CString csFileName;
	csFileName.Format("%s\\%s_%s_%s_%s",m_csFolder,csExchange,csProdCode,csFOIType,GetField(e_OutputDate)->GetValueString());

	FILE *fp = NULL;

	if( m_FileMap.find(csFileName) == m_FileMap.end() )
	{	
		if( m_iOpenFileCount > 500 )
		{
			for(m_Mapit = m_FileMap.begin(); m_Mapit != m_FileMap.end(); m_Mapit++)
			{
				if( m_Mapit->second )
				{
					fclose(m_Mapit->second);
					m_Mapit->second = NULL;
				}
			}

			m_iOpenFileCount = 0;
		}

		if( fopen_s(&fp, csFileName ,"w") )
			return false;

		m_iOpenFileCount++;

		fputs(sLine.c_str(),fp);
		m_FileMap.insert(std::pair<CString,FILE *>(csFileName,fp));
	}
	else if( m_FileMap[csFileName] == NULL )
	{
		if( m_iOpenFileCount > 500 )
		{
			for(m_Mapit = m_FileMap.begin(); m_Mapit != m_FileMap.end(); m_Mapit++)
			{
				if( m_Mapit->second )
				{
					fclose(m_Mapit->second);
					m_Mapit->second = NULL;
				}
			}

			m_iOpenFileCount = 0;
		}

		if( fopen_s(&fp, csFileName ,"a+") )
			return false;

		m_iOpenFileCount++;

		m_FileMap[csFileName] = fp;
		fputs(sLine.c_str(),fp);
	}
	else
	{
		fp = m_FileMap[csFileName];
		fputs(sLine.c_str(),fp);
	}
}

CString CMErlcProcessManager::fnGetCurrentDateTime()
{
	CString strCurrentTime;

	COleDateTime dtCurrent = COleDateTime::GetCurrentTime();
	strCurrentTime.Format("%d-%02d-%02d %02d:%02d:%02d", dtCurrent.GetYear(), dtCurrent.GetMonth(), dtCurrent.GetDay(), 
		dtCurrent.GetHour(), dtCurrent.GetMinute(), dtCurrent.GetSecond());
	return strCurrentTime;
}

void CMErlcProcessManager::fnUpdateFileStatus()
{
	CString csPath = m_csFolder + "\\";

	COleDateTime dtCurrent = COleDateTime::GetCurrentTime();
	CString strCurrentTime;
	strCurrentTime.Format("%d-%02d-%02d %02d:%02d:%02d", dtCurrent.GetYear(), dtCurrent.GetMonth(), dtCurrent.GetDay(), 
	dtCurrent.GetHour(), dtCurrent.GetMinute(), dtCurrent.GetSecond());

	CString sql;
	if( m_iStatus == 2 )
		sql.Format("UPDATE CME_Historical_Files set Status = %d, OutputPath = '%s', ProcessEnd = '%s' WHERE FileName = '%s'",m_iStatus, csPath, strCurrentTime, GetField(e_FileList)->GetDisplayString());
	else
		sql.Format("UPDATE CME_Historical_Files set Status = %d, OutputPath = '%s', ProcessStart = '%s' WHERE FileName = '%s'",m_iStatus,csPath, strCurrentTime, GetField(e_FileList)->GetDisplayString());

	_RecordsetPtr set;

	DB_TRY
	{
		if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql)))
		{
			SetErrorMessage(ErrorSeverity::e_Error, sql);
			PrintRawMessage(sql);
		}

	}
	DB_CATCH(sql);
}

void CMErlcProcessManager::LoadFromDB( )
{
	m_clFileListDB.clear();

	int iYear = GetField(e_Year)->GetValueInt();
	int iMonth = GetField(e_Month)->GetValueInt();
	int iStatus = GetField(e_Status)->GetValueInt();
	CString csFileType = GetField(e_FileType)->GetDisplayString();	
	CString csExchange = GetField(e_Exchange)->GetDisplayString();
	CString csFormat = GetField(e_FileFormat)->GetDisplayString();

	CString sql;
	sql.Format("SELECT * FROM CME_Historical_Files  where FileType = '%s' AND [Year] = %d	AND [Month] = %d AND	Exchange = '%s' And	Format = '%s'",csFileType,iYear,iMonth, csExchange,csFormat);

	_RecordsetPtr set;

	CString csList = "", csTemp = "";

	DB_TRY
	{
		if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql, false)))
		{
			SetErrorMessage(ErrorSeverity::e_Error, sql);
			PrintRawMessage(sql);
		}

		FileDetail clFileDetail;	

		while( VARIANT_FALSE == set->adoEOF )
		{
			SETSTR(	clFileDetail.csFileName,	set->Fields->Item[_variant_t("FileName")]->Value);	
			SETLONGLONG(clFileDetail.lFileSize,	set->Fields->Item[_variant_t("Size")]->Value);	
			SETLONG(clFileDetail.iStatus,		set->Fields->Item[_variant_t("Status")]->Value);
			SETSTR(clFileDetail.csFilePath,		set->Fields->Item[_variant_t("Path")]->Value);

			if( clFileDetail.iStatus == 0 || clFileDetail.iStatus == 1 )
				if( csList.GetLength() ==  0 )
				{
					csList.Format("%s",clFileDetail.csFileName);
				}
				else
				{
					csTemp.Format("|%s",clFileDetail.csFileName);
					csList = csList + csTemp;
				}			

				m_clFileListDB.push_back( clFileDetail );	
				set->MoveNext();
		}
		set->Close();
	}
	DB_CATCH(sql);	

	DisplayFileList( );
	((ListField*)GetField(e_FileList))->SetDataString(csList);
}

void CMErlcProcessManager::fnPageUpDown( )
{
	int iTotalRecord = m_clFileListDB.size( );

	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpBaseDB,e_PgDnBaseDB,iTotalRecord,m_numofRowsDB,m_curTopRowDB);		
}

CString __csStatus[3] = { "Pending","Running","Processed"};

void CMErlcProcessManager::DisplayFileList( )
{
	int iRow(0);
	for( int iI = m_curTopRowDB; iI < m_clFileListDB.size(); iI++, iRow++ )
	{
		if( iRow >= m_numofRowsDB )
			break;

		GetField( e_DisplayFileNameDB + iRow )->SetValueString( m_clFileListDB[iI].csFileName );
		GetField( e_DisplayFileSizeDB + iRow )->SetValueInt( m_clFileListDB[iI].lFileSize );
		GetField( e_DisplayFileStatusDB + iRow )->SetValueString( __csStatus[m_clFileListDB[iI].iStatus] );
	}

	while( iRow < m_numofRowsDB )
	{
		GetField( e_DisplayFileNameDB + iRow )->SetValueString("");
		GetField( e_DisplayFileSizeDB + iRow )->SetBlankState();
		GetField( e_DisplayFileStatusDB + iRow )->SetBlankState();
		iRow++;
	}
}
