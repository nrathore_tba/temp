#include "StdAfx.h"
#include "BBO_NEW.h"
#include "BBOConstants.h"
#include <math.h>
#include <sstream>
#include "zip.h"
#include "unzip.h"
#include <ShlDisp.h>


BF_NAMESPACE_USE

CBBO::~CBBO(void){}

// Test

void CBBO::setProcessParameters()
{
	if(_pImage)
	{	
		CString csFUTOPT,csInputFile,csOutputPath;

		setExchange(std::string((LPCTSTR)_pImage->GetField(e_filter_exchange)->GetDisplayString()));
		setProduct(std::string((LPCTSTR)_pImage->GetField(e_filter_product)->GetDisplayString()));
		setInerval(_pImage->GetField(e_interval)->GetValueInt());
		
		csOutputPath = _pImage->GetField(e_gen_outputfile)->GetValueString();
		
		csOutputPath.Trim();

		SetOutputBBOFilePath(std::string(csOutputPath));
			

		csFUTOPT = _pImage->GetField(e_filter_FO)->GetDisplayString();
		if(csFUTOPT == "Future")	
			SetFUTOPT("F");
		else if(csFUTOPT == "Option")
			SetFUTOPT("O");
		else
			SetFUTOPT(std::string((LPCTSTR)csFUTOPT));

		csInputFile = _pImage->GetField(e_gen_inputfile)->GetValueString();
		csInputFile.Trim();

		OpenInputBBOFile(std::string((LPCTSTR)csInputFile));

		OpenExceptionFile();
	}
	
}
bool CBBO::OpenInputBBOFile(std::string strFileName)
{
	try
	{
		/*int iIndex = strFileName.find_last_of("\\");
		_strInputPath = strFileName.substr(0,iIndex+1);
		_strInputFileName = createUnzip(strFileName);
		_strInputFileName = _strInputPath+_strInputFileName;*/
		
		std::size_t found = strFileName.find(".zip");
		if (found!=std::string::npos)
			_strInputFileName =	createUnzip(strFileName);
		else
			_strInputFileName = strFileName;
		int iIndex = _strInputFileName.find_last_of("\\");
		_strInputPath = _strInputFileName.substr(0,iIndex+1);
		_strIniFile = _strInputFileName + ".ini";

		fpInputFile.open(_strInputFileName.c_str());
		return true;
	}
	catch (CMemoryException* e)
	{
		CString csErrorMsg;
		std::cout<<"Exception::OpenBBOFile Error: CMemoryException"<<std::endl;
		e->Delete();
		return false;
	}
	catch (CFileException* e)
	{
		std::cout<<"Exception::OpenBBOFile Error: CFileException"<<std::endl;
		e->Delete();
		return false;
	}
	catch (CException* e)
	{
		std::cout<<"Exception::OpenBBOFile Error:CException"<<std::endl;
		e->Delete();
		return false;
	}
}


bool CBBO::OpenExceptionFile()
{
	try
	{
		_strExceptionFileName = _strInputFileName + ".not";

		fpExceptionFile.open(_strExceptionFileName.c_str(),std::ofstream::out | std::ofstream::app);
		return true;
	}
	catch (CMemoryException* e)
	{
		CString csErrorMsg;
		std::cout<<"Exception::OpenExceptionFile Error: CMemoryException"<<std::endl;
		e->Delete();
		return false;
	}
	catch (CFileException* e)
	{
		std::cout<<"Exception::OpenExceptionFile Error: CFileException"<<std::endl;
		e->Delete();
		return false;
	}
	catch (CException* e)
	{
		std::cout<<"Exception::OpenExceptionFile Error:CException"<<std::endl;
		e->Delete();
		return false;
	}
}


bool CBBO::Process()
{
	long lCnt=0;
	try
	{
		//Clear MetaData
		_mapBBOMetaData.clear();

		//Check Input File Processed Offset
		GotoLine(fpInputFile,GetFileProcessCount(_strInputFileName));
		std::string strLine;
		while(!fpInputFile.eof())
		{
			getline(fpInputFile,strLine);

			if (strLine.length() == BBO_STANDARD_LENGH || strLine.length() == BBO_OLD_LENGTH) 
			{
				std::string strProd = strLine.substr(23, 3);
				trim2(strProd);
				std::string strFO = strLine.substr(26, 1);
				trim2(strFO);

				std::string strKey = _strExchange + "_" + strProd + "_" + strFO;
		
				if((_strProduct == "*") && (_strFUTOPT == "*"))  // All Records
				{
					WriteRecord(strKey,strLine,);
				}
				else if((_strProduct == "*") || (_strFUTOPT == "*"))
				{
					if(_strProduct == "*")                // All Product
					{
						if(_stricmp(_strFUTOPT.c_str(),strFO.c_str()) == 0)
						{
							WriteRecord(strKey,strLine);
						}
					}
					else if ((_strFUTOPT == "*"))            // All FUT/OPT
					{
						if(IsProductFound(strProd))
						{
							WriteRecord(strKey,strLine);
						}
					}
				}
				else
				{
					// Filter product and Fut/Opt
					if((IsProductFound(strProd)) && (_stricmp(_strFUTOPT.c_str(),strFO.c_str()) == 0))
					{
						WriteRecord(strKey,strLine);
					}
				}

			}
			else
			{
				std::cout<<"The message " << strLine.c_str() << " will be skipped because it is malformed"<<std::endl;	
				fpExceptionFile << strLine.c_str()<<std::endl;
			}
			

			if((lCnt %_interval == 0))
			{
				FlushOpenFiles();
				WriteFileProcessCount(_strInputFileName,lCnt);
				updateImageFields(lCnt);
			}
			lCnt++;
		}

		
		WriteFileProcessCount(_strInputFileName,lCnt);
		
	}
	catch (CMemoryException* e)
	{
		e->Delete();
	}
	catch (CFileException* e)
	{
		e->Delete();
	}
	catch (CException* e)
	{
		e->Delete();
	}

	fpInputFile.close();
	fpExceptionFile.close();
	CloseOpenFiles();
	createZip();
	GenereateMetaData();

	char buff[100];
	sprintf(buff,"\r\n DONE  !!  Lines Processed: %ld",lCnt);
	_pImage->GetField(e_filter_status)->SetValueString(buff);
	CString csOutput = _pImage->GetField(e_output)->GetValueString();
	csOutput +=" ";
	csOutput +=buff;
	_pImage->GetField(e_output)->SetValueString(csOutput);
	_pImage->RequestExternalUpdate();
	return true;
}

void CBBO::GenereateMetaData()
{
	std::map<std::string,stBBOMetaData>::iterator it = _mapBBOMetaData.begin();
	CString csOutput = _pImage->GetField(e_output)->GetValueString();
	for(;it!=_mapBBOMetaData.end();it++)
	{
		char buff[100];
		sprintf(buff,"\r\n Key: %s  count: %ld  ",it->first.c_str(),it->second._count);
		csOutput +=buff;
		std::cout<<"Key: "<<it->first<<"  value: "<<it->second._count<<std::endl;
	}
	_pImage->GetField(e_output)->SetValueString(csOutput);
	_pImage->RequestExternalUpdate();

}
bool CBBO::WriteRecord(std::string &strKey,std::string & strLine,std::string &strExch,std::string &strProd,std::string &strFutOpt)
{
	//fpOutPutFile <<strLine.c_str()<<std::endl;

	std::map<std::string,stBBOMetaData>::iterator it = _mapBBOMetaData.find(strKey);
	
	if(it!=_mapBBOMetaData.end())
	{
		//found
		//*it->second._fpOutPutFile<<strLine.c_str()<<std::endl;
		it->second._strText += strLine;
		it->second._strText += "\n";
		it->second._count++;
	}
	else
	{
		//Not Found
		std::string strFileName;
		strFileName = _strOutputFilePath;
		strFileName += strKey + "_";
		strFileName += GetTime();
		strFileName += ".txt";

		stBBOMetaData _objstBBOMetaData;
		_objstBBOMetaData._strExch = strExch;
		_objstBBOMetaData._strProd = strProd;
		_objstBBOMetaData._strFUTOPT = strFutOpt;
		_objstBBOMetaData._strFileName = strFileName;

		_objstBBOMetaData._count=1;

		_objstBBOMetaData._strText += strLine;
		_objstBBOMetaData._strText += "\n";

		_objstBBOMetaData._fpOutPutFile = new std::ofstream(strFileName.c_str(),std::ofstream::out | std::ofstream::app) ;
		_mapBBOMetaData.insert(std::pair<std::string,stBBOMetaData>(strKey,_objstBBOMetaData));
		//*_objstBBOMetaData._fpOutPutFile<<strLine.c_str()<<std::endl;
		
	}

	//UpdateRecord(strKey);
	return true;
}
void CBBO::CloseOpenFiles()
{
	std::map<std::string,stBBOMetaData>::iterator it = _mapBBOMetaData.begin();
	for(;it!=_mapBBOMetaData.end();it++)
	{
		it->second._fpOutPutFile->close();
		delete it->second._fpOutPutFile;
	}
}



void CBBO::createZip()
{
	std::map<std::string,stBBOMetaData>::iterator it = _mapBBOMetaData.begin();
	for(;it!=_mapBBOMetaData.end();it++)
	{
		
		std::string strZipOutFileName,strZipFileName,strInputFileName;

		strZipOutFileName = _strOutputFilePath;
		strZipOutFileName += it->first + "_";
		strZipOutFileName += GetTime();
		strZipOutFileName += ".zip";

		strInputFileName = _strOutputFilePath;
		strInputFileName += it->first + "_";
		strInputFileName += GetTime();
		strInputFileName += ".txt";

		//strZipFileName ="zip";
		strZipFileName += it->first + "_";
		strZipFileName += GetTime();
		strZipFileName += ".txt";
		
		CString csTmp = "\r\n Creating Zip of ";
		csTmp += strZipOutFileName.c_str();
		_pImage->GetField(e_filter_status)->SetValueString(csTmp);

		HZIP hz = CreateZip(strZipOutFileName.c_str(),0);
		ZRESULT res = 	ZipAdd(hz,strZipFileName.c_str(),  strInputFileName.c_str());
		if(res == 0)
		{
			remove(strInputFileName.c_str());
		}
		CloseZip(hz);
		CString csOutput = _pImage->GetField(e_output)->GetValueString();
		csOutput += "\r\n Zip created: ";
		csOutput += strZipOutFileName.c_str();
		_pImage->GetField(e_output)->SetValueString(csOutput);
		_pImage->RequestExternalUpdate();
	}
}



std::string CBBO::formatTime(std::string  trdtime)
{
	switch (trdtime.length()) {
	case (1):
		return ONEDIGETTIME + trdtime;
	case (2):
		return TWODIGETTIME + trdtime;
	case (3):
		return THREEDIGETTIME + trdtime.substr(0, 1) + COL
			+ trdtime.substr(1);
	case (4):
		return FOURDIGETTIME + trdtime.substr(0, 2) + COL
			+ trdtime.substr(2);
	case (5):
		return FIVEDIGETTIME + trdtime.substr(0, 1) + COL
			+ trdtime.substr(1, 3) + COL + trdtime.substr(3);
	default:
		return trdtime.substr(0, 2) + COL + trdtime.substr(2, 4)
			+ COL + trdtime.substr(4, 6);

	}
}


std::string CBBO::formatEntryDate(std::string line) 
{
	if (line.length() == 70) 
	{
		std::string strtmp = line.substr(64, 6);
		trim2(strtmp);
		return TWENTY + strtmp;
	} else {
		return EMPTY_ENTRY_DATE;
	}
}


void CBBO::trim2(std::string & str)
{
	std::string::size_type pos = str.find_last_not_of(' ');
	if(pos != std::string::npos) {
		str.erase(pos + 1);
		pos = str.find_first_not_of(' ');
		if(pos != std::string::npos) str.erase(0, pos);
	}
	else str.erase(str.begin(), str.end());
}


std::string CBBO::formatPrice(std::string inPrice, std::string decimalLoc) 
{
	if (!_strcmpi(inPrice.c_str(),"")) 
	{
		return PRICEZERO;
	} 
	else
	{
		if (!_strcmpi(decimalLoc.c_str(),""))
		{
			return inPrice;
		} 
		else 
		{
			double idx = atof(decimalLoc.c_str());

			double price = atof(inPrice.c_str());

			std::ostringstream os;
			os << double(price / pow(10, idx));
			std::string tmpPrice = os.str();

			int dicl = tmpPrice.find(DOT);
			switch (tmpPrice.substr(dicl + 1).length()) {
			case (1):
				return tmpPrice + SIXZERO;
			case (2):
				return tmpPrice + FIVEZERO;
			case (3):
				return tmpPrice + FOURZERO;
			case (4):
				return tmpPrice + THREEZERO;
			case (5):
				return tmpPrice + TWOZERO;
			case (6):
				return tmpPrice + ONEZERO;
			default:
				return tmpPrice;
			}

		}
	}
}

void CBBO::updateImageFields(long & lCnt)
{
	char buff[512];
	sprintf(buff,"Lines Processed: %ld",lCnt);
	_pImage->GetField(e_filter_status)->SetValueString(buff);
	_pImage->RequestExternalUpdate();
}


std::string CBBO::createUnzip(std::string sInputFile)
{

	size_t index = sInputFile.find_last_of("\\");
	std::string unzipath = sInputFile.substr(0, index+1);
	std::string strOutputString;


	CString csTmp = "Creating UnZip of ";
	csTmp += sInputFile.c_str();
	_pImage->GetField(e_filter_status)->SetValueString(csTmp);
	_pImage->RequestExternalUpdate();

	HZIP hz = OpenZip(sInputFile.c_str(),0);
	ZIPENTRY ze; GetZipItem(hz,-1,&ze); int numitems=ze.index;
	// -1 gives overall information about the zipfile
	for (int zi=0; zi<numitems; zi++)
	{ ZIPENTRY ze; GetZipItem(hz,zi,&ze); // fetch individual details

	strOutputString =  unzipath + ze.name;
		//UnzipItem(hz, zi, ze.name);         // e.g. the item's name.
	UnzipItem(hz, zi, strOutputString.c_str());         // e.g. the item's name.
	}
	CloseZip(hz);

	CString csOutput = _pImage->GetField(e_output)->GetValueString();
	csOutput += "\r\n UnZip created: ";
	csOutput += strOutputString.c_str();
	_pImage->GetField(e_output)->SetValueString(csOutput);
	_pImage->RequestExternalUpdate();

	return strOutputString;
}
