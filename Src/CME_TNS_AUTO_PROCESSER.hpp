// Copyright (C) 2002 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#pragma once

//test

#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>

#ifdef _DEBUG
	#pragma comment(lib,"\\Thebeast\\application\\BFImageSharedD.lib")
	#pragma comment(lib,"\\SharedApps\\BFInterestRateSharedD.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFMarketD.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFSvrFieldsD.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFUtilD.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\persistd.lib")
	#pragma comment(lib,"\\SharedApps\\MailAutomationSharedD.lib")
	#pragma comment(lib,"\\3rdParty\\openssl-0.9.8k_WIN32\\lib\\libeay32.lib")
	#pragma comment(lib,"\\3rdParty\\openssl-0.9.8k_WIN32\\lib\\ssleay32.lib")
#else
	#pragma comment(lib,"\\Thebeast\\application\\BFImageShared.lib")
	#pragma comment(lib,"\\SharedApps\\BFInterestRateShared.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFMarket.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFSvrFields.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\BFUtil.lib")
	#pragma comment(lib,"\\TheBeast\\Shared\\persistr.lib")
	#pragma comment(lib,"\\SharedApps\\MailAutomationShared.lib")
	#pragma comment(lib,"\\3rdParty\\openssl-0.9.8k_WIN32\\lib\\libeay32.lib")
	#pragma comment(lib,"\\3rdParty\\openssl-0.9.8k_WIN32\\lib\\ssleay32.lib")
#endif


BF_NAMESPACE_BEGIN


struct ScheduleInfo
{
	int		iRetInterval;
	int		iRetryCount;
	bool	bValid;
	int		iStatus;
	CString csSubType;	
	COleDateTime clScheduleDtTime;	
	COleDateTime clMainScheduleDtTime;	
	CString csDate;

	CString  csFilePath;
	CString	 csEOD_FilePath;
	CString  csEODE_FilePath;
};

enum enmOperation
{
	OP_Download,
	OP_Uncompress,
	OP_Process,
	OP_ReadThread,
	OP_ProcessCompleted,
	OP_None
};



struct stEXchProd
{
	stEXchProd()
	{
		csALLPRODUCT =  csFileName= csFUTOPT= csVENUE = "";
		iRowCount = iFileCount=0;

	}
	CString csExch;
	CString csTNS;
	CString csFileName;
	CString csFUTOPT;
	CString csVENUE;
	CString csALLPRODUCT;
	int iFileCount;
	long iRowCount;
	
};

struct stSubOutPutFile
{
	stSubOutPutFile()
	{
		csFileName = csExch ="";
		iLineCount = iFileCount = 0;
		bIsFileOpen = false;
		fp=NULL;
	}

	CString csFileName;
	CString csExch;
	long iLineCount;
	int iFileCount;
	std::vector<CString> _mVecFileListForSplit;
	FILE *fp;
	bool bIsFileOpen;
};

struct TNSSubscriptionDtl
{
	TNSSubscriptionDtl()
	{
		stCUST_NAME = "";
		stCONTACT_EMAIL = "";
		stDEST_ACCOUNT = "";
		stFTP_PASSWORD = "";
		stALL_PRODUCTS = "";
		stOutputFileName = "";
		_mVecExchProdCodes.clear();
		_mlCount=0;
		_miFileCount=0;
		bUploaded =false;


	}
	CString stCUST_NAME;
	CString stCONTACT_EMAIL;
	CString stDEST_ACCOUNT;
	CString stFTP_PASSWORD;
	CString stALL_PRODUCTS;
	std::vector<stEXchProd> _mVecExchProdCodes;
	CString stOutputFileName;
	//std::vector<CString> _vecFileList;
	std::map<CString,stSubOutPutFile> m_mapOutputFiles;
	FILE *fp;
	long _mlCount;
	int _miFileCount;
	int iUploadReturn;
	bool bUploaded;
};

typedef std::map<CString,TNSSubscriptionDtl> mapTNSSubscriptionDtl;

class CME_TNS_AUTO_PROCESSER: public BaseImage
{
public:

	enum FieldIDs
	{
		e_NextDownloadMsg = 50,	
		e_MsgTitle		  = 65,
		e_OutputPath	  = 62,

		e_ReloadSchedule  = 100,
		e_DateFilter	  = 95,
		e_ManualRun		  = 96,
		e_Process		  = 97,
		e_RtryInterval	  = 98,
		
		e_Year		= 201,
		e_Month		= 202,
		e_Day		= 215,
		e_Hour		= 213,
		e_Minute	= 214,

		e_S3CMEBucket = 60,
		e_DestAccount = 58,
		e_S3FTPBucket = 59,

		e_LogMsg	= 4000,
		e_pgUP		= 92,
		e_pgDN		= 93,
	};


	virtual bool InitData();
	virtual void Recalculate();
	
	CString m_csLocalProcessPath;
	bool m_bIsTrheadCompleted;
	bool m_bIsThreadRunning;
	BaseField *fld_OutputPath;
	CString m_csMsg;
	CCriticalSection	m_MsgCriticalSec;

	void fnWriteMessage(CString csString )
	{
		CSingleLock clMsgSingleLock(&m_MsgCriticalSec);

		clMsgSingleLock.Lock();

		m_csMsg = csString.GetString();

		clMsgSingleLock.Unlock();
	}

	CString fnReadMessage( )
	{
		CSingleLock clMsgSingleLock(&m_MsgCriticalSec);

		clMsgSingleLock.Lock();

		CString csStr = m_csMsg.GetString();

		clMsgSingleLock.Unlock();

		return csStr;
	}	
	

	const std::string currentDateTime() 
	{
		time_t     now = time(0);
		struct tm  tstruct;
		char       buf[80];
		tstruct = *localtime(&now);
		strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
		return buf;
	}
	
	void LoadSubscription();
	void fnPageUpDown( );
	void DisplaySubscription();
	void fnCopyS3FiletoLocal( );
	void decompressAllFiles();
	void createUnzip(std::string & sInputFile);
	void fnDisplayMsg();
	void fnAddMessage( CString csaNewMsg );
	void fnGetNextSchedule( );
	void fnRecalc1( );
	void fnUpdateScheduleStatus( int iStatus );
	void ProcessTNS();
	bool ConvertRecord(std::string & strout,std::string & strExch,std::string & strLine);
	std::string formatTime(std::string  trdtime);
	std::string formatEntryDate(std::string line);
	void trim2(std::string & str);
	std::string formatPrice(std::string inPrice, std::string decimalLoc) ;
	void parseFile(TNSSubscriptionDtl & objSubDtl ,CString & csFile,CString & csExchange);
	bool dirExists(const std::string& dirName_in);
	void createZip();
	void UploadSubscription();
	void get_all_files_names_within_folder(std::string & folder, std::string & csExch);
	void RemoveUnncessaryFiles(std::string & folder);
	bool exist(const std::string& name);
	bool fnGetDownloadedFilesCount(std::string & folder);
	void fnInsertDeliveryReport(CString csPrdouct, CString csDestAccount, CString csFileName, unsigned long &lFileSize, CString &csAwscmd );
	void fnInsertErroReport(CString csPrdouct, CString csDestAccount, CString csFileName, CString csExchnage, CString &csErrorCmd );
	void fnSendMail();

	bool isSubExists(TNSSubscriptionDtl & SubDtl, stEXchProd &objstEXchProd)
	{
		for(unsigned int i=0;i<SubDtl._mVecExchProdCodes.size();i++)
		{
			stEXchProd tmpObj = SubDtl._mVecExchProdCodes[i];

			if(tmpObj.csALLPRODUCT  == objstEXchProd.csALLPRODUCT)
				if(tmpObj.csExch  == objstEXchProd.csExch)
					if(tmpObj.csTNS  == objstEXchProd.csTNS)
						if(tmpObj.csFUTOPT  == objstEXchProd.csFUTOPT)
							if(tmpObj.csVENUE  == objstEXchProd.csVENUE)
							{
								return true;
							}
		}
		return false;
	}

	long generateUniqueNo(CString & csSubAC,CString & csEx,CString &csDate)
	{
		long lCount = 0;
		for(int i =0;i <csSubAC.GetLength();i++)
			lCount += (int)csSubAC.GetAt(i);

		for(int i =0;i <csEx.GetLength();i++)
			lCount += (int)csEx.GetAt(i);

		for(int i =0;i < csDate.GetLength();i++)
			lCount += (int)csDate.GetAt(i);

		return lCount;
	}

	unsigned long  GetFileSize(std::string const &path) 
	{
		WIN32_FIND_DATA data;
		HANDLE h = FindFirstFile(path.c_str(), &data);
		if (h == INVALID_HANDLE_VALUE)
			return -1;

		FindClose(h);

		return data.nFileSizeLow | data.nFileSizeHigh << 32;
	}
private:

	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;
	
	int m_numofRows;
	int m_curTopRow;	
	mapTNSSubscriptionDtl m_TNSSubMap;
	std::vector<CString> m_vecDownloadedZipfiles;

public:
	ScheduleInfo m_clScheduleInfo;	
	enmOperation m_enmOperation;
	
};

BF_NAMESPACE_END
