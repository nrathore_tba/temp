#include <fstream>
#include <sstream>
#include <stdafx.h> 
#include <shldisp.h>
#include <tlhelp32.h>
#include "CME_Product_Calendar.hpp"

#define SPLITLINE 20000

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(CME_Product_Calendar)

bool status ;

CME_Product_Calendar::CME_Product_Calendar()
{	
}

CME_Product_Calendar::~CME_Product_Calendar()
{
}

bool CME_Product_Calendar::InitData()
{
	GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));

	m_strStatus			= GetField( e_strStatus);
	m_strProduct		= GetField( e_ProductName);

	m_lstExchange		= (ListField*) GetField(e_lstExchange);	
	m_lstFileType		= (ListField*) GetField(e_lstFileType);	
	m_lstFileFormat		= (ListField*) GetField(e_lstFileFormat);
			
	m_curTopRow= 0;
	for(m_numofRows = 0; GetField(e_strFileName + m_numofRows); m_numofRows++);
	if( !InitDataBase() )
	{
		m_strStatus->SetValueString("Database Connection Failed ..!!");
		return false;
	}	
	m_strProduct->SetValueString("");
	COleDateTime dtToday = COleDateTime::GetCurrentTime();
	BFDate bfToday;
	bfToday.SetYearMonthDate(dtToday.GetYear(),dtToday.GetMonth() - 1, dtToday.GetDay() ); 
	
	GetField(e_dtFromDate)->SetValueDate(bfToday); 
	GetField(e_dtToDate)->SetValueDate(bfToday); 
	
	return true;
}

void CME_Product_Calendar::Recalculate()
{
	if( IsFieldNew( e_btnGetDetails) )
	{
		GetProductDetails();		
	}

	if( m_lstFileType->GetValueInt() ^ 0 )
		m_lstFileFormat->SetBlankState();
	else
		m_lstFileFormat->SetNormalState();
		
	fnPageUpDown();
	DisplayProductDetails();
}

void CME_Product_Calendar::fnPageUpDown( )
{
	int iTotalRecord = m_vecProductDetails.size();
	
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_btnPageUp,e_btnPageDn,iTotalRecord,m_numofRows,m_curTopRow);	
}

bool CME_Product_Calendar::InitDataBase()
{
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));
	
	CString csServerName;
	RegString strServerName(pKey, "ServerName","UAT-RDS");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");

	if( bResult )
	{
		m_clpDBInt = m_clDBConn.GetDBInterfaceNew();
		return true;
	}
	return false ;
}

void  CME_Product_Calendar::GetProductDetails()
{
	m_vecProductDetails.clear(); 

	CString csExchange = m_lstExchange->GetDisplayString();
	CString csFileType = m_lstFileType->GetDisplayString();
	CString csFileFormat = m_lstFileFormat->GetDisplayString();
	CString csProductName = m_strProduct->GetDisplayString();

	if( m_lstFileType->GetValueInt() > 0 )
		csFileFormat = "";
	
	BFDate  bfFromDt = GetField(e_dtFromDate)->GetValueDate(); 
	BFDate  bfToDt	 = GetField(e_dtToDate)->GetValueDate(); 
	if( bfFromDt > bfToDt )
	{
		m_strStatus->SetValueString("Please correct Date range");
		return;
	}
	

	CString csFromDt, csToDt;
	csFromDt.Format("%d-%02d-%02d",	bfFromDt.GetYear(), bfFromDt.GetMonth()+1, bfFromDt.GetDate());
	csToDt.Format("%d-%02d-%02d",	bfToDt.GetYear(), bfToDt.GetMonth()+1, bfToDt.GetDate());

	CString sqlQuery ;
	if( m_lstExchange->GetValueInt() == 0 )
	{
		if(csProductName.IsEmpty())
			sqlQuery.Format("SELECT * FROM dbo.CME_Product_Calendar WHERE FileType = '%s' AND FileFormat = '%s' AND FileDate >= '%s' AND FileDate <= '%s' order by FileDate", csFileType, csFileFormat, csFromDt, csToDt);
		else
			sqlQuery.Format("SELECT * FROM dbo.CME_Product_Calendar WHERE Product = '%s' AND FileType = '%s' AND FileFormat = '%s' AND FileDate >= '%s' AND FileDate <= '%s' order by FileDate", csProductName, csFileType, csFileFormat, csFromDt, csToDt);
	}
	else
	{
		if(csProductName.IsEmpty())
			sqlQuery.Format("SELECT * FROM dbo.CME_Product_Calendar WHERE Exchange ='%s' AND FileType = '%s' AND FileFormat = '%s' AND FileDate >= '%s' AND FileDate <= '%s' order by FileDate", csExchange, csFileType, csFileFormat, csFromDt, csToDt);
		else
			sqlQuery.Format("SELECT * FROM dbo.CME_Product_Calendar WHERE Product = '%s' AND Exchange ='%s' AND FileType = '%s' AND FileFormat = '%s' AND FileDate >= '%s' AND FileDate <= '%s' order by FileDate", csProductName, csExchange, csFileType, csFileFormat, csFromDt, csToDt);
	}
	
	_RecordsetPtr recordSet;
	if( FAILED( m_clpDBInt->GetRecordset(&recordSet, sqlQuery, false) ) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sqlQuery);
		m_strStatus->SetValueString(sqlQuery );
		return;
	}
		
	CString fYear,fMonth,fDay ;
	while (VARIANT_FALSE == recordSet->adoEOF)
	{
		ProductDetails objProduct ;

		SETSTR(	objProduct.strFileName,	recordSet->Fields->Item[_variant_t("FileName")]->Value);	
		SETSTR(	objProduct.strFileType,	recordSet->Fields->Item[_variant_t("FileType")]->Value);
		SETBFDATE( objProduct.FileDate,	recordSet->Fields->Item[_variant_t("FileDate")]->Value);
		SETSTR(	objProduct.strExchange,	recordSet->Fields->Item[_variant_t("Exchange")]->Value);	
		SETSTR(	objProduct.strProduct,	recordSet->Fields->Item[_variant_t("Product")]->Value);	
		SETSTR(	objProduct.strFutOpt,	recordSet->Fields->Item[_variant_t("FutOpt")]->Value);
		SETSTR(	objProduct.strSpread,	recordSet->Fields->Item[_variant_t("Spread")]->Value);	
		SETSTR(	objProduct.strFileFormat,	recordSet->Fields->Item[_variant_t("FileFormat")]->Value);	
		SETLONGLONG(	objProduct.ulFileSize,		recordSet->Fields->Item[_variant_t("Size")]->Value);

		m_vecProductDetails.push_back(objProduct);

		recordSet->MoveNext();
	}
	recordSet->Close();

	CString msg ;
	msg.Format(" %d Records Found ..!!",m_vecProductDetails.size());
	m_strStatus->SetValueString(msg);
}

void CME_Product_Calendar :: DisplayProductDetails()
{
	int iRow = 0;
	for(int iIndex = m_curTopRow; iIndex < m_vecProductDetails.size(); iIndex++, iRow++)
	{
		if( iRow >= m_numofRows )
			break;

		ProductDetails  &clObjProduct = m_vecProductDetails[iIndex];

		GetField(e_strFileName	+ iRow)->SetValueString(clObjProduct.strFileName);  
		GetField(e_strFileType	+ iRow)->SetValueString(clObjProduct.strFileType);  
		GetField(e_strFileDate	+ iRow)->SetValueDate(clObjProduct.FileDate);  
		GetField(e_strExchange	+ iRow)->SetValueString(clObjProduct.strExchange);  
		GetField(e_strProduct	+ iRow)->SetValueString(clObjProduct.strProduct);
		GetField(e_strFutOption + iRow)->SetValueString(clObjProduct.strFutOpt);
		GetField(e_strSpread	+ iRow)->SetValueString(clObjProduct.strSpread);
		GetField(e_strFileFormat+ iRow)->SetValueString(clObjProduct.strFileFormat);
		GetField(e_size			+ iRow)->SetValueInt(clObjProduct.ulFileSize);		
	}

	while( iRow < m_numofRows )
	{
		GetField(e_strFileName	+ iRow)->SetBlankState();  
		GetField(e_strFileType	+ iRow)->SetBlankState();  
		GetField(e_strFileDate	+ iRow)->SetBlankState();   
		GetField(e_strExchange	+ iRow)->SetBlankState();   
		GetField(e_strProduct	+ iRow)->SetBlankState(); 
		GetField(e_strFutOption + iRow)->SetBlankState(); 
		GetField(e_strSpread	+ iRow)->SetBlankState(); 
		GetField(e_strFileFormat+ iRow)->SetBlankState(); 
		GetField(e_size			+ iRow)->SetBlankState(); 

		iRow++;
	}

	CString csPageNumber;
	int iRowCount = m_vecProductDetails.size();
	int iCurrPage = m_curTopRow / m_numofRows + 1;
	int iTotalPage;	
	if( (iRowCount % m_numofRows) == 0)
	{
		iTotalPage = (iRowCount/m_numofRows);	
	}
	else
	{
		iTotalPage = (iRowCount/m_numofRows) + 1;	
	}

	if(iTotalPage <= 0)
		iTotalPage = 1;

	csPageNumber.Format("%d/%d",iCurrPage,iTotalPage);
	GetField(e_Label_Navigate)->SetTitle(csPageNumber);
}




