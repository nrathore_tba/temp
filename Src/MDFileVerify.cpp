// Copyright (C) 2002 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#include "stdafx.h"
#include "MDFileVerify.hpp"
#include <fields/ListField.hpp>
#include <MailAutomation/MailAutomation.h>
#include <algorithm>

// Put this	after all the include statements

BF_NAMESPACE_USE
	
IMPLEMENT_IMAGE(MDFileVerify)	


const int		__niMsgCouunt = 500;
CString __csMsgArry[__niMsgCouunt] = { "" };



void MDFileVerify::LogMsg(char *buff)
{
	FILE *fp=NULL;
	fp = fopen(strLogFileName.GetBuffer(),"a+");
	if(fp)
	{
		fprintf(fp,"%s\n",buff);
		fclose(fp);
	}
}



static UINT MDFileProcesser(LPVOID lparam)
{
	MDFileVerify *pMDFileVerify = (MDFileVerify*)lparam;
	pMDFileVerify->fnStartProcess();
	return true;
}
bool MDFileVerify::InitData()
{
	
	m_curTopRow= 0;
	for(m_numofRows = 0; GetField(e_LogMsg + m_numofRows); m_numofRows++);

	COleDateTime dtToday = COleDateTime::GetCurrentTime();
	BFDate bfToday;
	bfToday.SetYearMonthDate(dtToday.GetYear(),dtToday.GetMonth() - 1, dtToday.GetDay() );
	GetField(e_date)->SetValueDate(bfToday);

	return true;
} 
bool MDFileVerify::exist(const std::string& name)
{

	std::ifstream file(name);
	if(!file)            // If the file was not found, then file is 0, i.e. !file=1 or true.
		return false;    // The file was not found.
	else                 // If the file was found, then file is non-0.
		return true;     // The file was found.
}

void MDFileVerify::get_all_files_names_within_folder(std::string & folder, std::string & csExch)
{

	mVecFileList.clear();
    char search_path[200];
    sprintf(search_path, "%s/*.*", folder.c_str());
    
	WIN32_FIND_DATA fd; 
    HANDLE hFind = ::FindFirstFile(search_path, &fd); 

    if(hFind != INVALID_HANDLE_VALUE) 
	{ 
        do 
		{ 
			// read all (real) files in current folder
			// , delete '!' read other 2 default folder . and ..
			if(! (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ) 
			{
				CString csFile(fd.cFileName);
				if(csFile.Find(csExch.c_str())!=-1 && csFile.Find(".gz") == -1)
				{
					std::string strFile = folder + fd.cFileName;
					if (std::find(mVecFileList.begin(), mVecFileList.end(), CString(strFile.c_str()))!=mVecFileList.end() )
					{
						continue; // we already downloaded this file skipping it.
						
					}
					else
					{
						mVecFileList.push_back(CString(strFile.c_str()));
					}
				}
			}
		}
		while(::FindNextFile(hFind, &fd)); 
        ::FindClose(hFind); 
    }  
	CString csLog;
	csLog.Format("Total files to process: %d",mVecFileList.size());
	fnAddMessage(csLog);
	RequestExternalUpdate();
}

bool MDFileVerify::fnStartProcess()
{
	CString csInputPath = GetField(e_inputpath)->GetValueString();
	CString csExch = GetField(e_exchange)->GetDisplayString();
	
	BFDate bfdt = GetField(e_date)->GetValueDate();

	CString csDate;
	csDate.Format("%d%02d%02d",bfdt.GetYear(), bfdt.GetMonth()+1, bfdt.GetDate());

	if(csInputPath.Right(1)!='\\')
		csInputPath += "\\";

	
	CString csProcessingPath;
	csProcessingPath.Format("%s%s_Output\\%s\\",csInputPath,csDate,csExch);

	if(GetField(e_abspath)->GetValueInt() == 1)
		csProcessingPath = GetField(e_inputpath)->GetValueString();


	get_all_files_names_within_folder(std::string(csProcessingPath.GetBuffer()),std::string(csExch.GetBuffer()));

	fnAddMessage(" Processing of files started ");
	RequestExternalUpdate();

	//GenerateOutputLogFileName
	CString cstmp(currentDateTime().c_str());
	cstmp.Replace("-",".");
	cstmp.Replace(':','.');
	strLogFileName = "D:\\Thebeast\\log\\";
	strLogFileName += "MDCheck_";
	strLogFileName +=  cstmp;
	strLogFileName +=".log";
	GetField(e_logfile)->SetValueString(strLogFileName);
	RequestExternalUpdate();
	
	if(mVecFileList.size()>0)
		GetField(e_btStart)->SetEnabled(false);
	else
		fnAddMessage(" Files not found. ");

	for(unsigned int i=0;i<mVecFileList.size();i++)
	{
		if(!exist(std::string(mVecFileList[i].GetBuffer())))
			return false;
		
		filein.open(mVecFileList[i].GetBuffer());
		int lastIndex = mVecFileList[i].ReverseFind('\\');
		strCurrentProcesingFile = mVecFileList[i].Right(mVecFileList[i].GetLength()-lastIndex-1);

		_mapThirtyFour.clear();
		iTag34 = 0;
		iTag35 = 0;
		_lineCount = 0;
		valthrityfour = 0;
		while(!filein.eof())
		{
			std::string strLine,strLine34,strLine35;
			std::getline(filein,strLine);
			strLine34 = strLine;
			strLine35 = strLine;
			fnCheck35(strLine35);
			fnCheck34(strLine34);
			_lineCount++;
		}
		filein.close();
		if((iTag34 == 0) && (iTag35 == 0))
		{
			fnAddMessage(mVecFileList[i] + " File is correct for Both Tag=34 and Tag35=X");
			RequestExternalUpdate();
		}
		else
		{
			CString csLog;
			csLog.Format(" Incorrect File %s :(Tag=34) no of wrong msg count : %d, (35=X & 83=) no of wrong msg count : %d",strCurrentProcesingFile,iTag34,iTag35);
			fnAddMessage(csLog);
		}
	}
	fnAddMessage(" Processing of files Complete. ");
		
	if((iTag34 > 0) || (iTag35 > 0))
		fnAddMessage("Please check Log File: "+strLogFileName); 
	
	RequestExternalUpdate();
	
	GetField(e_btStart)->SetEnabled(true);
	return true;
}

void MDFileVerify::Recalculate()
{	
	if(IsFieldNew(e_btStart))
	{
		AfxBeginThread(MDFileProcesser,this);
	}
	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_pgUP,e_pgDN,500,m_numofRows,m_curTopRow);	
	fnDisplayMsg();	
	StartRecalcTimer(10, false);
}

void MDFileVerify::fnAddMessage( CString csaNewMsg )
{	
	for( int iI = __niMsgCouunt-1; iI > 0; iI-- )
		__csMsgArry[iI] = __csMsgArry[iI -1];
	
	CString csMsg;
	csMsg.Format("(%s) %s",currentDateTime().c_str(),csaNewMsg);
	__csMsgArry[0] = csMsg;	
}

void MDFileVerify::fnDisplayMsg()
{
	int iRow = 0;
	for( int iIndex = m_curTopRow; iIndex < __niMsgCouunt; iIndex++, iRow++ )
	{
		if( iRow >= m_numofRows )
			break;

		BaseField * fldComment		= GetField(e_LogMsg + iRow);

		if(__csMsgArry[iIndex].Find("Success")!=-1 || __csMsgArry[iIndex].Find(" correct")!=-1)
			fldComment->SetForeColor(ColorManager::eFrameGreen);
		else if(__csMsgArry[iIndex].Find("Error")!=-1 || __csMsgArry[iIndex].Find(" Incorrect")!=-1)
			fldComment->SetForeColor(ColorManager::eFrameRed);
		else
			fldComment->SetForeColor(ColorManager::eFrameWhite) ;

		fldComment->SetValueString( __csMsgArry[iIndex] );
		fldComment->SetNotManual();
	}

	while( iRow < m_numofRows )
	{
		BaseField * fldComment		= GetField(e_LogMsg + iRow);
		fldComment->SetBlankState();
		iRow++;
	}
}

long MDFileVerify::GetTagValueLong(std::string & strLine,std::string & strTag)
{
	std::string strFinalTag = "\x01";
	strFinalTag +=strTag;
	strFinalTag +="=";
	std::size_t found = strLine.find(strFinalTag);
	if (found!=std::string::npos)
	{
		std::size_t foundSOH = strLine.find("\x01",found+1);
		if (foundSOH!=std::string::npos)
		{
			found = found + strFinalTag.length();
			std::string strVal = strLine.substr(found,(foundSOH-found));
			long tmpVal = atol(strVal.c_str());
			return tmpVal;
		}
	}
	return -1;
}

long MDFileVerify::GetTagValueLong(std::string & strLine,std::string & strTag,int pos,int & returnpos)
{
	std::string strFinalTag = "\x01";
	strFinalTag +=strTag;
	strFinalTag +="=";
	std::size_t found = strLine.find(strFinalTag,pos+1);
	if (found!=std::string::npos)
	{
		std::size_t foundSOH = strLine.find("\x01",found+1);
		if (foundSOH!=std::string::npos)
		{
			found = found + strFinalTag.length();
			std::string strVal = strLine.substr(found,(foundSOH-found));
			long tmpVal = atol(strVal.c_str());
			returnpos = found;
			return tmpVal;
		}
	}
	returnpos = -1;
	return -1;
}

std::string MDFileVerify::GetTagValueString(std::string & strLine,std::string & strTag)
{
	std::string strFinalTag = "\x01";
	strFinalTag +=strTag;
	strFinalTag +="=";
	std::size_t found = strLine.find(strFinalTag);
	if (found!=std::string::npos)
	{
		std::size_t foundSOH = strLine.find("\x01",found+1);
		if (foundSOH!=std::string::npos)
		{
			found = found + strFinalTag.length();
			std::string strVal = strLine.substr(found,(foundSOH-found));
			return strVal;
		}
	}

	return "null";
}

std::string MDFileVerify::GetTagValueString(std::string & strLine,std::string & strTag, int pos, int & returnpos)
{
	std::string strFinalTag = "\x01";
	strFinalTag +=strTag;
	strFinalTag +="=";
	std::size_t found = strLine.find(strFinalTag,pos+1);
	if (found!=std::string::npos)
	{
		std::size_t foundSOH = strLine.find("\x01",found+1);
		if (foundSOH!=std::string::npos)
		{
			found = found + strFinalTag.length();
			std::string strVal = strLine.substr(found,(foundSOH-found));
			returnpos = found;
			return strVal;
		}
	}
	returnpos = -1;
	return "null";
}

bool MDFileVerify::fnCheck34(std::string & strLine)
{
	long tmpVal = GetTagValueLong(strLine,std::string("34"));

	if(tmpVal!=-1)
	{
		if(tmpVal < valthrityfour)
		{
			CString csLog;
			csLog.Format(" Incorrect File: %s, Line:%lu, Tag 34 not in ascending prev=%u, current= %u",strCurrentProcesingFile,_lineCount+1,valthrityfour,tmpVal);
			LogMsg(csLog.GetBuffer());
			valthrityfour = tmpVal;
			iTag34++;
		}
		else
			valthrityfour = tmpVal;
	}
	return true;
}

bool MDFileVerify::fnCheck35(std::string & strLine)
{
	std::string S35= GetTagValueString(strLine,std::string("35"));
	if(S35.compare("X")==0)
	{
		long L268 = GetTagValueLong(strLine,std::string("268"));

		int pos=0;
		int IreturnPos48;
		int IreturnPos83;
		for(int i=0;i<L268;i++)
		{
			std::string S48 = GetTagValueString(strLine,std::string("48"),pos,IreturnPos48);
			if(S48.compare("null")!=0)
			{

				long L83 = GetTagValueLong(strLine,std::string("83"),IreturnPos48,IreturnPos83);

				if(L83!= -1)
				{
					MapThirtyFour::iterator it = _mapThirtyFour.find(S48);
					if(it !=_mapThirtyFour.end())
					{
						long oldVal = it->second;
						oldVal = oldVal + 1;
						if(L83 != oldVal)
						{
							CString csLog;
							csLog.Format(" Incorrect File: %s, Line:%lu, 35=X , Tag 48=%s Current83=%u, Previous83= %u",strCurrentProcesingFile,_lineCount+1,it->first.c_str(),L83,oldVal-1);
							LogMsg(csLog.GetBuffer());
							it->second = L83;
							iTag35++;
						}
						else
							it->second = L83;

					}
					else
					{
						_mapThirtyFour.insert(std::pair<std::string,long>(S48,L83));
					}
				}

				pos =   IreturnPos48 +1;
			}
		}
	}
	return true;
}
