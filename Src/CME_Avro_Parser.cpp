#include <stdafx.h>
#include "CME_Avro_Parser.hpp"
#include "..\Src\MessageParser.h"
#include <boost/filesystem.hpp>
#include <MailAutomation/MailAutomation.h>

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(MDAvroProcessor)

CCriticalSection gCriticalSection;

avro::ValidSchema loadSchema(const char* filename)
{
	std::ifstream ifs(filename);
	avro::ValidSchema result;
	avro::compileJsonSchema(ifs, result);
	return result;
}

std::string fnGetmdMessage(std::vector<uint8_t> &mdMessage )
{
	std::string strTemp;
	strTemp = "";

	for( int iI = 0; iI < mdMessage.size(); iI++)
	{
		unsigned char c = (unsigned char) mdMessage[iI];
		strTemp += c;
	}

	return strTemp;
}

MDAvroProcessor::MDAvroProcessor()
{
	m_bIsThreadRunning = false;
}

MDAvroProcessor::~MDAvroProcessor()
{
}

bool MDAvroProcessor::InitData()
{
	m_nScreenID = GetScreenID();

	COleDateTime dtCurr = COleDateTime::GetCurrentTime();
	GetField(e_DefaultDate)->SetValueDate(BFDate(dtCurr.GetYear(), dtCurr.GetMonth()-1, dtCurr.GetDay()));

	m_objSchedule.strProcessingDate = dtCurr.Format(_T("%Y%m%d"));
	GenerateScheduleInfo();
	
	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	//---------------------------------------------	

	BF_Reg_Key_Ptr pKey1(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\CME"));

	int iFTPLocation;
	RegDWORD strS3Location(pKey1, "S3FTPLocation",0);
	iFTPLocation = strS3Location.get_value();
	GetField(e_S3FTPBucket)->SetValueInt( iFTPLocation );

	int iInputLocation;
	RegDWORD strS3InPutLocation(pKey1, "S3InputLocation",2);
	iInputLocation = strS3InPutLocation.get_value();
	GetField(e_S3InputBucket)->SetValueInt( iInputLocation );

	m_curTopRow= 0;
	for(m_numofRows = 0; GetField(e_CUST_NAME + m_numofRows); m_numofRows++);

	m_objSchedule.m_nStatus = 3;
	m_objSchedule.bValid = false;
	m_enmOperation = OP_None;

	SequenceCheck::getInstance()->SetImage(this);

	m_cLogManager.Init(this, e_MessageBase, e_PgUpMsg, e_PgDnMsg);
	int nFileCount = _setmaxstdio(2048);
	return true;
}

UINT AvroProcessorThread(void *pVoid)
{
	MDAvroProcessor *pParser = (MDAvroProcessor*)pVoid;
	pParser->m_bIsThreadRunning = true;
	pParser->Parse();
	pParser->m_bIsThreadRunning = false;
	pParser->RequestExternalUpdate();
	return 0;
}

UINT SecDefParserThread(void *pVoid)
{
	MDAvroProcessor *pParser = (MDAvroProcessor*)pVoid;
	pParser->m_bIsThreadRunning = true;
	pParser->ParseSecDef();
	pParser->m_bIsThreadRunning = false;
	pParser->RequestExternalUpdate();
	return 0;
}

UINT ZipCreatorThread(void *pVoid)
{
	MDAvroProcessor *pParser = (MDAvroProcessor*)pVoid;
	pParser->m_bIsThreadRunning = true;
	pParser->CreateZip();
	pParser->m_enmOperation = OP_UploadSubscription;
	pParser->m_bIsThreadRunning = false;
	pParser->RequestExternalUpdate();
	return 0;
}

UINT SyncS3Thread(void *pVoid)
{
	MDAvroProcessor *pParser = (MDAvroProcessor*)pVoid;
	pParser->m_bIsThreadRunning = true;
	pParser->CopyS3FiletoLocal();
	pParser->m_enmOperation = OP_ProcessRunning;
	pParser->m_bIsThreadRunning = false;
	pParser->RequestExternalUpdate();
	return 0;
}

UINT GenerateSubscriptionThread(void *pVoid)
{
	MDAvroProcessor *pParser = (MDAvroProcessor*)pVoid;
	pParser->m_bIsThreadRunning = true;
	pParser->GenerateSubscriptions();
	pParser->m_bIsThreadRunning = false;
	pParser->RequestExternalUpdate();
	return 0;
}

UINT UploadSubscriptionThread(void *pVoid)
{
	MDAvroProcessor *pParser = (MDAvroProcessor*)pVoid;
	pParser->m_bIsThreadRunning = true;
	if( pParser->m_nScreenID == Img_FixTextAuto )
		pParser->GenerateSubscriptions();
	pParser->UploadSubscription();
	pParser->m_enmOperation = OP_EndOfDay;
	pParser->m_bIsThreadRunning = false;
	pParser->RequestExternalUpdate();
	return 0;
}

UINT FtpDownLoadThread(void *pVoid)
{
	MDAvroProcessor *pParser = (MDAvroProcessor*)pVoid;
	pParser->m_bIsThreadRunning = true;
	pParser->FtpDownload();
	pParser->m_enmOperation = OP_SecDefGenerate;
	pParser->m_bIsThreadRunning = false;
	pParser->RequestExternalUpdate();
	return 0;
}

UINT SecDefGenerateThread(void *pVoid)
{
	MDAvroProcessor *pParser = (MDAvroProcessor*)pVoid;
	pParser->m_bIsThreadRunning = true;
	pParser->SecDefGenerate();
	pParser->fnUpdateScheduleStatus(1);
	pParser->m_enmOperation = OP_S3Sync;
	pParser->m_bIsThreadRunning = false;
	pParser->RequestExternalUpdate();
	return 0;
}

UINT DownLoadAndParseSecDef(void *pVoid)
{
	MDAvroProcessor *pParser = (MDAvroProcessor*)pVoid;
	pParser->m_bIsThreadRunning = true;
	if(pParser->FtpDownload())
	{
		pParser->m_cLogManager.fnAddMessage("Parsing SecDef File downloaded from FTP");
		pParser->RequestExternalUpdate();
		pParser->SecDefGenerate();
	}
	pParser->m_bIsThreadRunning = false;
	pParser->m_cLogManager.fnAddMessage("Parsing Complete");
	pParser->RequestExternalUpdate();
	return 0;
}

void MDAvroProcessor::Parse()
{
	MessageUtil::getInstance()->LogToFile("Avro Processing Started");
	m_cLogManager.fnAddMessage("Avro Processing Started");
	RequestExternalUpdate();

	avro::ValidSchema cpxSchema;

	try
	{
		cpxSchema = loadSchema("cme.json");
	}
	catch(exception e)
	{
		m_cLogManager.fnAddMessage(e.what());
		m_enmOperation = OP_NoFileToProcess;
		RequestExternalUpdate();

		MessageUtil::getInstance()->LogToFile("CME Schema Not Found");
		return;
	}

	string inputfolder = m_objSchedule.strInputFolder;
	string outputfolder = m_objSchedule.strOutputFolder;

	if(inputfolder.empty())
	{
		m_cLogManager.fnAddMessage("Invalid input path");
		m_enmOperation = OP_NoFileToProcess;
		RequestExternalUpdate();

		MessageUtil::getInstance()->LogToFile("Invalid input path");
		return;
	}

	if(outputfolder.empty())
	{
		m_cLogManager.fnAddMessage("Invalid output path");
		m_enmOperation = OP_NoFileToProcess;
		RequestExternalUpdate();
		MessageUtil::getInstance()->LogToFile("Invalid output path");
		return;
	}

	CString strMstSecDefFolder;
	strMstSecDefFolder.Format("%s\\MST\\", m_objSchedule.strSecDefFolder);

	if( !(boost::filesystem::exists(strMstSecDefFolder.GetString())))
		boost::filesystem::create_directories(strMstSecDefFolder.GetString());

	std::vector<string> vAvroFiles;
	if( !MessageUtil::getInstance()->GetFilesInDirectory(vAvroFiles, inputfolder, "*.avro"))
	{
		m_cLogManager.fnAddMessage("Avro files not found in input directory");
		m_enmOperation = OP_NoFileToProcess;
		RequestExternalUpdate();
		MessageUtil::getInstance()->LogToFile("Avro files not found in input directory");
		return;
	}

	if( !(boost::filesystem::exists(outputfolder)))
	{
		bool bSuccess = boost::filesystem::create_directories(outputfolder);
		if( !bSuccess )
		{
			m_cLogManager.fnAddMessage("Error in creating output directory");
			m_enmOperation = OP_NoFileToProcess;
			RequestExternalUpdate();
			MessageUtil::getInstance()->LogToFile("Error in creating output directory");
			return;
		}
	}

	CString strLog;
	strLog.Format("Avro process running, %d files in bucket", vAvroFiles.size());
	MessageUtil::getInstance()->LogToFile(strLog.GetString());

	MessageUtil::getInstance()->LoadProcessedFiles(outputfolder);

	// Start -----------------------------
	MDMessageConvertor objMDMessageConvertor;
	objMDMessageConvertor.setup(inputfolder, outputfolder, (LPCSTR)m_objSchedule.strContractFile, (LPCSTR)m_objSchedule.strMstSecDef);

	// Process -----------------------------
	long lLineCount;
	int nFilesProcessed(0);
	m_bIsStopThread = false;

	for(int iCount(0); iCount < vAvroFiles.size(); ++iCount)
	{
		if(MessageUtil::getInstance()->IsFileProcessed(vAvroFiles[iCount]))
			continue;

		avro::DataFileReader<c::Message> dfr(vAvroFiles[iCount].c_str(), cpxSchema);
		c::Message c2;
		string strmessage;

		lLineCount = 0;
		++nFilesProcessed;

		CString strlog;
		strlog.Format("Processing File (%d/%d)  %s", (iCount+1), vAvroFiles.size(), vAvroFiles[iCount].c_str());
		m_cLogManager.fnAddMessage(strlog);
		PrintRawMessage(strlog);
		RequestExternalUpdate();

		SequenceCheck::getInstance()->SetFile(vAvroFiles[iCount]);

		try
		{
			while (dfr.read(c2)) 
			{
				++lLineCount;

				if( ((lLineCount) % 25000) == 0 )
				{
					strlog.Format("Processing Line %d of (%d/%d)", lLineCount, (iCount+1), vAvroFiles.size());
					m_cLogManager.fnAddMessage(strlog);
					PrintRawMessage(strlog);
					RequestExternalUpdate();
				}

				strmessage = fnGetmdMessage( c2.mdMsg );
				objMDMessageConvertor.convertMessage(Message(strmessage.c_str(), strmessage.length(), atoi(c2.mdpChannelName.c_str())));

				if(lLineCount == 1)
				{
					CString str;
					str.Format("%s - %s", c2.mdpChannelName.c_str(), vAvroFiles[iCount].c_str());
					OutputDebugString(str);
				}
			}
		}
		catch(...)
		{
			CString strlog;
			strlog.Format("Error in Processing File %d - %s - Line %ld", (iCount+1), vAvroFiles[iCount].c_str(), lLineCount);
			m_cLogManager.fnAddMessage(strlog);
			MessageUtil::getInstance()->LogToFile(strlog.GetString());
			RequestExternalUpdate();
		}

		MessageUtil::getInstance()->SetProcessedFile(vAvroFiles[iCount]);

		if( ((iCount+1) % 50) == 0 )
			objMDMessageConvertor.close();

		if(m_bIsStopThread)
			break;
	}

	// Close -----------------------------
	objMDMessageConvertor.close();	

	m_cLogManager.fnAddMessage("Avro Processing completed");
	
	if(nFilesProcessed)
	{
		strLog.Format("Avro process complete, %d out of %d files processed", nFilesProcessed, vAvroFiles.size());
		m_enmOperation = OP_ProcessCompleted;
	}
	else
	{
		strLog.Format("Avro process complete, No new file found in this batch");
		m_enmOperation = OP_NoFileToProcess;	
	}

	MessageUtil::getInstance()->LogToFile(strLog.GetString());
}

void MDAvroProcessor::ParseSecDef()
{
	MessageUtil::getInstance()->LogToFile("SecDef Parse Processing Started");

	avro::ValidSchema cpxSchema;

	try
	{
		cpxSchema = loadSchema("cme.json");
	}
	catch(exception e)
	{
		m_cLogManager.fnAddMessage(e.what());
		m_enmOperation = OP_NoFileToProcess;
		RequestExternalUpdate();

		MessageUtil::getInstance()->LogToFile("CME Schema Not Found");
		return;
	}

	string inputfolder = m_objSchedule.strInputFolder;
	string outputfolder = m_objSchedule.strOutputFolder;

	if(inputfolder.empty())
	{
		m_cLogManager.fnAddMessage("Invalid input path");
		m_enmOperation = OP_NoFileToProcess;
		RequestExternalUpdate();

		MessageUtil::getInstance()->LogToFile("Invalid input path");
		return;
	}

	if(outputfolder.empty())
	{
		m_cLogManager.fnAddMessage("Invalid output path");
		m_enmOperation = OP_NoFileToProcess;
		RequestExternalUpdate();
		MessageUtil::getInstance()->LogToFile("Invalid output path");
		return;
	}

	std::vector<string> vAvroFiles;
	if( !MessageUtil::getInstance()->GetFilesInDirectory(vAvroFiles, inputfolder, "*.avro"))
	{
		m_cLogManager.fnAddMessage("Avro files not found in input directory");
		m_enmOperation = OP_NoFileToProcess;
		RequestExternalUpdate();
		MessageUtil::getInstance()->LogToFile("Avro files not found in input directory");
		return;
	}

	CString strLog;
	strLog.Format("SecDef Parse process running, %d files in bucket", vAvroFiles.size());
	MessageUtil::getInstance()->LogToFile(strLog.GetString());

	if( !(boost::filesystem::exists(outputfolder)))
	{
		bool bSuccess = boost::filesystem::create_directories(outputfolder);
		if( !bSuccess )
		{
			m_cLogManager.fnAddMessage("Error in creating output directory");
			m_enmOperation = OP_NoFileToProcess;
			RequestExternalUpdate();
			MessageUtil::getInstance()->LogToFile("Error in creating output directory");
			return;
		}
	}

	// Start -----------------------------
	SecDefMessageConvertor objSecDefMessageConvertor;
	objSecDefMessageConvertor.setup((LPCSTR)m_objSchedule.strContractFile);


	// Process -----------------------------
	long lLineCount;
	m_bIsStopThread = false;

	for(int iCount(0); iCount < vAvroFiles.size(); ++iCount)
	{
		avro::DataFileReader<c::Message> dfr(vAvroFiles[iCount].c_str(), cpxSchema);
		c::Message c2;
		string strmessage;

		lLineCount = 0;

		CString strlog;
		strlog.Format("SecDefParser: Processing File (%d/%d)  %s", (iCount+1), vAvroFiles.size(), vAvroFiles[iCount].c_str());
		m_cLogManager.fnAddMessage(strlog);
		PrintRawMessage(strlog);
		RequestExternalUpdate();

		try
		{
			while (dfr.read(c2)) 
			{
				++lLineCount;

				if( ((lLineCount) % 10000) == 0 )
				{
					strlog.Format("SecDefParser: Processing Line %d of (%d/%d)", lLineCount, (iCount+1), vAvroFiles.size());
					m_cLogManager.fnAddMessage(strlog);
					PrintRawMessage(strlog);
					RequestExternalUpdate();
				}

				strmessage = fnGetmdMessage( c2.mdMsg );
				objSecDefMessageConvertor.convertInstrumentDef(Message(strmessage.c_str(), strmessage.length(), atoi(c2.mdpChannelName.c_str())));
			}
		}
		catch(...)
		{
			CString strlog;
			strlog.Format("SecDefParser: Error in Processing File %d - %s - Line %ld", (iCount+1), vAvroFiles[iCount].c_str(), lLineCount);
			m_cLogManager.fnAddMessage(strlog);
			MessageUtil::getInstance()->LogToFile(strlog.GetString());
			RequestExternalUpdate();
		}

		if( ((iCount+1) % 50) == 0 )
			objSecDefMessageConvertor.close();

		if(m_bIsStopThread)
			break;
	}

	// Close -----------------------------
	objSecDefMessageConvertor.close();	

	m_cLogManager.fnAddMessage("Process completed");
	strLog.Format("SecDef Parse process complete, %d files processed", vAvroFiles.size());
	MessageUtil::getInstance()->LogToFile(strLog.GetString());
}


void MDAvroProcessor::Recalculate()
{
	if( m_nScreenID == Img_FixText )
	{
		if( !m_bIsThreadRunning )
		{
			if(IsFieldNew(e_StartProcess))
			{
				SequenceCheck::getInstance()->Clear();
				AfxBeginThread(AvroProcessorThread, this);
			}

			if(IsFieldNew(e_SecDefParse))
			{
				AfxBeginThread(SecDefParserThread, this);
			}
		}

		if(IsFieldNew(e_DefaultDate))
		{
			BFDate bfdt = GetField(e_DefaultDate)->GetValueDate();
			m_objSchedule.strProcessingDate.Format("%d%02d%02d", bfdt.GetYear(), bfdt.GetMonth()+1, bfdt.GetDate());
			GenerateScheduleInfo();
		}

		if(IsFieldNew(e_GetSecDef))
		{
			m_cLogManager.fnAddMessage("Downloading SecDef File from FTP");
			AfxBeginThread(DownLoadAndParseSecDef, this);
		}

		if(IsFieldNew(e_CopyFromS3))
		{
			AfxBeginThread(SyncS3Thread, this);
		}

		if(IsFieldNew(e_CreateZip))
		{
			AfxBeginThread(ZipCreatorThread, this);
		}

		if(IsFieldNew(e_RemoveFiles))
		{
			string outputfolder = GetField(e_OutputFolder)->GetValueString();

			if(outputfolder.empty())
			{
				m_cLogManager.fnAddMessage("Invalid output path");
				return;
			}

			std::vector<string> vOutFiles;
			if( !MessageUtil::getInstance()->GetFilesInDirectory(vOutFiles, outputfolder, "*."))
			{
				m_cLogManager.fnAddMessage("Processed files not found in output directory");
				return;
			}

			CString strCmd, strIn;
			for(int iCount(0); iCount < vOutFiles.size(); ++iCount)
			{
				strIn = vOutFiles[iCount].c_str();

				BOOL iValue= DeleteFile(strIn);

				if( iValue == 0 )
				{
					strCmd.Format("Delete file for %s failed", strIn);
					m_cLogManager.fnAddMessage(strCmd);
				}
			}
		}

		if(IsFieldNew(e_LoadSub))
		{
			LoadSubscription();
		}

		if( IsFieldNew( e_UploadALL ) )
		{
			AfxBeginThread(UploadSubscriptionThread, this);
		}

		if( IsFieldNew( e_GenerateSub ) )
		{
			AfxBeginThread(GenerateSubscriptionThread, this);
		}

		for(int iIndex = 0; iIndex < m_numofRows; iIndex++)
		{
			if( IsFieldNew( e_Upload + iIndex ) )
			{
				SubscriptionV::iterator itrSub = m_vSubscriptions.begin();
				advance(itrSub, (iIndex + m_curTopRow));
				UploadSubscription(itrSub->stDEST_ACCOUNT);
			}
		}
	}

	if( IsFieldNew(e_StopProcess) )
	{
		m_bIsStopThread = true;
	}

	if( IsFieldNew(e_ClearGrid) )
	{
		SequenceCheck::getInstance()->Clear();
	}

	if( IsFieldNew(e_GenReport) )
	{
		SequenceCheck::getInstance()->GenerateReport();
	}
	
	fnPageUpDown();

	// ---------------------------------------------------------------
	// ----------------- Automatic Execution - Start -----------------
	// ---------------------------------------------------------------


	if( m_nScreenID == Img_FixTextAuto && !m_bIsThreadRunning )
	{
		CString strStatus("");

		if(IsFieldNew(e_StartProcess))
		{
			m_objSchedule.bValid = false;
		}

		if( m_enmOperation == OP_SecDefGenerate )
		{
			strStatus = "Parsing SecDef file downloaded from FTP";
			GetField(e_DownloadMsg)->SetTitle(strStatus);
			m_cLogManager.fnAddMessage(strStatus);

			AfxBeginThread(SecDefGenerateThread, this);
		}

		if( m_enmOperation == OP_FTPDownload )
		{
#ifdef _DEBUG
			m_csFtpSecDef = "D:\\Beast Projects\\CME\\20150830_Output\\secdef.dat";
			m_enmOperation = OP_SecDefGenerate;
#else
			strStatus = "Downloading SecDef file from FTP";
			GetField(e_DownloadMsg)->SetTitle(strStatus);
			m_cLogManager.fnAddMessage(strStatus);

			AfxBeginThread(FtpDownLoadThread, this);
#endif
		}

		if( m_enmOperation == OP_EndOfDay )
		{
			strStatus = "Complete processing finished for the day";
			GetField(e_DownloadMsg)->SetTitle(strStatus);
			m_cLogManager.fnAddMessage(strStatus);

			fnUpdateScheduleStatus(2);
			fnSendEmail();
			m_enmOperation = OP_None;
			m_objSchedule.bValid = false;
			StartRecalcTimer(0, false);
		}

		if( m_enmOperation == OP_UploadSubscription )
		{
			strStatus = "Uploading subcriptions";
			GetField(e_DownloadMsg)->SetTitle(strStatus);
			m_cLogManager.fnAddMessage(strStatus);

			AfxBeginThread(UploadSubscriptionThread, this);
			StartRecalcTimer(0, false);
		}

		if( m_enmOperation == OP_CreateZip )
		{
			strStatus = "Creating .gz files";
			GetField(e_DownloadMsg)->SetTitle(strStatus);
			m_cLogManager.fnAddMessage(strStatus);

			AfxBeginThread(ZipCreatorThread, this);
			StartRecalcTimer(0, false);
		}

		if( m_enmOperation == OP_ProcessCompleted )
		{
			strStatus = "Processing of current batch finished";
			GetField(e_DownloadMsg)->SetTitle(strStatus);
			m_cLogManager.fnAddMessage(strStatus);

			m_enmOperation = OP_S3Sync;
			StartRecalcTimer(0, false);
		}

		if( m_enmOperation == OP_ProcessRunning )
		{
			std::vector<string> vAvroFiles;
			MessageUtil::getInstance()->GetFilesInDirectory(vAvroFiles, m_objSchedule.strInputFolder.GetString(), "*.avro");
			int nNewFiles(0);
			for(int iCount(0); iCount < vAvroFiles.size(); ++iCount)
			{
				if(MessageUtil::getInstance()->IsFileProcessed(vAvroFiles[iCount]))
					continue;
			
				++nNewFiles;
			}

			if( nNewFiles )
			{
				strStatus = "Processing of current batch";
				GetField(e_DownloadMsg)->SetTitle(strStatus);
				m_cLogManager.fnAddMessage(strStatus);

				fnUpdateScheduleStatus(1);

				AfxBeginThread(AvroProcessorThread, this);
			}
			else
			{
				m_enmOperation = OP_NoFileToProcess;
			}
		}

		if( m_enmOperation == OP_S3Sync )
		{
			strStatus = "Sync bucket";
			GetField(e_DownloadMsg)->SetTitle(strStatus);
			m_cLogManager.fnAddMessage(strStatus);

#ifndef _DEBUG 
			AfxBeginThread(SyncS3Thread, this);
#else
			m_enmOperation = OP_ProcessRunning;
#endif
			//StartRecalcTimer(0, false);
		}

		if( m_enmOperation == OP_NoFileToProcess )
		{
			strStatus = "No file found for processing";
			GetField(e_DownloadMsg)->SetTitle(strStatus);
			m_cLogManager.fnAddMessage(strStatus);

			COleDateTime dtCurrent = COleDateTime::GetCurrentTime();

			int nHour = GetField(e_CutOffHour)->GetValueInt();
			int nMinute = GetField(e_CutOffMin)->GetValueInt();
			
			COleDateTime dtCutOff;
			dtCutOff.SetDateTime(m_objSchedule.dtCurrSchTime.GetYear(), m_objSchedule.dtCurrSchTime.GetMonth(), m_objSchedule.dtCurrSchTime.GetDay(), nHour, nMinute, 0);

			if( dtCurrent > dtCutOff)
			{
				strStatus = "Processing finished for the day, Creating Zip";
				GetField(e_DownloadMsg)->SetTitle(strStatus);
				m_cLogManager.fnAddMessage(strStatus);

				LoadSubscription();
				m_enmOperation = OP_CreateZip;
				//StartRecalcTimer(0, false);
			}
			else 
			{
				m_enmOperation = OP_None;
				m_objSchedule.dtCurrSchTime = dtCurrent;

				int nRetryInterval = GetField(e_RetryInterval)->GetValueInt();
				int nCurrMin = m_objSchedule.dtCurrSchTime.GetMinute();
				
				if( nCurrMin >= 5 && nCurrMin < 30 )
					nRetryInterval = (30 - nCurrMin) * 60;

				m_objSchedule.dtCurrSchTime += COleDateTimeSpan(0,0,0,nRetryInterval);
			}
		}

		int nRetryInterval = 5;

		if( m_enmOperation == OP_None )
		{
			if( !m_objSchedule.bValid )
			{
				strStatus = "Loading next schedule";
				GetField(e_DownloadMsg)->SetTitle(strStatus);
				m_cLogManager.fnAddMessage(strStatus);

				GetField(e_DownloadMsg)->SetTitleForeColor(ColorManager::eSignalPositiveLight);
				GetScheduleStatus();

				if( !m_objSchedule.bValid )
				{
					strStatus = "No schedule found";
					GetField(e_DownloadMsg)->SetTitle(strStatus);
					m_cLogManager.fnAddMessage(strStatus);
					nRetryInterval = 600;
				}
			}
			else
			{
				COleDateTime dtCurrent = COleDateTime::GetCurrentTime();

				if(  dtCurrent >= m_objSchedule.dtCurrSchTime )
				{
					if(m_objSchedule.dtCurrSchTime.GetDayOfWeek() == 1 && m_objSchedule.m_nStatus == 0 )
						m_enmOperation = OP_FTPDownload;
					else
						m_enmOperation = OP_S3Sync;
				
					SequenceCheck::getInstance()->Clear();
					nRetryInterval = 0;
				}
				else
				{
					nRetryInterval = 5;
					CString csTimer,csTmp;
					COleDateTimeSpan dtSpan = m_objSchedule.dtCurrSchTime - dtCurrent;

					int iDays =	dtSpan.GetDays();
					int iLeftTotalHr = dtSpan.GetHours();
					int iLeftTotalMinute = dtSpan.GetMinutes();
					int iLeftTotalSecond = dtSpan.GetSeconds();

					if( iDays > 0 )
					{
						csTmp.Format("%d Day and ",iDays);					
						csTimer += csTmp;

						csTmp.Format("%02d:",iLeftTotalHr);					
						csTimer += csTmp;
					}
					else
					{
						if( iLeftTotalHr > 0 )
						{					
							csTmp.Format("%02d:",iLeftTotalHr);					
							csTimer += csTmp;
						}
					}

					csTmp.Format("%02d:",iLeftTotalMinute);					
					csTimer += csTmp;

					csTmp.Format("%02d",iLeftTotalSecond);
					csTimer += csTmp;			

					CString csStr;
					csStr.Format("Next Download: %02d-%02d-%d %02d:%02d:%02d [Timer: %s]", m_objSchedule.dtCurrSchTime.GetDay(),
						m_objSchedule.dtCurrSchTime.GetMonth(), m_objSchedule.dtCurrSchTime.GetYear(),
						m_objSchedule.dtCurrSchTime.GetHour(), m_objSchedule.dtCurrSchTime.GetMinute(),
						m_objSchedule.dtCurrSchTime.GetSecond(), csTimer);

					GetField(e_DownloadMsg)->SetTitle(csStr);
				}	
			}
		}
		else
		{
			nRetryInterval = 0;
		}

		StartRecalcTimer(nRetryInterval, false);
	}


	// -------------------------------------------------------------
	// ----------------- Automatic Execution - End -----------------
	// -------------------------------------------------------------

	m_cLogManager.fnPageUpDown();
	m_cLogManager.fnDisplayMessages();
}

void MDAvroProcessor::GenerateScheduleInfo()
{
	int nYear = atoi(m_objSchedule.strProcessingDate.Left(4));
	int nMonth = atoi(m_objSchedule.strProcessingDate.Mid(4,2));
	int nDate = atoi(m_objSchedule.strProcessingDate.Right(2));
	COleDateTime dtSecDef(nYear, nMonth, nDate, 0, 0, 0);
	int nDay = dtSecDef.GetDayOfWeek() - 1;
	dtSecDef += COleDateTimeSpan( -nDay, 0, 0, 0 );

	CString m_csProductPath;
	m_csProductPath = GetField(e_BasePath)->GetValueString();

#ifdef _DEBUG
	m_csProductPath = "D:\\Beast Projects\\CME\\";
#endif

	m_objSchedule.strInputFolder.Format("%s%s", m_csProductPath, m_objSchedule.strProcessingDate);

	if( m_nScreenID == Img_FixText || m_nScreenID == Img_FixTextAuto )
	{
		m_objSchedule.strOutputFolder.Format("%s%s_Output", m_csProductPath, m_objSchedule.strProcessingDate);	
		m_objSchedule.strSecDefFolder.Format("%s%04d%02d%02d_Output", m_csProductPath, dtSecDef.GetYear(), dtSecDef.GetMonth(), dtSecDef.GetDay());
	}
	else
	{
		m_objSchedule.strOutputFolder.Format("%s%s_Output_Binary", m_csProductPath, m_objSchedule.strProcessingDate);	
		m_objSchedule.strSecDefFolder.Format("%s%04d%02d%02d_Output_Binary", m_csProductPath, dtSecDef.GetYear(), dtSecDef.GetMonth(), dtSecDef.GetDay());
	}

	m_objSchedule.strContractFile.Format("%s\\_SecDef.secdef", m_objSchedule.strSecDefFolder);

	if( m_nScreenID == Img_FixText )
		MessageUtil::getInstance()->strLogFile.Format("%s\\_InternalLogs_Manual.log", m_objSchedule.strOutputFolder);
	else if( m_nScreenID == Img_FixTextAuto )
		MessageUtil::getInstance()->strLogFile.Format("%s\\_InternalLogs.log", m_objSchedule.strOutputFolder);
	else if( m_nScreenID == Img_FixBinary )
		MessageUtil::getInstance()->strLogFile.Format("%s\\_InternalLogs_Binary.log", m_objSchedule.strOutputFolder);
		
	m_objSchedule.strMstSecDef.Format("%s\\MST\\secdef.dat", m_objSchedule.strSecDefFolder);

	GetField(e_InputFolder)->SetValueString(m_objSchedule.strInputFolder);
	GetField(e_OutputFolder)->SetValueString(m_objSchedule.strOutputFolder);
}

void MDAvroProcessor::GetScheduleStatus()
{
	m_objSchedule.bValid = false;
	m_enmOperation = OP_None;

	BFDate bfdt = GetField(e_DefaultDate)->GetValueDate();
	CString csStart;
	csStart.Format("%d-%02d-%02d 00:00:00", bfdt.GetYear(), bfdt.GetMonth()+1, bfdt.GetDate());

	CString sql;
	sql.Format("SELECT TOP 1 * from dbo.CME_Subscription_Schedule WHERE FileType ='MD' and SubType = 'FText' and IsNull(Status,0) In(0,1) and ScheduleDateTime > '%s' ORDER BY ScheduleDateTime", csStart);

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		return;
	}

	if( VARIANT_FALSE == set->adoEOF )
	{
		SETLONG( m_objSchedule.m_nStatus,			set->Fields->Item[_variant_t("Status")]->Value);	
		SETOLEDATE(	m_objSchedule.dtOrigSchTime,	set->Fields->Item[_variant_t("ScheduleDateTime")]->Value);	

#ifdef _DEBUG

		static int count = 0;
		if(count == 0)
			m_objSchedule.dtOrigSchTime.SetDateTime(2015,10,9,1,0,0);
		else
			m_objSchedule.dtOrigSchTime.SetDateTime(2015,10,25,1,0,0);
		
		count++;

#endif

		m_objSchedule.bValid = true;
		m_objSchedule.dtCurrSchTime = m_objSchedule.dtOrigSchTime;
		m_objSchedule.strProcessingDate = m_objSchedule.dtCurrSchTime.Format(_T("%Y%m%d"));
		GenerateScheduleInfo();

		CString strLog;
		strLog.Format("Get Next Schedule For: %s", m_objSchedule.strProcessingDate);
		MessageUtil::getInstance()->LogToFile(strLog.GetString());
	}

	set->Close();
}

unsigned int __stdcall ZipWorkerThread(void *pVoid)
{
	MDAvroProcessor *pParser = (MDAvroProcessor*)pVoid;
	pParser->MultiThread_Zip();
	return 0;
}

void MDAvroProcessor::MultiThread_Zip()
{
	CString strCmd, strIn, strOut;
	int iCount(0);

	do
	{
		CSingleLock lock(&gCriticalSection);
		lock.Lock();

		iCount = getNextFile();

		if( iCount >= 0 )
		{
			CString strlog;
			strlog.Format("Zip Creator: Processing File (%d/%d)  %s", (iCount+1), m_vTextFiles.size(), m_vTextFiles[iCount].strFilePath.c_str());
			m_cLogManager.fnAddMessage(strlog);
			RequestExternalUpdate();
		}

		lock.Unlock();

		if( iCount >= 0 )
		{
			strIn = m_vTextFiles[iCount].strFilePath.c_str();
			strOut = strIn + ".gz";

			if( !(boost::filesystem::exists(strOut.GetString())) )
			{
				strCmd.Format("gzip.exe -c \"%s\" > \"%s\"", strIn, strOut);
				int iValue = system(strCmd);
				if( iValue != 0 )
				{
					strCmd.Format("Zip conversion of %s failed", strOut);
					m_cLogManager.fnAddMessage(strCmd);
					MessageUtil::getInstance()->LogToFile((LPCSTR)strCmd);
				}
			}
		}

	}while(iCount >= 0);
}

int MDAvroProcessor::getNextFile()
{
	int nResult(-1);
	
	if(m_iCurrentFile != -1)
	{
		nResult = m_iCurrentFile;
		m_iCurrentFile++;
		if( m_iCurrentFile >= m_nTotalFiles )
			m_iCurrentFile = -1;
	}

	return nResult;
}

bool Sort_File(stFile &lhs, stFile &rhs)
{
	return ( lhs.lFileSize > rhs.lFileSize );
}

void MDAvroProcessor::CreateZip()
{
	MessageUtil::getInstance()->LogToFile("Create Zip Files Started");

	if(m_objSchedule.strOutputFolder.IsEmpty())
	{
		m_cLogManager.fnAddMessage("Invalid output path");
		MessageUtil::getInstance()->LogToFile("Invalid output path.");
		return;
	}

	CString strCmd, strIn, strOut;

	// Creating Zip For Master Security Definition
	strIn = m_objSchedule.strMstSecDef;
	strOut.Format("%s\\MST\\secdef.dat_%s.gz", m_objSchedule.strSecDefFolder, m_objSchedule.strProcessingDate);	
		
	if( !(boost::filesystem::exists(strOut.GetString())))
	{
		strCmd.Format("gzip.exe -c \"%s\" > \"%s\"", strIn, strOut);

		int iValue = system(strCmd);
		if( iValue != 0 )
		{
			strCmd.Format("Master Secdef Zip conversion of %s failed", strOut);
			m_cLogManager.fnAddMessage(strCmd);
			MessageUtil::getInstance()->LogToFile((LPCSTR)strCmd);
		}
	}

	m_vTextFiles.clear();

	if( !MessageUtil::getInstance()->GetFilesInDirectory(m_vTextFiles, m_objSchedule.strOutputFolder.GetString(), "*."))
	{
		m_cLogManager.fnAddMessage("Processed files not found in output directory");
		MessageUtil::getInstance()->LogToFile("Processed files not found in output directory");
		return;
	}

	sort(m_vTextFiles.begin(), m_vTextFiles.end(), Sort_File);

	int nTotalThreads;
	BF_Reg_Key_Ptr pKey1(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\CME"));
	RegDWORD strCPUCore(pKey1, "MDCPUCore", 5);
	nTotalThreads = strCPUCore.get_value();

	HANDLE *handle = new HANDLE[nTotalThreads];

	m_iCurrentFile = 0;
	m_nTotalFiles = m_vTextFiles.size();

	for(int iCount(0); iCount < nTotalThreads; ++iCount)
	{
		handle[iCount] = (HANDLE)_beginthreadex(0, 0, &ZipWorkerThread, this, 0, 0);
	}

	WaitForMultipleObjects(nTotalThreads, handle, true, INFINITE);

	for(int iCount(0); iCount < nTotalThreads; ++iCount)
		CloseHandle(handle[iCount]);

	m_cLogManager.fnAddMessage("Zip creation completed");
	MessageUtil::getInstance()->LogToFile("Zip creation completed");
	RequestExternalUpdate();
}

bool Sort_Acc(SubscriptionDetail &lhs, SubscriptionDetail &rhs)
{
	if( lhs.stDEST_ACCOUNT.CompareNoCase(rhs.stDEST_ACCOUNT) == 0)
	{
		if( lhs.stALL_PRODUCTS.CompareNoCase(rhs.stALL_PRODUCTS) == 0)
		{
			if( lhs.stEXCH_CODE.CompareNoCase(rhs.stEXCH_CODE) == 0)
			{
				if( lhs.stPROD_CODE.CompareNoCase(rhs.stPROD_CODE) == 0)
				{
					return ( lhs.stFOICode < rhs.stFOICode );				
				}
				return ( lhs.stPROD_CODE < rhs.stPROD_CODE );
			}
			else
				return ( lhs.stEXCH_CODE < rhs.stEXCH_CODE );
		}
		else
			return ( lhs.stALL_PRODUCTS > rhs.stALL_PRODUCTS );
	}
	else
		return ( lhs.stDEST_ACCOUNT < rhs.stDEST_ACCOUNT );
}

void MDAvroProcessor::LoadSubscription()
{
	m_vSubscriptions.clear(); 
	m_SubAccountMap.clear(); 

	CString sql;	
	sql.Format("SELECT CUST_NAME, CONTACT_EMAIL, DEST_ACCOUNT, FTP_PASSWORD, ALL_PRODUCTS, PROD_CODE, MD_CODE, EXCH_CODE, FOI_CODE FROM dbo.subscription_2015 where FILE_TYPE = 'MD' and Active = 1");

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		m_cLogManager.fnAddMessage(sql);
		return;
	}

	int iCount(1);
	while(VARIANT_FALSE == set->adoEOF)
	{
		SubscriptionDetail clEODSub;

		clEODSub.iOrderId = 0;

		SETSTR(	clEODSub.stCUST_NAME,		set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
		SETSTR(	clEODSub.stDEST_ACCOUNT,	set->Fields->Item[_variant_t("DEST_ACCOUNT")]->Value);	
		SETSTR(	clEODSub.stFTP_PASSWORD,	set->Fields->Item[_variant_t("FTP_PASSWORD")]->Value);	
		SETSTR(	clEODSub.stALL_PRODUCTS,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);	

		SETSTR(	clEODSub.stPROD_CODE,	set->Fields->Item[_variant_t("PROD_CODE")]->Value);	
		SETSTR(	clEODSub.stEXCH_CODE,	set->Fields->Item[_variant_t("EXCH_CODE")]->Value);
		SETSTR(	clEODSub.stFOICode,		set->Fields->Item[_variant_t("FOI_CODE")]->Value);

		if( clEODSub.stPROD_CODE.CompareNoCase("null") == 0 )
			clEODSub.stPROD_CODE.Empty();

		if(clEODSub.stALL_PRODUCTS.CompareNoCase("0") == 0)
		{
			if( clEODSub.stFOICode.CompareNoCase("OPT") == 0 )
			{
				m_vSubscriptions.push_back( clEODSub );
				clEODSub.stFOICode = "OPT_SPD";
				m_vSubscriptions.push_back( clEODSub );
			}
			else if( clEODSub.stFOICode.CompareNoCase("FUT") == 0 )
			{
				m_vSubscriptions.push_back( clEODSub );
				clEODSub.stFOICode = "FUT_SPD";
				m_vSubscriptions.push_back( clEODSub );
			}
		}
		else
		{
			clEODSub.stFOICode = "";
			m_vSubscriptions.push_back( clEODSub );
		}

		iCount++;
		set->MoveNext();
	}

	set->Close();	

	sql.Format("EXEC Proc_Beast_Get_Subscriptions 'MD', 2");
	
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		return;
	}

	CString csFormat;
	int nFosType;

	while( VARIANT_FALSE == set->adoEOF )
	{
		SubscriptionDetail clEODSub;
		
		SETLONG( clEODSub.iOrderId,			set->Fields->Item[_variant_t("ORDER_ID")]->Value);
		SETSTR(	clEODSub.stCUST_NAME,		set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
		SETSTR(	clEODSub.stDEST_ACCOUNT,	set->Fields->Item[_variant_t("FTPUserName")]->Value);	
		SETSTR(	clEODSub.stFTP_PASSWORD,	set->Fields->Item[_variant_t("FTPPassword")]->Value);
		SETSTR(	clEODSub.stALL_PRODUCTS,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);	
		SETSTR(	clEODSub.stPROD_CODE,		set->Fields->Item[_variant_t("PROD_CODE")]->Value);	
		SETSTR(	clEODSub.stEXCH_CODE,		set->Fields->Item[_variant_t("Exchange")]->Value);
		SETLONG( nFosType,					set->Fields->Item[_variant_t("FOS_Type")]->Value);

		SETSTR(	csFormat,	set->Fields->Item[_variant_t("File_Format")]->Value);
		
		if( csFormat == "Text")
		{
			if( !clEODSub.stEXCH_CODE.IsEmpty() )
			{
				if( clEODSub.stPROD_CODE.CompareNoCase("null") == 0 )
					clEODSub.stPROD_CODE.Empty();

				bool isAll = clEODSub.stALL_PRODUCTS.CompareNoCase("1") == 0;
				bool isEmpty = clEODSub.stPROD_CODE.IsEmpty();

				if( isAll )
				{
					clEODSub.stFOICode = "";
					m_vSubscriptions.push_back( clEODSub );
				}
				else if( !isEmpty )
				{
					if( nFosType & 2 )
					{
						clEODSub.stFOICode = "FUT";
						m_vSubscriptions.push_back( clEODSub );
					}

					if( nFosType & 4 )
					{
						clEODSub.stFOICode = "OPT";
						m_vSubscriptions.push_back( clEODSub );
					}

					if( nFosType & 8 )
					{
						clEODSub.stFOICode = "FUT_SPD";
						m_vSubscriptions.push_back( clEODSub );
					}

					if( nFosType & 16 )
					{
						clEODSub.stFOICode = "OPT_SPD";
						m_vSubscriptions.push_back( clEODSub );
					}
				}
			}
		}

		set->MoveNext();
	}

	set->Close();

	sort(m_vSubscriptions.begin(), m_vSubscriptions.end(), Sort_Acc);

	CString csTemp;
	csTemp.Format("%d Subscription loaded", m_vSubscriptions.size(), Sort_Acc);
	m_cLogManager.fnAddMessage(csTemp);

	DisplaySubscription();
}

void MDAvroProcessor::fnPageUpDown( )
{
	int iTotalRecord = m_vSubscriptions.size( );

	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpBase,e_PgDnBase,iTotalRecord,m_numofRows,m_curTopRow);	

	if( IsFieldNew(e_PgUpBase) || IsFieldNew(e_PgDnBase) )
		DisplaySubscription();
}

void MDAvroProcessor::DisplaySubscription()
{	
	int iRow = 0;
	CString strTemp;

	for(int iIndex = m_curTopRow; iIndex < m_vSubscriptions.size(); iIndex++, iRow++)
	{
		if( iRow >= m_numofRows )
			break;

		SubscriptionDetail &clEODSub = m_vSubscriptions[iIndex];

		GetField(e_SubscrID		+ iRow)->SetValueInt(clEODSub.iOrderId);  
		GetField(e_CUST_NAME	+ iRow)->SetValueString(clEODSub.stCUST_NAME);  
		GetField(e_FTP_Account + iRow)->SetValueString(clEODSub.stDEST_ACCOUNT);  
		GetField(e_VENUE_CODE	+ iRow)->SetValueString(clEODSub.stFOICode);
		GetField(e_ALL_PRODUCTS + iRow)->SetValueString(clEODSub.stALL_PRODUCTS);
		GetField(e_PROD_CODE	+ iRow)->SetValueString(clEODSub.stPROD_CODE);
		GetField(e_EXCH_CODE	+ iRow)->SetValueString(clEODSub.stEXCH_CODE);
		GetField(e_OutputFile + iRow)->SetValueString(clEODSub.stOutputFileName);	

		std::string strdestAct = clEODSub.stDEST_ACCOUNT.GetString();

		SubAccountMap::iterator itrAccount =  m_SubAccountMap.find( clEODSub.stDEST_ACCOUNT.GetString() );

		if( itrAccount != m_SubAccountMap.end() )
		{
			int nFileCount(0);

			if( clEODSub.stALL_PRODUCTS == "1" )
			{
				for(int kCount(0); kCount < itrAccount->second.vReportFiles.size(); kCount++)
				{
					ReportFile &objReportFile = itrAccount->second.vReportFiles[kCount];

					if( objReportFile.csExchange.CompareNoCase(clEODSub.stEXCH_CODE) == 0 )
						++nFileCount;
				}
			}
			else
			{
				for(int kCount(0); kCount < itrAccount->second.vReportFiles.size(); kCount++)
				{
					ReportFile &objReportFile = itrAccount->second.vReportFiles[kCount];

					CString csSubFile;
					csSubFile.Format("%s_MD_%s_%s_%s", clEODSub.stEXCH_CODE, clEODSub.stPROD_CODE, clEODSub.stFOICode, m_objSchedule.strProcessingDate);

					if( objReportFile.csFileName.Find(csSubFile) >= 0 )
						++nFileCount;
				}
			}

			strTemp.Format("%d/%d files", nFileCount, itrAccount->second.vReportFiles.size());
			GetField(e_OutputFile + iRow)->SetValueString( strTemp );

			if( itrAccount->second.iStatus == 999 )
			{
				GetField(e_Upload + iRow)->SetEnabled( true );
				GetField(e_OutputFile + iRow)->SetBackColor(ColorManager::eDefault);
			}
			else
			{
				GetField(e_Upload + iRow)->SetEnabled( false );
				GetField(e_OutputFile + iRow)->SetBackColor(ColorManager::eSignalPositiveDark);
			}
		}
		else
		{
			GetField(e_OutputFile + iRow)->SetBackColor(ColorManager::eDefault);
			GetField(e_OutputFile + iRow)->SetValueString("");
			GetField(e_Upload + iRow)->SetEnabled( false );
		}
	}

	while( iRow < m_numofRows )
	{
		GetField(e_SubscrID		+ iRow)->SetBlankState();  
		GetField(e_CUST_NAME	+ iRow)->SetBlankState();  
		GetField(e_FTP_Account + iRow)->SetBlankState();  
		GetField(e_VENUE_CODE	+ iRow)->SetBlankState();  
		GetField(e_ALL_PRODUCTS + iRow)->SetBlankState();  
		GetField(e_PROD_CODE	+ iRow)->SetBlankState();  
		GetField(e_EXCH_CODE	+ iRow)->SetBlankState();  
		GetField(e_OutputFile + iRow)->SetBlankState();  
		GetField(e_OutputFile + iRow)->SetBackColor(ColorManager::eDefault);

		GetField(e_Upload + iRow)->SetEnabled( false );
		iRow++;
	}
}

unsigned int __stdcall UploadWorkerThread(void *pVoid)
{
	MDAvroProcessor *pParser = (MDAvroProcessor*)pVoid;
	pParser->MultiThread_Upload("");
	return 0;
}

void MDAvroProcessor::MultiThread_Upload(CString csAccount)
{
	CString __csDestPath = ((ListField*)GetField(e_S3FTPBucket))->GetShortString();
	CString __csAccount = ((ListField*)GetField(e_DestAccount))->GetShortString();

	int nSuccess = 0;
	int iCount(0);

#ifdef _DEBUG 
	nSuccess = 1;
#endif

	do
	{
		CSingleLock lock(&gCriticalSection);
		lock.Lock();

		iCount = getNextFile();

		lock.Unlock();

		if( iCount == -1 || iCount >= m_SubAccountMap.size() )
			return;

		SubAccountMap::iterator itrAccount = m_SubAccountMap.begin();
		advance(itrAccount, iCount);

		if( !csAccount.IsEmpty() )
		{
			if( csAccount.CompareNoCase(itrAccount->second.strDestAccount) != 0 )
				continue;
		}

		if(iCount >= 0)
		{
			lock.Lock();

			CString strLog;
			strLog.Format("Uploading Subcriptions for customer %d/%d (%s)", (iCount+1), m_SubAccountMap.size(), itrAccount->first);
			m_cLogManager.fnAddMessage(strLog);
			RequestExternalUpdate();
			
			lock.Unlock();
		}

		CString csDestFolder;
		if( GetField(e_DestAccount)->GetValueInt() == 1 )
			csDestFolder.Format("%s%s/",__csDestPath,  itrAccount->second.strDestAccount);
		else
			csDestFolder.Format("%s%s/",__csDestPath,  __csAccount);

		// Upload Security Definition
		if(true)
		{
			CString csSourcePath;
			csSourcePath.Format("%s\\MST\\secdef.dat_%s.gz", m_objSchedule.strSecDefFolder, m_objSchedule.strProcessingDate);	

			/*CString strLog;
			strLog.Format("Uploading Security Definition for customer %d/%d (%s)", nCurrent, nTotalAccounts, itrAccount->first);
			m_cLogManager.fnAddMessage(strLog);
			RequestExternalUpdate();*/

			CString csAWSCommand;
			csAWSCommand.Format(_T("aws s3 cp %s --profile s3user \"%s\""), csSourcePath, csDestFolder);

			int iStatus;
			for( int iTry(0); iTry < 2; ++iTry)
			{
				iStatus = system(csAWSCommand);	

				if( iStatus == nSuccess )
					break;
			}

			if( iStatus != nSuccess )
			{
				lock.Lock();

				m_cLogManager.fnAddMessage(csAWSCommand);
				MessageUtil::getInstance()->LogToFile((LPCSTR)csAWSCommand); 
				fnInsertErrorReport(itrAccount->first, csSourcePath, "", csAWSCommand);

				lock.Unlock();
			}
		}

		// Upload Exchange Files
		int nNoOfFolders = itrAccount->second.vExchanges.size();
		for(int jCount(0); jCount < nNoOfFolders; jCount++)
		{
			CString csSourcePath, csExchange;
			csExchange = itrAccount->second.vExchanges[jCount];
			csSourcePath.Format("%s\\%s", m_objSchedule.strOutputFolder, csExchange);

			/*CString strLog;
			strLog.Format("Uploading All Exchange files for customer %d/%d (%s) - %s", nCurrent, nTotalAccounts, itrAccount->first, csSourcePath);
			m_cLogManager.fnAddMessage(strLog);
			RequestExternalUpdate();*/

			CString csAWSCommand;
			// aws s3 sync \\uat-app3\e$\Daily\MarketDepth\20150902_Output\DUMX\ --profile s3user s3://cmeftpuat/ftpdata/cme1/Test_DUMEX --exclude "*" --include "*.gz" 
			csAWSCommand.Format(_T("aws s3 sync %s --profile s3user \"%s\" --exclude \"*\" --include \"*.gz\""),csSourcePath, csDestFolder);					

			for( int iTry(0); iTry < 2; ++iTry)
			{
				itrAccount->second.iStatus = system(csAWSCommand);	

				if( itrAccount->second.iStatus == nSuccess )
					break;
			}

			FileGroupItr itrExchGroup = itrAccount->second.exchangeGroup.find(csExchange);

			if( itrAccount->second.iStatus == nSuccess )
			{
				COleDateTime dtCurr = COleDateTime::GetCurrentTime();
				CString strDateTime = dtCurr.Format(_T("%y-%m-%d %H:%M"));
				bool bReportSaved(false);

				for(int kCount(0); kCount < itrAccount->second.vReportFiles.size(); kCount++)
				{
					ReportFile &objReportFile = itrAccount->second.vReportFiles[kCount];

					if( objReportFile.csExchange.CompareNoCase(csExchange) == 0 )
					{
						itrAccount->second.totalFiles.Increment(objReportFile.lFileSize);
						objReportFile.csDeliveryTime = strDateTime;

						if( itrExchGroup != itrAccount->second.exchangeGroup.end() )
							itrExchGroup->second.Increment(objReportFile.lFileSize);

						if( !bReportSaved )
						{
							fnInsertDeliveryReport(itrAccount->first, objReportFile, csAWSCommand);
							bReportSaved = true;
						}
					}
				}				
			}
			else
			{
				lock.Lock();

				m_cLogManager.fnAddMessage(csAWSCommand);
				MessageUtil::getInstance()->LogToFile((LPCSTR)csAWSCommand);
				fnInsertErrorReport(itrAccount->first, "", csExchange, csAWSCommand);

				lock.Unlock();
			}
		}

		// Upload Products Files
		int nNoOfFiles = itrAccount->second.vReportFiles.size();
		for(int jCount(0); jCount < nNoOfFiles; jCount++)
		{
			ReportFile &objReportFile = itrAccount->second.vReportFiles[jCount];

			if( !objReportFile.bProductFile )
				continue;

			CString csSourcePath = objReportFile.csFilePath;

			/*CString strLog;
			strLog.Format("Uploading File for customer %d/%d (%s) - %s", nCurrent, nTotalAccounts, itrAccount->first, csSourcePath);
			m_cLogManager.fnAddMessage(strLog);
			RequestExternalUpdate();*/

			CString csAWSCommand;
			csAWSCommand.Format(_T("aws s3 cp %s --profile s3user \"%s\""),csSourcePath, csDestFolder);					
			
			for( int iTry(0); iTry < 2; ++iTry)
			{
				itrAccount->second.iStatus = system(csAWSCommand);	

				if( itrAccount->second.iStatus == nSuccess )
					break;
			}

			if( itrAccount->second.iStatus == nSuccess )
			{
				COleDateTime dtCurr = COleDateTime::GetCurrentTime();
				CString strDateTime = dtCurr.Format(_T("%y-%m-%d %H:%M"));

				itrAccount->second.totalFiles.Increment(objReportFile.lFileSize);
				objReportFile.csDeliveryTime = strDateTime;

				FileGroupItr itrExchGroup = itrAccount->second.exchangeGroup.find(objReportFile.csExchange);

				if( itrExchGroup != itrAccount->second.exchangeGroup.end() )
					itrExchGroup->second.Increment(objReportFile.lFileSize);

				fnInsertDeliveryReport(itrAccount->first, objReportFile, csAWSCommand);
			}
			else
			{
				lock.Lock();

				m_cLogManager.fnAddMessage(csAWSCommand);
				MessageUtil::getInstance()->LogToFile((LPCSTR)csAWSCommand);
				fnInsertErrorReport(itrAccount->first, objReportFile.csFileName, "", csAWSCommand);

				lock.Unlock();
			}
		}

		itrAccount->second.iStatus = true;

		// Upload Report File
		CString strReportPath = GenerateReport(itrAccount->second);
		if( !strReportPath.IsEmpty() )
		{
			CString csAWSCommand;
			csAWSCommand.Format(_T("aws s3 cp \"%s\" --profile s3user \"%s\""), strReportPath, csDestFolder);
			
			/*CString strLog;
			strLog.Format("Uploading Report File for customer %d/%d (%s)", nCurrent, nTotalAccounts, itrAccount->first);
			m_cLogManager.fnAddMessage(strLog);
			RequestExternalUpdate();*/

			int iStatus;
			for( int iTry(0); iTry < 2; ++iTry)
			{
				iStatus = system(csAWSCommand);	

				if( iStatus == nSuccess )
					break;
			}

			if( iStatus != nSuccess )
			{
				lock.Lock();

				m_cLogManager.fnAddMessage(csAWSCommand);
				MessageUtil::getInstance()->LogToFile((LPCSTR)csAWSCommand);
				fnInsertErrorReport(itrAccount->first, strReportPath, "", csAWSCommand);

				lock.Unlock();
			}
		}

		lock.Lock();
		DisplaySubscription();
		lock.Unlock();

	}while(iCount >= 0);
}

void MDAvroProcessor::UploadSubscription(CString csAccount)
{	
	m_cLogManager.fnAddMessage("Uploading Subcriptions Start");
	MessageUtil::getInstance()->LogToFile("Uploading Subcriptions Start");

	if(csAccount.IsEmpty())
	{
		int nTotalThreads;
		BF_Reg_Key_Ptr pKey1(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\CME"));
		RegDWORD strCPUCore(pKey1, "MDCPUCore", 5);
		nTotalThreads = strCPUCore.get_value();

		HANDLE *handle = new HANDLE[nTotalThreads];

		m_iCurrentFile = 0;
		m_nTotalFiles = m_SubAccountMap.size();

		for(int iCount(0); iCount < nTotalThreads; ++iCount)
			handle[iCount] = (HANDLE)_beginthreadex(0, 0, &UploadWorkerThread, this, 0, 0);

		WaitForMultipleObjects(nTotalThreads, handle, true, INFINITE);

		for(int iCount(0); iCount < nTotalThreads; ++iCount)
			CloseHandle(handle[iCount]);
	}
	else
	{
		m_iCurrentFile = 0;
		m_nTotalFiles = m_SubAccountMap.size();
		MultiThread_Upload(csAccount);
	}

	m_cLogManager.fnAddMessage("Uploading Subcriptions End");
	MessageUtil::getInstance()->LogToFile("Uploading Subcriptions End");
}

void MDAvroProcessor::GenerateSubscriptions()
{
	m_cLogManager.fnAddMessage("Generate Subcriptions Start");
	RequestExternalUpdate();
	MessageUtil::getInstance()->LogToFile("Generate Subcriptions Start");
	
	fnGetCheksumList();

	m_SubAccountMap.clear();

	CString csFolder, csKey;
	typedef vector<stFile> FileV;
	typedef map<CString, FileV> ExchFileMap;
	ExchFileMap exchFiles;
	
	for(int iCount(0); iCount < m_vSubscriptions.size(); iCount++)
	{
		SubscriptionDetail &clEODSub = m_vSubscriptions[iCount];

		ExchFileMap::iterator itrExchFiles = exchFiles.find((LPCSTR)clEODSub.stEXCH_CODE);
		if( itrExchFiles == exchFiles.end() )
		{
			csFolder.Format("%s\\%s", m_objSchedule.strOutputFolder, clEODSub.stEXCH_CODE);
			std::vector<stFile> vFiles;
			MessageUtil::getInstance()->GetFilesInDirectory(vFiles, (LPCSTR)csFolder, "*.gz");
			exchFiles[(LPCSTR)clEODSub.stEXCH_CODE] = vFiles;
			itrExchFiles = exchFiles.find((LPCSTR)clEODSub.stEXCH_CODE);
		}

		SubAccountMap::iterator itrAccount = m_SubAccountMap.find((LPCSTR)clEODSub.stDEST_ACCOUNT);

		if( itrAccount == m_SubAccountMap.end() )
		{
			UniqueAccount objAccount;
			objAccount.strDestAccount = clEODSub.stDEST_ACCOUNT;
			m_SubAccountMap[objAccount.strDestAccount] = objAccount;
			itrAccount = m_SubAccountMap.find(objAccount.strDestAccount);
		}

		if(clEODSub.stALL_PRODUCTS == "1")
			csKey.Format("%s", clEODSub.stEXCH_CODE);
		else
			csKey.Format("%s_%s_%s", clEODSub.stEXCH_CODE, clEODSub.stPROD_CODE, clEODSub.stFOICode);
		
		set<CString>::iterator itrKey = itrAccount->second.sKeys.find(clEODSub.stEXCH_CODE);
		if( itrKey != itrAccount->second.sKeys.end() )
			continue;

		itrKey = itrAccount->second.sKeys.find(csKey);
		if( itrKey != itrAccount->second.sKeys.end() )
			continue;

		itrAccount->second.sKeys.insert((LPCSTR)csKey);

		FileGroup objExchangeFiles;
		itrAccount->second.exchangeGroup[clEODSub.stEXCH_CODE] = objExchangeFiles;

		FileV &vFiles = itrExchFiles->second;

		if( vFiles.size() <= 0 )
			continue;

		if(clEODSub.stALL_PRODUCTS == "1")
		{
			itrAccount->second.vExchanges.push_back(clEODSub.stEXCH_CODE);

			for(int jCount(0); jCount < vFiles.size(); jCount++)
			{
				ReportFile objReportFile;
				objReportFile.bProductFile = false;
				objReportFile.csExchange = clEODSub.stEXCH_CODE;
				objReportFile.csCheckSum = GetCheksum(vFiles[jCount].strFileName.c_str());
				objReportFile.csFileName = vFiles[jCount].strFileName.c_str();
				objReportFile.csFilePath = vFiles[jCount].strFilePath.c_str();
				objReportFile.lFileSize = vFiles[jCount].lFileSize;
				itrAccount->second.vReportFiles.push_back(objReportFile);
			}
		}
		else
		{
			csFolder.Format("%s\\%s\\%s_MD_%s_%s_%s", m_objSchedule.strOutputFolder, clEODSub.stEXCH_CODE, clEODSub.stEXCH_CODE, clEODSub.stPROD_CODE, clEODSub.stFOICode, m_objSchedule.strProcessingDate);

			for(int jCount(0); jCount < vFiles.size(); jCount++)
			{
				CString strFile = vFiles[jCount].strFilePath.c_str();
				if( strFile.Find(csFolder) >= 0 )
				{
					ReportFile objReportFile;
					objReportFile.bProductFile = true;
					objReportFile.csExchange = clEODSub.stEXCH_CODE;
					objReportFile.csCheckSum = GetCheksum(vFiles[jCount].strFileName.c_str());
					objReportFile.csFileName = vFiles[jCount].strFileName.c_str();
					objReportFile.csFilePath = vFiles[jCount].strFilePath.c_str();
					objReportFile.lFileSize = vFiles[jCount].lFileSize;
					itrAccount->second.vReportFiles.push_back(objReportFile);
				}
			}
		}
	}

	CString strPath; 
	strPath.Format("%s\\RPT\\", m_objSchedule.strOutputFolder);

	if( CreateDirectory(strPath, NULL) == 0 )
	{
		if( GetLastError() == ERROR_PATH_NOT_FOUND )
		{			
			//csTemp = "Error OputPath Not Found";				
		}
	}

	DisplaySubscription();

	m_cLogManager.fnAddMessage("Generate Subcriptions End");
	RequestExternalUpdate();

	MessageUtil::getInstance()->LogToFile("Generate Subcriptions End");
}

void MDAvroProcessor::CopyS3FiletoLocal()
{
	MessageUtil::getInstance()->LogToFile("Sync Bucket Started");

	CString csInputBucket = ((ListField*)GetField(e_S3InputBucket))->GetShortString();
	CString csProfile = ((ListField*)GetField(e_S3InputBucket))->GetLongString();
	CString csS3Path;
	csS3Path.Format("%s/daily/marketdepth/%s", csInputBucket, m_objSchedule.strProcessingDate);
	
	CString csAWSCommand;
	//csAWSCommand.Format(_T("aws s3 cp --recursive --profile cmeprod %s %s"), csS3Path, csInputFolder);					
	//aws s3 sync --profile cmeprod s3://cmegroup-main-datamine-prod-staging/daily/endofday/20150825 D:\20150825 -- 
	csAWSCommand.Format(_T("aws s3 sync --profile %s %s %s"), csProfile, csS3Path, m_objSchedule.strInputFolder);					
	int iValue = system(csAWSCommand);	

	if( iValue == 0 )
	{
		m_enmOperation = OP_ProcessRunning;
	}
	else
	{
		m_enmOperation = OP_None;
		MessageUtil::getInstance()->LogToFile("Sync Bucket Failed");
		m_cLogManager.fnAddMessage("Sync Bucket Failed");
	}

	MessageUtil::getInstance()->LogToFile("Sync Bucket End");
}

void MDAvroProcessor::fnUpdateScheduleStatus( int iStatus )
{
	MessageUtil::getInstance()->LogToFile("Updating Status in Database");

	CString csdttime;
	csdttime.Format("%d-%02d-%02d %02d:%02d:00", m_objSchedule.dtOrigSchTime.GetYear(), m_objSchedule.dtOrigSchTime.GetMonth(), 
		m_objSchedule.dtOrigSchTime.GetDay(), m_objSchedule.dtOrigSchTime.GetHour(), m_objSchedule.dtOrigSchTime.GetMinute());

	COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
	CString csTimeStatus;
	csTimeStatus.Format("%d-%02d-%02d %02d:%02d:00",dtCurrentTime.GetYear(), dtCurrentTime.GetMonth(), dtCurrentTime.GetDay(), dtCurrentTime.GetHour(), dtCurrentTime.GetMinute());

	m_objSchedule.m_nStatus = iStatus;

	CString sql;
	if( iStatus == 1 )
		sql.Format("UPDATE dbo.CME_Subscription_Schedule SET Status = %d,   ProcessStart = '%s' WHERE FileType ='MD' AND SubType = 'FText' AND ScheduleDateTime = '%s'", m_objSchedule.m_nStatus, csTimeStatus, csdttime);
	else
		sql.Format("UPDATE dbo.CME_Subscription_Schedule SET Status = %d,   ProcessEnd = '%s' WHERE FileType ='MD' AND SubType = 'FText' AND ScheduleDateTime = '%s'", m_objSchedule.m_nStatus, csTimeStatus, csdttime);

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		MessageUtil::getInstance()->LogToFile("Error in updating status in database");
	}

	MessageUtil::getInstance()->LogToFile("Updating Status in Database Finished");
}

bool MDAvroProcessor::FtpDownload()
{
	MessageUtil::getInstance()->LogToFile("FTP Download Started");

	CString strFtpServer = GetField(e_FtpServer)->GetValueString();
	CString strFtpFile = GetField(e_FtpFilePath)->GetValueString();

	if( !(boost::filesystem::exists(m_objSchedule.strSecDefFolder.GetString())))
		boost::filesystem::create_directories(m_objSchedule.strSecDefFolder.GetString());

	CString m_strPath = m_objSchedule.strSecDefFolder;
	CFTPAutomation cFTPConnection;
	bool bSuccess(true);
	bSuccess = cFTPConnection.Connect(strFtpServer, "", "", 21, true);
	if( bSuccess )
	{
		bSuccess = cFTPConnection.GetFiles(strFtpFile, m_strPath);
		cFTPConnection.Disconnect();

		if( bSuccess )
		{
			CString strFileName("");
			int nPos = strFtpFile.ReverseFind('/');
			if (nPos != -1)
				strFileName = strFtpFile.Right(strFtpFile.GetLength()-nPos-1);

			if( !strFileName.IsEmpty() )
			{
				CString strCmd;
				m_strPath = m_strPath + "\\" + strFileName;

				strCmd.Format("gzip.exe -d \"%s\"", m_strPath);

				int iValue = system(strCmd);
				if( iValue == 0 )
				{
					m_csFtpSecDef = m_strPath;
					m_csFtpSecDef.Replace(".gz", "");

					strCmd.Format("SecDef file is extracted - %s", m_strPath);
					m_cLogManager.fnAddMessage(strCmd);
				}
				else
				{
					CString strLog;
					strLog.Format("Fail to extract %s", strCmd);
					m_cLogManager.fnAddMessage(strLog);
					bSuccess = false;
				}
			}
			else
			{
				m_cLogManager.fnAddMessage("FTP file name is null");
				MessageUtil::getInstance()->LogToFile("FTP file name is null");
				bSuccess = false;
			}
		}
		else
		{
			m_cLogManager.fnAddMessage("FTP file download failed");
			m_cLogManager.fnAddMessage(cFTPConnection.GetErrorMessage());
			MessageUtil::getInstance()->LogToFile("FTP file download failed");
		}
	}
	else
	{
		m_cLogManager.fnAddMessage("FTP connection failed");
		m_cLogManager.fnAddMessage(cFTPConnection.GetErrorMessage());
		MessageUtil::getInstance()->LogToFile("FTP connection failed");

	}

	MessageUtil::getInstance()->LogToFile("FTP Download End");
	return bSuccess;
}

bool MDAvroProcessor::SecDefGenerate()
{
	MessageUtil::getInstance()->LogToFile("SecDef Generation Started");

	FILE *m_pFile;
	FILE *m_pFileNew;

	if(m_csFtpSecDef.IsEmpty())
	{
		CString strLog;
		strLog.Format("Downloaded secdef file name is empty");
		m_cLogManager.fnAddMessage(strLog);
		MessageUtil::getInstance()->LogToFile((LPCSTR)strLog);
		return false;
	}

	if(fopen_s(&m_pFile, m_csFtpSecDef ,"r"))
	{
		CString strLog;
		strLog.Format("Error in opening downloaded secdef file %s", m_csFtpSecDef);
		m_cLogManager.fnAddMessage(strLog);

		MessageUtil::getInstance()->LogToFile((LPCSTR)strLog);
		return false;
	}

	if(fopen_s(&m_pFileNew, m_objSchedule.strContractFile, "w"))
	{
		CString strLog;
		strLog.Format("Error in opening contract file %s", m_objSchedule.strContractFile);
		m_cLogManager.fnAddMessage(strLog);
		MessageUtil::getInstance()->LogToFile((LPCSTR)strLog);
		return false;
	}

	char chLine[4096] = {0};
	int nPosFrom, nPosTo;
	long lLineCount(0);
	CString strLine, strOutput;
	CString SecurityID, ApplID, SecurityExchange, SecurityGroup, Asset, CFICode, Symbol;

	try
	{
		while(fgets(chLine, 4096, m_pFile))
		{
			strLine = chLine;

			nPosFrom = strLine.Find("48=");
			nPosFrom += 3;
			nPosTo = strLine.Find("\001", nPosFrom);
			SecurityID = strLine.Mid(nPosFrom, nPosTo - nPosFrom);

			nPosFrom = strLine.Find("1180=");
			nPosFrom += 5;
			nPosTo = strLine.Find("\001", nPosFrom);
			ApplID = strLine.Mid(nPosFrom, nPosTo - nPosFrom);

			nPosFrom = strLine.Find("207=");
			nPosFrom += 4;
			nPosTo = strLine.Find("\001", nPosFrom);
			SecurityExchange = strLine.Mid(nPosFrom, nPosTo - nPosFrom);

			nPosFrom = strLine.Find("1151=");
			nPosFrom += 5;
			nPosTo = strLine.Find("\001", nPosFrom);
			SecurityGroup = strLine.Mid(nPosFrom, nPosTo - nPosFrom);

			nPosFrom = strLine.Find("6937=");
			nPosFrom += 5;
			nPosTo = strLine.Find("\001", nPosFrom);
			Asset = strLine.Mid(nPosFrom, nPosTo - nPosFrom);

			nPosFrom = strLine.Find("461=");
			nPosFrom += 4;
			nPosTo = strLine.Find("\001", nPosFrom);
			CFICode = strLine.Mid(nPosFrom, nPosTo - nPosFrom);

			nPosFrom = strLine.Find("55=");
			nPosFrom += 3;
			nPosTo = strLine.Find("\001", nPosFrom);
			Symbol = strLine.Mid(nPosFrom, nPosTo - nPosFrom);

			strOutput.Format("%s,%s,%s,%s,%s,%s,%s", SecurityID, ApplID, SecurityExchange, SecurityGroup, Asset, CFICode, Symbol);
			fputs(strOutput, m_pFileNew);
			fputs("\n", m_pFileNew);

			++lLineCount;
			if( ((lLineCount) % 100000) == 0 )
			{
				CString strLog;
				strLog.Format("SecDefGenerate: Processing Line %d", lLineCount);
				m_cLogManager.fnAddMessage(strLog);
				PrintRawMessage(strLog);
				RequestExternalUpdate();
			}
		}
	}
	catch(...)
	{
		m_cLogManager.fnAddMessage("Error in generating secdef");
		MessageUtil::getInstance()->LogToFile("Error in generating secdef");
		return false;
	}

	fclose(m_pFile);
	fclose(m_pFileNew);

	MessageUtil::getInstance()->LogToFile("SecDef Generation End");
	return true;
}

CString MDAvroProcessor::GetCheksum(CString strFile)
{
	std::map<CString,CString>::iterator itrFile = m_clChecksumMap.find(strFile);
	if( itrFile != m_clChecksumMap.end() )
		return itrFile->second;
	else
		return "";

}

void MDAvroProcessor::fnGetCheksumList()
{	
	m_clChecksumMap.clear();

	if( !(boost::filesystem::exists(m_objSchedule.strOutputFolder.GetString())))
	{
		m_cLogManager.fnAddMessage("Output directory does not exist");
		return;
	}


	CString csOuptFile = m_objSchedule.strOutputFolder + "\\AllChecksum.txt";

	CString csCMD;
	csCMD.Format("D:\\fciv.exe \"%s\" -wp -r -type *.gz -md5 > \"%s\"", m_objSchedule.strOutputFolder, csOuptFile);
	
	int iValue = system(csCMD);	
	if( iValue != 0 )
	{
		m_cLogManager.fnAddMessage( "fciv.exe is not found in D drive" );
		return;
	}

	FILE *fileIn(NULL);
	
	if( fopen_s(&fileIn, csOuptFile, "r") != 0 )
	{
		m_cLogManager.fnAddMessage( "Error while Opening fileIn" );
		return;
	}

	char chline[1024];
	CString csStrLine;

	// Read Header...
	while( fgets(chline, 1024, fileIn) )
	{
		if( strchr(chline, '\n') )
		{
			csStrLine += chline;
			
			if( csStrLine.Find(".gz") ^ -1 )
			{
				int iIndext = csStrLine.Find(" ");
				CString csCheckSum = csStrLine.Left(iIndext);
				CString csFileName = csStrLine.Mid(iIndext, 256);
				csFileName.Trim();

				m_clChecksumMap.insert(std::pair<CString,CString>(csFileName, csCheckSum));
			}
			
			csStrLine = "";
		}
		else
		{
			csStrLine += chline;
		}		
	}

	fclose(fileIn);
}

CString MDAvroProcessor::GenerateReport(UniqueAccount &objAccount)
{
	if( objAccount.totalFiles.iFileCount <= 0 )
		return "";

	CString  csLine1, csLine;
	csLine1 = "MarketDepth Files Info\n\n\nFile Count/Size(KB) for all exchanges\n\n_________________________________\n\n";
	csLine.Format("%d\t\t%u\n\n\n", objAccount.totalFiles.iFileCount, (unsigned long)objAccount.totalFiles.lFileSize/1024);		

	// Open file for that customer 
	FILE *fileOut = NULL;

	CString csDestAcc;
	csDestAcc.Format("%s\\RPT\\%s", m_objSchedule.strOutputFolder, objAccount.strDestAccount);
	CreateDirectory(csDestAcc, NULL);

	CString csFileName;
	csFileName.Format("%s\\EnrichMD_DeliveryInfo_%s.rpt", csDestAcc, m_objSchedule.strProcessingDate);
								
	if( fopen_s(&fileOut, csFileName, "w") != 0 )
		return "";

	/*CString stroutput;
	stroutput.Format("Creating report for %s", objAccount.strDestAccount );
	m_cLogManager.fnAddMessage( stroutput );
	RequestExternalUpdate();*/

	fputs( csLine1, fileOut );
	fputs( csLine, fileOut );

	csLine = "File Count/Size(KB) by exchanges\n\n_________________________________\n\n";

	fputs( csLine, fileOut );

	FileGroupItr itrExchGroup = objAccount.exchangeGroup.begin();

	while( itrExchGroup != objAccount.exchangeGroup.end() )
	{
		if( itrExchGroup->second.iFileCount > 0 )
		{
			csLine.Format("%s\t\t%d\t\t%u\n", itrExchGroup->first, itrExchGroup->second.iFileCount, (unsigned long)itrExchGroup->second.lFileSize/1024);
			fputs( csLine, fileOut );
		}

		itrExchGroup++;
	}

	csLine = "\n\nClient File size (Bytes) and delivery time\n\n___________________________________\n\n";
	fputs( csLine, fileOut );

	for(int iK = 0; iK < objAccount.vReportFiles.size(); iK++ )
	{
		if( !objAccount.vReportFiles[iK].csDeliveryTime.IsEmpty() )
		{
			csLine.Format("%s\t%u\t%s\t%s\n", objAccount.vReportFiles[iK].csFileName, objAccount.vReportFiles[iK].lFileSize, objAccount.vReportFiles[iK].csDeliveryTime, objAccount.vReportFiles[iK].csCheckSum);
			fputs( csLine, fileOut );
		}
	}

	fclose( fileOut );
	fileOut = NULL;

	return csFileName;
}

void MDAvroProcessor::fnInsertErrorReport(CString csDestAccount, CString csFileName, CString csExchnage, CString &csErrorCmd )
{	
	CString csSql;
	csSql.Format("EXEC Proc_Beast_Submit_CME_Daily_Error_Report 'MDText', '%s', '%s', '%s', '%s'", csDestAccount, csFileName, csExchnage, csErrorCmd);
	
	_RecordsetPtr set;
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)csSql, true)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSql);
		PrintRawMessage(csSql);
		return;
	}
}

void MDAvroProcessor::fnInsertDeliveryReport(CString csDestAccount, ReportFile &objReportFile, CString csAWSCommand)
{
	CString csSql;
	csSql.Format("EXEC Proc_Beast_Submit_CME_Daily_Delivery_Report 'MDText', '%s', '%s', %u, '%s','%s', '%s', '%s'", csDestAccount, 
		objReportFile.csFileName, objReportFile.lFileSize, objReportFile.csCheckSum, objReportFile.csDeliveryTime, objReportFile.csExchange, csAWSCommand);
	
	_RecordsetPtr set;
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)csSql, true)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSql);
		PrintRawMessage(csSql);
		return;
	}
}

const CString	__strSmtp	= "email-smtp.us-east-1.amazonaws.com";
const CString	__strLogin	= "AKIAIERFOJMRYCRRAHYQ";
const CString	__strPassword = "AvauV+bh9qVBlRQuCI5u9GMG2O1N2hajyhzE7MhyYANC"; 
const CString	__strSender	= "cmenotifications@thebeastapps.com";
const int		__iPort = 587;

void MDAvroProcessor::fnSendEmail()
{
	CSmtp mail;

	mail.SetSMTPServer(__strSmtp, __iPort, true);
	mail.SetSecurityType((SMTP_SECURITY_TYPE)USE_TLS);		
	mail.SetLogin(__strLogin);
	mail.SetPassword(__strPassword);
			
	//CC
	/*CString strSendTo		= GetField(e_T0)->GetValueString();
	CString strCC			= GetField(e_CC)->GetValueString();
	CString strSubject		= GetField(e_Subject)->GetValueString();
	CString strBody			= GetField(e_Body)->GetValueString();*/
	
	CString strSendTo		= "vcmops@thebeastapps.com,";
	CString strCC			= "mpatel@thebeastapps.com,dmodi@thebeastapps.com,mvpatel@thebeastapps.com,";
	CString strSubject		= "";
	CString strBody			= "";
	CString strTemp			= "";

	strSubject = "Execution MD Avro FIX Text - Notification";
	//m_clScheduleInfo.csFilePath
	strBody.Format("\nProcessing Date: %s\n\n", m_objSchedule.strProcessingDate);

	strTemp.Format("Status: Processed successfully\n\n");
	strBody += strTemp;

	strTemp.Format("Thanks\nBeast Apps Team\n");
	strBody += strTemp;
	
	//++++++++++++++++++++++++Send To+++++++++++++++++++++++
	if( !strSendTo.IsEmpty() )
	{
		if( strSendTo.Right(1).CompareNoCase(",") != 0 )
		{
			strSendTo = strSendTo + ",";
		}		
	}	
	
	if( strSendTo.Find(",") != -1 )
	{
		while(!strSendTo.IsEmpty())
		{
			CString tmpSendTo = strSendTo.Left(strSendTo.Find(","));			
			strSendTo.Delete(0,strSendTo.Find(",")+1);		
			tmpSendTo.TrimLeft();
			tmpSendTo.TrimRight();

			if(!tmpSendTo.IsEmpty())
				mail.AddRecipient(tmpSendTo);
		}
	}		
	else
	{			
		if( !strSendTo.IsEmpty() )
			mail.AddRecipient(strSendTo);
	}
	//+++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++ CC +++++++++++++++++++++++
	if( !strCC.IsEmpty() )
	{
		if( strCC.Right(1).CompareNoCase(",") != 0 )
		{
			strCC = strCC + ",";
		}		
	}		

	if( strCC.Find(",") != -1 )
	{
		while(!strCC.IsEmpty())
		{
			CString tmpCC = strCC.Left(strCC.Find(","));			
			strCC.Delete(0,strCC.Find(",")+1);		
			tmpCC.TrimLeft();
			tmpCC.TrimRight();

			if(!tmpCC.IsEmpty())
				mail.AddCCRecipient(tmpCC);
		}
	}		
	else
	{			
		if(!strCC.IsEmpty())
			mail.AddCCRecipient(strCC);
	}
	//+++++++++++++++++++++++++++++++++++++++++++++++
		
	mail.SetSenderMail(__strSender);
				
	if( !strBody.IsEmpty() )
		mail.AddBody(strBody);
				
	if( !strSubject.IsEmpty() )
		mail.SetSubject(strSubject);
		
	try
	{			
		mail.Send(NULL);		
		//GetField(e_Status)->SetValueString("Mail Sent successfully.");  	
	}
	catch(ECSmtp e)
	{
		CString stemp;
		stemp.Format("Mail Failed...! - %s", e.GetErrorText());		
		m_cLogManager.fnAddMessage( stemp );
		return;
	}
	catch(...)
	{
		CString stemp;
		stemp.Format("Mail Failed...! - Error Id: %d", GetLastError());		
		m_cLogManager.fnAddMessage( stemp );
		return;
	}

	m_cLogManager.fnAddMessage( "EMail sent successfully");
}