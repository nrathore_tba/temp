#include <fstream>
#include <sstream>
#include <stdafx.h> 
#include <shldisp.h>
#include <tlhelp32.h>
#include "zip.h"
#include <MailAutomation/MailAutomation.h>
#include "ExecutionBLOCKS.hpp"

#define SPLITLINE 50000

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(ExecutionBLOCKS)

ExecutionBLOCKS::ExecutionBLOCKS()
{	
}

ExecutionBLOCKS::~ExecutionBLOCKS()
{
}

bool ExecutionBLOCKS::InitData()
{
	GetField(900000)->SetValueString(this->GetUserName());
	AddInfoField(GetField(900000));

	m_curTopRow= 0;
	for(m_numofRows = 0; GetField(e_LogMsg + m_numofRows); m_numofRows++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	//---------------------------------------------
	if( !IsRestoreUpdate( ) )
	{
		BF_Reg_Key_Ptr pKey1(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\CME"));

		int iFTPLocation;
		RegDWORD strServerName(pKey1, "S3FTPLocation",0);
		iFTPLocation = strServerName.get_value();
		GetField(e_S3FTPBucket)->SetValueInt( iFTPLocation );

		int iInputLocation;
		RegDWORD strS3InPutLocation(pKey1, "S3InputLocation",2);
		iInputLocation = strS3InPutLocation.get_value();
		GetField(e_S3CMEBucket)->SetValueInt( iInputLocation );
		
		COleDateTime dtToday = COleDateTime::GetCurrentTime();
		BFDate bfToday;
		bfToday.SetYearMonthDate(dtToday.GetYear(),dtToday.GetMonth() - 1, dtToday.GetDay() );
		GetField(e_DateFilter)->SetValueDate(bfToday); 
	}

	fld_OutputPath = GetField(e_OutputPath);

	m_clScheduleInfo.bValid = false;
	m_clScheduleInfo.iRetInterval = GetField(e_RtryInterval)->GetValueInt() > 0 ? GetField(e_RtryInterval)->GetValueInt(): 10;
	
	m_bIsThreadRunning = false;
	m_bIsTrheadCompleted = true;
	m_iTotalCount = 0;
	m_iOpenFileCount = 0;

	m_enmOperation = OP_None;
	return true;
}

const int		__niMsgCouunt = 200;
CString __csMsgArry[__niMsgCouunt] = { "" };

void ExecutionBLOCKS::fnAddMessage( CString csaNewMsg )
{	
	for( int iI = __niMsgCouunt-1; iI > 0; iI-- )
		__csMsgArry[iI] = __csMsgArry[iI -1];
	
	__csMsgArry[0] = csaNewMsg;	
}

void ExecutionBLOCKS::fnDisplayMsg()
{
	int iRow = 0;
	for( int iIndex = m_curTopRow; iIndex < __niMsgCouunt; iIndex++, iRow++ )
	{
		if( iRow >= m_numofRows )
			break;
		
		BaseField * fldComment		= GetField(e_LogMsg + iRow);
		
		fldComment->SetValueString( __csMsgArry[iIndex] );
		fldComment->SetNotManual();
	}
	
	while( iRow < m_numofRows )
	{
		BaseField * fldComment		= GetField(e_LogMsg + iRow);
		fldComment->SetBlankState();
		iRow++;
	}
}

UINT BLOCKProcessor(void *pVoid)
{
	ExecutionBLOCKS *pParser = (ExecutionBLOCKS*)pVoid;
	
	pParser->m_iTotalCount = 0;
	pParser->m_bIsThreadRunning = true;
	pParser->m_bIsTrheadCompleted = false;
	
	pParser->fnUpdateScheduleStatus( 1 );
	
	pParser->ReadBLOCKFileCSV();

	pParser->fnUpdateScheduleStatus( 2 );
	pParser->fnSendEmail(2,0);

	pParser->m_enmOperation = OP_ProcessCompleted;
	pParser->m_clScheduleInfo.bValid  = false;	

	pParser->m_bIsThreadRunning = false;
	pParser->RequestExternalUpdate();

	return 0;
}

void ExecutionBLOCKS::Recalculate()
{
	if( IsFieldNew(e_RtryInterval) )
		m_clScheduleInfo.iRetInterval = GetField(e_RtryInterval)->GetValueInt() > 0 ? GetField(e_RtryInterval)->GetValueInt(): 10;

	if( m_bIsThreadRunning )
	{
		GetField(e_MsgTitle)->SetTitle(m_csMsg);		
	}
	else
	{
		if( !m_bIsThreadRunning && !m_bIsTrheadCompleted )
		{
			// After completion of thread
			GetField(e_MsgTitle)->SetTitle("Message");
			m_bIsTrheadCompleted = true;
		}

		if( !m_clScheduleInfo.bValid || m_enmOperation == OP_ProcessCompleted || IsFieldNew(e_ReloadSchedule) )
		{		
			fnGetNextSchedule( );
		}

		if( !m_clScheduleInfo.bValid )	
		{
			StartRecalcTimer(60, false);
			return;
		}

		if( m_enmOperation == OP_Process || IsFieldNew(e_Process) )
		{
			/*CString strBasePath = fld_OutputPath->GetValueString();

			CString csFilePath, csUncompFileName, csCommand;
			csFilePath.Format("%s\\%s\\BLOCKS_%s.gz", strBasePath, m_clScheduleInfo.csDate, m_clScheduleInfo.csDate);
			
			csUncompFileName = csFilePath;
			csUncompFileName.Replace(".gz",".txt");
			m_clScheduleInfo.csFilePath = csUncompFileName;*/

			fnCheckFolderPath( );

			if( m_enmOperation == OP_ReadThread )
			{
				AfxBeginThread(BLOCKProcessor, this);
			}
		}

		if( m_enmOperation == OP_Uncompress )
		{
			fnUncompressGZ( );
		}

		if( m_enmOperation == OP_Download )
		{
			fnCopyS3FiletoLocal( );
		}

		if( m_enmOperation == OP_None )
			fnRecalc1( );

		if( m_enmOperation != OP_None && m_enmOperation != OP_ReadThread )
			StartRecalcTimer(0, false);
	}

	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_pgUP,e_pgDN,200,m_numofRows,m_curTopRow);		
	fnDisplayMsg();	
}

void ExecutionBLOCKS::fnRecalc1( )
{
	COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
	long lRecalcTime = 0;

	if( GetField( e_ManualRun )->GetValueInt() == 1 )
	{
		GetField(e_NextDownloadMsg)->SetTitle("Processing Set To Manaul");
	}
	else if( dtCurrentTime >= m_clScheduleInfo.clScheduleDtTime )
	{
		//Need to Process subscription...
		GetField(e_NextDownloadMsg)->SetTitle("Processing File");
		m_enmOperation = OP_Download;		
		fnAddMessage("Downloading File");
	}
	else if( dtCurrentTime < m_clScheduleInfo.clScheduleDtTime )
	{
		COleDateTimeSpan dtSpan = m_clScheduleInfo.clScheduleDtTime - dtCurrentTime;
		lRecalcTime = dtSpan.GetTotalSeconds();

		CString csTimer,csTmp;

		int iDays =	dtSpan.GetDays();
		int iLeftTotalHr = dtSpan.GetHours();
		int iLeftTotalMinute = dtSpan.GetMinutes();
		int iLeftTotalSecond = dtSpan.GetSeconds();

		if( iDays > 0 )
		{
			csTmp.Format("%d Day and ",iDays);					
			csTimer += csTmp;

			csTmp.Format("%02d:",iLeftTotalHr);					
			csTimer += csTmp;
		}
		else
		{
			if( iLeftTotalHr > 0 )
			{					
				csTmp.Format("%02d:",iLeftTotalHr);					
				csTimer += csTmp;
			}
		}

		csTmp.Format("%02d:",iLeftTotalMinute);					
		csTimer += csTmp;

		csTmp.Format("%02d",iLeftTotalSecond);
		csTimer += csTmp;				

		int iDate	= m_clScheduleInfo.clScheduleDtTime.GetDay();
		int iMonth	= m_clScheduleInfo.clScheduleDtTime.GetMonth();
		int iYear	= m_clScheduleInfo.clScheduleDtTime.GetYear();
		int iHour	= m_clScheduleInfo.clScheduleDtTime.GetHour();
		int iMinute = m_clScheduleInfo.clScheduleDtTime.GetMinute();
		int iSecond = m_clScheduleInfo.clScheduleDtTime.GetSecond();

		CString csStr;
		csStr.Format("Next Download: %d-%d-%d %2d:%2d:%2d [Timer: %s]",iDate,iMonth,iYear,iHour,iMinute,iSecond, csTimer);

		GetField(e_NextDownloadMsg)->SetTitle(csStr);
		GetField(e_NextDownloadMsg)->SetTitleForeColor(ColorManager::eSignalPositiveLight);

		if( lRecalcTime > 4 || lRecalcTime < 0 )
			lRecalcTime = 4;
	}

	if( lRecalcTime )
		StartRecalcTimer(lRecalcTime, false);
}

void ExecutionBLOCKS::fnGetNextSchedule( )
{
	m_clScheduleInfo.bValid = false;
	m_enmOperation = OP_None;

	BFDate bfdt = GetField(e_DateFilter)->GetValueDate();

#ifdef _DEBUG 
	m_clScheduleInfo.clScheduleDtTime = COleDateTime(2015,9,9,0,0,0);

	m_clScheduleInfo.iStatus = 0;
	m_clScheduleInfo.bValid = true;

	BFDate bfDate = m_clScheduleInfo.clScheduleDtTime.m_dt;

	m_clScheduleInfo.csDate.Format("%d%02d%02d", bfDate.GetYear(), bfDate.GetMonth()+1, bfDate.GetDate() );
	m_clScheduleInfo.csDateBlock.Format("%02d%02d%d", bfDate.GetMonth()+1, bfDate.GetDate(), bfDate.GetYear());

#else

	CString csStrDtFilter;
	csStrDtFilter.Format("%d-%02d-%02d 00:00:00",bfdt.GetYear(), bfdt.GetMonth()+1, bfdt.GetDate());

	CString sql;
	sql.Format("SELECT top 1* from dbo.CME_Subscription_Schedule WHERE FileType ='BLOCKS' and IsNull(Status,0) In(0,1) and ScheduleDateTime > '%s' ORDER BY ScheduleDateTime", csStrDtFilter);

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		GetField(e_NextDownloadMsg)->SetTitle("Error while Loading Schedule");
		return;
	}
		
	if( VARIANT_FALSE == set->adoEOF )
	{
		SETLONG( m_clScheduleInfo.iStatus,		set->Fields->Item[_variant_t("Status")]->Value);	
		SETOLEDATE(	m_clScheduleInfo.clScheduleDtTime,	set->Fields->Item[_variant_t("ScheduleDateTime")]->Value);	
		m_clScheduleInfo.clMainScheduleDtTime = m_clScheduleInfo.clScheduleDtTime;

		m_clScheduleInfo.iRetryCount = 0;
		m_clScheduleInfo.csDate.Format("%d%02d%02d", m_clScheduleInfo.clScheduleDtTime.GetYear(), m_clScheduleInfo.clScheduleDtTime.GetMonth(), m_clScheduleInfo.clScheduleDtTime.GetDay() );
		m_clScheduleInfo.csDateBlock.Format("%02d%02d%d", m_clScheduleInfo.clScheduleDtTime.GetMonth(), m_clScheduleInfo.clScheduleDtTime.GetDay(), m_clScheduleInfo.clScheduleDtTime.GetYear());
		
		m_clScheduleInfo.bValid = true;
	}

	set->Close();
#endif

	if( m_clScheduleInfo.bValid  )
	{
		GetField( e_Year )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetYear() );
		GetField( e_Month )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetMonth() );
		GetField( e_Day )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetDay() );
		GetField( e_Hour )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetHour() );
		GetField( e_Minute )->SetValueInt( m_clScheduleInfo.clScheduleDtTime.GetMinute() );
	}
	else
	{
		GetField(e_NextDownloadMsg)->SetTitle("No Schedule Loaded");
	}
}

void ExecutionBLOCKS::fnUpdateScheduleStatus( int iStatus )
{
	CString csdttime;
	csdttime.Format("%d-%02d-%02d %02d:%02d:00",m_clScheduleInfo.clMainScheduleDtTime.GetYear(), m_clScheduleInfo.clMainScheduleDtTime.GetMonth(), m_clScheduleInfo.clMainScheduleDtTime.GetDay(), m_clScheduleInfo.clMainScheduleDtTime.GetHour(), m_clScheduleInfo.clMainScheduleDtTime.GetMinute());
	
	COleDateTime dtCurrentTime = COleDateTime::GetCurrentTime();
	CString csTimeStatus;
	csTimeStatus.Format("%d-%02d-%02d %02d:%02d:00",dtCurrentTime.GetYear(), dtCurrentTime.GetMonth(), dtCurrentTime.GetDay(), dtCurrentTime.GetHour(), dtCurrentTime.GetMinute());

	CString sql;
	if( iStatus == 1 )
		sql.Format("UPDATE dbo.CME_Subscription_Schedule SET Status = %d, ProcessStart = '%s' WHERE FileType ='BLOCKS' and ScheduleDateTime = '%s'", iStatus, csTimeStatus, csdttime);
	else
		sql.Format("UPDATE dbo.CME_Subscription_Schedule SET Status = %d,   ProcessEnd = '%s' WHERE FileType ='BLOCKS' and ScheduleDateTime = '%s'", iStatus, csTimeStatus, csdttime);

	_RecordsetPtr set;
	if( FAILED(m_clpdbInt->GetRecordset(&set, sql)) )
	{
		SetErrorMessage(ErrorSeverity::e_Error, sql);
		GetField(e_NextDownloadMsg)->SetTitle("Error while Loading Schedule");
		return;
	}
}

void ExecutionBLOCKS::fnCopyS3FiletoLocal( )
{
	CString strBasePath = fld_OutputPath->GetValueString();

	CString csInputBucket = ((ListField*)GetField(e_S3CMEBucket))->GetShortString();
	CString csProfile	  = ((ListField*)GetField(e_S3CMEBucket))->GetLongString();

	CString strFullInputPath;
	strFullInputPath.Format("%s/daily/blocks/%s/BLOCKS_%s.gz", csInputBucket, m_clScheduleInfo.csDate, m_clScheduleInfo.csDate);

	CString csDestinationFolder;
	csDestinationFolder.Format("%s\\%s\\",strBasePath, m_clScheduleInfo.csDate);

	//"s3 cp s3://cmegroup-main-datamine-qa-staging/daily/endofday/20150805/EOD_20150805_E.gz  d:\daily\endofday\20150805\";
	CString csAWSCommand;
	csAWSCommand.Format(_T("aws s3 cp --profile %s %s %s"), csProfile, strFullInputPath, csDestinationFolder);					
	int iValue = system(csAWSCommand);	

	if( iValue == 0 )
	{
		fnAddMessage(csAWSCommand + " File Downloaded successfully");
		m_enmOperation = OP_Uncompress;
	}
	else
	{
		m_clScheduleInfo.clScheduleDtTime = COleDateTime::GetCurrentTime();;
		m_clScheduleInfo.clScheduleDtTime += COleDateTimeSpan(0,0,m_clScheduleInfo.iRetInterval,0);
		m_clScheduleInfo.iRetryCount++;

		CString csTemp;
		csTemp.Format("Error cmd %s, File Download schedule after %d minute", csAWSCommand, m_clScheduleInfo.iRetInterval );
		fnAddMessage( csTemp );

		m_enmOperation = OP_None;
	}
}

void ExecutionBLOCKS::fnUncompressGZ( )
{
	CString strBasePath = fld_OutputPath->GetValueString();

	CString csFilePath, csUncompFileName, csCommand;

	csFilePath.Format("%s\\%s\\BLOCKS_%s.gz", strBasePath, m_clScheduleInfo.csDate, m_clScheduleInfo.csDate);
	csUncompFileName = csFilePath;
	csUncompFileName.Replace(".gz",".txt");

	//csCommand.Format("gzip.exe -d -c \\powervault3\CME_s3\daily\endofday\20150805\EOD_20150805_E.gz > \\powervault3\CME_s3\daily\endofday\20150805\EOD_20150805_E.txt");
	csCommand.Format("gzip.exe -d -c %s > %s", csFilePath, csUncompFileName);
		
	int	iValue = system(csCommand);

	if( iValue == 0 )
	{
		fnAddMessage(csCommand + " Uncomprssed file successfully");
		m_clScheduleInfo.csFilePath = csUncompFileName;
		m_enmOperation = OP_Process;
	}
	else if( PathFileExists(csUncompFileName) == FALSE )
	{
		fnAddMessage("Uncomprssed file Already exist");
		m_clScheduleInfo.csFilePath = csUncompFileName;
		m_enmOperation = OP_Process;
	}
	else
	{
		m_clScheduleInfo.clScheduleDtTime = COleDateTime::GetCurrentTime();;
		m_clScheduleInfo.clScheduleDtTime += COleDateTimeSpan(0,0,m_clScheduleInfo.iRetInterval,0);

		CString csTemp;
		csTemp.Format("error no = %d, cme = %s, - path %s, uncompress unsuccessful, File Download schedule after %d minute",iValue, csCommand, csFilePath, m_clScheduleInfo.iRetInterval);
		fnAddMessage( csTemp );

		m_enmOperation = OP_None;
	}
}

void ExecutionBLOCKS::fnCheckFolderPath( )
{
	bool bError = false;
	CString strBasePath = fld_OutputPath->GetValueString();

	CString strBLOCP_Path; 
	strBLOCP_Path.Format("%s\\%s_BLOCP", strBasePath, m_clScheduleInfo.csDate);

	if( CreateDirectory(strBLOCP_Path, NULL) == 0 )
	{
		if( GetLastError() == ERROR_PATH_NOT_FOUND )
		{			
			//csTemp = "Error OputPath Not Found";	
			bError = true;
		}
	}

	CString strBLOCK_Path; 
	strBLOCK_Path.Format("%s\\%s_BLOCK", strBasePath, m_clScheduleInfo.csDate);

	if( CreateDirectory(strBLOCK_Path, NULL) == 0 )
	{
		if( GetLastError() == ERROR_PATH_NOT_FOUND )
		{			
			//csTemp = "Error OputPath Not Found";	
			bError = true;
		}
	}	

	if( bError )
	{	
		m_enmOperation = OP_None;
		m_clScheduleInfo.clScheduleDtTime  = COleDateTime::GetCurrentTime();;
		m_clScheduleInfo.clScheduleDtTime += COleDateTimeSpan(0,0,m_clScheduleInfo.iRetInterval,0);

		CString csTemp;
		csTemp.Format("Directory structure not found, File Download scheduled after %d minutes",m_clScheduleInfo.iRetInterval);
		fnAddMessage(csTemp);
	}
	else
	{
		m_clScheduleInfo.csBLOCP_FilePath = strBLOCP_Path;
		m_clScheduleInfo.csBLOCK_FilePath = strBLOCK_Path;

		m_enmOperation = OP_ReadThread;
		fnAddMessage("Input File Reading is started");
	}
}

void ExecutionBLOCKS::ReadBLOCKFileCSV( )
{	
	LoadSubscription();

	m_clProdFileDtlMap.clear(); //Prod
	//m_clExchFileDtlMap.clear();
	
	m_clBlockProdSubFileMap.clear(); //Prod	
	m_clBlockExchSubFileMap.clear();

	FILE *fileIn(NULL);
	
	if( fopen_s(&fileIn, m_clScheduleInfo.csFilePath, "r") != 0 )
	{
		fnAddMessage( "Error while Opening fileIn" );
		return;
	}

	char chline[2048];
	std::string strLine("");	
	int lRecordRead(0);
	int iCountTT(0);
		
	CString csTemp = "Reading input File..";
	fnAddMessage( csTemp );
	this->RequestExternalUpdate();

	// Read Header...
	while( fgets(chline, 2048, fileIn) )
	{
		if( strchr(chline, '\n') )
		{
			strLine += chline;
			
			//ParseMessage(strLine); 
			strLine = "";
			lRecordRead++;
			break;
		}
		else
		{
			strLine += chline;
		}		
	}
	
	while( fgets(chline, 2048, fileIn) )
	{
		if(strchr(chline, '\n'))
		{
			strLine += chline;

			int pos = strLine.find('\n');
			strLine.erase(pos, 1);
			
			ParseLine(strLine); 

			strLine = "";
			lRecordRead++;
			iCountTT++;

			if( iCountTT == 1000 )
			{
				csTemp.Format("Reading line %d", lRecordRead);
				fnWriteMessage( csTemp );
				this->RequestExternalUpdate();
				iCountTT = 0;
			}
		}
		else
		{
			strLine += chline;
		}
	}

	fclose(fileIn);

	fnCloseAllOpenFiles( );

	fnSendEmail(1, lRecordRead);

	csTemp.Format("Total Row %d. Completed reading files.", lRecordRead);
	fnAddMessage( csTemp );

	//---------------------
	csTemp = "Processing prod file..";	
	fnAddMessage( csTemp );
	this->RequestExternalUpdate();	
		
	ProcessBlockProdSubscription();

	//---------------------
	csTemp = "Processing exch File..";	
	fnAddMessage( csTemp );
	this->RequestExternalUpdate();

	ProcessBlockExchSubscription();

	//---------------------
	csTemp = "Creating Prod zip File..";	
	fnAddMessage( csTemp );
	this->RequestExternalUpdate();
		
	fnCreateZip( );

	//---------------------
	csTemp = "Uploading to Dest account";	
	fnAddMessage( csTemp );
	this->RequestExternalUpdate();

	UploadSubscription();

	csTemp = "Process Completed..";
	fnAddMessage( csTemp );	
	this->RequestExternalUpdate();	
}

void ExecutionBLOCKS::ParseLine( std::string &ssLine )
{
	std::stringstream ss(ssLine);
	std::string item;
	
	BlockDetail clBlockDetail;
	while(std::getline(ss, item, ','))
	{
		if( item == "null" )
			clBlockDetail.strArray.push_back("");
		else
			clBlockDetail.strArray.push_back(item.c_str());
	}

	while( clBlockDetail.strArray.size() > m_niInputColumnCount )
	{
		clBlockDetail.strArray[enm_TICKER_SYMBOL_DESCRIPTION] = clBlockDetail.strArray[enm_TICKER_SYMBOL_DESCRIPTION]+","+clBlockDetail.strArray[enm_MARKET_SECTOR];
		clBlockDetail.strArray.erase(clBlockDetail.strArray.begin() + enm_MARKET_SECTOR);
	}

	fnAddInFiles( clBlockDetail );	
}

void ExecutionBLOCKS::fnAddInFiles( BlockDetail &blockRowData  )
{
	std::string strMsg;
	blockRowData.GetRowMessage(strMsg);

	std::string stExchFileName = blockRowData.strArray[enm_EXCH_CODE].c_str();
	std::string stProdFileName = blockRowData.strArray[enm_TICKER_SYMBOL].c_str();

	//BLOCP
	CString csExch_ProdFileName = stExchFileName.c_str();
	std::string stExch_ProdFileName = (csExch_ProdFileName = csExch_ProdFileName + "_" + stProdFileName.c_str()).GetString();

	//if( GetField(e_FilterProdExch)->GetValueInt() == 0 || m_clBlockDistProdMap.find( stExch_ProdFileName ) != m_clBlockDistProdMap.end() )
	{
		if( m_clProdFileDtlMap.find( stExch_ProdFileName ) == m_clProdFileDtlMap.end( ) )
		{
			m_clProdFileDtlMap[stExch_ProdFileName].strFileName = stExch_ProdFileName;		
		}

		fnAddRowInSubscription( strMsg, m_clProdFileDtlMap[stExch_ProdFileName], true );
	}

	////BLOCK
	//if( GetField(e_FilterProdExch)->GetValueInt() == 0 || m_clBlockDistExchMap.find( stExchFileName ) != m_clBlockDistExchMap.end() )	
	//{
	//	if( m_clExchFileDtlMap.find( stExchFileName ) == m_clExchFileDtlMap.end( ) )
	//	{
	//		m_clExchFileDtlMap[stExchFileName].strFileName = stExchFileName;		
	//	}

	//	fnAddRowInSubscription( strMsg, m_clExchFileDtlMap[stExchFileName], false );
	//}
}

void ExecutionBLOCKS::fnAddRowInSubscription( std::string &strMsg, FileDtl &blockSubDtl, bool bIsProduct )
{
	if( blockSubDtl.fpOut == NULL && blockSubDtl.iRowCount == 0 )
	{
		if( m_iOpenFileCount > 500 )
		{
			fnCloseAllOpenFiles( );
			m_iOpenFileCount = 0;
		}
				
		//First time file is created...
		CString csBasePath;
		if( bIsProduct )
			csBasePath = m_clScheduleInfo.csBLOCP_FilePath;
		else
			csBasePath = m_clScheduleInfo.csBLOCK_FilePath;
		
		CString csPath;
		csPath.Format("%s\\%s", csBasePath, blockSubDtl.strFileName.c_str());
		
		blockSubDtl.strFilePath = csPath;

		if( fopen_s(&blockSubDtl.fpOut, csPath,"w") )
			return;

		m_iOpenFileCount++;
		blockSubDtl.iRowCount = 1;
		//fputs( __strHeader.c_str(), blockSubDtl.fpOut );		

		fputs( strMsg.c_str(), blockSubDtl.fpOut);
	}
	else if( blockSubDtl.fpOut == NULL && blockSubDtl.iRowCount > 0 )
	{
		if( m_iOpenFileCount > 500 )
		{
			fnCloseAllOpenFiles( );
			m_iOpenFileCount = 0;
		}

		//First time file is created...
		CString csBasePath;
		if( bIsProduct )
			csBasePath = m_clScheduleInfo.csBLOCP_FilePath;
		else
			csBasePath = m_clScheduleInfo.csBLOCK_FilePath;
		
		CString csPath;
		csPath.Format("%s\\%s", csBasePath, blockSubDtl.strFileName.c_str());
		
		blockSubDtl.strFilePath = csPath;

		if( fopen_s(&blockSubDtl.fpOut, csPath,"a+") )
			return;

		m_iOpenFileCount++;
		//blockSubDtl.iRowCount = 1;
		//fputs( __strHeader.c_str(), blockSubDtl.fpOut );

		fputs( strMsg.c_str(), blockSubDtl.fpOut);
		blockSubDtl.iRowCount++;
	}
	else
	{
		fputs( strMsg.c_str(), blockSubDtl.fpOut);
		blockSubDtl.iRowCount++;
	}
}

void ExecutionBLOCKS::fnCloseAllOpenFiles( )
{
	FileDtlMap::iterator itr = m_clProdFileDtlMap.begin();

	while( itr != m_clProdFileDtlMap.end() )
	{
		if( itr->second.fpOut != NULL )
		{
			fclose( itr->second.fpOut );
			itr->second.fpOut = NULL;
		}
		itr++;
	}	

	/*itr = m_clExchFileDtlMap.begin();

	while( itr != m_clExchFileDtlMap.end() )
	{
		if( itr->second.fpOut != NULL )
		{	
			fclose( itr->second.fpOut );
			itr->second.fpOut = NULL;
		}

		itr++;
	}	*/
}

void fnGetExchange( CString &csExchange )
{
	if( csExchange == "CBOT" )
	{
		csExchange = "XCBT";
		return;
	}

	if( csExchange == "CME" )
	{
		csExchange = "XCME";
		return;
	}

	if( csExchange == "Comex" )
	{
		csExchange = "XCEC";
		return;
	}

	if( csExchange == "Nymex" )
	{
		csExchange = "XNYM";
		return;
	}
}


void ExecutionBLOCKS::LoadSubscription( )
{	
	m_clBlockProdSubDtlMap.clear(); 
	m_clBlockExchSubDtlMap.clear(); 

	//m_clBlockDistProdMap.clear();
	//m_clBlockDistExchMap.clear();
	
	CString csSingleAccnt = GetField(e_SingleAccount)->GetValueString();

	{
		CString sql;	
#ifdef _DEBUG
		sql.Format("SELECT CUST_NAME, CONTACT_EMAIL, DEST_ACCOUNT, FTP_PASSWORD, VENUE_CODE, ALL_PRODUCTS, PROD_CODE, BLOCK_CODE, EXCH_CODE FROM dbo.subscription_2015 where FILE_TYPE = 'BLOCP' and Active = 1 and DEST_ACCOUNT = 'ftp_beast'");
#else
		sql.Format("SELECT CUST_NAME, CONTACT_EMAIL, DEST_ACCOUNT, FTP_PASSWORD, VENUE_CODE, ALL_PRODUCTS, PROD_CODE, BLOCK_CODE, EXCH_CODE FROM dbo.subscription_2015 where FILE_TYPE = 'BLOCP' and Active = 1");
#endif

		_RecordsetPtr set;
		if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
		{
			SetErrorMessage(ErrorSeverity::e_Error, sql);
			return;
		}

		while( VARIANT_FALSE == set->adoEOF )
		{
			BLOCKSubscriptionDtl clBlockSub;

			//SETSTR(	clBlockSub.stCUST_NAME,		set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
			//SETSTR(	clBlockSub.stCONTACT_EMAIL,	set->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);	
			SETSTR(	clBlockSub.stDEST_ACCOUNT,	set->Fields->Item[_variant_t("DEST_ACCOUNT")]->Value);	
			SETSTR(	clBlockSub.stFTP_PASSWORD,	set->Fields->Item[_variant_t("FTP_PASSWORD")]->Value);	
			//SETSTR(	clBlockSub.stVENUE_CODE,		set->Fields->Item[_variant_t("VENUE_CODE")]->Value);	
			//SETSTR(	clBlockSub.stALL_PRODUCTS,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);	

			//SETSTR(	clBlockSub.stPROD_CODE,	set->Fields->Item[_variant_t("PROD_CODE")]->Value);	
			SETSTR(	clBlockSub.stBLOCK_CODE,	set->Fields->Item[_variant_t("BLOCK_CODE")]->Value);	
			SETSTR(	clBlockSub.stEXCH_CODE,	set->Fields->Item[_variant_t("EXCH_CODE")]->Value);

			clBlockSub.stEXCH_PROD_CODE = clBlockSub.stEXCH_CODE + "_" + clBlockSub.stBLOCK_CODE;

			std::pair <BLOCKSubDtlMap::iterator, BLOCKSubDtlMap::iterator> ret;
			ret = m_clBlockProdSubDtlMap.equal_range(clBlockSub.stDEST_ACCOUNT.GetString());

			bool bAlreadyExist = false;
			if( GetField(e_DestAccount)->GetValueInt() == 0 && clBlockSub.stDEST_ACCOUNT != csSingleAccnt )
				bAlreadyExist = true;

			for (BLOCKSubDtlMap::iterator it = ret.first; it != ret.second; ++it)
			{
				if( it->second.stEXCH_PROD_CODE == clBlockSub.stEXCH_PROD_CODE )
				{
					bAlreadyExist = true;
					break;
				}
			}

			if( !bAlreadyExist )
			{
				m_clBlockProdSubDtlMap.insert( std::pair<string, BLOCKSubscriptionDtl>(clBlockSub.stDEST_ACCOUNT, clBlockSub) );
				//m_clBlockDistProdMap[clBlockSub.stEXCH_PROD_CODE.GetString()] = true;
			}

			set->MoveNext();
		}

		set->Close();	
	}

	{
		CString sql;	
		sql.Format("SELECT CUST_NAME, CONTACT_EMAIL, DEST_ACCOUNT, FTP_PASSWORD, VENUE_CODE, ALL_PRODUCTS, PROD_CODE, BLOCK_CODE, EXCH_CODE FROM dbo.subscription_2015 where FILE_TYPE = 'BLOCK' and Active = 1");

		_RecordsetPtr set;
		if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
		{
			SetErrorMessage(ErrorSeverity::e_Error, sql);
			return;
		}

		while( VARIANT_FALSE == set->adoEOF )
		{
			BLOCKSubscriptionDtl clBlockSub;

			//SETSTR(	clBlockSub.stCUST_NAME,		set->Fields->Item[_variant_t("CUST_NAME")]->Value);	
			//SETSTR(	clBlockSub.stCONTACT_EMAIL,	set->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);	
			SETSTR(	clBlockSub.stDEST_ACCOUNT,	set->Fields->Item[_variant_t("DEST_ACCOUNT")]->Value);	
			SETSTR(	clBlockSub.stFTP_PASSWORD,	set->Fields->Item[_variant_t("FTP_PASSWORD")]->Value);	
			//SETSTR(	clBlockSub.stVENUE_CODE,		set->Fields->Item[_variant_t("VENUE_CODE")]->Value);	
			//SETSTR(	clBlockSub.stALL_PRODUCTS,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);	

			//SETSTR(	clBlockSub.stPROD_CODE,	set->Fields->Item[_variant_t("PROD_CODE")]->Value);	
			SETSTR(	clBlockSub.stBLOCK_CODE,	set->Fields->Item[_variant_t("BLOCK_CODE")]->Value);	
			SETSTR(	clBlockSub.stEXCH_CODE,	set->Fields->Item[_variant_t("EXCH_CODE")]->Value);

			std::pair <BLOCKSubDtlMap::iterator, BLOCKSubDtlMap::iterator> ret;
			ret = m_clBlockExchSubDtlMap.equal_range(clBlockSub.stDEST_ACCOUNT.GetString());

			bool bAlreadyExist = false;
			if( GetField(e_DestAccount)->GetValueInt() == 0 && clBlockSub.stDEST_ACCOUNT != csSingleAccnt )
				bAlreadyExist = true;

			for (BLOCKSubDtlMap::iterator it = ret.first; it != ret.second; ++it)
			{
				if( it->second.stEXCH_CODE == clBlockSub.stEXCH_CODE )
				{
					bAlreadyExist = true;
					break;
				}			
			}

			if( !bAlreadyExist )
			{
				m_clBlockExchSubDtlMap.insert( std::pair<string, BLOCKSubscriptionDtl>(clBlockSub.stDEST_ACCOUNT, clBlockSub) );
				//m_clBlockDistExchMap[clBlockSub.stEXCH_CODE.GetString()] = true;
			}

			set->MoveNext();
		}

		set->Close();	
	}

	CString csTemp;
	csTemp.Format("%d BLOCP Subscription loaded and %d BLOCK Subscription loaded", m_clBlockProdSubDtlMap.size(),  m_clBlockExchSubDtlMap.size() );
	fnAddMessage( csTemp );

	{
		CString sql;	
		sql.Format("EXEC Proc_Beast_Get_Subscriptions 'BLOCK', 2");

		_RecordsetPtr set;
		if( FAILED(m_clpdbInt->GetRecordset(&set, sql, false)) )
		{
			SetErrorMessage(ErrorSeverity::e_Error, sql);
			return;
		}

		int iAllProd(0);
		while( VARIANT_FALSE == set->adoEOF )
		{
			BLOCKSubscriptionDtl clBlockSub;

			//SETSTR(	clEODSub.stCUST_NAME,		set->Fields->Item[_variant_t("Customer_Id")]->Value);	
			//SETSTR(	clEODSub.stCONTACT_EMAIL,	set->Fields->Item[_variant_t("CONTACT_EMAIL")]->Value);	
			SETSTR(	clBlockSub.stDEST_ACCOUNT,	set->Fields->Item[_variant_t("FTPUserName")]->Value);	
			SETSTR(	clBlockSub.stFTP_PASSWORD,	set->Fields->Item[_variant_t("FTPPassword")]->Value);	
			//SETSTR(	clBlockSub.stVENUE_CODE,		set->Fields->Item[_variant_t("VENUE_CODE")]->Value);	
			SETLONG(	iAllProd,	set->Fields->Item[_variant_t("ALL_PRODUCTS")]->Value);	

			//SETSTR(	clBlockSub.stPROD_CODE,	set->Fields->Item[_variant_t("PROD_CODE")]->Value);	
			SETSTR(	clBlockSub.stBLOCK_CODE,	set->Fields->Item[_variant_t("Sub_Product_Code")]->Value);	
			SETSTR(	clBlockSub.stEXCH_CODE,	set->Fields->Item[_variant_t("Exchange")]->Value);

			//fnGetExchange( clBlockSub.stEXCH_CODE );

			if( iAllProd == 0 )
			{
				clBlockSub.stEXCH_PROD_CODE = clBlockSub.stEXCH_CODE + "_" + clBlockSub.stBLOCK_CODE;

				std::pair <BLOCKSubDtlMap::iterator, BLOCKSubDtlMap::iterator> ret;
				ret = m_clBlockProdSubDtlMap.equal_range(clBlockSub.stDEST_ACCOUNT.GetString());

				bool bAlreadyExist = false;
				if( GetField(e_DestAccount)->GetValueInt() == 0 && clBlockSub.stDEST_ACCOUNT != csSingleAccnt )
					bAlreadyExist = true;

				for (BLOCKSubDtlMap::iterator it = ret.first; it != ret.second; ++it)
				{
					if( it->second.stEXCH_PROD_CODE == clBlockSub.stEXCH_PROD_CODE )
					{
						bAlreadyExist = true;
						break;
					}
				}

				if( !bAlreadyExist && clBlockSub.stEXCH_CODE.GetLength() > 0 )
				{
					m_clBlockProdSubDtlMap.insert( std::pair<string, BLOCKSubscriptionDtl>(clBlockSub.stDEST_ACCOUNT, clBlockSub) );
					//m_clBlockDistProdMap[clBlockSub.stEXCH_PROD_CODE.GetString()] = true;
				}
			}
			else
			{
				std::pair <BLOCKSubDtlMap::iterator, BLOCKSubDtlMap::iterator> ret;
				ret = m_clBlockExchSubDtlMap.equal_range(clBlockSub.stDEST_ACCOUNT.GetString());

				bool bAlreadyExist = false;
				if( GetField(e_DestAccount)->GetValueInt() == 0 && clBlockSub.stDEST_ACCOUNT != csSingleAccnt )
					bAlreadyExist = true;

				for (BLOCKSubDtlMap::iterator it = ret.first; it != ret.second; ++it)
				{
					if( it->second.stEXCH_CODE == clBlockSub.stEXCH_CODE )
					{
						bAlreadyExist = true;
						break;
					}			
				}

				if( !bAlreadyExist && clBlockSub.stBLOCK_CODE.GetLength() > 0 )
				{
					m_clBlockExchSubDtlMap.insert( std::pair<string, BLOCKSubscriptionDtl>(clBlockSub.stDEST_ACCOUNT, clBlockSub) );
					//m_clBlockDistExchMap[clBlockSub.stEXCH_CODE.GetString()] = true;
				}
			}

			set->MoveNext();
		}

		set->Close();
	}

	csTemp;
	csTemp.Format("%d BLOCP Subscription loaded and %d BLOCK Subscription loaded", m_clBlockProdSubDtlMap.size(),  m_clBlockExchSubDtlMap.size() );
	fnAddMessage( csTemp );
}

void ExecutionBLOCKS::ProcessBlockProdSubscription()
{
	std::multimap<string, BLOCKSubscriptionDtl>::iterator it = m_clBlockProdSubDtlMap.begin();
	
	for (; it != m_clBlockProdSubDtlMap.end(); ++it)
	{
		CString csExch_Prod = it->second.stEXCH_CODE + "_" + it->second.stBLOCK_CODE;

		if( m_clProdFileDtlMap.find( csExch_Prod.GetString() ) == m_clProdFileDtlMap.end() )
			continue;  // this means no product file for that subscriptions..

		string strInputFile = m_clProdFileDtlMap[ csExch_Prod.GetString() ].strFilePath;
		
		if( m_clBlockProdSubFileMap.find( it->first ) == m_clBlockProdSubFileMap.end( ) )
		{
			m_clBlockProdSubFileMap[it->first].stDestAccount = it->second.stDEST_ACCOUNT;
			m_clBlockProdSubFileMap[it->first].iFileCount = 1;
		}

		FILE *fileIn;
		if( fopen_s(&fileIn, strInputFile.c_str(), "r") != 0 )
		{
			//CString strError = "Error while Opening fileIn";
			//GetField(e_Message)->SetValueString( strError );		
		}
		else
		{
			bool bContinue = true;
			do
			{
				BLOCKSubscriptionFile &clBLOCKSubscriptionFile = m_clBlockProdSubFileMap[ it->second.stDEST_ACCOUNT.GetString() ];

				if( clBLOCKSubscriptionFile.iRowCount == SPLITLINE )
				{
					fclose(clBLOCKSubscriptionFile.fpOut);

					clBLOCKSubscriptionFile.fpOut = NULL;
					clBLOCKSubscriptionFile.iFileCount++;
					clBLOCKSubscriptionFile.iRowCount = 0;
				}

				if( clBLOCKSubscriptionFile.fpOut == NULL )
				{	
					CString csOutputFolder;
					csOutputFolder.Format("%s\\%s",m_clScheduleInfo.csBLOCP_FilePath, it->second.stDEST_ACCOUNT);

					CreateDirectory( csOutputFolder, NULL);

					clBLOCKSubscriptionFile.strOutputFolderPath = csOutputFolder.GetString();

					CString csFileName;
					csFileName.Format("B%03d_%s.csv", clBLOCKSubscriptionFile.iFileCount, m_clScheduleInfo.csDateBlock);			

					CString csPath;
					csPath.Format("%s\\%s", csOutputFolder, csFileName);

					//clSubFileDtl.strFilePath = csPath.GetString();*
					if( fopen_s(&clBLOCKSubscriptionFile.fpOut, csPath,"w") )
						return;
					
					clBLOCKSubscriptionFile.iRowCount++;
					fputs( __strHeader.c_str(), clBLOCKSubscriptionFile.fpOut );
				}

				bContinue = CreateSubscriptionFile(clBLOCKSubscriptionFile,  fileIn);
			}
			while( bContinue );
		}		
	}

	BLOCKSubFileMap::iterator itr = m_clBlockProdSubFileMap.begin();

	while( itr != m_clBlockProdSubFileMap.end() )
	{		
		if( itr->second.fpOut != NULL )
		{
			fclose( itr->second.fpOut );
			itr->second.fpOut = NULL;
		}

		itr++;
	}
}

void ExecutionBLOCKS::ProcessBlockExchSubscription()
{
	std::multimap<string, BLOCKSubscriptionDtl>::iterator it = m_clBlockExchSubDtlMap.begin();
	
	for (; it != m_clBlockExchSubDtlMap.end(); ++it)
	{
		FileDtlMap::iterator itrProdFile = m_clProdFileDtlMap.begin();

		while( itrProdFile != m_clProdFileDtlMap.end() )
		{
			CString cstringExch_Prod = itrProdFile->first.c_str();

			if( cstringExch_Prod.Find( it->second.stEXCH_CODE ) == -1 )
			{
				itrProdFile++;
				continue;  // this means no product file for that subscriptions..
			}

			/*if( m_clExchFileDtlMap.find( it->second.stEXCH_CODE.GetString() ) == m_clExchFileDtlMap.end() )
			continue;  // this means no product file for that subscriptions..

			string strInputFile = m_clExchFileDtlMap[ it->second.stEXCH_CODE.GetString() ].strFilePath;*/
			string strInputFile = itrProdFile->second.strFilePath;

			if( m_clBlockExchSubFileMap.find( it->first ) == m_clBlockExchSubFileMap.end( ) )
			{
				m_clBlockExchSubFileMap[it->first].stDestAccount = it->second.stDEST_ACCOUNT;
				m_clBlockExchSubFileMap[it->first].iFileCount = 1;
			}

			FILE *fileIn;
			if( fopen_s(&fileIn, strInputFile.c_str(), "r") != 0 )
			{
				//CString strError = "Error while Opening fileIn";
				//GetField(e_Message)->SetValueString( strError );		
			}
			else
			{
				bool bContinue = true;
				do
				{
					BLOCKSubscriptionFile &clBLOCKSubscriptionFile = m_clBlockExchSubFileMap[ it->second.stDEST_ACCOUNT.GetString() ];

					if( clBLOCKSubscriptionFile.iRowCount == SPLITLINE )
					{
						fclose(clBLOCKSubscriptionFile.fpOut);

						clBLOCKSubscriptionFile.fpOut = NULL;
						clBLOCKSubscriptionFile.iFileCount++;
						clBLOCKSubscriptionFile.iRowCount = 0;
					}

					if( clBLOCKSubscriptionFile.fpOut == NULL )
					{	
						CString csOutputFolder;
						csOutputFolder.Format("%s\\%s",m_clScheduleInfo.csBLOCK_FilePath, it->second.stDEST_ACCOUNT);

						CreateDirectory( csOutputFolder, NULL);

						clBLOCKSubscriptionFile.strOutputFolderPath = csOutputFolder.GetString();

						CString csFileName;
						csFileName.Format("B%03d_%s.csv", clBLOCKSubscriptionFile.iFileCount, m_clScheduleInfo.csDateBlock);			

						CString csPath;
						csPath.Format("%s\\%s", csOutputFolder, csFileName);

						//clSubFileDtl.strFilePath = csPath.GetString();*
						if( fopen_s(&clBLOCKSubscriptionFile.fpOut, csPath,"w") )
							return;

						clBLOCKSubscriptionFile.iRowCount++;
						fputs( __strHeader.c_str(), clBLOCKSubscriptionFile.fpOut );
					}

					bContinue = CreateSubscriptionFile(clBLOCKSubscriptionFile,  fileIn);
				}
				while( bContinue );
			}

			itrProdFile++;
		}
	}

	BLOCKSubFileMap::iterator itr = m_clBlockExchSubFileMap.begin();

	while( itr != m_clBlockExchSubFileMap.end() )
	{		
		if( itr->second.fpOut != NULL )
		{
			fclose( itr->second.fpOut );
			itr->second.fpOut = NULL;
		}

		itr++;
	}
}

bool ExecutionBLOCKS::CreateSubscriptionFile(BLOCKSubscriptionFile &clSubFileDtl, FILE *fileIn )
{
	char chline[2048];
	std::string strLine("");

	while( fgets(chline, 2048, fileIn) )
	{
		if(strchr(chline, '\n'))
		{
			strLine += chline;

			fputs(strLine.c_str(), clSubFileDtl.fpOut);
			
			strLine = "";
			clSubFileDtl.iRowCount++;

			if( clSubFileDtl.iRowCount == SPLITLINE )
				return true;
		}
		else
		{
			strLine += chline;
		}
	}

	fclose(fileIn);
	
	return false;
}

void ExecutionBLOCKS::fnCreateZip( )
{
	CString csTemp;
	BLOCKSubFileMap::iterator itr = m_clBlockProdSubFileMap.begin();
	while( itr != m_clBlockProdSubFileMap.end() )
	{
		csTemp.Format("Creating zip %s Account",itr->second.stDestAccount);
		fnWriteMessage( csTemp );
		this->RequestExternalUpdate();

		if( itr->second.iFileCount > 0 )
		{
			CString csZipName;
			csZipName.Format("%s\\%s_%s.zip", itr->second.strOutputFolderPath, "BLOCP", m_clScheduleInfo.csDateBlock);

			itr->second.strOutPutFileName = csZipName;

			HZIP hz = CreateZip(csZipName,0);

			CString csFilePath;
			CString csFileName;

			for( int iFile = 0; iFile < itr->second.iFileCount; iFile++ )
			{				
				csFileName.Format("B%03d_%s.csv", iFile + 1, m_clScheduleInfo.csDateBlock);			 

				csFilePath.Format("%s\\%s", itr->second.strOutputFolderPath, csFileName);
				ZipAdd(hz, csFileName, csFilePath);			
			}
			CloseZip(hz);

			//		/*HZIP hz = CreateZip("c:\\simple1.zip",0);
			//		ZipAdd(hz, "E001_simple.csv", "\\\\powervault3\\CME_s3\\daily\\endofday\\20150805_Output\\E\\1000\\E001_20150805.txt");
			//		ZipAdd(hz,"E002_simple.csv", "\\\\powervault3\\CME_s3\\daily\\endofday\\20150805_Output\\E\\1000\\E002_20150805.txt");
			//		CloseZip(hz);*/			
		}

		itr++;
	}

	csTemp = "Creating Exchange zip File..";	
	fnAddMessage( csTemp );
	this->RequestExternalUpdate();

	itr = m_clBlockExchSubFileMap.begin();
	while( itr != m_clBlockExchSubFileMap.end() )
	{
		csTemp.Format("Creating zip %s Account",itr->second.stDestAccount);
		fnWriteMessage( csTemp );
		this->RequestExternalUpdate();

		if( itr->second.iFileCount > 0 )
		{
			CString csZipName;
			csZipName.Format("%s\\%s_%s.zip", itr->second.strOutputFolderPath, "BLOCK", m_clScheduleInfo.csDateBlock);

			itr->second.strOutPutFileName = csZipName;

			HZIP hz = CreateZip(csZipName,0);

			CString csFilePath;
			CString csFileName;

			for( int iFile = 0; iFile < itr->second.iFileCount; iFile++ )
			{				
				csFileName.Format("B%03d_%s.csv", iFile + 1, m_clScheduleInfo.csDateBlock);			 

				csFilePath.Format("%s\\%s", itr->second.strOutputFolderPath, csFileName);
				ZipAdd(hz, csFileName, csFilePath);			
			}
			CloseZip(hz);

			//		/*HZIP hz = CreateZip("c:\\simple1.zip",0);
			//		ZipAdd(hz, "E001_simple.csv", "\\\\powervault3\\CME_s3\\daily\\endofday\\20150805_Output\\E\\1000\\E001_20150805.txt");
			//		ZipAdd(hz,"E002_simple.csv", "\\\\powervault3\\CME_s3\\daily\\endofday\\20150805_Output\\E\\1000\\E002_20150805.txt");
			//		CloseZip(hz);*/			
		}

		itr++;
	}
}

void ExecutionBLOCKS::UploadSubscription()
{	
	CString csTemp;

	CString __csDestinationPath = ((ListField*)GetField(e_S3FTPBucket))->GetShortString();
	//CString __csAccount = ((ListField*)GetField(e_DestAccount))->GetShortString(); // This is Test Account

	BLOCKSubFileMap::iterator itr =  m_clBlockProdSubFileMap.begin( );

	while( itr != m_clBlockProdSubFileMap.end() )
	{
		CString csSourcePath = itr->second.strOutPutFileName;

		CString csDestinationFolder;
		CString csDestAccount;
		//if( GetField(e_DestAccount)->GetValueInt() == 1 )
			csDestAccount = itr->second.stDestAccount;
		//else
			//csDestAccount = __csAccount;

		csDestinationFolder.Format("%s%s/BLOCP/",__csDestinationPath,  csDestAccount );

		csTemp.Format("Uploading Prod zip %s Account", csDestAccount);
		fnWriteMessage( csTemp );
		this->RequestExternalUpdate();

		CString csAWSCommand;
		csAWSCommand.Format(_T("aws s3 cp \"%s\" --profile s3user \"%s\""),csSourcePath, csDestinationFolder);					
		itr->second.iUploadReturn = system(csAWSCommand);	

		CString csFileName = csSourcePath;
		csFileName.Replace(itr->second.strOutputFolderPath,"");
		csFileName.Replace("\\","");

		if( itr->second.iUploadReturn == 0 )
		{
			CString stroutput;
			stroutput.Format("cmd %s", csAWSCommand );
			fnAddMessage( stroutput );
			
			unsigned long lsize = GetFileSize(csSourcePath.GetString());

			fnInsertDeliveryReport("BLOCP", csDestAccount, csFileName, lsize, csAWSCommand);
		}
		else
		{
			CString stroutput;
			stroutput.Format("Error in upload cmd %s", csAWSCommand );
			fnAddMessage( stroutput );

			fnInsertErroReport("BLOCP", csDestAccount, csFileName,"",csAWSCommand);
		}

		itr->second.bUploaded = true;
		itr++;

		//if( GetField(e_DestAccount)->GetValueInt() == 0 )
			//break;
	}
		
	itr =  m_clBlockExchSubFileMap.begin( );

	while( itr != m_clBlockExchSubFileMap.end() )
	{
		CString csSourcePath = itr->second.strOutPutFileName;

		CString csDestinationFolder;
		CString csDestAccount;
		//if( GetField(e_DestAccount)->GetValueInt() == 1 )
			csDestAccount = itr->second.stDestAccount;
		//else
			//csDestAccount = __csAccount;

		csDestinationFolder.Format("%s%s/BLOCK/",__csDestinationPath,  csDestAccount);

		csTemp.Format("Uploading Exch zip %s Account", csDestAccount);
		fnWriteMessage( csTemp );
		this->RequestExternalUpdate();

		CString csAWSCommand;
		csAWSCommand.Format(_T("aws s3 cp \"%s\" --profile s3user \"%s\""),csSourcePath, csDestinationFolder);					
		itr->second.iUploadReturn = system(csAWSCommand);	

		CString csFileName = csSourcePath;
		csFileName.Replace(itr->second.strOutputFolderPath,"");
		csFileName.Replace("\\","");

		if( itr->second.iUploadReturn == 0 )
		{
			CString stroutput;
			stroutput.Format("cmd %s", csAWSCommand );
			fnAddMessage( stroutput );
			
			unsigned long lsize = GetFileSize(csSourcePath.GetString());

			fnInsertDeliveryReport("BLOCK", csDestAccount, csFileName, lsize, csAWSCommand);
		}
		else
		{
			CString stroutput;
			stroutput.Format("Error in upload cmd %s", csAWSCommand );
			fnAddMessage( stroutput );

			fnInsertErroReport("BLOCK", csDestAccount, csFileName,"",csAWSCommand);
		}

		itr->second.bUploaded = true;
		itr++;

		//if( GetField(e_DestAccount)->GetValueInt() == 0 )
			//break;
	}
}

void ExecutionBLOCKS::fnInsertDeliveryReport(CString csPrdouct, CString csDestAccount, CString csFileName, unsigned long &lFileSize, CString &csAwscmd )
{
	CString csCheckSum = "";
	CString csExchange = "";

	CString csDeliveryTime;
	COleDateTime oleDateTime = COleDateTime::GetCurrentTime();
	csDeliveryTime.Format("%02d-%02d-%02d %02d:%02d", oleDateTime.GetYear(), oleDateTime.GetMonth(), oleDateTime.GetDay(), oleDateTime.GetHour(), oleDateTime.GetMinute());

	CString csSql;
	csSql.Format("EXEC Proc_Beast_Submit_CME_Daily_Delivery_Report '%s', '%s', '%s', %u, '%s','%s','%s','%s'", csPrdouct, csDestAccount, csFileName, lFileSize, csCheckSum, csDeliveryTime, csExchange, csAwscmd);
	
	_RecordsetPtr set;
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)csSql, true)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSql);
		PrintRawMessage(csSql);
		return;
	}
}

void ExecutionBLOCKS::fnInsertErroReport(CString csPrdouct, CString csDestAccount, CString csFileName, CString csExchnage, CString &csErrorCmd )
{	
	CString csSql;
	csSql.Format("EXEC Proc_Beast_Submit_CME_Daily_Error_Report '%s', '%s', '%s', '%s', '%s'", csPrdouct, csDestAccount, csFileName, csExchnage, csErrorCmd);
	
	_RecordsetPtr set;
	if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)csSql, true)))
	{
		SetErrorMessage(ErrorSeverity::e_Error, csSql);
		PrintRawMessage(csSql);
		return;
	}
}


const CString	__strSmtp	= "email-smtp.us-east-1.amazonaws.com";
const CString	__strLogin	= "AKIAIERFOJMRYCRRAHYQ";
const CString	__strPassword = "AvauV+bh9qVBlRQuCI5u9GMG2O1N2hajyhzE7MhyYANC"; 
const CString	__strSender	= "cmenotifications@thebeastapps.com";
const int		__iPort = 587;

void  ExecutionBLOCKS::fnSendEmail(int iType, int iRowCount)
{
	CSmtp mail;

	mail.SetSMTPServer(__strSmtp, __iPort, true);
	mail.SetSecurityType((SMTP_SECURITY_TYPE)USE_TLS);		
	mail.SetLogin(__strLogin);
	mail.SetPassword(__strPassword);
			
	//CC
	/*CString strSendTo		= GetField(e_T0)->GetValueString();
	CString strCC			= GetField(e_CC)->GetValueString();
	CString strSubject		= GetField(e_Subject)->GetValueString();
	CString strBody			= GetField(e_Body)->GetValueString();*/
	
	CString strSendTo		= "vcmops@thebeastapps.com,";
	CString strCC			= "mpatel@thebeastapps.com,dmodi@thebeastapps.com,mvpatel@thebeastapps.com,";
	CString strSubject		= "";
	CString strBody			= "";
	CString strTemp			= "";

	if( iType == 1 )
	{
		strSubject = "BLOCK Notification";
		//m_clScheduleInfo.csFilePath
		strBody.Format("\nBLOCK File Name: %s\n\n",m_clScheduleInfo.csFilePath);

		strTemp.Format("Total Row Count: %d\n\n",iRowCount);
		strBody += strTemp;

		strTemp.Format("Thanks\nBeast Apps Team\n");
		strBody += strTemp;
	}
	else if( iType == 2 )
	{
		strSubject = "BLOCK Notification";
		//m_clScheduleInfo.csFilePath
		strBody.Format("\nBLOCK File Name: %s\n\n",m_clScheduleInfo.csFilePath);

		strTemp.Format("Status: Processed successfully\n\n",iRowCount);
		strBody += strTemp;

		strTemp.Format("Thanks\nBeast Apps Team\n");
		strBody += strTemp;
	}

	
	//++++++++++++++++++++++++Send To+++++++++++++++++++++++
	if( !strSendTo.IsEmpty() )
	{
		if( strSendTo.Right(1).CompareNoCase(",") != 0 )
		{
			strSendTo = strSendTo + ",";
		}		
	}	
	
	if( strSendTo.Find(",") != -1 )
	{
		while(!strSendTo.IsEmpty())
		{
			CString tmpSendTo = strSendTo.Left(strSendTo.Find(","));			
			strSendTo.Delete(0,strSendTo.Find(",")+1);		
			tmpSendTo.TrimLeft();
			tmpSendTo.TrimRight();

			if(!tmpSendTo.IsEmpty())
				mail.AddRecipient(tmpSendTo);
		}
	}		
	else
	{			
		if( !strSendTo.IsEmpty() )
			mail.AddRecipient(strSendTo);
	}
	//+++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++ CC +++++++++++++++++++++++
	if( !strCC.IsEmpty() )
	{
		if( strCC.Right(1).CompareNoCase(",") != 0 )
		{
			strCC = strCC + ",";
		}		
	}		

	if( strCC.Find(",") != -1 )
	{
		while(!strCC.IsEmpty())
		{
			CString tmpCC = strCC.Left(strCC.Find(","));			
			strCC.Delete(0,strCC.Find(",")+1);		
			tmpCC.TrimLeft();
			tmpCC.TrimRight();

			if(!tmpCC.IsEmpty())
				mail.AddCCRecipient(tmpCC);
		}
	}		
	else
	{			
		if(!strCC.IsEmpty())
			mail.AddCCRecipient(strCC);
	}
	//+++++++++++++++++++++++++++++++++++++++++++++++
		
	mail.SetSenderMail(__strSender);
				
	if( !strBody.IsEmpty() )
		mail.AddBody(strBody);
				
	if( !strSubject.IsEmpty() )
		mail.SetSubject(strSubject);
		
	try
	{			
		mail.Send(NULL);		
		//GetField(e_Status)->SetValueString("Mail Sent successfully.");  	
	}
	catch(ECSmtp e)
	{
		CString stemp;
		stemp.Format("Mail Failed...! - %s", e.GetErrorText());		
		//GetField(e_Status)->SetValueString(stemp); 
		fnAddMessage( stemp );
		return;
	}
	catch(...)
	{
		CString stemp;
		stemp.Format("Mail Failed...! - Error Id: %d", GetLastError());		
		//GetField(e_Status)->SetValueString(stemp);  
		fnAddMessage( stemp );
		return;
	}

	fnAddMessage( "EMail sent successfully");
}