
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include "afxinet.h"
#include <tlhelp32.h>
#include <string>

namespace BCP 
{
	static const char * sm_FatalSQLStates[] = 
	{
		{"08S01"}, //Communication Link Failure
		{"08001"}  //Client unable to establish connection
	};
}

BF_NAMESPACE_BEGIN

#include <Microsoft/Windows/v6.0A/Include/odbcss.h>
#pragma comment(lib,"/3rdParty/Microsoft/Windows/v6.0A/Lib/Odbcbcp.lib")

struct CME_SecDef
{
	char	Exch_Code	[500];
	char	Prod_Code	[500];
	char	Prod_desc	[500];
	//char	Sec_ID	[500];
	long 	Sec_ID;
	char	FOI_Type	[500];
	char	Group_Code [100];
	int		Spread_In;
	char	FileName	[500];
	
	CME_SecDef()
	{
		Exch_Code[0]= '\0';
		Prod_Code[0]= '\0';
		Prod_desc[0]= '\0';
		//Sec_ID[0]= '\0';
		Sec_ID=			0;
		FOI_Type[0]= '\0';
		Group_Code[0]= '\0';
		Spread_In=0;
		FileName[0]= '\0';
	}
};

struct CME_SecDefType
{
	std::string	GEP_Code;
	std::string	FOI;
	int		Sprd_In; 

	CME_SecDefType()
	{
		GEP_Code = "";
		FOI		= "";
		Sprd_In	= 0;
	}
};

struct FileSchedule
{
	int ScheduleId;
	CString Product;
	CString InputFormat;
	CString OutputFormat;
	COleDateTime FileProcessTime;
	int SameNextDay;
	CString DayOfWeek;
	CString InputPath;
	CString OutputPath;
	CString AlertEmailId;
	CString ExternalEmaild;
	int RetryInterval;
	CString RecordCreatedBy;
	COleDateTime RecordCreateDateTime;
	
	FileSchedule()
	{
		ScheduleId = 0;
		SameNextDay = -1;
		RetryInterval = -1;
	}
};


struct stMessages
{
	CString csDateTime;
	CString csAction;
	CString csStatus;

	int nError;
};

class FTPConnection
{
public:
	FTPConnection();
	~FTPConnection();	
//	
//	bool Connect(CString sFTPServer, CString sUserName, CString sPassword, int nFTPPort, bool bPassiveMode);
//	bool Disconnect();
	bool GetFiles(CString sSrcFile, CString sDstFolder);
//	bool GetFolder(CString sSrcFolder, CString sDstFolder, CString sFileType);
//	bool DeleteFiles(CString sSrcFile);
//	bool DeleteFolder(CString sFolderName);
//	bool RenameFiles(CString sOldName, CString sNewName);
//	bool ChangeFolder(int iOption, CString sFolderName);
//	bool CreateFolder(CString sFolderName);
//	void FillRemoteFileList(CString csPath, CStringArray &strFileNameArray);
//	void FillLocalFileList(CString csPath, CStringArray &strFileNameArray);
//	BOOL FillRemoteFilesFromFolder(CString csPath, CStringArray &strFileNameArray, CString csCriteria);
//	BOOL FillLocalFilesFromFolder(CString csPath, CStringArray &strFileNameArray, CString csCriteria);
//	bool DownloadFile(CString &csSrcFile, CString &csDstFile, CString csDirectory);
//	BOOL CreateLocalDirectory(CString csDirectory);
//
	CString GetErrorMessage() { return m_szError; };
//	bool IsConnected() { return m_pFtpConnection != NULL; };
//
private:

	CString m_szError;
	CString m_szCurrDir;
//
	CInternetSession *m_pInternetSession;
	CFtpConnection	 *m_pFtpConnection;
};


class CMESecDefParser : public BaseImage
{
public:
	
	enum FieldIDs
	{
		e_PrimaryServerStatus = 106,
		e_BackupServerStatus = 107,

		e_Update		  = 152,
		e_ExternalEmail	  = 153,
		e_InternalEmail	  = 154,
		e_DownloadHr	  = 171,
		e_DownloadMinute  = 181,
		e_RetryInterval   = 157,
		e_ExternalEmailTiming   = 191,
		e_MailServer      = 159,
		e_UnZip			  = 160,
		//e_Zip			  = 161,

		e_UseBackupServer = 111,
		e_EmailNotification = 108,
		e_DoManual = 110,
		e_Message = 204,
		//e_ConStatus = 209,

		e_NextDownloadMsg = 120,

		e_Server = 200,
		e_BackupServer = 210,
		e_Port = 201,
		e_Username = 202,
		e_Password = 203,

		e_ManualParsingDate = 121,
		//e_ManualUpdloadDate  = 122,
		e_dntTouch			 = 126,

		e_SelectedScedule = 141,

		e_ManualParser = 303,
		//e_ManualUpdload = 304,
		
		e_File_Path	= 205,
		e_Local_Dn_Path = 206,
		e_Local_Up_Path = 207,
		e_FTP_Up_Path	= 208,

		e_Connect = 300,
		e_Download = 301,
		//e_Upload = 302,

		e_Message_DateTime	= 350,
		e_Message_Action	= 450,
		e_Message_Status	= 550,
		
	};
	
	CMESecDefParser();
	~CMESecDefParser();	
	
	bool InitData();		
	void Recalculate();	

	//void GetConnectionParams();
	void DisplayMessages();
	
	void SendMailOnError(stMessages &stMessage, CString csFileName, bool bExternalAlso = false);
	CString GetFileName( BFDate &clBFDate,int index);
	bool GetFiledetails(CString csFileName);
	//void fnGetFileDownloded();
//	void SetFileDownloadFlag(CString csFileName, CString csDateTime);

	//bool fnConnect(bool bBackServer = false);
	//void fnDownload();
	//bool	fnZip( );
	//CString unzip(CString csFilepath,CString csFileName);
	void fnAddMessage(stMessages &clarFTPMessages);

	std::vector<FileSchedule> m_clFileschedules;

	std::map<std::string, CME_SecDefType> SecDefTypeMap;	

	//**** FOR BCP ****
	const static int	MAX_CONN_OUT = 255;
	SQLCHAR				m_ConnStrOut[MAX_CONN_OUT];
	SQLSMALLINT			m_cbConnStrOut;
 	SQLHENV				m_henv;
	HDBC				m_hdbc;
	CString				m_csConnect;

	//EDI_BOND	m_AllEDIBondView;
	//EDI_ISSUER	m_AllEDIISUUERView;
	//EDI_SCMS	m_AllEDISCMSView;
	//EDI_SCEXC	m_AllEDISCEXCView;
	//EDI_CPOPT	m_AllEDICPOPTView;
	
	CME_SecDef m_CME_SecDefView;

	RETCODE ConnectBCP();
	void DisconnectBCP();
	bool ExtractODBCerror();

	RETCODE InitSECDEFBCP();

	void GetFileProcessSchedule();
	void SubmitSECDEFBCP(CString sFileName);
	void SubmitFileName(CString csFileName, CString csProcessStart, CString csProcessEnd);
	//****************
	void ExtractTokensToArray(std::string s,std::string Delimiters);
	bool FindSecDefType(std::string GEP_Code);
	void fnDoThreadProcessing();
	bool m_bThredRunning;
	long m_lTotalReadDisplay;
	
	long m_lEDecompress;
	long m_lEParsing;
	long m_lTotalRead;
	long m_lTotalProcess;

	int m_iOperation;
	long 	Sec_MsgLenghth;

private:

	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;
	int m_nOperation;

	COleDateTime m_dtDownloadTime;
	COleDateTime m_dtMainDownloadTime;
	COleDateTime m_dtMainDownloadTime1stEmail;
	COleDateTime m_dtMainDownloadTime2ndEmail;

	int m_iEmailSent;

	CString m_csUploadFile;
	CString m_csUploadFileBackup;
	CString m_csDownLoadFile;

	std::vector<stMessages> m_vMessages;
	//FTPConnection	 m_cFtpConnection;
	
	int m_iRetryCountDownload;
};

BF_NAMESPACE_END


