// Copyright (C) 2000 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#pragma once


#include <portalhandler/BFInterestRateBaseApp.hpp>
#include <shared/DBConnection.hpp>

#define _ColumnCount 17

BF_NAMESPACE_BEGIN

struct FileDetail
{
	CString csFileName;
	CString csFilePath;
	BFDate bfFileDate;
	unsigned long lFileSize;
	int iStatus;
};	

struct SecDef
{
	CString csProductCode;
	CString csFUIType;
	int iSpread;

	std::string stffMsg;
};	

struct OpneFiles
{
	FILE *pFileOut;	
	std::string csFileName;

	OpneFiles( )
	{
		pFileOut = NULL;
		csFileName = "";
	}
};	

typedef std::map<long, SecDef> SecDefMap;

typedef std::map<std::string, std::vector<std::string>> MessageMap;
typedef std::map<std::string, OpneFiles> OpenFileMap;

typedef std::map<std::string, std::map<long, bool> > SecIDMap;

class CMEMDProcessManager : public BFInterestRateBaseApp 
{

public:
	enum Field
	{
		e_Message = 61,
		e_FileType = 200,
		e_Year = 201,
		e_Month = 202,
		
		e_Exchange		= 203,		
		e_FileFormat	= 204,
		e_Status		= 205,
		
		e_UncomparessedPath	= 100,
		e_FileList			= 101,

		e_OutputBasePath	= 102,
		e_OutputDate		= 103,

		e_SecDefFilePath	= 104,

		e_LoadFromDB		= 112,
		e_StartProcess		= 113,

		e_LoadSecDefDB		= 114,

		e_DisplayFileName	= 1000,
		e_DisplayFileSize	= 1050,

		e_DisplayFileNameDB = 2000,
		e_DisplayFileSizeDB	= 3000,
		e_DisplayFileStatusDB = 3050,

		e_PgUpBase	= 90,
		e_PgDnBase	= 91,

		e_PgUpBaseDB	= 92,
		e_PgDnBaseDB	= 93,		

		e_StartTime	= 95,
		e_EndTime	= 96,

	};

	CMEMDProcessManager();
	virtual ~CMEMDProcessManager();
	
	bool RealInitData();
	void RealRecalculate();
		
	int m_numofRows;
	int m_curTopRow;

	int m_numofRowsDB;
	int m_curTopRowDB;
	
	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;
	
	std::vector<FileDetail> m_clFileList;
	std::vector<FileDetail> m_clFileListDB;

	bool	m_bIsThreadRunning;
	int		m_lTotalRead;

	MessageMap m_clMessageMap;
	int m_iTotalMessage;

	SecDefMap  m_clSecDefMap;
	OpenFileMap m_clOpenFileMap;
	int	m_iActualOpenFile;

	std::string m_csBasePath;
	std::string m_csFileDate;

	SecIDMap m_clSecIDMap;

private:
	void CMEMDProcessManager::DisplayFileList( );	
	void CMEMDProcessManager::fnPageUpDown( );
	void CMEMDProcessManager::LoadFileListFromDB( );

public:
	void CMEMDProcessManager::Parse();
	void CMEMDProcessManager::ParseMessage( std::string &strLine );
	void CMEMDProcessManager::ParseIncreamentalRefreshMessage( std::string &strLine );
	long CMEMDProcessManager::ParseSecurityIDMessage( std::string &strMessage );

	void CMEMDProcessManager::fnWriteMessageinOutputFile();
	//void CMEMDProcessManager::LoadSecDefFromDB( );
	void CMEMDProcessManager::LoadSecDefFromFile( );
	FILE * CMEMDProcessManager::fnGetOutputFilePointer( const  std::string &strOutputFileName );
	bool CMEMDProcessManager::fnGetSecDefFromMaster(const long &laSecId);

	std::string CMEMDProcessManager::fnGetFileNameFromSecId( const long &laSecId );

	void CMEMDProcessManager::ParseSecDefMessage( std::string &strLine );
	void CMEMDProcessManager::ParseSecStatusMessage( std::string &strLine );

	void CMEMDProcessManager::fnCreateDirectory( );

	void CMEMDProcessManager::UpdateFileStatus( int iStatus, long &lOffset );
	void CMEMDProcessManager::UpdateFileSeekOffset(  long lOffset );

	void CMEMDProcessManager::ParseQuoteRequestMessage( std::string &strLine );

	bool CMEMDProcessManager::fnPushdMessage( std::string strFileName, long &lSecId );
	void CMEMDProcessManager::ParseSecdefLine(std::string &strLine );
	void CMEMDProcessManager::fnAddExtratag(std::string &strLine);
};

BF_NAMESPACE_END
