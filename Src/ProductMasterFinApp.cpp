// Copyright (C) 2000 TheBEAST.COM, Inc..  All rights reserved.
// This software may not be reproduced, republished, broadcast or otherwise
// distributed in any form or medium (written, electronic or otherwise)
// without the prior written permission of TheBEAST.COM, Inc..

#include <stdafx.h>

#include "ProductMasterFinApp.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BF_NAMESPACE_USE

IMPLEMENT_IMAGE(ProductMasterFinApp)

ProductMasterFinApp::ProductMasterFinApp()
{
}

ProductMasterFinApp::~ProductMasterFinApp()
{
}

bool ProductMasterFinApp::RealInitData()
{ 
	m_curTopRow= 0;
	for(m_numofRows = 0; GetField(e_ProdNameBase + m_numofRows); m_numofRows++);

	// DB Connection....
	BF_Reg_Key_Ptr pKey(new BF_Reg_Key(HKEY_LOCAL_MACHINE, "Software\\TheBEAST\\Application\\TradeCapture"));

	CString csServerName;
	RegString strServerName(pKey, "ServerName","BeastDB");
	csServerName = strServerName.get_value().c_str();

	bool bResult = m_clDBConn.InitDatabase(this, csServerName, "CME","watchdog","watchdog","");
	if( !bResult )
	{
		SetErrorMessage(ErrorSeverity::e_Error, "Could not initialize datastore!");
		return true;
	}

	m_clpdbInt = m_clDBConn.GetDBInterfaceNew();
	// DB Connection Completed....

	COleDateTime dtToday = COleDateTime::GetCurrentTime();
	BFDate bfToday;
	bfToday.SetYearMonthDate(dtToday.GetYear(),dtToday.GetMonth() - 1, dtToday.GetDay() ); 

	int iDofW = bfToday.GetDayOfWeek();
	bfToday.OffsetDate(-iDofW);

	GetField(e_FileDate)->SetValueDate( bfToday );

	bLoadedFromDB = true;

	return true;
}

void ProductMasterFinApp::RealRecalculate()
{	
	fnDownloadFile( );

	if( IsFieldNew( e_LoadDataFromFile ) ) 
	{
		ReadProductFileCSV( );
	}
	else if( IsFieldNew( e_LoadDataFromDB ) || IsInitialRecalc() ) 
	{
		ReadProductFromDB( );
	}
	else if( IsFieldNew(e_ProdNameFilter) || IsFieldNew(e_ExchangeFilter) || IsFieldNew(e_ProductFilter) || IsFieldNew(e_GroupFilter)  || IsFieldNew(e_MDCodeFilter) || IsFieldNew(e_BLOCKCodeFilter) || IsFieldNew(e_EODCodeFilter) )
	{
		if( bLoadedFromDB )
			ReadProductFromDB( );
		else 
			ReadProductFileCSV( );
	}

	GetField(e_SubmitToDB)->SetEnabled(!bLoadedFromDB);
			
	fnPageUpDown( );
	fnDisplayDetails( );
}

void ProductMasterFinApp::ReadProductFromDB( )
{
	GetField(200)->SetValueInt(0);

	bLoadedFromDB = true;
	m_clProdDetailV.clear();
	m_curTopRow = 0;

	CString strProdNameFilter = GetField(e_ProdNameFilter)->GetValueString();
	CString strExchangeFilter = GetField(e_ExchangeFilter)->GetValueString();
	CString strProdFilter = GetField(e_ProductFilter)->GetValueString();
	CString strGroupFilter = GetField(e_GroupFilter)->GetValueString();

	CString strMDCodeFilter = GetField(e_MDCodeFilter)->GetValueString();
	CString strBLOCKCodeFilter = GetField(e_BLOCKCodeFilter)->GetValueString();
	CString strEODCodeFilter = GetField(e_EODCodeFilter)->GetValueString();

	if( GetField(e_ProdNameFilter)->IsBlankState( ) )
		strProdNameFilter = "";

	if( GetField(e_ExchangeFilter)->IsBlankState( ) )
		strExchangeFilter = "";

	if( GetField(e_ProductFilter)->IsBlankState( ) )
		strProdFilter = "";

	if( GetField(e_GroupFilter)->IsBlankState( ) )
		strGroupFilter = "";

	if( GetField(e_MDCodeFilter)->IsBlankState( ) )
		strMDCodeFilter = "";

	if( GetField(e_BLOCKCodeFilter)->IsBlankState( ) )
		strBLOCKCodeFilter = "";

	if( GetField(e_EODCodeFilter)->IsBlankState( ) )
		strEODCodeFilter = "";
	
	strExchangeFilter.MakeUpper();
	strProdFilter.MakeUpper();
	strGroupFilter.MakeUpper();

	strMDCodeFilter.MakeUpper();
	strBLOCKCodeFilter.MakeUpper();
	strEODCodeFilter.MakeUpper();

	CString sql;
	sql.Format("EXEC Proc_Beast_Get_CME_All_Products_Mst");

	_RecordsetPtr set;

	DB_TRY
	{
		if (FAILED(m_clpdbInt->GetRecordset(&set, (LPCSTR)sql, false)))
		{
			SetErrorMessage(ErrorSeverity::e_Error, sql);
			PrintRawMessage(sql);
			return;
		}


		ProdDetail clProdDetail;
		while( VARIANT_FALSE == set->adoEOF )
		{
			int iCol(0);
			CString cstemp;
			while( iCol < _ColumnCount )
			{
				SETSTR(	cstemp,	set->Fields->Item[_variant_t(__csTitles[iCol])]->Value);	
				cstemp.MakeUpper();
				clProdDetail.strArray.push_back(cstemp.GetString());
				iCol++;
			}

			bool bOK = true;			
			if( strProdNameFilter.GetLength() > 0 )
			{
				if( clProdDetail.strArray[enm_PROD_NAME].find( strProdNameFilter ) == -1 )
					bOK = false;
			}

			if( strExchangeFilter.GetLength() > 0 )
			{
				if( clProdDetail.strArray[enm_EXCH_CODE].compare( strExchangeFilter ) != 0 )
					bOK = false;
			}

			if( strProdFilter.GetLength() > 0 ) 
			{
				if( clProdDetail.strArray[enm_PROD_CODE].compare( strProdFilter ) != 0)
					bOK = false;
			}

			if( strGroupFilter.GetLength() > 0 )
			{
				if( clProdDetail.strArray[enm_GROUP_CODE].compare(strGroupFilter ) != 0)
					bOK = false;
			}

			if( strMDCodeFilter.GetLength() > 0 )
			{
				if( clProdDetail.strArray[enm_MD_CODE].compare(strMDCodeFilter ) != 0)
					bOK = false;
			}

			if( strBLOCKCodeFilter.GetLength() > 0 )
			{
				if( clProdDetail.strArray[enm_BLOCK_CODE].compare(strBLOCKCodeFilter ) != 0)
					bOK = false;
			}

			if( strEODCodeFilter.GetLength() > 0 )
			{
				if( clProdDetail.strArray[enm_EOD_CODE].compare(strEODCodeFilter ) != 0)
					bOK = false;
			}

			if( bOK )
				m_clProdDetailV.push_back( clProdDetail );	

			clProdDetail.strArray.clear();
			set->MoveNext();
		}
		set->Close();
	}
	DB_CATCH(sql);	
}

void ProductMasterFinApp::fnDownloadFile( )
{
	if( !IsFieldNew(e_DownloadFile) )
	{
		return;
	}

	CString csS3CME = ((ListField*)GetField(e_S3InputBucket))->GetShortString();
	CString csProfile = ((ListField*)GetField(e_S3InputBucket))->GetLongString();

	BFDate bfDate = GetField(e_FileDate)->GetValueDate();
	CString csDate; 
	csDate.Format("%d%02d%02d",bfDate.GetYear(), bfDate.GetMonth()+1, bfDate.GetDate());

	CString strFullInputPath;
	strFullInputPath.Format("%s/daily/productfile/%s/products_%s.gz",csS3CME,csDate,csDate);

	CString csBasePath = GetField(e_BasePath)->GetValueString();	
	CString csDestinationFolder;
	csDestinationFolder.Format("%s\\%s\\",csBasePath, csDate);
	
	CString csAWSCommand;
	csAWSCommand.Format(_T("aws s3 cp --profile %s %s %s"), csProfile, strFullInputPath, csDestinationFolder);					
	int iValue = system(csAWSCommand);	

	if( iValue != 0 )
	{
		SetErrorMessage(ErrorSeverity::e_Error, csAWSCommand);
		PrintRawMessage(csAWSCommand);
		return;
	}

	CString csFilePath;
	csFilePath.Format("%sproducts_%s.gz", csDestinationFolder,  csDate);
	
	CString csUncomFilePath = csFilePath;
	csUncomFilePath.Replace(".gz",".txt");	

	DeleteFile( csUncomFilePath ); // Already exist file delete it.

	CString csCMD;
	csCMD.Format("gzip.exe -d -c %s > %s",csFilePath, csUncomFilePath);

	iValue = system(csCMD);	

	if( iValue != 0 )
	{
		SetErrorMessage(ErrorSeverity::e_Error, csCMD);
		PrintRawMessage(csCMD);
		return;
	}

	GetField(e_BasePath)->SetValueString( csUncomFilePath );
}

#define FSUCCESS 0
void ProductMasterFinApp::ReadProductFileCSV( )
{	
	bLoadedFromDB = false;
	m_clProdDetailV.clear();
	m_curTopRow = 0;

	CString csFileNamePath = GetField(e_BasePath)->GetValueString();
	
	FILE *fileIn(NULL);
	
	if( fopen_s(&fileIn, csFileNamePath, "r") != FSUCCESS )
	{
		CString strError = "Error while Opening fileIn";
		SetErrorMessage(ErrorSeverity::e_Error,strError);
		return;
	}
	
	char line[1024] = {0};
	CString sLine, sWord;
	int iColumnCount(0);
	ProdDetail clProdDetail;

	CString strExchangeFilter = GetField(e_ExchangeFilter)->GetValueString();
	CString strProdFilter = GetField(e_ProductFilter)->GetValueString();
	CString strGroupFilter = GetField(e_GroupFilter)->GetValueString();

	if( GetField(e_ExchangeFilter)->IsBlankState( ) )
		strExchangeFilter = "";

	if( GetField(e_ProductFilter)->IsBlankState( ) )
		strProdFilter = "";

	if( GetField(e_GroupFilter)->IsBlankState( ) )
		strGroupFilter = "";
	
	strExchangeFilter.MakeUpper();
	strProdFilter.MakeUpper();
	strGroupFilter.MakeUpper();

	// Read Header...
	if(fgets(line, 1025, fileIn))
	{
		sLine = line;		
		const std::string &ssLine = sLine.GetString();
		std::stringstream ss(ssLine);
		std::string item;

		while(std::getline(ss, item, '|'))
		{
			iColumnCount++;
		}
	}

	while(fgets(line, 1025, fileIn))
	{
		sLine = line;		
		int curPos(0);

		const std::string &ssLine = sLine.GetString();

		std::stringstream ss(ssLine);
		std::string item;

		while(std::getline(ss, item, '|'))
		{
			clProdDetail.strArray.push_back(item.c_str());			
		}

		while( clProdDetail.strArray.size() > iColumnCount )
		{
			clProdDetail.strArray[0] = clProdDetail.strArray[0]+","+clProdDetail.strArray[1];
			clProdDetail.strArray.erase(clProdDetail.strArray.begin() + 1);
		}

		bool bOK = true;
		if( strExchangeFilter.GetLength() > 0 )
		{
			if( clProdDetail.strArray[enm_EXCH_CODE].compare( strExchangeFilter ) != 0 )
				bOK = false;
		}

		if( strProdFilter.GetLength() > 0 ) 
		{
			if( clProdDetail.strArray[enm_PROD_CODE].compare(strProdFilter ) != 0)
				bOK = false;
		}

		if( strGroupFilter.GetLength() > 0 )
		{
			if( clProdDetail.strArray[enm_GROUP_CODE].compare(strGroupFilter ) != 0)
				bOK = false;
		}

		if( bOK )
			m_clProdDetailV.push_back( clProdDetail );	
		clProdDetail.strArray.clear();
	}

	fclose(fileIn);
}

void ProductMasterFinApp::fnPageUpDown( )
{
	int iTotalRecord = m_clProdDetailV.size( );

	if( IsFieldNew(e_CurTopRecord) && GetField(e_CurTopRecord)->GetValueInt( ) < iTotalRecord  )
		m_curTopRow = GetField(e_CurTopRecord)->GetValueInt( );

	if( m_curTopRow < 0 )
		m_curTopRow = 0;

	PAGE_UP_DOWN_OR_LEFT_RIGHT(e_PgUpBase,e_PgDnBase,iTotalRecord,m_numofRows,m_curTopRow);	
}

void ProductMasterFinApp::fnDisplayDetails( )
{
	CString strTemp;
	strTemp.Format("%d Records Loaded From %s",m_clProdDetailV.size( ), bLoadedFromDB? "DB":"File");
	GetField(e_TotalRecord)->SetTitle(strTemp);

	GetField(e_CurTopRecord)->SetValueInt( m_curTopRow );

	int iRow(0);
	for( int iI = m_curTopRow; iI < m_clProdDetailV.size( ); iI++, iRow++ )
	{
		if( iRow >= m_numofRows )
			break;

		for( int iJ = 0; iJ < _ColumnCount; iJ++ )
		{
			if( iJ == 0 )
				GetField(e_ProdNameBase + iRow )->SetToolTipText( m_clProdDetailV[iI].strArray[enm_PROD_NAME + iJ].c_str() );

			GetField(e_ProdNameBase + iRow + iJ*e_columnOffset )->SetValueString( m_clProdDetailV[iI].strArray[enm_PROD_NAME + iJ].c_str() );
		}		
	}

	while( iRow < m_numofRows )
	{
		for( int iJ = 0; iJ < _ColumnCount; iJ++ )
		{
			if( iJ == 0 )
				GetField(e_ProdNameBase + iRow )->SetToolTipText( "" );

			GetField(e_ProdNameBase + iRow + iJ*e_columnOffset )->SetValueString( "" );
		}	

		iRow++;
	}
}