
#pragma once
#include <shared/BaseImage.hpp>
#include <shared/DBConnection.hpp>
#include <shared/MDDDataHandler.hpp>

BF_NAMESPACE_BEGIN

using namespace std;

struct ScheduleInfo
{
	int		iRetInterval;
	int		iRetryCount;
	bool	bValid;
	int		iStatus;	

	COleDateTime clMainScheduleDtTime;
	COleDateTime clScheduleDtTime;
	CString		 csDate;

	CString		csGlobextFolder;
};

enum enmOperation
{
	OP_Download,
	OP_ListFiles,
	OP_Process,
	OP_ProcessCompleted,
	OP_None
};

struct BBOSubscriptionDtl
{
	BBOSubscriptionDtl()
	{		
		stCUST_NAME = "";
		stCONTACT_EMAIL = "";
		stDEST_ACCOUNT = "";
		stFTP_PASSWORD = "";
		stVENUE_CODE = "";
		stALL_PRODUCTS = "";
		stPROD_CODE = "";
		stBBO_CODE = "";
		stEXCH_CODE = "";
	}	
	
	CString stCUST_NAME;
	CString stCONTACT_EMAIL;
	CString stDEST_ACCOUNT;
	CString stFTP_PASSWORD;
	CString stVENUE_CODE;
	CString stALL_PRODUCTS;
	CString stPROD_CODE;
	CString stBBO_CODE;
	CString stEXCH_CODE;	
};

typedef std::multimap<std::string, BBOSubscriptionDtl> BBOSubDtlMap;

struct FileDtl
{
	std::string strFileName;
	std::string strFilePath;
	std::string strFolderPath;
	
	std::string strExch_code;
	std::string strProd_code;	
	std::string strFutOpt;
	std::string strSpread;

	unsigned long lFileSize;

	FILE *fpOut;
	int iRowCount;

	FileDtl( )
	{
		strFileName = "";
		strFilePath = "";
		fpOut = NULL;
		iRowCount = 0;
	}
};

struct ReportTotalFile 
{
	CString csDestAccount;
	int iFileCount;
	unsigned long lFileSize;
};

struct ReportExchangeTotalFile 
{
	CString csDestAccount;
	CString csExchange;
	int iFileCount;
	unsigned long lFileSize;
};

struct ReporFile 
{
	CString csDestAccount;
	CString csFileName;	
	CString csDeliveryTime;		
	unsigned long lFileSize;
};

typedef std::map<std::string, FileDtl> FileDtlMap;
typedef std::map<CString, bool> DistItemMap;

typedef std::vector<ReportTotalFile> ReportTotalFileV;
typedef std::vector<ReportExchangeTotalFile> ReportExchangeTotalFileV;
typedef std::map<CString, std::vector<ReporFile> > ReporFileMapV;

class ExecutionBBO : public BaseImage
{
public:
	
	enum FieldIDs
	{
		e_NextDownloadMsg = 50,	
		e_MsgTitle		  = 65,
		e_OutputPath	  = 62,
		e_SingleAccount   = 63,

		e_ReloadSchedule  = 100,
		e_DateFilter	  = 95,
		e_ManualRun		  = 96,
		e_Process		  = 97,
		e_RtryInterval	  = 98,
						
		e_Year		= 201,
		e_Month		= 202,
		e_Day		= 215,
		e_Hour		= 213,
		e_Minute	= 214,

		e_S3CMEBucket = 60,
		e_DestAccount = 58,
		e_S3FTPBucket = 59,

		e_LogMsg	= 4000,
		e_pgUP		= 92,
		e_pgDN		= 93,
	};
	
	ExecutionBBO();
	~ExecutionBBO();	

	bool InitData();		
	void Recalculate();	
private:

	void ExecutionBBO::fnAddMessage( CString csaNewMsg );
	void ExecutionBBO::fnDisplayMsg();
	
	DBConnection m_clDBConn;
	DBInterfaceNew	*m_clpdbInt;
	
	int m_numofRows;
	int m_curTopRow;

	static const int m_niInputColumnCount	= 55;
	static const int m_niOutputColumnCount	= 55;

	BaseField *fld_OutputPath;

public:
	bool m_bIsTrheadCompleted;
	bool m_bIsThreadRunning;
	
	CString m_csMsg;
	int m_iTotalCount;
	int m_iOpenFileCount;
	
	CCriticalSection	m_MsgCriticalSec;

	void fnWriteMessage(CString csString )
	{
		CSingleLock clMsgSingleLock(&m_MsgCriticalSec);

		clMsgSingleLock.Lock();

		m_csMsg = csString.GetString();

		clMsgSingleLock.Unlock();
	}

	CString fnReadMessage( )
	{
		CSingleLock clMsgSingleLock(&m_MsgCriticalSec);

		clMsgSingleLock.Lock();

		CString csStr = m_csMsg.GetString();

		clMsgSingleLock.Unlock();

		return csStr;
	}	

	void ExecutionBBO::fnGetNextSchedule( );
	void ExecutionBBO::fnUpdateScheduleStatus(int iStatus );

	void ExecutionBBO::fnRecalc1( );
	void ExecutionBBO::fnCopyS3FiletoLocal( );
	void ExecutionBBO::ProcessBBOFiles( );
	void ExecutionBBO::LoadSubscription( );

	void ExecutionBBO::Recurse(LPCTSTR pstr);
	void ExecutionBBO::SplitFileNameString(CString cStrFileName, string delimiter, vector<string> &result);

	void ExecutionBBO::UploadSubscription();
	bool ExecutionBBO::GetFilePathForProduct(const string &stProdCode, string &stFolderPath, string &stFilePath);
	bool ExecutionBBO::GetFilePathForExchange(const string &stExchCode, string &stFilePath, string &stFolderPath, int &iarStart);

	void ExecutionBBO::fnGenerateReports( );

	void ExecutionBBO::fnInsertDeliveryReport(CString csPrdouct, CString csDestAccount, CString csFileName, CString csExchangeCode, unsigned long &lFileSize, CString &csAwscmd );

	void ExecutionBBO::fnInsertErroReport(CString csPrdouct, CString csDestAccount, CString csFileName, CString csExchnage, CString &csErrorCmd );

	void  ExecutionBBO::fnSendEmail(int iType, int iRowCount);

	unsigned long  GetFileSize(std::string const &path) 
	{
		WIN32_FIND_DATA data;
		HANDLE h = FindFirstFile(path.c_str(), &data);
		if (h == INVALID_HANDLE_VALUE)
			return -1;

		FindClose(h);

		return data.nFileSizeLow | data.nFileSizeHigh << 32;
	}

	void ExecutionBBO::fnGetCheksumList( );

	ScheduleInfo m_clScheduleInfo;
	enmOperation m_enmOperation;

	FileDtlMap m_clProdFileDtlMap; //Prod
	
	BBOSubDtlMap m_clBBOProdSubDtlMap; //Prod subscription
	BBOSubDtlMap m_clBBOExchSubDtlMap; //Exchange subscriptin

	DistItemMap m_clDistDestAccount;

	ReportTotalFileV m_clReportTotalFileV;
	ReportExchangeTotalFileV m_clReportExchangeTotalFileV;
	ReporFileMapV m_clReporFileMapV;
	//------------------------------	
};

BF_NAMESPACE_END


