#include "StdAfx.h"
#include "TNS.h"

#include <math.h>
#include <sstream>
#include <algorithm>
#include "zip.h"
#include "unzip.h"
#include "BBOConstants.h"
#include "CME_BBO_TNS_DECODE.hpp"

BF_NAMESPACE_USE

// Test comment

CTNS::~CTNS(void)
{
}

bool CTNS::dirExists(const std::string& dirName_in)
{
	DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
	if (ftyp == INVALID_FILE_ATTRIBUTES)
		return false;  //something is wrong with your path!

	if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
		return true;   // this is a directory!

	return false;    // this is not a directory!
}

void CTNS::setProcessParameters()
{
	if(_pImage)
	{	
		CString csFUTOPT,csInputFile,csOutputPath;
		setExchange(std::string((LPCTSTR)_pImage->GetField(e_filter_exchange)->GetDisplayString()));
		setInerval(_pImage->GetField(e_interval)->GetValueInt());
		setPartition(_pImage->GetField(e_tns_part)->GetValueInt());		

		csOutputPath= _pImage->GetField(e_gen_outputfile)->GetValueString();
		csOutputPath.Trim();
		SetOutputTSFilePath(std::string(csOutputPath));
		
		csInputFile = _pImage->GetField(e_gen_inputfile)->GetValueString();
		csInputFile.Trim();
		OpenBBOFile(std::string((LPCTSTR)csInputFile));
		OpenExceptionFile();
	}
}

bool CTNS::OpenBBOFile(std::string strFileName)
{
	try
	{

		std::size_t found = strFileName.find(".zip");
		if (found!=std::string::npos)
			_strInputFileName =	createUnzip(strFileName);
		else
			_strInputFileName = strFileName;

		int iIndex = _strInputFileName.find_last_of("\\");

		_strInputPath = _strInputFileName.substr(0,iIndex+1);

		_strIniFile = _strInputFileName + ".ini";

		
		fpInputFile.open(_strInputFileName.c_str());
		return true;
	}
	catch (CMemoryException* e)
	{
		CString csErrorMsg;
		std::cout<<"Exception::OpenBBOFile Error: CMemoryException"<<std::endl;
		e->Delete();
		return false;
	}
	catch (CFileException* e)
	{
		std::cout<<"Exception::OpenBBOFile Error: CFileException"<<std::endl;
		e->Delete();
		return false;
	}
	catch (CException* e)
	{
		std::cout<<"Exception::OpenBBOFile Error:CException"<<std::endl;
		e->Delete();
		return false;
	}
}


bool CTNS::OpenExceptionFile()
{
	try
	{
		
		_strExceptionFileName = _strInputFileName + ".not";
		fpExceptionFile.open(_strExceptionFileName.c_str(),std::ofstream::out | std::ofstream::app);
		return true;
	}
	catch (CMemoryException* e)
	{
		CString csErrorMsg;
		std::cout<<"Exception::OpenExceptionFile Error: CMemoryException"<<std::endl;
		e->Delete();
		return false;
	}
	catch (CFileException* e)
	{
		std::cout<<"Exception::OpenExceptionFile Error: CFileException"<<std::endl;
		e->Delete();
		return false;
	}
	catch (CException* e)
	{
		std::cout<<"Exception::OpenExceptionFile Error:CException"<<std::endl;
		e->Delete();
		return false;
	}
}



bool CTNS::Process()
{
	long lCnt=0;
	try
	{
		_mapFileDtl.clear();
		_mapTNSMetaData.clear();
		GotoLine(fpInputFile,GetFileProcessCount(_strInputFileName));
		std::string strLine;	
		while(!fpInputFile.eof())
		{
			getline(fpInputFile,strLine);

			if (strLine.length() == BBO_STANDARD_LENGH || strLine.length() == BBO_OLD_LENGTH) 
			{
				std::string strProd = strLine.substr(23, 3); 
				trim2(strProd);
				std::string strFO = strLine.substr(26, 1);
				trim2(strFO);
				std::string strKey = _strExchange + "_" + strProd + "_" + strFO;
				ConvertRecord(strKey,_strExchange,strProd,strFO,strLine);
			}
			else
			{
				std::cout<<"The message " << strLine.c_str() << " will be skipped because it is malformed"<<std::endl;	
				fpExceptionFile << strLine.c_str()<<std::endl;
			}	
			
			if((lCnt %_interval == 0))
			{
				WriteFileProcessCount(_strInputFileName,lCnt);
				updateImageFields(lCnt);
			}
			lCnt++;
		}
				
		WriteFileProcessCount(_strInputFileName,lCnt);
	}
	catch (CMemoryException* e)
	{
		e->Delete();
	}
	catch (CFileException* e)
	{
		e->Delete();
	}
	catch (CException* e)
	{
		e->Delete();
	}

	fpInputFile.close();
	fpExceptionFile.close();
	CloseOpenFiles();
	createLastZip();
	GenereateMetaData();

	char buff[100];
	sprintf(buff,"DONE  !!  Lines Processed: %ld",lCnt);
	_pImage->GetField(e_filter_status)->SetValueString(buff);
	CString csOutput;
	csOutput +=" ";
	csOutput +=buff;

	UpdateOutputField(std::string(csOutput.GetBuffer()));
		return true;
}

void CTNS::GenereateMetaData()
{
	std::map<std::string,stTNSMetaData>::iterator it = _mapTNSMetaData.begin();
	CString csOutput;
	for(;it!=_mapTNSMetaData.end();it++)
	{
		char buff[100];
		sprintf(buff,"\r\n Key: %s  count: %ld  ",it->first.c_str(),it->second._count);
		csOutput +=buff;
		std::cout<<"Key: "<<it->first<<"  value: "<<it->second._count<<std::endl;
	}

	UpdateOutputField(std::string(csOutput.GetBuffer()));
}
bool CTNS::ConvertRecord(std::string & strKey,std::string & strExch,std::string & strProd, std::string & strFO,std::string & strLine)
{
	std::string strout="";
	strout.append(strLine.substr(0, 8));
	strout.append(DELIM);

	strout.append(formatTime(strLine.substr(8, 6)));
	strout.append(DELIM);

	std::string strNum = strLine.substr(14, 8);
	trim2(strNum);

	strout.append(strNum);
	strout.append(DELIM);

	strout.append(strLine.substr(22, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(23, 3));
	strout.append(DELIM);

	strout.append(strLine.substr(26, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(27, 4));
	strout.append(DELIM);

	std::string strNum1 = strLine.substr(31, 5);
	trim2(strNum1);

	strout.append(strNum1);
	strout.append(DELIM);

	std::string strPrice1 = strLine.substr(36, 7);
	trim2(strPrice1);

	std::string strPrice2 = strLine.substr(43, 1);
	trim2(strPrice2);

	strout.append(formatPrice(strPrice1, strPrice2));
	strout.append(DELIM);

	strPrice1 = strLine.substr(44, 7);
	trim2(strPrice1);

	strPrice2 = strLine.substr(51, 1);
	trim2(strPrice2);

	strout.append(formatPrice(strPrice1, strPrice2));
	strout.append(DELIM);

	strout.append(strLine.substr(52, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(53, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(54, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(55, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(56, 2));
	strout.append(DELIM);

	strout.append(strLine.substr(58, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(59, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(60, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(61, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(62, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(63, 1));
	strout.append(DELIM);

	strout.append(formatEntryDate(strLine));
	strout.append(DELIM);

	strout.append(_strExchange);
	strout.append(DELIM);
		
	std::vector<BBOSubscriptionDtl>::iterator it = ((CME_BBO_TNS_DECODE*)_pImage)->m_clBBOSubV.begin();
		
	for(;it!=((CME_BBO_TNS_DECODE*)_pImage)->m_clBBOSubV.end();it++)
	{
		if(((strExch.compare(it->stEXCH_CODE) ==0)) && ((it->stALL_PRODUCTS == "1")  || ((strProd.compare(it->stPROD_CODE) == 0))))
		{
			
				CString csPath;
				csPath.Format("%s%s\\%s.csv", _strOutputFilePath.c_str(), it->stDEST_ACCOUNT.GetBuffer(),GetTime().c_str());

				std::map<CString,TNSFileDtl>::iterator itTNS = _mapFileDtl.find(csPath);

				if(itTNS!=_mapFileDtl.end())
				{
					fputs( strout.c_str(), itTNS->second.fp);
					fputs("\n",itTNS->second.fp);
					it->iRowCount++;
					itTNS->second._SplitCount++;
					it->stOutputFileName = csPath;
					
					if((itTNS->second._SplitCount % _partition)  == 0)
					{
						fclose(itTNS->second.fp);
						createSingleZip(itTNS->second);
						itTNS->second.fp =NULL;
						int fileCount = itTNS->second._fileCount;
						int splitcount = itTNS->second._SplitCount;
						std::vector<CString> tmpVecFileList = itTNS->second._vecFileList;
						_mapFileDtl.erase(itTNS);
						
						fileCount = fileCount + 1;
						splitcount = splitcount + 1;
						
						TNSFileDtl objTNSFileDtl;
						objTNSFileDtl.strOutputFileName.Format("%s%s\\%s_%d.csv", _strOutputFilePath.c_str(), it->stDEST_ACCOUNT.GetBuffer(),GetTime().c_str(),fileCount);
						objTNSFileDtl._SplitCount = splitcount ;
						objTNSFileDtl._fileCount = fileCount;
						objTNSFileDtl._vecFileList = tmpVecFileList;
						objTNSFileDtl._vecFileList.push_back(objTNSFileDtl.strOutputFileName);
						
						if( fopen_s(&objTNSFileDtl.fp, objTNSFileDtl.strOutputFileName,"w") )
							return 0;

						fputs("T.Date,T.Time,Sequence,Session Ind,Symbol,C/P/F,Contract Delivery,Volume,Strike Price,T.Price,A/B,IND,MKQ,C/O,VOE,PC,CAN,INS,F/L,CAB,BKI,Entry Date,exch_code\n",objTNSFileDtl.fp);
						fputs(strout.c_str(),objTNSFileDtl.fp);
						fputs("\n",objTNSFileDtl.fp);
						_mapFileDtl.insert(std::pair<CString,TNSFileDtl>(csPath,objTNSFileDtl));
						std::string strOutput = "Generating File ";
						strOutput += objTNSFileDtl.strOutputFileName.GetBuffer();
						UpdateOutputField(strOutput);
												
					}
					
				}
				else
				{
					CString strPath;
					strPath.Format("%s%s\\",_strOutputFilePath.c_str(), it->stDEST_ACCOUNT.GetBuffer());
					std::string strdir = strPath;
					
					TNSFileDtl objTNSFileDtl;

					objTNSFileDtl._fileCount = 0;
					objTNSFileDtl.strOutputFileName.Format("%s%s\\%s_%d.csv", _strOutputFilePath.c_str(), it->stDEST_ACCOUNT.GetBuffer(),GetTime().c_str(),objTNSFileDtl._fileCount);
					objTNSFileDtl._SplitCount =1;
					objTNSFileDtl._vecFileList.push_back(objTNSFileDtl.strOutputFileName);
										

					if(!dirExists(strdir))
						CreateDirectory(strdir.c_str(), NULL);

					if( fopen_s(&objTNSFileDtl.fp, objTNSFileDtl.strOutputFileName,"w") )
						return 0;

					it->iRowCount =1;

					fputs("T.Date,T.Time,Sequence,Session Ind,Symbol,C/P/F,Contract Delivery,Volume,Strike Price,T.Price,A/B,IND,MKQ,C/O,VOE,PC,CAN,INS,F/L,CAB,BKI,Entry Date,exch_code\n",objTNSFileDtl.fp);
										
					fputs(strout.c_str(),objTNSFileDtl.fp);
					fputs("\n",objTNSFileDtl.fp);


					csPath.Format("%s%s\\%s.csv", _strOutputFilePath.c_str(), it->stDEST_ACCOUNT.GetBuffer(),GetTime().c_str());

					_mapFileDtl.insert(std::pair<CString,TNSFileDtl>(csPath,objTNSFileDtl));
					it->stOutputFileName = csPath;

					std::string strOutput = "Generating File ";
					strOutput += objTNSFileDtl.strOutputFileName.GetBuffer();
					UpdateOutputField(strOutput);
				}

			}
		}

	return true;
}

void CTNS::CloseOpenFiles()
{
	std::map<CString,TNSFileDtl>::iterator itTNS = _mapFileDtl.begin();
	for(;itTNS!=_mapFileDtl.end();itTNS++)
	{
		if(itTNS->second.fp)
		{
			fclose(itTNS->second.fp);

		}
	}

}

std::string CTNS::formatTime(std::string  trdtime)
{
	switch (trdtime.length()) {
	case (1):
		return ONEDIGETTIME + trdtime;
	case (2):
		return TWODIGETTIME + trdtime;
	case (3):
		return THREEDIGETTIME + trdtime.substr(0, 1) + COL
			+ trdtime.substr(1);
	case (4):
		return FOURDIGETTIME + trdtime.substr(0, 2) + COL
			+ trdtime.substr(2);
	case (5):
		return FIVEDIGETTIME + trdtime.substr(0, 1) + COL
			+ trdtime.substr(1, 3) + COL + trdtime.substr(3);
	default:
		return trdtime.substr(0, 2) + COL + trdtime.substr(2, 4)
			+ COL + trdtime.substr(4, 6);

	}
}


std::string CTNS::formatEntryDate(std::string line) 
{
	if (line.length() == 70) 
	{
		std::string strtmp = line.substr(64, 6);
		trim2(strtmp);
		return TWENTY + strtmp;
	} 
	else 
	{
		return EMPTY_ENTRY_DATE;
	}
}


void CTNS::trim2(std::string & str)
{
	std::string::size_type pos = str.find_last_not_of(' ');
	if(pos != std::string::npos) {
		str.erase(pos + 1);
		pos = str.find_first_not_of(' ');
		if(pos != std::string::npos) str.erase(0, pos);
	}
	else str.erase(str.begin(), str.end());
}


std::string CTNS::formatPrice(std::string inPrice, std::string decimalLoc) 
{
	if (!_strcmpi(inPrice.c_str(),"")) 
	{
		return PRICEZERO;
	} 
	else
	{
		if (!_strcmpi(decimalLoc.c_str(),""))
		{
			return inPrice;
		} 
		else 
		{
			double idx = atof(decimalLoc.c_str());
			double price = atof(inPrice.c_str());

			std::ostringstream os;
			os << double(price / pow(10, idx));
			std::string tmpPrice = os.str();

			int dicl = tmpPrice.find(DOT);
			switch (tmpPrice.substr(dicl + 1).length()) {
			case (1):
				return tmpPrice + SIXZERO;
			case (2):
				return tmpPrice + FIVEZERO;
			case (3):
				return tmpPrice + FOURZERO;
			case (4):
				return tmpPrice + THREEZERO;
			case (5):
				return tmpPrice + TWOZERO;
			case (6):
				return tmpPrice + ONEZERO;
			default:
				return tmpPrice;
			}

		}
	}
}

void CTNS::updateImageFields(long & lCnt)
{
	char buff[512];
	sprintf(buff,"Lines Processed: %ld",lCnt);
	_pImage->GetField(e_filter_status)->SetValueString(buff);
	_pImage->RequestExternalUpdate();
}



void CTNS::createSingleZip(TNSFileDtl & objTNSFileDtl)
{
	CString strZipOutFileName = objTNSFileDtl.strOutputFileName;
	strZipOutFileName.Replace(".csv",".zip");

	int iIndex = objTNSFileDtl.strOutputFileName.ReverseFind('\\');
	CString fileName = objTNSFileDtl.strOutputFileName.Mid(iIndex);

	HZIP hz = CreateZip(strZipOutFileName.GetBuffer(),0);
	ZRESULT res = 	ZipAdd(hz,fileName.GetBuffer(),  objTNSFileDtl.strOutputFileName.GetBuffer());
	if(res == 0)
	{
		remove(objTNSFileDtl.strOutputFileName.GetBuffer());
	}
	CloseZip(hz);
}

void CTNS::createLastZip()
{
	std::map<CString,TNSFileDtl>::iterator itTNS = _mapFileDtl.begin();

	for(;itTNS!=_mapFileDtl.end();itTNS++)
	{
		if(itTNS->second.strOutputFileName.Find(".csv")!= -1)
		{

			CString strZipOutFileName = itTNS->second.strOutputFileName;
			strZipOutFileName.Replace(".csv",".zip");

			int iIndex = itTNS->second.strOutputFileName.ReverseFind('\\');
			CString fileName = itTNS->second.strOutputFileName.Mid(iIndex);

			HZIP hz = CreateZip(strZipOutFileName.GetBuffer(),0);
			ZRESULT res = 	ZipAdd(hz,fileName.GetBuffer(),  itTNS->second.strOutputFileName.GetBuffer());
			if(res == 0)
			{
				remove(itTNS->second.strOutputFileName.GetBuffer());
			}
			CloseZip(hz);
		}
	}
}

std::string CTNS::createUnzip(std::string sInputFile)
{

	size_t index = sInputFile.find_last_of("\\");
	std::string unzipath = sInputFile.substr(0, index+1);
	std::string strOutputString;

	CString csTmp = "Creating UnZip of ";
	csTmp += sInputFile.c_str();
	_pImage->GetField(e_filter_status)->SetValueString(csTmp);
	_pImage->RequestExternalUpdate();


	HZIP hz = OpenZip(sInputFile.c_str(),0);
	ZIPENTRY ze; GetZipItem(hz,-1,&ze); int numitems=ze.index;
	// -1 gives overall information about the zipfile
	for (int zi=0; zi<numitems; zi++)
	{ ZIPENTRY ze; GetZipItem(hz,zi,&ze); // fetch individual details

	strOutputString =  unzipath + ze.name;
	//UnzipItem(hz, zi, ze.name);         // e.g. the item's name.
	UnzipItem(hz, zi, strOutputString.c_str());         // e.g. the item's name.
	}
	CloseZip(hz);

	CString csOutput;
	csOutput += "UnZip created: ";
	csOutput += strOutputString.c_str();
	UpdateOutputField(std::string(csOutput.GetBuffer()));
	

	return strOutputString;
}

void CTNS::UpdateOutputField(std::string &stroutput)
{
	CString csOutput = _pImage->GetField(e_output)->GetValueString();
	csOutput +="\r\n";
	csOutput += CString(currentDateTime().c_str());
	csOutput += "      ";
	csOutput += CString(stroutput.c_str());
	_pImage->GetField(e_output)->SetValueString(csOutput);
	_pImage->RequestExternalUpdate();
}

void CTNS::UploadAllSubscription()
{
	CString __csDestinationPath = _pImage->GetField(e_S3Bucket)->GetDisplayString();
	CString __csAccount = _pImage->GetField(e_DestAccount)->GetDisplayString(); // This is Test Account
	
	std::vector<BBOSubscriptionDtl>::iterator it = ((CME_BBO_TNS_DECODE*)_pImage)->m_clBBOSubV.begin();
	std::map<std::string,BBOSubscriptionDtl> mapUnique;
	for(;it!=((CME_BBO_TNS_DECODE*)_pImage)->m_clBBOSubV.end();it++)
	{
				
		CString csOutputFile = it->stOutputFileName;

		std::map<std::string,BBOSubscriptionDtl>::iterator itUnique = mapUnique.find(std::string(csOutputFile.GetBuffer()));

		if(itUnique!= mapUnique.end())
			continue;

		mapUnique.insert(std::pair<std::string,BBOSubscriptionDtl>(csOutputFile,*it));
		
		std::map<CString,TNSFileDtl>::iterator itFile = _mapFileDtl.find(csOutputFile);

		if(itFile!= _mapFileDtl.end())
		{
			std::vector<CString> vecFiles = itFile->second._vecFileList;

			for(unsigned int i=0;i<vecFiles.size();i++)
			{
				
				CString	csDestinationFolder;
				if( _pImage->GetField(e_DestAccount)->GetValueInt() == 1 )
					csDestinationFolder.Format("%s%s/%s/",__csDestinationPath,  it->stDEST_ACCOUNT.GetBuffer(), _pImage->GetField(e_gen_filetype)->GetDisplayString());
				else
					csDestinationFolder.Format("%s%s/%s/",__csDestinationPath, __csAccount,  _pImage->GetField(e_gen_filetype)->GetDisplayString());

				CString csAWSCommand;
				CString csSourceFile = vecFiles[i];
				csSourceFile.Replace(".csv",".zip");

				csAWSCommand.Format(_T("aws s3 cp %s --profile s3user %s"),csSourceFile.GetBuffer(), csDestinationFolder);					
				int iUpload= system(csAWSCommand);	

				CString stroutput;
				stroutput.Format("cmd %s @@\n", csAWSCommand, iUpload );
				_pImage->PrintRawMessage( stroutput );
				std::string stoutput = "Uploading file ";
				stoutput += csSourceFile.GetBuffer();
				stoutput += " to ";
				stoutput += csDestinationFolder.GetBuffer();
			
				UpdateOutputField(stoutput);
			}	

		}

		
	}
}
void CTNS::UploadSingleSub(BBOSubscriptionDtl &clBlockSub)
{
	CString __csDestinationPath = _pImage->GetField(e_S3Bucket)->GetDisplayString();
	CString __csAccount = _pImage->GetField(e_DestAccount)->GetDisplayString(); // This is Test Account
	std::vector<std::string> vecUploadFileList;

	CString csOutputFile = clBlockSub.stOutputFileName;

	std::map<CString,TNSFileDtl>::iterator itFile = _mapFileDtl.find(csOutputFile);

	if(itFile!= _mapFileDtl.end())
	{
		std::vector<CString> vecFiles = itFile->second._vecFileList;

		for(unsigned int i=0;i<vecFiles.size();i++)
		{

			CString	csDestinationFolder;
			if( _pImage->GetField(e_DestAccount)->GetValueInt() == 1 )
				csDestinationFolder.Format("%s%s/%s/",__csDestinationPath,  clBlockSub.stDEST_ACCOUNT.GetBuffer(), _pImage->GetField(e_gen_filetype)->GetDisplayString());
			else
				csDestinationFolder.Format("%s%s/%s/",__csDestinationPath, __csAccount,  _pImage->GetField(e_gen_filetype)->GetDisplayString());

			CString csAWSCommand;
			CString csSourceFile = vecFiles[i];
			csSourceFile.Replace(".csv",".zip");

			csAWSCommand.Format(_T("aws s3 cp %s --profile s3user %s"),csSourceFile.GetBuffer(), csDestinationFolder);					
			int iUpload= system(csAWSCommand);	

			CString stroutput;
			stroutput.Format("cmd %s @@\n", csAWSCommand, iUpload );
			_pImage->PrintRawMessage( stroutput );
			std::string stoutput = "Uploading file ";
			stoutput += csSourceFile.GetBuffer();
			stoutput += " to ";
			stoutput += csDestinationFolder.GetBuffer();

			UpdateOutputField(stoutput);
		}	

	}
}

std::vector<std::string> CTNS::get_all_files_names_within_folder(std::string & folder, std::string & cstime)
{
    std::vector<std::string> names;
    char search_path[200];
    sprintf(search_path, "%s/*.*", folder.c_str());
    WIN32_FIND_DATA fd; 
    HANDLE hFind = ::FindFirstFile(search_path, &fd); 
    if(hFind != INVALID_HANDLE_VALUE) { 
        do { 
            if(! (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ) 
			{
					CString csFile(fd.cFileName);
					if((csFile.Find(".zip")!=-1) && csFile.Find(cstime.c_str())!=-1)
					{
						std::string strFile = folder + fd.cFileName;
						names.push_back(strFile);
					}
				
            }
        }while(::FindNextFile(hFind, &fd)); 
        ::FindClose(hFind); 
    } 
    return names;
}



