#include "StdAfx.h"
#include "BBO.h"
#include <math.h>
#include <sstream>
#include "zip.h"
#include "unzip.h"
#include <ShlDisp.h>
#include "BBOConstants.h"
#include "CME_BBO_TNS_DECODE.hpp"
#include <windows.h>

// Test comment
BF_NAMESPACE_USE

CBBO::~CBBO(void){}

// Test

bool CBBO::dirExists(const std::string& dirName_in)
{
	DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
	if (ftyp == INVALID_FILE_ATTRIBUTES)
		return false;  //something is wrong with your path!

	if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
		return true;   // this is a directory!

	return false;    // this is not a directory!
}

void CBBO::setProcessParameters()
{
	if(_pImage)
	{	
		CString csFUTOPT,csInputFile,csOutputPath;
		setExchange(std::string(_pImage->GetField(e_filter_exchange)->GetDisplayString()));
		setInerval(_pImage->GetField(e_interval)->GetValueInt());
		csOutputPath = _pImage->GetField(e_gen_outputfile)->GetValueString();
		csOutputPath.Trim();
		SetOutputBBOFilePath(std::string(csOutputPath));
		csInputFile = _pImage->GetField(e_gen_inputfile)->GetValueString();
		csInputFile.Trim();
		OpenInputBBOFile(std::string((LPCTSTR)csInputFile));
		OpenExceptionFile();
	}
	
}
bool CBBO::OpenInputBBOFile(std::string strFileName)
{
	try
	{
		std::size_t found = strFileName.find(".zip");
		if (found!=std::string::npos)
			_strInputFileName =	createUnzip(strFileName);
		else
			_strInputFileName = strFileName;
		int iIndex = _strInputFileName.find_last_of("\\");
		_strInputPath = _strInputFileName.substr(0,iIndex+1);
		_strIniFile = _strInputFileName + ".ini";

		fpInputFile.open(_strInputFileName.c_str());
		return true;
	}
	catch (CMemoryException* e)
	{
		CString csErrorMsg;
		std::cout<<"Exception::OpenBBOFile Error: CMemoryException"<<std::endl;
		e->Delete();
		return false;
	}
	catch (CFileException* e)
	{
		std::cout<<"Exception::OpenBBOFile Error: CFileException"<<std::endl;
		e->Delete();
		return false;
	}
	catch (CException* e)
	{
		std::cout<<"Exception::OpenBBOFile Error:CException"<<std::endl;
		e->Delete();
		return false;
	}
}


bool CBBO::OpenExceptionFile()
{
	try
	{
		_strExceptionFileName = _strInputFileName + ".not";

		fpExceptionFile.open(_strExceptionFileName.c_str(),std::ofstream::out | std::ofstream::app);
		return true;
	}
	catch (CMemoryException* e)
	{
		CString csErrorMsg;
		std::cout<<"Exception::OpenExceptionFile Error: CMemoryException"<<std::endl;
		e->Delete();
		return false;
	}
	catch (CFileException* e)
	{
		std::cout<<"Exception::OpenExceptionFile Error: CFileException"<<std::endl;
		e->Delete();
		return false;
	}
	catch (CException* e)
	{
		std::cout<<"Exception::OpenExceptionFile Error:CException"<<std::endl;
		e->Delete();
		return false;
	}
}


bool CBBO::Process()
{
	long lCnt=0;
	try
	{
				
		_mapFileDtl.clear();
		
		GotoLine(fpInputFile,GetFileProcessCount(_strInputFileName));
		
		std::string strLine;

		while(!fpInputFile.eof())
		{
			getline(fpInputFile,strLine);

			if (strLine.length() == BBO_STANDARD_LENGH || strLine.length() == BBO_OLD_LENGTH) 
			{
				std::string strProd = strLine.substr(23, 3);
				trim2(strProd);
				std::string strFO = strLine.substr(26, 1);
				trim2(strFO);

				std::string strKey = _strExchange + "_" + strProd + "_" + strFO;

				WriteRecord(strKey,_strExchange,strProd,strFO,strLine);

				//if((_strProduct == "*") && (_strFUTOPT == "*"))  // All Records
				//{
				//	WriteRecord(strKey,_strExchange,strProd,strFO,strLine);
				//}
				//else if((_strProduct == "*") || (_strFUTOPT == "*"))
				//{
				//	if(_strProduct == "*")                // All Product
				//	{
				//		if(_stricmp(_strFUTOPT.c_str(),strFO.c_str()) == 0)
				//		{
				//			WriteRecord(strKey,_strExchange,strProd,strFO,strLine);
				//		}
				//	}
				//	else if ((_strFUTOPT == "*"))            // All FUT/OPT
				//	{
				//		if(IsProductFound(strProd))
				//		{
				//			WriteRecord(strKey,_strExchange,strProd,strFO,strLine);
				//		}
				//	}
				//}
				//else
				//{
				//	// Filter product and Fut/Opt
				//	if((IsProductFound(strProd)) && (_stricmp(_strFUTOPT.c_str(),strFO.c_str()) == 0))
				//	{
				//		WriteRecord(strKey,_strExchange,strProd,strFO,strLine);
				//	}
				//}

			}
			else
			{
				std::cout<<"The message " << strLine.c_str() << " will be skipped because it is malformed"<<std::endl;	
				fpExceptionFile << strLine.c_str()<<std::endl;
			}
			

			if((lCnt %_interval == 0))
			{
			
				WriteFileProcessCount(_strInputFileName,lCnt);
				updateImageFields(lCnt);
			}
			lCnt++;
		}

		
		WriteFileProcessCount(_strInputFileName,lCnt);
		
	}
	catch (CMemoryException* e)
	{
		e->Delete();
	}
	catch (CFileException* e)
	{
		e->Delete();
	}
	catch (CException* e)
	{
		e->Delete();
	}

	fpInputFile.close();
	fpExceptionFile.close();
	CloseOpenFiles();
	createZip();
	GenereateMetaData();

	char buff[100];
	sprintf(buff,"DONE  !!  Lines Processed: %ld",lCnt);
	UpdateOutputField(std::string(buff));
	
	return true;
}

void CBBO::GenereateMetaData()
{
	std::map<std::string,stBBOMetaData>::iterator it = _mapBBOMetaData.begin();
	CString csOutput;
	for(;it!=_mapBBOMetaData.end();it++)
	{
		char buff[100];
		sprintf(buff,"\r\n Key: %s  count: %ld  ",it->first.c_str(),it->second._count);
		csOutput +=buff;
		std::cout<<"Key: "<<it->first<<"  value: "<<it->second._count<<std::endl;
	}
	UpdateOutputField(std::string(csOutput.GetBuffer()));
	
}

bool CBBO::WriteRecord(std::string &strKey,std::string &strExch, std::string & strProd, std::string & strFO,std::string & strLine)
{
	std::vector<BBOSubscriptionDtl>::iterator it = ((CME_BBO_TNS_DECODE*)_pImage)->m_clBBOSubV.begin();

	for(;it!=((CME_BBO_TNS_DECODE*)_pImage)->m_clBBOSubV.end();it++)
	{
		if(((strExch.compare(it->stEXCH_CODE) ==0)) && ((it->stALL_PRODUCTS == "1")  || ((strProd.compare(it->stPROD_CODE) == 0))))
		{
				CString csPath;
				csPath.Format("%s%s\\%s.txt", _strOutputFilePath.c_str(), it->stDEST_ACCOUNT.GetBuffer(),GetTime().c_str());

				std::map<CString,BBOFileDtl>::iterator itbbo = _mapFileDtl.find(csPath);

				if(itbbo!=_mapFileDtl.end())
				{
					fputs( strLine.c_str(), itbbo->second.fp);
					fputs("\n",itbbo->second.fp);
					it->iRowCount++;
					itbbo->second._SplitCount++;
					it->stOutputFileName = csPath;
				}
				else
				{
					CString strPath;
					strPath.Format("%s%s\\",_strOutputFilePath.c_str(), it->stDEST_ACCOUNT.GetBuffer());
					std::string strdir = strPath;

					BBOFileDtl objBBOFileDtl;

					objBBOFileDtl.strOutputFileName = csPath;

					if(!dirExists(strdir))
						CreateDirectory(strdir.c_str(), NULL);

					if( fopen_s(&objBBOFileDtl.fp, csPath,"w") )
						return 0;

					it->iRowCount =1;

					fputs(strLine.c_str(),objBBOFileDtl.fp);

					fputs("\n",objBBOFileDtl.fp);

					_mapFileDtl.insert(std::pair<CString,BBOFileDtl>(objBBOFileDtl.strOutputFileName,objBBOFileDtl));
					
					it->stOutputFileName = csPath;
				}

			}
		}
	return 1;
}


void CBBO::CloseOpenFiles()
{
	
	std::map<CString,BBOFileDtl>::iterator itbbo = _mapFileDtl.begin();
	for(;itbbo!=_mapFileDtl.end();itbbo++)
	{
	
		if(itbbo->second.fp)
		{
			fclose(itbbo->second.fp);
			CString csOutputZipFile = itbbo->second.strOutputFileName;

			int iIndex = itbbo->second.strOutputFileName.ReverseFind('\\');

			CString csFileName = itbbo->second.strOutputFileName.Mid(iIndex+1);

			csOutputZipFile.Replace(".txt",".zip");

			HZIP hz = CreateZip(csOutputZipFile.GetBuffer(),0);
			ZRESULT res = 	ZipAdd(hz,csFileName.GetBuffer(),  itbbo->second.strOutputFileName.GetBuffer());
			if(res == 0)
			{
				remove(itbbo->second.strOutputFileName.GetBuffer());
			}
			CloseZip(hz);
		}
		
	}	
}



void CBBO::createZip()
{
	std::map<std::string,stBBOMetaData>::iterator it = _mapBBOMetaData.begin();
	for(;it!=_mapBBOMetaData.end();it++)
	{
		
		std::string strZipOutFileName,strZipFileName,strInputFileName;

		strZipOutFileName = _strOutputFilePath;
		strZipOutFileName += it->first + "_";
		strZipOutFileName += GetTime();
		strZipOutFileName += ".zip";

		strInputFileName = _strOutputFilePath;
		strInputFileName += it->first + "_";
		strInputFileName += GetTime();
		strInputFileName += ".txt";

		//strZipFileName ="zip";
		strZipFileName += it->first + "_";
		strZipFileName += GetTime();
		strZipFileName += ".txt";
		
		CString csTmp = "Creating Zip of ";
		csTmp += strZipOutFileName.c_str();
		_pImage->GetField(e_filter_status)->SetValueString(csTmp);

		HZIP hz = CreateZip(strZipOutFileName.c_str(),0);
		ZRESULT res = 	ZipAdd(hz,strZipFileName.c_str(),  strInputFileName.c_str());
		if(res == 0)
		{
			remove(strInputFileName.c_str());
		}
		CloseZip(hz);
		CString csOutput;
		csOutput += "Zip created: ";
		csOutput += strZipOutFileName.c_str();
		UpdateOutputField(std::string(csOutput.GetBuffer()));
		
	}
}



std::string CBBO::formatTime(std::string  trdtime)
{
	switch (trdtime.length()) {
	case (1):
		return ONEDIGETTIME + trdtime;
	case (2):
		return TWODIGETTIME + trdtime;
	case (3):
		return THREEDIGETTIME + trdtime.substr(0, 1) + COL
			+ trdtime.substr(1);
	case (4):
		return FOURDIGETTIME + trdtime.substr(0, 2) + COL
			+ trdtime.substr(2);
	case (5):
		return FIVEDIGETTIME + trdtime.substr(0, 1) + COL
			+ trdtime.substr(1, 3) + COL + trdtime.substr(3);
	default:
		return trdtime.substr(0, 2) + COL + trdtime.substr(2, 4)
			+ COL + trdtime.substr(4, 6);

	}
}


std::string CBBO::formatEntryDate(std::string line) 
{
	if (line.length() == 70) 
	{
		std::string strtmp = line.substr(64, 6);
		trim2(strtmp);
		return TWENTY + strtmp;
	} else {
		return EMPTY_ENTRY_DATE;
	}
}


void CBBO::trim2(std::string & str)
{
	std::string::size_type pos = str.find_last_not_of(' ');
	if(pos != std::string::npos) {
		str.erase(pos + 1);
		pos = str.find_first_not_of(' ');
		if(pos != std::string::npos) str.erase(0, pos);
	}
	else str.erase(str.begin(), str.end());
}


std::string CBBO::formatPrice(std::string inPrice, std::string decimalLoc) 
{
	if (!_strcmpi(inPrice.c_str(),"")) 
	{
		return PRICEZERO;
	} 
	else
	{
		if (!_strcmpi(decimalLoc.c_str(),""))
		{
			return inPrice;
		} 
		else 
		{
			double idx = atof(decimalLoc.c_str());

			double price = atof(inPrice.c_str());

			std::ostringstream os;
			os << double(price / pow(10, idx));
			std::string tmpPrice = os.str();

			int dicl = tmpPrice.find(DOT);
			switch (tmpPrice.substr(dicl + 1).length()) {
			case (1):
				return tmpPrice + SIXZERO;
			case (2):
				return tmpPrice + FIVEZERO;
			case (3):
				return tmpPrice + FOURZERO;
			case (4):
				return tmpPrice + THREEZERO;
			case (5):
				return tmpPrice + TWOZERO;
			case (6):
				return tmpPrice + ONEZERO;
			default:
				return tmpPrice;
			}

		}
	}
}

void CBBO::updateImageFields(long & lCnt)
{
	char buff[512];
	sprintf(buff,"Lines Processed: %ld",lCnt);
	_pImage->GetField(e_filter_status)->SetValueString(buff);
	_pImage->RequestExternalUpdate();
}


std::string CBBO::createUnzip(std::string sInputFile)
{

	size_t index = sInputFile.find_last_of("\\");
	std::string unzipath = sInputFile.substr(0, index+1);
	std::string strOutputString;


	CString csTmp = "Creating UnZip of ";
	csTmp += sInputFile.c_str();
	_pImage->GetField(e_filter_status)->SetValueString(csTmp);
	_pImage->RequestExternalUpdate();

	HZIP hz = OpenZip(sInputFile.c_str(),0);
	ZIPENTRY ze; GetZipItem(hz,-1,&ze); int numitems=ze.index;
	// -1 gives overall information about the zipfile
	for (int zi=0; zi<numitems; zi++)
	{ ZIPENTRY ze; GetZipItem(hz,zi,&ze); // fetch individual details

	strOutputString =  unzipath + ze.name;
		//UnzipItem(hz, zi, ze.name);         // e.g. the item's name.
	UnzipItem(hz, zi, strOutputString.c_str());         // e.g. the item's name.
	}
	CloseZip(hz);

	CString csOutput;
	csOutput += "UnZip created: ";
	csOutput += strOutputString.c_str();
	UpdateOutputField(std::string(csOutput.GetBuffer()));
	
	return strOutputString;
}

void CBBO::UpdateOutputField(std::string &stroutput)
{
	CString csOutput = _pImage->GetField(e_output)->GetValueString();
	csOutput +="\r\n";
	csOutput += CString(currentDateTime().c_str());
	csOutput += "      ";
	csOutput += CString(stroutput.c_str());
	_pImage->GetField(e_output)->SetValueString(csOutput);
	_pImage->RequestExternalUpdate();
}

void CBBO::UploadAllSubscription()
{
	CString __csDestinationPath = _pImage->GetField(e_S3Bucket)->GetDisplayString();
	CString __csAccount = _pImage->GetField(e_DestAccount)->GetDisplayString(); // This is Test Account

	std::vector<BBOSubscriptionDtl>::iterator it = ((CME_BBO_TNS_DECODE*)_pImage)->m_clBBOSubV.begin();
	std::vector<std::string> vecUploadFileList;

	std::map<std::string,BBOSubscriptionDtl> mapUnique;
	for(;it!=((CME_BBO_TNS_DECODE*)_pImage)->m_clBBOSubV.end();it++)
	{
		vecUploadFileList.clear();

		std::string strUploadFolder = _strOutputFilePath.c_str();
		strUploadFolder+= it->stDEST_ACCOUNT.GetBuffer();
		strUploadFolder+="\\";

		vecUploadFileList = get_all_files_names_within_folder(strUploadFolder,GetTime());

		if(vecUploadFileList.size()<1)
			continue;

		std::map<std::string,BBOSubscriptionDtl>::iterator itUnique = mapUnique.find(strUploadFolder);

		if(itUnique!= mapUnique.end())
			continue;

		mapUnique.insert(std::pair<std::string,BBOSubscriptionDtl>(strUploadFolder,*it));

		for(unsigned int i=0;i<vecUploadFileList.size();i++)
		{
			CString	csDestinationFolder;
			if( _pImage->GetField(e_DestAccount)->GetValueInt() == 1 )
				csDestinationFolder.Format("%s%s/%s/",__csDestinationPath,  it->stDEST_ACCOUNT.GetBuffer(), _pImage->GetField(e_gen_filetype)->GetDisplayString());
			else
				csDestinationFolder.Format("%s%s/%s/",__csDestinationPath, __csAccount,  _pImage->GetField(e_gen_filetype)->GetDisplayString());

			CString csAWSCommand;
			csAWSCommand.Format(_T("aws s3 cp %s %s"),vecUploadFileList[i].c_str(), csDestinationFolder);					
			int iUpload= system(csAWSCommand);	

			CString stroutput;
			stroutput.Format("cmd %s @@\n", csAWSCommand, iUpload );
			_pImage->PrintRawMessage( stroutput );
			std::string stoutput = "Uploading file " + vecUploadFileList[i];
			stoutput += " to ";
			stoutput += csDestinationFolder.GetBuffer();
			
			UpdateOutputField(stoutput);
		}	
	}
}


void CBBO::UploadSingleSub(BBOSubscriptionDtl &clBlockSub)
{
	CString __csDestinationPath = _pImage->GetField(e_S3Bucket)->GetDisplayString();
	CString __csAccount = _pImage->GetField(e_DestAccount)->GetDisplayString(); // This is Test Account
	std::vector<std::string> vecUploadFileList;

	vecUploadFileList.clear();

	std::string strUploadFolder = _strOutputFilePath.c_str();
	strUploadFolder+= clBlockSub.stDEST_ACCOUNT.GetBuffer();
	strUploadFolder+="\\";

	vecUploadFileList = get_all_files_names_within_folder(strUploadFolder,GetTime());

	if(vecUploadFileList.size()<1)
		return;

	for(unsigned int i=0;i<vecUploadFileList.size();i++)
	{
		CString	csDestinationFolder;
		if( _pImage->GetField(e_DestAccount)->GetValueInt() == 1 )
			csDestinationFolder.Format("%s%s/%s/",__csDestinationPath,  clBlockSub.stDEST_ACCOUNT.GetBuffer(), _pImage->GetField(e_gen_filetype)->GetDisplayString());
		else
			csDestinationFolder.Format("%s%s/%s/",__csDestinationPath, __csAccount,  _pImage->GetField(e_gen_filetype)->GetDisplayString());

		CString csAWSCommand;
		csAWSCommand.Format(_T("aws s3 cp %s %s"),vecUploadFileList[i].c_str(), csDestinationFolder);					
		int iUpload= system(csAWSCommand);	

		CString stroutput;
		stroutput.Format("cmd %s @@\n", csAWSCommand, iUpload );
		_pImage->PrintRawMessage( stroutput );
		std::string stoutput = "Uploading file " + vecUploadFileList[i];
		stoutput += " to ";
		stoutput += csDestinationFolder.GetBuffer();
		
		UpdateOutputField(stoutput);
	}
}

std::vector<std::string> CBBO::get_all_files_names_within_folder(std::string & folder, std::string & cstime)
{
   std::vector<std::string> names;
    char search_path[200];
    sprintf(search_path, "%s/*.*", folder.c_str());
    WIN32_FIND_DATA fd; 
    HANDLE hFind = ::FindFirstFile(search_path, &fd); 
    if(hFind != INVALID_HANDLE_VALUE) { 
        do { 
            // read all (real) files in current folder
            // , delete '!' read other 2 default folder . and ..
            if(! (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ) 
			{
					CString csFile(fd.cFileName);
					if((csFile.Find(".zip")!=-1) && csFile.Find(cstime.c_str())!=-1)
					{
						std::string strFile = folder + fd.cFileName;
						names.push_back(strFile);
					}
				
            }
        }while(::FindNextFile(hFind, &fd)); 
        ::FindClose(hFind); 
    } 
    return names;
}