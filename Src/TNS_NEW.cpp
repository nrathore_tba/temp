#include "StdAfx.h"
#include "TNS_NEW.h"
#include "BBOConstants.h"
#include <math.h>
#include <sstream>
#include <algorithm>
#include "zip.h"
#include "unzip.h"

BF_NAMESPACE_USE

// Test

CTNS::~CTNS(void)
{
}

void CTNS::setProcessParameters()
{
	if(_pImage)
	{	
		CString csFUTOPT,csInputFile,csOutputPath;

		setExchange(std::string((LPCTSTR)_pImage->GetField(e_filter_exchange)->GetDisplayString()));
		setProduct(std::string((LPCTSTR)_pImage->GetField(e_filter_product)->GetDisplayString()));
		setInerval(_pImage->GetField(e_interval)->GetValueInt());
		
		csOutputPath= _pImage->GetField(e_gen_outputfile)->GetValueString();
		csOutputPath.Trim();
		SetOutputTSFilePath(std::string(csOutputPath));
		

		setPartition(_pImage->GetField(e_tns_part)->GetValueInt());

		csFUTOPT = _pImage->GetField(e_filter_FO)->GetDisplayString();
		if(csFUTOPT == "Future")	
			SetFUTOPT("F");
		else if(csFUTOPT == "Option")
			SetFUTOPT("O");
		else
			SetFUTOPT(std::string((LPCTSTR)csFUTOPT));


		csInputFile = _pImage->GetField(e_gen_inputfile)->GetValueString();
		csInputFile.Trim();
		OpenBBOFile(std::string((LPCTSTR)csInputFile));

		OpenExceptionFile();
	}
}

bool CTNS::OpenBBOFile(std::string strFileName)
{
	try
	{

		std::size_t found = strFileName.find(".zip");
		if (found!=std::string::npos)
			_strInputFileName =	createUnzip(strFileName);
		else
			_strInputFileName = strFileName;

		int iIndex = _strInputFileName.find_last_of("\\");
		_strInputPath = _strInputFileName.substr(0,iIndex+1);
		_strIniFile = _strInputFileName + ".ini";

		fpInputFile.open(_strInputFileName.c_str());
		return true;
	}
	catch (CMemoryException* e)
	{
		CString csErrorMsg;
		std::cout<<"Exception::OpenBBOFile Error: CMemoryException"<<std::endl;
		e->Delete();
		return false;
	}
	catch (CFileException* e)
	{
		std::cout<<"Exception::OpenBBOFile Error: CFileException"<<std::endl;
		e->Delete();
		return false;
	}
	catch (CException* e)
	{
		std::cout<<"Exception::OpenBBOFile Error:CException"<<std::endl;
		e->Delete();
		return false;
	}
}


bool CTNS::OpenExceptionFile()
{
	try
	{
		_strExceptionFileName = _strInputFileName + ".not";
		fpExceptionFile.open(_strExceptionFileName.c_str(),std::ofstream::out | std::ofstream::app);
		return true;
	}
	catch (CMemoryException* e)
	{
		CString csErrorMsg;
		std::cout<<"Exception::OpenExceptionFile Error: CMemoryException"<<std::endl;
		e->Delete();
		return false;
	}
	catch (CFileException* e)
	{
		std::cout<<"Exception::OpenExceptionFile Error: CFileException"<<std::endl;
		e->Delete();
		return false;
	}
	catch (CException* e)
	{
		std::cout<<"Exception::OpenExceptionFile Error:CException"<<std::endl;
		e->Delete();
		return false;
	}
}



bool CTNS::Process()
{
	long lCnt=0;
	try
	{
		//fpMapOutPutFiles.clear();
		//_stMDTNSDetails.clear();
		_mapTNSMetaData.clear();
		GotoLine(fpInputFile,GetFileProcessCount(_strInputFileName));
		std::string strLine;	
		while(!fpInputFile.eof())
		{
			getline(fpInputFile,strLine);

			if (strLine.length() == BBO_STANDARD_LENGH || strLine.length() == BBO_OLD_LENGTH) 
			{
				/*std::string sessionIndicator = strLine.substr(SESSION_INDICATOR-1, 1);
				std::string askBidType = strLine.substr(ASK_BID_TYPE-1, 1);
				std::string indQuote = strLine.substr(INDICATIVE_QUOTE-1, 1);
				std::string mktQuote = strLine.substr(MARKET_QUOTE-1, 1);
				std::string closeOpenType = strLine.substr(CLOSE_OPEN_TYPE-1, 1);
				
				if (orderVenue.contains(sessionIndicator) 
				&&!(_strcmpi(askBidType.c_str(),"A"))
				&& !(_strcmpi(askBidType.c_str(),"B"))
				&& !(_strcmpi(indQuote.c_str(),"I"))
				&& !(_strcmpi(mktQuote.c_str(),"M"))
				&& !(_strcmpi(closeOpenType.c_str(),"B")))*/

				std::string strProd = strLine.substr(23, 3);
				trim2(strProd);
				std::string strFO = strLine.substr(26, 1);
				trim2(strFO);

				std::string strKey = _strExchange + "_" + strProd + "_" + strFO;
			
				if((_strProduct == "*") && (_strFUTOPT == "*"))  // All Records
				{		
					ConvertRecord(strKey,strLine);
				}

				else if((_strProduct == "*") || (_strFUTOPT == "*"))
				{
					if(_strProduct == "*")                // All Product
					{
						if(_stricmp(_strFUTOPT.c_str(),strFO.c_str()) == 0)
						{
							ConvertRecord(strKey,strLine);
						}
					}
					else if ((_strFUTOPT == "*"))            // All FUT/OPT
					{
						if(IsProductFound(strProd))
						{
							ConvertRecord(strKey,strLine);
						}
					}
				}
				else
				{
					// Filter product and Fut/Opt
					if(IsProductFound(strProd) && (_stricmp(_strFUTOPT.c_str(),strFO.c_str()) == 0))
					{
						ConvertRecord(strKey,strLine);
					}
				}
			}
			else
			{
				std::cout<<"The message " << strLine.c_str() << " will be skipped because it is malformed"<<std::endl;	
				fpExceptionFile << strLine.c_str()<<std::endl;
			}	
			
			if((lCnt %_interval == 0))
			{
				FlushOpenFiles();
				WriteFileProcessCount(_strInputFileName,lCnt);
				updateImageFields(lCnt);
			}
			lCnt++;
		}
				
		WriteFileProcessCount(_strInputFileName,lCnt);
	}
	catch (CMemoryException* e)
	{
		e->Delete();
	}
	catch (CFileException* e)
	{
		e->Delete();
	}
	catch (CException* e)
	{
		e->Delete();
	}

	fpInputFile.close();
	fpExceptionFile.close();
	CloseOpenFiles();
	createZip();
	GenereateMetaData();



	char buff[100];
	sprintf(buff,"\r\n DONE  !!  Lines Processed: %ld",lCnt);
	_pImage->GetField(e_filter_status)->SetValueString(buff);
	CString csOutput = _pImage->GetField(e_output)->GetValueString();
	csOutput +=" ";
	csOutput +=buff;
	_pImage->GetField(e_output)->SetValueString(csOutput);
	_pImage->RequestExternalUpdate();
		return true;
}

void CTNS::GenereateMetaData()
{
	std::map<std::string,stTNSMetaData>::iterator it = _mapTNSMetaData.begin();
	CString csOutput = _pImage->GetField(e_output)->GetValueString();
	for(;it!=_mapTNSMetaData.end();it++)
	{
		char buff[100];
		sprintf(buff,"\r\n Key: %s  count: %ld  ",it->first.c_str(),it->second._count);
		csOutput +=buff;
		std::cout<<"Key: "<<it->first<<"  value: "<<it->second._count<<std::endl;
	}

	_pImage->GetField(e_output)->SetValueString(csOutput);
	_pImage->RequestExternalUpdate();
}
bool CTNS::ConvertRecord(std::string & strKey,std::string & strLine)
{
	std::string strout="";
	strout.append(strLine.substr(0, 8));
	strout.append(DELIM);

	strout.append(formatTime(strLine.substr(8, 6)));
	strout.append(DELIM);

	std::string strNum = strLine.substr(14, 8);
	trim2(strNum);

	strout.append(strNum);
	strout.append(DELIM);

	strout.append(strLine.substr(22, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(23, 3));
	strout.append(DELIM);

	strout.append(strLine.substr(26, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(27, 4));
	strout.append(DELIM);

	std::string strNum1 = strLine.substr(31, 5);
	trim2(strNum1);

	strout.append(strNum1);
	strout.append(DELIM);

	std::string strPrice1 = strLine.substr(36, 7);
	trim2(strPrice1);

	std::string strPrice2 = strLine.substr(43, 1);
	trim2(strPrice2);

	strout.append(formatPrice(strPrice1, strPrice2));
	strout.append(DELIM);

	strPrice1 = strLine.substr(44, 7);
	trim2(strPrice1);

	strPrice2 = strLine.substr(51, 1);
	trim2(strPrice2);

	strout.append(formatPrice(strPrice1, strPrice2));
	strout.append(DELIM);

	strout.append(strLine.substr(52, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(53, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(54, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(55, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(56, 2));
	strout.append(DELIM);

	strout.append(strLine.substr(58, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(59, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(60, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(61, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(62, 1));
	strout.append(DELIM);

	strout.append(strLine.substr(63, 1));
	strout.append(DELIM);

	strout.append(formatEntryDate(strLine));
	strout.append(DELIM);

	strout.append(_strExchange);
	strout.append(DELIM);

	std::map<std::string,stTNSMetaData>::iterator it = _mapTNSMetaData.find(strKey);
	if(it!=_mapTNSMetaData.end())
	{
		// File Already Exists found
		int rem = 0;
		int threasMod = GetOutputFileThreshHold(strKey,rem);
		if(threasMod == 0)
		{
			long lastCount = it->second._count;
			//std::string strText = it->second._strText;
			*it->second._fpOutPutFile<<it->second._strText.c_str();
			it->second._fpOutPutFile->close();
			delete it->second._fpOutPutFile;
			creatSingleZip(it->second);
			_mapTNSMetaData.erase(it);
			

			std::string strFileName;

			char buff[100];
			_itoa(rem,buff,10);

			strFileName = _strOutputFilePath;
			strFileName += strKey + "_";
			strFileName += GetTime();
			strFileName += "_";
			strFileName += buff;
					
			strFileName += ".csv";

			stTNSMetaData _objstTNSMetaData;
			_objstTNSMetaData._strFileName= strFileName;
			_objstTNSMetaData._count=lastCount;
			_objstTNSMetaData._strText = "T.Date,T.Time,Sequence,Session Ind,Symbol,C/P/F,Contract Delivery,Volume,Strike Price,T.Price,A/B,IND,MKQ,C/O,VOE,PC,CAN,INS,F/L,CAB,BKI,Entry Date,exch_code\n";
			_objstTNSMetaData._strText += strout;
			_objstTNSMetaData._strText += "\n";
			_objstTNSMetaData._count++;
			_objstTNSMetaData._fpOutPutFile = new std::ofstream(strFileName.c_str(),std::ofstream::out | std::ofstream::app) ;
			_mapTNSMetaData.insert(std::pair<std::string,stTNSMetaData>(strKey,_objstTNSMetaData));		

			//*_objstTNSMetaData._fpOutPutFile<<"T.Date,T.Time,Sequence,Session Ind,Symbol,C/P/F,Contract Delivery,Volume,Strike Price,T.Price,A/B,IND,MKQ,C/O,VOE,PC,CAN,INS,F/L,CAB,BKI,Entry Date,exch_code"<<std::endl;
			//*_objstTNSMetaData._fpOutPutFile<<strout.c_str()<<std::endl;
			
		}	
		else
		{
			//*it->second._fpOutPutFile<<strout.c_str()<<std::endl;
			it->second._strText += strout;
			it->second._strText += "\n";
			it->second._count++;
		}
	}
	else
	{
		//Not Found
		std::string strFileName;
		strFileName = _strOutputFilePath;
		strFileName += strKey + "_";
		strFileName += GetTime();
		strFileName += ".csv";

		stTNSMetaData _objstTNSMetaData;
		_objstTNSMetaData._count=1;
		_objstTNSMetaData._strFileName= strFileName;

		_objstTNSMetaData._strText = "T.Date,T.Time,Sequence,Session Ind,Symbol,C/P/F,Contract Delivery,Volume,Strike Price,T.Price,A/B,IND,MKQ,C/O,VOE,PC,CAN,INS,F/L,CAB,BKI,Entry Date,exch_code\n";
		_objstTNSMetaData._strText += strout;
		_objstTNSMetaData._strText += "\n";

		_objstTNSMetaData._fpOutPutFile = new std::ofstream(strFileName.c_str(),std::ofstream::out | std::ofstream::app) ;
		_mapTNSMetaData.insert(std::pair<std::string,stTNSMetaData>(strKey,_objstTNSMetaData));		
		
		//*_objstTNSMetaData._fpOutPutFile <<"T.Date,T.Time,Sequence,Session Ind,Symbol,C/P/F,Contract Delivery,Volume,Strike Price,T.Price,A/B,IND,MKQ,C/O,VOE,PC,CAN,INS,F/L,CAB,BKI,Entry Date,exch_code"<<std::endl;
		//*_objstTNSMetaData._fpOutPutFile<<strout.c_str()<<std::endl;
	}

	//UpdateRecord(strKey);
	return true;
}

void CTNS::CloseOpenFiles()
{
	std::map<std::string,stTNSMetaData>::iterator it = _mapTNSMetaData.begin();
	for(;it!=_mapTNSMetaData.end();it++)
	{
		it->second._fpOutPutFile->close();
		delete it->second._fpOutPutFile;
	}
}

std::string CTNS::formatTime(std::string  trdtime)
{
	switch (trdtime.length()) {
	case (1):
		return ONEDIGETTIME + trdtime;
	case (2):
		return TWODIGETTIME + trdtime;
	case (3):
		return THREEDIGETTIME + trdtime.substr(0, 1) + COL
			+ trdtime.substr(1);
	case (4):
		return FOURDIGETTIME + trdtime.substr(0, 2) + COL
			+ trdtime.substr(2);
	case (5):
		return FIVEDIGETTIME + trdtime.substr(0, 1) + COL
			+ trdtime.substr(1, 3) + COL + trdtime.substr(3);
	default:
		return trdtime.substr(0, 2) + COL + trdtime.substr(2, 4)
			+ COL + trdtime.substr(4, 6);

	}
}


std::string CTNS::formatEntryDate(std::string line) 
{
	if (line.length() == 70) 
	{
		std::string strtmp = line.substr(64, 6);
		trim2(strtmp);
		return TWENTY + strtmp;
	} 
	else 
	{
		return EMPTY_ENTRY_DATE;
	}
}


void CTNS::trim2(std::string & str)
{
	std::string::size_type pos = str.find_last_not_of(' ');
	if(pos != std::string::npos) {
		str.erase(pos + 1);
		pos = str.find_first_not_of(' ');
		if(pos != std::string::npos) str.erase(0, pos);
	}
	else str.erase(str.begin(), str.end());
}


std::string CTNS::formatPrice(std::string inPrice, std::string decimalLoc) 
{
	if (!_strcmpi(inPrice.c_str(),"")) 
	{
		return PRICEZERO;
	} 
	else
	{
		if (!_strcmpi(decimalLoc.c_str(),""))
		{
			return inPrice;
		} 
		else 
		{
			double idx = atof(decimalLoc.c_str());
			double price = atof(inPrice.c_str());

			std::ostringstream os;
			os << double(price / pow(10, idx));
			std::string tmpPrice = os.str();

			int dicl = tmpPrice.find(DOT);
			switch (tmpPrice.substr(dicl + 1).length()) {
			case (1):
				return tmpPrice + SIXZERO;
			case (2):
				return tmpPrice + FIVEZERO;
			case (3):
				return tmpPrice + FOURZERO;
			case (4):
				return tmpPrice + THREEZERO;
			case (5):
				return tmpPrice + TWOZERO;
			case (6):
				return tmpPrice + ONEZERO;
			default:
				return tmpPrice;
			}

		}
	}
}

void CTNS::updateImageFields(long & lCnt)
{
	char buff[512];
	sprintf(buff,"Lines Processed: %ld",lCnt);
	_pImage->GetField(e_filter_status)->SetValueString(buff);
	_pImage->RequestExternalUpdate();
}

void CTNS::creatSingleZip(stTNSMetaData & objstTNSMetaData)
{
	std::string strZipOutFileName,strZipFileName,strInputFileName,strFile;
	int iIndex = objstTNSMetaData._strFileName.find_last_of("\\");
	int LastIndex = objstTNSMetaData._strFileName.find_last_of(".csv");
	iIndex++;
	LastIndex-=3;
	strFile = objstTNSMetaData._strFileName.substr(iIndex,LastIndex - iIndex);

	strZipOutFileName = _strOutputFilePath;
	strZipOutFileName += strFile; 
	strZipOutFileName += ".zip";

	strInputFileName = _strOutputFilePath;
	strInputFileName += strFile;
	strInputFileName += ".csv";

	//strZipFileName ="zip";
	strZipFileName += strFile;
	strZipFileName += ".csv";

	CString csTmp = "Creating Zip of ";
	csTmp +=strZipOutFileName.c_str();
	_pImage->GetField(e_filter_status)->SetValueString(csTmp);

	HZIP hz = CreateZip(strZipOutFileName.c_str(),0);
	ZRESULT res = 	ZipAdd(hz,strZipFileName.c_str(),  strInputFileName.c_str());
	if(res == 0)
	{
		remove(strInputFileName.c_str());
	}
	CloseZip(hz);

	CString csOutput = _pImage->GetField(e_output)->GetValueString();
	csOutput += "\r\n Zip created: ";
	csOutput += strZipOutFileName.c_str();
	_pImage->GetField(e_output)->SetValueString(csOutput);
	_pImage->RequestExternalUpdate();
}
void CTNS::createZip()
{
	std::map<std::string,stTNSMetaData>::iterator it = _mapTNSMetaData.begin();
	for(;it!=_mapTNSMetaData.end();it++)
	{

		std::string strZipOutFileName,strZipFileName,strInputFileName,strFile;
		int iIndex = it->second._strFileName.find_last_of("\\");
		int LastIndex = it->second._strFileName.find_last_of(".csv");
		iIndex++;
		LastIndex-=3;
		strFile = it->second._strFileName.substr(iIndex,LastIndex - iIndex);


		strZipOutFileName = _strOutputFilePath;
		strZipOutFileName += strFile;
		strZipOutFileName += ".zip";

		strInputFileName = _strOutputFilePath;
		strInputFileName += strFile ;
		strInputFileName += ".csv";

		//strZipFileName ="zip";
		strZipFileName += strFile ;
		strZipFileName += ".csv";

		CString csTmp = "Creating Zip of ";
		csTmp += strZipOutFileName.c_str();
		_pImage->GetField(e_filter_status)->SetValueString(csTmp);

		HZIP hz = CreateZip(strZipOutFileName.c_str(),0);
		ZRESULT res = 	ZipAdd(hz,strZipFileName.c_str(),  strInputFileName.c_str());
		if(res == 0)
		{
			remove(strInputFileName.c_str());
		}
		CloseZip(hz);
		CString csOutput = _pImage->GetField(e_output)->GetValueString();
		csOutput += "\r\n Zip created: ";
		csOutput += strZipOutFileName.c_str();
		_pImage->GetField(e_output)->SetValueString(csOutput);
		_pImage->RequestExternalUpdate();

	}
}

std::string CTNS::createUnzip(std::string sInputFile)
{

	size_t index = sInputFile.find_last_of("\\");
	std::string unzipath = sInputFile.substr(0, index+1);
	std::string strOutputString;

	CString csTmp = "Creating UnZip of ";
	csTmp += sInputFile.c_str();
	_pImage->GetField(e_filter_status)->SetValueString(csTmp);
	_pImage->RequestExternalUpdate();


	HZIP hz = OpenZip(sInputFile.c_str(),0);
	ZIPENTRY ze; GetZipItem(hz,-1,&ze); int numitems=ze.index;
	// -1 gives overall information about the zipfile
	for (int zi=0; zi<numitems; zi++)
	{ ZIPENTRY ze; GetZipItem(hz,zi,&ze); // fetch individual details

	strOutputString =  unzipath + ze.name;
	//UnzipItem(hz, zi, ze.name);         // e.g. the item's name.
	UnzipItem(hz, zi, strOutputString.c_str());         // e.g. the item's name.
	}
	CloseZip(hz);

	CString csOutput = _pImage->GetField(e_output)->GetValueString();
	csOutput += "\r\n UnZip created: ";
	csOutput += strOutputString.c_str();
	_pImage->GetField(e_output)->SetValueString(csOutput);
	_pImage->RequestExternalUpdate();

	return strOutputString;
}
