#include <stdafx.h>
#include <windows.h>
#include "MessageParser.h"
#include <iomanip>
#include <boost/filesystem.hpp>
#include "LogManager.h"

//#pragma warning( disable: ThatWarning )

double Enrichable::GetValue(PRICENULL prc)
{
	if( prc.mantissa() != prc.mantissaNullValue() )
		return (double)prc.mantissa() / 10000000;
	else
		return 0.0;
}

double Enrichable::GetValue(PRICE prc)
{
	if( prc.mantissa() != prc.mantissaNullValue() )
		return (double)prc.mantissa() / 10000000;
	else
		return 0.0;
}

double Enrichable::GetValue(mktdata::FLOAT prc)
{
	if( prc.mantissa() != prc.mantissaNullValue() )
		return (double)prc.mantissa() / 10000000;
	else
		return 0.0;
}

double Enrichable::GetValue(DecimalQty qty)
{
	if( qty.mantissa() != qty.mantissaNullValue() )
		return (double)qty.mantissa() / 10000;
	else
		return 0.0;
}

string Enrichable::getTradeDate(__int64 time)
{
	time_t seconds = time / 1000000000;
	const time_t dt(seconds);
	tm *tpm = gmtime (&dt);
	stringstream strstream;
	strstream << (tpm->tm_year + 1900) << std::setfill('0') << std::setw(2) << (tpm->tm_mon+1) << std::setfill('0') << std::setw(2) <<tpm->tm_mday;
	return strstream.str();
}

string Enrichable::getTransactionTime(__int64 time)
{
	time_t seconds = time / 1000000000;
	long nanoFraction = time % 1000000000;
	const time_t dt(seconds);
	tm *tpm = gmtime (&dt);
	stringstream strstream;
	strstream << (tpm->tm_year + 1900) 
		<< std::setfill('0') << std::setw(2) << (tpm->tm_mon+1) 
		<< std::setfill('0') << std::setw(2) << tpm->tm_mday
		<< std::setfill('0') << std::setw(2) << (tpm->tm_hour)
		<< std::setfill('0') << std::setw(2) << (tpm->tm_min)
		<< std::setfill('0') << std::setw(2) << (tpm->tm_sec)
		<< std::setfill('0') << std::setw(9) << nanoFraction;
	
	return strstream.str();
}

void Enrichable::setMatchEventIndicator(string &strIndicator, MatchEventIndicator indicator) 
{
	stringstream stream;
	stream << (int)indicator.EndOfEvent() << "0" << (int)indicator.RecoveryMsg() << (int)indicator.LastImpliedMsg()
		<< (int)indicator.LastStatsMsg() << (int)indicator.LastQuoteMsg() << (int)indicator.LastVolumeMsg() << (int)indicator.LastTradeMsg();
	strIndicator = stream.str();
}

void Enrichable::setInstAttribValue(string &strInstAttribValue, InstAttribValue instValue) 
{
	stringstream stream;
	stream << "000000000000"
		<< (int)instValue.ImpliedMatchingEligibility() << (int)instValue.GTOrdersEligibility() 
		<< (int)instValue.DailyProductEligibility()	<< (int)instValue.VariableProductEligibility() 
		<< (int)instValue.DecayingProductEligibility() 	<< (int)instValue.ZeroPriceOutrightEligible() 
		<< (int)instValue.RFQCrossEligible() << "0"
		<< (int)instValue.IsFractional() << (int)instValue.NegativePriceOutrightEligible() 
		<< (int)instValue.NegativeStrikeEligible() << (int)instValue.iLinkIndicativeMassQuotingEligible() 
		<< (int)instValue.OTCEligible() << (int)instValue.EFREligible() 
		<< (int)instValue.EFSEligible() << (int)instValue.EBFEligible() 
		<< (int)instValue.EFPEligible() << (int)instValue.BlockTradeEligible()
		<< (int)instValue.OrderCrossEligible() << (int)instValue.ElectronicMatchEligible();

	strInstAttribValue = stream.str();
}

void Enrichable::setSettlPriceType(string &strSettlPriceType, SettlPriceType settlPriceType)
{
	stringstream strstream;
	strstream << string("000") << (int)settlPriceType.Intraday() << (int)settlPriceType.Rounded() << (int)settlPriceType.Actual() << (int)settlPriceType.Final();
	strSettlPriceType = strstream.str();

	strstream.str("");

	if( strSettlPriceType == "0000000" )
	{
		strSettlPriceType = "10000000";
	}
	else
	{
		strstream << "0" << strSettlPriceType;
		strSettlPriceType = strstream.str();
	}
}

void Enrichable::enrichIncrementalRefresh(Contract &contract)
{
	ContractMapItr itrContract = MessageUtil::getInstance()->contracts.find(contract.SecurityID);

	if(itrContract != MessageUtil::getInstance()->contracts.end())
		contract = itrContract->second;
}

ChannelReset4MsgDecoder::ChannelReset4MsgDecoder()
{
	
}

ChannelReset4MsgDecoder::~ChannelReset4MsgDecoder()
{
}

void ChannelReset4MsgDecoder::decode(Message &message, ChannelResetList &listChannelReset, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr)
{
	ChannelReset4 mdIncrBook32;
	mdIncrBook32.wrapForDecode(const_cast<char*>(message.buffer), nOffset, hdr.blockLength(), hdr.version(), message.length);
	ChannelReset4::NoMDEntries clobj = mdIncrBook32.noMDEntries();
	
	ChannelReset mktData;
	mktData.nOffset = nOffset;
	mktData.MsgTyp = "X";
	mktData.MsgSeqNum = sequence;
	mktData.SendingTime = getTransactionTime(sentTime);
	mktData.TransactTime = getTransactionTime(mdIncrBook32.transactTime());
	mktData.TradeDate = getTradeDate(mdIncrBook32.transactTime());
	setMatchEventIndicator(mktData.MatchEventIndicator, mdIncrBook32.matchEventIndicator());

	for(int iCount(0); iCount < clobj.count(); ++iCount )
	{
		MDEntry objMDEntry;
		ChannelReset4::NoMDEntries clobjTemp  = clobj.next();

		objMDEntry.MDEntryType = *clobjTemp.mDEntryType();
		objMDEntry.MDUpdateAction = clobjTemp.mDUpdateAction();
		mktData.objContract.ApplID = clobjTemp.applID();
		objMDEntry.ApplID = clobjTemp.applID();

		ChannelMap::iterator itrChannel = MessageUtil::getInstance()->channelMap.find(mktData.objContract.ApplID);

		if(itrChannel != MessageUtil::getInstance()->channelMap.end())
			mktData.objContract.SecurityExchange = itrChannel->second;

		mktData.vMDEntry.push_back(objMDEntry);
		listChannelReset.push_back(mktData);
	}
}

void FutureContractTemplate27Decoder::decode(Message &message, SecurityDefList &listContract, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr)
{
	SecurityDefinition objSecDef;
	MDInstrumentDefinitionFuture27 objBook;
	objBook.wrapForDecode(const_cast<char*>(message.buffer), nOffset, hdr.blockLength(), hdr.version(), message.length);

	objSecDef.MsgSeqNum = sequence;
	objSecDef.SendingTime = getTransactionTime(sentTime);

	setMatchEventIndicator(objSecDef.MatchEventIndicator, objBook.matchEventIndicator());

	objSecDef.objContract.SecurityID = objBook.securityID();
	objSecDef.SecurityIDSource = objBook.securityIDSource();
	objSecDef.objContract.ApplID = objBook.applID();
	objSecDef.MarketSegmentID = objBook.marketSegmentID();
	objSecDef.objContract.SecurityExchange = objBook.securityExchange();
	objSecDef.objContract.CFICode = objBook.cFICode();
	objSecDef.objContract.SecurityGroup = objBook.securityGroup();
	objSecDef.objContract.Symbol = objBook.symbol();

	if( objSecDef.SecurityIDSource.length() > 0 )
		objSecDef.SecurityIDSource = objSecDef.SecurityIDSource.substr(0, objBook.securityIDSourceLength());

	if( objSecDef.objContract.SecurityExchange.length() > 0 )
		objSecDef.objContract.SecurityExchange = objSecDef.objContract.SecurityExchange.substr(0, objBook.securityExchangeLength());

	if( objSecDef.objContract.Symbol.length() > 0 )
		objSecDef.objContract.Symbol = objSecDef.objContract.Symbol.substr(0, objBook.symbolLength());

	if( objSecDef.objContract.CFICode.length() > 0 )
		objSecDef.objContract.CFICode = objSecDef.objContract.CFICode.substr(0, objBook.cFICodeLength());

	objSecDef.UnderlyingProduct = objBook.underlyingProduct();
	objSecDef.objContract.Asset = objBook.asset();
	objSecDef.SecurityType = objBook.securityType();

	if( objSecDef.UnderlyingProduct == objBook.underlyingProductNullValue() )
		objSecDef.UnderlyingProduct = 0;

	int year = objBook.maturityMonthYear().year();
	int month = objBook.maturityMonthYear().month();

	stringstream strstream;
	strstream << std::setfill('0') << std::setw(4) << year
		      << std::setfill('0') << std::setw(2) << month;
	
	objSecDef.MaturityMonthYear = strstream.str();

	if( objBook.totNumReports() != objBook.totNumReportsNullValue() )
		objSecDef.TotNumReports = objBook.totNumReports();

	objSecDef.SecurityUpdateAction = objBook.securityUpdateAction();
	if( objBook.lastUpdateTime() != objBook.lastUpdateTimeNullValue() )
		objSecDef.LastUpdateTime = getTransactionTime(objBook.lastUpdateTime());
	objSecDef.UserDefinedInstrument = objBook.userDefinedInstrument();
	objSecDef.MDSecurityTradingStatus = objBook.mDSecurityTradingStatus();

	if( objSecDef.MDSecurityTradingStatus == SecurityTradingStatus::NULL_VALUE )
		objSecDef.MDSecurityTradingStatus = 0;

	objSecDef.Currency = objBook.currency();
	objSecDef.SettlCurrency = objBook.settlCurrency();

	if( objSecDef.Currency.length() > 0 )
		objSecDef.Currency = objSecDef.Currency.substr(0, objBook.currencyLength());

	if( objSecDef.SettlCurrency.length() > 0 )
		objSecDef.SettlCurrency = objSecDef.SettlCurrency.substr(0, objBook.settlCurrencyLength());

	objSecDef.MatchAlgorithm = objBook.matchAlgorithm();
	
	if( objBook.minTradeVol() != objBook.minTradeVolNullValue() )
		objSecDef.MinTradeVol = objBook.minTradeVol();

	if( objBook.maxTradeVol() != objBook.maxTradeVolNullValue() )
		objSecDef.MaxTradeVol = objBook.maxTradeVol();

	objSecDef.MinPriceIncrement = GetValue(objBook.minPriceIncrement());
	objSecDef.MinPriceIncrementAmount = GetValue(objBook.minPriceIncrementAmount());
	objSecDef.DisplayFactor = GetValue(objBook.displayFactor());

	if( objBook.mainFraction() != objBook.mainFractionNullValue() )
		objSecDef.MainFraction = objBook.mainFraction();

	if( objBook.subFraction() != objBook.subFractionNullValue() )
		objSecDef.SubFraction = objBook.subFraction();

	if( objBook.priceDisplayFormat() != objBook.priceDisplayFormatNullValue() )
		objSecDef.PriceDisplayFormat = objBook.priceDisplayFormat();

	if( objBook.contractMultiplierUnit() != objBook.contractMultiplierUnitNullValue() )
		objSecDef.ContractMultiplierUnit = objBook.contractMultiplierUnit();

	if( objBook.flowScheduleType() != objBook.flowScheduleTypeNullValue() )
		objSecDef.FlowScheduleType = objBook.flowScheduleType();

	if( objBook.contractMultiplier() != objBook.contractMultiplierNullValue() )
		objSecDef.ContractMultiplier = objBook.contractMultiplier();

	objSecDef.UnitOfMeasure = objBook.unitOfMeasure();
	objSecDef.UnitOfMeasureQty = GetValue(objBook.unitOfMeasureQty());

	if( objBook.decayQuantity() != objBook.decayQuantityNullValue() )
		objSecDef.DecayQty = objBook.decayQuantity();

	if( objBook.decayStartDate() != objBook.decayStartDateNullValue() )
		objSecDef.DecayStartDate = getTradeDate(objBook.decayStartDate());

	if( objBook.originalContractSize() != objBook.originalContractSizeNullValue() )
		objSecDef.OriginalContractSize = objBook.originalContractSize();

	objSecDef.HighLimitPrice = GetValue(objBook.highLimitPrice());
	objSecDef.LowLimitPrice = GetValue(objBook.lowLimitPrice());
	objSecDef.MaxPriceVariation = GetValue(objBook.maxPriceVariation());
	objSecDef.TradingReferencePrice = GetValue(objBook.tradingReferencePrice());

	setSettlPriceType(objSecDef.SettlPriceType, objBook.settlPriceType());

	if( objBook.openInterestQty() != objBook.openInterestQtyNullValue() )
		objSecDef.OpenInterestQty = objBook.openInterestQty();

	if( objBook.clearedVolume() != objBook.clearedVolumeNullValue() )
		objSecDef.ClearedVolume = objBook.clearedVolume();

	try
	{
		MDInstrumentDefinitionFuture27::NoEvents clobj2 = objBook.noEvents();
		for(int iCount(0); iCount < clobj2.count(); ++iCount )
		{
			MDInstrumentDefinitionFuture27::NoEvents clobjTemp  = clobj2.next();
		
			NoEvents objNoEvents;
			objNoEvents.EventType = clobjTemp.eventType();
			objNoEvents.EventTime = getTransactionTime(clobjTemp.eventTime());
			objSecDef.vNoEvents.push_back(objNoEvents);
		}

		MDInstrumentDefinitionFuture27::NoMDFeedTypes clobj1 = objBook.noMDFeedTypes();
		for(int iCount(0); iCount < clobj1.count(); ++iCount )
		{
			MDInstrumentDefinitionFuture27::NoMDFeedTypes clobjTemp  = clobj1.next();
		
			NoMdFeedTypes objNoMdFeedTypes;
			objNoMdFeedTypes.MDFeedType = clobjTemp.mDFeedType();
			if( objNoMdFeedTypes.MDFeedType.length() > 0 )
				objNoMdFeedTypes.MDFeedType = objNoMdFeedTypes.MDFeedType.substr(0, clobjTemp.mDFeedTypeLength());
			objNoMdFeedTypes.MarketDepth = clobjTemp.marketDepth();
			objSecDef.vNoMdFeedTypes.push_back(objNoMdFeedTypes);
		}

		MDInstrumentDefinitionFuture27::NoInstAttrib clobj3 = objBook.noInstAttrib();
		for(int iCount(0); iCount < clobj3.count(); ++iCount )
		{
			MDInstrumentDefinitionFuture27::NoInstAttrib clobjTemp  = clobj3.next();
		
			NoInstAttrib objNoInstAttrib;
			objNoInstAttrib.InstAttribType = clobjTemp.instAttribType();
			setInstAttribValue(objNoInstAttrib.InstAttribValue, clobjTemp.instAttribValue());
			objSecDef.vNoInstAttrib.push_back(objNoInstAttrib);
		}

		MDInstrumentDefinitionFuture27::NoLotTypeRules clobj4 = objBook.noLotTypeRules();
		for(int iCount(0); iCount < clobj4.count(); ++iCount )
		{
			MDInstrumentDefinitionFuture27::NoLotTypeRules clobjTemp  = clobj4.next();
		
			NoLotTypeRules objNoLotTypeRules;
			objNoLotTypeRules.LotType = clobjTemp.lotType();
			objNoLotTypeRules.MinLotSize = GetValue(clobjTemp.minLotSize());
			objSecDef.vNoLotTypeRules.push_back(objNoLotTypeRules);
		}
	}
	catch(...)
	{
	}

	listContract.push_back(objSecDef);
}

void SpreadContractTemplate29Decoder::decode(Message &message, SecurityDefList &listContract, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr)
{
	SecurityDefinition objSecDef;
	MDInstrumentDefinitionSpread29 objBook;
	objBook.wrapForDecode(const_cast<char*>(message.buffer), nOffset, hdr.blockLength(), hdr.version(), message.length);

	objSecDef.MsgSeqNum = sequence;
	objSecDef.SendingTime = getTransactionTime(sentTime);

	setMatchEventIndicator(objSecDef.MatchEventIndicator, objBook.matchEventIndicator());

	objSecDef.objContract.SecurityID = objBook.securityID();
	objSecDef.SecurityIDSource = objBook.securityIDSource();
	objSecDef.objContract.ApplID = objBook.applID();
	objSecDef.MarketSegmentID = objBook.marketSegmentID();
	objSecDef.objContract.SecurityExchange = objBook.securityExchange();
	objSecDef.objContract.CFICode = objBook.cFICode();

	if( objSecDef.SecurityIDSource.length() > 0 )
		objSecDef.SecurityIDSource = objSecDef.SecurityIDSource.substr(0, objBook.securityIDSourceLength());

	if( objSecDef.objContract.SecurityExchange.length() > 0 )
		objSecDef.objContract.SecurityExchange = objSecDef.objContract.SecurityExchange.substr(0, objBook.securityExchangeLength());

	objSecDef.objContract.SecurityGroup = objBook.securityGroup();
	objSecDef.objContract.Symbol = objBook.symbol();

	if( objSecDef.objContract.Symbol.length() > 0 )
		objSecDef.objContract.Symbol = objSecDef.objContract.Symbol.substr(0, objBook.symbolLength());

	if( objSecDef.objContract.CFICode.length() > 0 )
		objSecDef.objContract.CFICode = objSecDef.objContract.CFICode.substr(0, objBook.cFICodeLength());

	objSecDef.UnderlyingProduct = objBook.underlyingProduct();
	objSecDef.objContract.Asset = objBook.asset();
	objSecDef.SecurityType = objBook.securityType();
	objSecDef.SecuritySubType = objBook.securitySubType();

	if( objSecDef.UnderlyingProduct == objBook.underlyingProductNullValue() )
		objSecDef.UnderlyingProduct = 0;

	int year = objBook.maturityMonthYear().year();
	int month = objBook.maturityMonthYear().month();

	stringstream strstream;
	strstream << std::setfill('0') << std::setw(4) << year
		      << std::setfill('0') << std::setw(2) << month;
	
	objSecDef.MaturityMonthYear = strstream.str();

	if( objBook.totNumReports() != objBook.totNumReportsNullValue() )
		objSecDef.TotNumReports = objBook.totNumReports();

	objSecDef.SecurityUpdateAction = objBook.securityUpdateAction();
	if( objBook.lastUpdateTime() != objBook.lastUpdateTimeNullValue() )
		objSecDef.LastUpdateTime = getTransactionTime(objBook.lastUpdateTime());
	objSecDef.UserDefinedInstrument = objBook.userDefinedInstrument();
	objSecDef.MDSecurityTradingStatus = objBook.mDSecurityTradingStatus();

	if( objSecDef.MDSecurityTradingStatus == SecurityTradingStatus::NULL_VALUE )
		objSecDef.MDSecurityTradingStatus = 0;

	objSecDef.Currency = objBook.currency();

	if( objSecDef.Currency.length() > 0 )
		objSecDef.Currency = objSecDef.Currency.substr(0, objBook.currencyLength());

	objSecDef.PriceRatio = GetValue(objBook.priceRatio());
	objSecDef.MatchAlgorithm = objBook.matchAlgorithm();

	if( objBook.minTradeVol() != objBook.minTradeVolNullValue() )
		objSecDef.MinTradeVol = objBook.minTradeVol();

	if( objBook.maxTradeVol() != objBook.maxTradeVolNullValue() )
		objSecDef.MaxTradeVol = objBook.maxTradeVol();

	objSecDef.MinPriceIncrement = GetValue(objBook.minPriceIncrement());
	objSecDef.DisplayFactor = GetValue(objBook.displayFactor());

	if( objBook.tickRule() != objBook.tickRuleNullValue() )
		objSecDef.TickRule = objBook.tickRule();
	
	if( objBook.mainFraction() != objBook.mainFractionNullValue() )
		objSecDef.MainFraction = objBook.mainFraction();

	if( objBook.subFraction() != objBook.subFractionNullValue() )
		objSecDef.SubFraction = objBook.subFraction();

	if( objBook.priceDisplayFormat() != objBook.priceDisplayFormatNullValue() )
		objSecDef.PriceDisplayFormat = objBook.priceDisplayFormat();

	objSecDef.UnitOfMeasure = objBook.unitOfMeasure();

	objSecDef.HighLimitPrice = GetValue(objBook.highLimitPrice());
	objSecDef.LowLimitPrice = GetValue(objBook.lowLimitPrice());
	objSecDef.MaxPriceVariation = GetValue(objBook.maxPriceVariation());
	objSecDef.TradingReferencePrice = GetValue(objBook.tradingReferencePrice());

	setSettlPriceType(objSecDef.SettlPriceType, objBook.settlPriceType());

	if( objBook.openInterestQty() != objBook.openInterestQtyNullValue() )
		objSecDef.OpenInterestQty = objBook.openInterestQty();

	if( objBook.clearedVolume() != objBook.clearedVolumeNullValue() )
		objSecDef.ClearedVolume = objBook.clearedVolume();

	try
	{
		MDInstrumentDefinitionSpread29::NoEvents clobj2 = objBook.noEvents();
		for(int iCount(0); iCount < clobj2.count(); ++iCount )
		{
			MDInstrumentDefinitionSpread29::NoEvents clobjTemp  = clobj2.next();
		
			NoEvents objNoEvents;
			objNoEvents.EventType = clobjTemp.eventType();
			objNoEvents.EventTime = getTransactionTime(clobjTemp.eventTime());
			objSecDef.vNoEvents.push_back(objNoEvents);
		}

		MDInstrumentDefinitionSpread29::NoMDFeedTypes clobj1 = objBook.noMDFeedTypes();

		for(int iCount(0); iCount < clobj1.count(); ++iCount )
		{
			MDInstrumentDefinitionSpread29::NoMDFeedTypes clobjTemp  = clobj1.next();
		
			NoMdFeedTypes objNoMdFeedTypes;
			objNoMdFeedTypes.MDFeedType = clobjTemp.mDFeedType();
			if( objNoMdFeedTypes.MDFeedType.length() > 0 )
				objNoMdFeedTypes.MDFeedType = objNoMdFeedTypes.MDFeedType.substr(0, clobjTemp.mDFeedTypeLength());
			objNoMdFeedTypes.MarketDepth = clobjTemp.marketDepth();
			objSecDef.vNoMdFeedTypes.push_back(objNoMdFeedTypes);
		}

		MDInstrumentDefinitionSpread29::NoInstAttrib clobj3 = objBook.noInstAttrib();
		for(int iCount(0); iCount < clobj3.count(); ++iCount )
		{
			MDInstrumentDefinitionSpread29::NoInstAttrib clobjTemp  = clobj3.next();
		
			NoInstAttrib objNoInstAttrib;
			objNoInstAttrib.InstAttribType = clobjTemp.instAttribType();
			setInstAttribValue(objNoInstAttrib.InstAttribValue, clobjTemp.instAttribValue());
			objSecDef.vNoInstAttrib.push_back(objNoInstAttrib);
		}

		MDInstrumentDefinitionSpread29::NoLotTypeRules clobj4 = objBook.noLotTypeRules();
		for(int iCount(0); iCount < clobj4.count(); ++iCount )
		{
			MDInstrumentDefinitionSpread29::NoLotTypeRules clobjTemp  = clobj4.next();
		
			NoLotTypeRules objNoLotTypeRules;
			objNoLotTypeRules.LotType = clobjTemp.lotType();
			objNoLotTypeRules.MinLotSize = GetValue(clobjTemp.minLotSize());
			objSecDef.vNoLotTypeRules.push_back(objNoLotTypeRules);
		}

		MDInstrumentDefinitionSpread29::NoLegs clobj5 = objBook.noLegs();
		for(int iCount(0); iCount < clobj5.count(); ++iCount )
		{
			MDInstrumentDefinitionSpread29::NoLegs clobjTemp  = clobj5.next();
		
			NoLegs objNoLegs;
			objNoLegs.LegSecurityID = clobjTemp.legSecurityID();
			objNoLegs.LegSecurityIDSource = clobjTemp.legSecurityIDSource();
			objNoLegs.LegSide = clobjTemp.legSide();
			objNoLegs.LegRatioQty = clobjTemp.legRatioQty();
			objNoLegs.LegPrice = GetValue(clobjTemp.legPrice());
			objNoLegs.LegOptionDelta = GetValue(clobjTemp.legOptionDelta());

			if( objNoLegs.LegSecurityIDSource.length() > 0 )
				objNoLegs.LegSecurityIDSource = objNoLegs.LegSecurityIDSource.substr(0, clobjTemp.legSecurityIDSourceLength());

			objSecDef.vNoLegs.push_back(objNoLegs);
		}
	}
	catch(...)
	{
	}

	listContract.push_back(objSecDef);
}

void OptionContractTemplate41Decoder::decode(Message &message, SecurityDefList &listContract, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr)
{
	SecurityDefinition objSecDef;
	MDInstrumentDefinitionOption41 objBook;
	objBook.wrapForDecode(const_cast<char*>(message.buffer), nOffset, hdr.blockLength(), hdr.version(), message.length);

	objSecDef.MsgSeqNum = sequence;
	objSecDef.SendingTime = getTransactionTime(sentTime);

	setMatchEventIndicator(objSecDef.MatchEventIndicator, objBook.matchEventIndicator());

	objSecDef.objContract.SecurityID = objBook.securityID();
	objSecDef.SecurityIDSource = objBook.securityIDSource();
	objSecDef.objContract.ApplID = objBook.applID();;
	objSecDef.MarketSegmentID = objBook.marketSegmentID();
	objSecDef.objContract.SecurityExchange = objBook.securityExchange();
	objSecDef.objContract.CFICode = objBook.cFICode();

	if( objSecDef.SecurityIDSource.length() > 0 )
		objSecDef.SecurityIDSource = objSecDef.SecurityIDSource.substr(0, objBook.securityIDSourceLength());

	if( objSecDef.objContract.SecurityExchange.length() > 0 )
		objSecDef.objContract.SecurityExchange = objSecDef.objContract.SecurityExchange.substr(0, objBook.securityExchangeLength());

	objSecDef.objContract.SecurityGroup = objBook.securityGroup();
	objSecDef.objContract.Symbol = objBook.symbol();

	if( objSecDef.objContract.Symbol.length() > 0 )
		objSecDef.objContract.Symbol = objSecDef.objContract.Symbol.substr(0, objBook.symbolLength());

	if( objSecDef.objContract.CFICode.length() > 0 )
		objSecDef.objContract.CFICode = objSecDef.objContract.CFICode.substr(0, objBook.cFICodeLength());

	objSecDef.UnderlyingProduct = objBook.underlyingProduct();
	objSecDef.objContract.Asset = objBook.asset();
	objSecDef.SecurityType = objBook.securityType();

	if( objSecDef.UnderlyingProduct == objBook.underlyingProductNullValue() )
		objSecDef.UnderlyingProduct = 0;

	int year = objBook.maturityMonthYear().year();
	int month = objBook.maturityMonthYear().month();

	stringstream strstream;
	strstream << std::setfill('0') << std::setw(4) << year
		      << std::setfill('0') << std::setw(2) << month;
	
	objSecDef.MaturityMonthYear = strstream.str();

	if( objBook.totNumReports() != objBook.totNumReportsNullValue() )
		objSecDef.TotNumReports = objBook.totNumReports();

	objSecDef.SecurityUpdateAction = objBook.securityUpdateAction();
	if( objBook.lastUpdateTime() != objBook.lastUpdateTimeNullValue() )
		objSecDef.LastUpdateTime = getTransactionTime(objBook.lastUpdateTime());
	objSecDef.PutOrCall = objBook.putOrCall();
	objSecDef.UserDefinedInstrument = objBook.userDefinedInstrument();
	objSecDef.MDSecurityTradingStatus = objBook.mDSecurityTradingStatus();

	if( objSecDef.MDSecurityTradingStatus == SecurityTradingStatus::NULL_VALUE )
		objSecDef.MDSecurityTradingStatus = 0;

	objSecDef.StrikePrice = GetValue(objBook.strikePrice());
	objSecDef.StrikeCurrency = objBook.strikeCurrency();
	objSecDef.Currency = objBook.currency();
	objSecDef.SettlCurrency = objBook.settlCurrency();
	objSecDef.MinCabPrice = GetValue(objBook.minCabPrice());
	objSecDef.MatchAlgorithm = objBook.matchAlgorithm();

	if( objSecDef.Currency.length() > 0 )
		objSecDef.Currency = objSecDef.Currency.substr(0, objBook.currencyLength());

	if( objSecDef.SettlCurrency.length() > 0 )
		objSecDef.SettlCurrency = objSecDef.SettlCurrency.substr(0, objBook.settlCurrencyLength());
	
	if( objBook.minTradeVol() != objBook.minTradeVolNullValue() )
		objSecDef.MinTradeVol = objBook.minTradeVol();

	if( objBook.maxTradeVol() != objBook.maxTradeVolNullValue() )
		objSecDef.MaxTradeVol = objBook.maxTradeVol();

	objSecDef.MinPriceIncrement = GetValue(objBook.minPriceIncrement());
	objSecDef.MinPriceIncrementAmount = GetValue(objBook.minPriceIncrementAmount());
	objSecDef.DisplayFactor = GetValue(objBook.displayFactor());

	if( objBook.tickRule() != objBook.tickRuleNullValue() )
		objSecDef.TickRule = objBook.tickRule();

	if( objBook.mainFraction() != objBook.mainFractionNullValue() )
		objSecDef.MainFraction = objBook.mainFraction();

	if( objBook.subFraction() != objBook.subFractionNullValue() )
		objSecDef.SubFraction = objBook.subFraction();

	if( objBook.priceDisplayFormat() != objBook.priceDisplayFormatNullValue() )
		objSecDef.PriceDisplayFormat = objBook.priceDisplayFormat();

	objSecDef.UnitOfMeasure = objBook.unitOfMeasure();
	objSecDef.UnitOfMeasureQty = GetValue(objBook.unitOfMeasureQty());
	objSecDef.HighLimitPrice = GetValue(objBook.highLimitPrice());
	objSecDef.LowLimitPrice = GetValue(objBook.lowLimitPrice());
	objSecDef.TradingReferencePrice = GetValue(objBook.tradingReferencePrice());

	setSettlPriceType(objSecDef.SettlPriceType, objBook.settlPriceType());

	if( objBook.openInterestQty() != objBook.openInterestQtyNullValue() )
		objSecDef.OpenInterestQty = objBook.openInterestQty();

	if( objBook.clearedVolume() != objBook.clearedVolumeNullValue() )
		objSecDef.ClearedVolume = objBook.clearedVolume();

	try
	{
		MDInstrumentDefinitionOption41::NoEvents clobj2 = objBook.noEvents();
		for(int iCount(0); iCount < clobj2.count(); ++iCount )
		{
			MDInstrumentDefinitionOption41::NoEvents clobjTemp  = clobj2.next();
			NoEvents objNoEvents;
			objNoEvents.EventType = clobjTemp.eventType();
			objNoEvents.EventTime = getTransactionTime(clobjTemp.eventTime());
			objSecDef.vNoEvents.push_back(objNoEvents);
		}

		MDInstrumentDefinitionOption41::NoMDFeedTypes clobj1 = objBook.noMDFeedTypes();
		for(int iCount(0); iCount < clobj1.count(); ++iCount )
		{
			MDInstrumentDefinitionOption41::NoMDFeedTypes clobjTemp  = clobj1.next();
			NoMdFeedTypes objNoMdFeedTypes;
			objNoMdFeedTypes.MDFeedType = clobjTemp.mDFeedType();
			if( objNoMdFeedTypes.MDFeedType.length() > 0 )
				objNoMdFeedTypes.MDFeedType = objNoMdFeedTypes.MDFeedType.substr(0, clobjTemp.mDFeedTypeLength());
			objNoMdFeedTypes.MarketDepth = clobjTemp.marketDepth();
			objSecDef.vNoMdFeedTypes.push_back(objNoMdFeedTypes);
		}

		MDInstrumentDefinitionOption41::NoInstAttrib clobj3 = objBook.noInstAttrib();
		for(int iCount(0); iCount < clobj3.count(); ++iCount )
		{
			MDInstrumentDefinitionOption41::NoInstAttrib clobjTemp  = clobj3.next();
			NoInstAttrib objNoInstAttrib;
			objNoInstAttrib.InstAttribType = clobjTemp.instAttribType();
			setInstAttribValue(objNoInstAttrib.InstAttribValue, clobjTemp.instAttribValue());
			objSecDef.vNoInstAttrib.push_back(objNoInstAttrib);
		}

		MDInstrumentDefinitionOption41::NoLotTypeRules clobj4 = objBook.noLotTypeRules();
		for(int iCount(0); iCount < clobj4.count(); ++iCount )
		{
			MDInstrumentDefinitionOption41::NoLotTypeRules clobjTemp  = clobj4.next();
			NoLotTypeRules objNoLotTypeRules;
			objNoLotTypeRules.LotType = clobjTemp.lotType();
			objNoLotTypeRules.MinLotSize = GetValue(clobjTemp.minLotSize());
			objSecDef.vNoLotTypeRules.push_back(objNoLotTypeRules);
		}

		MDInstrumentDefinitionOption41::NoUnderlyings clobj5 = objBook.noUnderlyings();
		for(int iCount(0); iCount < clobj5.count(); ++iCount )
		{
			MDInstrumentDefinitionOption41::NoUnderlyings clobjTemp  = clobj5.next();
			NoUnderlyings objNoUnderlyings;
			objNoUnderlyings.UnderlyingSymbol = clobjTemp.underlyingSymbol();
			objNoUnderlyings.UnderlyingSecurityID = clobjTemp.underlyingSecurityID();
			objNoUnderlyings.UnderlyingSecurityIDSource = clobjTemp.underlyingSecurityIDSource();
			objSecDef.vNoUnderlyings.push_back(objNoUnderlyings);
		}
	}
	catch(...)
	{
	}

	listContract.push_back(objSecDef);
}

SecurityStatus30Decoder::SecurityStatus30Decoder()
{
	MSGTYPE = "f";
}

SecurityStatus30Decoder::~SecurityStatus30Decoder()
{
}

void SecurityStatus30Decoder::decode(Message &message, SecurityStatusList &listSecurityStatus, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr)
{
	SecurityStatus30 mdIncrBook32;
	mdIncrBook32.wrapForDecode(const_cast<char*>(message.buffer), nOffset, hdr.blockLength(), hdr.version(), message.length);

	SecurityStatus secStatus;

	setMatchEventIndicator(secStatus.MatchEventIndicator, mdIncrBook32.matchEventIndicator());
	
	secStatus.MsgTyp = MSGTYPE;
	secStatus.MsgSeqNum = sequence;
	secStatus.SendingTime = getTransactionTime(sentTime);
	secStatus.TransactTime = getTransactionTime(mdIncrBook32.transactTime());

	secStatus.HaltReason = mdIncrBook32.haltReason();
	secStatus.SecurityTradingEvent = mdIncrBook32.securityTradingEvent();
	secStatus.SecurityTradingStatus = mdIncrBook32.securityTradingStatus();
	
	secStatus.objContract.SecurityGroup = mdIncrBook32.securityGroup();

	//if( !secStatus.objContract.SecurityGroup.empty() )
	//	enrichChannel(secStatus.objContract, secStatus.objContract.SecurityGroup, true);
	
	secStatus.objContract.Asset = mdIncrBook32.asset();

	//if( !secStatus.objContract.Asset.empty() )
	//	enrichChannel(secStatus.objContract, secStatus.objContract.Asset, false);

	secStatus.TradeDate = getTradeDate((__int64)mdIncrBook32.tradeDate() * 24 * 60 * 60 * 1000000000);

	if( mdIncrBook32.securityID() != mdIncrBook32.securityIDNullValue() )
	{
		secStatus.objContract.SecurityID = secStatus.SecurityID = mdIncrBook32.securityID();
		enrichIncrementalRefresh(secStatus.objContract);
	}
	
	listSecurityStatus.push_back(secStatus);
}

IncrementalRefresh32Decoder::IncrementalRefresh32Decoder()
{
	MSGTYPE = "X";
}

IncrementalRefresh32Decoder::~IncrementalRefresh32Decoder()
{
}

void IncrementalRefresh32Decoder::decode(Message &message, MarketDataList &listMktData, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr)
{
	MDIncrementalRefreshBook32 mdIncrBook32;
	mdIncrBook32.wrapForDecode(const_cast<char*>(message.buffer), nOffset, hdr.blockLength(), hdr.version(), message.length);
	MDIncrementalRefreshBook32::NoMDEntries clobj = mdIncrBook32.noMDEntries();

	MarketData mktData;
	mktData.nOffset = nOffset;
	mktData.MsgTyp = MSGTYPE;
	mktData.MsgSeqNum = sequence;
	mktData.SendingTime = getTransactionTime(sentTime);
	mktData.TransactTime = getTransactionTime(mdIncrBook32.transactTime());
	mktData.TradeDate = getTradeDate(mdIncrBook32.transactTime());
	setMatchEventIndicator(mktData.MatchEventIndicator, mdIncrBook32.matchEventIndicator());

	for(int iCount(0); iCount < clobj.count(); ++iCount )
	{
		MDEntry objMDEntry;
		MDIncrementalRefreshBook32::NoMDEntries clobjTemp  = clobj.next();

		objMDEntry.MDEntryPx = GetValue(clobjTemp.mDEntryPx());
		if( clobjTemp.mDEntrySize() != clobjTemp.mDEntrySizeNullValue() )
			objMDEntry.MDEntrySize = clobjTemp.mDEntrySize();
		objMDEntry.MDEntryType = clobjTemp.mDEntryType();
		objMDEntry.MDPriceLevel = clobjTemp.mDPriceLevel();
		objMDEntry.MDUpdateAction = clobjTemp.mDUpdateAction();
		objMDEntry.NumberOfOrders = clobjTemp.numberOfOrders();
		objMDEntry.RptSeq = clobjTemp.rptSeq();
		objMDEntry.SecurityID = clobjTemp.securityID();

		if( objMDEntry.NumberOfOrders == clobjTemp.numberOfOrdersNullValue() )
			objMDEntry.NumberOfOrders = 0;

		mktData.objContract.SecurityID = objMDEntry.SecurityID;
		enrichIncrementalRefresh(mktData.objContract);
		objMDEntry.Symbol = mktData.objContract.Symbol;

		mktData.buildoutputfile();

		TheBEAST::SequenceCheck::getInstance()->CheckSequenceSec(objMDEntry.SecurityID, objMDEntry.RptSeq);

		bool bNewMessage(true);
		for( MarketDataListItr itrMktData = listMktData.begin(); itrMktData != listMktData.end(); itrMktData++)
		{
			if( itrMktData->nOffset == mktData.nOffset )
			{
				if( itrMktData->getoutputfile() == mktData.getoutputfile() )
				{
					itrMktData->vMDEntry.push_back(objMDEntry);
					bNewMessage = false;
					break;
				}
			}
		}

		if(bNewMessage)
		{
			mktData.vMDEntry.clear();
			mktData.vMDEntry.push_back(objMDEntry);
			listMktData.push_back(mktData);
		}
	}
}

IncrementalRefresh33Decoder::IncrementalRefresh33Decoder()
{
	MSGTYPE = "X";
}

IncrementalRefresh33Decoder::~IncrementalRefresh33Decoder()
{
}

void IncrementalRefresh33Decoder::decode(Message &message, MarketDataList &listMktData, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr)
{
	MDIncrementalRefreshDailyStatistics33 mdIncrBook32;
	mdIncrBook32.wrapForDecode(const_cast<char*>(message.buffer), nOffset, hdr.blockLength(), hdr.version(), message.length);
	MDIncrementalRefreshDailyStatistics33::NoMDEntries clobj = mdIncrBook32.noMDEntries();

	MarketData mktData;
	mktData.nOffset = nOffset;
	mktData.MsgTyp = MSGTYPE;
	mktData.MsgSeqNum = sequence;
	mktData.SendingTime = getTransactionTime(sentTime);
	mktData.TransactTime = getTransactionTime(mdIncrBook32.transactTime());
	mktData.TradeDate = getTradeDate(mdIncrBook32.transactTime());
	setMatchEventIndicator(mktData.MatchEventIndicator, mdIncrBook32.matchEventIndicator());
	
	for(int iCount(0); iCount < clobj.count(); ++iCount)
	{
		MDEntry objMDEntry;
		MDIncrementalRefreshDailyStatistics33::NoMDEntries clobjTemp  = clobj.next();

		setSettlPriceType(objMDEntry.SettlPriceType, clobj.settlPriceType());
		
		objMDEntry.MDEntryPx = GetValue(clobjTemp.mDEntryPx());
		if( clobjTemp.mDEntrySize() != clobjTemp.mDEntrySizeNullValue() )
			objMDEntry.MDEntrySize = clobjTemp.mDEntrySize();
		objMDEntry.MDEntryType = clobjTemp.mDEntryType();
		objMDEntry.MDUpdateAction = clobjTemp.mDUpdateAction();
		objMDEntry.TradingReferenceDate = getTradeDate((__int64)clobjTemp.tradingReferenceDate() * 24 * 60 * 60 * 1000000000); 
		objMDEntry.RptSeq = clobjTemp.rptSeq();
		objMDEntry.SecurityID = clobjTemp.securityID();
		
		mktData.objContract.SecurityID = objMDEntry.SecurityID;
		enrichIncrementalRefresh(mktData.objContract);
		objMDEntry.Symbol = mktData.objContract.Symbol;

		mktData.buildoutputfile();

		TheBEAST::SequenceCheck::getInstance()->CheckSequenceSec(objMDEntry.SecurityID, objMDEntry.RptSeq);

		bool bNewMessage(true);
		for( MarketDataListItr itrMktData = listMktData.begin(); itrMktData != listMktData.end(); itrMktData++)
		{
			if( itrMktData->nOffset == mktData.nOffset )
			{
				if( itrMktData->getoutputfile() == mktData.getoutputfile() )
				{
					itrMktData->vMDEntry.push_back(objMDEntry);
					bNewMessage = false;
					break;
				}
			}
		}

		if(bNewMessage)
		{
			mktData.vMDEntry.clear();
			mktData.vMDEntry.push_back(objMDEntry);
			listMktData.push_back(mktData);
		}
	}
}

IncrementalRefresh34Decoder::IncrementalRefresh34Decoder()
{
	MSGTYPE = "X";
}

IncrementalRefresh34Decoder::~IncrementalRefresh34Decoder()
{
}

void IncrementalRefresh34Decoder::decode(Message &message, MarketDataList &listMktData, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr)
{
	MDIncrementalRefreshLimitsBanding34 mdIncrBook32;
	mdIncrBook32.wrapForDecode(const_cast<char*>(message.buffer), nOffset, hdr.blockLength(), hdr.version(), message.length);
	MDIncrementalRefreshLimitsBanding34::NoMDEntries clobj = mdIncrBook32.noMDEntries();
	
	MarketData mktData;
	mktData.nOffset = nOffset;
	mktData.MsgTyp = MSGTYPE;
	mktData.MsgSeqNum = sequence;
	mktData.SendingTime = getTransactionTime(sentTime);
	mktData.TransactTime = getTransactionTime(mdIncrBook32.transactTime());
	mktData.TradeDate = getTradeDate(mdIncrBook32.transactTime());
	setMatchEventIndicator(mktData.MatchEventIndicator, mdIncrBook32.matchEventIndicator());

	for(int iCount(0); iCount < clobj.count(); ++iCount )
	{
		MDEntry objMDEntry;
		MDIncrementalRefreshLimitsBanding34::NoMDEntries clobjTemp  = clobj.next();

		objMDEntry.MDEntryType = *clobjTemp.mDEntryType();
		objMDEntry.MDUpdateAction = clobjTemp.mDUpdateAction();
		objMDEntry.RptSeq = clobjTemp.rptSeq();
		objMDEntry.SecurityID = clobjTemp.securityID();
		objMDEntry.HighLimitPrice = GetValue(clobjTemp.highLimitPrice());
		objMDEntry.LowLimitPrice = GetValue(clobjTemp.lowLimitPrice());
		objMDEntry.MaxPriceVariation = GetValue(clobjTemp.maxPriceVariation());

		mktData.objContract.SecurityID = objMDEntry.SecurityID;
		enrichIncrementalRefresh(mktData.objContract);
		objMDEntry.Symbol = mktData.objContract.Symbol;

		mktData.buildoutputfile();

		TheBEAST::SequenceCheck::getInstance()->CheckSequenceSec(objMDEntry.SecurityID, objMDEntry.RptSeq);

		bool bNewMessage(true);
		for( MarketDataListItr itrMktData = listMktData.begin(); itrMktData != listMktData.end(); itrMktData++)
		{
			if( itrMktData->nOffset == mktData.nOffset )
			{
				if( itrMktData->getoutputfile() == mktData.getoutputfile() )
				{
					itrMktData->vMDEntry.push_back(objMDEntry);
					bNewMessage = false;
					break;
				}
			}
		}

		if(bNewMessage)
		{
			mktData.vMDEntry.clear();
			mktData.vMDEntry.push_back(objMDEntry);
			listMktData.push_back(mktData);
		}
	}
}

IncrementalRefresh35Decoder::IncrementalRefresh35Decoder()
{
	MSGTYPE = "X";
}

IncrementalRefresh35Decoder::~IncrementalRefresh35Decoder()
{
}

void IncrementalRefresh35Decoder::decode(Message &message, MarketDataList &listMktData, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr)
{
	MDIncrementalRefreshSessionStatistics35 mdIncrBook32;
	mdIncrBook32.wrapForDecode(const_cast<char*>(message.buffer), nOffset, hdr.blockLength(), hdr.version(), message.length);
	MDIncrementalRefreshSessionStatistics35::NoMDEntries clobj = mdIncrBook32.noMDEntries();
	
	MarketData mktData;
	mktData.nOffset = nOffset;
	mktData.MsgTyp = MSGTYPE;
	mktData.MsgSeqNum = sequence;
	mktData.SendingTime = getTransactionTime(sentTime);
	mktData.TransactTime = getTransactionTime(mdIncrBook32.transactTime());
	mktData.TradeDate = getTradeDate(mdIncrBook32.transactTime());
	setMatchEventIndicator(mktData.MatchEventIndicator, mdIncrBook32.matchEventIndicator());

	for(int iCount(0); iCount < clobj.count(); ++iCount )
	{
		MDEntry objMDEntry;
		MDIncrementalRefreshSessionStatistics35::NoMDEntries clobjTemp  = clobj.next();

		objMDEntry.MDEntryPx = GetValue(clobjTemp.mDEntryPx());
		objMDEntry.MDEntryType = clobjTemp.mDEntryType();
		objMDEntry.MDUpdateAction = clobjTemp.mDUpdateAction();
		objMDEntry.RptSeq = clobjTemp.rptSeq();
		objMDEntry.SecurityID = clobjTemp.securityID();
		objMDEntry.OpenCloseSettlFlag = clobjTemp.openCloseSettlFlag();

		mktData.objContract.SecurityID = objMDEntry.SecurityID;
		enrichIncrementalRefresh(mktData.objContract);
		objMDEntry.Symbol = mktData.objContract.Symbol;

		mktData.buildoutputfile();

		TheBEAST::SequenceCheck::getInstance()->CheckSequenceSec(objMDEntry.SecurityID, objMDEntry.RptSeq);

		bool bNewMessage(true);
		for( MarketDataListItr itrMktData = listMktData.begin(); itrMktData != listMktData.end(); itrMktData++)
		{
			if( itrMktData->nOffset == mktData.nOffset )
			{
				if( itrMktData->getoutputfile() == mktData.getoutputfile() )
				{
					itrMktData->vMDEntry.push_back(objMDEntry);
					bNewMessage = false;
					break;
				}
			}
		}

		if(bNewMessage)
		{
			mktData.vMDEntry.clear();
			mktData.vMDEntry.push_back(objMDEntry);
			listMktData.push_back(mktData);
		}
	}
}

IncrementalRefresh36Decoder::IncrementalRefresh36Decoder()
{
	MSGTYPE = "X";
}

IncrementalRefresh36Decoder::~IncrementalRefresh36Decoder()
{
}

void IncrementalRefresh36Decoder::decode(Message &message, MarketDataList &listMktData, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr)
{
	MDIncrementalRefreshTrade36 mdIncrBook32;
	mdIncrBook32.wrapForDecode(const_cast<char*>(message.buffer), nOffset, hdr.blockLength(), hdr.version(), message.length);
	MDIncrementalRefreshTrade36::NoMDEntries clobj = mdIncrBook32.noMDEntries();
	
	MarketData mktData;
	mktData.nOffset = nOffset;
	mktData.MsgTyp = MSGTYPE;
	mktData.MsgSeqNum = sequence;
	mktData.SendingTime = getTransactionTime(sentTime);
	mktData.TransactTime = getTransactionTime(mdIncrBook32.transactTime());
	mktData.TradeDate = getTradeDate(mdIncrBook32.transactTime());
	setMatchEventIndicator(mktData.MatchEventIndicator, mdIncrBook32.matchEventIndicator());

	for(int iCount(0); iCount < clobj.count(); ++iCount )
	{
		MDEntry objMDEntry;
		MDIncrementalRefreshTrade36::NoMDEntries clobjTemp  = clobj.next();

		objMDEntry.MDEntryPx = GetValue(clobjTemp.mDEntryPx());
		if( clobjTemp.mDEntrySize() != clobjTemp.mDEntrySizeNullValue() )
			objMDEntry.MDEntrySize = clobjTemp.mDEntrySize();
		objMDEntry.MDEntryType = *clobjTemp.mDEntryType();
		objMDEntry.MDUpdateAction = clobjTemp.mDUpdateAction();
		objMDEntry.NumberOfOrders = clobjTemp.numberOfOrders();
		objMDEntry.RptSeq = clobjTemp.rptSeq();
		objMDEntry.SecurityID = clobjTemp.securityID();

		objMDEntry.TradeID = clobjTemp.tradeID();
		objMDEntry.AggressorSide = clobjTemp.aggressorSide();

		if( objMDEntry.NumberOfOrders == clobjTemp.numberOfOrdersNullValue() )
			objMDEntry.NumberOfOrders = 0;

		mktData.objContract.SecurityID = objMDEntry.SecurityID;
		enrichIncrementalRefresh(mktData.objContract);
		objMDEntry.Symbol = mktData.objContract.Symbol;

		mktData.buildoutputfile();

		TheBEAST::SequenceCheck::getInstance()->CheckSequenceSec(objMDEntry.SecurityID, objMDEntry.RptSeq);

		bool bNewMessage(true);
		for( MarketDataListItr itrMktData = listMktData.begin(); itrMktData != listMktData.end(); itrMktData++)
		{
			if( itrMktData->nOffset == mktData.nOffset )
			{
				if( itrMktData->getoutputfile() == mktData.getoutputfile() )
				{
					itrMktData->vMDEntry.push_back(objMDEntry);
					bNewMessage = false;
					break;
				}
			}
		}

		if(bNewMessage)
		{
			mktData.vMDEntry.clear();
			mktData.vMDEntry.push_back(objMDEntry);
			listMktData.push_back(mktData);
		}
	}
}

IncrementalRefresh37Decoder::IncrementalRefresh37Decoder()
{
	MSGTYPE = "X";
}

IncrementalRefresh37Decoder::~IncrementalRefresh37Decoder()
{
}


void IncrementalRefresh37Decoder::decode(Message &message, MarketDataList &listMktData, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr)
{
	MDIncrementalRefreshVolume37 mdIncrBook32;
	mdIncrBook32.wrapForDecode(const_cast<char*>(message.buffer), nOffset, hdr.blockLength(), hdr.version(), message.length);
	MDIncrementalRefreshVolume37::NoMDEntries clobj = mdIncrBook32.noMDEntries();
	
	MarketData mktData;
	mktData.nOffset = nOffset;
	mktData.MsgTyp = MSGTYPE;
	mktData.MsgSeqNum = sequence;
	mktData.SendingTime = getTransactionTime(sentTime);
	mktData.TransactTime = getTransactionTime(mdIncrBook32.transactTime());
	mktData.TradeDate = getTradeDate(mdIncrBook32.transactTime());
	setMatchEventIndicator(mktData.MatchEventIndicator, mdIncrBook32.matchEventIndicator());

	for(int iCount(0); iCount < clobj.count(); ++iCount )
	{
		MDEntry objMDEntry;
		MDIncrementalRefreshVolume37::NoMDEntries clobjTemp  = clobj.next();

		if( clobjTemp.mDEntrySize() != clobjTemp.mDEntrySizeNullValue() )
			objMDEntry.MDEntrySize = clobjTemp.mDEntrySize();
		objMDEntry.MDEntryType = *clobjTemp.mDEntryType();
		objMDEntry.MDUpdateAction = clobjTemp.mDUpdateAction();
		objMDEntry.RptSeq = clobjTemp.rptSeq();
		objMDEntry.SecurityID = clobjTemp.securityID();

		mktData.objContract.SecurityID = objMDEntry.SecurityID;
		enrichIncrementalRefresh(mktData.objContract);
		objMDEntry.Symbol = mktData.objContract.Symbol;

		mktData.buildoutputfile();

		TheBEAST::SequenceCheck::getInstance()->CheckSequenceSec(objMDEntry.SecurityID, objMDEntry.RptSeq);

		bool bNewMessage(true);
		for( MarketDataListItr itrMktData = listMktData.begin(); itrMktData != listMktData.end(); itrMktData++)
		{
			if( itrMktData->nOffset == mktData.nOffset )
			{
				if( itrMktData->getoutputfile() == mktData.getoutputfile() )
				{
					itrMktData->vMDEntry.push_back(objMDEntry);
					bNewMessage = false;
					break;
				}
			}
		}

		if(bNewMessage)
		{
			mktData.vMDEntry.clear();
			mktData.vMDEntry.push_back(objMDEntry);
			listMktData.push_back(mktData);
		}
	}
}

QuoteRequest39Decoder::QuoteRequest39Decoder()
{
	MSGTYPE = "R";
}

QuoteRequest39Decoder::~QuoteRequest39Decoder()
{
}

void QuoteRequest39Decoder::decode(Message &message, QuoteRequestList &listQuoteRequest, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr)
{
	QuoteRequest39 mdIncrBook32;
	mdIncrBook32.wrapForDecode(const_cast<char*>(message.buffer), nOffset, hdr.blockLength(), hdr.version(), message.length);
	QuoteRequest39::NoRelatedSym clobj = mdIncrBook32.noRelatedSym();
	
	int iRecordCount = clobj.count();

	QuoteRequest quoteRequest;
	quoteRequest.nOffset = nOffset;
	quoteRequest.MsgTyp = MSGTYPE;
	quoteRequest.MsgSeqNum = sequence;
	quoteRequest.SendingTime = getTransactionTime(sentTime);
	quoteRequest.TransactTime = getTransactionTime(mdIncrBook32.transactTime());
	quoteRequest.TradeDate = getTradeDate(mdIncrBook32.transactTime());
	setMatchEventIndicator(quoteRequest.MatchEventIndicator, mdIncrBook32.matchEventIndicator());

	for(int iCount(0); iCount < iRecordCount; ++iCount )
	{
		
		NoRelatedSym objNoSym;
		QuoteRequest39::NoRelatedSym clobjTemp  = clobj.next();

		objNoSym.SecurityID = clobjTemp.securityID();

		quoteRequest.QuoteReqID = mdIncrBook32.quoteReqID();
		objNoSym.Symbol = clobjTemp.symbol();
		objNoSym.OrderQty = clobjTemp.orderQty();
		objNoSym.QuoteType = clobjTemp.quoteType();
		objNoSym.Side = clobjTemp.side();

		if( objNoSym.Symbol.length() > 0 )
			objNoSym.Symbol = objNoSym.Symbol.substr(0, clobjTemp.symbolLength());

		if( objNoSym.OrderQty == clobjTemp.orderQtyNullValue() )
			objNoSym.OrderQty = 0;

		if( objNoSym.Side == clobjTemp.sideNullValue() )
			objNoSym.Side = 0;

		quoteRequest.objContract.SecurityID = objNoSym.SecurityID;
		enrichIncrementalRefresh(quoteRequest.objContract);

		quoteRequest.buildoutputfile();

		bool bNewMessage(true);
		for( QuoteRequestListItr itrQuote = listQuoteRequest.begin(); itrQuote != listQuoteRequest.end(); itrQuote++)
		{
			if( itrQuote->nOffset == quoteRequest.nOffset )
			{
				if( itrQuote->getoutputfile() == quoteRequest.getoutputfile() )
				{
					itrQuote->vNoRelatedSym.push_back(objNoSym);
					bNewMessage = false;
					break;
				}
			}
		}

		if(bNewMessage)
		{
			quoteRequest.vNoRelatedSym.clear();
			quoteRequest.vNoRelatedSym.push_back(objNoSym);
			listQuoteRequest.push_back(quoteRequest);
		}
	}
}

IncrementalRefresh42Decoder::IncrementalRefresh42Decoder()
{
	MSGTYPE = "X";
}

IncrementalRefresh42Decoder::~IncrementalRefresh42Decoder()
{
}

void IncrementalRefresh42Decoder::decode(Message &message, MarketDataList &listMktData, long sequence, __int64 sentTime, int nOffset, MessageHeader &hdr)
{
	MDIncrementalRefreshTradeSummary42 mdIncrBook32;
	mdIncrBook32.wrapForDecode(const_cast<char*>(message.buffer), nOffset, hdr.blockLength(), hdr.version(), message.length);
	MDIncrementalRefreshTradeSummary42::NoMDEntries clobj = mdIncrBook32.noMDEntries();
	
	MarketData mktData;
	mktData.nOffset = nOffset;
	mktData.MsgTyp = MSGTYPE;
	mktData.MsgSeqNum = sequence;
	mktData.SendingTime = getTransactionTime(sentTime);
	mktData.TransactTime = getTransactionTime(mdIncrBook32.transactTime());
	mktData.TradeDate = getTradeDate(mdIncrBook32.transactTime());
	setMatchEventIndicator(mktData.MatchEventIndicator, mdIncrBook32.matchEventIndicator());

	int n = clobj.count();

	for(int iCount(0); iCount < clobj.count(); ++iCount )
	{
		MDEntry objMDEntry;
		MDIncrementalRefreshTradeSummary42::NoMDEntries clobjTemp  = clobj.next();

		objMDEntry.MDEntryPx = GetValue(clobjTemp.mDEntryPx());
		if( clobjTemp.mDEntrySize() != clobjTemp.mDEntrySizeNullValue() )
			objMDEntry.MDEntrySize = clobjTemp.mDEntrySize();
		objMDEntry.SecurityID = clobjTemp.securityID();
		objMDEntry.RptSeq = clobjTemp.rptSeq();
		objMDEntry.NumberOfOrders = clobjTemp.numberOfOrders();
		objMDEntry.AggressorSide = clobjTemp.aggressorSide();
		objMDEntry.MDUpdateAction = clobjTemp.mDUpdateAction();
		objMDEntry.MDEntryType = *clobjTemp.mDEntryType();
		
		if( objMDEntry.NumberOfOrders == clobjTemp.numberOfOrdersNullValue() )
			objMDEntry.NumberOfOrders = 0;

		mktData.objContract.SecurityID = objMDEntry.SecurityID;
		enrichIncrementalRefresh(mktData.objContract);
		objMDEntry.Symbol = mktData.objContract.Symbol;

		mktData.buildoutputfile();

		TheBEAST::SequenceCheck::getInstance()->CheckSequenceSec(objMDEntry.SecurityID, objMDEntry.RptSeq);

		bool bNewMessage(true);
		for( MarketDataListItr itrMktData = listMktData.begin(); itrMktData != listMktData.end(); itrMktData++)
		{
			if( itrMktData->nOffset == mktData.nOffset )
			{
				if( itrMktData->getoutputfile() == mktData.getoutputfile() )
				{
					itrMktData->vMDEntry.push_back(objMDEntry);
					bNewMessage = false;
					break;
				}
			}
		}

		if(bNewMessage)
		{
			mktData.vMDEntry.clear();
			mktData.vMDEntry.push_back(objMDEntry);
			listMktData.push_back(mktData);
		}
	}	

	MDIncrementalRefreshTradeSummary42::NoOrderIDEntries objOrders = mdIncrBook32.noOrderIDEntries();
	OrderIDEntry objOrderIDEntry;
	vector<OrderIDEntry> vOrderIDEntry;
	
	for(int iCount(0); iCount < objOrders.count(); ++iCount )
	{
		MDIncrementalRefreshTradeSummary42::NoOrderIDEntries clobjTemp  = objOrders.next();

		objOrderIDEntry.OrderID = clobjTemp.orderID();
		objOrderIDEntry.LastQty = clobjTemp.lastQty();
		vOrderIDEntry.push_back(objOrderIDEntry);
	}

	int nStart(0);
	for( MarketDataListItr itrMktData = listMktData.begin(); itrMktData != listMktData.end(); itrMktData++)
	{
		int nCount(0);
		for(int iCount(0); iCount < itrMktData->vMDEntry.size(); ++iCount)
			nCount += itrMktData->vMDEntry[iCount].NumberOfOrders;

		for(int iCount(nStart); iCount < (nStart+nCount); ++iCount)
		{
			if( iCount < vOrderIDEntry.size() )
				itrMktData->vOrderIDEntry.push_back(vOrderIDEntry[iCount]);
		}

		nStart += nCount;
	}
}

MDMessageConvertor::MDMessageConvertor()
{
	BINARY_PACKET_HEADER = 12;
	
	// Contracts
	m_strContractFile = "";
	m_pContactFile = NULL;

	// Master Security Definition
	m_strMstSecDefFile = "";
	m_pMstSecDefFile = NULL;
}

MDMessageConvertor::~MDMessageConvertor()
{
}

sbe_uint32_t MarketDataDecoderUtil::getPacketSequenceNumber(const char *buffer)
{
	return SBE_LITTLE_ENDIAN_ENCODE_32(*((sbe_uint32_t *)(buffer + 0)));
}

sbe_uint64_t MarketDataDecoderUtil::getPacketSentEpochTime(const char *buffer)
{
	return SBE_LITTLE_ENDIAN_ENCODE_64(*((sbe_uint64_t *)(buffer + 4)));
}

sbe_uint16_t MarketDataDecoderUtil::getMsgSize(const char *buffer, int offset)
{
	return SBE_LITTLE_ENDIAN_ENCODE_16(*((sbe_uint16_t *)(buffer + offset)));
}

int MDMessageConvertor::getTemplateId(const char *buffer, int offset, int bufferLength)
{
	MessageHeader hdr;
	hdr.wrap(const_cast<char*>(buffer), offset, 0, bufferLength);
	return hdr.templateId();
}

void MDMessageConvertor::convertMessage(Message &message)
{
	ChannelResetList lstChannelReset;
	MarketDataList lstMktData;
	SecurityDefList lstContract;
	SecurityStatusList lstSecurityStatus;
	QuoteRequestList lstQuoteRequest;

	MessageHeader hdr;

	__int64 sequence = (long)MarketDataDecoderUtil::getPacketSequenceNumber(message.buffer);
	__int64 sentTime = MarketDataDecoderUtil::getPacketSentEpochTime(message.buffer);

	TheBEAST::SequenceCheck::getInstance()->CheckSequence(message.applID, sequence);

	int nOffset = BINARY_PACKET_HEADER;

	try
	{
		do
		{
			int MsgSize = MarketDataDecoderUtil::getMsgSize(message.buffer, nOffset);
			nOffset += 2;

			hdr.wrap(const_cast<char*>(message.buffer), nOffset, 0, message.length);
			int templateId = hdr.templateId();
			nOffset += hdr.size();

			switch(templateId)
			{
			case 27:
				futureContractTemplate27Decoder.decode(message, lstContract, sequence, sentTime, nOffset, hdr);
				break;

			case 29:
				spreadContractTemplate29Decoder.decode(message, lstContract, sequence, sentTime, nOffset, hdr);
				break;

			case 41:
				optionContractTemplate41Decoder.decode(message, lstContract, sequence, sentTime, nOffset, hdr);
				break; 
		
			case 4:
				channelReset4Decoder.decode(message, lstChannelReset, sequence, sentTime, nOffset, hdr);
				break;

			case 30:
				securityStatus30Decoder.decode(message, lstSecurityStatus, sequence, sentTime, nOffset, hdr);
				break;

			case 32:
				incrementalRefresh32Decoder.decode(message, lstMktData, sequence, sentTime, nOffset, hdr);
				break;

			case 33:
				incrementalRefresh33Decoder.decode(message, lstMktData, sequence, sentTime, nOffset, hdr);
				break; 

			case 34:
				incrementalRefresh34Decoder.decode(message, lstMktData, sequence, sentTime, nOffset, hdr);
				break;

			case 35:
				incrementalRefresh35Decoder.decode(message, lstMktData, sequence, sentTime, nOffset, hdr);
				break;

			case 36:
				incrementalRefresh36Decoder.decode(message, lstMktData, sequence, sentTime, nOffset, hdr);
				break;

			case 37:
				incrementalRefresh37Decoder.decode(message, lstMktData, sequence, sentTime, nOffset, hdr);
				break;

			case 39:
				quoteRequest39Decoder.decode(message, lstQuoteRequest, sequence, sentTime, nOffset, hdr);
				break;

			case 42:
				incrementalRefresh42Decoder.decode(message, lstMktData, sequence, sentTime, nOffset, hdr);
				break;
			}

			nOffset = nOffset + (MsgSize - 10);

		}while(nOffset < (message.length-2));
	}
	catch(...)
	{
	}

	string fixMessage;

	for( SecurityDefListItr itrSecDef = lstContract.begin(); itrSecDef != lstContract.end(); itrSecDef++)
	{
		// Contracts
		if(MessageUtil::getInstance()->updateContracts(itrSecDef->objContract))
		{
			string secdefMessage = itrSecDef->objContract.getContract();

			if(m_pContactFile == NULL)
				fopen_s(&m_pContactFile, m_strContractFile.c_str() ,"a");

			if(m_pContactFile)
			{
				fwrite(secdefMessage.c_str(), secdefMessage.length(), 1, m_pContactFile);
				fwrite("\n", 1, 1, m_pContactFile);
			}
		}

		// Master Security Definition
		fixMessage = itrSecDef->getString();
		if(m_pMstSecDefFile == NULL)
			fopen_s(&m_pMstSecDefFile, m_strMstSecDefFile.c_str() ,"a");

		if(m_pMstSecDefFile)
		{
			fwrite(fixMessage.c_str(), fixMessage.length(), 1, m_pMstSecDefFile);
			fwrite("\n", 1, 1, m_pMstSecDefFile);
		}

		// Product Files
		FileList filelist = itrSecDef->getFiles();

		for( FileListItr itrFile = filelist.begin(); itrFile != filelist.end(); itrFile++)
		{
			writeToFile(*itrFile, fixMessage);
		}
	}

	for( MarketDataListItr itrMktData = lstMktData.begin(); itrMktData != lstMktData.end(); itrMktData++)
	{
		writeToFile(itrMktData->getoutputfile(), itrMktData->getString());
	}

	for( ChannelResetListItr itrChannel = lstChannelReset.begin(); itrChannel != lstChannelReset.end(); itrChannel++)
	{
		fixMessage = itrChannel->getString();
		FileList filelist = itrChannel->getFiles();

		for( FileListItr itfile = filelist.begin(); itfile != filelist.end(); itfile++)
		{
			writeToFile(*itfile, fixMessage);
		}
	}

	for( SecurityStatusListItr itrSecStat = lstSecurityStatus.begin(); itrSecStat != lstSecurityStatus.end(); itrSecStat++)
	{
		fixMessage = itrSecStat->getString();
		FileList filelist = itrSecStat->getFiles(message.applID);

		for( FileListItr itrFile = filelist.begin(); itrFile != filelist.end(); itrFile++)
		{
			writeToFile(*itrFile, fixMessage);
		}
	}

	for( QuoteRequestListItr it = lstQuoteRequest.begin(); it != lstQuoteRequest.end(); it++)
	{
		writeToFile(it->getoutputfile(), it->getString());
	}
}

void MDMessageConvertor::writeToFile(string &strfilename, string &fixMessage)
{
	string strExchange = strfilename.substr(0, strfilename.find("\\"));
	string strFolder = stroutputfolder + "\\" + strExchange;

	if( !(boost::filesystem::exists(strFolder)))
		boost::filesystem::create_directories(strFolder);

	string strfilepath = stroutputfolder + "\\" + strfilename + "_" + stroutdate;

	if(files.size() >= 2000)
		close();

	FILE *pFile=0;
	FilePtrItr itrFile = files.find(strfilepath);

	if(itrFile != files.end())
	{
		pFile = itrFile->second;
	}
	else
	{
		fopen_s(&pFile, strfilepath.c_str() ,"a");
		files.insert(pair<string, FILE*>(strfilepath, pFile));
	}
		
	if(pFile)
	{
		fwrite(fixMessage.c_str(), fixMessage.length(), 1, pFile);
		fwrite("\n", 1, 1, pFile);
	}
}

void MDMessageConvertor::convertMessageBinary(Message &message)
{
	std::stringstream stream;
	stream << message.applID;
	string strChannel = stream.str();

	string strFolder = stroutputfolder + "\\" + strChannel;
	short nChannel = message.applID;
	short length = message.length;

	if( !(boost::filesystem::exists(strFolder)))
		boost::filesystem::create_directories(strFolder);
			
	string strfilename = strFolder + "\\" + strChannel + "_" + stroutdate;

	if(files.size() >= 2000)
		close();

	FilePtrItr itrFile = files.find(strfilename);

	FILE *pFile = NULL;

	if(itrFile != files.end())
	{
		pFile = itrFile->second;
	}
	else
	{
		fopen_s(&pFile, strfilename.c_str() ,"a");
		files.insert(pair<string, FILE*>(strfilename, pFile));
	}
		
	if(pFile)
	{
		fwrite(&length, 2, 1, pFile);
		fwrite(&nChannel, 2, 1, pFile);
		fwrite(message.buffer, message.length, 1, pFile);
	}
}

void MDMessageConvertor::setup(string inputfolder, string outputfolder, string contractFile, string secdefFile)
{
	stroutputfolder = outputfolder;
	stroutdate = inputfolder.substr(inputfolder.find_last_of("\\") + 1);
	
	// Contracts
	m_strContractFile = contractFile;
	MessageUtil::getInstance()->getContracts(m_strContractFile);
	fopen_s(&m_pContactFile, m_strContractFile.c_str() ,"a");

	// Master Security Definition
	m_strMstSecDefFile = secdefFile;
	fopen_s(&m_pMstSecDefFile, m_strMstSecDefFile.c_str() ,"a");
}

void MDMessageConvertor::close()
{
	FilePtrItr itrFile = files.begin();

	while(itrFile != files.end())
	{
		fclose(itrFile->second);
		itrFile++;
	}

	files.clear();

	// Contracts
	if(m_pContactFile)
	{
		fclose(m_pContactFile);
		m_pContactFile = NULL;
	}

	// Master Security Definition
	if(m_pMstSecDefFile)
	{
		fclose(m_pMstSecDefFile);
		m_pMstSecDefFile = NULL;
	}
}

SecDefMessageConvertor::SecDefMessageConvertor()
{
	BINARY_PACKET_HEADER = 12;
}

SecDefMessageConvertor::~SecDefMessageConvertor()
{
}

void SecDefMessageConvertor::setup(string contractFile)
{
	m_strContractFile = contractFile;
	MessageUtil::getInstance()->getContracts(m_strContractFile);
	fopen_s(&m_pContactFile, m_strContractFile.c_str() ,"a");
}

void SecDefMessageConvertor::close()
{
	if(m_pContactFile)
	{
		fclose(m_pContactFile);
		m_pContactFile = NULL;
	}
}

void SecDefMessageConvertor::convertInstrumentDef(Message &message)
{
	SecurityDefList lstContracts;
	MessageHeader hdr;

	long sequence = (long)MarketDataDecoderUtil::getPacketSequenceNumber(message.buffer);
	__int64 sentTime = MarketDataDecoderUtil::getPacketSentEpochTime(message.buffer);

	int nOffset = BINARY_PACKET_HEADER;

	do
	{
		int MsgSize = MarketDataDecoderUtil::getMsgSize(message.buffer, nOffset);
		nOffset += 2;

		hdr.wrap(const_cast<char*>(message.buffer), nOffset, 0, message.length);
		int templateId = hdr.templateId();
		nOffset += hdr.size();

		switch(templateId)
		{
		case 27:
			futureContractTemplate27Decoder.decode(message, lstContracts, sequence, sentTime, nOffset, hdr);
			break;

		case 29:
			spreadContractTemplate29Decoder.decode(message, lstContracts, sequence, sentTime, nOffset, hdr);
			break;

		case 41:
			optionContractTemplate41Decoder.decode(message, lstContracts, sequence, sentTime, nOffset, hdr);
			break; 
		}

		nOffset = nOffset + (MsgSize - 10);

	}while(nOffset < (message.length-2));

	
	string fixMessage;
	for( SecurityDefListItr itrSecDef = lstContracts.begin(); itrSecDef != lstContracts.end(); itrSecDef++)
	{
		if(MessageUtil::getInstance()->updateContracts(itrSecDef->objContract))
		{
			string secdefMessage = itrSecDef->objContract.getContract();

			if(m_pContactFile == NULL)
				fopen_s(&m_pContactFile, m_strContractFile.c_str() ,"a");

			if(m_pContactFile)
			{
				fwrite(secdefMessage.c_str(), secdefMessage.length(), 1, m_pContactFile);
				fwrite("\n", 1, 1, m_pContactFile);
			}
		}
	}
}